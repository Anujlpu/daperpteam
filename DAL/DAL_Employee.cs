﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Utility;

namespace DAL
{
    public class DAL_Employee
    {
        public List<EmployeeEntity> GetEmployeelist(int Role)
        {
            List<EmployeeEntity> Employeelist = new List<EmployeeEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 4);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageEmployee", arParms))
            {
                while (reader.Read())
                {


                    if (Role == 2 && Convert.ToInt32(reader["RoleId"].ToString()) == 2)
                    {
                        Employeelist.Add(new EmployeeEntity
                        {
                            EmployeeName = reader["EmployeeName"].ToString(),
                            DOJ = Convert.ToDateTime(reader["DOJ"].ToString()),
                            DOB = Convert.ToDateTime(reader["DOB"].ToString()),
                            Address = reader["Address"].ToString(),
                            MobileNo = reader["MobileNo"].ToString(),
                            P_EmailId = reader["P_EmailId"].ToString(),
                            BasicSalary = reader["BasicSalary"].ToString(),
                            BasicHourlySalary = reader["BasicHourlySalary"].ToString(),
                            ActualSalary = reader["ActualSalary"].ToString(),
                            ActualHourlySalary = reader["ActualHourlySalary"].ToString(),
                            EmployeeStatus = reader["EmployeeStatus"].ToString(),
                            HiredLocation = reader["HiredLocation"].ToString(),
                            CurrentWorkingLocation = reader["CurrentWorkingLocation"].ToString(),
                            LivingAllowance = reader["LivingAllowance"].ToString(),
                            TravelAllowance = reader["TravelAllowance"].ToString(),
                            VisaIssueDate = Convert.ToDateTime(reader["VisaIssueDate"].ToString()),
                            VisaExpireDate = Convert.ToDateTime(reader["VisaExpireDate"].ToString()),
                            VisaDescription = (reader["VisaDescription"].ToString()),
                            EmailId = reader["EmailId"].ToString(),
                            RoleId = Convert.ToInt32(reader["RoleId"].ToString()),
                            Department = reader["DepartmentId"].ToString(),
                            LocationId = Convert.ToInt32(reader["LocationId"].ToString()),
                            UserName = reader["UserName"].ToString(),
                            Password = reader["Password"].ToString(),
                            EmployeeID = Convert.ToInt32(reader["EmployeeId"].ToString()),
                        });
                    }
                    else if (Role == 3 && Convert.ToInt32(reader["RoleId"].ToString()) != 2)
                    {
                        Employeelist.Add(new EmployeeEntity
                        {
                            EmployeeName = reader["EmployeeName"].ToString(),
                            DOJ = Convert.ToDateTime(reader["DOJ"].ToString()),
                            DOB = Convert.ToDateTime(reader["DOB"].ToString()),
                            Address = reader["Address"].ToString(),
                            MobileNo = reader["MobileNo"].ToString(),
                            P_EmailId = reader["P_EmailId"].ToString(),
                            BasicSalary = reader["BasicSalary"].ToString(),
                            BasicHourlySalary = reader["BasicHourlySalary"].ToString(),
                            ActualSalary = reader["ActualSalary"].ToString(),
                            ActualHourlySalary = reader["ActualHourlySalary"].ToString(),
                            EmployeeStatus = reader["EmployeeStatus"].ToString(),
                            HiredLocation = reader["HiredLocation"].ToString(),
                            CurrentWorkingLocation = reader["CurrentWorkingLocation"].ToString(),
                            LivingAllowance = reader["LivingAllowance"].ToString(),
                            TravelAllowance = reader["TravelAllowance"].ToString(),
                            VisaIssueDate = Convert.ToDateTime(reader["VisaIssueDate"].ToString()),
                            VisaExpireDate = Convert.ToDateTime(reader["VisaExpireDate"].ToString()),
                            VisaDescription = (reader["VisaDescription"].ToString()),
                            EmailId = reader["EmailId"].ToString(),
                            RoleId = Convert.ToInt32(reader["RoleId"].ToString()),
                            Department = reader["DepartmentId"].ToString(),
                            LocationId = Convert.ToInt32(reader["LocationId"].ToString()),
                            UserName = reader["UserName"].ToString(),
                            Password = reader["Password"].ToString(),
                            Employementtype = reader["EmployementType"].ToString(),
                            EmployeeID = Convert.ToInt32(reader["EmployeeId"].ToString()),
                        });
                    }
                }
            }
            return Employeelist;

        }

        public List<EmployeeEntity> GetEmployeelist()
        {
            List<EmployeeEntity> Employeelist = new List<EmployeeEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 4);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageEmployee", arParms))
            {
                while (reader.Read())
                {
                    Employeelist.Add(new EmployeeEntity
                    {
                        EmployeeName = reader["EmployeeName"].ToString(),
                        DOJ = Convert.ToDateTime(reader["DOJ"].ToString()),
                        DOB = Convert.ToDateTime(reader["DOB"].ToString()),
                        Address = reader["Address"].ToString(),
                        MobileNo = reader["MobileNo"].ToString(),
                        P_EmailId = reader["P_EmailId"].ToString(),
                        BasicSalary = reader["BasicSalary"].ToString(),
                        BasicHourlySalary = reader["BasicHourlySalary"].ToString(),
                        ActualSalary = reader["ActualSalary"].ToString(),
                        ActualHourlySalary = reader["ActualHourlySalary"].ToString(),
                        EmployeeStatus = reader["EmployeeStatus"].ToString(),
                        HiredLocation = reader["HiredLocation"].ToString(),
                        CurrentWorkingLocation = reader["CurrentWorkingLocation"].ToString(),
                        LivingAllowance = reader["LivingAllowance"].ToString(),
                        TravelAllowance = reader["TravelAllowance"].ToString(),
                        VisaIssueDate = Convert.ToDateTime(reader["VisaIssueDate"].ToString()),
                        VisaExpireDate = Convert.ToDateTime(reader["VisaExpireDate"].ToString()),
                        VisaDescription = (reader["VisaDescription"].ToString()),
                        EmailId = reader["EmailId"].ToString(),
                        RoleId = Convert.ToInt32(reader["RoleId"].ToString()),
                        Department = reader["Department"].ToString(),
                        LocationId = Convert.ToInt32(reader["LocationId"].ToString()),
                        UserName = reader["UserName"].ToString(),
                        Password = reader["Password"].ToString(),
                        EmployeeID = Convert.ToInt32(reader["EmployeeId"].ToString()),
                        Employementtype = reader["EmployementType"].ToString(),
                        EmergencyContactNumber = reader["EmergencyContactNumber"].ToString(),
                        EmegencyContactAddress = reader["EmergencyContactAddress"].ToString(),
                        HourlyRateEffFrom = Convert.ToDateTime(reader["HourlyRateEffFrom"] ?? DateTime.MinValue),
                        HourlyRateEffTo = Convert.ToDateTime(reader["HourlyRateEffTo"] ?? DateTime.MinValue),
                        PayRollRate =float.Parse(Convert.ToString(reader["PayRollRate"])),
                        PayRollEffFrom = Convert.ToDateTime(reader["PayRollEffFrom"] ?? DateTime.MinValue),
                        PayRollEffTo = Convert.ToDateTime(reader["PayRollEffTo"] ?? DateTime.MinValue),

                    });
                }
            }
            return Employeelist;

        }
        public List<PaymentEntity> GetPaymentlist()
        {
            List<PaymentEntity> Profilelist = new List<PaymentEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 1);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_GetPaymnetType", arParms))
            {
                while (reader.Read())
                {
                    Profilelist.Add(new PaymentEntity
                    {
                        PaymentTypeId = reader.GetInt32(0),
                        PaymentTypeCode = reader.GetString(1),
                    });
                }
            }
            return Profilelist;

        }
        public int InsertUser(EmployeeEntity EmployeeEntity, int Type)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[36];
                arParms[0] = new SqlParameter("@Type", Type);
                arParms[1] = new SqlParameter("@EmployeeName", EmployeeEntity.EmployeeName);
                arParms[2] = new SqlParameter("@DOJ", EmployeeEntity.DOJ);
                arParms[3] = new SqlParameter("@DOB", EmployeeEntity.DOB);
                arParms[4] = new SqlParameter("@EmailId", EmployeeEntity.EmailId);
                arParms[5] = new SqlParameter("@UserName", EmployeeEntity.UserName);
                arParms[6] = new SqlParameter("@Password", EmployeeEntity.Password);
                arParms[7] = new SqlParameter("@Address", EmployeeEntity.Address);
                arParms[8] = new SqlParameter("@RoleId", EmployeeEntity.RoleId);
                arParms[9] = new SqlParameter("@Department", EmployeeEntity.Department);
                arParms[10] = new SqlParameter("@LocationId", EmployeeEntity.LocationId);
                arParms[11] = new SqlParameter("@MobileNo", EmployeeEntity.MobileNo);
                arParms[12] = new SqlParameter("@P_EmailId", EmployeeEntity.P_EmailId);
                arParms[13] = new SqlParameter("@BasicSalary", EmployeeEntity.BasicSalary);
                arParms[14] = new SqlParameter("@BasicHourlySalary", EmployeeEntity.BasicHourlySalary);
                arParms[15] = new SqlParameter("@ActualSalary", EmployeeEntity.ActualSalary);
                arParms[16] = new SqlParameter("@ActualHourlySalary", EmployeeEntity.ActualHourlySalary);
                arParms[17] = new SqlParameter("@EmployeeStatus", EmployeeEntity.EmployeeStatus);
                arParms[18] = new SqlParameter("@HiredLocation", EmployeeEntity.HiredLocation);
                arParms[19] = new SqlParameter("@CurrentWorkingLocation", EmployeeEntity.CurrentWorkingLocation);
                arParms[20] = new SqlParameter("@LivingAllowance", EmployeeEntity.LivingAllowance);
                arParms[21] = new SqlParameter("@TravelAllowance", EmployeeEntity.TravelAllowance);
                arParms[22] = new SqlParameter("@VisaIssueDate", EmployeeEntity.VisaIssueDate);
                arParms[23] = new SqlParameter("@VisaExpireDate", EmployeeEntity.VisaExpireDate);
                arParms[24] = new SqlParameter("@VisaDescription", EmployeeEntity.VisaDescription);
                arParms[25] = new SqlParameter("@Remarks", EmployeeEntity.Remarks);
                arParms[26] = new SqlParameter("@UserId", EmployeeEntity.UserId);
                arParms[27] = new SqlParameter("@EmployeeID", EmployeeEntity.EmployeeID);
                arParms[28] = new SqlParameter("@EmployementType", EmployeeEntity.Employementtype);
                arParms[29] = new SqlParameter("@EmergencyContactNumber", EmployeeEntity.EmergencyContactNumber);
                arParms[30] = new SqlParameter("@EmergencyContactAddress", EmployeeEntity.EmegencyContactAddress);
                arParms[31] = new SqlParameter("@HourlyRateEffFrom", EmployeeEntity.HourlyRateEffFrom);
                arParms[32] = new SqlParameter("@HourlyRateEffTo", EmployeeEntity.HourlyRateEffTo);
                arParms[33] = new SqlParameter("@PayRollRate", EmployeeEntity.PayRollRate);
                arParms[34] = new SqlParameter("@PayRollEffFrom", EmployeeEntity.PayRollEffFrom);
                arParms[35] = new SqlParameter("@PayRollEffTo", EmployeeEntity.PayRollEffTo);
                gp.ExecuteNonQuery("Proc_ManageEmployee", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeleteEmployee(EmployeeEntity employeeEntity)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@type", 4);
                arParms[1] = new SqlParameter("@EmployeeID", employeeEntity.EmployeeID);
                arParms[2] = new SqlParameter("@UserId", employeeEntity.UserId);

                gp.ExecuteNonQuery("Proc_ManageVendor", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }

        public List<EmpStatusEntity> GetEmpStatusList()
        {
            List<EmpStatusEntity> EmpStatusList = new List<EmpStatusEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 2);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_GetDepartEmpStatus", arParms))
            {
                while (reader.Read())
                {
                    EmpStatusList.Add(new EmpStatusEntity
                    {
                        EmployeeStatusId = reader.GetInt32(0),
                        EmployeeStatus = reader.GetString(1),

                    });
                }
            }
            return EmpStatusList;

        }

        public List<DepartEntity> GetDepartList()
        {
            List<DepartEntity> Profilelist = new List<DepartEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 1);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_GetDepartEmpStatus", arParms))
            {
                while (reader.Read())
                {
                    Profilelist.Add(new DepartEntity
                    {
                        DepartmentId = reader.GetInt32(0),
                        DepartName = reader.GetString(1),

                    });
                }
            }
            return Profilelist;

        }


    }
}
