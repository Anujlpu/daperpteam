﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Utility;

namespace DAL
{
    public class DAL_Vendor
    {
        public List<VenderEntity> GetVendorlist()
        {
            List<VenderEntity> Vendorlist = new List<VenderEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 3);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageVendor", arParms))
            {
                while (reader.Read())
                {
                    Vendorlist.Add(new VenderEntity
                    {
                        VendorCode = reader["VendorCode"].ToString(),
                        VendorName = reader["VendorName"].ToString(),
                        ContactNo = reader["ContactNo"].ToString(),
                        EmailId = reader["EmailId"].ToString(),
                        OnlineUserName = reader["OnlineUserName"].ToString(),
                        StoreEmailPassword = reader["Password"].ToString(),
                        AccountNo = reader["AccountNo"].ToString(),
                        Address = reader["Address"].ToString(),
                        SEName = reader["SEName"].ToString(),
                        SEEmailId = reader["SEEmailId"].ToString(),
                        SEMobileNo = reader["SEMobileNo"].ToString(),
                        DAPEmailId = reader["DAPEmailId"].ToString(),
                        PaymentType = reader["PaymentType"].ToString(),
                        VendorId = reader.GetInt32(0),
                        VendorType = reader["VendorType"].ToString(),
                        OnlinePortal = reader["OnlinePortal"].ToString(),
                        OnlinePortalUserName = reader["OnlineUserName"].ToString(),
                        OnlinePortalPassword = reader["Password"].ToString(),
                        ApplicationStatus = reader["ApplicationStatus"].ToString(),
                        ApplicationDate = (reader["ApplicationDate"]) == null ? Convert.ToDateTime(reader["ApplicationDate"]) : DateTime.Now
                    });
                }
            }
            return Vendorlist;

        }
        public List<PaymentEntity> GetPaymentlist()
        {
            List<PaymentEntity> Profilelist = new List<PaymentEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 1);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_GetPaymnetType", arParms))
            {
                while (reader.Read())
                {
                    Profilelist.Add(new PaymentEntity
                    {
                        PaymentTypeId = reader.GetInt32(0),
                        PaymentTypeCode = reader.GetString(1),

                    });
                }
            }
            return Profilelist;

        }
        public int InsertUser(VenderEntity vendorEntity, int Type)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[23];
                arParms[0] = new SqlParameter("@Type", Type);
                arParms[1] = new SqlParameter("@VendorCode", vendorEntity.VendorCode);
                arParms[2] = new SqlParameter("@VendorName", vendorEntity.VendorName);
                arParms[3] = new SqlParameter("@ContactNo", vendorEntity.ContactNo);
                arParms[4] = new SqlParameter("@EmailId", vendorEntity.EmailId);
                arParms[5] = new SqlParameter("@OnlineUserName", vendorEntity.OnlineUserName);
                arParms[6] = new SqlParameter("@Password", vendorEntity.StoreEmailPassword);
                arParms[7] = new SqlParameter("@Address", vendorEntity.Address);
                arParms[8] = new SqlParameter("@SEName", vendorEntity.SEName);
                arParms[9] = new SqlParameter("@SEEmailId", vendorEntity.SEEmailId);
                arParms[10] = new SqlParameter("@SEMobileNo", vendorEntity.SEMobileNo);
                arParms[11] = new SqlParameter("@DAPEmailId", vendorEntity.DAPEmailId);
                arParms[12] = new SqlParameter("@PaymentType", vendorEntity.PaymentType);
                arParms[13] = new SqlParameter("@CreatedBy", vendorEntity.CreatedBy);
                arParms[14] = new SqlParameter("@ModifiedBy", vendorEntity.ModifiedBy);
                arParms[15] = new SqlParameter("@VendorId", vendorEntity.VendorId);
                arParms[16] = new SqlParameter("@AccountNo", vendorEntity.AccountNo);
                arParms[17] = new SqlParameter("@OnlinePortal", vendorEntity.OnlinePortal);
                arParms[18] = new SqlParameter("@OnlinePortalUserName", vendorEntity.OnlinePortalUserName);
                arParms[19] = new SqlParameter("@OnlinePortalPassword", vendorEntity.OnlinePortalPassword);
                arParms[20] = new SqlParameter("@VendorType", vendorEntity.VendorType);
                arParms[21] = new SqlParameter("@ApplicationStatus", vendorEntity.ApplicationStatus);
                arParms[22] = new SqlParameter("@ApplicationDate", vendorEntity.ApplicationDate);
                gp.ExecuteNonQuery("Proc_ManageVendor", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeleteVendor(VenderEntity vendorEntity)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@type", 4);
                arParms[1] = new SqlParameter("@VendorId", vendorEntity.VendorId);
                arParms[2] = new SqlParameter("@ModifiedBy", vendorEntity.ModifiedBy);

                gp.ExecuteNonQuery("Proc_ManageVendor", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
    }
}
