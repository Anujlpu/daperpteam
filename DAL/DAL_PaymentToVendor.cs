﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Utility;

namespace DAL
{
    public class DAL_PaymentToVendor
    {
        public List<PaidPaymentEntity> GetPaidPaymentlist()
        {
            List<PaidPaymentEntity> Paidpaymentlist = new List<PaidPaymentEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 4);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManagePaymentTransaction", arParms))
            {
                while (reader.Read())
                {
                    Paidpaymentlist.Add(new PaidPaymentEntity
                    {
                        PaymentDate = Convert.ToDateTime(reader["PaymentDate"].ToString()),
                        Amount = Convert.ToDouble(reader["Amount"].ToString()),
                        InvoiceNo = reader["InvoiceNo"].ToString(),
                        CashPayoutAmount = Convert.ToDouble(reader["CashPayoutAmount"].ToString()),
                        CashAmount = Convert.ToDouble(reader["CashPaidAmount"].ToString()),
                        PaymentId = Convert.ToInt32(reader["Paymentid"].ToString()),
                        PaymentType = Convert.ToInt32(reader["PaymentType"].ToString()),
                        PaymentToId = Convert.ToInt32(reader["PaymentToId"].ToString()),
                        Remarks  = reader["Remarks"].ToString(),
                        TransactionType = Convert.ToInt32(reader["TransactionType"].ToString()),
                    });
                }
            }
            return Paidpaymentlist;

        }

        public int InsertPaymentDetails(PaidPaymentEntity PaymentEntity, int Type)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[13];
                arParms[0] = new SqlParameter("@Type", Type);
                arParms[1] = new SqlParameter("@PaymentDate", PaymentEntity.PaymentDate);
                arParms[2] = new SqlParameter("@TransactionType", PaymentEntity.TransactionType);
                arParms[3] = new SqlParameter("@PaymentToId", PaymentEntity.PaymentToId);
                arParms[4] = new SqlParameter("@PaymentType", PaymentEntity.PaymentType);
                arParms[5] = new SqlParameter("@InvoiceNo", PaymentEntity.InvoiceNo);
                arParms[6] = new SqlParameter("@Amount", PaymentEntity.Amount);
                arParms[7] = new SqlParameter("@Remarks", PaymentEntity.Remarks);
                arParms[8] = new SqlParameter("@UploadedDocPath", PaymentEntity.UploadedDocPath);
                arParms[9] = new SqlParameter("@UserId", PaymentEntity.UserId);
                arParms[10] = new SqlParameter("@PaymentId", PaymentEntity.PaymentId);
                arParms[11] = new SqlParameter("@Cash", PaymentEntity.CashAmount);
                arParms[12] = new SqlParameter("@Payout", PaymentEntity.CashPayoutAmount);
                gp.ExecuteNonQuery("Proc_ManagePaymentTransaction", arParms); return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeleteEmployee(PaidPaymentEntity PaymentEntity)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@type", 3);
                arParms[1] = new SqlParameter("@EmployeeID", PaymentEntity.PaymentId);
                arParms[2] = new SqlParameter("@UserId", PaymentEntity.UserId);

                gp.ExecuteNonQuery("Proc_ManagePaymentTransaction", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }

    }
}
