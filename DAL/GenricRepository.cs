﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;
using Utility;
namespace DAL
{
    public class GenricRepository : IGenricRepository
    {
        public string cnnSqlConnection = "";

        public GenricRepository()
        {
            // string ConnectionString = BA_Commman.Decrypt("", true)
            string ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnnectionString"].ToString();

            cnnSqlConnection = ConnectionString;
        }

        public int ExecuteNonQuery(string ProcName, params System.Data.SqlClient.SqlParameter[] param)
        {


            return SqlHelper.ExecuteNonQuery(cnnSqlConnection, CommandType.StoredProcedure, ProcName, param);

        }
        public string ExecuteScalar(string ProcName, params System.Data.SqlClient.SqlParameter[] param)
        {

            return SqlHelper.ExecuteScalar(cnnSqlConnection, CommandType.StoredProcedure, ProcName, param).ToString();
        }
        public DataSet ExecuteDataset(string ProcName, params System.Data.SqlClient.SqlParameter[] param)
        {
            //pass through the call providing null for the set of SqlParameters
            return SqlHelper.ExecuteDataset(cnnSqlConnection, CommandType.StoredProcedure, ProcName, param);
        }



        public SqlDataReader ExecuteDatareader(string ProcName, params System.Data.SqlClient.SqlParameter[] param)
        {
            return SqlHelper.ExecuteReader(cnnSqlConnection, CommandType.StoredProcedure, ProcName, param);
        }
    }
}
