﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Utility;

namespace DAL
{
    public class DAL_Location
    {
        public List<LocationEntity> GetLocationlist()
        {
            List<LocationEntity> Locationlist = new List<LocationEntity>();
            try
            {
                SqlParameter[] arParms = new SqlParameter[1];
                arParms[0] = new SqlParameter("@Type", 4);
                GenricRepository gp = new GenricRepository();
                using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageLocation", arParms))
                {
                    while (reader.Read())
                    {
                        Locationlist.Add(new LocationEntity
                        {

                            LocationId = int.Parse(reader.GetValue(0).ToString()),
                            LocationCode = reader.GetString(1),
                            LocationName = reader.GetString(2),
                            LocationFullAddress = reader.GetString(3),
                            CreatedBy = 0

                        });
                    }
                }
                return Locationlist;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);

            }
            return Locationlist;

        }
        public int InsertLocation(LocationEntity locationEntity, int Type)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[6];
                arParms[0] = new SqlParameter("@Type", Type);
                arParms[1] = new SqlParameter("@LocationCode", locationEntity.LocationCode);
                arParms[2] = new SqlParameter("@LocationName", locationEntity.LocationName);
                arParms[3] = new SqlParameter("@LocationDescription", locationEntity.LocationFullAddress);
                arParms[4] = new SqlParameter("@LocationId", locationEntity.LocationId);
                arParms[5] = new SqlParameter("@UserId", locationEntity.CreatedBy);
                gp.ExecuteNonQuery("Proc_ManageLocation", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeleteLocation(LocationEntity locationEntity)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@Type", 3);
                arParms[1] = new SqlParameter("@LocationId", locationEntity.LocationId);
                arParms[2] = new SqlParameter("@UserId", locationEntity.CreatedBy);
                gp.ExecuteNonQuery("Proc_ManageLocation", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
    }
}
