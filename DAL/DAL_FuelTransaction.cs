﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data.SqlClient;
using System.Globalization;
using Utility;
using System.Data;

namespace DAL
{
    public class DAL_FuelTransaction
    {
        public string GetPath(string dt)
        {
            string FilePath = String.Empty;
            SqlParameter[] arParms = new SqlParameter[2];
            arParms[0] = new SqlParameter("@type", 4);
            arParms[1] = new SqlParameter("@UploadId", dt);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageDocument", arParms))
            {
                while (reader.Read())
                {
                    FilePath = Convert.ToString(reader["FilePath"]);



                }
            }
            return FilePath;
        }
        public List<DocsEntity> GetDocsList(string dt)
        {
            List<DocsEntity> DocsList = new List<DocsEntity>();
            SqlParameter[] arParms = new SqlParameter[2];
            arParms[0] = new SqlParameter("@type", 3);
            arParms[1] = new SqlParameter("@TransactionDate", dt);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageDocument", arParms))
            {
                while (reader.Read())
                {
                    DocsList.Add(new DocsEntity
                    {
                        DocId = Convert.ToInt32(reader["UploadId"]),
                        DocName = Convert.ToString(reader["DocsIdentity"]),
                        DocType = Convert.ToString(reader["DocsType"]),
                        FilePath = Convert.ToString(reader["FilePath"]),
                        TransactionDate = Convert.ToDateTime(reader["TransactionDate"]),
                    });

                }
            }
            return DocsList;
        }
        public List<DocsEntity> GetDocsList(string dt, string DocType)
        {
            List<DocsEntity> DocsList = new List<DocsEntity>();
            SqlParameter[] arParms = new SqlParameter[3];
            arParms[0] = new SqlParameter("@Type", 3);
            arParms[1] = new SqlParameter("@TransactionDate", dt);
            arParms[2] = new SqlParameter("@DocsType", DocType);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageDocument", arParms))
            {
                while (reader.Read())
                {
                    if (Convert.ToString(reader["DocsType"]) != "--Select--")
                    {
                        DocsList.Add(new DocsEntity
                        {
                            DocId = Convert.ToInt32(reader["UploadId"]),
                            DocName = Convert.ToString(reader["DocsIdentity"]),
                            DocType = Convert.ToString(reader["DocsType"]),
                            FilePath = Convert.ToString(reader["FilePath"]),
                            TransactionDate = Convert.ToDateTime(reader["TransactionDate"]),
                            CreatedBy = Convert.ToString(reader["UserName"]),
                            CreatedDate = Convert.ToDateTime(reader["CreatedDate"])
                        });
                    }

                }
            }
            return DocsList;
        }
        public int ManageDocs(DocsEntity docety)
        {

            try
            {
                List<DocsEntity> DocsList = new List<DocsEntity>();
                SqlParameter[] arParms = new SqlParameter[6];
                arParms[0] = new SqlParameter("@type", 1);
                arParms[1] = new SqlParameter("@TransactionDate", docety.TransactionDate);
                arParms[2] = new SqlParameter("@DocsType", docety.DocType);
                arParms[3] = new SqlParameter("@FilePath", docety.FilePath);
                arParms[4] = new SqlParameter("@docIdentity", docety.DocName);
                arParms[5] = new SqlParameter("@userid", docety.UserId);
                GenricRepository gp = new GenricRepository();
                gp.ExecuteNonQuery("Proc_ManageDocument", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int Deletedocs(DocsEntity docety)
        {
            try
            {
                List<DocsEntity> DocsList = new List<DocsEntity>();
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@type", 2);
                arParms[1] = new SqlParameter("@UploadId", docety.DocId);

                GenricRepository gp = new GenricRepository();
                gp.ExecuteNonQuery("Proc_ManageDocument", arParms);
                return 1;

            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }

        public List<FuelTransactionEntities> GetFuelTransactionList(string dt)
        {
            List<FuelTransactionEntities> transactionList = new List<FuelTransactionEntities>();
            SqlParameter[] arParms;
            if (dt == "")
            {
                arParms = new SqlParameter[1];
                arParms[0] = new SqlParameter("@type", 1);

            }
            else
            {
                arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@type", 6);
                arParms[1] = new SqlParameter("@TransactionDate", dt);

            }
            GenricRepository gp = new GenricRepository();

            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_FuelTransaction", arParms))
            {
                while (reader.Read())
                {
                    transactionList.Add(new FuelTransactionEntities
                    {
                        #region C-Store Sale Details
                        GasTransactionID = Convert.ToInt32(reader["GasTransactionID"]),
                        TransactionDate = Convert.ToDateTime(reader["TransactionDate"]),
                        C_StoreSale = float.Parse((reader["C_StoreSale"].ToString()), CultureInfo.InvariantCulture.NumberFormat),
                        C_AttachmentID = Convert.ToString(reader["C_AttachmentID"]),
                        C_Store_Remakrs = Convert.ToString(reader["C_Store_Remakrs"]),
                        #endregion
                        #region TabbbacoDetial
                        Tabbacco = float.Parse(Convert.ToString(reader["Tabbacco"]), CultureInfo.InvariantCulture.NumberFormat),
                        OnlineLotto = float.Parse(Convert.ToString(reader["OnlineLotto"]), CultureInfo.InvariantCulture.NumberFormat),
                        ScratchLotto = float.Parse(Convert.ToString(reader["ScratchLotto"]), CultureInfo.InvariantCulture.NumberFormat),
                        PhoneCard = float.Parse(Convert.ToString(reader["PhoneCard"]), CultureInfo.InvariantCulture.NumberFormat),
                        GiftCard = float.Parse(Convert.ToString(reader["GiftCard"]), CultureInfo.InvariantCulture.NumberFormat),
                        ShellGifTcar = float.Parse(Convert.ToString(reader["ShellGifTcar"]), CultureInfo.InvariantCulture.NumberFormat),
                        Discount = float.Parse(Convert.ToString(reader["Discount"]), CultureInfo.InvariantCulture.NumberFormat),
                        Fuel = float.Parse(Convert.ToString(reader["Fuel"]), CultureInfo.InvariantCulture.NumberFormat),
                        GST = float.Parse(Convert.ToString(reader["GST"]), CultureInfo.InvariantCulture.NumberFormat),
                        DepositLevy = float.Parse(Convert.ToString(reader["DepositLevy"]), CultureInfo.InvariantCulture.NumberFormat),
                        TabboccoRemarks = Convert.ToString(reader["TabboccoRemarks"]),
                        TabbbaccoAttachemntid = Convert.ToString(reader["TabbbaccoAttachemntid"]),

                        #endregion
                        #region Fuel Details
                        Bronze = float.Parse(Convert.ToString(reader["Bronze"]), CultureInfo.InvariantCulture.NumberFormat),
                        Diesel = float.Parse(Convert.ToString(reader["Diesel"]), CultureInfo.InvariantCulture.NumberFormat),
                        Silver = float.Parse(Convert.ToString(reader["Silver"]), CultureInfo.InvariantCulture.NumberFormat),
                        VPowerPower = float.Parse(Convert.ToString(reader["VPowerPower"]), CultureInfo.InvariantCulture.NumberFormat),
                        VPowerDiesel = float.Parse(Convert.ToString(reader["VPowerDiesel"]), CultureInfo.InvariantCulture.NumberFormat),
                        AirMilesCash = float.Parse(Convert.ToString(reader["AirMilesCash"]), CultureInfo.InvariantCulture.NumberFormat),
                        LottoWinnerGiftCard = float.Parse(Convert.ToString(reader["LottoWinner"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashForBankDeposit = float.Parse(Convert.ToString(reader["CashForBankDeposit"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashDriveAway = float.Parse(Convert.ToString(reader["CashDriveAway"]), CultureInfo.InvariantCulture.NumberFormat),
                        FuelAttachmentID = Convert.ToString(reader["FuelAttachmentID"]),
                        FuelRemarks = Convert.ToString(reader["FuelRemarks"]),
                        #endregion
                        #region Cash
                        CashPayoutForStorePurchase = float.Parse(Convert.ToString(reader["CashPayoutForStorePurchase"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashLottoPayout = float.Parse(Convert.ToString(reader["CashLottoPayout"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashPaidForStorePur = float.Parse(Convert.ToString(reader["CashPaidForStorePur"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashPaidToThirdParties = float.Parse(Convert.ToString(reader["CashPaidToThirdParties"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashPaidToMGT = float.Parse(Convert.ToString(reader["CashPaidToMGT"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashShort = float.Parse(Convert.ToString(reader["CashShort"]), CultureInfo.InvariantCulture.NumberFormat),
                        GiftCertificate = float.Parse(Convert.ToString(reader["GiftCertificate"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashReceived = float.Parse(Convert.ToString(reader["CashReceived"]), CultureInfo.InvariantCulture.NumberFormat),
                        CashRemarks = Convert.ToString(reader["CashRemarks"]),
                        CashAttachmentID = Convert.ToString(reader["CashAttachmentID"]),
                        CashDifference = float.Parse(Convert.ToString(reader["CashDifference"]), CultureInfo.InvariantCulture.NumberFormat),
                        EnabledActualBankDeposit = float.Parse(Convert.ToString(reader["EnabledActualBankDeposit"]), CultureInfo.InvariantCulture.NumberFormat),
                        DebitCredit= float.Parse(Convert.ToString(reader["DebitCredit"]), CultureInfo.InvariantCulture.NumberFormat),
                        #endregion
                        #region Settlement
                        RadiantDebit = float.Parse(Convert.ToString(reader["RadiantDebit"]), CultureInfo.InvariantCulture.NumberFormat),
                        RadiantCredit = float.Parse(Convert.ToString(reader["RadiantCredit"]), CultureInfo.InvariantCulture.NumberFormat),
                        ShellDebit = float.Parse(Convert.ToString(reader["ShellDebit"]), CultureInfo.InvariantCulture.NumberFormat),
                        ShellCredit = float.Parse(Convert.ToString(reader["ShellCredit"]), CultureInfo.InvariantCulture.NumberFormat),
                        DailyStoreRentincludingGs = float.Parse(Convert.ToString(reader["DailyStoreRentincludingGs"]), CultureInfo.InvariantCulture.NumberFormat),
                        ActualCommission = float.Parse(Convert.ToString(reader["ActualCommission"]), CultureInfo.InvariantCulture.NumberFormat),
                        CardsReimbursements = float.Parse(Convert.ToString(reader["CardsReimbursements"]), CultureInfo.InvariantCulture.NumberFormat),
                        ActualSettlement = float.Parse(Convert.ToString(reader["ActualSettlement"]), CultureInfo.InvariantCulture.NumberFormat),
                        FairShare = float.Parse(Convert.ToString(reader["FairShare"]), CultureInfo.InvariantCulture.NumberFormat),
                        AttachmentIDShell = Convert.ToString(reader["AttachmentIDShell"]),
                        ShellRemarks = Convert.ToString(reader["ShellRemarks"])
                        #endregion

                    });
                }
                return transactionList;
            }
        }
        public int AddEditFuelTransaction(FuelTransactionEntities obj)
        {
            if (obj != null)
            {

                GenricRepository gp = new GenricRepository();
                try
                {
                    SqlParameter[] arParms = new SqlParameter[55];
                    arParms[0] = new SqlParameter("@type", "2");
                    arParms[1] = new SqlParameter("@TransactionDate", obj.TransactionDate);
                    arParms[2] = new SqlParameter("@C_StoreSale", obj.C_StoreSale);
                    arParms[3] = new SqlParameter("@C_AttachmentID", obj.C_AttachmentID);
                    arParms[4] = new SqlParameter("@C_Store_Remakrs", obj.C_Store_Remakrs);

                    arParms[5] = new SqlParameter("@Tabbacco", obj.Tabbacco);
                    arParms[6] = new SqlParameter("@OnlineLotto", obj.OnlineLotto);
                    arParms[7] = new SqlParameter("@ScratchLotto", obj.ScratchLotto);
                    arParms[8] = new SqlParameter("@PhoneCard", obj.PhoneCard);
                    arParms[9] = new SqlParameter("@GiftCard", obj.GiftCard);
                    arParms[10] = new SqlParameter("@ShellGifTcar", obj.ShellGifTcar);
                    arParms[11] = new SqlParameter("@Discount", obj.Discount);
                    arParms[12] = new SqlParameter("@Fuel", obj.Fuel);
                    arParms[13] = new SqlParameter("@GST", obj.GST);
                    arParms[14] = new SqlParameter("@DepositLevy", obj.DepositLevy);
                    arParms[15] = new SqlParameter("@TabboccoRemarks", obj.TabboccoRemarks);
                    arParms[16] = new SqlParameter("@TabbbaccoAttachemntid", obj.TabbbaccoAttachemntid);

                    arParms[17] = new SqlParameter("@Bronze", obj.Bronze);
                    arParms[18] = new SqlParameter("@Diesel", obj.Diesel);
                    arParms[19] = new SqlParameter("@Silver", obj.Silver);
                    arParms[20] = new SqlParameter("@VPowerPower", obj.VPowerPower);
                    arParms[21] = new SqlParameter("@VPowerDiesel", obj.VPowerDiesel);
                    arParms[22] = new SqlParameter("@AirMilesCash", obj.AirMilesCash);
                    arParms[23] = new SqlParameter("@Lottowinner", obj.LottoWinnerGiftCard);
                    arParms[24] = new SqlParameter("@CashForBankDeposit", obj.CashForBankDeposit);
                    arParms[25] = new SqlParameter("@CashDriveAway", obj.CashDriveAway);
                    arParms[26] = new SqlParameter("@FuelAttachmentID", obj.FuelAttachmentID);
                    arParms[27] = new SqlParameter("@FuelRemarks", obj.FuelRemarks);


                    arParms[28] = new SqlParameter("@CashPayoutForStorePurchase", obj.CashPayoutForStorePurchase);
                    arParms[29] = new SqlParameter("@CashLottoPayout", obj.CashLottoPayout);
                    arParms[30] = new SqlParameter("@CashPaidForStorePur", obj.CashPaidForStorePur);
                    arParms[31] = new SqlParameter("@CashPaidToThirdParties", obj.CashPaidToThirdParties);
                    arParms[32] = new SqlParameter("@CashPaidToMGT", obj.CashPaidToMGT);
                    arParms[33] = new SqlParameter("@CashShort", obj.CashShort);
                    arParms[34] = new SqlParameter("@GiftCertificate", obj.GiftCertificate);
                    arParms[35] = new SqlParameter("@CashReceived", obj.CashReceived);
                    arParms[36] = new SqlParameter("@CashRemarks", obj.CashRemarks);
                    arParms[37] = new SqlParameter("@CashAttachmentID", obj.CashAttachmentID);

                    arParms[38] = new SqlParameter("@RadiantDebit", obj.RadiantDebit);
                    arParms[39] = new SqlParameter("@RadiantCredit", obj.RadiantCredit);
                    arParms[40] = new SqlParameter("@ShellDebit", obj.ShellDebit);
                    arParms[41] = new SqlParameter("@ShellCredit", obj.ShellCredit);
                    arParms[42] = new SqlParameter("@DailyStoreRentincludingGs", obj.DailyStoreRentincludingGs);
                    arParms[43] = new SqlParameter("@ActualCommission", obj.ActualCommission);
                    arParms[44] = new SqlParameter("@CardsReimbursements", obj.CardsReimbursements);
                    arParms[45] = new SqlParameter("@ActualSettlement", obj.ActualSettlement);
                    arParms[46] = new SqlParameter("@FairShare", obj.FairShare);
                    arParms[47] = new SqlParameter("@AttachmentIDShell", obj.AttachmentIDShell);
                    arParms[48] = new SqlParameter("@ShellRemarks", obj.ShellRemarks);

                    arParms[49] = new SqlParameter("@Remarks", obj.Remarks);
                    arParms[50] = new SqlParameter("@UserId", BA_Commman._userId);
                    arParms[51] = new SqlParameter("@AttachmentID", obj.AttachmentID);
                    arParms[52] = new SqlParameter("@CashDifference", obj.CashDifference);
                    arParms[53] = new SqlParameter("@EnabledActualBankDeposit", obj.EnabledActualBankDeposit);
                    arParms[54] = new SqlParameter("@DebitCredit", obj.DebitCredit);

                    gp.ExecuteNonQuery("Proc_FuelTransaction", arParms);
                    return 1;
                }

                catch (Exception ex)
                {

                    BA_Commman.SendErrorToText(ex);
                    return 0;
                }
            }
            else
            {// Edit code
                return 0;
            }
        }
        public List<TicketDetails> GetTicketList(int type)
        {
            List<TicketDetails> TicketList = new List<TicketDetails>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@type", type);
            GenricRepository gp = new GenricRepository();
            if (type == 3)
            {
                using (SqlDataReader reader = gp.ExecuteDatareader("Proc_FuelTransaction", arParms))
                {
                    while (reader.Read())
                    {
                        TicketList.Add(new TicketDetails
                        {
                            Ticketid = Convert.ToInt32(reader["TicketId"]),
                            TicketStatus = Convert.ToString(reader["TicketStatus"]),
                        });
                    }
                }
            }
            else
            {
                using (SqlDataReader reader = gp.ExecuteDatareader("Proc_FuelTransaction", arParms))
                {
                    while (reader.Read())
                    {
                        TicketList.Add(new TicketDetails
                        {
                            Ticketid = Convert.ToInt32(reader["TicketId"]),
                            TicketStatus = Convert.ToString(reader["TicketStatus"]),
                            TransactionId = Convert.ToInt32(reader["TransactionId"]),
                            TransactionDate = Convert.ToDateTime(reader["TransactionDate"]),

                        });

                    }
                }
            }
            return TicketList;
        }
        public int ManageTicket(int TicketStatusid, string dt)
        {
            try
            {
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@type", 4);
                arParms[1] = new SqlParameter("@TransactionDate", dt);
                arParms[2] = new SqlParameter("@ticketStatusId", TicketStatusid);


                GenricRepository gp = new GenricRepository();
                gp.ExecuteNonQuery("Proc_FuelTransaction", arParms);
                return 1;

            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }



        public DataSet GetFuelTransactionList()
        {
            SqlParameter[] arParms;

            arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@type", 1);


            GenricRepository gp = new GenricRepository();
            return gp.ExecuteDataset("Proc_FuelTransaction", arParms);
        }
        public DataSet FuelTransactionReport(DateTime FrmDate, DateTime todate, int type)
        {
            SqlParameter[] arParms;

            arParms = new SqlParameter[3];
            arParms[0] = new SqlParameter("@type", type);
            arParms[1] = new SqlParameter("@FromTransactionDate", FrmDate);
            arParms[2] = new SqlParameter("@ToTransactionDate", todate);
            GenricRepository gp = new GenricRepository();
            return gp.ExecuteDataset("Proc_FuelTransactionReport", arParms);
        }

    }
}




