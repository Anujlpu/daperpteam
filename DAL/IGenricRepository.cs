﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
namespace DAL
{

    public interface IGenricRepository
    {

        int ExecuteNonQuery(string ProcName, params SqlParameter[] param);
        string ExecuteScalar(string ProcName, params SqlParameter[] param);
        System.Data.DataSet ExecuteDataset(string ProcName, params SqlParameter[] param);
        SqlDataReader ExecuteDatareader(string ProcName, params SqlParameter[] param);

    }
}
