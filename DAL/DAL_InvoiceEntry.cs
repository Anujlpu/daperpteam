﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Utility;
namespace DAL
{
    public class DAL_InvoiceEntry
    {
        public DataSet getInvoiceDetails(int type, int InvoiceId)
        {
           
            SqlParameter[] arParms = new SqlParameter[2];
            arParms[0] = new SqlParameter("@Type", type);
            arParms[1] = new SqlParameter("@InvoiceId", InvoiceId);
            GenricRepository gp = new GenricRepository();
            return gp.ExecuteDataset("Proc_InvoiceEntries", arParms);

        }
        public int InsertInvoice(DateTime InvDate, int VendorId, string InvoiceNo
, int PNLType, int PaymentType, string TransacctionType, double GST, double TotalAmount, double Cash
, double Payout, double Total, string Remarks, int CreatedBy, bool IsActive, string UploadPath)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[16];
                arParms[0] = new SqlParameter("@Type", 1);
                arParms[1] = new SqlParameter("@InvDate", InvDate);
                arParms[2] = new SqlParameter("@VendorId", VendorId);
                arParms[3] = new SqlParameter("@InvoiceNo", InvoiceNo);
                arParms[4] = new SqlParameter("@PNLType", PNLType);
                arParms[5] = new SqlParameter("@PaymentType", PaymentType);
                arParms[6] = new SqlParameter("@TransactionType", TransacctionType);
                arParms[7] = new SqlParameter("@GST", GST);
                arParms[8] = new SqlParameter("@TotalAmount", TotalAmount);
                arParms[9] = new SqlParameter("@Cash", Cash);
                arParms[10] = new SqlParameter("@Payout", Payout);
                arParms[11] = new SqlParameter("@Total", Total);
                arParms[12] = new SqlParameter("@Remarks", Remarks);
                arParms[13] = new SqlParameter("@CreatedBy", CreatedBy);
                arParms[14] = new SqlParameter("@IsActive", IsActive);
                arParms[15] = new SqlParameter("@UploadPath", UploadPath);
                gp.ExecuteNonQuery("Proc_InvoiceEntries", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int UpdateInvoice(int InvoiceId, DateTime InvDate, int VendorId, string InvoiceNo
, int PNLType, int PaymentType, string TransacctionType, double GST, double TotalAmount, double Cash
, double Payout, double Total, string Remarks, int CreatedBy, bool IsActive, string UploadPath)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[17];

                arParms[0] = new SqlParameter("@Type", 2);
                arParms[1] = new SqlParameter("@InvoiceId", InvoiceId);
                arParms[2] = new SqlParameter("@InvDate", InvDate);
                arParms[3] = new SqlParameter("@VendorId", VendorId);
                arParms[4] = new SqlParameter("@InvoiceNo", InvoiceNo);
                arParms[5] = new SqlParameter("@PNLType", PNLType);
                arParms[6] = new SqlParameter("@PaymentType", PaymentType);
                arParms[7] = new SqlParameter("@TransactionType", TransacctionType);
                arParms[8] = new SqlParameter("@GST", GST);
                arParms[9] = new SqlParameter("@TotalAmount", TotalAmount);
                arParms[10] = new SqlParameter("@Cash", Cash);
                arParms[11] = new SqlParameter("@Payout", Payout);
                arParms[12] = new SqlParameter("@Total", Total);
                arParms[13] = new SqlParameter("@Remarks", Remarks);
                arParms[14] = new SqlParameter("@CreatedBy", CreatedBy);
                arParms[15] = new SqlParameter("@IsActive", IsActive);
                arParms[16] = new SqlParameter("@UploadPath", UploadPath);
                gp.ExecuteNonQuery("Proc_InvoiceEntries", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeleteInvoice(int InvoiceId)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@Type", 3);
                arParms[0] = new SqlParameter("@InvoiceId", InvoiceId);
                gp.ExecuteNonQuery("Proc_InvoiceEntries", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }

    }
}
