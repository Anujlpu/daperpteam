﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{

    public class EmployeeEntities
    {
        public EmployeeEntities()
        {
        }
        public List<EmployeeEntity> GetEmployeeList()
        {
            List<EmployeeEntity> Locationlist = null;
            return Locationlist;
        }
    }
    public class EmployeeEntity
    {
        public string EmployeeName { get; set; }
        public DateTime DOJ { get; set; }
        public DateTime DOB { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }
        public string Address { get; set; }
        public string P_EmailId { get; set; }
        public string BasicSalary { get; set; }
        public string BasicHourlySalary { get; set; }
        public string ActualSalary { get; set; }
        public string ActualHourlySalary { get; set; }
        public string LivingAllowance { get; set; }
        public string TravelAllowance { get; set; }
        public DateTime VisaIssueDate { get; set; }
        public DateTime VisaExpireDate { get; set; }
        public string VisaDescription { get; set; }

        public int EmployeeID { get; set; }
        public string UserName { get; set; }
        public string Password
        { get; set; }
        public string Remarks { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string Department { get; set; }
        public int LocationId { get; set; }
        public string EmployeeStatus { get; set; }
        public string HiredLocation { get; set; }
        public string CurrentWorkingLocation { get; set; }
        public string Employementtype { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string EmegencyContactAddress { get; set; }
        public DateTime? HourlyRateEffFrom { get; set; }
        public DateTime? HourlyRateEffTo { get; set; }
        public float? PayRollRate { get; set; }
        public DateTime? PayRollEffFrom { get; set; }
        public DateTime? PayRollEffTo { get; set; }
    }

    public class DepartEntities
    {
        public DepartEntities()
        {
        }
        public List<DepartEntity> GetDepartList()
        {
            List<DepartEntity> Departlist = null;
            return Departlist;
        }
    }
    public class DepartEntity
    {
        public string DepartName { get; set; }
        public int DepartmentId { get; set; }
    }

    public class EmpStatusEntities
    {
        public EmpStatusEntities()
        {
        }
        public List<EmpStatusEntity> GetEmployeeStatusList()
        {
            List<EmpStatusEntity> EmployeestatusList = null;
            return EmployeestatusList;
        }
    }
    public class EmpStatusEntity
    {

        public string EmployeeStatus { get; set; }
        public int EmployeeStatusId { get; set; }
        public int IsVisaDetailsRequired { get; set; }

    }
}
