﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class StoreEntity
    {
        public int StoreId { get; set; }
        public string OutletNumber { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public int StoreLocationId { get; set; }
        public string StoreLocation { get; set; }
        public int Userid { get; set; }
        public string PhoneNo { get; set; }
        public string Emailid { get; set; }
        public string FaxNo { get; set; }
        public string StoreBrand { get; set; }
        public string OutletNo { get; set; }
        public int EmpCount { get; set; }
        public int ContractorCount { get; set; }
        public string ApplicationStatus { get; set; }
        public DateTime AppDate { get; set; }

    }
    public class StoreEntities
    {
        public StoreEntities()
        {
        }
        public List<StoreEntity> GetStoreList()
        {
            List<StoreEntity> Storelist = null;
            return Storelist;
        }
    }
}
