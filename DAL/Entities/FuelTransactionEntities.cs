﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.ComponentModel;
namespace DAL
{
    public class FuelTransactionEntities
    {
        public int GasTransactionID;
        //C-Store Details
        public DateTime TransactionDate;
        public float C_StoreSale;
        public string C_AttachmentID;
        public string C_Store_Remakrs;

        //Tavacoo Details
        public float Tabbacco;
        public float OnlineLotto;
        public float ScratchLotto;
        public float PhoneCard;
        public float GiftCard;
        public float ShellGifTcar;
        public float Discount;
        public float Fuel;
        public float GST;
        public float DepositLevy;
        public string TabboccoRemarks;
        public string TabbbaccoAttachemntid;

        // fuel Details
        public float Bronze;
        public float Diesel;
        public float Silver;
        public float VPowerPower;
        public float VPowerDiesel;
        public float AirMilesCash;
        public float LottoWinnerGiftCard;
        public float CashForBankDeposit;
        public float CashDriveAway;
        public string FuelAttachmentID;
        public string FuelRemarks;
        //Cash Details
        public float CashPayoutForStorePurchase;
        public float CashLottoPayout;
        public float CashPaidForStorePur;
        public float CashPaidToThirdParties;
        public float CashPaidToMGT;
        public float CashShort;
        public float GiftCertificate;
        public float CashReceived;
        public string CashRemarks;
        public string CashAttachmentID;
        public float CashDifference;
        public float EnabledActualBankDeposit;
        public float DebitCredit;

        // Settlement details
        public float RadiantDebit;
        public float RadiantCredit;
        public float ShellDebit;
        public float ShellCredit;
        public float DailyStoreRentincludingGs;
        public float ActualCommission;
        public float CardsReimbursements;
        public float ActualSettlement;
        public float CP;
        public float AB;
        public string Remarks;
        public DateTime CreatedDat;
        public int CreatedBy;
        public DateTime ModifiedDate;
        public int ModifiedBy;
        public string AttachmentID;
        public float FairShare;
        public string AttachmentIDShell;
        public string ShellRemarks;
    }
    public class VFuelTransactionEntities
    {
        public int GasTransactionID;
        //C-Store Details
        public DateTime TransactionDate;
        public float C_StoreSale;
        //Tavacoo Details
        public float Tabbacco;
        public float OnlineLotto;
        public float ScratchLotto;
        public float PhoneCard;
        public float GiftCard;
        public float ShellGifTcar;
        public float Discount;
        public float Fuel;
        public float GST;
        public float DepositLevy;
        // fuel Details
        //public float Bronze;
        //public float Diesel;
        //public float Silver;
        //public float VPowerPower;
        //public float VPowerDiesel;
        //public float AirMilesCash;
        //public float LottoWinnerGiftCard;
        //public float CashForBankDeposit;
        //public float CashDriveAway;
        // //Cash Details
        //public float CashPayoutForStorePurchase;
        //public float CashLottoPayout;
        //public float CashPaidForStorePur;
        //public float CashPaidToThirdParties;
        //public float CashPaidToMGT;
        //public float CashShort;
        //public float GiftCertificate;
        //public float CashReceived;
        //// Settlement details
        //public float RadiantDebit;
        //public float RadiantCredit;
        //public float ShellDebit;
        //public float ShellCredit;
    }

    public class DocsEntity
    {
        public int DocId { get; set; }
        [DisplayName("Transaction Date")]
        public DateTime TransactionDate { get; set; }
        [DisplayName("Document Type")]
        public string DocType { get; set; }
        [DisplayName("File Path")]
        public string FilePath { get; set; }
        [DisplayName("Document Name")]
        public string DocName { get; set; }
        [DisplayName("Created By")]
        public string CreatedBy { get; set; }
        [DisplayName("Created Date")]
        public DateTime? CreatedDate { get; set; }
        public int UserId { get; set; }
    }
    public class DocumentList
    {
        public long? DocId { get; set; }
        public string DocsIdentity { get; set; }
        public DateTime? TransactionDate { get; set; }
    }

    public class TicketDetails
    {
        public int Ticketid { get; set; }
        public DateTime TransactionDate { get; set; }
        public int Ticketstatusid { get; set; }
        public string TicketStatus { get; set; }
        public int TransactionId { get; set; }


    }
}
