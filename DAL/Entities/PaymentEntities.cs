﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class PaidPaymentEntities
    {
        public PaidPaymentEntities()
        {
        }
        public List<PaidPaymentEntity> GetPaidPaymentList()
        {
            List<PaidPaymentEntity> PaidPaymentlist = null;
            return PaidPaymentlist;
        }
    }
    public class PaidPaymentEntity
    {
        public int PaymentId { get; set; }
        public DateTime PaymentDate { get; set; }
        public string InvoiceNo { get; set; }
        public double Amount { get; set; }
        public double CashAmount { get; set; }
        public double CashPayoutAmount { get; set; }
        public string Remarks { get; set; }
        public string UploadedDocPath { get; set; }
        public int UserId { get; set; }
        public int TransactionType { get; set; }
        public int PaymentToId { get; set; }
        public int PaymentType { get; set; }
    }
}
