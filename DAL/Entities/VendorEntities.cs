﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DAL
{
    public class VendorEntities
    {
        public VendorEntities()
        {
        }
        public List<VenderEntity> GetVendorList()
        {
            List<VenderEntity> Vendorlist = null;
            return Vendorlist;
        }
    }
    public class VenderEntity
    {
        public VenderEntity() { }
        public int VendorId { get; set; }
        [DisplayName("Vendor Code")]
        public string VendorCode { get; set; }
        [DisplayName("Vendor Name")]
        public string VendorName { get; set; }
        [DisplayName("Contact No")]
        public string ContactNo { get; set; }
        [DisplayName("Email Id")]
        public string EmailId { get; set; }
        [DisplayName("Online User Name")]
        public string OnlineUserName { get; set; }
        public string StoreEmailPassword { get; set; }
        public string AccountNo { get; set; }
        public string Address { get; set; }
        [DisplayName("SR Name")]
        public string SEName { get; set; }
        [DisplayName("SR Email Id")]
        public string SEEmailId { get; set; }
        [DisplayName("SR Mobile No")]
        public string SEMobileNo { get; set; }
        [DisplayName("Vendor Name")]
        public string DAPEmailId { get; set; }
        [DisplayName("Payment Type")]
        public string PaymentType { get; set; }
        public string Remarks { get; set; }
        [DisplayName("Created By")]
        public int CreatedBy { get; set; }
        [DisplayName("Modified Date")]
        public DateTime ModifiedDate { get; set; }
        [DisplayName("Modified By")]
        public int ModifiedBy { get; set; }
        [DisplayName("Online Portal")]
        public string OnlinePortal { get; set; }
        [DisplayName("Online Portal User Name")]
        public string OnlinePortalUserName { get; set; }
        public string OnlinePortalPassword { get; set; }
        [DisplayName("Vendor Type")]
        public string VendorType { get; set; }
        [DisplayName("Application Status")]
        public string ApplicationStatus { get; set; }
        [DisplayName("Application Date")]
        public DateTime? ApplicationDate { get; set; }

    }
    public class PaymentEntity
    {
        public PaymentEntity()
        {
        }
        public int PaymentTypeId { get; set; }
        public string PaymentTypeCode { get; set; }

    }
    public class PaymentEntities
    {
        public PaymentEntities()
        {
        }
        public List<PaymentEntity> GetPaymenttypeList()
        {
            List<PaymentEntity> Paymentist = null;
            return Paymentist;
        }
    }
}
