﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Utility;
namespace DAL
{
    public class DAL_Login
    {
        UserEntity user = null;
        public DAL_Login(UserEntity _user)
        {
            user = _user;
        }
        public DAL_Login()
        {
        }
        public List<UserEntity> Login()
        {
            List<UserEntity> Userlist = new List<UserEntity>();
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@UserCode", user.UserCode);
                arParms[1] = new SqlParameter("@Password", user.Password);
                arParms[2] = new SqlParameter("@type", 5);

                UserEntity userEntity = new UserEntity();
                using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageUser", arParms))
                {
                    while (reader.Read())
                    {

                        userEntity.UserCode = reader.GetString(0);
                        userEntity.UserName = reader.GetString(1);
                        userEntity.ProfileCode = reader.GetString(2);
                        userEntity.UserId = reader.GetInt32(3);


                        Userlist.Add(userEntity);
                    }
                }
                return Userlist;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return Userlist;
            }

        }
        public int DeleteUser()
        {
            GenricRepository gp = new GenricRepository();
            try
            {

                SqlParameter[] arParms = new SqlParameter[2];
                arParms[0] = new SqlParameter("@type", 4);
                arParms[1] = new SqlParameter("@UserCode", user.UserCode);
                gp.ExecuteNonQuery("Proc_ManageUser", arParms);
                return 1;

            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }

        }
        public int InsertUser()
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[7];
                arParms[0] = new SqlParameter("@type", 1);
                arParms[1] = new SqlParameter("@UserCode", user.UserCode);
                arParms[2] = new SqlParameter("@UserName", user.UserName);
                arParms[3] = new SqlParameter("@CreatedBy", user.UserId);
                arParms[4] = new SqlParameter("@ModifiedBy", user.UserId);
                arParms[5] = new SqlParameter("@Password", user.Password);
                arParms[6] = new SqlParameter("@ProfileId", user.Profileid);
                gp.ExecuteNonQuery("Proc_ManageUser", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int UpdattUser()
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[5];
                arParms[0] = new SqlParameter("@type", 2);
                arParms[1] = new SqlParameter("@UserCode", user.UserCode);
                arParms[2] = new SqlParameter("@UserName", user.UserName);
                arParms[3] = new SqlParameter("@ModifiedBy", user.UserId);
                arParms[4] = new SqlParameter("@ProfileId", user.Profileid);
                gp.ExecuteNonQuery("Proc_ManageUser", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public List<UserEntity> GetUserlist()
        {
            List<UserEntity> Userlist = new List<UserEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 3);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageUser", arParms))
            {
                while (reader.Read())
                {
                    Userlist.Add(new UserEntity
                    {
                        UserCode = reader.GetString(0),
                        UserName = reader.GetString(1),
                        ProfileCode = reader.GetString(2),
                        UserId = reader.GetInt32(4),
                        Profileid = reader.GetInt32(5),
                        IsActive = reader.GetString(3)
                    });
                }
            }
            return Userlist;

        }
        public int ChangePwd()
        {
            try
            {
                GenricRepository gp = new GenricRepository();
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@type", 7);
                arParms[1] = new SqlParameter("@UserCode", user.UserCode);
                arParms[2] = new SqlParameter("@Password", user.Password);
                gp.ExecuteNonQuery("Proc_ManageUser", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }




        }
        public List<ProfileEntity> GetProfilelist()
        {
            List<ProfileEntity> Profilelist = new List<ProfileEntity>();
            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 6);
            GenricRepository gp = new GenricRepository();
            using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageUser", arParms))
            {
                while (reader.Read())
                {
                    Profilelist.Add(new ProfileEntity
                    {
                        ProfileId = reader.GetInt32(0),
                        ProfileCode = reader.GetString(1),

                    });
                }
            }
            return Profilelist;

        }
    }
}
