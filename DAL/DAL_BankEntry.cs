﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Utility;
namespace DAL
{
    public class DAL_BankEntry
    {
        public DataSet getBankEntriesDetails()
        {

            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 4);
            GenricRepository gp = new GenricRepository();
            return gp.ExecuteDataset("Proc_BankEntries", arParms);

        }
        public int InsertBankentries(DateTime InvDate, string TransacctionType, double TotalAmount,
            string Remarks, int CreatedBy, bool IsActive, string UploadPath, string BankName, int vendorid, int PNLID)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[11];
                arParms[0] = new SqlParameter("@Type", 1);
                arParms[1] = new SqlParameter("@StateMentDate", InvDate);
                arParms[2] = new SqlParameter("@TransactionType", TransacctionType);
                arParms[3] = new SqlParameter("@TotalAmount", TotalAmount);
                arParms[4] = new SqlParameter("@Remarks", Remarks);
                arParms[5] = new SqlParameter("@CreatedBy", CreatedBy);
                arParms[6] = new SqlParameter("@IsActive", IsActive);
                arParms[7] = new SqlParameter("@UploadPath", UploadPath);
                arParms[8] = new SqlParameter("@BankName", BankName);
                arParms[9] = new SqlParameter("@PNLID", PNLID);
                arParms[10] = new SqlParameter("@VendorID", vendorid);


                gp.ExecuteNonQuery("Proc_BankEntries", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int UpdateBankEntries(int BankEntriesId, DateTime InvDate, string TransacctionType, double TotalAmount,
            string Remarks, int CreatedBy, bool IsActive, string UploadPath, string BankName , int PNLID, int vendorid)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[12];

                arParms[0] = new SqlParameter("@Type", 2);
                arParms[1] = new SqlParameter("@BankEntriesId", BankEntriesId);
                arParms[2] = new SqlParameter("@StateMentDate", InvDate);
                arParms[3] = new SqlParameter("@TransactionType", TransacctionType);
                arParms[4] = new SqlParameter("@TotalAmount", TotalAmount);
                arParms[5] = new SqlParameter("@Remarks", Remarks);
                arParms[6] = new SqlParameter("@CreatedBy", CreatedBy);
                arParms[7] = new SqlParameter("@IsActive", IsActive);
                arParms[8] = new SqlParameter("@UploadPath", UploadPath);
                arParms[9] = new SqlParameter("@BankName", BankName);
                arParms[10] = new SqlParameter("@PNLID", PNLID);
                arParms[11] = new SqlParameter("@VendorID", vendorid);
                gp.ExecuteNonQuery("Proc_BankEntries", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeleteEntries(int BankEntriesId)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[2];

                arParms[0] = new SqlParameter("@Type", 3);
                arParms[1] = new SqlParameter("@BankEntriesId", BankEntriesId);
                arParms[8] = new SqlParameter("@IsActive", 0);

                gp.ExecuteNonQuery("Proc_BankEntries", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }

    }
}
