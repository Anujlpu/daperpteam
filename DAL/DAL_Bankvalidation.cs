﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Utility;
namespace DAL
{
    public class DAL_BankValidation
    {
        public DataSet getBankEntriesDetails()
        {

            SqlParameter[] arParms = new SqlParameter[1];
            arParms[0] = new SqlParameter("@Type", 4);
            GenricRepository gp = new GenricRepository();
            return gp.ExecuteDataset("Proc_BankValidation", arParms);

        }
        public int InsertBankValentries(DateTime ValidationDate, int BankEntry, int invoiceID, string Remarks, int CreatedBy, bool IsActive, string UploadPath)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[7];
                arParms[0] = new SqlParameter("@Type", 1);
                arParms[1] = new SqlParameter("@BankEntriesId", BankEntry);
                arParms[2] = new SqlParameter("@ValidationDate", ValidationDate);
                arParms[3] = new SqlParameter("@invociesId", invoiceID);
                arParms[4] = new SqlParameter("@Remarks", Remarks);
                arParms[5] = new SqlParameter("@CreatedBy", CreatedBy);
                arParms[6] = new SqlParameter("@IsActive", IsActive);
                gp.ExecuteNonQuery("Proc_BankValidation", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int UpdateBankEntries(int BankvalEntriesId, DateTime ValidationDate, int BankEntry, int invoiceID, string Remarks, int CreatedBy, bool IsActive, string UploadPath)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[8];
                arParms[0] = new SqlParameter("@Type", 2);
                arParms[1] = new SqlParameter("@BankValidationId", BankvalEntriesId);
                arParms[2] = new SqlParameter("@BankEntriesId", BankEntry);
                arParms[3] = new SqlParameter("@ValidationDate", ValidationDate);
                arParms[4] = new SqlParameter("@invociesId", invoiceID);
                arParms[5] = new SqlParameter("@Remarks", Remarks);
                arParms[6] = new SqlParameter("@CreatedBy", CreatedBy);
                arParms[7] = new SqlParameter("@IsActive", IsActive);
                gp.ExecuteNonQuery("Proc_BankValidation", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeleteEntries(int BankvalEntriesId)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[2];

                arParms[0] = new SqlParameter("@Type", 3);
                arParms[1] = new SqlParameter("@BankValidationId", BankvalEntriesId);
                arParms[2] = new SqlParameter("@IsActive", 0);

                gp.ExecuteNonQuery("Proc_BankValidation", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public DataSet GetOpenInvoiceDetails(string TranType, string VendorId)
        {
            SqlParameter[] arParms = new SqlParameter[3];
            arParms[0] = new SqlParameter("@Type", 6);
            arParms[1] = new SqlParameter("@TranType", TranType);
            arParms[2] = new SqlParameter("@VendorCode", VendorId);
            GenricRepository gp = new GenricRepository();
            return gp.ExecuteDataset("Proc_BankValidation", arParms);
        }
        public DataSet GetVendor(string VendorId,string Transtype)
        {
            SqlParameter[] arParms = new SqlParameter[3];
            arParms[0] = new SqlParameter("@Type", 5);
            arParms[1] = new SqlParameter("@VendorCode", VendorId);
            arParms[2] = new SqlParameter("@TranType", Transtype);
            GenricRepository gp = new GenricRepository();
            return gp.ExecuteDataset("Proc_BankValidation", arParms);
        }
        public DataSet GetOpenBankDetails(string TranType)
        {
            SqlParameter[] arParms = new SqlParameter[2];
            arParms[0] = new SqlParameter("@Type", 7);
            arParms[1] = new SqlParameter("@TranType", TranType);
            GenricRepository gp = new GenricRepository();
            return gp.ExecuteDataset("Proc_BankValidation", arParms);
        }

    }
}
