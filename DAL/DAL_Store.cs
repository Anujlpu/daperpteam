﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Utility;

namespace DAL
{
    public class DAL_Store
    {
        public List<StoreEntity> GetStorelist()
        {
            List<StoreEntity> Storelist = new List<StoreEntity>();
            try
            {
                SqlParameter[] arParms = new SqlParameter[1];
                arParms[0] = new SqlParameter("@Type", 4);
                GenricRepository gp = new GenricRepository();
                using (SqlDataReader reader = gp.ExecuteDatareader("Proc_ManageStore", arParms))
                {
                    while (reader.Read())
                    {
                        Storelist.Add(new StoreEntity
                        {

                            StoreId = int.Parse(reader["StoreId"].ToString()),
                            OutletNumber = reader["StoreCode"].ToString(),
                            StoreName = reader["StoreName"].ToString(),
                            StoreAddress = reader["StoreAddress"].ToString(),
                            StoreLocationId = int.Parse(reader["StoreLocationId"].ToString()),
                            StoreLocation = reader["LocationCode"].ToString(),
                            PhoneNo = reader["PhoneNo"].ToString(),
                            Emailid = reader["Emailid"].ToString(),
                            FaxNo = reader["FaxNo"].ToString(),
                            StoreBrand = reader["StoreBrand"].ToString(),
                            OutletNo = reader["StoreAddress"].ToString(),
                            EmpCount = int.Parse(reader["EmpCount"].ToString()),
                            ContractorCount = int.Parse(reader["ContractorCount"].ToString()),
                            ApplicationStatus = reader["ApplicationStatus"].ToString(),
                            AppDate = Convert.ToDateTime(reader["ApplicationDate"].ToString()),

                        });
                    }
                }
                return Storelist;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);

            }
            return Storelist;

        }
        public int InsertStore(StoreEntity StoreEntity, int Type)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[16];
                arParms[0] = new SqlParameter("@Type", Type);
                arParms[1] = new SqlParameter("@StoreCode", StoreEntity.OutletNumber);
                arParms[2] = new SqlParameter("@StoreName", StoreEntity.StoreName);
                arParms[3] = new SqlParameter("@StoreAddress", StoreEntity.StoreAddress);
                arParms[4] = new SqlParameter("@StoreId", StoreEntity.StoreId);
                arParms[5] = new SqlParameter("@StoreLocationId", StoreEntity.StoreLocationId);
                arParms[6] = new SqlParameter("@UserId", StoreEntity.Userid);
                arParms[7] = new SqlParameter("@PhoneNo", StoreEntity.PhoneNo);
                arParms[8] = new SqlParameter("@Emailid", StoreEntity.Emailid);
                arParms[9] = new SqlParameter("@FaxNo", StoreEntity.FaxNo);
                arParms[10] = new SqlParameter("@StoreBrand", StoreEntity.StoreBrand);
                arParms[11] = new SqlParameter("@OutletNo", StoreEntity.OutletNo);
                arParms[12] = new SqlParameter("@EmpCount", StoreEntity.EmpCount);
                arParms[13] = new SqlParameter("@ContractorCount", StoreEntity.ContractorCount);
                arParms[14] = new SqlParameter("@ApplicationStatus", StoreEntity.ApplicationStatus);
                arParms[15] = new SqlParameter("@AppDate", StoreEntity.AppDate);
                gp.ExecuteNonQuery("Proc_ManageStore", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeleteStore(StoreEntity StoreEntity)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@Type", 3);
                arParms[1] = new SqlParameter("@StoreId", StoreEntity.StoreId);
                arParms[2] = new SqlParameter("@UserId", StoreEntity.Userid);
                gp.ExecuteNonQuery("Proc_ManageStore", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
    }
}
