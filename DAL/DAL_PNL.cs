﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Utility;

namespace DAL
{
    public class DAL_PNL
    {
        public List<PNLEntity> GetPNLlist()
        {
            List<PNLEntity> PNLlist = new List<PNLEntity>();
            try
            {
                SqlParameter[] arParms = new SqlParameter[1];
                arParms[0] = new SqlParameter("@Type", 4);
                GenricRepository gp = new GenricRepository();
                using (SqlDataReader reader = gp.ExecuteDatareader("Proc_PNLMaster", arParms))
                {
                    while (reader.Read())
                    {
                        PNLlist.Add(new PNLEntity
                        {

                            PNLID = int.Parse(reader.GetValue(0).ToString()),
                            PNLCode = reader.GetString(1),
                            PNLDescription = reader.GetString(2),
                            CreatedBy = 0

                        });
                    }
                }
                return PNLlist;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);

            }
            return PNLlist;

        }
        public int InsertPNL(PNLEntity PNLEntity, int Type)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[5];
                arParms[0] = new SqlParameter("@Type", Type);
                arParms[1] = new SqlParameter("@PNLCode", PNLEntity.PNLCode);
                arParms[2] = new SqlParameter("@PNLDescription", PNLEntity.PNLDescription);
                arParms[3] = new SqlParameter("@PNLId", PNLEntity.PNLID);
                arParms[4] = new SqlParameter("@UserId", PNLEntity.CreatedBy);
                gp.ExecuteNonQuery("Proc_PNLMaster", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
        public int DeletePNL(PNLEntity PNLEntity)
        {
            GenricRepository gp = new GenricRepository();
            try
            {
                SqlParameter[] arParms = new SqlParameter[3];
                arParms[0] = new SqlParameter("@Type", 3);
                arParms[1] = new SqlParameter("@PNLId", PNLEntity.PNLID);
                arParms[2] = new SqlParameter("@UserId", PNLEntity.CreatedBy);
                gp.ExecuteNonQuery("[Proc_PNLMaster]", arParms);
                return 1;
            }
            catch (Exception ex)
            {
                BA_Commman.SendErrorToText(ex);
                return 0;
            }
        }
    }
}
