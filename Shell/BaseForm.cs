﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;


namespace Shell
{
    public class BaseForm : Form
    {
        public BaseForm()
        {
            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) return;
            this.Load += BaseForm_Load;
            this.FormClosed += BaseForm_FormClosed;
        }
        private IEnumerable<Control> GetAllControls(Control control)
        {
            var controls = control.Controls.Cast<Control>();
            return controls.SelectMany(ctrl => GetAllControls(ctrl)).Concat(controls);
        }
        void BaseForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Log(string.Format("{0} Closed", this.Name));
        }
        void BaseForm_Load(object sender, EventArgs e)
        {
            Log(string.Format("{0} Opened", this.Name));
            GetAllControls(this).OfType<Button>().ToList()
                .ForEach(x => x.Click += ButtonClick);
            //Log(string.Format("{0} Leave", this.Name));
            //GetAllControls(this).OfType<TextBox>().ToList()
            //  .ForEach(x => x.Leave += ControlLeave);
            GetAllControls(this).OfType<ToolStripMenuItem>().ToList()
             .ForEach(x => x.Click += MenuClick);
        }
        void ButtonClick(object sender, EventArgs e)
        {
            var button = sender as Button;
            if (button != null) Log(string.Format("{0} {1} Clicked",this.Name, button.Name));
        }
        void ControlLeave(object sender, EventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox != null) Log(string.Format("{0} {1} Leaved",this.Name, textbox.Name));
        }
        void MenuClick(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;
            if (menuItem != null) Log(string.Format("{0} {1} Menu Clicked",this.Name, menuItem.Name));
        }
        public void Log(string text)
        {
            try
            {
                ILogger log = new Logger();
                log.LogActivity(text);
            }
            catch (Exception ex)
            {
                ILogger log = new Logger();
                log.LogActivity(ex.Message);
            }

        }
    }
}
