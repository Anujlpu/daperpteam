﻿namespace Shell.Report
{
    partial class TobaccoInvReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TobaccoProductCmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ToDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.Exportbutton = new System.Windows.Forms.Button();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.FromDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TobaccoInventoryDgv = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TobaccoInventoryDgv)).BeginInit();
            this.SuspendLayout();
            // 
            // TobaccoProductCmb
            // 
            this.TobaccoProductCmb.FormattingEnabled = true;
            this.TobaccoProductCmb.Items.AddRange(new object[] {
            "--Select--",
            "All",
            "Tobacco 8S",
            "Tobacco 10S",
            "Chew",
            "E-Cigar",
            "Cigar 10S",
            "Cigar 8S",
            "Cigar 5S",
            "Singles And Papers"});
            this.TobaccoProductCmb.Location = new System.Drawing.Point(172, 57);
            this.TobaccoProductCmb.Name = "TobaccoProductCmb";
            this.TobaccoProductCmb.Size = new System.Drawing.Size(144, 21);
            this.TobaccoProductCmb.TabIndex = 242;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(61, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 241;
            this.label2.Text = "Product";
            // 
            // ToDatePicker
            // 
            this.ToDatePicker.CustomFormat = "MMM/dd/yyyy";
            this.ToDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.ToDatePicker.Location = new System.Drawing.Point(454, 24);
            this.ToDatePicker.Name = "ToDatePicker";
            this.ToDatePicker.Size = new System.Drawing.Size(144, 20);
            this.ToDatePicker.TabIndex = 240;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(343, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 15);
            this.label1.TabIndex = 239;
            this.label1.Text = "To Date";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.AliceBlue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(359, 105);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 33);
            this.button1.TabIndex = 238;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // Exportbutton
            // 
            this.Exportbutton.BackColor = System.Drawing.Color.AliceBlue;
            this.Exportbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exportbutton.Location = new System.Drawing.Point(263, 105);
            this.Exportbutton.Name = "Exportbutton";
            this.Exportbutton.Size = new System.Drawing.Size(90, 33);
            this.Exportbutton.TabIndex = 237;
            this.Exportbutton.Text = "Export";
            this.Exportbutton.UseVisualStyleBackColor = false;
            this.Exportbutton.Click += new System.EventHandler(this.Exportbutton_Click);
            // 
            // ViewBtn
            // 
            this.ViewBtn.BackColor = System.Drawing.Color.AliceBlue;
            this.ViewBtn.Location = new System.Drawing.Point(168, 105);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(79, 33);
            this.ViewBtn.TabIndex = 236;
            this.ViewBtn.Text = "View";
            this.ViewBtn.UseVisualStyleBackColor = false;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // FromDatePicker
            // 
            this.FromDatePicker.CustomFormat = "MMM/dd/yyyy";
            this.FromDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.FromDatePicker.Location = new System.Drawing.Point(172, 22);
            this.FromDatePicker.Name = "FromDatePicker";
            this.FromDatePicker.Size = new System.Drawing.Size(144, 20);
            this.FromDatePicker.TabIndex = 235;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(61, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 234;
            this.label3.Text = "From Date";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TobaccoInventoryDgv);
            this.groupBox1.Location = new System.Drawing.Point(12, 171);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(971, 341);
            this.groupBox1.TabIndex = 243;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tobacco Inventory Report";
            // 
            // TobaccoInventoryDgv
            // 
            this.TobaccoInventoryDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TobaccoInventoryDgv.Location = new System.Drawing.Point(18, 29);
            this.TobaccoInventoryDgv.Name = "TobaccoInventoryDgv";
            this.TobaccoInventoryDgv.Size = new System.Drawing.Size(937, 306);
            this.TobaccoInventoryDgv.TabIndex = 0;
            // 
            // TobaccoInvReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(994, 524);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TobaccoProductCmb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ToDatePicker);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Exportbutton);
            this.Controls.Add(this.ViewBtn);
            this.Controls.Add(this.FromDatePicker);
            this.Controls.Add(this.label3);
            this.Name = "TobaccoInvReportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tobacco Inventory";
            this.Load += new System.EventHandler(this.TobaccoInvReportForm_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TobaccoInventoryDgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox TobaccoProductCmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker ToDatePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Exportbutton;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.DateTimePicker FromDatePicker;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView TobaccoInventoryDgv;
    }
}