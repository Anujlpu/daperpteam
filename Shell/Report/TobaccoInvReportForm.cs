﻿using ExportToExcel;
using Framework;
using Framework.Data;
using Framework.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Report
{
    public partial class TobaccoInvReportForm : BaseForm
    {
        public TobaccoInvReportForm()
        {
            InitializeComponent();
        }
        private List<TobaccoEntity> BindOrExportGrid()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime fromDate = Convert.ToDateTime(FromDatePicker.Text);
                DateTime toDate = Convert.ToDateTime(ToDatePicker.Text);
                List<TobaccoEntity> tobaccoInvt = new List<TobaccoEntity>();
                string type = string.Empty;
                if (TobaccoProductCmb.SelectedIndex > 1)
                {
                    type = TobaccoProductCmb.Text;
                }

                if (!string.IsNullOrEmpty(type))
                {
                    tobaccoInvt = (from s in context.Tbl_TaboccoInventory
                                   join v in context.Mst_Vendor on s.VendorId equals v.VendorId
                                   join u in context.MST_UserMaster on s.CreatedBy equals u.Userid
                                   //join um in context.MST_UserMaster on s.ModifiedBy equals um.ModifiedBy
                                   where (s.IsActive != false) && (s.TransactionDate >= fromDate && s.TransactionDate <= toDate) && s.ProductCategoryName == type
                                   select new TobaccoEntity
                                   {
                                       TaboccoID = s.TaboccoID,
                                       TransactionDate = s.TransactionDate,
                                       ProductCategoryName = s.ProductCategoryName,
                                       Balance = s.Balance,
                                       Add = s.Add,
                                       Substract = s.Substract,
                                       Remarks = s.Remarks,
                                       SubstractionReason = s.SubstractionReason,
                                       VendorName = v.VendorName,
                                       CreatedBy = s.CreatedBy,
                                       CreatedDate = s.CreatedDate,


                                   }).Distinct().OrderBy(s => s.TransactionDate).ToList();
                    return tobaccoInvt;
                }
                else
                {
                    tobaccoInvt = (from s in context.Tbl_TaboccoInventory
                                   join v in context.Mst_Vendor on s.VendorId equals v.VendorId
                                   join u in context.MST_UserMaster on s.CreatedBy equals u.Userid
                                   //join um in context.MST_UserMaster on s.ModifiedBy equals um.ModifiedBy
                                   where (s.IsActive != false)&& (s.TransactionDate >= fromDate && s.TransactionDate <= toDate)
                                   select new TobaccoEntity
                                   {
                                       TaboccoID = s.TaboccoID,
                                       TransactionDate = s.TransactionDate,
                                       ProductCategoryName = s.ProductCategoryName,
                                       Balance = s.Balance,
                                       Add = s.Add,
                                       Substract = s.Substract,
                                       Remarks = s.Remarks,
                                       SubstractionReason = s.SubstractionReason,
                                       VendorName = v.VendorName,
                                       CreatedBy = s.CreatedBy,
                                       CreatedDate = s.CreatedDate,

                                   }).Distinct().OrderBy(s => s.TransactionDate).ToList();
                    return tobaccoInvt;
                }

            }
        }
        private void TobaccoInvReportForm_Load(object sender, EventArgs e)
        {
            TobaccoProductCmb.SelectedIndex = 0;
        }

        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                TobaccoInventoryDgv.DataSource = BindOrExportGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Exportbutton_Click(object sender, EventArgs e)
        {
            try
            {

                try
                {
                    if (BindOrExportGrid().Count > 0)
                    {
                        CreateExcelFile.CreateExcelDocument(BindOrExportGrid(), Path.Combine(ShellComman.ReportExelPath, "TobaccoMasterInventory_" + DateTime.Now.ToString("yyyy_MMM_dd_hhmmss")) + ".xls");
                        MessageBox.Show("Tobacco Report extracted in excel.");
                    }
                    else
                    {
                        MessageBox.Show("No record found.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }
    }
}
