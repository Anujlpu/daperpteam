﻿namespace Shell.Report
{
    partial class EmployeeReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EmployeelistView = new System.Windows.Forms.ListView();
            this.Exportbutton = new System.Windows.Forms.Button();
            this.Displaybutton = new System.Windows.Forms.Button();
            this.TodateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.FromdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.Fromlabel = new System.Windows.Forms.Label();
            this.Namelabel = new System.Windows.Forms.Label();
            this.groupGridView = new System.Windows.Forms.GroupBox();
            this.EmployeedataGridView = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupGridView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeedataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EmployeelistView);
            this.groupBox1.Controls.Add(this.Exportbutton);
            this.groupBox1.Controls.Add(this.Displaybutton);
            this.groupBox1.Controls.Add(this.TodateTimePicker);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.FromdateTimePicker);
            this.groupBox1.Controls.Add(this.Fromlabel);
            this.groupBox1.Controls.Add(this.Namelabel);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(941, 238);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Employee Report Filter";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // EmployeelistView
            // 
            this.EmployeelistView.Alignment = System.Windows.Forms.ListViewAlignment.Left;
            this.EmployeelistView.CheckBoxes = true;
            this.EmployeelistView.Location = new System.Drawing.Point(170, 22);
            this.EmployeelistView.Name = "EmployeelistView";
            this.EmployeelistView.Size = new System.Drawing.Size(725, 113);
            this.EmployeelistView.TabIndex = 12;
            this.EmployeelistView.UseCompatibleStateImageBehavior = false;
            this.EmployeelistView.View = System.Windows.Forms.View.List;
            this.EmployeelistView.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.EmployeelistView_ItemCheck);
            this.EmployeelistView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.EmployeelistView_ItemChecked);
            // 
            // Exportbutton
            // 
            this.Exportbutton.Location = new System.Drawing.Point(487, 199);
            this.Exportbutton.Name = "Exportbutton";
            this.Exportbutton.Size = new System.Drawing.Size(90, 33);
            this.Exportbutton.TabIndex = 11;
            this.Exportbutton.Text = "Export";
            this.Exportbutton.UseVisualStyleBackColor = true;
            this.Exportbutton.Click += new System.EventHandler(this.Exportbutton_Click);
            // 
            // Displaybutton
            // 
            this.Displaybutton.Location = new System.Drawing.Point(391, 199);
            this.Displaybutton.Name = "Displaybutton";
            this.Displaybutton.Size = new System.Drawing.Size(90, 33);
            this.Displaybutton.TabIndex = 10;
            this.Displaybutton.Text = "Display";
            this.Displaybutton.UseVisualStyleBackColor = true;
            this.Displaybutton.Click += new System.EventHandler(this.Displaybutton_Click);
            // 
            // TodateTimePicker
            // 
            this.TodateTimePicker.Location = new System.Drawing.Point(170, 170);
            this.TodateTimePicker.Name = "TodateTimePicker";
            this.TodateTimePicker.Size = new System.Drawing.Size(176, 23);
            this.TodateTimePicker.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "To";
            // 
            // FromdateTimePicker
            // 
            this.FromdateTimePicker.Location = new System.Drawing.Point(170, 141);
            this.FromdateTimePicker.Name = "FromdateTimePicker";
            this.FromdateTimePicker.Size = new System.Drawing.Size(176, 23);
            this.FromdateTimePicker.TabIndex = 3;
            // 
            // Fromlabel
            // 
            this.Fromlabel.AutoSize = true;
            this.Fromlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fromlabel.Location = new System.Drawing.Point(6, 141);
            this.Fromlabel.Name = "Fromlabel";
            this.Fromlabel.Size = new System.Drawing.Size(40, 17);
            this.Fromlabel.TabIndex = 2;
            this.Fromlabel.Text = "From";
            // 
            // Namelabel
            // 
            this.Namelabel.AutoSize = true;
            this.Namelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Namelabel.Location = new System.Drawing.Point(6, 32);
            this.Namelabel.Name = "Namelabel";
            this.Namelabel.Size = new System.Drawing.Size(111, 17);
            this.Namelabel.TabIndex = 0;
            this.Namelabel.Text = "Employee Name";
            // 
            // groupGridView
            // 
            this.groupGridView.Controls.Add(this.EmployeedataGridView);
            this.groupGridView.Location = new System.Drawing.Point(12, 295);
            this.groupGridView.Name = "groupGridView";
            this.groupGridView.Size = new System.Drawing.Size(941, 188);
            this.groupGridView.TabIndex = 1;
            this.groupGridView.TabStop = false;
            this.groupGridView.Text = "Employee Details";
            // 
            // EmployeedataGridView
            // 
            this.EmployeedataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EmployeedataGridView.Location = new System.Drawing.Point(6, 19);
            this.EmployeedataGridView.Name = "EmployeedataGridView";
            this.EmployeedataGridView.Size = new System.Drawing.Size(920, 234);
            this.EmployeedataGridView.TabIndex = 0;
            // 
            // EmployeeReportForm
            // 
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(965, 508);
            this.Controls.Add(this.groupGridView);
            this.Controls.Add(this.groupBox1);
            this.Name = "EmployeeReportForm";
            this.Text = "Employee Report";
            this.Load += new System.EventHandler(this.EmployeeReportForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupGridView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EmployeedataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Namelabel;
        private System.Windows.Forms.Button Exportbutton;
        private System.Windows.Forms.Button Displaybutton;
        private System.Windows.Forms.DateTimePicker TodateTimePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker FromdateTimePicker;
        private System.Windows.Forms.Label Fromlabel;
        private System.Windows.Forms.GroupBox groupGridView;
        private System.Windows.Forms.DataGridView EmployeedataGridView;
        private System.Windows.Forms.ListView EmployeelistView;
    }
}
