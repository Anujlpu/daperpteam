﻿using ExportToExcel;
using Framework.Data;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
namespace Shell.Report
{
    public partial class LottoWeeklyReportForm : BaseForm
    {
        public LottoWeeklyReportForm()
        {
            InitializeComponent();
        }
        #region private Methods
        private void BindGrid()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime fromDate = Convert.ToDateTime(FromdateTimePicker.Text);
                DateTime toDate = Convert.ToDateTime(TodateTimePicker.Text);

                var LottoReports = (from v in context.Tbl_WeeklyLotto
                                    where v.IsActive == true
                                    select new
                                    {
                                        v.InvoiceDate,
                                        v.OnlineSold,
                                        v.Activation,
                                        v.Credit,
                                        v.Redemptions,
                                        v.RetailerBonus,
                                        v.AmountDue

                                    }
                                  ).ToList();
                LottoReports = LottoReports.Where(s => Convert.ToDateTime(s.InvoiceDate) >= fromDate && Convert.ToDateTime(s.InvoiceDate) <= toDate).ToList();

                if (LottoReports.Count > 0)
                {
                    dgvLottoReport.DataSource = LottoReports;
                }
                else
                {
                    MessageBox.Show("No record found.");
                }
            }

        }


        #endregion
        #region Form Events
        private void Displaybutton_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LottoWeeklyReportForm_Load(object sender, EventArgs e)
        {

        }

        private void Exportbutton_Click(object sender, EventArgs e)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime fromDate = Convert.ToDateTime(FromdateTimePicker.Text);
                    DateTime toDate = Convert.ToDateTime(TodateTimePicker.Text);

                    var LottoReports = (from v in context.Tbl_WeeklyLotto
                                        where v.IsActive == true
                                        select new
                                        {
                                            v.InvoiceDate,
                                            v.OnlineSold,
                                            v.Activation,
                                            v.Credit,
                                            v.Redemptions,
                                            v.RetailerBonus,
                                            v.AmountDue

                                        }
                                      ).ToList();
                    LottoReports = LottoReports.Where(s => Convert.ToDateTime(s.InvoiceDate) >= fromDate && Convert.ToDateTime(s.InvoiceDate) <= toDate).ToList();

                    if (LottoReports.Count > 0)
                    {
                        CreateExcelFile.CreateExcelDocument(LottoReports, Path.Combine(ShellComman.ReportExelPath, "InvoiceReport_" + DateTime.Now.ToString("yyyy_MMM_dd_hhmmss")) + ".xls");
                        MessageBox.Show("Data extracted in excel.");
                    }
                    else
                    {
                        MessageBox.Show("No record found.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

    }
}
