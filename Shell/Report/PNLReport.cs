﻿using BAL;
using DAL;
using ExportToExcel;
using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Report
{
    public partial class PNLReport : BaseForm
    {
        public PNLReport()
        {
            InitializeComponent();
        }


        #region Private methods
        private void BindPNL()
        {
            cmbPNlList.DisplayMember = "PNLCode";
            cmbPNlList.ValueMember = "PNLID";
            BAL_PNL _ObjPNL = new BAL_PNL();
            var list = _ObjPNL.GetPNLlist();
            list.Insert(0, new PNLEntity { PNLID = 0, PNLCode = "--Select--" });
            cmbPNlList.DataSource = list;
        }
        private void BindGrid()
        {
            string invoiceDate = Convert.ToDateTime(dtpInvoiceDate.Text).ToString("MMddyyyy");
            if (cmbPNlList.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select PNL.");
                return;
            }
            int vendorId = Convert.ToInt32(cmbPNlList.SelectedValue);

            using (ShellEntities context = new ShellEntities())
            {
                string tranType = TransactionType();
                var InvoiceList = (from i in context.Tbl_PNLEntry
                                   join v in context.MST_PnlMaster
                                   on i.PNLID equals v.PNLId
                                   where i.IsActive == true && v.IsActive == true && i.PNLID == vendorId && i.TransactionType == tranType
                                   select new
                                   {
                                       v.PNLCode,
                                       v.PNLDescription,
                                       i.EntryDate,
                                       i.InvoiceNo,
                                       i.GST,
                                       i.Cash,
                                       i.Payout,
                                       i.TotalAmount,
                                       i.Total,
                                       IsProcessed = i.IsClosed == true ? "Yes" : "No",
                                       i.Remarks,
                                       i.IsActive,
                                       i.IsClosed,
                                       i.IsApproved
                                   }
                                 ).ToList();
                if (Status.SelectedIndex > 0)
                {
                    if (Status.Text == "Active")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsActive == true).ToList();
                    }
                    else if (Status.Text == "InActive")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsActive == false || s.IsActive == null).ToList();
                    }
                    else if (Status.Text == "Close")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsClosed == true).ToList();
                    }
                    else if (Status.Text == "Open")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsClosed == false || s.IsClosed == null).ToList();
                    }
                    else if (Status.Text == "Approved")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsApproved == true).ToList();
                    }
                    else if (Status.Text == "Non-Approved")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsApproved == false || s.IsApproved == null).ToList();
                    }
                }
                InvoiceList = InvoiceList.Where(s => Convert.ToDateTime(s.EntryDate).ToString("MMddyyyy") == invoiceDate).ToList();
                var Invoice = (from v in InvoiceList
                               select new
                               {

                                   v.PNLCode,
                                   v.InvoiceNo,
                                   v.EntryDate,
                                   v.GST,
                                   v.Cash,
                                   v.Payout,
                                   v.TotalAmount,
                                   v.Total,
                                   v.Remarks,
                               }
                               ).ToList();
                dgvPNL.DataSource = InvoiceList;

            }
        }
        private string TransactionType()
        {
            if (radioButton1.Checked == true)
            {
                return "Debit";
            }
            else
            {
                return "Credit";
            }
        }
        #endregion
        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }

        private void Exportbutton_Click(object sender, EventArgs e)
        {
            try
            {
                string invoiceDate = Convert.ToDateTime(dtpInvoiceDate.Text).ToString("MMddyyyy");
                if (cmbPNlList.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select PNL.");
                    return;
                }
                int vendorId = Convert.ToInt32(cmbPNlList.SelectedValue);

                using (ShellEntities context = new ShellEntities())
                {
                    string tranType = TransactionType();
                    var InvoiceList = (from i in context.Tbl_PNLEntry
                                       join v in context.MST_PnlMaster
                                       on i.PNLID equals v.PNLId
                                       where i.IsActive == true && v.IsActive == true && i.PNLID == vendorId && i.TransactionType == tranType
                                       select new
                                       {
                                           v.PNLCode,
                                           v.PNLDescription,
                                           i.EntryDate,
                                           i.InvoiceNo,
                                           i.GST,
                                           i.Cash,
                                           i.Payout,
                                           i.TotalAmount,
                                           i.Total,
                                           IsProcessed = i.IsClosed == true ? "Yes" : "No",
                                           i.Remarks,
                                           i.IsActive,
                                           i.IsClosed,
                                           i.IsApproved
                                       }
                                     ).ToList();
                    if (Status.SelectedIndex > 0)
                    {
                        if (Status.Text == "Active")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsActive == true).ToList();
                        }
                        else if (Status.Text == "InActive")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsActive == false || s.IsActive == null).ToList();
                        }
                        else if (Status.Text == "Close")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsClosed == true).ToList();
                        }
                        else if (Status.Text == "Open")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsClosed == false || s.IsClosed == null).ToList();
                        }
                        else if (Status.Text == "Approved")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsApproved == true).ToList();
                        }
                        else if (Status.Text == "Non-Approved")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsApproved == false || s.IsApproved == null).ToList();
                        }
                    }
                    InvoiceList = InvoiceList.Where(s => Convert.ToDateTime(s.EntryDate).ToString("MMddyyyy") == invoiceDate).ToList();
                    var Invoice = (from v in InvoiceList
                                   select new
                                   {

                                       v.PNLCode,
                                       v.InvoiceNo,
                                       v.EntryDate,
                                       v.GST,
                                       v.Cash,
                                       v.Payout,
                                       v.TotalAmount,
                                       v.Total,
                                       v.Remarks,
                                   }
                                   ).ToList();
                    if (InvoiceList.Count() > 0)
                    {
                        CreateExcelFile.CreateExcelDocument(InvoiceList, Path.Combine(ShellComman.ReportExelPath, "PNLReport_" + DateTime.Now.ToString("yyyy_MMM_dd_hhmmss")) + ".xls");
                        MessageBox.Show("Data extracted in excel.");
                    }
                    else
                    {
                        MessageBox.Show("There is no record to export.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PNLReport_Load(object sender, EventArgs e)
        {
            try
            {
                BindPNL();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }
    }
}
