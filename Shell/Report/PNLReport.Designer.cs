﻿namespace Shell.Report
{
    partial class PNLReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvPNL = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.Exportbutton = new System.Windows.Forms.Button();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.ddlVendorName = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dtpInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtDebit = new System.Windows.Forms.RadioButton();
            this.rbtCredit = new System.Windows.Forms.RadioButton();
            this.cmbPNlList = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PNLEntryDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.Status = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNL)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvPNL);
            this.groupBox1.Location = new System.Drawing.Point(12, 199);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(967, 306);
            this.groupBox1.TabIndex = 242;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PNL Report";
            // 
            // dgvPNL
            // 
            this.dgvPNL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPNL.Location = new System.Drawing.Point(6, 19);
            this.dgvPNL.Name = "dgvPNL";
            this.dgvPNL.Size = new System.Drawing.Size(929, 281);
            this.dgvPNL.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.AliceBlue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(466, 151);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 33);
            this.button1.TabIndex = 241;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Exportbutton
            // 
            this.Exportbutton.BackColor = System.Drawing.Color.AliceBlue;
            this.Exportbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exportbutton.Location = new System.Drawing.Point(370, 151);
            this.Exportbutton.Name = "Exportbutton";
            this.Exportbutton.Size = new System.Drawing.Size(90, 33);
            this.Exportbutton.TabIndex = 240;
            this.Exportbutton.Text = "Export";
            this.Exportbutton.UseVisualStyleBackColor = false;
            this.Exportbutton.Click += new System.EventHandler(this.Exportbutton_Click);
            // 
            // ViewBtn
            // 
            this.ViewBtn.BackColor = System.Drawing.Color.AliceBlue;
            this.ViewBtn.Location = new System.Drawing.Point(275, 151);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(79, 33);
            this.ViewBtn.TabIndex = 239;
            this.ViewBtn.Text = "View";
            this.ViewBtn.UseVisualStyleBackColor = false;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // ddlVendorName
            // 
            this.ddlVendorName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVendorName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlVendorName.FormattingEnabled = true;
            this.ddlVendorName.Items.AddRange(new object[] {
            "--Select--",
            "PR",
            "Forign Worker",
            "Student Open Work Permit",
            "Closed Work Permit",
            "Citizen",
            "Work Permit",
            "Tourist",
            "Visitor",
            "Others"});
            this.ddlVendorName.Location = new System.Drawing.Point(192, -115);
            this.ddlVendorName.Name = "ddlVendorName";
            this.ddlVendorName.Size = new System.Drawing.Size(173, 22);
            this.ddlVendorName.TabIndex = 232;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(82, -49);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 15);
            this.label13.TabIndex = 238;
            this.label13.Text = "Transaction Type";
            // 
            // dtpInvoiceDate
            // 
            this.dtpInvoiceDate.CustomFormat = "MMM/dd/yyyy";
            this.dtpInvoiceDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpInvoiceDate.Location = new System.Drawing.Point(192, -83);
            this.dtpInvoiceDate.Name = "dtpInvoiceDate";
            this.dtpInvoiceDate.Size = new System.Drawing.Size(173, 20);
            this.dtpInvoiceDate.TabIndex = 233;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(87, -77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 237;
            this.label1.Text = "Invoice Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(87, -112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 15);
            this.label2.TabIndex = 236;
            this.label2.Text = "Vendor Code";
            // 
            // rbtDebit
            // 
            this.rbtDebit.AutoSize = true;
            this.rbtDebit.Checked = true;
            this.rbtDebit.Location = new System.Drawing.Point(188, -49);
            this.rbtDebit.Name = "rbtDebit";
            this.rbtDebit.Size = new System.Drawing.Size(50, 17);
            this.rbtDebit.TabIndex = 234;
            this.rbtDebit.TabStop = true;
            this.rbtDebit.Text = "Debit";
            this.rbtDebit.UseVisualStyleBackColor = true;
            // 
            // rbtCredit
            // 
            this.rbtCredit.AutoSize = true;
            this.rbtCredit.Location = new System.Drawing.Point(266, -48);
            this.rbtCredit.Name = "rbtCredit";
            this.rbtCredit.Size = new System.Drawing.Size(52, 17);
            this.rbtCredit.TabIndex = 235;
            this.rbtCredit.Text = "Credit";
            this.rbtCredit.UseVisualStyleBackColor = true;
            // 
            // cmbPNlList
            // 
            this.cmbPNlList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPNlList.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPNlList.FormattingEnabled = true;
            this.cmbPNlList.Items.AddRange(new object[] {
            "--Select--",
            "PR",
            "Forign Worker",
            "Student Open Work Permit",
            "Closed Work Permit",
            "Citizen",
            "Work Permit",
            "Tourist",
            "Visitor",
            "Others"});
            this.cmbPNlList.Location = new System.Drawing.Point(383, 19);
            this.cmbPNlList.Name = "cmbPNlList";
            this.cmbPNlList.Size = new System.Drawing.Size(173, 22);
            this.cmbPNlList.TabIndex = 243;
            this.cmbPNlList.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(273, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 15);
            this.label3.TabIndex = 249;
            this.label3.Text = "Transaction Type";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // PNLEntryDate
            // 
            this.PNLEntryDate.CustomFormat = "MMM/dd/yyyy";
            this.PNLEntryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PNLEntryDate.Location = new System.Drawing.Point(383, 51);
            this.PNLEntryDate.Name = "PNLEntryDate";
            this.PNLEntryDate.Size = new System.Drawing.Size(173, 20);
            this.PNLEntryDate.TabIndex = 244;
            this.PNLEntryDate.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AliceBlue;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(278, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 15);
            this.label4.TabIndex = 248;
            this.label4.Text = "Entry Date";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(278, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 15);
            this.label5.TabIndex = 247;
            this.label5.Text = "PNL List";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(379, 85);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(50, 17);
            this.radioButton1.TabIndex = 245;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Debit";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(457, 86);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(52, 17);
            this.radioButton2.TabIndex = 246;
            this.radioButton2.Text = "Credit";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // Status
            // 
            this.Status.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Status.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.FormattingEnabled = true;
            this.Status.Items.AddRange(new object[] {
            "--Select--",
            "Active",
            "InActive",
            "Close",
            "Open",
            "Approved",
            "Non-Approved"});
            this.Status.Location = new System.Drawing.Point(383, 109);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(173, 22);
            this.Status.TabIndex = 250;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(274, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 15);
            this.label6.TabIndex = 251;
            this.label6.Text = "Status";
            // 
            // PNLReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1003, 517);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbPNlList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PNLEntryDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Exportbutton);
            this.Controls.Add(this.ViewBtn);
            this.Controls.Add(this.ddlVendorName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.dtpInvoiceDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbtDebit);
            this.Controls.Add(this.rbtCredit);
            this.Name = "PNLReport";
            this.Text = "PNLReport";
            this.Load += new System.EventHandler(this.PNLReport_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvPNL;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Exportbutton;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.ComboBox ddlVendorName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dtpInvoiceDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtDebit;
        private System.Windows.Forms.RadioButton rbtCredit;
        private System.Windows.Forms.ComboBox cmbPNlList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker PNLEntryDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ComboBox Status;
        private System.Windows.Forms.Label label6;
    }
}