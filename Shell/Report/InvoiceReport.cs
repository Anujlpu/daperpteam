﻿using DAL;
using ExportToExcel;
using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Report
{
    public partial class InvoiceReport : BaseForm
    {
        public InvoiceReport()
        {
            InitializeComponent();
        }

        #region Private methods
        private void BindVendor()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendors = _objVendor.GetVendorlist();
            vendors.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            ddlVendorName.DataSource = vendors;
            ddlVendorName.DisplayMember = "VendorName";
            ddlVendorName.ValueMember = "VendorID";
        }
        private void BindGrid()
        {
            string invoiceDate = Convert.ToDateTime(dtpInvoiceDate.Text).ToString("MMddyyyy");
            if (ddlVendorName.SelectedIndex <= 0)
            {
                MessageBox.Show("Please select vendor.");
                return;
            }
            int vendorId = Convert.ToInt32(ddlVendorName.SelectedValue);

            using (ShellEntities context = new ShellEntities())
            {
                string tranType = TransactionType();
                var InvoiceList = (from i in context.Tbl_Invoices
                                   join v in context.Mst_Vendor
                                   on i.VendorId equals v.VendorId
                                   where i.VendorId == vendorId && i.TransactionType == tranType
                                   select new
                                   {
                                       v.VendorName,
                                       v.VendorCode,
                                       i.InvoiceNo,
                                       i.InvDate,
                                       i.GST,
                                       i.Cash,
                                       i.Payout,
                                       i.TotalAmount,
                                       i.Total,
                                       IsProcessed = i.IsClosed == true ? "Yes" : "No",
                                       i.Remarks,
                                       i.IsApproved,
                                       i.IsClosed,
                                       i.IsActive
                                   }
                                 ).ToList();
                if (Status.SelectedIndex > 0)
                {
                    if (Status.Text == "Active")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsActive == true).ToList();
                    }
                    else if (Status.Text == "InActive")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsActive == false || s.IsActive == null).ToList();
                    }
                    else if (Status.Text == "Close")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsClosed == true).ToList();
                    }
                    else if (Status.Text == "Open")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsClosed == false || s.IsClosed == null).ToList();
                    }
                    else if (Status.Text == "Approved")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsApproved == true).ToList();
                    }
                    else if (Status.Text == "Non-Approved")
                    {
                        InvoiceList = InvoiceList.Where(s => s.IsApproved == false || s.IsApproved == null).ToList();
                    }
                }


                InvoiceList = InvoiceList.Where(s => Convert.ToDateTime(s.InvDate).ToString("MMddyyyy") == invoiceDate).ToList();
                var Invoice = (from v in InvoiceList
                               select new
                               {

                                   v.VendorName,
                                   v.VendorCode,
                                   v.InvoiceNo,
                                   v.InvDate,
                                   v.GST,
                                   v.Cash,
                                   v.Payout,
                                   v.TotalAmount,
                                   v.Total,

                                   v.Remarks,


                               }
                               ).ToList();
                dgvInvoice.DataSource = Invoice;

            }
        }
        private string TransactionType()
        {
            if (rbtDebit.Checked == true)
            {
                return "Debit";
            }
            else
            {
                return "Credit";
            }
        }
        #endregion

        #region Form Events
        private void InvoiceReport_Load(object sender, EventArgs e)
        {
            try
            {
                BindVendor();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }

        private void rbtDebit_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void ddlVendorName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void ddlPaymnetType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void dtpPayment_ValueChanged(object sender, EventArgs e)
        {

        }

        private void rbtCredit_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }

        private void Exportbutton_Click(object sender, EventArgs e)
        {
            try
            {
                string invoiceDate = Convert.ToDateTime(dtpInvoiceDate.Text).ToString("MMddyyyy");
                if (ddlVendorName.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select vendor.");
                    return;
                }
                int vendorId = Convert.ToInt32(ddlVendorName.SelectedValue);
                using (ShellEntities context = new ShellEntities())
                {
                    string tranType = TransactionType();
                    var InvoiceList = (from i in context.Tbl_Invoices
                                       join v in context.Mst_Vendor
                                       on i.VendorId equals v.VendorId
                                       where i.VendorId == vendorId && i.TransactionType == tranType
                                       select new
                                       {
                                           v.VendorName,
                                           v.VendorCode,
                                           i.InvoiceNo,
                                           i.InvDate,
                                           i.GST,
                                           i.Cash,
                                           i.Payout,
                                           i.TotalAmount,
                                           i.Total,
                                           IsProcessed = i.IsClosed == true ? "Yes" : "No",
                                           i.Remarks,
                                           i.IsApproved,
                                           i.IsClosed,
                                           i.IsActive
                                       }
                                     ).ToList();
                    if (Status.SelectedIndex > 0)
                    {
                        if (Status.Text == "Active")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsActive == true).ToList();
                        }
                        else if (Status.Text == "InActive")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsActive == false || s.IsActive == null).ToList();
                        }
                        else if (Status.Text == "Close")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsClosed == true).ToList();
                        }
                        else if (Status.Text == "Open")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsClosed == false || s.IsClosed == null).ToList();
                        }
                        else if (Status.Text == "Approved")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsApproved == true).ToList();
                        }
                        else if (Status.Text == "Non-Approved")
                        {
                            InvoiceList = InvoiceList.Where(s => s.IsApproved == false || s.IsApproved == null).ToList();
                        }
                    }

                    InvoiceList = InvoiceList.Where(s => Convert.ToDateTime(s.InvDate).ToString("MMddyyyy") == invoiceDate).ToList();
                    var Invoice = (from v in InvoiceList
                                   select new
                                   {
                                       v.VendorName,
                                       v.VendorCode,
                                       v.InvoiceNo,
                                       v.InvDate,
                                       v.GST,
                                       v.Cash,
                                       v.Payout,
                                       v.TotalAmount,
                                       v.Total,
                                       v.Remarks,
                                   }
                                   ).ToList();
                    if (InvoiceList.Count > 0)
                    {
                        CreateExcelFile.CreateExcelDocument(Invoice, Path.Combine(ShellComman.ReportExelPath, "InvoiceReport_" + DateTime.Now.ToString("yyyy_MMM_dd_hhmmss")) + ".xls");
                        MessageBox.Show("Data extracted in excel.");
                    }
                    else
                    {
                        MessageBox.Show("There is no record to export.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}
