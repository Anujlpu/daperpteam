﻿using ExportToExcel;
using Framework;
using Framework.Data;
using Framework.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Shell.Report
{
    public partial class LottoMasterInventoryReport : BaseForm
    {
        public LottoMasterInventoryReport()
        {
            InitializeComponent();
        }

        #region Private Methods

        private List<LottoEODInventoryEntity> BindGrid()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime fromDate = Convert.ToDateTime(FromDatePicker.Text);
                DateTime toDate = Convert.ToDateTime(ToDatePicker.Text);
                List<LottoEODInventoryEntity> lottoInvt = new List<LottoEODInventoryEntity>();
                string type = string.Empty;
                if (LottoProductCmb.SelectedIndex > 1)
                {
                    type = LottoProductCmb.Text;
                }

                if (!string.IsNullOrEmpty(type))
                {
                    lottoInvt = (from s in context.Tbl_LottoEODMasterInventory
                                     // join m in context.Mst_MarginMaster on p.ProductId equals m.ProductId
                                     join u in context.MST_UserMaster on s.CreatedBy equals u.Userid
                                     //join um in context.MST_UserMaster on s.ModifiedBy equals um.ModifiedBy
                                 where s.IsActive == true && (s.TransactionDate >= fromDate && s.TransactionDate <= toDate) && s.InventoryType == type
                                 select new LottoEODInventoryEntity
                                 {
                                     LottoEODMasterID = s.LottoEODMasterID,
                                     TransactionDate = s.TransactionDate,
                                     InventoryType = s.InventoryType,
                                     Opening1__50 = s.Opening1__50,
                                     Add1__50 = s.Add1__50,
                                     Substract1__50 = s.Substract1__50,
                                     Return1__50 = s.Return1__50,
                                     Closing1__50 = s.Closing1__50,
                                     Opening1__100 = s.Opening1__100,
                                     Add1__100 = s.Add1__100,
                                     Substract1__100 = s.Substract1__100,
                                     Return1__100 = s.Return1__100,
                                     Closing1__100 = s.Closing1__100,
                                     Opening2_ = s.Opening2_,
                                     Add2_ = s.Add2_,
                                     Substract2_ = s.Substract2_,
                                     Return2_ = s.Return2_,
                                     Closing2_ = s.Closing2_,
                                     Opening3_ = s.Opening3_,
                                     Add3_ = s.Add3_,
                                     Substract3_ = s.Substract3_,
                                     Return3_ = s.Return3_,
                                     Closing3_ = s.Closing3_,
                                     Opening4_ = s.Opening4_,
                                     Add4_ = s.Add4_,
                                     Substract4_ = s.Substract4_,
                                     Return4_ = s.Return4_,
                                     Closing4_ = s.Closing4_,
                                     Opening5_ = s.Opening5_,
                                     Add5_ = s.Add5_,
                                     Substract5_ = s.Substract5_,
                                     Return5_ = s.Return5_,
                                     Closing5_ = s.Closing5_,
                                     Opening7_ = s.Opening7_,
                                     Add7_ = s.Add7_,
                                     Substract7_ = s.Substract7_,
                                     Return7_ = s.Return7_,
                                     Closing7_ = s.Closing7_,
                                     Opening10_ = s.Opening10_,
                                     Add10_ = s.Add10_,
                                     Substract10_ = s.Substract10_,
                                     Return10_ = s.Return10_,
                                     Closing10_ = s.Closing10_,
                                     Opening20_ = s.Opening20_,
                                     Add20_ = s.Add20_,
                                     Substract20_ = s.Substract20_,
                                     Return20_ = s.Return20_,
                                     Closing20_ = s.Closing20_,
                                     Add30__50 = s.Add30__50,
                                     Substract30__50 = s.Substract30__50,
                                     Return30__50 = s.Return30__50,
                                     Closing30__50 = s.Closing30__50,
                                     Add30__100 = s.Add30__100,
                                     Substract30__100 = s.Substract30__100,
                                     Return30__100 = s.Return30__100,
                                     Closing30__100 = s.Closing30__100,
                                     IsActive = s.IsActive,
                                     TotalActivatedLotto = s.TotalActivatedLotto,
                                     TotalUnActivatedLotto = s.TotalUnActivatedLotto,
                                     Remarks =s.Remarks,
                                     CreatedBy=s.CreatedBy,
                                     CreatedDate=s.CreatedDate,
                                  
                                   
                                 }).Distinct().OrderBy(s => s.TransactionDate).ToList();
                    return lottoInvt;
                }
                else
                {
                    lottoInvt = (from s in context.Tbl_LottoEODMasterInventory
                                     // join m in context.Mst_MarginMaster on p.ProductId equals m.ProductId
                                 where s.IsActive == true && (s.TransactionDate >= fromDate && s.TransactionDate <= toDate)
                                 select new LottoEODInventoryEntity
                                 {
                                     LottoEODMasterID = s.LottoEODMasterID,
                                     TransactionDate = s.TransactionDate,
                                     InventoryType = s.InventoryType,
                                     Opening1__50 = s.Opening1__50,
                                     Add1__50 = s.Add1__50,
                                     Substract1__50 = s.Substract1__50,
                                     Return1__50 = s.Return1__50,
                                     Closing1__50 = s.Closing1__50,
                                     Opening1__100 = s.Opening1__100,
                                     Add1__100 = s.Add1__100,
                                     Substract1__100 = s.Substract1__100,
                                     Return1__100 = s.Return1__100,
                                     Closing1__100 = s.Closing1__100,
                                     Opening2_ = s.Opening2_,
                                     Add2_ = s.Add2_,
                                     Substract2_ = s.Substract2_,
                                     Return2_ = s.Return2_,
                                     Closing2_ = s.Closing2_,
                                     Opening3_ = s.Opening3_,
                                     Add3_ = s.Add3_,
                                     Substract3_ = s.Substract3_,
                                     Return3_ = s.Return3_,
                                     Closing3_ = s.Closing3_,
                                     Opening4_ = s.Opening4_,
                                     Add4_ = s.Add4_,
                                     Substract4_ = s.Substract4_,
                                     Return4_ = s.Return4_,
                                     Closing4_ = s.Closing4_,
                                     Opening5_ = s.Opening5_,
                                     Add5_ = s.Add5_,
                                     Substract5_ = s.Substract5_,
                                     Return5_ = s.Return5_,
                                     Closing5_ = s.Closing5_,
                                     Opening7_ = s.Opening7_,
                                     Add7_ = s.Add7_,
                                     Substract7_ = s.Substract7_,
                                     Return7_ = s.Return7_,
                                     Closing7_ = s.Closing7_,
                                     Opening10_ = s.Opening10_,
                                     Add10_ = s.Add10_,
                                     Substract10_ = s.Substract10_,
                                     Return10_ = s.Return10_,
                                     Closing10_ = s.Closing10_,
                                     Opening20_ = s.Opening20_,
                                     Add20_ = s.Add20_,
                                     Substract20_ = s.Substract20_,
                                     Return20_ = s.Return20_,
                                     Closing20_ = s.Closing20_,
                                     Add30__50 = s.Add30__50,
                                     Substract30__50 = s.Substract30__50,
                                     Return30__50 = s.Return30__50,
                                     Closing30__50 = s.Closing30__50,
                                     Add30__100 = s.Add30__100,
                                     Substract30__100 = s.Substract30__100,
                                     Return30__100 = s.Return30__100,
                                     Closing30__100 = s.Closing30__100,
                                     IsActive = s.IsActive,
                                     TotalActivatedLotto = s.TotalActivatedLotto,
                                     TotalUnActivatedLotto = s.TotalUnActivatedLotto,
                                     Remarks = s.Remarks,
                                     CreatedBy = s.CreatedBy,
                                     CreatedDate = s.CreatedDate,

                                 }).Distinct().OrderBy(s => s.TransactionDate).ToList();
                    return lottoInvt;
                }

            }
        }

        #endregion

        #region Form Events


        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (BindGrid().Count > 0)
                {

                    LottoInventoryDgv.DataSource = BindGrid();

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }

        }

        private void Exportbutton_Click(object sender, EventArgs e)
        {
            try
            {

                try
                {
                    if (BindGrid().Count > 0)
                    {
                        CreateExcelFile.CreateExcelDocument(BindGrid(), Path.Combine(ShellComman.ReportExelPath, "LottoMasterInventory_" + DateTime.Now.ToString("yyyy_MMM_dd_hhmmss")) + ".xls");
                        MessageBox.Show("Lotto Report extracted in excel.");
                    }
                    else
                    {
                        MessageBox.Show("No record found.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }
        private void InvoiceReport_Load(object sender, EventArgs e)
        {
            LottoProductCmb.SelectedIndex = 0;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

    }
}
