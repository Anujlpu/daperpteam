﻿using ExportToExcel;
using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
namespace Shell.Report
{
    public partial class EmployeeReportForm : BaseForm
    {
        public EmployeeReportForm()
        {
            InitializeComponent();
        }
        #region Private Event
        private void BindEmployee()
        {
            try
            {
                EmployeelistView.Items.Clear();
                ShellEntities context = new ShellEntities();
                var employees = from m in context.Mst_EmployeeMaster select new { m.EmployeeName, m.EmployeeId };
                foreach (var e in employees)
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = e.EmployeeName;
                    item.ImageKey = e.EmployeeId.ToString();
                    EmployeelistView.Items.Add(item);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.InnerException);
            }

        }
        List<string> checkedItemsList = new List<string>();
        private dynamic ListOfEmployees()
        {
            if (string.IsNullOrEmpty(FromdateTimePicker.Text))
            {
                MessageBox.Show("Please select from date.");
                FromdateTimePicker.Focus();
                return 0;
            }
            else if (string.IsNullOrEmpty(TodateTimePicker.Text))
            {
                MessageBox.Show("Please select to date.");
                FromdateTimePicker.Focus();
                return 0;
            }

            ShellEntities context = new ShellEntities();
            DateTime fromDate = Convert.ToDateTime(FromdateTimePicker.Text);
            DateTime toDate = Convert.ToDateTime(TodateTimePicker.Text);
            var list = (from e in context.Mst_EmployeeSalaries
                        join s in context.Mst_EmployeeMaster on e.EmployeeID equals s.EmployeeId
                        where e.IsActive == true && s.IsActive == true
                        select new
                        {
                            e.FromDate,
                            e.ToDate,
                            e.EmployeeID,
                            s.EmployeeName,
                            e.PPHours,
                            e.GrossPay,
                            e.VPayTotal,
                            e.Bonus,
                            e.Advance,
                            e.CPP,
                            e.EL,
                            e.Tax,
                            e.Deduction,
                            e.NetSalary
                        }).ToList();

            if (checkedItemsList.Count > 0)
            {
                list = list.Where(s => checkedItemsList.Contains(s.EmployeeID.ToString())).ToList();
            }

            return list;
        }
        #endregion

        #region Form Events
        private void Exportbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ListOfEmployees().Count > 0)
                {
                    CreateExcelFile.CreateExcelDocument(ListOfEmployees(), Path.Combine(ShellComman.ReportExelPath, "EmployeeSalary_" + DateTime.Now.ToString("yyyy_MMM_dd_hhmmss")) + ".xls");
                    MessageBox.Show("Data extracted in excel.");
                }
                else
                {
                    MessageBox.Show("Please contact administrator.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }

        }
        private void EmployeelistView_ItemCheck(object sender, ItemCheckEventArgs e)
        {

        }

        private void EmployeelistView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            try
            {
                if (e.Item.Checked)
                {
                    checkedItemsList.Add(e.Item.ImageKey);
                }
                else
                {
                    if (checkedItemsList.Contains(e.Item.ImageKey))
                    {
                        checkedItemsList.Remove(e.Item.ImageKey);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void Displaybutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ListOfEmployees().Count > 0)
                {
                    EmployeedataGridView.DataSource = ListOfEmployees();
                }
                else
                {
                    MessageBox.Show("Please contact administrator.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void EmployeeReportForm_Load(object sender, EventArgs e)
        {
            try
            {
                BindEmployee();
                FromdateTimePicker.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        #endregion


    }
}
