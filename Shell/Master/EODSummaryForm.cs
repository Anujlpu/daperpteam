﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class EODSummaryForm : BaseForm
    {
        public EODSummaryForm()
        {
            InitializeComponent();
        }
        public int TransactionId { get; set; }
        private void LoadSummary(int transactionId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                var summaryList = (from v in context.TBL_EODRemarks
                                   join d in context.MST_ProfileMaster
                                   on v.ProfileId equals d.ProfileId
                                   where v.FuelTransactionID == transactionId && v.IsActive == true
                                   select new
                                   {
                                       ApprovedByRole = d.ProfileCode,
                                       Remarks = v.Remarks
                                   }
                                 ).ToList();
                SummaryGridView.DataSource = summaryList;
            }
        }
        private void EODSummaryForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadSummary(TransactionId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
