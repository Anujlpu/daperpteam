﻿using Framework;
using Framework.Data;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Shell
{
    public partial class ProductMaster : BaseForm
    {
        public ProductMaster()
        {
            InitializeComponent();
        }

        #region Form Events
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveProduct();
                MessageBox.Show("Data Saved!");
                BindProductList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtProductName.Text))
                {
                    MessageBox.Show("Please enter ptoduct name/code.");
                    return;
                }
                UpdateProduct(ProductId);
                MessageBox.Show("Data Updated!");
                BindProductList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteProduct(ProductId);
                MessageBox.Show("Record Deleted!");
                BindProductList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void ProductMaster_Load(object sender, EventArgs e)
        {

            try
            {
                LoadCategories();
                BindProductList();
                BtnSave.Enabled = true;
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                grpProductList.Visible = false;
                grpManageProduct.Visible = true;
                LoadCategories();
                BtnSave.Enabled = true;
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }

        }
        private void dgvProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dgvProduct.Rows[e.RowIndex];
                    ProductId = Convert.ToInt32(row.Cells["ProductId"].Value);
                    BtnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    BtnDelete.Enabled = true;
                    FillFields(ProductId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        #endregion

        #region Private Methods

        private void LoadCategories()
        {
            using (ShellEntities context = new ShellEntities())
            {
                CategorycomboBox.ValueMember = "CategoryID";
                CategorycomboBox.DisplayMember = "CategoryName";
                var categorymstList = context.MST_CategoriesMaster.Where(s => s.IsActive == true).Select(s => new { s.CategoryID, s.CategoryName }).ToList();
                categorymstList.Insert(0, new { CategoryID = 0, CategoryName = "--Select--" });
                CategorycomboBox.DataSource = categorymstList;
            }
        }
        private void SaveProduct()
        {
            using (ShellEntities context = new ShellEntities())
            {
                Mst_ProductMaster prodMaster = new Mst_ProductMaster();
                prodMaster.ProductGroup = ddlGroup.Text;
                prodMaster.ProductCategory = Convert.ToInt32(CategorycomboBox.SelectedValue);
                prodMaster.ProductCode = txtProductName.Text;
                prodMaster.ProductDescription = txtProductDes.Text;
                prodMaster.IsActive = true;
                prodMaster.CreateDate = DateTime.Now;
                prodMaster.CreatedBy = Utility.BA_Commman._userId; ;
                context.Mst_ProductMaster.Add(prodMaster);
                context.SaveChanges();
            }
        }

        private void UpdateProduct(int productId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                Mst_ProductMaster prodMaster = context.Mst_ProductMaster.FirstOrDefault(s => s.ProductId == productId);
                prodMaster.ProductGroup = ddlGroup.Text;
                prodMaster.ProductCategory = Convert.ToInt32(CategorycomboBox.SelectedValue);
                prodMaster.ProductCode = txtProductName.Text;
                prodMaster.ProductDescription = txtProductDes.Text;
                prodMaster.IsActive = true;
                prodMaster.ModifiedDate = DateTime.Now;
                prodMaster.ModifiedBy = Utility.BA_Commman._userId; ;
                context.SaveChanges();
            }
        }

        private void DeleteProduct(int productId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                Mst_ProductMaster prodMaster = context.Mst_ProductMaster.FirstOrDefault(s => s.ProductId == productId);
                prodMaster.IsActive = false;
                prodMaster.ModifiedDate = DateTime.Now;
                prodMaster.ModifiedBy = Utility.BA_Commman._userId; ;
                context.SaveChanges();
            }
        }

        private void BindProductList()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var prodMasterList = (from p in context.Mst_ProductMaster
                                      join c in context.MST_CategoriesMaster on p.ProductCategory equals c.CategoryID
                                      where p.IsActive == true
                                      select new
                                      {
                                          p.ProductId,
                                          p.ProductGroup,
                                          p.ProductCode,
                                          c.CategoryName,
                                          p.ProductDescription
                                      }).ToList();
                dgvProduct.DataSource = prodMasterList;


            }
        }

        private void FillFields(int ProductId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                LoadCategories();
                var prodMaster = (from p in context.Mst_ProductMaster
                                  join c in context.MST_CategoriesMaster on
                                    p.ProductCategory equals c.CategoryID
                                  where p.ProductId == ProductId && p.IsActive == true

                                  select new
                                  {
                                      p.ProductGroup,
                                      p.ProductCode,
                                      p.ProductDescription,
                                      c.CategoryID
                                  }
                                                ).FirstOrDefault();
                ddlGroup.Text = prodMaster.ProductGroup;
                CategorycomboBox.SelectedValue = prodMaster.CategoryID;
                txtProductName.Text = prodMaster.ProductCode;
                txtProductDes.Text = prodMaster.ProductDescription;
            }
        }
        #endregion
        int ProductId = 0;
    }
}
