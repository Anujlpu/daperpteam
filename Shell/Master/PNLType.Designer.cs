﻿namespace Shell
{
    partial class PNLType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpRoleList = new System.Windows.Forms.GroupBox();
            this.dgvPnl = new System.Windows.Forms.DataGridView();
            this.grpManageRole = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.txtPNLDescription = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPnCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grpRoleList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPnl)).BeginInit();
            this.grpManageRole.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpRoleList
            // 
            this.grpRoleList.Controls.Add(this.dgvPnl);
            this.grpRoleList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpRoleList.Location = new System.Drawing.Point(6, 196);
            this.grpRoleList.Name = "grpRoleList";
            this.grpRoleList.Size = new System.Drawing.Size(531, 216);
            this.grpRoleList.TabIndex = 49;
            this.grpRoleList.TabStop = false;
            this.grpRoleList.Text = "PNL  List";
            // 
            // dgvPnl
            // 
            this.dgvPnl.AllowDrop = true;
            this.dgvPnl.AllowUserToAddRows = false;
            this.dgvPnl.AllowUserToDeleteRows = false;
            this.dgvPnl.AllowUserToResizeColumns = false;
            this.dgvPnl.AllowUserToResizeRows = false;
            this.dgvPnl.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPnl.BackgroundColor = System.Drawing.Color.White;
            this.dgvPnl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPnl.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPnl.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPnl.Location = new System.Drawing.Point(3, 16);
            this.dgvPnl.MultiSelect = false;
            this.dgvPnl.Name = "dgvPnl";
            this.dgvPnl.ReadOnly = true;
            this.dgvPnl.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvPnl.RowHeadersVisible = false;
            this.dgvPnl.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPnl.Size = new System.Drawing.Size(525, 197);
            this.dgvPnl.TabIndex = 1;
            this.dgvPnl.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPnl_CellClick);
            // 
            // grpManageRole
            // 
            this.grpManageRole.Controls.Add(this.label18);
            this.grpManageRole.Controls.Add(this.btnExit);
            this.grpManageRole.Controls.Add(this.BtnDelete);
            this.grpManageRole.Controls.Add(this.BtnSave);
            this.grpManageRole.Controls.Add(this.txtPNLDescription);
            this.grpManageRole.Controls.Add(this.label16);
            this.grpManageRole.Controls.Add(this.txtPnCode);
            this.grpManageRole.Controls.Add(this.label1);
            this.grpManageRole.Controls.Add(this.btnUpdate);
            this.grpManageRole.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageRole.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageRole.ForeColor = System.Drawing.Color.Black;
            this.grpManageRole.Location = new System.Drawing.Point(6, 12);
            this.grpManageRole.Name = "grpManageRole";
            this.grpManageRole.Size = new System.Drawing.Size(531, 178);
            this.grpManageRole.TabIndex = 48;
            this.grpManageRole.TabStop = false;
            this.grpManageRole.Text = "PNL Type";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.AliceBlue;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(483, 42);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 15);
            this.label18.TabIndex = 174;
            this.label18.Text = "*";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(364, 138);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 82;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(268, 138);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 44;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(78, 138);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 43;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // txtPNLDescription
            // 
            this.txtPNLDescription.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPNLDescription.ForeColor = System.Drawing.Color.Black;
            this.txtPNLDescription.Location = new System.Drawing.Point(176, 69);
            this.txtPNLDescription.Multiline = true;
            this.txtPNLDescription.Name = "txtPNLDescription";
            this.txtPNLDescription.Size = new System.Drawing.Size(301, 56);
            this.txtPNLDescription.TabIndex = 35;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(10, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 15);
            this.label16.TabIndex = 34;
            this.label16.Text = "PNL Description";
            // 
            // txtPnCode
            // 
            this.txtPnCode.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPnCode.ForeColor = System.Drawing.Color.Black;
            this.txtPnCode.Location = new System.Drawing.Point(176, 36);
            this.txtPnCode.Name = "txtPnCode";
            this.txtPnCode.Size = new System.Drawing.Size(301, 26);
            this.txtPnCode.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(10, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "PNL Name";
            // 
            // btnUpdate
            // 
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUpdate.FlatAppearance.BorderSize = 2;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(172, 138);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(90, 32);
            this.btnUpdate.TabIndex = 45;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // PNLType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(537, 417);
            this.Controls.Add(this.grpRoleList);
            this.Controls.Add(this.grpManageRole);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PNLType";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PNLType";
            this.Load += new System.EventHandler(this.PNLType_Load);
            this.grpRoleList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPnl)).EndInit();
            this.grpManageRole.ResumeLayout(false);
            this.grpManageRole.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpRoleList;
        private System.Windows.Forms.DataGridView dgvPnl;
        private System.Windows.Forms.GroupBox grpManageRole;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.TextBox txtPNLDescription;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPnCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label18;
    }
}