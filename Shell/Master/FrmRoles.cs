﻿using Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class FrmRoles : BaseForm
    {
        public FrmRoles()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                grpRoleList.Visible = false;
                grpManageRole.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            grpRoleList.Visible = true;
            grpManageRole.Visible = false;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Saved!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Updated!");
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Record Deleted!");
        }

        private void FrmRoles_Load(object sender, EventArgs e)
        {
            grpRoleList.Visible = true;
            grpManageRole.Visible = false;
        }


    }
}
