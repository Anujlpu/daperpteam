﻿namespace Shell.Master
{
    partial class SystemColorConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ParentFontStyleTxt = new System.Windows.Forms.TextBox();
            this.Exit = new System.Windows.Forms.Button();
            this.ParentSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.FontSizeTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.FontFamilyTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ChildFontStyleTxt = new System.Windows.Forms.TextBox();
            this.ChildSave = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.ChildFontSizeTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ChildFontFamilyTxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ChildFont = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.BackgroundcolorDialog = new System.Windows.Forms.ColorDialog();
            this.FontcolorDialog = new System.Windows.Forms.ColorDialog();
            this.ParentBgColorCmbBox = new System.Windows.Forms.ComboBox();
            this.ParentFontColorComboBox = new System.Windows.Forms.ComboBox();
            this.ChildFontColorBomboBox = new System.Windows.Forms.ComboBox();
            this.ChildBgColorComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ParentFontColorComboBox);
            this.groupBox1.Controls.Add(this.ParentBgColorCmbBox);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.ParentFontStyleTxt);
            this.groupBox1.Controls.Add(this.Exit);
            this.groupBox1.Controls.Add(this.ParentSave);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.FontSizeTxt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.FontFamilyTxt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(795, 295);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Parent Form Configuration";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 150);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 16);
            this.label11.TabIndex = 17;
            this.label11.Text = "Font Style";
            // 
            // ParentFontStyleTxt
            // 
            this.ParentFontStyleTxt.Enabled = false;
            this.ParentFontStyleTxt.Location = new System.Drawing.Point(177, 144);
            this.ParentFontStyleTxt.Name = "ParentFontStyleTxt";
            this.ParentFontStyleTxt.Size = new System.Drawing.Size(250, 22);
            this.ParentFontStyleTxt.TabIndex = 16;
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(323, 239);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(104, 35);
            this.Exit.TabIndex = 14;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // ParentSave
            // 
            this.ParentSave.Location = new System.Drawing.Point(177, 239);
            this.ParentSave.Name = "ParentSave";
            this.ParentSave.Size = new System.Drawing.Size(140, 35);
            this.ParentSave.TabIndex = 13;
            this.ParentSave.Text = "Save And Re-Login";
            this.ParentSave.UseVisualStyleBackColor = true;
            this.ParentSave.Click += new System.EventHandler(this.ParentSave_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Font Size";
            // 
            // FontSizeTxt
            // 
            this.FontSizeTxt.Enabled = false;
            this.FontSizeTxt.Location = new System.Drawing.Point(177, 200);
            this.FontSizeTxt.Name = "FontSizeTxt";
            this.FontSizeTxt.Size = new System.Drawing.Size(250, 22);
            this.FontSizeTxt.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Font Family";
            // 
            // FontFamilyTxt
            // 
            this.FontFamilyTxt.Enabled = false;
            this.FontFamilyTxt.Location = new System.Drawing.Point(177, 172);
            this.FontFamilyTxt.Name = "FontFamilyTxt";
            this.FontFamilyTxt.Size = new System.Drawing.Size(250, 22);
            this.FontFamilyTxt.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Font Color";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(177, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Font Picker";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Font Configuration";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Background Color";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ChildFontColorBomboBox);
            this.groupBox2.Controls.Add(this.ChildBgColorComboBox);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.ChildFontStyleTxt);
            this.groupBox2.Controls.Add(this.ChildSave);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.ChildFontSizeTxt);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.ChildFontFamilyTxt);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.ChildFont);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 313);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(795, 250);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Child Form Configuration";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(26, 131);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 16);
            this.label12.TabIndex = 29;
            this.label12.Text = "Font Style";
            // 
            // ChildFontStyleTxt
            // 
            this.ChildFontStyleTxt.Enabled = false;
            this.ChildFontStyleTxt.Location = new System.Drawing.Point(177, 125);
            this.ChildFontStyleTxt.Name = "ChildFontStyleTxt";
            this.ChildFontStyleTxt.Size = new System.Drawing.Size(250, 22);
            this.ChildFontStyleTxt.TabIndex = 28;
            // 
            // ChildSave
            // 
            this.ChildSave.Location = new System.Drawing.Point(177, 209);
            this.ChildSave.Name = "ChildSave";
            this.ChildSave.Size = new System.Drawing.Size(140, 35);
            this.ChildSave.TabIndex = 26;
            this.ChildSave.Text = "Save And Re-Login";
            this.ChildSave.UseVisualStyleBackColor = true;
            this.ChildSave.Click += new System.EventHandler(this.ChildSave_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 16);
            this.label6.TabIndex = 25;
            this.label6.Text = "Font Size";
            // 
            // ChildFontSizeTxt
            // 
            this.ChildFontSizeTxt.Enabled = false;
            this.ChildFontSizeTxt.Location = new System.Drawing.Point(177, 181);
            this.ChildFontSizeTxt.Name = "ChildFontSizeTxt";
            this.ChildFontSizeTxt.Size = new System.Drawing.Size(250, 22);
            this.ChildFontSizeTxt.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 159);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 16);
            this.label7.TabIndex = 23;
            this.label7.Text = "Font Family";
            // 
            // ChildFontFamilyTxt
            // 
            this.ChildFontFamilyTxt.Enabled = false;
            this.ChildFontFamilyTxt.Location = new System.Drawing.Point(177, 153);
            this.ChildFontFamilyTxt.Name = "ChildFontFamilyTxt";
            this.ChildFontFamilyTxt.Size = new System.Drawing.Size(250, 22);
            this.ChildFontFamilyTxt.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 16);
            this.label8.TabIndex = 21;
            this.label8.Text = "Font Color";
            // 
            // ChildFont
            // 
            this.ChildFont.Location = new System.Drawing.Point(177, 96);
            this.ChildFont.Name = "ChildFont";
            this.ChildFont.Size = new System.Drawing.Size(119, 23);
            this.ChildFont.TabIndex = 18;
            this.ChildFont.Text = "Font Picker";
            this.ChildFont.UseVisualStyleBackColor = true;
            this.ChildFont.Click += new System.EventHandler(this.ChildFont_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 16);
            this.label9.TabIndex = 16;
            this.label9.Text = "Font Configuration";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 16);
            this.label10.TabIndex = 15;
            this.label10.Text = "Background Color";
            // 
            // ParentBgColorCmbBox
            // 
            this.ParentBgColorCmbBox.FormattingEnabled = true;
            this.ParentBgColorCmbBox.Location = new System.Drawing.Point(177, 41);
            this.ParentBgColorCmbBox.Name = "ParentBgColorCmbBox";
            this.ParentBgColorCmbBox.Size = new System.Drawing.Size(250, 24);
            this.ParentBgColorCmbBox.TabIndex = 18;
            // 
            // ParentFontColorComboBox
            // 
            this.ParentFontColorComboBox.FormattingEnabled = true;
            this.ParentFontColorComboBox.Location = new System.Drawing.Point(177, 74);
            this.ParentFontColorComboBox.Name = "ParentFontColorComboBox";
            this.ParentFontColorComboBox.Size = new System.Drawing.Size(250, 24);
            this.ParentFontColorComboBox.TabIndex = 19;
            // 
            // ChildFontColorBomboBox
            // 
            this.ChildFontColorBomboBox.FormattingEnabled = true;
            this.ChildFontColorBomboBox.Location = new System.Drawing.Point(177, 65);
            this.ChildFontColorBomboBox.Name = "ChildFontColorBomboBox";
            this.ChildFontColorBomboBox.Size = new System.Drawing.Size(250, 24);
            this.ChildFontColorBomboBox.TabIndex = 31;
            // 
            // ChildBgColorComboBox
            // 
            this.ChildBgColorComboBox.FormattingEnabled = true;
            this.ChildBgColorComboBox.Location = new System.Drawing.Point(177, 32);
            this.ChildBgColorComboBox.Name = "ChildBgColorComboBox";
            this.ChildBgColorComboBox.Size = new System.Drawing.Size(250, 24);
            this.ChildBgColorComboBox.TabIndex = 30;
            // 
            // SystemColorConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(874, 563);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "SystemColorConfiguration";
            this.Text = "System Color Configuration";
            this.Load += new System.EventHandler(this.SystemColorConfiguration_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColorDialog BackgroundcolorDialog;
        private System.Windows.Forms.ColorDialog FontcolorDialog;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox FontSizeTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox FontFamilyTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button ParentSave;
        private System.Windows.Forms.Button ChildSave;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ChildFontSizeTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ChildFontFamilyTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button ChildFont;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox ParentFontStyleTxt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox ChildFontStyleTxt;
        private System.Windows.Forms.ComboBox ParentBgColorCmbBox;
        private System.Windows.Forms.ComboBox ParentFontColorComboBox;
        private System.Windows.Forms.ComboBox ChildFontColorBomboBox;
        private System.Windows.Forms.ComboBox ChildBgColorComboBox;
    }
}