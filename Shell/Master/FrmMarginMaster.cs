﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class FrmMarginMaster : BaseForm
    {
        public FrmMarginMaster()
        {
            InitializeComponent();
        }
        #region Private Methods
        private void LoadCategories()
        {
            using (ShellEntities context = new ShellEntities())
            {
               
            }
        }
        private void BindProducts(int categoryId)
        {
            using (ShellEntities context = new ShellEntities())
            {
               
            }
        }
        private void SaveMargin()
        {
            using (ShellEntities context = new ShellEntities())
            {
                Mst_MarginMaster marginMaster = new Mst_MarginMaster();
                marginMaster.CategoryID = Convert.ToInt32(CategoriesCombobox.SelectedValue);
                marginMaster.ProductId = Convert.ToInt64(ProductscomboBox.SelectedValue);
                marginMaster.EffectiveFrom = Convert.ToDateTime(FromDatePicker.Text);
                marginMaster.EffectiveTo = Convert.ToDateTime(ToDatePicker.Text);
                marginMaster.discount = Convert.ToDouble(DiscountTxt.Text);
                marginMaster.MarkUp = Convert.ToDouble(MarkUpTxt.Text);
                marginMaster.OffPerTwoPacks = Convert.ToDouble(OffPerTwoPackTxt.Text);
                marginMaster.Remarks = RemarksTxt.Text;
                marginMaster.BuyingPrice = Convert.ToDouble(BuyingPriceTxt.Text);
                marginMaster.BuyingPricePerPiece = Convert.ToDouble(BuyingPricePerPieceTxt.Text);
                marginMaster.CartonMargin = Convert.ToDouble(CartonMarginTxt.Text);
                marginMaster.MarginOnTwoPackDeal = Convert.ToDouble(MarginOnTwoPackDealTxt.Text);
                marginMaster.MarginOnSellingPrice = Convert.ToDouble(MarginonSellingpriceTxt.Text);
                marginMaster.DiscountedCartonPrice = Convert.ToDouble(DiscountedCartonPriceTxt.Text);
                marginMaster.DiscountedPrice = Convert.ToDouble(DiscountedPriceTxt.Text);
                marginMaster.SellingPrice = Convert.ToDouble(SellingPriceTxt.Text);
                marginMaster.TwoPackDeal = Convert.ToDouble(TwoPackDealTxt.Text);
                marginMaster.RegularCartonPrice = Convert.ToDouble(RegularCartonPriceTxt.Text);
                context.Mst_MarginMaster.Add(marginMaster);
                context.SaveChanges();
                MessageBox.Show("Margin saved.");
            }
        }

        private void UpdateMargin(long marginid)
        {
            using (ShellEntities context = new ShellEntities())
            {
                Mst_MarginMaster marginMaster = context.Mst_MarginMaster.FirstOrDefault(s => s.MarginId == marginid);
                marginMaster.CategoryID = Convert.ToInt32(CategoriesCombobox.SelectedValue);
                marginMaster.ProductId = Convert.ToInt64(ProductscomboBox.SelectedValue);
                marginMaster.EffectiveFrom = Convert.ToDateTime(FromDatePicker.Text);
                marginMaster.EffectiveTo = Convert.ToDateTime(ToDatePicker.Text);
                marginMaster.discount = Convert.ToDouble(DiscountTxt.Text);
                marginMaster.MarkUp = Convert.ToDouble(MarkUpTxt.Text);
                marginMaster.OffPerTwoPacks = Convert.ToDouble(OffPerTwoPackTxt.Text);
                marginMaster.Remarks = RemarksTxt.Text;
                marginMaster.BuyingPrice = Convert.ToDouble(BuyingPriceTxt.Text);
                marginMaster.BuyingPricePerPiece = Convert.ToDouble(BuyingPricePerPieceTxt.Text);
                marginMaster.CartonMargin = Convert.ToDouble(CartonMarginTxt.Text);
                marginMaster.MarginOnTwoPackDeal = Convert.ToDouble(MarginOnTwoPackDealTxt.Text);
                marginMaster.MarginOnSellingPrice = Convert.ToDouble(MarginonSellingpriceTxt.Text);
                marginMaster.DiscountedCartonPrice = Convert.ToDouble(DiscountedCartonPriceTxt.Text);
                marginMaster.DiscountedPrice = Convert.ToDouble(DiscountedPriceTxt.Text);
                marginMaster.SellingPrice = Convert.ToDouble(SellingPriceTxt.Text);
                marginMaster.TwoPackDeal = Convert.ToDouble(TwoPackDealTxt.Text);
                marginMaster.RegularCartonPrice = Convert.ToDouble(RegularCartonPriceTxt.Text);
                context.SaveChanges();
                MessageBox.Show("Margin Updated.");
            }

        }
        private void DeleteMargin(long marginId)
        {

        }

        private void BindMarginList()
        {

        }

        private void FillFields(long marginId)
        {

        }
        private void BindBuyingPrice()
        {
            using (ShellEntities context = new ShellEntities())
            {
                if (CategoriesCombobox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select category !");
                    return;
                }
                if (ProductscomboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select product !");
                    return;
                }

                int categoryId = Convert.ToInt32(CategoriesCombobox.SelectedValue);
                int productId = Convert.ToInt32(ProductscomboBox.SelectedValue);
                var priceMaster = context.MST_PriceMaster.Where(s => s.IsActive == true && s.CategoryID == categoryId && s.ProductId == productId).Select(s => new { s.PriceId, s.Price }).FirstOrDefault();
                BuyingPriceTxt.Text = Convert.ToDouble(priceMaster.Price).ToString("0.##");
            }
        }

        private double DiscountedPrice()
        {
            double discounterPrice = Convert.ToDouble(BuyingPriceTxt.Text) - Convert.ToDouble(DiscountTxt.Text);
            return discounterPrice;
        }
        private double SellingPrice()
        {
            double sellingPrice = (Convert.ToDouble(BuyingPricePerPieceTxt.Text) + (Convert.ToDouble(BuyingPricePerPieceTxt.Text) / 100) * Convert.ToDouble(MarkUpTxt.Text));
            return sellingPrice;
        }
        private double MarginOnSellingPrice()
        {
            double MarginOnSellingPrice = ((SellingPrice() - Convert.ToDouble(BuyingPricePerPieceTxt.Text)) / SellingPrice() * 100);
            return MarginOnSellingPrice;
        }
        private double TwoPacksDeals()
        {
            double twopackDeals = (SellingPrice() * 2) - 1;
            return twopackDeals;
        }
        private double MarginOnTwoPacksDeals()
        {
            double MarginOntwopackDeals = ((TwoPacksDeals() - (Convert.ToDouble(BuyingPricePerPieceTxt.Text) * 2)) / TwoPacksDeals() * 100);
            return MarginOntwopackDeals;
        }
        private double RegularCartonPrice()
        {
            double RegularCartonPrice = (SellingPrice() * 8);
            return RegularCartonPrice;
        }
        private double DiscountedCartonPrice()
        {
            double DiscountedCartonPrice = (RegularCartonPrice() - 4);
            return DiscountedCartonPrice;
        }
        private double CartonMargin()
        {
            double CartonMargin = ((MarginOnTwoPacksDeals() - DiscountedPrice()) / RegularCartonPrice() * 100);
            return CartonMargin;
        }
        private double BuyingPricePerPiece()
        {
            double BuyingPricePerPiece = ((DiscountedPrice() / 8));
            return BuyingPricePerPiece;
        }

        private void AutoCalculate()
        {
            BuyingPricePerPieceTxt.Text = BuyingPricePerPiece().ToString();
            CartonMarginTxt.Text = CartonMargin().ToString();
            MarginOnTwoPackDealTxt.Text = MarginOnTwoPacksDeals().ToString();
            MarginonSellingpriceTxt.Text = MarginOnSellingPrice().ToString();
            DiscountedCartonPriceTxt.Text = DiscountedCartonPrice().ToString();
            DiscountedPriceTxt.Text = DiscountedPrice().ToString();
            SellingPriceTxt.Text = SellingPrice().ToString();
            TwoPackDealTxt.Text = TwoPacksDeals().ToString();
            RegularCartonPriceTxt.Text = RegularCartonPrice().ToString();
        }
        #endregion
        #region Form Events
        private void FrmMarginMaster_Load(object sender, EventArgs e)
        {
            try
            {
                LoadCategories();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void CategorycomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        long MarginId = 0;
        private void dgvProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveMargin();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateMargin(MarginId);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {

        }

        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                BindBuyingPrice();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void CategoriesCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CategoriesCombobox.SelectedIndex > 0)
                {
                    int categoryId = Convert.ToInt32(CategoriesCombobox.SelectedValue);
                    BindProducts(categoryId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }
        private void DiscountTxt_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                AutoCalculate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MarkUpTxt_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                AutoCalculate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DiscountTxt_TextChanged(object sender, EventArgs e)
        {
            
        }

        #endregion

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
