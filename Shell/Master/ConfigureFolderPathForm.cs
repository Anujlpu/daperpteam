﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class ConfigureFolderPathForm : BaseForm
    {
        public ConfigureFolderPathForm()
        {
            InitializeComponent();
        }

        #region Private Methods

        private void GetSystemUsers()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var userList = context.MST_UserMaster.Where(s => s.IsActive == true && s.Userid == Utility.BA_Commman._userId).Select(s => new { s.Userid, s.UserName }).ToList();
                UserComboBox.DataSource = userList;
                UserComboBox.DisplayMember = "UserName"; 
                UserComboBox.ValueMember = "Userid";
            }
        }

        private void FillForm()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var systemFolder = context.TBL_SystemFolder.Where(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId).FirstOrDefault();
                if (systemFolder != null)
                {
                    UploadPathTxt.Text = systemFolder.UploadPathKey;
                    SalaryDocPathtxt.Text = systemFolder.SalaryDocumentPath;
                    ReportPathTxt.Text = systemFolder.ReportExcelPath;
                    LogPathTxt.Text = systemFolder.LogPath;
                }
                else
                {
                    MessageBox.Show("You need to enter system folders.");
                }
            }
        }
        #endregion
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var systemFolder = context.TBL_SystemFolder.Where(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId).FirstOrDefault();
                    if (systemFolder != null)
                    {
                        systemFolder.UploadPathKey = UploadPathTxt.Text;
                        systemFolder.SalaryDocumentPath = SalaryDocPathtxt.Text;
                        systemFolder.ReportExcelPath = ReportPathTxt.Text;
                        systemFolder.LogPath = LogPathTxt.Text;
                        systemFolder.ModifiedBy = Utility.BA_Commman._userId;
                        systemFolder.ModifiedDate = DateTime.Now;
                        systemFolder.IsActive = true;
                        context.SaveChanges();
                        MessageBox.Show("Configuration updated.");
                    }
                    else
                    {
                        TBL_SystemFolder systemFolderObj = new TBL_SystemFolder();
                        systemFolderObj.UploadPathKey = UploadPathTxt.Text;
                        systemFolderObj.SalaryDocumentPath = SalaryDocPathtxt.Text;
                        systemFolderObj.ReportExcelPath = ReportPathTxt.Text;
                        systemFolderObj.LogPath = LogPathTxt.Text;
                        systemFolderObj.UserID = Utility.BA_Commman._userId;
                        systemFolderObj.CreatedBy = Utility.BA_Commman._userId;
                        systemFolderObj.CreatedDate = DateTime.Now;
                        systemFolderObj.IsActive = true;
                        context.TBL_SystemFolder.Add(systemFolderObj);
                        context.SaveChanges();
                        MessageBox.Show("Configuration Saved.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }

        private void ConfigureFolderPathForm_Load(object sender, EventArgs e)
        {
            try
            {
                GetSystemUsers();
                FillForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
