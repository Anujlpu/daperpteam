﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Framework.Data;

namespace Shell.Master
{
    public partial class SummaryPopupForm : BaseForm
    {
        public SummaryPopupForm()
        {
            InitializeComponent();
        }

        public int EODId { get; set; }

        private void BindRemarksDetails()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var summary = (from s in context.TBL_EODRemarks
                                   join p in context.MST_ProfileMaster on s.ProfileId equals p.ProfileId
                                   join u in context.MST_UserMaster on s.CreatedBy equals u.Userid
                                   where s.EODID == EODId && s.IsActive == true
                                   select new
                                   {
                                       s.Remarks,
                                       p.ProfileName,
                                       u.UserName,
                                       s.CreatedDate,
                                       ApprovalStatus = s.ApprovalStatus
                                   }).FirstOrDefault();
                    if (summary != null)
                    {
                        RemarksTxt.Text = summary.Remarks;
                        ApprovedByLabel.Text = summary.UserName;
                        ApprovalRoleLabel.Text = summary.ProfileName;
                        ApprovedDateLabel.Text = summary.CreatedDate.ToString();
                        ApprovalStatusLabel.Text = summary.ApprovalStatus;
                    }
                    else
                    {
                        MessageBox.Show("Remarks detail not available.");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SummaryPopupForm_Load(object sender, EventArgs e)
        {
            BindRemarksDetails();
        }
    }
}
