﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class ClaimPopupForm : BaseForm
    {
        private void ChildColorConfig(Form childForm)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var childConfig = context.TBL_ChilFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                    if (childConfig != null)
                    {
                        childForm.BackColor = Color.FromName(childConfig.BackgroundColor);
                        float fl = (float)childConfig.FontSize;
                        FontStyle fontStyle = (FontStyle)Enum.Parse(typeof(FontStyle), childConfig.FontStyle, true);
                        Font childFont = new Font(childConfig.FontName, fl, fontStyle);
                        childForm.ForeColor = Color.FromName(childConfig.FontColor);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator." + ex.Message);
            }
        }
        public ClaimPopupForm()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void ClaimPopupForm_Load(object sender, EventArgs e)
        {
            ChildColorConfig(this);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ClaimCheckListInstance ClaimCheckListInstance = new ClaimCheckListInstance();
                ClaimCheckListInstance.InvoiceNumber = InvoicenumberTxt.Text;
                ClaimCheckListInstance.InvoiceDate = Convert.ToDateTime(InvoicedatePicker.Text);
                ClaimCheckListInstance.ItemNumber = ClaimItemNumberTxt.Text;
                ClaimCheckListInstance.ItemDescripion = ItemDescriptionTxt.Text;
                ClaimCheckListInstance.NetAmount = float.Parse((NetAmountTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                ClaimCheckListInstance.GST = float.Parse((GSTTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                ClaimCheckListInstance.GrossAmount = float.Parse((GrossAmountTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                ClaimCheckListInstance.ClaimNumber = ClaimNumberTxt.Text;
                ClaimCheckListInstance.FollowUpDate = Convert.ToDateTime(FollowUpDatePicker.Text);
                ClaimCheckListInstance.ClaimStatus = ClaimStatusCmbbox.Text;
                ClaimCheckListInstance.FollowUpRemarks = FollowupRemarksTxt.Text;
                ClaimCheckListInstance.ReceivingAmount = float.Parse((ReceivingAmountTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                ClaimCheckListInstance.Difference = float.Parse((DifferenceTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                ClaimCheckListInstance.DifferenceRemarks = DifferenceRemarksTxt.Text;
                InvoiceAndClaimCheckList.ClaimInvoiceNumber = InvoicenumberTxt.Text;
                InvoiceAndClaimCheckList.ClaimInvoiceDate = Convert.ToDateTime(InvoicedatePicker.Text);
                InvoiceAndClaimCheckList.ItemNumber = ClaimItemNumberTxt.Text;
                InvoiceAndClaimCheckList.ItemDescripion = ItemDescriptionTxt.Text;
                InvoiceAndClaimCheckList.NetAmount = float.Parse((NetAmountTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                InvoiceAndClaimCheckList.GST = float.Parse((GSTTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                InvoiceAndClaimCheckList.GrossAmount = float.Parse((GrossAmountTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                InvoiceAndClaimCheckList.ClaimNumber = ClaimNumberTxt.Text;
                InvoiceAndClaimCheckList.FollowUpDate = Convert.ToDateTime(FollowUpDatePicker.Text);
                InvoiceAndClaimCheckList.ClaimStatus = ClaimStatusCmbbox.Text;
                InvoiceAndClaimCheckList.FollowUpRemarks = FollowupRemarksTxt.Text;
                InvoiceAndClaimCheckList.ReceivingAmount = float.Parse((ReceivingAmountTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                InvoiceAndClaimCheckList.Difference = float.Parse((DifferenceTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                InvoiceAndClaimCheckList.DifferenceRemarks = DifferenceRemarksTxt.Text;
                MessageBox.Show("Data saved as draft.");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
