﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class DashBoardOpenCount : BaseForm
    {
        public DashBoardOpenCount()
        {
            InitializeComponent();
        }
        private void OpenInvoiceCount()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var invoiceDetails = context.Tbl_Invoices.Where(i => i.IsClosed == false && i.IsActive == true && (i.IsApproved == false || i.IsApproved == null)).Select(s => new { s.InvoiceId, s.InvDate, s.InvoiceNo }).ToList();
                    if (invoiceDetails.Count > 0)
                    {
                        dgvOpenInvoices.DataSource = invoiceDetails;
                        dgvOpenInvoices.Columns["InvoiceId"].Visible = false;
                    }

                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }

        }
        private void OpenInvoiceForApproval()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var invoiceDetails = context.Tbl_Invoices.Where(i => i.IsClosed == true && i.IsActive == true && (i.IsApproved == false || i.IsApproved == null)).Select(s => new { s.InvoiceId, s.InvDate, s.InvoiceNo }).ToList();
                    if (invoiceDetails.Count > 0)
                    {
                        // dgvInvoiceApproval.DataSource = invoiceDetails;
                        // dgvInvoiceApproval.Columns["InvoiceId"].Visible = false;
                    }

                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }

        }

        private void OpenPNLCount()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var pnlList = (from m in context.Tbl_PNLEntry
                                   join pn in context.MST_PnlMaster on m.PNLID equals pn.PNLId
                                   where m.IsActive == true && m.IsClosed == false && (m.IsApproved == false || m.IsApproved == null)
                                   select new
                                   {
                                       m.PNLEntryID,
                                       pn.PNLCode,
                                       m.EntryDate

                                   }).ToList();

                    if (pnlList.Count > 0)
                    {
                        // dgvPNLList.DataSource = pnlList;
                        // dgvPNLList.Columns["PNLEntryID"].Visible = false;
                    }

                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }

        }

        private void OpenPNLForApproval()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var pnlList = (from m in context.Tbl_PNLEntry
                                   join pn in context.MST_PnlMaster on m.PNLID equals pn.PNLId
                                   where m.IsActive == true && m.IsClosed == true && (m.IsApproved == false || m.IsApproved == null)
                                   select new
                                   {
                                       m.PNLEntryID,
                                       pn.PNLCode,
                                       m.EntryDate

                                   }).ToList();
                    if (pnlList.Count > 0)
                    {
                        // dgvPNLApproval.DataSource = pnlList;
                        //dgvPNLApproval.Columns["PNLEntryID"].Visible = false;
                    }

                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }

        }
        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void OpenTickets()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var userProfile = context.MST_ProfileMaster.FirstOrDefault(s => s.ProfileCode == Utility.BA_Commman._Profile);
                if (userProfile != null)
                {
                    var openCount = context.TBL_EODRemarks.Where(s => s.ProfileId == userProfile.ProfileId && s.IsActive == true).OrderByDescending(s => s.CreatedDate).ToList();
                    if (Utility.BA_Commman._Profile == "Adm" || Utility.BA_Commman._Profile == "SupAdm")
                    {
                        openCount = context.TBL_EODRemarks.Where(s => s.IsActive == true && s.ApprovalStatus != "").OrderByDescending(s => s.CreatedDate).ToList();

                    }
                    else if (Utility.BA_Commman._Profile == "FE")
                    {
                        openCount = openCount.Where(s => s.ApprovalStatus == ERPEODApprovalStatus.Completed || s.ApprovalStatus == ERPEODApprovalStatus.ReferBack || s.ApprovalStatus == ERPEODApprovalStatus.Reopen || s.ApprovalStatus == ERPEODApprovalStatus.Submitted).ToList();
                    }
                    else if (Utility.BA_Commman._Profile == "BE")
                    {
                        openCount = openCount.Where(s => s.ApprovalStatus == ERPEODApprovalStatus.Completed || s.ApprovalStatus == ERPEODApprovalStatus.ReferBack).ToList();
                    }
                    else if (Utility.BA_Commman._Profile == "Auditor")
                    {
                        openCount = openCount.Where(s => s.ApprovalStatus == ERPEODApprovalStatus.Submitted || s.ApprovalStatus == ERPEODApprovalStatus.Completed || s.ApprovalStatus == ERPEODApprovalStatus.ReferBack).ToList();
                    }
                    var data = openCount.GroupBy(s => new { s.ApprovalStatus, s.CreatedDate }).Select(s => new
                    {
                        // TicketCount = s.Count(),
                        Status = s.Key.ApprovalStatus,
                        Date = s.Key.CreatedDate
                    }).ToList();
                    dgvOpenInvoices.DataSource = data;
                }
            }
        }
        private void DashBoardOpenCount_Load(object sender, EventArgs e)
        {
            try
            {

                OpenTickets();

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string targetURL = @"http://www.aapkaadsindia.com/";
            System.Diagnostics.Process.Start(targetURL);
        }
    }
}
