﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class ProductSerialKeyGenerator : BaseForm
    {
        public ProductSerialKeyGenerator()
        {
            InitializeComponent();
        }

        private void GenerateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var serialKey = new MST_SerialKeys();
                    serialKey.SerialKey = Guid.NewGuid();
                    serialKey.ValidFrom = Convert.ToDateTime(FromDatePicker.Text);
                    serialKey.ValidTill = Convert.ToDateTime(ToDatePicker.Text);
                    serialKey.IsActive = true;
                    serialKey.IsApplied = false;
                    serialKey.CreatedDate = DateTime.Now;
                    serialKey.CreatedBy = Utility.BA_Commman._userId;
                    context.MST_SerialKeys.Add(serialKey);
                    context.SaveChanges();
                    KeyTextBox.Text = serialKey.SerialKey.ToString();
                    MessageBox.Show("New Key Generated");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
