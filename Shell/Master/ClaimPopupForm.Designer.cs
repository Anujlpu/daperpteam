﻿namespace Shell.Master
{
    partial class ClaimPopupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.ClaimStatusCmbbox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.DifferenceRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.DifferenceTxt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ReceivingAmountTxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.FollowupRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.FollowUpDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.ClaimNumberTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.GrossAmountTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.GSTTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NetAmountTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ItemDescriptionTxt = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ClaimItemNumberTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.InvoicedatePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.InvoicenumberTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.BtnSave);
            this.groupBox1.Controls.Add(this.ClaimStatusCmbbox);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.DifferenceRemarksTxt);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.DifferenceTxt);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.ReceivingAmountTxt);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.FollowupRemarksTxt);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.FollowUpDatePicker);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.ClaimNumberTxt);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.GrossAmountTxt);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.GSTTxt);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.NetAmountTxt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ItemDescriptionTxt);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ClaimItemNumberTxt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.InvoicedatePicker);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.InvoicenumberTxt);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(23, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(901, 500);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Claim Check List";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(338, 444);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 27);
            this.btnExit.TabIndex = 174;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial Narrow", 9.25F);
            this.BtnSave.Location = new System.Drawing.Point(233, 445);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 27);
            this.BtnSave.TabIndex = 172;
            this.BtnSave.Text = "Save As";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // ClaimStatusCmbbox
            // 
            this.ClaimStatusCmbbox.FormattingEnabled = true;
            this.ClaimStatusCmbbox.Items.AddRange(new object[] {
            "Open",
            "Processed",
            "Rejected with remarks",
            "Closed",
            "Re-Open"});
            this.ClaimStatusCmbbox.Location = new System.Drawing.Point(234, 340);
            this.ClaimStatusCmbbox.Name = "ClaimStatusCmbbox";
            this.ClaimStatusCmbbox.Size = new System.Drawing.Size(192, 24);
            this.ClaimStatusCmbbox.TabIndex = 110;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(28, 339);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 16);
            this.label13.TabIndex = 109;
            this.label13.Text = "Claim Status";
            // 
            // DifferenceRemarksTxt
            // 
            this.DifferenceRemarksTxt.Location = new System.Drawing.Point(645, 87);
            this.DifferenceRemarksTxt.Name = "DifferenceRemarksTxt";
            this.DifferenceRemarksTxt.Size = new System.Drawing.Size(238, 81);
            this.DifferenceRemarksTxt.TabIndex = 108;
            this.DifferenceRemarksTxt.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(497, 87);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(117, 15);
            this.label12.TabIndex = 106;
            this.label12.Text = "Difference Remarks";
            // 
            // DifferenceTxt
            // 
            this.DifferenceTxt.Location = new System.Drawing.Point(645, 52);
            this.DifferenceTxt.Name = "DifferenceTxt";
            this.DifferenceTxt.Size = new System.Drawing.Size(192, 22);
            this.DifferenceTxt.TabIndex = 105;
            this.DifferenceTxt.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(497, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 15);
            this.label11.TabIndex = 104;
            this.label11.Text = "Difference";
            // 
            // ReceivingAmountTxt
            // 
            this.ReceivingAmountTxt.Location = new System.Drawing.Point(645, 24);
            this.ReceivingAmountTxt.Name = "ReceivingAmountTxt";
            this.ReceivingAmountTxt.Size = new System.Drawing.Size(192, 22);
            this.ReceivingAmountTxt.TabIndex = 103;
            this.ReceivingAmountTxt.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(497, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 15);
            this.label10.TabIndex = 102;
            this.label10.Text = "Receiving Amount";
            // 
            // FollowupRemarksTxt
            // 
            this.FollowupRemarksTxt.Location = new System.Drawing.Point(234, 374);
            this.FollowupRemarksTxt.Name = "FollowupRemarksTxt";
            this.FollowupRemarksTxt.Size = new System.Drawing.Size(259, 64);
            this.FollowupRemarksTxt.TabIndex = 101;
            this.FollowupRemarksTxt.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 374);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 16);
            this.label9.TabIndex = 100;
            this.label9.Text = "Follow Up Remarks";
            // 
            // FollowUpDatePicker
            // 
            this.FollowUpDatePicker.Location = new System.Drawing.Point(234, 311);
            this.FollowUpDatePicker.Name = "FollowUpDatePicker";
            this.FollowUpDatePicker.Size = new System.Drawing.Size(192, 22);
            this.FollowUpDatePicker.TabIndex = 99;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(28, 311);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 15);
            this.label8.TabIndex = 98;
            this.label8.Text = "Follow up Date";
            // 
            // ClaimNumberTxt
            // 
            this.ClaimNumberTxt.Location = new System.Drawing.Point(234, 283);
            this.ClaimNumberTxt.Name = "ClaimNumberTxt";
            this.ClaimNumberTxt.Size = new System.Drawing.Size(192, 22);
            this.ClaimNumberTxt.TabIndex = 97;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 283);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 16);
            this.label7.TabIndex = 96;
            this.label7.Text = "Claim Number";
            // 
            // GrossAmountTxt
            // 
            this.GrossAmountTxt.Location = new System.Drawing.Point(234, 255);
            this.GrossAmountTxt.Name = "GrossAmountTxt";
            this.GrossAmountTxt.Size = new System.Drawing.Size(192, 22);
            this.GrossAmountTxt.TabIndex = 95;
            this.GrossAmountTxt.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 16);
            this.label6.TabIndex = 94;
            this.label6.Text = "Gross Amount";
            // 
            // GSTTxt
            // 
            this.GSTTxt.Location = new System.Drawing.Point(234, 228);
            this.GSTTxt.Name = "GSTTxt";
            this.GSTTxt.Size = new System.Drawing.Size(192, 22);
            this.GSTTxt.TabIndex = 93;
            this.GSTTxt.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 228);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 16);
            this.label5.TabIndex = 92;
            this.label5.Text = "GST";
            // 
            // NetAmountTxt
            // 
            this.NetAmountTxt.Location = new System.Drawing.Point(234, 200);
            this.NetAmountTxt.Name = "NetAmountTxt";
            this.NetAmountTxt.Size = new System.Drawing.Size(192, 22);
            this.NetAmountTxt.TabIndex = 91;
            this.NetAmountTxt.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 16);
            this.label4.TabIndex = 90;
            this.label4.Text = "Net Amount";
            // 
            // ItemDescriptionTxt
            // 
            this.ItemDescriptionTxt.Location = new System.Drawing.Point(234, 130);
            this.ItemDescriptionTxt.Name = "ItemDescriptionTxt";
            this.ItemDescriptionTxt.Size = new System.Drawing.Size(250, 64);
            this.ItemDescriptionTxt.TabIndex = 89;
            this.ItemDescriptionTxt.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 16);
            this.label3.TabIndex = 88;
            this.label3.Text = "Item Description";
            // 
            // ClaimItemNumberTxt
            // 
            this.ClaimItemNumberTxt.Location = new System.Drawing.Point(234, 102);
            this.ClaimItemNumberTxt.Name = "ClaimItemNumberTxt";
            this.ClaimItemNumberTxt.Size = new System.Drawing.Size(192, 22);
            this.ClaimItemNumberTxt.TabIndex = 71;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 16);
            this.label2.TabIndex = 70;
            this.label2.Text = "Item Number";
            // 
            // InvoicedatePicker
            // 
            this.InvoicedatePicker.Location = new System.Drawing.Point(234, 69);
            this.InvoicedatePicker.Name = "InvoicedatePicker";
            this.InvoicedatePicker.Size = new System.Drawing.Size(192, 22);
            this.InvoicedatePicker.TabIndex = 69;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(28, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 68;
            this.label1.Text = "Invoice Date";
            // 
            // InvoicenumberTxt
            // 
            this.InvoicenumberTxt.Location = new System.Drawing.Point(234, 30);
            this.InvoicenumberTxt.Name = "InvoicenumberTxt";
            this.InvoicenumberTxt.Size = new System.Drawing.Size(192, 22);
            this.InvoicenumberTxt.TabIndex = 67;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(28, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 15);
            this.label16.TabIndex = 66;
            this.label16.Text = "Invoice Number";
            // 
            // ClaimPopupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(936, 524);
            this.Controls.Add(this.groupBox1);
            this.Name = "ClaimPopupForm";
            this.Text = "Claim Form";
            this.Load += new System.EventHandler(this.ClaimPopupForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox ClaimItemNumberTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker InvoicedatePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox InvoicenumberTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RichTextBox ItemDescriptionTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NetAmountTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox GSTTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox GrossAmountTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ClaimNumberTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker FollowUpDatePicker;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox FollowupRemarksTxt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ReceivingAmountTxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox DifferenceTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RichTextBox DifferenceRemarksTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox ClaimStatusCmbbox;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button BtnSave;
    }
}