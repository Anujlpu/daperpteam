﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class SystemColorConfiguration : BaseForm
    {
        public SystemColorConfiguration()
        {
            InitializeComponent();
        }

        private void FillParentConfig()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var parentConfiguration = context.TBL_ParentFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                if (parentConfiguration != null)
                {
                    ParentBgColorCmbBox.Text = parentConfiguration.BackgroundColor;
                    ParentFontColorComboBox.Text = parentConfiguration.FontStyle;
                    FontFamilyTxt.Text = parentConfiguration.FontName;
                    FontSizeTxt.Text = parentConfiguration.FontSize.ToString();
                    ParentFontStyleTxt.Text = parentConfiguration.FontStyle.ToString();
                }
            }
        }
        private void FillChildConfig()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var childConfig = context.TBL_ChilFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                if (childConfig != null)
                {
                    ChildBgColorComboBox.Text = childConfig.BackgroundColor;
                    ChildFontColorBomboBox.Text = childConfig.FontStyle;
                    ChildFontFamilyTxt.Text = childConfig.FontName;
                    ChildFontSizeTxt.Text = childConfig.FontSize.ToString();
                    ChildFontStyleTxt.Text = childConfig.FontStyle.ToString();
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            FontDialog fontDlg = new FontDialog();
            fontDlg.ShowDialog();
            fontDlg.ShowColor = true;
            fontDlg.ShowApply = true;
            fontDlg.ShowEffects = true;
            fontDlg.ShowHelp = true;

            // Update the text box color if the user clicks OK 
            if (fontDlg.ShowDialog() == DialogResult.OK)
                ParentFontStyleTxt.Text = fontDlg.Font.Style.ToString();
            FontFamilyTxt.Text = fontDlg.Font.FontFamily.Name;
            FontSizeTxt.Text = fontDlg.Font.Size.ToString();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void ChildFont_Click(object sender, EventArgs e)
        {
            FontDialog fontDlg = new FontDialog();
            fontDlg.ShowDialog();
            fontDlg.ShowColor = true;
            fontDlg.ShowApply = true;
            fontDlg.ShowEffects = true;
            fontDlg.ShowHelp = true;

            if (fontDlg.ShowDialog() == DialogResult.OK)
                ChildFontStyleTxt.Text = fontDlg.Font.Style.ToString();
            ChildFontFamilyTxt.Text = fontDlg.Font.FontFamily.Name;
            ChildFontSizeTxt.Text = fontDlg.Font.Size.ToString();
        }

        private void ParentSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var parentConfiguration = context.TBL_ParentFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                    if (parentConfiguration != null)
                    {
                        parentConfiguration.BackgroundColor = ParentBgColorCmbBox.Text;
                        parentConfiguration.FontStyle = ParentFontStyleTxt.Text;
                        parentConfiguration.FontName = FontFamilyTxt.Text;
                        parentConfiguration.FontSize = Convert.ToDouble(FontSizeTxt.Text);
                        parentConfiguration.ModifiedBy = Utility.BA_Commman._userId;
                        parentConfiguration.FontColor = ParentFontColorComboBox.Text;
                        parentConfiguration.ModifiedDate = DateTime.Now;
                        parentConfiguration.IsActive = true;
                        context.SaveChanges();
                        MessageBox.Show("Parent Configuration updated.");
                    }
                    else
                    {
                        TBL_ParentFormColorFontSettings parentConfig = new TBL_ParentFormColorFontSettings();
                        parentConfig.BackgroundColor = ParentBgColorCmbBox.Text;
                        parentConfig.FontStyle = ParentFontStyleTxt.Text;
                        parentConfig.FontName = FontFamilyTxt.Text;
                        parentConfig.FontSize = Convert.ToDouble(FontSizeTxt.Text);
                        parentConfig.UserID = Utility.BA_Commman._userId;
                        parentConfig.CreatedBy = Utility.BA_Commman._userId;
                        parentConfig.CreatedDate = DateTime.Now;
                        parentConfig.FontColor = ParentFontColorComboBox.Text;
                        parentConfig.IsActive = true;
                        context.TBL_ParentFormColorFontSettings.Add(parentConfig);
                        context.SaveChanges();
                        MessageBox.Show("Parent Configuration Saved.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator." + ex.Message);
            }
        }

        private void ChildSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var childConfiguration = context.TBL_ChilFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                    if (childConfiguration != null)
                    {
                        childConfiguration.BackgroundColor = ChildBgColorComboBox.Text;
                        childConfiguration.FontStyle = ChildFontStyleTxt.Text;
                        childConfiguration.FontName = FontFamilyTxt.Text;
                        childConfiguration.FontSize = Convert.ToDouble(FontSizeTxt.Text);
                        childConfiguration.ModifiedBy = Utility.BA_Commman._userId;
                        childConfiguration.ModifiedDate = DateTime.Now;
                        childConfiguration.FontColor = ChildFontColorBomboBox.Text;
                        childConfiguration.IsActive = true;
                        context.SaveChanges();
                        MessageBox.Show("Child Configuration updated.");
                    }
                    else
                    {
                        TBL_ChilFormColorFontSettings childConfig = new TBL_ChilFormColorFontSettings();
                        childConfig.BackgroundColor = ParentBgColorCmbBox.Text;
                        childConfig.FontStyle = ChildFontStyleTxt.Text;
                        childConfig.FontName = FontFamilyTxt.Text;
                        childConfig.FontSize = Convert.ToDouble(FontSizeTxt.Text);
                        childConfig.UserID = Utility.BA_Commman._userId;
                        childConfig.CreatedBy = Utility.BA_Commman._userId;
                        childConfig.CreatedDate = DateTime.Now;
                        childConfig.IsActive = true;
                        childConfig.FontColor = ChildFontColorBomboBox.Text;
                        context.TBL_ChilFormColorFontSettings.Add(childConfig);
                        context.SaveChanges();
                        MessageBox.Show("Child Configuration Saved.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator." + ex.Message);
            }
        }

        private void SystemColorConfiguration_Load(object sender, EventArgs e)
        {
            try
            {
                BindListOfColors();
                FillParentConfig();
                FillChildConfig();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BindListOfColors()
        {

            foreach (System.Reflection.PropertyInfo prop in typeof(Color).GetProperties())
            {
                if (prop.PropertyType.FullName == "System.Drawing.Color")
                {
                    ParentBgColorCmbBox.Items.Add(prop.Name);
                    ParentFontColorComboBox.Items.Add(prop.Name);
                    ChildBgColorComboBox.Items.Add(prop.Name);
                    ChildFontColorBomboBox.Items.Add(prop.Name);
                }
            }
        }



        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void FontColorTxt_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
