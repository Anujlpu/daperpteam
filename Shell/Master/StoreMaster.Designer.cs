﻿namespace Shell
{
    partial class StoreMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpStoreList = new System.Windows.Forms.GroupBox();
            this.dgvStore = new System.Windows.Forms.DataGridView();
            this.grpManageStore = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtStoreName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFaxNo = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtEmailId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPhoneNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtStorreBrand = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNoofContractor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNoofEmp = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ddlStoreLocation = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtStoreAddress = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtOutletNo = new System.Windows.Forms.TextBox();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.grpStoreList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStore)).BeginInit();
            this.grpManageStore.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpStoreList
            // 
            this.grpStoreList.Controls.Add(this.dgvStore);
            this.grpStoreList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpStoreList.Location = new System.Drawing.Point(4, 296);
            this.grpStoreList.Name = "grpStoreList";
            this.grpStoreList.Size = new System.Drawing.Size(584, 245);
            this.grpStoreList.TabIndex = 41;
            this.grpStoreList.TabStop = false;
            this.grpStoreList.Text = "Store List";
            // 
            // dgvStore
            // 
            this.dgvStore.AllowDrop = true;
            this.dgvStore.AllowUserToAddRows = false;
            this.dgvStore.AllowUserToDeleteRows = false;
            this.dgvStore.AllowUserToResizeColumns = false;
            this.dgvStore.AllowUserToResizeRows = false;
            this.dgvStore.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvStore.BackgroundColor = System.Drawing.Color.White;
            this.dgvStore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.NullValue = null;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvStore.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvStore.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvStore.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvStore.Location = new System.Drawing.Point(3, 16);
            this.dgvStore.MultiSelect = false;
            this.dgvStore.Name = "dgvStore";
            this.dgvStore.ReadOnly = true;
            this.dgvStore.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvStore.RowHeadersVisible = false;
            this.dgvStore.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvStore.Size = new System.Drawing.Size(578, 226);
            this.dgvStore.TabIndex = 17;
            this.dgvStore.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStore_CellContentClick);
            // 
            // grpManageStore
            // 
            this.grpManageStore.Controls.Add(this.ClearBtn);
            this.grpManageStore.Controls.Add(this.label14);
            this.grpManageStore.Controls.Add(this.label12);
            this.grpManageStore.Controls.Add(this.label11);
            this.grpManageStore.Controls.Add(this.txtStoreName);
            this.grpManageStore.Controls.Add(this.label10);
            this.grpManageStore.Controls.Add(this.label9);
            this.grpManageStore.Controls.Add(this.txtFaxNo);
            this.grpManageStore.Controls.Add(this.btnExit);
            this.grpManageStore.Controls.Add(this.label8);
            this.grpManageStore.Controls.Add(this.txtEmailId);
            this.grpManageStore.Controls.Add(this.label6);
            this.grpManageStore.Controls.Add(this.txtPhoneNo);
            this.grpManageStore.Controls.Add(this.label7);
            this.grpManageStore.Controls.Add(this.BtnDelete);
            this.grpManageStore.Controls.Add(this.btnSave);
            this.grpManageStore.Controls.Add(this.btnUpdate);
            this.grpManageStore.Controls.Add(this.txtStorreBrand);
            this.grpManageStore.Controls.Add(this.label5);
            this.grpManageStore.Controls.Add(this.txtNoofContractor);
            this.grpManageStore.Controls.Add(this.label4);
            this.grpManageStore.Controls.Add(this.txtNoofEmp);
            this.grpManageStore.Controls.Add(this.label3);
            this.grpManageStore.Controls.Add(this.ddlStoreLocation);
            this.grpManageStore.Controls.Add(this.label2);
            this.grpManageStore.Controls.Add(this.txtStoreAddress);
            this.grpManageStore.Controls.Add(this.label16);
            this.grpManageStore.Controls.Add(this.txtOutletNo);
            this.grpManageStore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageStore.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageStore.ForeColor = System.Drawing.Color.Black;
            this.grpManageStore.Location = new System.Drawing.Point(4, 3);
            this.grpManageStore.Name = "grpManageStore";
            this.grpManageStore.Size = new System.Drawing.Size(584, 287);
            this.grpManageStore.TabIndex = 0;
            this.grpManageStore.TabStop = false;
            this.grpManageStore.Text = "Manage Store";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.AliceBlue;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(543, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(12, 15);
            this.label14.TabIndex = 83;
            this.label14.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.AliceBlue;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(378, 119);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(12, 15);
            this.label12.TabIndex = 78;
            this.label12.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.AliceBlue;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(317, 53);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(12, 15);
            this.label11.TabIndex = 77;
            this.label11.Text = "*";
            // 
            // txtStoreName
            // 
            this.txtStoreName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStoreName.ForeColor = System.Drawing.Color.Black;
            this.txtStoreName.Location = new System.Drawing.Point(408, 18);
            this.txtStoreName.MaxLength = 50;
            this.txtStoreName.Name = "txtStoreName";
            this.txtStoreName.ReadOnly = true;
            this.txtStoreName.Size = new System.Drawing.Size(131, 21);
            this.txtStoreName.TabIndex = 2;
            this.txtStoreName.TextChanged += new System.EventHandler(this.txtStoreName_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.AliceBlue;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(322, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 15);
            this.label10.TabIndex = 57;
            this.label10.Text = "Store Name";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.AliceBlue;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(14, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 15);
            this.label9.TabIndex = 55;
            this.label9.Text = "Outlet No";
            // 
            // txtFaxNo
            // 
            this.txtFaxNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFaxNo.ForeColor = System.Drawing.Color.Black;
            this.txtFaxNo.Location = new System.Drawing.Point(424, 179);
            this.txtFaxNo.MaxLength = 15;
            this.txtFaxNo.Name = "txtFaxNo";
            this.txtFaxNo.Size = new System.Drawing.Size(131, 21);
            this.txtFaxNo.TabIndex = 10;
            this.txtFaxNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CtrlKeyPress);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(456, 249);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.AliceBlue;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(317, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 15);
            this.label8.TabIndex = 53;
            this.label8.Text = "Fax No.";
            // 
            // txtEmailId
            // 
            this.txtEmailId.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailId.ForeColor = System.Drawing.Color.Black;
            this.txtEmailId.Location = new System.Drawing.Point(424, 149);
            this.txtEmailId.Name = "txtEmailId";
            this.txtEmailId.Size = new System.Drawing.Size(131, 21);
            this.txtEmailId.TabIndex = 8;
            this.txtEmailId.Leave += new System.EventHandler(this.txtEmailId_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(317, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 15);
            this.label6.TabIndex = 51;
            this.label6.Text = "Email Id";
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhoneNo.ForeColor = System.Drawing.Color.Black;
            this.txtPhoneNo.Location = new System.Drawing.Point(424, 119);
            this.txtPhoneNo.MaxLength = 15;
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.Size = new System.Drawing.Size(131, 21);
            this.txtPhoneNo.TabIndex = 6;
            this.txtPhoneNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CtrlKeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.AliceBlue;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(317, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 49;
            this.label7.Text = "Phone No";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(266, 249);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 15;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnSave.FlatAppearance.BorderSize = 2;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(76, 249);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 32);
            this.btnSave.TabIndex = 13;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUpdate.FlatAppearance.BorderSize = 2;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(170, 249);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(90, 32);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtStorreBrand
            // 
            this.txtStorreBrand.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStorreBrand.ForeColor = System.Drawing.Color.Black;
            this.txtStorreBrand.Location = new System.Drawing.Point(180, 176);
            this.txtStorreBrand.MaxLength = 3;
            this.txtStorreBrand.Name = "txtStorreBrand";
            this.txtStorreBrand.Size = new System.Drawing.Size(131, 21);
            this.txtStorreBrand.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(14, 179);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 15);
            this.label5.TabIndex = 42;
            this.label5.Text = "Store Brand";
            // 
            // txtNoofContractor
            // 
            this.txtNoofContractor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoofContractor.ForeColor = System.Drawing.Color.Black;
            this.txtNoofContractor.Location = new System.Drawing.Point(180, 151);
            this.txtNoofContractor.MaxLength = 3;
            this.txtNoofContractor.Name = "txtNoofContractor";
            this.txtNoofContractor.Size = new System.Drawing.Size(131, 21);
            this.txtNoofContractor.TabIndex = 7;
            this.txtNoofContractor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CtrlKeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AliceBlue;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(14, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 15);
            this.label4.TabIndex = 40;
            this.label4.Text = "NumberOfContractors";
            // 
            // txtNoofEmp
            // 
            this.txtNoofEmp.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoofEmp.ForeColor = System.Drawing.Color.Black;
            this.txtNoofEmp.Location = new System.Drawing.Point(180, 119);
            this.txtNoofEmp.MaxLength = 3;
            this.txtNoofEmp.Name = "txtNoofEmp";
            this.txtNoofEmp.Size = new System.Drawing.Size(131, 21);
            this.txtNoofEmp.TabIndex = 5;
            this.txtNoofEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CtrlKeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(14, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 15);
            this.label3.TabIndex = 38;
            this.label3.Text = "NumberOfEmployees";
            // 
            // ddlStoreLocation
            // 
            this.ddlStoreLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlStoreLocation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlStoreLocation.FormattingEnabled = true;
            this.ddlStoreLocation.Location = new System.Drawing.Point(181, 16);
            this.ddlStoreLocation.Name = "ddlStoreLocation";
            this.ddlStoreLocation.Size = new System.Drawing.Size(131, 23);
            this.ddlStoreLocation.TabIndex = 1;
            this.ddlStoreLocation.SelectedIndexChanged += new System.EventHandler(this.ddlStoreLocation_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(15, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 36;
            this.label2.Text = "Store Location";
            // 
            // txtStoreAddress
            // 
            this.txtStoreAddress.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStoreAddress.ForeColor = System.Drawing.Color.Black;
            this.txtStoreAddress.Location = new System.Drawing.Point(180, 74);
            this.txtStoreAddress.MaxLength = 150;
            this.txtStoreAddress.Multiline = true;
            this.txtStoreAddress.Name = "txtStoreAddress";
            this.txtStoreAddress.ReadOnly = true;
            this.txtStoreAddress.Size = new System.Drawing.Size(301, 38);
            this.txtStoreAddress.TabIndex = 4;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(14, 97);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 15);
            this.label16.TabIndex = 34;
            this.label16.Text = "Store Address";
            // 
            // txtOutletNo
            // 
            this.txtOutletNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOutletNo.ForeColor = System.Drawing.Color.Black;
            this.txtOutletNo.Location = new System.Drawing.Point(180, 47);
            this.txtOutletNo.Name = "txtOutletNo";
            this.txtOutletNo.Size = new System.Drawing.Size(131, 21);
            this.txtOutletNo.TabIndex = 3;
            // 
            // ClearBtn
            // 
            this.ClearBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ClearBtn.FlatAppearance.BorderSize = 2;
            this.ClearBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearBtn.Location = new System.Drawing.Point(362, 249);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(88, 32);
            this.ClearBtn.TabIndex = 84;
            this.ClearBtn.Text = "&Clear";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // StoreMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(593, 553);
            this.Controls.Add(this.grpManageStore);
            this.Controls.Add(this.grpStoreList);
            this.Name = "StoreMaster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "StoreMaster";
            this.Load += new System.EventHandler(this.StoreMaster_Load);
            this.grpStoreList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStore)).EndInit();
            this.grpManageStore.ResumeLayout(false);
            this.grpManageStore.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpStoreList;
        private System.Windows.Forms.DataGridView dgvStore;
        private System.Windows.Forms.GroupBox grpManageStore;
        private System.Windows.Forms.TextBox txtStoreAddress;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtOutletNo;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ComboBox ddlStoreLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNoofContractor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNoofEmp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtStorreBrand;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtFaxNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtEmailId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPhoneNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtStoreName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button ClearBtn;
    }
}