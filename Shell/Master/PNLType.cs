﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using Framework;

namespace Shell
{
    public partial class PNLType : BaseForm
    {
        public PNLType()
        {
            InitializeComponent();
        }

        private void PNLType_Load(object sender, EventArgs e)
        {
            ClearValue();
        }
        public void BindGrid()
        {
            try
            {
                BAL_PNL _ObjPNL = new BAL_PNL();
                dgvPnl.DataSource = _ObjPNL.GetPNLlist();
                dgvPnl.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }

        }

        public int _PNLID;

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtPnCode.Text))
                {
                    MessageBox.Show("Please Enter the location name "); txtPnCode.Focus();
                    return;
                }
                BAL_PNL _obj = new BAL_PNL();
                int Result = _obj.DeletePNL(_PNLID, Utility.BA_Commman._userId);
                if (Result == 1)
                {
                    MessageBox.Show("Deleted Sucessfully");
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtPnCode.Text))
                {
                    MessageBox.Show("Please Enter the location Code "); txtPnCode.Focus();
                    return;
                }

                BAL_PNL _obj = new BAL_PNL();
                int Result = _obj.ManagePNL(_PNLID, txtPnCode.Text, txtPNLDescription.Text, Utility.BA_Commman._userId, 2);
                if (Result == 1)
                {
                    MessageBox.Show("Updated Sucessfully");
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void ClearValue()
        {
            txtPnCode.Text = "";
            txtPNLDescription.Text = "";
            BindGrid();
            btnUpdate.Enabled = false;
            BtnDelete.Enabled = false;
            BtnSave.Enabled = true;
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtPnCode.Text))
                {
                    MessageBox.Show("Please Enter the PNL name "); txtPnCode.Focus();
                    return;
                }
                BAL_PNL _ObjPNL = new BAL_PNL();
                int Result = _ObjPNL.ManagePNL(0, txtPnCode.Text, txtPNLDescription.Text, Utility.BA_Commman._userId, 1);
                if (Result == 1)
                {
                    MessageBox.Show("PNL Type Created Sucessfully");
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void dgvPnl_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    DialogResult dg = MessageBox.Show("Do You Want to Update/Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dg == DialogResult.Yes)
                    {
                        _PNLID = int.Parse(dgvPnl.Rows[e.RowIndex].Cells["PNLID"].Value.ToString());
                        txtPnCode.Text = dgvPnl.Rows[e.RowIndex].Cells["PNLCode"].Value.ToString();
                        txtPNLDescription.Text = dgvPnl.Rows[e.RowIndex].Cells["PNLDescription"].Value.ToString();
                        btnUpdate.Enabled = true;
                        BtnDelete.Enabled = true;
                        BtnSave.Enabled = false;
                    }
                    else
                    {
                        btnUpdate.Enabled = false;
                        BtnDelete.Enabled = false;
                        BtnSave.Enabled = true;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
    }
}
