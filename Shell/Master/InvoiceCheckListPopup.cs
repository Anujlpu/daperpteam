﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class InvoiceCheckListPopup : BaseForm
    {
        #region Private Method
        public InvoiceCheckListPopup()
        {
            InitializeComponent();
        }
        private void ChildColorConfig(Form childForm)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var childConfig = context.TBL_ChilFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                    if (childConfig != null)
                    {
                        childForm.BackColor = Color.FromName(childConfig.BackgroundColor);
                        float fl = (float)childConfig.FontSize;
                        FontStyle fontStyle = (FontStyle)Enum.Parse(typeof(FontStyle), childConfig.FontStyle, true);
                        Font childFont = new Font(childConfig.FontName, fl, fontStyle);
                        childForm.ForeColor = Color.FromName(childConfig.FontColor);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator." + ex.Message);
            }
        }
        private bool HasReceivingSignature()
        {
            if (SignatureRadioBtnYes.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool HasVerifiedSignature()
        {
            if (VerifedyesBtn.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool AnyShortage()
        {
            if (YesShortageBtn.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool IsShortageClaimed()
        {
            if (YesClainedBtn.Checked)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Form Methods
        private void ReceivedByTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void InvoicedatePicker_ValueChanged(object sender, EventArgs e)
        {

        }

        private void InvoicenumberTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void InvoiceCheckListPopup_Load(object sender, EventArgs e)
        {
            ChildColorConfig(this);
        }

        private void YesClainedBtn_CheckedChanged(object sender, EventArgs e)
        {
            ClaimPopupForm claimForm = new ClaimPopupForm();
            if (YesClainedBtn.Checked && NoClaimedBtn.Checked == false)
            {
                ClaimRemarksLabel.Visible = false;
                ClaimNoRemakrsTxt.Visible = false;
                claimForm.Show();

            }
            else
            {
                ClaimRemarksLabel.Visible = true;
                ClaimNoRemakrsTxt.Visible = true;
                claimForm.Close();
            }
        }

        private void NoClaimedBtn_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void YesShortageBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (YesShortageBtn.Checked)
            {
                ShortageClaimedLabel.Visible = true;
                ShortageClaimedPanel.Visible = true;
            }
            else
            {
                ShortageClaimedLabel.Visible = false;
                ShortageClaimedPanel.Visible = false;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                InvoiceCheckListInstance InvoiceCheckListInstance = new InvoiceCheckListInstance();
                InvoiceCheckListInstance.InvoiceNumber = InvoicenumberTxt.Text;
                InvoiceCheckListInstance.InvoiceDate = Convert.ToDateTime(InvoicedatePicker.Text);
                InvoiceCheckListInstance.ReceivedBy = ReceivedByTxt.Text;
                InvoiceCheckListInstance.HasReceivingSignature = HasReceivingSignature();
                InvoiceCheckListInstance.WhoVerified = WhoVerifiedTxt.Text;
                InvoiceCheckListInstance.HasVerifiedSignature = HasVerifiedSignature();
                InvoiceCheckListInstance.AnyShortage = AnyShortage();
                InvoiceCheckListInstance.AnyShortageClaimed = IsShortageClaimed();
                InvoiceCheckListInstance.ClaimRemarks = ClaimNoRemakrsTxt.Text;

                InvoiceAndClaimCheckList.InvoiceNumber = InvoicenumberTxt.Text;
                InvoiceAndClaimCheckList.InvoiceDate = Convert.ToDateTime(InvoicedatePicker.Text);
                InvoiceAndClaimCheckList.ReceivedBy = ReceivedByTxt.Text;
                InvoiceAndClaimCheckList.HasReceivingSignature = HasReceivingSignature();
                InvoiceAndClaimCheckList.WhoVerified = WhoVerifiedTxt.Text;
                InvoiceAndClaimCheckList.HasVerifiedSignature = HasVerifiedSignature();
                InvoiceAndClaimCheckList.AnyShortage = AnyShortage();
                InvoiceAndClaimCheckList.AnyShortageClaimed = IsShortageClaimed();
                InvoiceAndClaimCheckList.ClaimRemarks = ClaimNoRemakrsTxt.Text;
                MessageBox.Show("Data saved as Draft.");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
