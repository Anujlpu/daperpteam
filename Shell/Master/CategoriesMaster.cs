﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Framework.Data;
using Framework;

namespace Shell.Master
{
    public partial class CategoriesMaster : BaseForm
    {
        public CategoriesMaster()
        {
            InitializeComponent();
        }

        #region Form Events
        private void CategoriesMaster_Load(object sender, EventArgs e)
        {
            try
            {
                BindCategoryGrid();
                BtnSave.Enabled = true;
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CategoriesdataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void SubCategoryNametextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void CategoryDescription_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteCategory(CategoryId);
                MessageBox.Show("Data Deleted.");
                BindCategoryGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveCategory();
                MessageBox.Show("Data Saved.");
                BindCategoryGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateCategory(CategoryId);
                MessageBox.Show("Data Updated.");
                BindCategoryGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private int CategoryId = 0;
        private void CategoriesdataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.CategoriesdataGridView.Rows[e.RowIndex];
                    CategoryId = Convert.ToInt32(row.Cells["CategoryID"].Value);
                    BtnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    BtnDelete.Enabled = true;
                    FillFields(CategoryId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }

        }
        private void CategoryNamtextBox_TextChanged(object sender, EventArgs e)
        {

        }
        #endregion

        #region Private Methods
        private void FillFields(int categoryId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                MST_CategoriesMaster categorymst = context.MST_CategoriesMaster.FirstOrDefault(s => s.CategoryID == categoryId);
                CategoryNamtextBox.Text = categorymst.CategoryName;
                SubCategoryNametextBox.Text = categorymst.SubCategory;
                CategoryDescription.Text = categorymst.CategoryDescription;
            }
        }
        private void SaveCategory()
        {
            using (ShellEntities context = new ShellEntities())
            {
                MST_CategoriesMaster categorymst = new MST_CategoriesMaster();
                categorymst.CategoryName = CategoryNamtextBox.Text;
                categorymst.SubCategory = SubCategoryNametextBox.Text;
                categorymst.CategoryDescription = CategoryDescription.Text;
                categorymst.IsActive = true;
                categorymst.CreatedBy= Utility.BA_Commman._userId;
                categorymst.CreatedDate = DateTime.Now;
                context.MST_CategoriesMaster.Add(categorymst);
                context.SaveChanges();
                BindCategoryGrid();
            }
        }

        private void UpdateCategory(int categoryId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                MST_CategoriesMaster categorymst = context.MST_CategoriesMaster.FirstOrDefault(s => s.CategoryID == categoryId);
                categorymst.CategoryName = CategoryNamtextBox.Text;
                categorymst.SubCategory = SubCategoryNametextBox.Text;
                categorymst.CategoryDescription = CategoryDescription.Text;
                
                categorymst.IsActive = true;
                context.SaveChanges();
                BindCategoryGrid();
            }
        }

        private void DeleteCategory(int categoryId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                MST_CategoriesMaster categorymst = context.MST_CategoriesMaster.FirstOrDefault(s => s.CategoryID == categoryId);
                categorymst.IsActive = false;
                context.SaveChanges();
                BindCategoryGrid();
            }
        }

        private void BindCategoryGrid()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var categorymstList = context.MST_CategoriesMaster.Where(s => s.IsActive == true).Select(s => new { s.CategoryID, s.CategoryName, s.SubCategory, s.CategoryDescription }).ToList();
                CategoriesdataGridView.DataSource = categorymstList;
            }
        }

        #endregion

    }
}
