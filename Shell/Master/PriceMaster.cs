﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class FrmPriceMaster : BaseForm
    {
        public FrmPriceMaster()
        {
            InitializeComponent();
        }
        #region Private Methods
        private void LoadCategories()
        {
            using (ShellEntities context = new ShellEntities())
            {
                CategorycomboBox.ValueMember = "CategoryID";
                CategorycomboBox.DisplayMember = "CategoryName";
                var categorymstList = context.MST_CategoriesMaster.Where(s => s.IsActive == true).Select(s => new { s.CategoryID, s.CategoryName }).ToList();
                categorymstList.Insert(0, new { CategoryID = 0, CategoryName = "--Select--" });
                CategorycomboBox.DataSource = categorymstList;
            }
        }
        private void BindProducts(int categoryId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                ddlProduct.ValueMember = "ProductId";
                ddlProduct.DisplayMember = "ProductCode";
                var ProductMaster = context.Mst_ProductMaster.Where(s => s.IsActive == true && s.ProductCategory == categoryId).Select(s => new { s.ProductId, s.ProductCode }).ToList();
                ddlProduct.DataSource = ProductMaster;
            }
        }
        private void SavePrice()
        {
            using (ShellEntities context = new ShellEntities())
            {
                MST_PriceMaster pricemaster = new MST_PriceMaster();
                pricemaster.CategoryID = Convert.ToInt32(CategorycomboBox.SelectedValue);
                pricemaster.ProductId = Convert.ToInt32(ddlProduct.SelectedValue);
                pricemaster.EffectiveFrom = Convert.ToDateTime(dtpFrom.Text);
                pricemaster.EffectiveTo = Convert.ToDateTime(dateTo.Text);
                //pricemaster.Des = txtPriceDesc.Text;
                pricemaster.IsActive = true;
                pricemaster.CreateDate = DateTime.Now;
                pricemaster.CreatedBy = Utility.BA_Commman._userId; ;
                pricemaster.Price = Convert.ToDouble(txtProductPrice.Text);
                pricemaster.Size= Convert.ToDouble(SizeTxt.Text);
                pricemaster.Quantity= Convert.ToDouble(QuantityTxt.Text);
                pricemaster.Weight= Convert.ToDouble(WeightTxt.Text);
                context.MST_PriceMaster.Add(pricemaster);
                context.SaveChanges();
            }
        }

        private void UpdatePrice(long priceId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                MST_PriceMaster pricemaster = context.MST_PriceMaster.FirstOrDefault(s => s.PriceId == priceId);
                pricemaster.CategoryID = Convert.ToInt32(CategorycomboBox.SelectedValue);
                pricemaster.ProductId = Convert.ToInt32(ddlProduct.SelectedValue);
                pricemaster.EffectiveFrom = Convert.ToDateTime(dtpFrom.Text);
                pricemaster.EffectiveTo = Convert.ToDateTime(dateTo.Text);
                //pricemaster.Des = txtPriceDesc.Text;
                pricemaster.IsActive = true;
                pricemaster.ModifiedDate = DateTime.Now;
                pricemaster.ModifiedBy = Utility.BA_Commman._userId; ;
                pricemaster.Price = Convert.ToDouble(txtProductPrice.Text);
                pricemaster.Size = Convert.ToDouble(SizeTxt.Text);
                pricemaster.Quantity = Convert.ToDouble(QuantityTxt.Text);
                pricemaster.Weight = Convert.ToDouble(WeightTxt.Text);
                context.SaveChanges();
            }
        }

        private void DeletePrice(long priceId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                MST_PriceMaster prodMaster = context.MST_PriceMaster.FirstOrDefault(s => s.PriceId == priceId);
                prodMaster.IsActive = false;
                prodMaster.ModifiedDate = DateTime.Now;
                prodMaster.ModifiedBy = Utility.BA_Commman._userId; ;
                context.SaveChanges();
            }
        }

        private void BindPriceList()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var priceMasterList = (from p in context.MST_PriceMaster
                                       join c in context.Mst_ProductMaster on p.ProductId equals c.ProductId
                                       join ca in context.MST_CategoriesMaster on p.CategoryID equals ca.CategoryID
                                       where p.IsActive == true
                                       select new
                                       {
                                           p.PriceId,
                                           p.EffectiveFrom,
                                           p.EffectiveTo,
                                           ca.CategoryName,
                                           c.ProductCode,
                                           p.Price
                                       }).ToList();
                dgvProduct.DataSource = priceMasterList;


            }
        }

        private void FillFields(long priceid)
        {
            using (ShellEntities context = new ShellEntities())
            {
                LoadCategories();
                //BindProducts();
                MST_PriceMaster priceMaster = context.MST_PriceMaster.FirstOrDefault(s => s.PriceId == priceid);
                CategorycomboBox.SelectedValue = priceMaster.CategoryID;
                BindProducts((int)priceMaster.CategoryID);
                ddlProduct.SelectedValue = priceMaster.ProductId;
                dtpFrom.Text = priceMaster.EffectiveFrom.ToString();
                dateTo.Text = priceMaster.EffectiveTo.ToString();
                txtProductPrice.Text = Convert.ToDouble(priceMaster.Price).ToString("0.##");
                txtPriceDesc.Text = "";
            }
        }
        #endregion
        long PriceId = 0;
        #region Form Events
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SavePrice();
                MessageBox.Show("Data Saved!");
                BindPriceList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                UpdatePrice(PriceId);
                BtnSave.Enabled = true;
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
                MessageBox.Show("Data Updated!");
                BindPriceList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeletePrice(PriceId);
                BtnSave.Enabled = true;
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
                MessageBox.Show("Data Deleted!");
                BindPriceList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void FrmPriceMaster_Load(object sender, EventArgs e)
        {
            try
            {
                BindPriceList();
                LoadCategories();
                BtnSave.Enabled = true;
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void dgvProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dgvProduct.Rows[e.RowIndex];
                    PriceId = Convert.ToInt64(row.Cells["PriceId"].Value);
                    FillFields(PriceId);
                    BtnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    BtnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void CategorycomboBox_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void CategorycomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CategorycomboBox.SelectedIndex > 0)
                {
                    int categoryId = Convert.ToInt32(CategorycomboBox.SelectedValue);
                    BindProducts(categoryId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        #endregion


    }
}
