﻿using Framework;
using System;
using System.IO;
using System.Windows.Forms;
using System.Linq;
using Framework.Data;
using Shell.Master;

namespace Shell
{
    public partial class FrmDocumentMove : BaseForm
    {
        public FrmDocumentMove()
        {
            InitializeComponent();
        }

        private void FrmDocumentMove_Load(object sender, EventArgs e)
        {
            try
            {
                BindGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        BAL.BAL_FuelTransaction _ObjDoc;
        public void BindGrid()
        {
            try
            {
                _ObjDoc = new BAL.BAL_FuelTransaction();
                var data = _ObjDoc.ListGetDocsList(DateTime.Now, "");
                if (SortBycomboBox.SelectedIndex > 0)
                {
                    if (SortBycomboBox.SelectedIndex == 1)
                    {
                        //Sort by date
                        data = _ObjDoc.ListGetDocsList(DateTime.Now, "").Where(s => Convert.ToDateTime(s.TransactionDate).Date == dtpTransactionDate.Value.Date).ToList();
                    }
                    else if (SortBycomboBox.SelectedIndex == 2)
                    {
                        data = _ObjDoc.ListGetDocsList(DateTime.Now, "").Where(s => s.DocType == ddlDoctype.Text).ToList(); ;
                    }
                }
                dgvDocumentlis.DataSource = data;
                dgvDocumentlis.Columns[1].Visible = false;
                dgvDocumentlis.Columns[8].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }

        private void BindVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendors = _objVendor.GetVendorlist();
            vendors.Insert(0, new DAL.VenderEntity { VendorId = 0, VendorName = "--Select--" });
            vendorNameCombo.DataSource = vendors;
            vendorNameCombo.DisplayMember = "VendorName";
            vendorNameCombo.ValueMember = "VendorID";
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIdentity.Text == "")
                {
                    MessageBox.Show("Enter the Documnet Name");
                    txtIdentity.Focus();
                    return;
                }
                if (ddlDoctype.Text == "--Select--")
                {
                    MessageBox.Show("Select the Doc Type");
                    ddlDoctype.Focus();
                    return;
                }
                string root = ShellComman.DocumentUploadPath;
                root = Path.Combine(root, ddlDoctype.Text);
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }

                if (Openpdf.FileName.ToString() == "")
                {
                    MessageBox.Show("Please Select the File");
                    return;
                }
                if (Openpdf.FileName.ToString() == "")
                {
                    MessageBox.Show("Please Select the File");
                    return;
                }

                string file = Convert.ToDateTime(dtpTransactionDate.Text).ToString("yyyyMMdd");
                int fileCount = 1;
                string fileName = ddlDoctype.Text + "_" + "Report" + "_" + file + "_" + fileCount;
                file = root + "\\" + fileName;
                file = file.Remove(file.Length - 1, 1);
                fileName = fileName.Remove(fileName.Length - 1, 1);
                int count = 0;
                count = Directory.GetFiles(root, Path.GetFileName(file) + "*").Length;
                if (count > 0)
                {
                    count = count + 1;
                    file = file + count;
                    fileName = fileName + count;
                }
                else
                {
                    file = file + fileCount;
                    fileName = fileName + fileCount;
                }
                file = file + Path.GetExtension(txtIdentity.Text);
                int userId = Utility.BA_Commman._userId;
                _ObjDoc = new BAL.BAL_FuelTransaction();
                if (_ObjDoc.ManageDocs(dtpTransactionDate.Value, ddlDoctype.Text, fileName, file, userId) == 1)
                {
                    #region Normal Docs
                    File.Copy(((System.Windows.Forms.FileDialog)(Openpdf)).FileName, file);
                    if (ddlDoctype.Text != "Invoices")
                    {
                        MessageBox.Show("Docs Saved in System Sucessfully");
                    }
                    #endregion
                    else
                    {
                        #region Invoice Docs
                        // Save Clain and Invoice Check List
                        if (ddlDoctype.Text == "Invoices")
                        {
                            if (!string.IsNullOrEmpty(InvoiceAndClaimCheckList.InvoiceNumber) && !string.IsNullOrEmpty(InvoiceAndClaimCheckList.InvoiceDate.ToString()))
                            {
                                using (ShellEntities context = new ShellEntities())
                                {
                                    DateTime invoiceDate = InvoiceAndClaimCheckList.InvoiceDate;
                                    var invoiceCheckList = context.TBL_InvoiceCheckList.Where(s => s.InvoiceDate == invoiceDate).FirstOrDefault();
                                    if (invoiceCheckList != null)
                                    {
                                        invoiceCheckList.AnyShortage = InvoiceAndClaimCheckList.AnyShortage;
                                        invoiceCheckList.AnyShortageClaimed = InvoiceAndClaimCheckList.AnyShortageClaimed;
                                        invoiceCheckList.ClaimInvoiceDate = InvoiceAndClaimCheckList.ClaimInvoiceDate;
                                        invoiceCheckList.ClaimInvoiceNumber = InvoiceAndClaimCheckList.ClaimInvoiceNumber;
                                        invoiceCheckList.ClaimItemNumber = InvoiceAndClaimCheckList.ItemNumber;
                                        invoiceCheckList.ClaimNumber = InvoiceAndClaimCheckList.ClaimNumber;
                                        invoiceCheckList.ClaimRemarks = InvoiceAndClaimCheckList.ClaimRemarks;
                                        invoiceCheckList.ClaimStatus = InvoiceAndClaimCheckList.ClaimStatus;
                                        invoiceCheckList.CreatedBy = 1;
                                        invoiceCheckList.CreatedDate = DateTime.Now;
                                        invoiceCheckList.Difference = InvoiceAndClaimCheckList.Difference;
                                        invoiceCheckList.DifferenceRemarks = InvoiceAndClaimCheckList.DifferenceRemarks;
                                        invoiceCheckList.DocumentUploadedID = 1;
                                        invoiceCheckList.FollowUpDate = InvoiceAndClaimCheckList.FollowUpDate;
                                        invoiceCheckList.FollowUpRemarks = InvoiceAndClaimCheckList.FollowUpRemarks;
                                        invoiceCheckList.GrossAmount = InvoiceAndClaimCheckList.GrossAmount;
                                        invoiceCheckList.GST = InvoiceAndClaimCheckList.GST;
                                        invoiceCheckList.HasReceivingSignature = InvoiceAndClaimCheckList.HasReceivingSignature;
                                        invoiceCheckList.HasVerifiedSignature = InvoiceAndClaimCheckList.HasVerifiedSignature;
                                        invoiceCheckList.InvoiceDate = InvoiceAndClaimCheckList.InvoiceDate;
                                        invoiceCheckList.IsActive = true;
                                        invoiceCheckList.ItemDescripion = InvoiceAndClaimCheckList.ItemDescripion;
                                        invoiceCheckList.ModifiedBy = 1;
                                        invoiceCheckList.ModifiedDate = DateTime.Now;
                                        invoiceCheckList.NetAmount = InvoiceAndClaimCheckList.NetAmount;
                                        invoiceCheckList.ReceivedBy = InvoiceAndClaimCheckList.ReceivedBy;
                                        invoiceCheckList.ReceivingAmount = InvoiceAndClaimCheckList.ReceivingAmount;
                                        invoiceCheckList.WhoVerified = InvoiceAndClaimCheckList.WhoVerified;
                                        context.SaveChanges();
                                        MessageBox.Show("Docs Saved in System Sucessfully");

                                    }
                                    else
                                    {
                                        invoiceCheckList = new TBL_InvoiceCheckList();
                                        invoiceCheckList.InvoiceNumber = InvoiceAndClaimCheckList.InvoiceNumber;
                                        invoiceCheckList.InvoiceDate = InvoiceAndClaimCheckList.InvoiceDate;
                                        invoiceCheckList.AnyShortage = InvoiceAndClaimCheckList.AnyShortage;
                                        invoiceCheckList.AnyShortageClaimed = InvoiceAndClaimCheckList.AnyShortageClaimed;
                                        invoiceCheckList.ClaimInvoiceDate = InvoiceAndClaimCheckList.ClaimInvoiceDate;
                                        invoiceCheckList.ClaimInvoiceNumber = InvoiceAndClaimCheckList.ClaimInvoiceNumber;
                                        invoiceCheckList.ClaimItemNumber = InvoiceAndClaimCheckList.ItemNumber;
                                        invoiceCheckList.ClaimNumber = InvoiceAndClaimCheckList.ClaimNumber;
                                        invoiceCheckList.ClaimRemarks = InvoiceAndClaimCheckList.ClaimRemarks;
                                        invoiceCheckList.ClaimStatus = InvoiceAndClaimCheckList.ClaimStatus;
                                        invoiceCheckList.CreatedBy = 1;
                                        invoiceCheckList.CreatedDate = DateTime.Now;
                                        invoiceCheckList.Difference = InvoiceAndClaimCheckList.Difference;
                                        invoiceCheckList.DifferenceRemarks = InvoiceAndClaimCheckList.DifferenceRemarks;
                                        invoiceCheckList.DocumentUploadedID = 1;
                                        invoiceCheckList.FollowUpDate = InvoiceAndClaimCheckList.FollowUpDate;
                                        invoiceCheckList.FollowUpRemarks = InvoiceAndClaimCheckList.FollowUpRemarks;
                                        invoiceCheckList.GrossAmount = InvoiceAndClaimCheckList.GrossAmount;
                                        invoiceCheckList.GST = InvoiceAndClaimCheckList.GST;
                                        invoiceCheckList.HasReceivingSignature = InvoiceAndClaimCheckList.HasReceivingSignature;
                                        invoiceCheckList.HasVerifiedSignature = InvoiceAndClaimCheckList.HasVerifiedSignature;
                                        // invoiceCheckList.InvoiceDate = InvoiceAndClaimCheckList.InvoiceDate;
                                        invoiceCheckList.IsActive = true;
                                        invoiceCheckList.ItemDescripion = InvoiceAndClaimCheckList.ItemDescripion;
                                        invoiceCheckList.ModifiedBy = 1;
                                        invoiceCheckList.ModifiedDate = DateTime.Now;
                                        invoiceCheckList.NetAmount = InvoiceAndClaimCheckList.NetAmount;
                                        invoiceCheckList.ReceivedBy = InvoiceAndClaimCheckList.ReceivedBy;
                                        invoiceCheckList.ReceivingAmount = InvoiceAndClaimCheckList.ReceivingAmount;
                                        invoiceCheckList.WhoVerified = InvoiceAndClaimCheckList.WhoVerified;
                                        context.TBL_InvoiceCheckList.Add(invoiceCheckList);
                                        context.SaveChanges();
                                        MessageBox.Show("Docs Saved in System Sucessfully");
                                    }


                                }

                            }
                        }

                        else
                        {
                            MessageBox.Show("Please enter invoice number and invoice date.");
                        }
                    }
                    #endregion
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");
                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }


        }
        public void ClearValue()
        {

            txtIdentity.Text = "";
            pdfUploadbox.Text = "";
            BindGrid();
            BtnDelete.Enabled = false;
            BtnSave.Enabled = true;

        }

        OpenFileDialog Openpdf = new OpenFileDialog();
        private void btnbrowse_Click(object sender, EventArgs e)
        {
            try
            {

                // Openpdf.Filter = "All files|*.*|All files|*.*;";
                if (Openpdf.ShowDialog() == DialogResult.OK)
                {
                    string pdfLog = Openpdf.FileName.ToString();
                    pdfUploadbox.Text = pdfLog;
                }
                if (Openpdf.FileName.ToString() != "")
                {
                    if (ddlDoctype.Text != "Others")
                    {
                        string docName = ddlDoctype.Text + "_" + "Report_" + Openpdf.SafeFileName;
                        txtIdentity.Text = docName;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }
        string FilePath = "";
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(FilePath))
                    File.Delete(FilePath);
                if (_ObjDoc.Deletedocs(DocId) == 1)
                {

                    MessageBox.Show("Document deleted successfully.");
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private int DocId;

        private void FillDetails(int docId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                var document = context.Mst_DocUpload.Where(s => s.UploadId == docId).FirstOrDefault();
                if (document != null)
                {
                    ddlDoctype.Text = document.DocsType;
                    txtIdentity.Text = document.DocsIdentity;
                    pdfUploadbox.Text = document.FilePath;
                    dtpTransactionDate.Text = Convert.ToDateTime(document.TransactionDate).ToString();
                }

            }
        }
        private void dgvDocumentlis_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex > 0 && e.RowIndex >= 0)
                {
                    DialogResult dg = MessageBox.Show("Do You Want to Update/Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dg == DialogResult.Yes)
                    {
                        DocId = int.Parse(dgvDocumentlis.Rows[e.RowIndex].Cells["DocId"].Value.ToString());
                        FillDetails(DocId);
                        BtnDelete.Enabled = true;
                        BtnSave.Enabled = false;
                    }
                    else
                    {
                        BtnDelete.Enabled = false;
                        BtnSave.Enabled = true;
                    }
                }
                else if (e.ColumnIndex == 0 && e.RowIndex >= 0)
                {

                    FilePath = (dgvDocumentlis.Rows[e.RowIndex].Cells["FilePath"].Value.ToString());
                    if (File.Exists(FilePath))
                    {
                        System.Diagnostics.Process.Start("explorer.exe", FilePath);
                    }
                    else
                    {
                        MessageBox.Show("This file doesn't exist.");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void ddlDoctype_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (ddlDoctype.SelectedIndex > 0)
                {
                    if (ddlDoctype.Text == "Invoices")
                    {
                        vendorNameCombo.Enabled = true;
                        BindVendors();
                    }
                    else
                    {
                        vendorNameCombo.Enabled = false;
                    }
                }
                else
                {
                    MessageBox.Show("Please select document type...");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SortBycomboBox.SelectedIndex > 0)
            {
                if (SortBycomboBox.SelectedIndex == 2 && ddlDoctype.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select doc type first.");
                }
                else
                {
                    BindGrid();
                }

            }
        }

        private void vendorNameCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            InvoiceCheckListPopup checkListPopup = new InvoiceCheckListPopup();
            if (vendorNameCombo.SelectedIndex > 0)
            {
                checkListPopup.Show();
            }
            else
            {
                checkListPopup.Close();
            }
        }
    }
}
