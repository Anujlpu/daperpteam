﻿namespace Shell
{
    partial class FrmDocumentMove
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmDocumentMove));
            this.label16 = new System.Windows.Forms.Label();
            this.dtpTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.btnbrowse = new System.Windows.Forms.Button();
            this.grpDocumentList = new System.Windows.Forms.GroupBox();
            this.dgvDocumentlis = new System.Windows.Forms.DataGridView();
            this.View = new System.Windows.Forms.DataGridViewLinkColumn();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.pdfUploadbox = new System.Windows.Forms.TextBox();
            this.ddlDoctype = new System.Windows.Forms.ComboBox();
            this.label74 = new System.Windows.Forms.Label();
            this.txtIdentity = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.vendorLbl = new System.Windows.Forms.Label();
            this.vendorNameCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SortBycomboBox = new System.Windows.Forms.ComboBox();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.grpDocumentList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentlis)).BeginInit();
            this.SuspendLayout();
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(12, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 15);
            this.label16.TabIndex = 57;
            this.label16.Text = "Docs Type";
            // 
            // dtpTransactionDate
            // 
            this.dtpTransactionDate.CustomFormat = "MMM/dd/yyyy";
            this.dtpTransactionDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransactionDate.Location = new System.Drawing.Point(140, 17);
            this.dtpTransactionDate.Name = "dtpTransactionDate";
            this.dtpTransactionDate.Size = new System.Drawing.Size(227, 22);
            this.dtpTransactionDate.TabIndex = 54;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.AliceBlue;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(12, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 16);
            this.label15.TabIndex = 56;
            this.label15.Text = "Transaction Date";
            // 
            // btnbrowse
            // 
            this.btnbrowse.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnbrowse.FlatAppearance.BorderSize = 2;
            this.btnbrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbrowse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbrowse.Location = new System.Drawing.Point(140, 113);
            this.btnbrowse.Name = "btnbrowse";
            this.btnbrowse.Size = new System.Drawing.Size(227, 33);
            this.btnbrowse.TabIndex = 55;
            this.btnbrowse.Text = "B&rowse";
            this.btnbrowse.UseVisualStyleBackColor = true;
            this.btnbrowse.Click += new System.EventHandler(this.btnbrowse_Click);
            // 
            // grpDocumentList
            // 
            this.grpDocumentList.Controls.Add(this.dgvDocumentlis);
            this.grpDocumentList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDocumentList.Location = new System.Drawing.Point(15, 180);
            this.grpDocumentList.Name = "grpDocumentList";
            this.grpDocumentList.Size = new System.Drawing.Size(1018, 300);
            this.grpDocumentList.TabIndex = 60;
            this.grpDocumentList.TabStop = false;
            this.grpDocumentList.Text = "Document  List";
            // 
            // dgvDocumentlis
            // 
            this.dgvDocumentlis.AllowDrop = true;
            this.dgvDocumentlis.AllowUserToAddRows = false;
            this.dgvDocumentlis.AllowUserToDeleteRows = false;
            this.dgvDocumentlis.AllowUserToResizeColumns = false;
            this.dgvDocumentlis.AllowUserToResizeRows = false;
            this.dgvDocumentlis.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDocumentlis.BackgroundColor = System.Drawing.Color.White;
            this.dgvDocumentlis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDocumentlis.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.View});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDocumentlis.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvDocumentlis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDocumentlis.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDocumentlis.Location = new System.Drawing.Point(3, 16);
            this.dgvDocumentlis.MultiSelect = false;
            this.dgvDocumentlis.Name = "dgvDocumentlis";
            this.dgvDocumentlis.ReadOnly = true;
            this.dgvDocumentlis.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvDocumentlis.RowHeadersVisible = false;
            this.dgvDocumentlis.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvDocumentlis.Size = new System.Drawing.Size(1012, 281);
            this.dgvDocumentlis.TabIndex = 1;
            this.dgvDocumentlis.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDocumentlis_CellClick);
            // 
            // View
            // 
            this.View.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.NullValue = "View";
            this.View.DefaultCellStyle = dataGridViewCellStyle1;
            this.View.HeaderText = "View Document";
            this.View.Name = "View";
            this.View.ReadOnly = true;
            this.View.Text = "View";
            this.View.ToolTipText = "View Document";
            this.View.Width = 197;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial Narrow", 9.25F);
            this.BtnDelete.Location = new System.Drawing.Point(235, 152);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 27);
            this.BtnDelete.TabIndex = 63;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial Narrow", 9.25F);
            this.BtnSave.Location = new System.Drawing.Point(139, 152);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 27);
            this.BtnSave.TabIndex = 61;
            this.BtnSave.Text = "&Upload";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // pdfUploadbox
            // 
            this.pdfUploadbox.Enabled = false;
            this.pdfUploadbox.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pdfUploadbox.ForeColor = System.Drawing.Color.Black;
            this.pdfUploadbox.Location = new System.Drawing.Point(524, 45);
            this.pdfUploadbox.Name = "pdfUploadbox";
            this.pdfUploadbox.ReadOnly = true;
            this.pdfUploadbox.Size = new System.Drawing.Size(227, 22);
            this.pdfUploadbox.TabIndex = 64;
            // 
            // ddlDoctype
            // 
            this.ddlDoctype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlDoctype.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlDoctype.FormattingEnabled = true;
            this.ddlDoctype.Items.AddRange(new object[] {
            "--Select--",
            "Sale",
            "Cash",
            "Fuel",
            "Settlement",
            "Tobacco",
            "Invoices",
            "Payment Voucher",
            "BankEntries",
            "PNL",
            "Lotto Activated",
            "Lotto UnActivated",
            "Chew",
            "Cigratees",
            "Lotto Scratch And Win",
            "Lotto Scratch And Win Online",
            "Addition",
            "Cigar",
            "E-Cigar",
            "WWFCalc",
            "Tobacco Inventory Docs",
            "Cigar Inventory Docs",
            "Singles And Papers",
            "Lotto Weekly",
            "Milk",
            "Other"});
            this.ddlDoctype.Location = new System.Drawing.Point(140, 45);
            this.ddlDoctype.Name = "ddlDoctype";
            this.ddlDoctype.Size = new System.Drawing.Size(227, 22);
            this.ddlDoctype.TabIndex = 65;
            this.ddlDoctype.SelectedIndexChanged += new System.EventHandler(this.ddlDoctype_SelectedIndexChanged);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(396, 19);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(66, 15);
            this.label74.TabIndex = 170;
            this.label74.Text = "Doc Name";
            // 
            // txtIdentity
            // 
            this.txtIdentity.Enabled = false;
            this.txtIdentity.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIdentity.ForeColor = System.Drawing.Color.Black;
            this.txtIdentity.Location = new System.Drawing.Point(524, 13);
            this.txtIdentity.MaxLength = 40;
            this.txtIdentity.Name = "txtIdentity";
            this.txtIdentity.ReadOnly = true;
            this.txtIdentity.Size = new System.Drawing.Size(227, 26);
            this.txtIdentity.TabIndex = 169;
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(331, 152);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 27);
            this.btnExit.TabIndex = 171;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.AliceBlue;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(757, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(12, 15);
            this.label18.TabIndex = 172;
            this.label18.Text = "*";
            // 
            // vendorLbl
            // 
            this.vendorLbl.AutoSize = true;
            this.vendorLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vendorLbl.ForeColor = System.Drawing.Color.Black;
            this.vendorLbl.Location = new System.Drawing.Point(15, 82);
            this.vendorLbl.Name = "vendorLbl";
            this.vendorLbl.Size = new System.Drawing.Size(82, 15);
            this.vendorLbl.TabIndex = 173;
            this.vendorLbl.Text = "Vendor Name";
            // 
            // vendorNameCombo
            // 
            this.vendorNameCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.vendorNameCombo.Enabled = false;
            this.vendorNameCombo.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vendorNameCombo.FormattingEnabled = true;
            this.vendorNameCombo.Location = new System.Drawing.Point(140, 79);
            this.vendorNameCombo.Name = "vendorNameCombo";
            this.vendorNameCombo.Size = new System.Drawing.Size(227, 22);
            this.vendorNameCombo.TabIndex = 174;
            this.vendorNameCombo.SelectedIndexChanged += new System.EventHandler(this.vendorNameCombo_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(396, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 175;
            this.label1.Text = "Sort By";
            // 
            // SortBycomboBox
            // 
            this.SortBycomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SortBycomboBox.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SortBycomboBox.FormattingEnabled = true;
            this.SortBycomboBox.Items.AddRange(new object[] {
            "--Select--",
            "Transaction Date",
            "Document Type"});
            this.SortBycomboBox.Location = new System.Drawing.Point(524, 76);
            this.SortBycomboBox.Name = "SortBycomboBox";
            this.SortBycomboBox.Size = new System.Drawing.Size(227, 22);
            this.SortBycomboBox.TabIndex = 176;
            this.SortBycomboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle3.NullValue")));
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(5);
            this.dataGridViewImageColumn1.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewImageColumn1.HeaderText = "View Document";
            this.dataGridViewImageColumn1.Image = global::Shell.Properties.Resources.download;
            this.dataGridViewImageColumn1.MinimumWidth = 25;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ToolTipText = "View Document";
            this.dataGridViewImageColumn1.Width = 150;
            // 
            // FrmDocumentMove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1045, 492);
            this.Controls.Add(this.SortBycomboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.vendorNameCombo);
            this.Controls.Add(this.vendorLbl);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.txtIdentity);
            this.Controls.Add(this.ddlDoctype);
            this.Controls.Add(this.pdfUploadbox);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.grpDocumentList);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.dtpTransactionDate);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnbrowse);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmDocumentMove";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Document Upload Master";
            this.Load += new System.EventHandler(this.FrmDocumentMove_Load);
            this.grpDocumentList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDocumentlis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dtpTransactionDate;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnbrowse;
        private System.Windows.Forms.GroupBox grpDocumentList;
        private System.Windows.Forms.DataGridView dgvDocumentlis;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.TextBox pdfUploadbox;
        private System.Windows.Forms.ComboBox ddlDoctype;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox txtIdentity;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewLinkColumn View;
        private System.Windows.Forms.Label vendorLbl;
        private System.Windows.Forms.ComboBox vendorNameCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox SortBycomboBox;
    }
}