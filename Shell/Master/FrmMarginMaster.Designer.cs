﻿namespace Shell
{
    partial class FrmMarginMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.ToDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.FromDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.ProductscomboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CategoriesCombobox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.OffPerTwoPackTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.MarkUpTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DiscountTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.CartonMarginTxt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.DiscountedCartonPriceTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.RegularCartonPriceTxt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.MarginOnTwoPackDealTxt = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.TwoPackDealTxt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.MarginonSellingpriceTxt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.SellingPriceTxt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.BuyingPricePerPieceTxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.DiscountedPriceTxt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.BuyingPriceTxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.MarginListGridView = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MarginListGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ViewBtn);
            this.groupBox1.Controls.Add(this.ToDatePicker);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.FromDatePicker);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ProductscomboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.CategoriesCombobox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(15, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1034, 181);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Master Selections";
            // 
            // ViewBtn
            // 
            this.ViewBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ViewBtn.FlatAppearance.BorderSize = 2;
            this.ViewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewBtn.Location = new System.Drawing.Point(195, 126);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(105, 37);
            this.ViewBtn.TabIndex = 55;
            this.ViewBtn.Text = "View";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // ToDatePicker
            // 
            this.ToDatePicker.Location = new System.Drawing.Point(617, 96);
            this.ToDatePicker.Name = "ToDatePicker";
            this.ToDatePicker.Size = new System.Drawing.Size(214, 21);
            this.ToDatePicker.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(455, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "To";
            // 
            // FromDatePicker
            // 
            this.FromDatePicker.Location = new System.Drawing.Point(196, 89);
            this.FromDatePicker.Name = "FromDatePicker";
            this.FromDatePicker.Size = new System.Drawing.Size(214, 21);
            this.FromDatePicker.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "From";
            // 
            // ProductscomboBox
            // 
            this.ProductscomboBox.FormattingEnabled = true;
            this.ProductscomboBox.Items.AddRange(new object[] {
            "--Select--"});
            this.ProductscomboBox.Location = new System.Drawing.Point(617, 40);
            this.ProductscomboBox.Name = "ProductscomboBox";
            this.ProductscomboBox.Size = new System.Drawing.Size(214, 23);
            this.ProductscomboBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(455, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Products";
            // 
            // CategoriesCombobox
            // 
            this.CategoriesCombobox.FormattingEnabled = true;
            this.CategoriesCombobox.Items.AddRange(new object[] {
            "--Select--",
            "Lotto",
            "Tobaccco",
            "Milk"});
            this.CategoriesCombobox.Location = new System.Drawing.Point(196, 38);
            this.CategoriesCombobox.Name = "CategoriesCombobox";
            this.CategoriesCombobox.Size = new System.Drawing.Size(214, 23);
            this.CategoriesCombobox.TabIndex = 1;
            this.CategoriesCombobox.SelectedIndexChanged += new System.EventHandler(this.CategoriesCombobox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Category";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RemarksTxt);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.BtnDelete);
            this.groupBox2.Controls.Add(this.BtnSave);
            this.groupBox2.Controls.Add(this.btnUpdate);
            this.groupBox2.Controls.Add(this.OffPerTwoPackTxt);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.MarkUpTxt);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.DiscountTxt);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(14, 203);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1034, 215);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Margin Inputs";
            // 
            // RemarksTxt
            // 
            this.RemarksTxt.Location = new System.Drawing.Point(595, 98);
            this.RemarksTxt.Name = "RemarksTxt";
            this.RemarksTxt.Size = new System.Drawing.Size(409, 61);
            this.RemarksTxt.TabIndex = 54;
            this.RemarksTxt.Text = "";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(441, 98);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 15);
            this.label18.TabIndex = 53;
            this.label18.Text = "Remarks";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(423, 155);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(103, 37);
            this.btnExit.TabIndex = 49;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click_1);
            // 
            // BtnDelete
            // 
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(311, 155);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(105, 37);
            this.BtnDelete.TabIndex = 51;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(90, 155);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(105, 37);
            this.BtnSave.TabIndex = 50;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUpdate.FlatAppearance.BorderSize = 2;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(201, 155);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(105, 37);
            this.btnUpdate.TabIndex = 52;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // OffPerTwoPackTxt
            // 
            this.OffPerTwoPackTxt.Location = new System.Drawing.Point(177, 98);
            this.OffPerTwoPackTxt.Name = "OffPerTwoPackTxt";
            this.OffPerTwoPackTxt.Size = new System.Drawing.Size(214, 21);
            this.OffPerTwoPackTxt.TabIndex = 13;
            this.OffPerTwoPackTxt.Text = "0.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "Off Per 2 Packs";
            // 
            // MarkUpTxt
            // 
            this.MarkUpTxt.Location = new System.Drawing.Point(595, 40);
            this.MarkUpTxt.Name = "MarkUpTxt";
            this.MarkUpTxt.Size = new System.Drawing.Size(214, 21);
            this.MarkUpTxt.TabIndex = 11;
            this.MarkUpTxt.Text = "0.00";
            this.MarkUpTxt.MouseLeave += new System.EventHandler(this.MarkUpTxt_MouseLeave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(433, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Mark Up";
            // 
            // DiscountTxt
            // 
            this.DiscountTxt.Location = new System.Drawing.Point(177, 40);
            this.DiscountTxt.Name = "DiscountTxt";
            this.DiscountTxt.Size = new System.Drawing.Size(214, 21);
            this.DiscountTxt.TabIndex = 9;
            this.DiscountTxt.Text = "0.00";
            this.DiscountTxt.TextChanged += new System.EventHandler(this.DiscountTxt_TextChanged);
            this.DiscountTxt.MouseLeave += new System.EventHandler(this.DiscountTxt_MouseLeave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Discount";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CartonMarginTxt);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.DiscountedCartonPriceTxt);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.RegularCartonPriceTxt);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.MarginOnTwoPackDealTxt);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.TwoPackDealTxt);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.MarginonSellingpriceTxt);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.SellingPriceTxt);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.BuyingPricePerPieceTxt);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.DiscountedPriceTxt);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.BuyingPriceTxt);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(8, 425);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1034, 227);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Auto Calculated";
            // 
            // CartonMarginTxt
            // 
            this.CartonMarginTxt.Enabled = false;
            this.CartonMarginTxt.Location = new System.Drawing.Point(257, 110);
            this.CartonMarginTxt.Name = "CartonMarginTxt";
            this.CartonMarginTxt.Size = new System.Drawing.Size(214, 21);
            this.CartonMarginTxt.TabIndex = 33;
            this.CartonMarginTxt.Text = "0.00";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 117);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 15);
            this.label17.TabIndex = 32;
            this.label17.Text = "Carton Margin ";
            // 
            // DiscountedCartonPriceTxt
            // 
            this.DiscountedCartonPriceTxt.Enabled = false;
            this.DiscountedCartonPriceTxt.Location = new System.Drawing.Point(765, 29);
            this.DiscountedCartonPriceTxt.Name = "DiscountedCartonPriceTxt";
            this.DiscountedCartonPriceTxt.Size = new System.Drawing.Size(214, 21);
            this.DiscountedCartonPriceTxt.TabIndex = 31;
            this.DiscountedCartonPriceTxt.Text = "0.00";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(521, 36);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(139, 15);
            this.label16.TabIndex = 30;
            this.label16.Text = "Discounted Carton Price";
            // 
            // RegularCartonPriceTxt
            // 
            this.RegularCartonPriceTxt.Enabled = false;
            this.RegularCartonPriceTxt.Location = new System.Drawing.Point(765, 177);
            this.RegularCartonPriceTxt.Name = "RegularCartonPriceTxt";
            this.RegularCartonPriceTxt.Size = new System.Drawing.Size(214, 21);
            this.RegularCartonPriceTxt.TabIndex = 29;
            this.RegularCartonPriceTxt.Text = "0.00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(521, 183);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(121, 15);
            this.label15.TabIndex = 28;
            this.label15.Text = "Regular Carton Price";
            // 
            // MarginOnTwoPackDealTxt
            // 
            this.MarginOnTwoPackDealTxt.Enabled = false;
            this.MarginOnTwoPackDealTxt.Location = new System.Drawing.Point(257, 149);
            this.MarginOnTwoPackDealTxt.Name = "MarginOnTwoPackDealTxt";
            this.MarginOnTwoPackDealTxt.Size = new System.Drawing.Size(214, 21);
            this.MarginOnTwoPackDealTxt.TabIndex = 27;
            this.MarginOnTwoPackDealTxt.Text = "0.00";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 156);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(134, 15);
            this.label14.TabIndex = 26;
            this.label14.Text = "Margin On 2 Pack Deal";
            // 
            // TwoPackDealTxt
            // 
            this.TwoPackDealTxt.Enabled = false;
            this.TwoPackDealTxt.Location = new System.Drawing.Point(765, 140);
            this.TwoPackDealTxt.Name = "TwoPackDealTxt";
            this.TwoPackDealTxt.Size = new System.Drawing.Size(214, 21);
            this.TwoPackDealTxt.TabIndex = 25;
            this.TwoPackDealTxt.Text = "0.00";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(521, 147);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 15);
            this.label13.TabIndex = 24;
            this.label13.Text = "2 Pack Deal";
            // 
            // MarginonSellingpriceTxt
            // 
            this.MarginonSellingpriceTxt.Enabled = false;
            this.MarginonSellingpriceTxt.Location = new System.Drawing.Point(257, 186);
            this.MarginonSellingpriceTxt.Name = "MarginonSellingpriceTxt";
            this.MarginonSellingpriceTxt.Size = new System.Drawing.Size(214, 21);
            this.MarginonSellingpriceTxt.TabIndex = 23;
            this.MarginonSellingpriceTxt.Text = "0.00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 189);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(137, 15);
            this.label12.TabIndex = 22;
            this.label12.Text = "Margin On Selling Price";
            // 
            // SellingPriceTxt
            // 
            this.SellingPriceTxt.Enabled = false;
            this.SellingPriceTxt.Location = new System.Drawing.Point(765, 103);
            this.SellingPriceTxt.Name = "SellingPriceTxt";
            this.SellingPriceTxt.Size = new System.Drawing.Size(214, 21);
            this.SellingPriceTxt.TabIndex = 21;
            this.SellingPriceTxt.Text = "0.00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(524, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 15);
            this.label11.TabIndex = 20;
            this.label11.Text = "Selling Price";
            // 
            // BuyingPricePerPieceTxt
            // 
            this.BuyingPricePerPieceTxt.Enabled = false;
            this.BuyingPricePerPieceTxt.Location = new System.Drawing.Point(257, 73);
            this.BuyingPricePerPieceTxt.Name = "BuyingPricePerPieceTxt";
            this.BuyingPricePerPieceTxt.Size = new System.Drawing.Size(214, 21);
            this.BuyingPricePerPieceTxt.TabIndex = 19;
            this.BuyingPricePerPieceTxt.Text = "0.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 15);
            this.label10.TabIndex = 18;
            this.label10.Text = "Buying Price/Piece";
            // 
            // DiscountedPriceTxt
            // 
            this.DiscountedPriceTxt.Enabled = false;
            this.DiscountedPriceTxt.Location = new System.Drawing.Point(765, 66);
            this.DiscountedPriceTxt.Name = "DiscountedPriceTxt";
            this.DiscountedPriceTxt.Size = new System.Drawing.Size(214, 21);
            this.DiscountedPriceTxt.TabIndex = 17;
            this.DiscountedPriceTxt.Text = "0.00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(524, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "Discounted Price";
            // 
            // BuyingPriceTxt
            // 
            this.BuyingPriceTxt.Enabled = false;
            this.BuyingPriceTxt.Location = new System.Drawing.Point(257, 36);
            this.BuyingPriceTxt.Name = "BuyingPriceTxt";
            this.BuyingPriceTxt.Size = new System.Drawing.Size(214, 21);
            this.BuyingPriceTxt.TabIndex = 15;
            this.BuyingPriceTxt.Text = "0.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 15);
            this.label8.TabIndex = 14;
            this.label8.Text = "Buying Price";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.MarginListGridView);
            this.groupBox4.Location = new System.Drawing.Point(15, 660);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1027, 172);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Margin List";
            // 
            // MarginListGridView
            // 
            this.MarginListGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MarginListGridView.Location = new System.Drawing.Point(24, 22);
            this.MarginListGridView.Name = "MarginListGridView";
            this.MarginListGridView.Size = new System.Drawing.Size(979, 143);
            this.MarginListGridView.TabIndex = 0;
            this.MarginListGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // FrmMarginMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1063, 749);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMarginMaster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Margin Master";
            this.Load += new System.EventHandler(this.FrmMarginMaster_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MarginListGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CategoriesCombobox;
        private System.Windows.Forms.ComboBox ProductscomboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker ToDatePicker;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker FromDatePicker;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox DiscountTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox MarkUpTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox OffPerTwoPackTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox BuyingPriceTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox DiscountedPriceTxt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox BuyingPricePerPieceTxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox SellingPriceTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox MarginonSellingpriceTxt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TwoPackDealTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox MarginOnTwoPackDealTxt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox RegularCartonPriceTxt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox DiscountedCartonPriceTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox CartonMarginTxt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RichTextBox RemarksTxt;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView MarginListGridView;
    }
}