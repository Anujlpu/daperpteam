﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class EmailNotificationConfigurationForm : BaseForm
    {
        public EmailNotificationConfigurationForm()
        {
            InitializeComponent();
        }

        #region private methods
        private void SaveFromEmailConfiguration()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var fromEmailConfig = context.MST_EmailConfiguration.FirstOrDefault(s => s.FromEmailID.Equals(EmailIDTxt.Text));
                    if (fromEmailConfig != null)
                    {
                        string password = Utility.BA_Commman.Encrypt(PasswordTxt.Text, true);
                        fromEmailConfig.FromEmailID = EmailIDTxt.Text;
                        fromEmailConfig.FromEmailIDPassword = password;
                        fromEmailConfig.FromServer = ServerTxt.Text;
                        fromEmailConfig.FromPort = Convert.ToInt32(PortTxt.Text);
                        fromEmailConfig.FromSubject = SubjectTxt.Text;
                        fromEmailConfig.FromName = FromNameTxt.Text;
                        fromEmailConfig.ModifiedDate = DateTime.Now;
                        fromEmailConfig.ModifiedBy = 1;
                        fromEmailConfig.IsActive = true;
                        fromEmailConfig.ToEmailIds = ToEmailIdsTxt.Text;
                        fromEmailConfig.CcEmailIds = CcEmailIdsTxt.Text;
                        fromEmailConfig.BccEmailIds = BccEmailIdsTxt.Text;
                    }
                    else
                    {
                        fromEmailConfig = new MST_EmailConfiguration();
                        fromEmailConfig.FromEmailID = EmailIDTxt.Text;
                        fromEmailConfig.FromEmailIDPassword = PasswordTxt.Text;
                        fromEmailConfig.FromServer = ServerTxt.Text;
                        fromEmailConfig.FromPort = Convert.ToInt32(PortTxt.Text);
                        fromEmailConfig.FromSubject = SubjectTxt.Text;
                        fromEmailConfig.FromName = FromNameTxt.Text;
                        fromEmailConfig.CreatedDate = DateTime.Now;
                        fromEmailConfig.CreatedBy = 1;
                        fromEmailConfig.IsActive = true;
                        fromEmailConfig.ToEmailIds = ToEmailIdsTxt.Text;
                        fromEmailConfig.CcEmailIds = CcEmailIdsTxt.Text;
                        fromEmailConfig.BccEmailIds = BccEmailIdsTxt.Text;
                        context.MST_EmailConfiguration.Add(fromEmailConfig);
                    }
                    context.SaveChanges();
                    MessageBox.Show("Saved");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LoadEmailConfiguration()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var fromEmailConfig = context.MST_EmailConfiguration.FirstOrDefault(s => s.IsActive == true);
                if (fromEmailConfig != null)
                {
                    EmailIDTxt.Text = fromEmailConfig.FromEmailID;
                    PasswordTxt.Text = "********";
                    ServerTxt.Text = fromEmailConfig.FromServer;
                    PortTxt.Text = Convert.ToInt32(fromEmailConfig.FromPort).ToString();
                    SubjectTxt.Text = fromEmailConfig.FromSubject;
                    FromNameTxt.Text = fromEmailConfig.FromName;
                    ToEmailIdsTxt.Text = fromEmailConfig.ToEmailIds;
                    CcEmailIdsTxt.Text = fromEmailConfig.CcEmailIds;
                    BccEmailIdsTxt.Text = fromEmailConfig.BccEmailIds;
                }
            }
        }

        #endregion
        #region Form Events
        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFromEmailConfiguration();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FromEmailSaveBtn_Click(object sender, EventArgs e)
        {

        }
        private void EmailNotificationConfigurationForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadEmailConfiguration();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion


    }
}
