﻿namespace Shell.Master
{
    partial class SummaryPopupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ApprovalStatusLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ApprovalRoleLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ApprovedDateLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ApprovedByLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.RemarksTxt = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RemarksTxt);
            this.groupBox1.Controls.Add(this.ApprovalStatusLabel);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ApprovalRoleLabel);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.ApprovedDateLabel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ApprovedByLabel);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 285);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Remarks Details";
            // 
            // ApprovalStatusLabel
            // 
            this.ApprovalStatusLabel.AutoSize = true;
            this.ApprovalStatusLabel.Location = new System.Drawing.Point(153, 125);
            this.ApprovalStatusLabel.Name = "ApprovalStatusLabel";
            this.ApprovalStatusLabel.Size = new System.Drawing.Size(87, 16);
            this.ApprovalStatusLabel.TabIndex = 9;
            this.ApprovalStatusLabel.Text = "Approved By";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 16);
            this.label6.TabIndex = 8;
            this.label6.Text = "Approval Status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 163);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Approval Remarks";
            // 
            // ApprovalRoleLabel
            // 
            this.ApprovalRoleLabel.AutoSize = true;
            this.ApprovalRoleLabel.Location = new System.Drawing.Point(153, 93);
            this.ApprovalRoleLabel.Name = "ApprovalRoleLabel";
            this.ApprovalRoleLabel.Size = new System.Drawing.Size(87, 16);
            this.ApprovalRoleLabel.TabIndex = 5;
            this.ApprovalRoleLabel.Text = "Approved By";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Approval Role";
            // 
            // ApprovedDateLabel
            // 
            this.ApprovedDateLabel.AutoSize = true;
            this.ApprovedDateLabel.Location = new System.Drawing.Point(153, 57);
            this.ApprovedDateLabel.Name = "ApprovedDateLabel";
            this.ApprovedDateLabel.Size = new System.Drawing.Size(87, 16);
            this.ApprovedDateLabel.TabIndex = 3;
            this.ApprovedDateLabel.Text = "Approved By";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Approved Date";
            // 
            // ApprovedByLabel
            // 
            this.ApprovedByLabel.AutoSize = true;
            this.ApprovedByLabel.Location = new System.Drawing.Point(153, 22);
            this.ApprovedByLabel.Name = "ApprovedByLabel";
            this.ApprovedByLabel.Size = new System.Drawing.Size(87, 16);
            this.ApprovedByLabel.TabIndex = 1;
            this.ApprovedByLabel.Text = "Approved By";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Approved By";
            // 
            // RemarksTxt
            // 
            this.RemarksTxt.Enabled = false;
            this.RemarksTxt.Location = new System.Drawing.Point(156, 160);
            this.RemarksTxt.Name = "RemarksTxt";
            this.RemarksTxt.Size = new System.Drawing.Size(241, 119);
            this.RemarksTxt.TabIndex = 10;
            this.RemarksTxt.Text = "";
            // 
            // SummaryPopupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(428, 311);
            this.Controls.Add(this.groupBox1);
            this.Name = "SummaryPopupForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Remarks Details";
            this.Load += new System.EventHandler(this.SummaryPopupForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ApprovedByLabel;
        private System.Windows.Forms.Label ApprovalRoleLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label ApprovedDateLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label ApprovalStatusLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox RemarksTxt;
    }
}