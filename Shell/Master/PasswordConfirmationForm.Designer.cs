﻿namespace Shell.Master
{
    partial class PasswordConfirmationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Password = new System.Windows.Forms.Label();
            this.CurrentPasswordTxt = new System.Windows.Forms.TextBox();
            this.VerifyBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Password
            // 
            this.Password.AutoSize = true;
            this.Password.Location = new System.Drawing.Point(38, 40);
            this.Password.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(135, 20);
            this.Password.TabIndex = 0;
            this.Password.Text = "Current Password";
            // 
            // CurrentPasswordTxt
            // 
            this.CurrentPasswordTxt.Location = new System.Drawing.Point(216, 34);
            this.CurrentPasswordTxt.Name = "CurrentPasswordTxt";
            this.CurrentPasswordTxt.PasswordChar = '*';
            this.CurrentPasswordTxt.Size = new System.Drawing.Size(245, 26);
            this.CurrentPasswordTxt.TabIndex = 1;
            // 
            // VerifyBtn
            // 
            this.VerifyBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.VerifyBtn.FlatAppearance.BorderSize = 2;
            this.VerifyBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VerifyBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VerifyBtn.Location = new System.Drawing.Point(216, 79);
            this.VerifyBtn.Name = "VerifyBtn";
            this.VerifyBtn.Size = new System.Drawing.Size(114, 32);
            this.VerifyBtn.TabIndex = 57;
            this.VerifyBtn.Text = "Verify";
            this.VerifyBtn.UseVisualStyleBackColor = true;
            this.VerifyBtn.Click += new System.EventHandler(this.VerifyBtn_Click);
            // 
            // PasswordConfirmationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(578, 158);
            this.Controls.Add(this.VerifyBtn);
            this.Controls.Add(this.CurrentPasswordTxt);
            this.Controls.Add(this.Password);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "PasswordConfirmationForm";
            this.Text = "Current Password Confirmation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Password;
        private System.Windows.Forms.TextBox CurrentPasswordTxt;
        private System.Windows.Forms.Button VerifyBtn;
    }
}