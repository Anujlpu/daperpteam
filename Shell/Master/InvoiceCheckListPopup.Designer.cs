﻿namespace Shell.Master
{
    partial class InvoiceCheckListPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ShortageClaimedPanel = new System.Windows.Forms.Panel();
            this.NoClaimedBtn = new System.Windows.Forms.RadioButton();
            this.YesClainedBtn = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.NoShortageBtn = new System.Windows.Forms.RadioButton();
            this.YesShortageBtn = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.NoVerifiedbtn = new System.Windows.Forms.RadioButton();
            this.VerifedyesBtn = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.NoSignatureRadioBtn = new System.Windows.Forms.RadioButton();
            this.SignatureRadioBtnYes = new System.Windows.Forms.RadioButton();
            this.ClaimNoRemakrsTxt = new System.Windows.Forms.RichTextBox();
            this.ClaimRemarksLabel = new System.Windows.Forms.Label();
            this.ShortageClaimedLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.WhoVerifiedTxt = new System.Windows.Forms.TextBox();
            this.WhoVerifiedLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ReceivedByTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.InvoicedatePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.InvoicenumberTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.ShortageClaimedPanel.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.BtnSave);
            this.groupBox1.Controls.Add(this.ShortageClaimedPanel);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.ClaimNoRemakrsTxt);
            this.groupBox1.Controls.Add(this.ClaimRemarksLabel);
            this.groupBox1.Controls.Add(this.ShortageClaimedLabel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.WhoVerifiedTxt);
            this.groupBox1.Controls.Add(this.WhoVerifiedLabel);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.ReceivedByTxt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.InvoicedatePicker);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.InvoicenumberTxt);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(795, 442);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Invoice Check List";
            // 
            // ShortageClaimedPanel
            // 
            this.ShortageClaimedPanel.Controls.Add(this.NoClaimedBtn);
            this.ShortageClaimedPanel.Controls.Add(this.YesClainedBtn);
            this.ShortageClaimedPanel.Location = new System.Drawing.Point(234, 252);
            this.ShortageClaimedPanel.Name = "ShortageClaimedPanel";
            this.ShortageClaimedPanel.Size = new System.Drawing.Size(200, 31);
            this.ShortageClaimedPanel.TabIndex = 91;
            this.ShortageClaimedPanel.Visible = false;
            // 
            // NoClaimedBtn
            // 
            this.NoClaimedBtn.AutoSize = true;
            this.NoClaimedBtn.Checked = true;
            this.NoClaimedBtn.Location = new System.Drawing.Point(94, 4);
            this.NoClaimedBtn.Name = "NoClaimedBtn";
            this.NoClaimedBtn.Size = new System.Drawing.Size(44, 20);
            this.NoClaimedBtn.TabIndex = 85;
            this.NoClaimedBtn.TabStop = true;
            this.NoClaimedBtn.Text = "No";
            this.NoClaimedBtn.UseVisualStyleBackColor = true;
            this.NoClaimedBtn.CheckedChanged += new System.EventHandler(this.NoClaimedBtn_CheckedChanged);
            // 
            // YesClainedBtn
            // 
            this.YesClainedBtn.AutoSize = true;
            this.YesClainedBtn.Location = new System.Drawing.Point(23, 3);
            this.YesClainedBtn.Name = "YesClainedBtn";
            this.YesClainedBtn.Size = new System.Drawing.Size(50, 20);
            this.YesClainedBtn.TabIndex = 84;
            this.YesClainedBtn.Text = "Yes";
            this.YesClainedBtn.UseVisualStyleBackColor = true;
            this.YesClainedBtn.CheckedChanged += new System.EventHandler(this.YesClainedBtn_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.NoShortageBtn);
            this.panel3.Controls.Add(this.YesShortageBtn);
            this.panel3.Location = new System.Drawing.Point(234, 226);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 24);
            this.panel3.TabIndex = 90;
            // 
            // NoShortageBtn
            // 
            this.NoShortageBtn.AutoSize = true;
            this.NoShortageBtn.Checked = true;
            this.NoShortageBtn.Location = new System.Drawing.Point(94, 3);
            this.NoShortageBtn.Name = "NoShortageBtn";
            this.NoShortageBtn.Size = new System.Drawing.Size(44, 20);
            this.NoShortageBtn.TabIndex = 82;
            this.NoShortageBtn.TabStop = true;
            this.NoShortageBtn.Text = "No";
            this.NoShortageBtn.UseVisualStyleBackColor = true;
            // 
            // YesShortageBtn
            // 
            this.YesShortageBtn.AutoSize = true;
            this.YesShortageBtn.Location = new System.Drawing.Point(23, 3);
            this.YesShortageBtn.Name = "YesShortageBtn";
            this.YesShortageBtn.Size = new System.Drawing.Size(50, 20);
            this.YesShortageBtn.TabIndex = 81;
            this.YesShortageBtn.Text = "Yes";
            this.YesShortageBtn.UseVisualStyleBackColor = true;
            this.YesShortageBtn.CheckedChanged += new System.EventHandler(this.YesShortageBtn_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.NoVerifiedbtn);
            this.panel2.Controls.Add(this.VerifedyesBtn);
            this.panel2.Location = new System.Drawing.Point(234, 189);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 27);
            this.panel2.TabIndex = 89;
            // 
            // NoVerifiedbtn
            // 
            this.NoVerifiedbtn.AutoSize = true;
            this.NoVerifiedbtn.Location = new System.Drawing.Point(94, 0);
            this.NoVerifiedbtn.Name = "NoVerifiedbtn";
            this.NoVerifiedbtn.Size = new System.Drawing.Size(44, 20);
            this.NoVerifiedbtn.TabIndex = 79;
            this.NoVerifiedbtn.Text = "No";
            this.NoVerifiedbtn.UseVisualStyleBackColor = true;
            // 
            // VerifedyesBtn
            // 
            this.VerifedyesBtn.AutoSize = true;
            this.VerifedyesBtn.Checked = true;
            this.VerifedyesBtn.Location = new System.Drawing.Point(23, 4);
            this.VerifedyesBtn.Name = "VerifedyesBtn";
            this.VerifedyesBtn.Size = new System.Drawing.Size(50, 20);
            this.VerifedyesBtn.TabIndex = 78;
            this.VerifedyesBtn.TabStop = true;
            this.VerifedyesBtn.Text = "Yes";
            this.VerifedyesBtn.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.NoSignatureRadioBtn);
            this.panel1.Controls.Add(this.SignatureRadioBtnYes);
            this.panel1.Location = new System.Drawing.Point(234, 130);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(192, 25);
            this.panel1.TabIndex = 88;
            // 
            // NoSignatureRadioBtn
            // 
            this.NoSignatureRadioBtn.AutoSize = true;
            this.NoSignatureRadioBtn.Location = new System.Drawing.Point(94, 3);
            this.NoSignatureRadioBtn.Name = "NoSignatureRadioBtn";
            this.NoSignatureRadioBtn.Size = new System.Drawing.Size(44, 20);
            this.NoSignatureRadioBtn.TabIndex = 74;
            this.NoSignatureRadioBtn.Text = "No";
            this.NoSignatureRadioBtn.UseVisualStyleBackColor = true;
            // 
            // SignatureRadioBtnYes
            // 
            this.SignatureRadioBtnYes.AutoSize = true;
            this.SignatureRadioBtnYes.Checked = true;
            this.SignatureRadioBtnYes.Location = new System.Drawing.Point(23, 5);
            this.SignatureRadioBtnYes.Name = "SignatureRadioBtnYes";
            this.SignatureRadioBtnYes.Size = new System.Drawing.Size(50, 20);
            this.SignatureRadioBtnYes.TabIndex = 73;
            this.SignatureRadioBtnYes.TabStop = true;
            this.SignatureRadioBtnYes.Text = "Yes";
            this.SignatureRadioBtnYes.UseVisualStyleBackColor = true;
            // 
            // ClaimNoRemakrsTxt
            // 
            this.ClaimNoRemakrsTxt.Location = new System.Drawing.Point(234, 298);
            this.ClaimNoRemakrsTxt.Name = "ClaimNoRemakrsTxt";
            this.ClaimNoRemakrsTxt.Size = new System.Drawing.Size(282, 75);
            this.ClaimNoRemakrsTxt.TabIndex = 87;
            this.ClaimNoRemakrsTxt.Text = "";
            // 
            // ClaimRemarksLabel
            // 
            this.ClaimRemarksLabel.AutoSize = true;
            this.ClaimRemarksLabel.Location = new System.Drawing.Point(21, 298);
            this.ClaimRemarksLabel.Name = "ClaimRemarksLabel";
            this.ClaimRemarksLabel.Size = new System.Drawing.Size(100, 16);
            this.ClaimRemarksLabel.TabIndex = 86;
            this.ClaimRemarksLabel.Text = "Claim Remarks";
            // 
            // ShortageClaimedLabel
            // 
            this.ShortageClaimedLabel.AutoSize = true;
            this.ShortageClaimedLabel.Location = new System.Drawing.Point(28, 252);
            this.ShortageClaimedLabel.Name = "ShortageClaimedLabel";
            this.ShortageClaimedLabel.Size = new System.Drawing.Size(125, 16);
            this.ShortageClaimedLabel.TabIndex = 83;
            this.ShortageClaimedLabel.Text = "Is shortage claimed";
            this.ShortageClaimedLabel.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 16);
            this.label5.TabIndex = 80;
            this.label5.Text = "Any Shortage";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 16);
            this.label4.TabIndex = 77;
            this.label4.Text = "Has Verified Signature";
            // 
            // WhoVerifiedTxt
            // 
            this.WhoVerifiedTxt.Location = new System.Drawing.Point(234, 161);
            this.WhoVerifiedTxt.Name = "WhoVerifiedTxt";
            this.WhoVerifiedTxt.Size = new System.Drawing.Size(192, 22);
            this.WhoVerifiedTxt.TabIndex = 76;
            // 
            // WhoVerifiedLabel
            // 
            this.WhoVerifiedLabel.AutoSize = true;
            this.WhoVerifiedLabel.Location = new System.Drawing.Point(28, 164);
            this.WhoVerifiedLabel.Name = "WhoVerifiedLabel";
            this.WhoVerifiedLabel.Size = new System.Drawing.Size(85, 16);
            this.WhoVerifiedLabel.TabIndex = 75;
            this.WhoVerifiedLabel.Text = "Who Verified";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 16);
            this.label3.TabIndex = 72;
            this.label3.Text = "Has Recieving Signature";
            // 
            // ReceivedByTxt
            // 
            this.ReceivedByTxt.Location = new System.Drawing.Point(234, 102);
            this.ReceivedByTxt.Name = "ReceivedByTxt";
            this.ReceivedByTxt.Size = new System.Drawing.Size(192, 22);
            this.ReceivedByTxt.TabIndex = 71;
            this.ReceivedByTxt.TextChanged += new System.EventHandler(this.ReceivedByTxt_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 70;
            this.label2.Text = "Received By";
            // 
            // InvoicedatePicker
            // 
            this.InvoicedatePicker.Location = new System.Drawing.Point(234, 69);
            this.InvoicedatePicker.Name = "InvoicedatePicker";
            this.InvoicedatePicker.Size = new System.Drawing.Size(192, 22);
            this.InvoicedatePicker.TabIndex = 69;
            this.InvoicedatePicker.ValueChanged += new System.EventHandler(this.InvoicedatePicker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(28, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 68;
            this.label1.Text = "Invoice Date";
            // 
            // InvoicenumberTxt
            // 
            this.InvoicenumberTxt.Location = new System.Drawing.Point(234, 30);
            this.InvoicenumberTxt.Name = "InvoicenumberTxt";
            this.InvoicenumberTxt.Size = new System.Drawing.Size(192, 22);
            this.InvoicenumberTxt.TabIndex = 67;
            this.InvoicenumberTxt.TextChanged += new System.EventHandler(this.InvoicenumberTxt_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(28, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 15);
            this.label16.TabIndex = 66;
            this.label16.Text = "Invoice Number";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(339, 378);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 27);
            this.btnExit.TabIndex = 176;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial Narrow", 9.25F);
            this.BtnSave.Location = new System.Drawing.Point(234, 379);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 27);
            this.BtnSave.TabIndex = 175;
            this.BtnSave.Text = "Save As";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // InvoiceCheckListPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(820, 467);
            this.Controls.Add(this.groupBox1);
            this.Name = "InvoiceCheckListPopup";
            this.Text = "Invoice CheckList";
            this.Load += new System.EventHandler(this.InvoiceCheckListPopup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ShortageClaimedPanel.ResumeLayout(false);
            this.ShortageClaimedPanel.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox InvoicenumberTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker InvoicedatePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ReceivedByTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton SignatureRadioBtnYes;
        private System.Windows.Forms.RadioButton NoSignatureRadioBtn;
        private System.Windows.Forms.Label WhoVerifiedLabel;
        private System.Windows.Forms.TextBox WhoVerifiedTxt;
        private System.Windows.Forms.RadioButton NoVerifiedbtn;
        private System.Windows.Forms.RadioButton VerifedyesBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton NoShortageBtn;
        private System.Windows.Forms.RadioButton YesShortageBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton NoClaimedBtn;
        private System.Windows.Forms.RadioButton YesClainedBtn;
        private System.Windows.Forms.Label ShortageClaimedLabel;
        private System.Windows.Forms.Label ClaimRemarksLabel;
        private System.Windows.Forms.RichTextBox ClaimNoRemakrsTxt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel ShortageClaimedPanel;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button BtnSave;
    }
}