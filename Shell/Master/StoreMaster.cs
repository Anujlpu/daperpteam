﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using System.Text.RegularExpressions;
using Framework;

namespace Shell
{
    public partial class StoreMaster : BaseForm
    {
        public StoreMaster()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void BindGrid()
        {

            BAL_Store _objStore
                = new BAL_Store();
            dgvStore.DataSource = _objStore.GetStorelist();
            dgvStore.Columns[0].Visible = false;
            dgvStore.Columns["StoreID"].Visible = false;
            dgvStore.Columns["StoreLocationId"].Visible = false;
            dgvStore.Columns["FaxNo"].Visible = false;
            dgvStore.Columns["StoreID"].Visible = false;
            dgvStore.Columns["UserID"].Visible = false;

        }
        string str = "0123456789.";
        private void CtrlKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }
        }

        private void CtrlKeyPress1(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) > 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }
        }

        private void StoreMaster_Load(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                binddropdown();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        public void binddropdown()
        {
            ddlStoreLocation.DisplayMember = "LocationName";
            ddlStoreLocation.ValueMember = "LocationId";
            BAL.BAL_Location _objLocation = new BAL.BAL_Location();
            var list = _objLocation.GetLocationlist();
            list.Insert(0, new DAL.LocationEntity { LocationId = 0, LocationName = "--Select--" });
            ddlStoreLocation.DataSource = list.ToList();
        }

        private void FillStoreNameAndAddress()
        {
            int locationId = Convert.ToInt32(ddlStoreLocation.SelectedValue);
            BAL.BAL_Location _objLocation = new BAL.BAL_Location();
            var location = _objLocation.GetLocationlist().Where(s => s.LocationId == locationId).Single();
            txtStoreName.Text = location.LocationName;
            txtStoreAddress.Text = location.LocationFullAddress;
        }
        public void ClearValue()
        {
            // Random _ram = new Random();
            //txtOutletNo.Text = "SHST" + _ram.Next(999999).ToString();
            txtOutletNo.Text = "";
            txtStoreAddress.Text = "";
            BindGrid();
            btnUpdate.Enabled = false;
            BtnDelete.Enabled = false;
            binddropdown();
            txtPhoneNo.Text = "";
            txtEmailId.Text = "";
            txtFaxNo.Text = "";
            txtStorreBrand.Text = "";
            txtOutletNo.Text = "";
            txtNoofEmp.Text = "0";
            txtNoofContractor.Text = "0";
            
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtOutletNo.Text))
                {
                    MessageBox.Show("Please enter outlet number.");
                    txtOutletNo.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtStoreName.Text))
                {
                    MessageBox.Show("Store name is mandatory.");
                    txtStoreName.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtPhoneNo.Text))
                {
                    MessageBox.Show("Please enter phone number.");
                    txtPhoneNo.Focus();
                    return;
                }
               
                BAL_Store _objStore = new BAL_Store();
                int Result = _objStore.ManageStore(0, txtOutletNo.Text, txtStoreName.Text, txtStoreAddress.Text, int.Parse(ddlStoreLocation.SelectedValue.ToString()), Utility.BA_Commman._userId, 1, txtPhoneNo.Text, txtEmailId.Text, txtFaxNo.Text, txtStorreBrand.Text, txtOutletNo.Text, int.Parse(txtNoofEmp.Text), int.Parse(txtNoofContractor.Text), "", DateTime.Now);
                if (Result == 1)
                {
                    MessageBox.Show("Store created sucessfully");
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");
                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }
        public int StoreId = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtStoreName.Text))
                {
                    MessageBox.Show("Please Enter the Store Name");
                    return;
                }
                if (string.IsNullOrEmpty(txtPhoneNo.Text))
                {
                    MessageBox.Show("Please Enter the PhoneNo");
                    return;
                }

               
                BAL_Store _objStore = new BAL_Store();
                int Result = _objStore.ManageStore(StoreId, txtOutletNo.Text, txtStoreName.Text, txtStoreAddress.Text, int.Parse(ddlStoreLocation.SelectedValue.ToString()), Utility.BA_Commman._userId, 2, txtPhoneNo.Text, txtEmailId.Text, txtFaxNo.Text, txtStorreBrand.Text, txtOutletNo.Text, int.Parse(txtNoofEmp.Text), int.Parse(txtNoofContractor.Text), "", DateTime.Now);
                if (Result == 1)
                {
                    MessageBox.Show("Store Updated Sucessfully");
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dg = MessageBox.Show("Do you want to delete selected record ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    BAL_Store _objStore = new BAL_Store();
                    int Result = _objStore.DeleteStore(StoreId, Utility.BA_Commman._userId);
                    if (Result == 1)
                    {
                        MessageBox.Show("Store Deleted Sucessfully");
                        BindGrid();
                    }
                    else
                    {
                        MessageBox.Show("There is some issue . Please try later!");
                    }
                    ClearValue();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void dgvStore_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {

                    StoreId = int.Parse(dgvStore.Rows[e.RowIndex].Cells["StoreId"].Value.ToString());
                    txtOutletNo.Text = dgvStore.Rows[e.RowIndex].Cells["OutletNumber"].Value.ToString();
                    txtStoreName.Text = dgvStore.Rows[e.RowIndex].Cells["StoreName"].Value.ToString();
                    txtStoreAddress.Text = dgvStore.Rows[e.RowIndex].Cells["StoreAddress"].Value.ToString();
                    txtPhoneNo.Text = dgvStore.Rows[e.RowIndex].Cells["PhoneNo"].Value.ToString();
                    txtEmailId.Text = dgvStore.Rows[e.RowIndex].Cells["EmailId"].Value.ToString();
                    txtFaxNo.Text = dgvStore.Rows[e.RowIndex].Cells["FaxNo"].Value.ToString();
                    txtStorreBrand.Text = dgvStore.Rows[e.RowIndex].Cells["StoreBrand"].Value.ToString();
                    txtNoofEmp.Text = dgvStore.Rows[e.RowIndex].Cells["EmpCount"].Value.ToString();
                    txtNoofContractor.Text = dgvStore.Rows[e.RowIndex].Cells["ContractorCount"].Value.ToString();
                    ddlStoreLocation.SelectedValue = int.Parse(dgvStore.Rows[e.RowIndex].Cells["StoreLocationId"].Value.ToString());
                    btnUpdate.Enabled = true;
                    BtnDelete.Enabled = true;
                }
                else
                {
                    btnUpdate.Enabled = false;
                    BtnDelete.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }

        private void txtEmailId_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!Utility.BA_Commman.ValidateEmail(txtEmailId.Text) == true)
                    MessageBox.Show("Enter valid email Id");
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void txtStoreName_TextChanged(object sender, EventArgs e)
        {

        }

        private void ddlStoreLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlStoreLocation.SelectedIndex > 0)
                {
                    FillStoreNameAndAddress();
                }
                else
                {
                    txtStoreName.Text = string.Empty;
                    txtStoreAddress.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            this.ClearValue();
        }
    }
}
