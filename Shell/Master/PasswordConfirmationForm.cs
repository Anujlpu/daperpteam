﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class PasswordConfirmationForm : BaseForm
    {
        public PasswordConfirmationForm()
        {
            InitializeComponent();
        }

        public frmGasTrans FormRef { get; set; }
        private void VerifyBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string password = Utility.BA_Commman.Encrypt(CurrentPasswordTxt.Text, true);
                using (ShellEntities context = new ShellEntities())
                {
                    var user = (from u in context.MST_UserMaster
                                join
                                r in context.MST_ProfileMaster on u.ProfileId equals r.ProfileId
                                where u.Userid == Utility.BA_Commman._userId && u.Password == password && u.IsActive == true
                                select new
                                {
                                    u.UserName,
                                }).FirstOrDefault();
                    if (user == null)
                    {
                        MessageBox.Show("Your password doesn't match.");
                    }
                    else
                    {
                        this.Close();
                        FormRef.CreateNewEditEODRecord();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
