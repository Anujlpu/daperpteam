﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using Framework;
using Framework.Data;

namespace Shell
{
    public partial class FrmVendorMaster : BaseForm
    {
        public FrmVendorMaster()
        {
            InitializeComponent();
        }

        private void FrmVendorMaster_Load(object sender, EventArgs e)
        {
            try
            {
                Random _ram = new Random();
                txtVendorCode.Text = "SHVEN" + _ram.Next(999999).ToString();
                BindGrid();
                binddropdown();
                ddlApplicationStatus.SelectedItem = "--Select--";
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }
        public void BindGrid()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            dgvVendorList.DataSource = _objVendor.GetVendorlist();
            dgvVendorList.Columns[0].Visible = false;
            dgvVendorList.Columns["StoreEmailPassword"].Visible = false;
            dgvVendorList.Columns["OnlinePortalPassword"].Visible = false;
            dgvVendorList.Columns["PaymentType"].Visible = true;
            dgvVendorList.Columns["Remarks"].Visible = false;
            dgvVendorList.Columns["CreatedBy"].Visible = false;
            dgvVendorList.Columns["ModifiedDate"].Visible = false;
            dgvVendorList.Columns["ModifiedBy"].Visible = false;
        }
        public void binddropdown()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();

            ddlPaymentType.DisplayMember = "PaymenttypeCode";
            ddlPaymentType.ValueMember = "PaymentTypeId";
            ddlPaymentType.DataSource = _objVendor.GetPaymentlist();
        }
        string str = "0123456789.";
        private void CtrlKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }
        }
        public int _vendorid;
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dg = MessageBox.Show("Do you want to delete selected record ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    BAL_Vendor _obj = new BAL_Vendor();
                    int Result = _obj.DeleteVendor(_vendorid);
                    if (Result == 1)
                    {
                        MessageBox.Show("Vendor Deleted Sucessfully");
                        BindGrid();
                    }
                    else
                    {
                        MessageBox.Show("There is some issue . Please try later!");

                    }
                    ClearValue();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }

        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtVendorName.Text))
                {
                    MessageBox.Show("Please Enter the Vendor Name");
                    txtVendorName.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtContactNo.Text))
                {
                    MessageBox.Show("Please Enter the Contact No");
                    txtContactNo.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtMobileNo.Text))
                {
                    MessageBox.Show("Please Enter the SR Mobile No");
                    txtMobileNo.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtAccountNo.Text))
                {
                    MessageBox.Show("Please Enter the Accoount No");
                    txtAccountNo.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtSEName.Text))
                {
                    MessageBox.Show("Please Enter the SE Name");
                    txtSEName.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtDap.Text))
                {
                    MessageBox.Show("Please Enter the DAP Email Id for Invoice");
                    txtDap.Focus();
                    return;
                }
                else if (ddlPaymentType.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select payment type.");
                    return;
                }
                else if (string.IsNullOrEmpty(OnlinePortalPasswordTxt.Text) && !String.IsNullOrEmpty(OnlinePortalUserName.Text))
                {
                    MessageBox.Show("Please enter online portal password.");
                    return;
                }
                else if (string.IsNullOrEmpty(txtPAssword.Text) && !String.IsNullOrEmpty(txtEmailID.Text))
                {
                    MessageBox.Show("Please enter store email password.");
                    return;
                }
                else if (ddlApplicationStatus.SelectedText == "--Select--" || ddlApplicationStatus.Text == "--Select--")
                {
                    MessageBox.Show("Please Select Application Status");
                    ddlApplicationStatus.Focus();
                    return;
                }
                else if (VendorProductCategoryCmb.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select vendor type.");
                    VendorProductCategoryCmb.Focus();
                    return;
                }
                BAL_Vendor _obj = new BAL_Vendor();
                int Result = _obj.ManageVendor(_vendorid, txtVendorCode.Text, txtVendorName.Text, txtContactNo.Text, txtEmailID.Text, OnlinePortalUserName.Text, txtPAssword.Text, txtAccountNo.Text, txtAddress.Text, txtSEName.Text, txtEmailID.Text, txtMobileNo.Text, txtDap.Text, ddlPaymentType.SelectedValue.ToString(), "", Utility.BA_Commman._userId, Utility.BA_Commman._userId, 2, txtonlineportal.Text, OnlinePortalUserName.Text, OnlinePortalPasswordTxt.Text, VendorProductCategoryCmb.Text, ddlApplicationStatus.Text, Convert.ToDateTime(dtpAppDate.Text));
                if (Result == 1)
                {
                    MessageBox.Show("Vendor Updated Sucessfully");
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");
                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtVendorName.Text))
                {
                    MessageBox.Show("Please Enter the Vendor Name");
                    txtVendorName.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtContactNo.Text))
                {
                    MessageBox.Show("Please Enter the Contact No");
                    txtContactNo.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtMobileNo.Text))
                {
                    MessageBox.Show("Please Enter the SR Mobile No");
                    txtMobileNo.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtAccountNo.Text))
                {
                    MessageBox.Show("Please Enter the Accoount No");
                    txtAccountNo.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtSEName.Text))
                {
                    MessageBox.Show("Please Enter the SE Name");
                    txtSEName.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(txtDap.Text))
                {
                    MessageBox.Show("Please Enter the DAP Email Id for Invoice");
                    txtDap.Focus();
                    return;
                }
                else if (ddlPaymentType.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select payment type.");
                    return;
                }
                else if (string.IsNullOrEmpty(OnlinePortalPasswordTxt.Text) && !String.IsNullOrEmpty(OnlinePortalUserName.Text))
                {
                    MessageBox.Show("Please enter online portal password.");
                    return;
                }
                else if (string.IsNullOrEmpty(txtPAssword.Text) && !String.IsNullOrEmpty(txtEmailID.Text))
                {
                    MessageBox.Show("Please enter store email password.");
                    return;
                }
                else if (ddlApplicationStatus.SelectedText == "--Select--" || ddlApplicationStatus.Text == "--Select--")
                {
                    MessageBox.Show("Please select aaplication status.");
                    ddlApplicationStatus.Focus();
                    return;
                }
                else if (VendorProductCategoryCmb.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select vendor type.");
                    VendorProductCategoryCmb.Focus();
                    return;
                }
                BAL_Vendor _obj = new BAL_Vendor();
                int Result = _obj.ManageVendor(0, txtVendorCode.Text, txtVendorName.Text, txtContactNo.Text, txtEmailID.Text, OnlinePortalUserName.Text, txtPAssword.Text, txtAccountNo.Text, txtAddress.Text, txtSEName.Text, txtEmailID.Text, txtMobileNo.Text, txtDap.Text, ddlPaymentType.SelectedValue.ToString(), "", Utility.BA_Commman._userId, Utility.BA_Commman._userId, 1, txtonlineportal.Text, OnlinePortalUserName.Text, OnlinePortalPasswordTxt.Text, VendorProductCategoryCmb.Text, ddlApplicationStatus.Text, Convert.ToDateTime(dtpAppDate.Text));
                if (Result == 1)
                {
                    MessageBox.Show("Vendor Created Sucessfully");
                    BindGrid();
                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }
        private void dgvVendorList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public void ClearValue()
        {
            Random _ram = new Random();
            txtVendorCode.Text = "SHVEN" + _ram.Next(999999).ToString();
            txtVendorName.Text = "";
            txtContactNo.Text = "";
            txtEmailID.Text = "";
            OnlinePortalUserName.Text = "";
            txtPAssword.Text = "";
            txtAccountNo.Text = "";
            txtAddress.Text = "";
            txtSEName.Text = "";
            txtSEEMailId.Text = "";
            txtonlineportal.Text = "";
            txtMobileNo.Text = "";
            txtDap.Text = "";
            binddropdown();
            BindGrid();
            BtnDelete.Enabled = false;
            button1.Enabled = false;
            BtnSave.Enabled = true;
            ddlApplicationStatus.SelectedText = "--Select--";
            VendorProductCategoryCmb.SelectedIndex = 0;
            ddlApplicationStatus.SelectedIndex = 0;
            OnlinePortalPasswordTxt.Text = string.Empty;

        }
        private void dgvVendorList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {

                    txtVendorCode.Text = dgvVendorList.Rows[e.RowIndex].Cells["VendorCode"].Value.ToString();
                    txtVendorName.Text = dgvVendorList.Rows[e.RowIndex].Cells["VendorName"].Value.ToString();
                    txtContactNo.Text = dgvVendorList.Rows[e.RowIndex].Cells["ContactNo"].Value.ToString();
                    txtEmailID.Text = dgvVendorList.Rows[e.RowIndex].Cells["EmailId"].Value.ToString();
                    OnlinePortalUserName.Text = dgvVendorList.Rows[e.RowIndex].Cells["OnlinePortalUserName"].Value.ToString();
                    // txtPAssword.Text = string.Empty;
                    txtAccountNo.Text = dgvVendorList.Rows[e.RowIndex].Cells["AccountNo"].Value.ToString();
                    txtAddress.Text = dgvVendorList.Rows[e.RowIndex].Cells["Address"].Value.ToString();
                    txtSEName.Text = dgvVendorList.Rows[e.RowIndex].Cells["SEName"].Value.ToString();
                    txtSEEMailId.Text = dgvVendorList.Rows[e.RowIndex].Cells["SEEmailId"].Value.ToString();
                    txtMobileNo.Text = dgvVendorList.Rows[e.RowIndex].Cells["SEMobileNo"].Value.ToString();
                    txtDap.Text = dgvVendorList.Rows[e.RowIndex].Cells["DAPEmailId"].Value.ToString();
                    ddlPaymentType.SelectedValue = Convert.ToInt32(dgvVendorList.Rows[e.RowIndex].Cells["PaymentType"].Value.ToString());
                    _vendorid = Convert.ToInt32(dgvVendorList.Rows[e.RowIndex].Cells["vendorid"].Value);
                    txtonlineportal.Text = Convert.ToString(dgvVendorList.Rows[e.RowIndex].Cells["OnlinePortal"].Value);
                    VendorProductCategoryCmb.Text = Convert.ToString(dgvVendorList.Rows[e.RowIndex].Cells["VendorType"].Value);
                    ddlApplicationStatus.Text = Convert.ToString(dgvVendorList.Rows[e.RowIndex].Cells["ApplicationStatus"].Value);
                    dtpAppDate.Text = Convert.ToString(dgvVendorList.Rows[e.RowIndex].Cells["ApplicationDate"].Value);
                    //OnlinePortalPasswordTxt.Text = string.Empty;
                    BtnDelete.Enabled = true;
                    button1.Enabled = true;
                    BtnSave.Enabled = false;
                    using (ShellEntities context = new ShellEntities())
                    {
                        var currentPassword = context.Mst_Vendor.FirstOrDefault(u => u.VendorCode == txtVendorCode.Text);
                        if (currentPassword != null)
                        {
                            txtPAssword.Text = currentPassword.Password;
                            txtPAssword.Text = Utility.BA_Commman.Decrypt(currentPassword.Password, true);
                            OnlinePortalPasswordTxt.Text = currentPassword.OnlinePortalPassword;
                            OnlinePortalPasswordTxt.Text = Utility.BA_Commman.Decrypt(currentPassword.OnlinePortalPassword, true);
                        }
                    }
                }
                else
                {
                    BtnDelete.Enabled = false;
                    button1.Enabled = false;
                    BtnSave.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }
        private void txtEmailID_Leave(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
                {
                    ((TextBoxBase)(sender)).Text = String.Empty;

                }
                else if (!string.IsNullOrEmpty(((TextBoxBase)(sender)).Text) && !Utility.BA_Commman.ValidateEmail(((TextBoxBase)(sender)).Text) == true)
                {
                    MessageBox.Show("Enter valid Email Id");
                    ((TextBoxBase)(sender)).Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void dtpAppDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            this.ClearValue();
        }
    }
}
