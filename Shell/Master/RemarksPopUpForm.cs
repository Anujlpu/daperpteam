﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class RemarksPopUpForm : BaseForm
    {
        public frmGasTrans FormRef { get; set; }
        public RemarksPopUpForm()
        {
            InitializeComponent();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(RemarksTxt.Text))
                {
                    SubmitEODForm();
                }
                else
                {
                    MessageBox.Show("You must have to enter remarks..!");
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public bool IsReferBack = false;
        public bool IsEscalated = false;
        public string AdminReferBackStatus = "";
        public string FormStatus = "";
        private string ApprovalStatus()
        {
            if (Utility.BA_Commman._Profile == "FE")
            {
                return ERPEODApprovalStatus.Submitted;
            }
            else if (Utility.BA_Commman._Profile == "BE" && !IsReferBack)
            {
                return ERPEODApprovalStatus.Completed;
            }
            else if (Utility.BA_Commman._Profile == "Auditor" && IsEscalated)
            {
                return ERPEODApprovalStatus.Escalated;
            }
            else if (Utility.BA_Commman._Profile == "Auditor" && !IsReferBack && !IsEscalated)
            {
                return ERPEODApprovalStatus.Closed;
            }
            else if ( IsReferBack && ((Utility.BA_Commman._Profile == "SupAdm" || Utility.BA_Commman._Profile == "Adm")))
            {
                return AdminReferBackStatus;
            }
            else if (IsReferBack)
            {
                return ERPEODApprovalStatus.ReferBack;
            }
            else
            {
                return ERPEODApprovalStatus.Reopen;
            }
        }
        private void MaintainApprovalHistory(int transactionid, int profileId)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    TBL_EODRemarks summary = new TBL_EODRemarks();
                    summary.FuelTransactionID = transactionid;
                    summary.Remarks = RemarksTxt.Text;
                    summary.ProfileId = profileId;
                    summary.IsActive = true;
                    summary.CreatedBy = Utility.BA_Commman._userId;
                    summary.CreatedDate = DateTime.Now;
                    summary.IsFinalApprovalDone = false;
                    summary.IsClosed = false;
                    summary.IsDone = true;
                    summary.ApprovalStatus = ApprovalStatus();
                    context.TBL_EODRemarks.Add(summary);
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        public void SubmitEODForm()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var eodData = context.MST_FuelTransaction.FirstOrDefault(s => s.TransactionDate == FormRef.TransactionDate);
                var user = (from u in context.MST_UserMaster
                            join
                            r in context.MST_ProfileMaster on u.ProfileId equals r.ProfileId
                            where u.Userid == Utility.BA_Commman._userId && u.IsActive == true
                            select new
                            {
                                u.UserName,
                                u.Userid,
                                r.ProfileId
                            }).FirstOrDefault();
                if (eodData != null)
                {
                    MaintainApprovalHistory(eodData.GasTransactionID, user.ProfileId);
                    if (ApprovalStatus() == ERPEODApprovalStatus.Submitted || FormStatus== ERPEODApprovalStatus.ReferBack || FormStatus == ERPEODApprovalStatus.Reopen || ApprovalStatus() == ERPEODApprovalStatus.ReferBack ||ApprovalStatus() != ERPEODApprovalStatus.Completed)
                    {
                        FormRef.CreateNewEditEODRecord();
                    }
                    eodData.SubmissionRemarks = RemarksTxt.Text;
                    eodData.IsSubmitted = true;
                    eodData.SubmittedRole = Utility.BA_Commman._Profile;
                    eodData.SubmittedRoleId = user.ProfileId;
                    context.SaveChanges();
                    //MessageBox.Show("EOD record " + ApprovalStatus() + ".");
                    this.Close();
                    FormRef.Close();
                }
            }
        }

        private void RemarksForm_Load(object sender, EventArgs e)
        {
           
        }
    }
}
