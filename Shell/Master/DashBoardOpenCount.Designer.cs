﻿namespace Shell.Master
{
    partial class DashBoardOpenCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InvoiceGroupBox = new System.Windows.Forms.GroupBox();
            this.dgvOpenInvoices = new System.Windows.Forms.DataGridView();
            this.View = new System.Windows.Forms.DataGridViewLinkColumn();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.InvoiceGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOpenInvoices)).BeginInit();
            this.SuspendLayout();
            // 
            // InvoiceGroupBox
            // 
            this.InvoiceGroupBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.InvoiceGroupBox.Controls.Add(this.dgvOpenInvoices);
            this.InvoiceGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InvoiceGroupBox.Location = new System.Drawing.Point(163, 23);
            this.InvoiceGroupBox.Name = "InvoiceGroupBox";
            this.InvoiceGroupBox.Size = new System.Drawing.Size(683, 278);
            this.InvoiceGroupBox.TabIndex = 13;
            this.InvoiceGroupBox.TabStop = false;
            this.InvoiceGroupBox.Text = "Ticket Details";
            // 
            // dgvOpenInvoices
            // 
            this.dgvOpenInvoices.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvOpenInvoices.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgvOpenInvoices.BackgroundColor = System.Drawing.Color.Peru;
            this.dgvOpenInvoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOpenInvoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.View});
            this.dgvOpenInvoices.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvOpenInvoices.Location = new System.Drawing.Point(42, 25);
            this.dgvOpenInvoices.Name = "dgvOpenInvoices";
            this.dgvOpenInvoices.Size = new System.Drawing.Size(601, 247);
            this.dgvOpenInvoices.TabIndex = 13;
            // 
            // View
            // 
            this.View.ActiveLinkColor = System.Drawing.Color.DimGray;
            this.View.HeaderText = "Goto";
            this.View.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.View.Name = "View";
            this.View.Text = "Go to";
            this.View.ToolTipText = "Go to Sales Data Form";
            this.View.UseColumnTextForLinkValue = true;
            this.View.VisitedLinkColor = System.Drawing.Color.Gray;
            // 
            // ExitBtn
            // 
            this.ExitBtn.Location = new System.Drawing.Point(434, 439);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(88, 31);
            this.ExitBtn.TabIndex = 14;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = true;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(790, 448);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(86, 13);
            this.linkLabel1.TabIndex = 15;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Copy right - Shell";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // DashBoardOpenCount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(963, 497);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.InvoiceGroupBox);
            this.Name = "DashBoardOpenCount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "My DashBoard";
            this.Load += new System.EventHandler(this.DashBoardOpenCount_Load);
            this.InvoiceGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOpenInvoices)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox InvoiceGroupBox;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.DataGridView dgvOpenInvoices;
        private System.Windows.Forms.DataGridViewLinkColumn View;
    }
}