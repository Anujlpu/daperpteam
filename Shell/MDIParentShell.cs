﻿using Framework.Data;
using Shell.Master;
using Shell.Report;
using Shell.Transaction;
using System;
using System.Windows.Forms;
using System.Linq;
using Framework;
using Shell.Audit;
using System.Drawing;
using Shell.Employee;

namespace Shell
{
    public partial class MDIParentShell : BaseForm
    {
        private int childFormNumber = 0;

        public MDIParentShell()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            LocationMaster childForm = new LocationMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void ParentColorConfig()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var parentConfig = context.TBL_ParentFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                    if (parentConfig != null)
                    {
                        foreach (Control control in this.Controls)
                        {
                            // #2
                            MdiClient client = control as MdiClient;
                            if (!(client == null))
                            {
                                client.BackColor = Color.FromName(parentConfig.BackgroundColor);
                                float fl = (float)parentConfig.FontSize;
                                FontStyle fontStyle = (FontStyle)Enum.Parse(typeof(FontStyle), parentConfig.FontStyle, true);
                                Font childFont = new Font(parentConfig.FontName, fl, fontStyle);
                                client.ForeColor = Color.FromName(parentConfig.FontColor);
                                // 4#
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator." + ex.Message);
            }
        }

        private void ChildColorConfig(Form childForm)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var childConfig = context.TBL_ChilFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                    if (childConfig != null)
                    {
                        childForm.BackColor = Color.FromName(childConfig.BackgroundColor);
                        float fl = (float)childConfig.FontSize;
                        FontStyle fontStyle = (FontStyle)Enum.Parse(typeof(FontStyle), childConfig.FontStyle, true);
                        Font childFont = new Font(childConfig.FontName, fl, fontStyle);
                        childForm.ForeColor = Color.FromName(childConfig.FontColor);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator." + ex.Message);
            }
        }

        private void ParentLoadConfig()
        {
            DashBoardOpenCount childForm = new DashBoardOpenCount();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
            lblUserName.Text = "Logedin User : " + Utility.BA_Commman._UserName;

            // NA MENU START
            CategoryToolMenuStrip.Visible = false;
            manageProductToolStripMenuItem.Visible = false;
            managePriceToolStripMenuItem.Visible = false;
            manageMarginToolStripMenuItem.Visible = false;

            // NA END 

            lblUserName.Text = Utility.BA_Commman.titlecase(lblUserName.Text);
            timer1.Start();
            if (Utility.BA_Commman._Profile == "Adm" || Utility.BA_Commman._Profile == "SupAdm")
            {

                createUserToolStripMenuItem.Visible = true;
                userPasswordChangeToolStripMenuItem.Visible = true;
                employeeSalaryToolStripMenuItem.Visible = true;
                manageSerialMenu.Visible = true;
                emailConfigurationToolStripMenuItem.Visible = true;
            }
            else
            {
                createUserToolStripMenuItem.Visible = false;
                userPasswordChangeToolStripMenuItem.Visible = false;
                employeeSalaryToolStripMenuItem.Visible = false;
                manageSerialMenu.Visible = false;
                emailConfigurationToolStripMenuItem.Visible = false;
            }
            ParentColorConfig();
        }
        private void MDIParent1_Load(object sender, EventArgs e)
        {
            try
            {
                ParentLoadConfig();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }

        }

        private void manageStoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StoreMaster childForm = new StoreMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void manageProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductMaster childForm = new ProductMaster();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Start();
            timer1.Interval = 50;
            timer1.Enabled = true;
            lblTime.Text = System.DateTime.Now.ToString("hh:mm:ss tt");

        }

        private void gasTransationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGasTrans childForm = new frmGasTrans();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void uploadSaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FrmBrandSales childForm = new FrmBrandSales();
            //childForm.MdiParent = this;
            //childForm.Show();
        }

        private void manageEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEmployeeMaster childForm = new FrmEmployeeMaster();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void manageRolesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmRoles childForm = new FrmRoles();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void managePriceToolStripMenuItem_Click(object sender, EventArgs e)
        {

            FrmPriceMaster childForm = new FrmPriceMaster();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void manageMarginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMarginMaster childForm = new FrmMarginMaster();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void vendorManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmVendorMaster childForm = new FrmVendorMaster();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void uploadDocumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDocumentMove childForm = new FrmDocumentMove();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void lblticket_Click(object sender, EventArgs e)
        {
            FrmViewTicket childForm = new FrmViewTicket();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void paymnetToVendorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPayment childForm = new FrmPayment();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void paymentToEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPaymnetToEmployee childForm = new FrmPaymnetToEmployee(2);
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void paymentToManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPaymnetToEmployee childForm = new FrmPaymnetToEmployee(1);
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);

        }

        private void employeeSalaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSalaryCreation childForm = new FrmSalaryCreation();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void createUserToolStripMenuItem_Click(object sender, EventArgs e)
        {

            CreateUser childForm = new CreateUser();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword childForm = new ChangePassword();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void lnkLogoff_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            DialogResult dgResult = MessageBox.Show("Do you Want Log Off", "", MessageBoxButtons.YesNo);
            if (dgResult == DialogResult.Yes)
            {
                foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcessesByName("Shell.exe"))
                {
                    process.Kill();
                }
                Application.Restart();
            }
            else
            {
                //this.Close();

            }
        }

        private void dashBoardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DashBoardOpenCount childForm = new DashBoardOpenCount();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void transactionViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GasTransactionViewForm gasForm = new GasTransactionViewForm();
            gasForm.ShowDialog();
            ChildColorConfig(gasForm);
        }

        private void pNLMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PNLType childForm = new PNLType();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void invoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InvoiceEntry childForm = new InvoiceEntry();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void bankEntriesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void bankValidationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BankValidation childForm = new BankValidation();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void employeeReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BankInfoMaster childForm = new BankInfoMaster();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void pNLEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PNLEntryForm childForm = new PNLEntryForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void bankAndPNLValidationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PNLBankValidation childForm = new PNLBankValidation();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void CountInvoice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            BankValidation childForm = new BankValidation();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);

        }

        private void CountPNL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PNLBankValidation childForm = new PNLBankValidation();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CategoriesMaster childForm = new CategoriesMaster();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            EODManagementForm childForm = new EODManagementForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void invoiceReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void invoiceReportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            InvoiceReport childForm = new InvoiceReport();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void gasTransactionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GasTransactionViewForm gasForm = new GasTransactionViewForm();
            gasForm.ShowDialog();
            ChildColorConfig(gasForm);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void pNLReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PNLReport report = new PNLReport();
            report.MdiParent = this;
            report.Show();
            ChildColorConfig(report);
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            GasTrasTicket report = new GasTrasTicket();
            report.MdiParent = this;
            report.Show();
            ChildColorConfig(report);
        }

        private void gasAuditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TicketSearchForm report = new TicketSearchForm();
            report.MdiParent = this;
            report.Show();
            ChildColorConfig(report);
        }

        private void auditorViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AuditMasterForm report = new AuditMasterForm();
            report.MdiParent = this;
            report.Show();
            ChildColorConfig(report);
        }

        private void bankStatementEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BankEntry childForm = new BankEntry();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void userPasswordChangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePasswordMaster childForm = new ChangePasswordMaster();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void lottoWeeklyInvoiceEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LottoWeeklyForm childForm = new LottoWeeklyForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void lottoWeeklyReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LottoWeeklyReportForm childForm = new LottoWeeklyReportForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void configureFolderKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ConfigureFolderPathForm childForm = new ConfigureFolderPathForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void themeColorConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {

            SystemColorConfiguration childForm = new SystemColorConfiguration();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void lottoMasterInventoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LottoMasterForm childForm = new LottoMasterForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);

        }

        private void taboccoInventoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TaboccoMasterInventoryForm childForm = new TaboccoMasterInventoryForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);

        }

        private void lottoMasterInvReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LottoMasterInventoryReport childForm = new LottoMasterInventoryReport();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            ProductSerialKeyGenerator childForm = new ProductSerialKeyGenerator();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void tobaccoMasterInvReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TobaccoInvReportForm childForm = new TobaccoInvReportForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void employeePayRollToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEmployeePayRoll childForm = new FrmEmployeePayRoll();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void employeeReportToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            EmployeeReportForm childForm = new EmployeeReportForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void employeeSalaryReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void emailConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmailNotificationConfigurationForm childForm = new EmailNotificationConfigurationForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void toolStripMenuItem5_Click_1(object sender, EventArgs e)
        {
            MilkEODForm childForm = new MilkEODForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            EODMilkReportForm childForm = new EODMilkReportForm();
            childForm.MdiParent = this;
            childForm.Show();
            ChildColorConfig(childForm);
        }
    }
}
