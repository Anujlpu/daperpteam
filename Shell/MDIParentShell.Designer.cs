﻿namespace Shell
{
    partial class MDIParentShell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParentShell));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.userManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userPasswordChangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.manageStoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pNLMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dashBoardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CategoryToolMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.manageProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageSerialMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.managePriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageMarginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureFolderKeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.themeColorConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.manageEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.employeeSalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeePayRollToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadDocumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gasTransationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.cashManagemnetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymnetToVendorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.watcherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pNLEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankStatementEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lottoMasterInventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taboccoInventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankValidationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankAndPNLValidationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auditMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.gasAuditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auditMasterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.auditorViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.approveGasTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saleReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gasTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeSalaryReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lottoMasterInvReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tobaccoMasterInvReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eODReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pNLReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.lblUserName = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lblTime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lnkLogoff = new System.Windows.Forms.LinkLabel();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.AliceBlue;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.viewMenu,
            this.bankToolStripMenuItem,
            this.auditMasterToolStripMenuItem,
            this.auditMasterToolStripMenuItem1,
            this.toolsMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(1043, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userManagementToolStripMenuItem,
            this.newToolStripMenuItem,
            this.toolStripSeparator3,
            this.manageStoreToolStripMenuItem,
            this.vendorManagementToolStripMenuItem,
            this.toolStripSeparator4,
            this.toolStripMenuItem1,
            this.pNLMasterToolStripMenuItem,
            this.dashBoardToolStripMenuItem,
            this.CategoryToolMenuStrip,
            this.manageProductToolStripMenuItem,
            this.manageSerialMenu,
            this.managePriceToolStripMenuItem,
            this.manageMarginToolStripMenuItem,
            this.toolStripSeparator1,
            this.configurationToolStripMenuItem,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(55, 20);
            this.fileMenu.Text = "&Master";
            // 
            // userManagementToolStripMenuItem
            // 
            this.userManagementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createUserToolStripMenuItem,
            this.changePasswordToolStripMenuItem,
            this.userPasswordChangeToolStripMenuItem});
            this.userManagementToolStripMenuItem.Name = "userManagementToolStripMenuItem";
            this.userManagementToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.userManagementToolStripMenuItem.Text = "User Management";
            // 
            // createUserToolStripMenuItem
            // 
            this.createUserToolStripMenuItem.Name = "createUserToolStripMenuItem";
            this.createUserToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.createUserToolStripMenuItem.Text = "User Management";
            this.createUserToolStripMenuItem.Click += new System.EventHandler(this.createUserToolStripMenuItem_Click);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.changePasswordToolStripMenuItem.Text = "Change My Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // userPasswordChangeToolStripMenuItem
            // 
            this.userPasswordChangeToolStripMenuItem.Name = "userPasswordChangeToolStripMenuItem";
            this.userPasswordChangeToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.userPasswordChangeToolStripMenuItem.Text = "User Password Management";
            this.userPasswordChangeToolStripMenuItem.Click += new System.EventHandler(this.userPasswordChangeToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShowShortcutKeys = false;
            this.newToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.newToolStripMenuItem.Text = "&Manage Location";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.ShowNewForm);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(182, 6);
            // 
            // manageStoreToolStripMenuItem
            // 
            this.manageStoreToolStripMenuItem.Name = "manageStoreToolStripMenuItem";
            this.manageStoreToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.manageStoreToolStripMenuItem.Text = "Manage Store";
            this.manageStoreToolStripMenuItem.Click += new System.EventHandler(this.manageStoreToolStripMenuItem_Click);
            // 
            // vendorManagementToolStripMenuItem
            // 
            this.vendorManagementToolStripMenuItem.Name = "vendorManagementToolStripMenuItem";
            this.vendorManagementToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.vendorManagementToolStripMenuItem.Text = "Vendor Management";
            this.vendorManagementToolStripMenuItem.Click += new System.EventHandler(this.vendorManagementToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(182, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.toolStripMenuItem1.Text = "Bank Master";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // pNLMasterToolStripMenuItem
            // 
            this.pNLMasterToolStripMenuItem.Name = "pNLMasterToolStripMenuItem";
            this.pNLMasterToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.pNLMasterToolStripMenuItem.Text = "PNL Master";
            this.pNLMasterToolStripMenuItem.Click += new System.EventHandler(this.pNLMasterToolStripMenuItem_Click);
            // 
            // dashBoardToolStripMenuItem
            // 
            this.dashBoardToolStripMenuItem.Name = "dashBoardToolStripMenuItem";
            this.dashBoardToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.dashBoardToolStripMenuItem.Text = "My DashBoard";
            this.dashBoardToolStripMenuItem.Click += new System.EventHandler(this.dashBoardToolStripMenuItem_Click);
            // 
            // CategoryToolMenuStrip
            // 
            this.CategoryToolMenuStrip.Name = "CategoryToolMenuStrip";
            this.CategoryToolMenuStrip.Size = new System.Drawing.Size(185, 22);
            this.CategoryToolMenuStrip.Text = "Manage Category";
            this.CategoryToolMenuStrip.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // manageProductToolStripMenuItem
            // 
            this.manageProductToolStripMenuItem.Name = "manageProductToolStripMenuItem";
            this.manageProductToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.manageProductToolStripMenuItem.Text = "Manage Product";
            this.manageProductToolStripMenuItem.Click += new System.EventHandler(this.manageProductToolStripMenuItem_Click);
            // 
            // manageSerialMenu
            // 
            this.manageSerialMenu.Name = "manageSerialMenu";
            this.manageSerialMenu.Size = new System.Drawing.Size(185, 22);
            this.manageSerialMenu.Text = "Manage Serial Keys";
            this.manageSerialMenu.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // managePriceToolStripMenuItem
            // 
            this.managePriceToolStripMenuItem.Name = "managePriceToolStripMenuItem";
            this.managePriceToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.managePriceToolStripMenuItem.Text = "Manage Price";
            this.managePriceToolStripMenuItem.Click += new System.EventHandler(this.managePriceToolStripMenuItem_Click);
            // 
            // manageMarginToolStripMenuItem
            // 
            this.manageMarginToolStripMenuItem.Name = "manageMarginToolStripMenuItem";
            this.manageMarginToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.manageMarginToolStripMenuItem.Text = "Manage Margin";
            this.manageMarginToolStripMenuItem.Click += new System.EventHandler(this.manageMarginToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(182, 6);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configureFolderKeyToolStripMenuItem,
            this.themeColorConfigurationToolStripMenuItem,
            this.emailConfigurationToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.configurationToolStripMenuItem.Text = "Configuration";
            // 
            // configureFolderKeyToolStripMenuItem
            // 
            this.configureFolderKeyToolStripMenuItem.Name = "configureFolderKeyToolStripMenuItem";
            this.configureFolderKeyToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.configureFolderKeyToolStripMenuItem.Text = "Configure Folders";
            this.configureFolderKeyToolStripMenuItem.Click += new System.EventHandler(this.configureFolderKeyToolStripMenuItem_Click);
            // 
            // themeColorConfigurationToolStripMenuItem
            // 
            this.themeColorConfigurationToolStripMenuItem.Name = "themeColorConfigurationToolStripMenuItem";
            this.themeColorConfigurationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.themeColorConfigurationToolStripMenuItem.Text = "Color Configuration";
            this.themeColorConfigurationToolStripMenuItem.Click += new System.EventHandler(this.themeColorConfigurationToolStripMenuItem_Click);
            // 
            // emailConfigurationToolStripMenuItem
            // 
            this.emailConfigurationToolStripMenuItem.Name = "emailConfigurationToolStripMenuItem";
            this.emailConfigurationToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.emailConfigurationToolStripMenuItem.Text = "Email Configuration";
            this.emailConfigurationToolStripMenuItem.Click += new System.EventHandler(this.emailConfigurationToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(182, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageEmployeeToolStripMenuItem,
            this.toolStripSeparator7,
            this.employeeSalaryToolStripMenuItem,
            this.employeePayRollToolStripMenuItem});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(142, 20);
            this.editMenu.Text = "&EmployeeManagement";
            // 
            // manageEmployeeToolStripMenuItem
            // 
            this.manageEmployeeToolStripMenuItem.Name = "manageEmployeeToolStripMenuItem";
            this.manageEmployeeToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.manageEmployeeToolStripMenuItem.Text = "Manage Employee";
            this.manageEmployeeToolStripMenuItem.Click += new System.EventHandler(this.manageEmployeeToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(169, 6);
            // 
            // employeeSalaryToolStripMenuItem
            // 
            this.employeeSalaryToolStripMenuItem.Name = "employeeSalaryToolStripMenuItem";
            this.employeeSalaryToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.employeeSalaryToolStripMenuItem.Text = "Employee Salary";
            this.employeeSalaryToolStripMenuItem.Click += new System.EventHandler(this.employeeSalaryToolStripMenuItem_Click);
            // 
            // employeePayRollToolStripMenuItem
            // 
            this.employeePayRollToolStripMenuItem.Name = "employeePayRollToolStripMenuItem";
            this.employeePayRollToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.employeePayRollToolStripMenuItem.Text = "Employee PayRoll";
            this.employeePayRollToolStripMenuItem.Click += new System.EventHandler(this.employeePayRollToolStripMenuItem_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uploadDocumentsToolStripMenuItem,
            this.gasTransationToolStripMenuItem,
            this.toolStripMenuItem5,
            this.toolStripMenuItem3,
            this.cashManagemnetToolStripMenuItem,
            this.watcherToolStripMenuItem,
            this.memoToolStripMenuItem,
            this.invoiceToolStripMenuItem,
            this.pNLEntryToolStripMenuItem,
            this.bankStatementEntryToolStripMenuItem,
            this.lottoMasterInventoryToolStripMenuItem,
            this.taboccoInventoryToolStripMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(80, 20);
            this.viewMenu.Text = "&Transaction";
            // 
            // uploadDocumentsToolStripMenuItem
            // 
            this.uploadDocumentsToolStripMenuItem.Name = "uploadDocumentsToolStripMenuItem";
            this.uploadDocumentsToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.uploadDocumentsToolStripMenuItem.Text = "Document Upload Master";
            this.uploadDocumentsToolStripMenuItem.Click += new System.EventHandler(this.uploadDocumentsToolStripMenuItem_Click);
            // 
            // gasTransationToolStripMenuItem
            // 
            this.gasTransationToolStripMenuItem.Name = "gasTransationToolStripMenuItem";
            this.gasTransationToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.gasTransationToolStripMenuItem.Text = "Sales Data Form";
            this.gasTransationToolStripMenuItem.Click += new System.EventHandler(this.gasTransationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(210, 22);
            this.toolStripMenuItem5.Text = "Milk EOD Management";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click_1);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(210, 22);
            this.toolStripMenuItem3.Text = "EOD Management";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // cashManagemnetToolStripMenuItem
            // 
            this.cashManagemnetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paymnetToVendorToolStripMenuItem,
            this.paymentToEmployeeToolStripMenuItem,
            this.paymentToManagementToolStripMenuItem});
            this.cashManagemnetToolStripMenuItem.Name = "cashManagemnetToolStripMenuItem";
            this.cashManagemnetToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.cashManagemnetToolStripMenuItem.Text = "Cash Managemnet";
            // 
            // paymnetToVendorToolStripMenuItem
            // 
            this.paymnetToVendorToolStripMenuItem.Name = "paymnetToVendorToolStripMenuItem";
            this.paymnetToVendorToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.paymnetToVendorToolStripMenuItem.Text = "Payment To Vendor";
            this.paymnetToVendorToolStripMenuItem.Click += new System.EventHandler(this.paymnetToVendorToolStripMenuItem_Click);
            // 
            // paymentToEmployeeToolStripMenuItem
            // 
            this.paymentToEmployeeToolStripMenuItem.Name = "paymentToEmployeeToolStripMenuItem";
            this.paymentToEmployeeToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.paymentToEmployeeToolStripMenuItem.Text = "Payment To Employee";
            this.paymentToEmployeeToolStripMenuItem.Click += new System.EventHandler(this.paymentToEmployeeToolStripMenuItem_Click);
            // 
            // paymentToManagementToolStripMenuItem
            // 
            this.paymentToManagementToolStripMenuItem.Name = "paymentToManagementToolStripMenuItem";
            this.paymentToManagementToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.paymentToManagementToolStripMenuItem.Text = "Payment To Management";
            this.paymentToManagementToolStripMenuItem.Click += new System.EventHandler(this.paymentToManagementToolStripMenuItem_Click);
            // 
            // watcherToolStripMenuItem
            // 
            this.watcherToolStripMenuItem.Name = "watcherToolStripMenuItem";
            this.watcherToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.watcherToolStripMenuItem.Text = "Watcher";
            // 
            // memoToolStripMenuItem
            // 
            this.memoToolStripMenuItem.Name = "memoToolStripMenuItem";
            this.memoToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.memoToolStripMenuItem.Text = "Memo";
            // 
            // invoiceToolStripMenuItem
            // 
            this.invoiceToolStripMenuItem.Name = "invoiceToolStripMenuItem";
            this.invoiceToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.invoiceToolStripMenuItem.Text = "Invoice Entry";
            this.invoiceToolStripMenuItem.Click += new System.EventHandler(this.invoiceToolStripMenuItem_Click);
            // 
            // pNLEntryToolStripMenuItem
            // 
            this.pNLEntryToolStripMenuItem.Name = "pNLEntryToolStripMenuItem";
            this.pNLEntryToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.pNLEntryToolStripMenuItem.Text = "PNL Entry";
            this.pNLEntryToolStripMenuItem.Click += new System.EventHandler(this.pNLEntryToolStripMenuItem_Click);
            // 
            // bankStatementEntryToolStripMenuItem
            // 
            this.bankStatementEntryToolStripMenuItem.Name = "bankStatementEntryToolStripMenuItem";
            this.bankStatementEntryToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.bankStatementEntryToolStripMenuItem.Text = "Bank Statement Entry";
            this.bankStatementEntryToolStripMenuItem.Click += new System.EventHandler(this.bankStatementEntryToolStripMenuItem_Click);
            // 
            // lottoMasterInventoryToolStripMenuItem
            // 
            this.lottoMasterInventoryToolStripMenuItem.Name = "lottoMasterInventoryToolStripMenuItem";
            this.lottoMasterInventoryToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.lottoMasterInventoryToolStripMenuItem.Text = "Lotto Master Inventory";
            this.lottoMasterInventoryToolStripMenuItem.Click += new System.EventHandler(this.lottoMasterInventoryToolStripMenuItem_Click);
            // 
            // taboccoInventoryToolStripMenuItem
            // 
            this.taboccoInventoryToolStripMenuItem.Name = "taboccoInventoryToolStripMenuItem";
            this.taboccoInventoryToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.taboccoInventoryToolStripMenuItem.Text = "Tabocco Inventory";
            this.taboccoInventoryToolStripMenuItem.Click += new System.EventHandler(this.taboccoInventoryToolStripMenuItem_Click);
            // 
            // bankToolStripMenuItem
            // 
            this.bankToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bankValidationToolStripMenuItem,
            this.bankAndPNLValidationToolStripMenuItem});
            this.bankToolStripMenuItem.Name = "bankToolStripMenuItem";
            this.bankToolStripMenuItem.Size = new System.Drawing.Size(116, 20);
            this.bankToolStripMenuItem.Text = "Close Invoice/PNL";
            // 
            // bankValidationToolStripMenuItem
            // 
            this.bankValidationToolStripMenuItem.Name = "bankValidationToolStripMenuItem";
            this.bankValidationToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.bankValidationToolStripMenuItem.Text = "Close Invoices";
            this.bankValidationToolStripMenuItem.Click += new System.EventHandler(this.bankValidationToolStripMenuItem_Click);
            // 
            // bankAndPNLValidationToolStripMenuItem
            // 
            this.bankAndPNLValidationToolStripMenuItem.Name = "bankAndPNLValidationToolStripMenuItem";
            this.bankAndPNLValidationToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.bankAndPNLValidationToolStripMenuItem.Text = "Close PNL";
            this.bankAndPNLValidationToolStripMenuItem.Click += new System.EventHandler(this.bankAndPNLValidationToolStripMenuItem_Click);
            // 
            // auditMasterToolStripMenuItem
            // 
            this.auditMasterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.gasAuditToolStripMenuItem});
            this.auditMasterToolStripMenuItem.Name = "auditMasterToolStripMenuItem";
            this.auditMasterToolStripMenuItem.Size = new System.Drawing.Size(90, 20);
            this.auditMasterToolStripMenuItem.Text = "Ticket Master";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(157, 22);
            this.toolStripMenuItem4.Text = "Manage Tickets";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // gasAuditToolStripMenuItem
            // 
            this.gasAuditToolStripMenuItem.Name = "gasAuditToolStripMenuItem";
            this.gasAuditToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.gasAuditToolStripMenuItem.Text = "Search Ticket";
            this.gasAuditToolStripMenuItem.Click += new System.EventHandler(this.gasAuditToolStripMenuItem_Click);
            // 
            // auditMasterToolStripMenuItem1
            // 
            this.auditMasterToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.auditorViewToolStripMenuItem,
            this.approveGasTransactionToolStripMenuItem});
            this.auditMasterToolStripMenuItem1.Name = "auditMasterToolStripMenuItem1";
            this.auditMasterToolStripMenuItem1.Size = new System.Drawing.Size(106, 20);
            this.auditMasterToolStripMenuItem1.Text = "Approval Master";
            // 
            // auditorViewToolStripMenuItem
            // 
            this.auditorViewToolStripMenuItem.Name = "auditorViewToolStripMenuItem";
            this.auditorViewToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.auditorViewToolStripMenuItem.Text = "Approve PNL/Invoices";
            this.auditorViewToolStripMenuItem.Click += new System.EventHandler(this.auditorViewToolStripMenuItem_Click);
            // 
            // approveGasTransactionToolStripMenuItem
            // 
            this.approveGasTransactionToolStripMenuItem.Name = "approveGasTransactionToolStripMenuItem";
            this.approveGasTransactionToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.approveGasTransactionToolStripMenuItem.Text = "Approve Gas Transaction";
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saleReportToolStripMenuItem,
            this.purchaseReportToolStripMenuItem,
            this.gasTransactionToolStripMenuItem,
            this.employeeReportToolStripMenuItem,
            this.invoiceReportToolStripMenuItem,
            this.toolStripMenuItem6,
            this.invoiceReportToolStripMenuItem1,
            this.pNLReportToolStripMenuItem});
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(54, 20);
            this.toolsMenu.Text = "&Report";
            // 
            // saleReportToolStripMenuItem
            // 
            this.saleReportToolStripMenuItem.Name = "saleReportToolStripMenuItem";
            this.saleReportToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.saleReportToolStripMenuItem.Text = "Sale Report";
            // 
            // purchaseReportToolStripMenuItem
            // 
            this.purchaseReportToolStripMenuItem.Name = "purchaseReportToolStripMenuItem";
            this.purchaseReportToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.purchaseReportToolStripMenuItem.Text = "Purchase Report";
            // 
            // gasTransactionToolStripMenuItem
            // 
            this.gasTransactionToolStripMenuItem.Name = "gasTransactionToolStripMenuItem";
            this.gasTransactionToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.gasTransactionToolStripMenuItem.Text = "Sales Data Transaction Report";
            this.gasTransactionToolStripMenuItem.Click += new System.EventHandler(this.gasTransactionToolStripMenuItem_Click);
            // 
            // employeeReportToolStripMenuItem
            // 
            this.employeeReportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeReportToolStripMenuItem1,
            this.employeeSalaryReportToolStripMenuItem});
            this.employeeReportToolStripMenuItem.Name = "employeeReportToolStripMenuItem";
            this.employeeReportToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.employeeReportToolStripMenuItem.Text = "Employee Report";
            this.employeeReportToolStripMenuItem.Click += new System.EventHandler(this.employeeReportToolStripMenuItem_Click);
            // 
            // employeeReportToolStripMenuItem1
            // 
            this.employeeReportToolStripMenuItem1.Name = "employeeReportToolStripMenuItem1";
            this.employeeReportToolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
            this.employeeReportToolStripMenuItem1.Text = "Employee Report";
            this.employeeReportToolStripMenuItem1.Click += new System.EventHandler(this.employeeReportToolStripMenuItem1_Click);
            // 
            // employeeSalaryReportToolStripMenuItem
            // 
            this.employeeSalaryReportToolStripMenuItem.Name = "employeeSalaryReportToolStripMenuItem";
            this.employeeSalaryReportToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.employeeSalaryReportToolStripMenuItem.Text = "Employee Salary Report";
            this.employeeSalaryReportToolStripMenuItem.Click += new System.EventHandler(this.employeeSalaryReportToolStripMenuItem_Click);
            // 
            // invoiceReportToolStripMenuItem
            // 
            this.invoiceReportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lottoMasterInvReportToolStripMenuItem,
            this.tobaccoMasterInvReportToolStripMenuItem,
            this.eODReportToolStripMenuItem});
            this.invoiceReportToolStripMenuItem.Name = "invoiceReportToolStripMenuItem";
            this.invoiceReportToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.invoiceReportToolStripMenuItem.Text = "Inventory Report";
            this.invoiceReportToolStripMenuItem.Click += new System.EventHandler(this.invoiceReportToolStripMenuItem_Click);
            // 
            // lottoMasterInvReportToolStripMenuItem
            // 
            this.lottoMasterInvReportToolStripMenuItem.Name = "lottoMasterInvReportToolStripMenuItem";
            this.lottoMasterInvReportToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.lottoMasterInvReportToolStripMenuItem.Text = "Lotto Master Inv Report";
            this.lottoMasterInvReportToolStripMenuItem.Click += new System.EventHandler(this.lottoMasterInvReportToolStripMenuItem_Click);
            // 
            // tobaccoMasterInvReportToolStripMenuItem
            // 
            this.tobaccoMasterInvReportToolStripMenuItem.Name = "tobaccoMasterInvReportToolStripMenuItem";
            this.tobaccoMasterInvReportToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.tobaccoMasterInvReportToolStripMenuItem.Text = "Tobacco Master Inv Report";
            this.tobaccoMasterInvReportToolStripMenuItem.Click += new System.EventHandler(this.tobaccoMasterInvReportToolStripMenuItem_Click);
            // 
            // eODReportToolStripMenuItem
            // 
            this.eODReportToolStripMenuItem.Name = "eODReportToolStripMenuItem";
            this.eODReportToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.eODReportToolStripMenuItem.Text = "EOD Report";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(229, 22);
            this.toolStripMenuItem6.Text = "Milk EOD Report";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // invoiceReportToolStripMenuItem1
            // 
            this.invoiceReportToolStripMenuItem1.Name = "invoiceReportToolStripMenuItem1";
            this.invoiceReportToolStripMenuItem1.Size = new System.Drawing.Size(229, 22);
            this.invoiceReportToolStripMenuItem1.Text = "Invoice Report";
            this.invoiceReportToolStripMenuItem1.Click += new System.EventHandler(this.invoiceReportToolStripMenuItem1_Click);
            // 
            // pNLReportToolStripMenuItem
            // 
            this.pNLReportToolStripMenuItem.Name = "pNLReportToolStripMenuItem";
            this.pNLReportToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.pNLReportToolStripMenuItem.Text = "PNL Report";
            this.pNLReportToolStripMenuItem.Click += new System.EventHandler(this.pNLReportToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 497);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(737, 25);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Visible = false;
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 20);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.AliceBlue;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblUserName,
            this.toolStripSeparator2,
            this.lblTime,
            this.toolStripSeparator6});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1043, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // lblUserName
            // 
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(86, 22);
            this.lblUserName.Text = "toolStripLabel1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // lblTime
            // 
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(86, 22);
            this.lblTime.Text = "toolStripLabel2";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lnkLogoff
            // 
            this.lnkLogoff.AutoSize = true;
            this.lnkLogoff.Font = new System.Drawing.Font("Arial Narrow", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkLogoff.Location = new System.Drawing.Point(852, 28);
            this.lnkLogoff.Name = "lnkLogoff";
            this.lnkLogoff.Size = new System.Drawing.Size(55, 22);
            this.lnkLogoff.TabIndex = 6;
            this.lnkLogoff.TabStop = true;
            this.lnkLogoff.Text = "Log Off";
            this.lnkLogoff.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLogoff_LinkClicked);
            // 
            // MDIParentShell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1043, 532);
            this.Controls.Add(this.lnkLogoff);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIParentShell";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shell";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIParent1_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem manageStoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem managePriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageMarginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageEmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeSalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gasTransactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gasTransationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem watcherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel lblUserName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel lblTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem vendorManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashManagemnetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymnetToVendorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToEmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadDocumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem userManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.LinkLabel lnkLogoff;
        private System.Windows.Forms.ToolStripMenuItem dashBoardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pNLMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invoiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankValidationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pNLEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankAndPNLValidationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invoiceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CategoryToolMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem invoiceReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pNLReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auditMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gasAuditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem auditMasterToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem auditorViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userPasswordChangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem approveGasTransactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankStatementEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureFolderKeyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem themeColorConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lottoMasterInventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taboccoInventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lottoMasterInvReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageSerialMenu;
        private System.Windows.Forms.ToolStripMenuItem tobaccoMasterInvReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeePayRollToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eODReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem employeeSalaryReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
    }
}



