﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell
{
    public class Logger : ILogger
    {
        public void LogActivity(string text)
        {
            using (ShellEntities context = new ShellEntities())
            {
                var logDb = new TBL_Log();
                logDb.CreatedAt = DateTime.Now;
                logDb.CreatedBy = Utility.BA_Commman._userId;
                logDb.Text = text;
                context.TBL_Log.Add(logDb);
                context.SaveChanges();
            }
            //var path = Path.Combine(Application.StartupPath, "log.txt");
            //if (File.Exists(path))
            //{
            //    text = string.Format("{0} - {1}", DateTime.Now, text);
            //    File.AppendAllLines(path, new string[] { text });
            //}
            //else
            //{
            //    File.Create(Path.Combine(Application.StartupPath, "log.txt"));
            //    {
            //        text = string.Format("{0} - {1}", DateTime.Now, text);
            //        File.AppendAllLines(path, new string[] { text });
            //    }
            //}
        }
    }
}
