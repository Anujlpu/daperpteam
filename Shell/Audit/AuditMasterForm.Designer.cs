﻿namespace Shell.Audit
{
    partial class AuditMasterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpSale = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dtpTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnUpdate = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.AuditGrp = new System.Windows.Forms.GroupBox();
            this.dgvInvoicesDocs = new System.Windows.Forms.DataGridView();
            this.ViewDoc = new System.Windows.Forms.DataGridViewLinkColumn();
            this.dgvInvoices = new System.Windows.Forms.DataGridView();
            this.InvoiceCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Document = new System.Windows.Forms.DataGridViewLinkColumn();
            this.PNLgroup = new System.Windows.Forms.GroupBox();
            this.dgvPNLDoc = new System.Windows.Forms.DataGridView();
            this.dataGridViewLinkColumn1 = new System.Windows.Forms.DataGridViewLinkColumn();
            this.dgvPNlList = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BindDocs = new System.Windows.Forms.DataGridViewLinkColumn();
            this.grpSale.SuspendLayout();
            this.AuditGrp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoicesDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoices)).BeginInit();
            this.PNLgroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNLDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNlList)).BeginInit();
            this.SuspendLayout();
            // 
            // grpSale
            // 
            this.grpSale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSale.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpSale.BackColor = System.Drawing.Color.AliceBlue;
            this.grpSale.Controls.Add(this.button1);
            this.grpSale.Controls.Add(this.dtpTransactionDate);
            this.grpSale.Controls.Add(this.btnExit);
            this.grpSale.Controls.Add(this.BtnUpdate);
            this.grpSale.Controls.Add(this.label23);
            this.grpSale.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.grpSale.Location = new System.Drawing.Point(26, 3);
            this.grpSale.Name = "grpSale";
            this.grpSale.Size = new System.Drawing.Size(905, 118);
            this.grpSale.TabIndex = 1;
            this.grpSale.TabStop = false;
            this.grpSale.Text = "Filter";
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(270, 66);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 32);
            this.button1.TabIndex = 61;
            this.button1.Text = "Approve";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtpTransactionDate
            // 
            this.dtpTransactionDate.CustomFormat = "MMM/dd/yyyy";
            this.dtpTransactionDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransactionDate.Location = new System.Drawing.Point(169, 18);
            this.dtpTransactionDate.Name = "dtpTransactionDate";
            this.dtpTransactionDate.Size = new System.Drawing.Size(163, 22);
            this.dtpTransactionDate.TabIndex = 60;
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(372, 66);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 59;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnUpdate
            // 
            this.BtnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnUpdate.FlatAppearance.BorderSize = 2;
            this.BtnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnUpdate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpdate.Location = new System.Drawing.Point(164, 66);
            this.BtnUpdate.Name = "BtnUpdate";
            this.BtnUpdate.Size = new System.Drawing.Size(90, 32);
            this.BtnUpdate.TabIndex = 58;
            this.BtnUpdate.Text = "Search";
            this.BtnUpdate.UseVisualStyleBackColor = true;
            this.BtnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(29, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(101, 15);
            this.label23.TabIndex = 52;
            this.label23.Text = "Transaction Date";
            // 
            // AuditGrp
            // 
            this.AuditGrp.Controls.Add(this.dgvInvoicesDocs);
            this.AuditGrp.Controls.Add(this.dgvInvoices);
            this.AuditGrp.Location = new System.Drawing.Point(26, 138);
            this.AuditGrp.Name = "AuditGrp";
            this.AuditGrp.Size = new System.Drawing.Size(905, 191);
            this.AuditGrp.TabIndex = 2;
            this.AuditGrp.TabStop = false;
            this.AuditGrp.Text = "Invoice Audit";
            // 
            // dgvInvoicesDocs
            // 
            this.dgvInvoicesDocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoicesDocs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ViewDoc});
            this.dgvInvoicesDocs.Location = new System.Drawing.Point(609, 19);
            this.dgvInvoicesDocs.Name = "dgvInvoicesDocs";
            this.dgvInvoicesDocs.Size = new System.Drawing.Size(276, 166);
            this.dgvInvoicesDocs.TabIndex = 1;
            this.dgvInvoicesDocs.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoicesDocs_CellClick);
            // 
            // ViewDoc
            // 
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle9.NullValue = "Open Doc";
            this.ViewDoc.DefaultCellStyle = dataGridViewCellStyle9;
            this.ViewDoc.HeaderText = "Open Document";
            this.ViewDoc.Name = "ViewDoc";
            // 
            // dgvInvoices
            // 
            this.dgvInvoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InvoiceCheckBox,
            this.Document});
            this.dgvInvoices.Location = new System.Drawing.Point(32, 19);
            this.dgvInvoices.Name = "dgvInvoices";
            this.dgvInvoices.Size = new System.Drawing.Size(537, 166);
            this.dgvInvoices.TabIndex = 0;
            this.dgvInvoices.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoices_CellClick);
            // 
            // InvoiceCheckBox
            // 
            this.InvoiceCheckBox.HeaderText = "";
            this.InvoiceCheckBox.Name = "InvoiceCheckBox";
            this.InvoiceCheckBox.ToolTipText = "Click to view doc and close";
            // 
            // Document
            // 
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle10.NullValue = "Bind Doc";
            this.Document.DefaultCellStyle = dataGridViewCellStyle10;
            this.Document.HeaderText = "Bind Document";
            this.Document.Name = "Document";
            // 
            // PNLgroup
            // 
            this.PNLgroup.Controls.Add(this.dgvPNLDoc);
            this.PNLgroup.Controls.Add(this.dgvPNlList);
            this.PNLgroup.Location = new System.Drawing.Point(26, 335);
            this.PNLgroup.Name = "PNLgroup";
            this.PNLgroup.Size = new System.Drawing.Size(905, 199);
            this.PNLgroup.TabIndex = 3;
            this.PNLgroup.TabStop = false;
            this.PNLgroup.Text = "PNL Audit";
            // 
            // dgvPNLDoc
            // 
            this.dgvPNLDoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPNLDoc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewLinkColumn1});
            this.dgvPNLDoc.Location = new System.Drawing.Point(609, 19);
            this.dgvPNLDoc.Name = "dgvPNLDoc";
            this.dgvPNLDoc.Size = new System.Drawing.Size(276, 174);
            this.dgvPNLDoc.TabIndex = 3;
            this.dgvPNLDoc.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPNLDoc_CellClick);
            // 
            // dataGridViewLinkColumn1
            // 
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Lime;
            dataGridViewCellStyle11.NullValue = "Open Doc";
            this.dataGridViewLinkColumn1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewLinkColumn1.HeaderText = "Open Document";
            this.dataGridViewLinkColumn1.Name = "dataGridViewLinkColumn1";
            // 
            // dgvPNlList
            // 
            this.dgvPNlList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPNlList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1,
            this.BindDocs});
            this.dgvPNlList.Location = new System.Drawing.Point(32, 19);
            this.dgvPNlList.Name = "dgvPNlList";
            this.dgvPNlList.Size = new System.Drawing.Size(537, 174);
            this.dgvPNlList.TabIndex = 2;
            this.dgvPNlList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPNlList_CellClick);
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.ToolTipText = "Click to view doc and close";
            // 
            // BindDocs
            // 
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.Aqua;
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Blue;
            dataGridViewCellStyle12.NullValue = "Bind Doc";
            this.BindDocs.DefaultCellStyle = dataGridViewCellStyle12;
            this.BindDocs.HeaderText = "Bind Documents";
            this.BindDocs.Name = "BindDocs";
            // 
            // AuditMasterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(988, 556);
            this.Controls.Add(this.PNLgroup);
            this.Controls.Add(this.AuditGrp);
            this.Controls.Add(this.grpSale);
            this.Name = "AuditMasterForm";
            this.Text = "Approval Master";
            this.Load += new System.EventHandler(this.AuditMasterForm_Load);
            this.grpSale.ResumeLayout(false);
            this.grpSale.PerformLayout();
            this.AuditGrp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoicesDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoices)).EndInit();
            this.PNLgroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNLDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNlList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpSale;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button BtnUpdate;
        private System.Windows.Forms.DateTimePicker dtpTransactionDate;
        private System.Windows.Forms.GroupBox AuditGrp;
        private System.Windows.Forms.DataGridView dgvInvoicesDocs;
        private System.Windows.Forms.DataGridView dgvInvoices;
        private System.Windows.Forms.GroupBox PNLgroup;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewLinkColumn ViewDoc;
        private System.Windows.Forms.DataGridView dgvPNLDoc;
        private System.Windows.Forms.DataGridViewLinkColumn dataGridViewLinkColumn1;
        private System.Windows.Forms.DataGridView dgvPNlList;
        private System.Windows.Forms.DataGridViewCheckBoxColumn InvoiceCheckBox;
        private System.Windows.Forms.DataGridViewLinkColumn Document;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewLinkColumn BindDocs;
    }
}