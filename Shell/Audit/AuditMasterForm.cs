﻿using BAL;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Audit
{
    public partial class AuditMasterForm : BaseForm
    {
        public AuditMasterForm()
        {
            InitializeComponent();
        }
        #region Private Methods
        List<int> invoiceIds = new List<int>();
        List<int> PnlEntryIds = new List<int>();
        private bool IsAnyCheked = false;

        private void AnyChecked()
        {
            foreach (DataGridViewRow row in dgvInvoices.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    IsAnyCheked = true;
                }
            }
            foreach (DataGridViewRow row in dgvPNLDoc.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    IsAnyCheked = true;
                }
            }
        }

        private void ApproveInvoiceList()
        {
            if (dgvInvoices.Rows.Count > 0)
            {
                using (ShellEntities context = new ShellEntities())
                {
                    foreach (DataGridViewRow row in dgvInvoices.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {

                            int invoiceId = Convert.ToInt32(row.Cells[2].Value);
                            invoiceIds.Add(Convert.ToInt32(row.Cells[2].Value));
                            var invoice = context.Tbl_Invoices.FirstOrDefault(s => s.InvoiceId == invoiceId);
                            if (invoice != null)
                            {
                                invoice.IsApproved = true;
                                invoice.ApprovedBy = Utility.BA_Commman._userId;
                                invoice.ApprovedDate = DateTime.Now;
                            }
                        }
                    }

                    context.SaveChanges();
                }
            }
        }
        private void ApprovePnlList()
        {
            if (dgvPNLDoc.Rows.Count > 0)
            {
                using (ShellEntities context = new ShellEntities())
                {
                    foreach (DataGridViewRow row in dgvPNLDoc.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            int invoiceId = Convert.ToInt32(row.Cells[2].Value);
                            invoiceIds.Add(Convert.ToInt32(row.Cells[2].Value));
                            var invoice = context.Tbl_Invoices.FirstOrDefault(s => s.InvoiceId == invoiceId);
                            if (invoice != null)
                            {
                                invoice.IsApproved = true;
                                invoice.ApprovedBy = Utility.BA_Commman._userId;
                                invoice.ApprovedDate = DateTime.Now;
                            }
                        }
                    }

                    context.SaveChanges();
                }
            }
        }
        private void BindInvoiceGrid()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var ClosedInvoice = context.Tbl_Invoices.Where(s => s.IsActive == true && s.IsClosed == true && (s.IsApproved == false || s.IsApproved == null)).Select(s => new { s.InvoiceId, s.InvoiceNo, s.InvDate, s.Remarks }).ToList();
                ClosedInvoice = ClosedInvoice.Where(s => Convert.ToDateTime(s.InvDate).ToString("MMddyyyy") == Convert.ToDateTime(dtpTransactionDate.Text).ToString("MMddyyy")).ToList();
                if (ClosedInvoice.Count > 0)
                {
                    AuditGrp.Visible = true;
                    dgvInvoices.DataSource = ClosedInvoice;
                    dgvInvoices.Columns["InvoiceId"].Visible = false;
                    dgvInvoicesDocs.Visible = false;
                }
                else
                {
                    MessageBox.Show("No closed invoices for this date.");
                }

            }

        }
        private void BindPNLGrid()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var ClosedInvoice = (from m in context.Tbl_PNLEntry
                                     join pn in context.MST_PnlMaster on m.PNLID equals pn.PNLId
                                     where m.IsActive == true && m.IsClosed == true && (m.IsApproved == false || m.IsApproved == null)
                                     select new
                                     {
                                         m.PNLEntryID,
                                         pn.PNLCode,
                                         m.TransactionType,
                                         m.TotalAmount,
                                         m.EntryDate

                                     }).ToList();
                ClosedInvoice = ClosedInvoice.Where(s => Convert.ToDateTime(s.EntryDate).ToString("MMddyyyy") == Convert.ToDateTime(dtpTransactionDate.Text).ToString("MMddyyy")).ToList();
                if (ClosedInvoice.Count > 0)
                {
                    PNLgroup.Visible = true;
                    dgvPNLDoc.Visible = false;
                    dgvPNlList.DataSource = ClosedInvoice;
                    dgvPNlList.Columns["PNLEntryID"].Visible = false;
                }
                else
                {
                    MessageBox.Show("No closed PNL for this date.");
                }
            }

        }
        private int InvoiceId = 0;
        private int PNLEntryId = 0;
        private void BindInvoiceDoc()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var invoiceDoc = (from d in context.Tbl_InvoiceEntryDocuments
                                  join m in context.Mst_DocUpload on d.UploadedDocID equals m.UploadId
                                  where d.IsActive == true && d.InvoiceID == InvoiceId
                                  select new
                                  {
                                      m.DocsIdentity,
                                      m.DocsType,
                                      m.FilePath
                                  }).ToList();

                if (invoiceDoc.Count > 0)
                {
                    dgvInvoicesDocs.Visible = true;
                    AuditGrp.Visible = true;
                    dgvInvoicesDocs.DataSource = invoiceDoc;
                    dgvInvoicesDocs.Columns["FilePath"].Visible = false;
                }
                

            }

        }
        private void BindPNLDoc()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var invoiceDoc = (from d in context.Tbl_PNLEntryDocuments
                                  join m in context.Mst_DocUpload on d.UploadedDocID equals m.UploadId
                                  where d.IsActive == true && d.PNLID == PNLEntryId
                                  select new
                                  {
                                      m.DocsIdentity,
                                      m.DocsType,
                                      m.FilePath
                                  }).ToList();

                if (invoiceDoc.Count > 0)
                {
                    dgvPNLDoc.Visible = true;
                    dgvPNLDoc.DataSource = invoiceDoc;
                    dgvPNLDoc.Columns["FilePath"].Visible = false;
                }

            }

        }
        #endregion
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                BindInvoiceGrid();
                BindPNLGrid();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AuditMasterForm_Load(object sender, EventArgs e)
        {
            AuditGrp.Visible = false;
            PNLgroup.Visible = false;
        }

        private void dgvInvoices_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0 && e.ColumnIndex == 1)
                {
                    InvoiceId = Convert.ToInt32(dgvInvoices.Rows[e.RowIndex].Cells[2].Value);
                    BindInvoiceDoc();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                AnyChecked();
                if (IsAnyCheked)
                {
                    ApproveInvoiceList();
                    ApprovePnlList();
                    BindInvoiceGrid();
                    BindPNLGrid();
                    MessageBox.Show("Selected Invoice/PNL has been approved.");
                }
                else
                {
                    MessageBox.Show("Please select any invoice/PNL to approve");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvPNlList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0 && e.ColumnIndex == 1)
                {
                    PNLEntryId = Convert.ToInt32(dgvPNlList.Rows[e.RowIndex].Cells[2].Value);
                    BindPNLDoc();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvInvoicesDocs_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    string fileName = "";

                    DataGridView dgv = ((DataGridView)(sender));
                    fileName = dgv.Rows[e.RowIndex].Cells["FilePath"].Value.ToString();
                    System.Diagnostics.Process.Start("explorer.exe", fileName);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvPNLDoc_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    string fileName = "";
                    DataGridView dgv = ((DataGridView)(sender));
                    fileName = dgv.Rows[e.RowIndex].Cells["FilePath"].Value.ToString();
                    System.Diagnostics.Process.Start("explorer.exe", fileName);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
