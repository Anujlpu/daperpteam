﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Shell
{
    public static class ShellERPActivities
    {
        #region Login
        public static string LoginForm
        {
            get
            {
                return "LoginForm";
            }
        }
        public static string LottoMasterForm
        {
            get
            {
                return "LottoMasterForm";
            }
        }
        public static string LoginSave
        {
            get
            {
                return "LoginClick";
            }
        }
        public static string LottoMasterException
        {
            get
            {
                return "Lotto Form";
            }
        }
        public static string NotAplicable
        {
            get
            {
                return "NA";
            }
        }
        public static string SalesDataForm
        {
            get
            {
                return "Sales Data Form Opened";
            }
        }
        public static string SalesDataFormSaved
        {
            get
            {
                return "Sales Data Form Saved";
            }
        }
        public static string SalesDataFormSubmiited
        {
            get
            {
                return "Sales Data Form Submitted";
            }
        }
        public static string SalesDataFormReferBack
        {
            get
            {
                return "Sales Data Form Referred Back";
            }
        }
        public static string SalesDataFormEscalated
        {
            get
            {
                return "Sales Data Form Escalated to Management";
            }
        }
       
        public static string IPAddress
        {
            get
            {
                string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
                string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
                return myIP;
            }
        }
        public static string HostName
        {
            get
            {
                return Dns.GetHostName();
            }
        }

        #endregion

    }
}
