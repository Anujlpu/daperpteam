﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shell
{
    public static class DocumentTypeConstants
    {
        public const string Tobacco10S = "Tobacco 10S";
        public const string Tobacco8S = "Tobacco 8S";
        public const string Chew = "Chew";
        public const string Cigar10S = "Cigar 10S";
        public const string Cigar8S = "Cigar 8S";
        public const string Cigar5S = "Cigar 5S";
        public const string ECigar = "E-Cigar";
        public const string WWFCalc = "WWFCalc";
        public const string SinglesAndPapers = "Singles And Papers";
        public const string LottoTypeActivated = "Lotto Activated";
        public const string LottoTypeUnActivated = "Lotto UnActivated";
        public const string LottoWeekly = "Lotto Weekly";
        public const string Milk310ml = "Milk 310ml";
        public const string Milk473ml = "Milk 473ml";
        public const string Milk1L = "Milk 1L";
        public const string Milk2L = "Milk 2L";
        public const string Milk4L = "Milk 4L";
        #region Doc Types
        public const string TobaccoDocType = "Tobacco Inventory Docs";
        public const string CigarDocType = "Cigar Inventory Docs";
        public const string LottoScratchAndWinDoc = "Lotto Scratch And Win";
        public const string LottoScratchAndWinOnline = "Lotto Scratch And Win Online";
        public const string MilkDoc = "Milk";
        public const string MilkAdditionDoc = "Milk Addition";

        #endregion
    }
}
