﻿namespace Shell
{
    partial class FrmPaymnetToEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpCashMngtList = new System.Windows.Forms.GroupBox();
            this.dgvVendorList = new System.Windows.Forms.DataGridView();
            this.grpManageVendor = new System.Windows.Forms.GroupBox();
            this.pdfUploadbox = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.btnbrowse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpPayment = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.ddlEmpName = new System.Windows.Forms.ComboBox();
            this.txtVoucherNo = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.lblEmpName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.ddlPaymnetType = new System.Windows.Forms.ComboBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.grpCashMngtList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendorList)).BeginInit();
            this.grpManageVendor.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpCashMngtList
            // 
            this.grpCashMngtList.Controls.Add(this.dgvVendorList);
            this.grpCashMngtList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCashMngtList.Location = new System.Drawing.Point(3, 230);
            this.grpCashMngtList.Name = "grpCashMngtList";
            this.grpCashMngtList.Size = new System.Drawing.Size(806, 238);
            this.grpCashMngtList.TabIndex = 49;
            this.grpCashMngtList.TabStop = false;
            this.grpCashMngtList.Text = "Payment List";
            // 
            // dgvVendorList
            // 
            this.dgvVendorList.AllowDrop = true;
            this.dgvVendorList.AllowUserToAddRows = false;
            this.dgvVendorList.AllowUserToDeleteRows = false;
            this.dgvVendorList.AllowUserToResizeColumns = false;
            this.dgvVendorList.AllowUserToResizeRows = false;
            this.dgvVendorList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvVendorList.BackgroundColor = System.Drawing.Color.White;
            this.dgvVendorList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVendorList.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVendorList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVendorList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvVendorList.Location = new System.Drawing.Point(3, 16);
            this.dgvVendorList.MultiSelect = false;
            this.dgvVendorList.Name = "dgvVendorList";
            this.dgvVendorList.ReadOnly = true;
            this.dgvVendorList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvVendorList.RowHeadersVisible = false;
            this.dgvVendorList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvVendorList.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvVendorList.Size = new System.Drawing.Size(800, 219);
            this.dgvVendorList.TabIndex = 1;
            this.dgvVendorList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVendorList_CellContentClick);
            // 
            // grpManageVendor
            // 
            this.grpManageVendor.Controls.Add(this.pdfUploadbox);
            this.grpManageVendor.Controls.Add(this.btnExit);
            this.grpManageVendor.Controls.Add(this.txtAmount);
            this.grpManageVendor.Controls.Add(this.label6);
            this.grpManageVendor.Controls.Add(this.label62);
            this.grpManageVendor.Controls.Add(this.txtRemarks);
            this.grpManageVendor.Controls.Add(this.btnbrowse);
            this.grpManageVendor.Controls.Add(this.label4);
            this.grpManageVendor.Controls.Add(this.dtpPayment);
            this.grpManageVendor.Controls.Add(this.label7);
            this.grpManageVendor.Controls.Add(this.ddlEmpName);
            this.grpManageVendor.Controls.Add(this.txtVoucherNo);
            this.grpManageVendor.Controls.Add(this.label15);
            this.grpManageVendor.Controls.Add(this.BtnDelete);
            this.grpManageVendor.Controls.Add(this.BtnSave);
            this.grpManageVendor.Controls.Add(this.lblEmpName);
            this.grpManageVendor.Controls.Add(this.label1);
            this.grpManageVendor.Controls.Add(this.btnUpdate);
            this.grpManageVendor.Controls.Add(this.ddlPaymnetType);
            this.grpManageVendor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageVendor.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageVendor.ForeColor = System.Drawing.Color.Black;
            this.grpManageVendor.Location = new System.Drawing.Point(3, 2);
            this.grpManageVendor.Name = "grpManageVendor";
            this.grpManageVendor.Size = new System.Drawing.Size(800, 222);
            this.grpManageVendor.TabIndex = 48;
            this.grpManageVendor.TabStop = false;
            this.grpManageVendor.Text = "Cash Management";
            // 
            // pdfUploadbox
            // 
            this.pdfUploadbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pdfUploadbox.ForeColor = System.Drawing.Color.Black;
            this.pdfUploadbox.Location = new System.Drawing.Point(699, 62);
            this.pdfUploadbox.Name = "pdfUploadbox";
            this.pdfUploadbox.Size = new System.Drawing.Size(40, 26);
            this.pdfUploadbox.TabIndex = 171;
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(535, 178);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(90, 32);
            this.btnExit.TabIndex = 84;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.ForeColor = System.Drawing.Color.Black;
            this.txtAmount.Location = new System.Drawing.Point(491, 27);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(202, 26);
            this.txtAmount.TabIndex = 83;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(340, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 15);
            this.label6.TabIndex = 82;
            this.label6.Text = "Amount";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(340, 95);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 81;
            this.label62.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(491, 98);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(202, 65);
            this.txtRemarks.TabIndex = 80;
            // 
            // btnbrowse
            // 
            this.btnbrowse.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnbrowse.FlatAppearance.BorderSize = 2;
            this.btnbrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbrowse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbrowse.Location = new System.Drawing.Point(491, 58);
            this.btnbrowse.Name = "btnbrowse";
            this.btnbrowse.Size = new System.Drawing.Size(202, 24);
            this.btnbrowse.TabIndex = 78;
            this.btnbrowse.Text = "B&rowse";
            this.btnbrowse.UseVisualStyleBackColor = true;
            this.btnbrowse.Click += new System.EventHandler(this.btnbrowse_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(340, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 15);
            this.label4.TabIndex = 79;
            this.label4.Text = "Upload Attachment";
            // 
            // dtpPayment
            // 
            this.dtpPayment.CustomFormat = "MM/dd/yyyy";
            this.dtpPayment.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPayment.Location = new System.Drawing.Point(121, 30);
            this.dtpPayment.Name = "dtpPayment";
            this.dtpPayment.Size = new System.Drawing.Size(202, 26);
            this.dtpPayment.TabIndex = 77;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.AliceBlue;
            this.label7.Font = new System.Drawing.Font("Arial", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(16, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 76;
            this.label7.Text = "Payment Date";
            // 
            // ddlEmpName
            // 
            this.ddlEmpName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEmpName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlEmpName.FormattingEnabled = true;
            this.ddlEmpName.Location = new System.Drawing.Point(121, 62);
            this.ddlEmpName.Name = "ddlEmpName";
            this.ddlEmpName.Size = new System.Drawing.Size(202, 22);
            this.ddlEmpName.TabIndex = 75;
            // 
            // txtVoucherNo
            // 
            this.txtVoucherNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVoucherNo.ForeColor = System.Drawing.Color.Black;
            this.txtVoucherNo.Location = new System.Drawing.Point(121, 124);
            this.txtVoucherNo.Name = "txtVoucherNo";
            this.txtVoucherNo.Size = new System.Drawing.Size(202, 26);
            this.txtVoucherNo.TabIndex = 5;
            this.txtVoucherNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(16, 98);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 15);
            this.label15.TabIndex = 55;
            this.label15.Text = "Payment Type";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(439, 178);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 15;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(249, 178);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // lblEmpName
            // 
            this.lblEmpName.AutoSize = true;
            this.lblEmpName.BackColor = System.Drawing.Color.AliceBlue;
            this.lblEmpName.Font = new System.Drawing.Font("Arial", 9F);
            this.lblEmpName.ForeColor = System.Drawing.Color.Black;
            this.lblEmpName.Location = new System.Drawing.Point(16, 64);
            this.lblEmpName.Name = "lblEmpName";
            this.lblEmpName.Size = new System.Drawing.Size(99, 15);
            this.lblEmpName.TabIndex = 36;
            this.lblEmpName.Text = "Employee Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 130);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Voucher No";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUpdate.FlatAppearance.BorderSize = 2;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(343, 178);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(90, 32);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // ddlPaymnetType
            // 
            this.ddlPaymnetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPaymnetType.Enabled = false;
            this.ddlPaymnetType.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPaymnetType.FormattingEnabled = true;
            this.ddlPaymnetType.Location = new System.Drawing.Point(121, 95);
            this.ddlPaymnetType.Name = "ddlPaymnetType";
            this.ddlPaymnetType.Size = new System.Drawing.Size(202, 22);
            this.ddlPaymnetType.TabIndex = 4;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FrmPaymnetToEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(811, 469);
            this.Controls.Add(this.grpCashMngtList);
            this.Controls.Add(this.grpManageVendor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmPaymnetToEmployee";
            this.Text = "Paymnet To Employee";
            this.Load += new System.EventHandler(this.FrmPaymnetToEmployee_Load);
            this.grpCashMngtList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendorList)).EndInit();
            this.grpManageVendor.ResumeLayout(false);
            this.grpManageVendor.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpCashMngtList;
        private System.Windows.Forms.DataGridView dgvVendorList;
        private System.Windows.Forms.GroupBox grpManageVendor;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Button btnbrowse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpPayment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox ddlEmpName;
        private System.Windows.Forms.TextBox txtVoucherNo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label lblEmpName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ComboBox ddlPaymnetType;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox pdfUploadbox;
    }
}