﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell.Transaction
{
    public partial class CommonFileUploadForm : Form
    {
        public CommonFileUploadForm()
        {
            InitializeComponent();
            btnSaveFile.Enabled = false;
        }

        private void fileBtn_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            lblFilePath.Text = openFileDialog1.FileName;
            btnBrowse.Enabled = false;
            btnSaveFile.Enabled = true;
        }

        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            // Save File
        }
    }
}
