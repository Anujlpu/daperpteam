﻿namespace Shell
{
    partial class frmGasTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCStoreSale = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGST = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDepositLevy = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFuel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtdailystorerntincgst = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dtpTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.grpFuel = new System.Windows.Forms.GroupBox();
            this.DiffInFuelCommTxt = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.dgvFuel = new System.Windows.Forms.DataGridView();
            this.FuelCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtbronzeltr = new System.Windows.Forms.TextBox();
            this.txtActualCommission = new System.Windows.Forms.TextBox();
            this.txtGasRemarks = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtdiffinsettlement = new System.Windows.Forms.TextBox();
            this.txtdifindrcr = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtgstreceivedfuel = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtfuelcommexgst = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtVpdvdd = new System.Windows.Forms.TextBox();
            this.txtCardRembsment = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtBSDltr = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtActualShort = new System.Windows.Forms.TextBox();
            this.txtDiff = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txttotalFuel = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtAirmiles = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtdieselltr = new System.Windows.Forms.TextBox();
            this.txtvpowerltr = new System.Windows.Forms.TextBox();
            this.txtVppltr = new System.Windows.Forms.TextBox();
            this.txtSilverltr = new System.Windows.Forms.TextBox();
            this.txtfinalsettlement = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txttotalsettlement = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtgstpaidonrent = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.txtTabbRemarks = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txtCalstoreother = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.txtshelGisftcard = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtgiftcard = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtphonecard = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtscratchlotto = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtonlinelotto = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txttobbaco = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.txtSalTotal = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtActualSettlement = new System.Windows.Forms.TextBox();
            this.txtShellCredit = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtShellDebit = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtRadiantcr = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.txtfairshare = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtRadiantdb = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.grpSale = new System.Windows.Forms.GroupBox();
            this.dgvCstore = new System.Windows.Forms.DataGridView();
            this.CStoreCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCStoreRemarks = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.grpSettlement = new System.Windows.Forms.GroupBox();
            this.SettlementpopBtn = new System.Windows.Forms.Button();
            this.label83 = new System.Windows.Forms.Label();
            this.DiffInSettlementTxt = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.dgvSettlement = new System.Windows.Forms.DataGridView();
            this.SettlementCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label73 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.txtShellRemarks = new System.Windows.Forms.TextBox();
            this.txtradiantcrdiff = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txtRadiantdbdiff = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.grpTabbocco = new System.Windows.Forms.GroupBox();
            this.dgvTabbacco = new System.Windows.Forms.DataGridView();
            this.Tabbaco = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label63 = new System.Windows.Forms.Label();
            this.btnCals = new System.Windows.Forms.Button();
            this.SubmitBtn = new System.Windows.Forms.Button();
            this.ClearFormBtn = new System.Windows.Forms.Button();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SummaryGridView = new System.Windows.Forms.DataGridView();
            this.ReferBackBtn = new System.Windows.Forms.Button();
            this.EscalateBtn = new System.Windows.Forms.Button();
            this.StatusComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtActualBankDeposit = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtleftBankdeposit = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtCashReceived = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtGiftcert = new System.Windows.Forms.TextBox();
            this.txtCashSort = new System.Windows.Forms.TextBox();
            this.txtCashpaidmgt = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtCashPaidEmp = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtCashPaid = new System.Windows.Forms.TextBox();
            this.txtCashRemarks = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.txtcashpayoutStore = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.dgvCash = new System.Windows.Forms.DataGridView();
            this.CashCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCashBankDep = new System.Windows.Forms.TextBox();
            this.txtCashDriveAway = new System.Windows.Forms.TextBox();
            this.CashDifferenceTxt = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.txtAirMilesCash = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.txtDebitCredit = new System.Windows.Forms.TextBox();
            this.txtTotalBankSection = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.txtBankDiff = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txttotallotopayout = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.txtCashlottto = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtLotowinner = new System.Windows.Forms.TextBox();
            this.grpCash = new System.Windows.Forms.GroupBox();
            this.grpFuel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuel)).BeginInit();
            this.grpSale.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCstore)).BeginInit();
            this.grpSettlement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlement)).BeginInit();
            this.grpTabbocco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabbacco)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SummaryGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCash)).BeginInit();
            this.grpCash.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnAdd.FlatAppearance.BorderSize = 2;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(8, 7);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(78, 32);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Save";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(280, 7);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(65, 32);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(11, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Bronze ltr";
            // 
            // txtCStoreSale
            // 
            this.txtCStoreSale.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCStoreSale.ForeColor = System.Drawing.Color.Black;
            this.txtCStoreSale.Location = new System.Drawing.Point(129, 14);
            this.txtCStoreSale.MaxLength = 10;
            this.txtCStoreSale.Name = "txtCStoreSale";
            this.txtCStoreSale.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCStoreSale.Size = new System.Drawing.Size(173, 22);
            this.txtCStoreSale.TabIndex = 9;
            this.txtCStoreSale.Text = "0";
            this.txtCStoreSale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCStoreSale.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCStoreSale.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(10, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Silver ltr";
            // 
            // txtDiscount
            // 
            this.txtDiscount.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.ForeColor = System.Drawing.Color.Black;
            this.txtDiscount.Location = new System.Drawing.Point(17, 80);
            this.txtDiscount.MaxLength = 12;
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(83, 22);
            this.txtDiscount.TabIndex = 21;
            this.txtDiscount.Text = "0";
            this.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtDiscount.Leave += new System.EventHandler(this.txtCStoreSale_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "VPower Gas ltr";
            // 
            // txtGST
            // 
            this.txtGST.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGST.ForeColor = System.Drawing.Color.Black;
            this.txtGST.Location = new System.Drawing.Point(210, 80);
            this.txtGST.MaxLength = 12;
            this.txtGST.Name = "txtGST";
            this.txtGST.Size = new System.Drawing.Size(83, 22);
            this.txtGST.TabIndex = 23;
            this.txtGST.Text = "0";
            this.txtGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGST.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtGST.Leave += new System.EventHandler(this.txtCStoreSale_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AliceBlue;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(281, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "VPower Diesel ltr";
            // 
            // txtDepositLevy
            // 
            this.txtDepositLevy.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepositLevy.ForeColor = System.Drawing.Color.Black;
            this.txtDepositLevy.Location = new System.Drawing.Point(306, 80);
            this.txtDepositLevy.MaxLength = 12;
            this.txtDepositLevy.Name = "txtDepositLevy";
            this.txtDepositLevy.Size = new System.Drawing.Size(83, 22);
            this.txtDepositLevy.TabIndex = 24;
            this.txtDepositLevy.Text = "0";
            this.txtDepositLevy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDepositLevy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtDepositLevy.Leave += new System.EventHandler(this.txtCStoreSale_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(10, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Diesel ltr";
            // 
            // txtFuel
            // 
            this.txtFuel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuel.ForeColor = System.Drawing.Color.Black;
            this.txtFuel.Location = new System.Drawing.Point(117, 80);
            this.txtFuel.MaxLength = 12;
            this.txtFuel.Name = "txtFuel";
            this.txtFuel.Size = new System.Drawing.Size(83, 22);
            this.txtFuel.TabIndex = 22;
            this.txtFuel.Text = "0";
            this.txtFuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtFuel.Leave += new System.EventHandler(this.txtCStoreSale_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(564, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 32);
            this.label7.TabIndex = 15;
            this.label7.Text = "Cards\r\nReimbursements";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(7, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 30);
            this.label10.TabIndex = 21;
            this.label10.Text = "Daily Store\r\nRent including Gst";
            // 
            // txtdailystorerntincgst
            // 
            this.txtdailystorerntincgst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdailystorerntincgst.ForeColor = System.Drawing.Color.Black;
            this.txtdailystorerntincgst.Location = new System.Drawing.Point(122, 39);
            this.txtdailystorerntincgst.MaxLength = 10;
            this.txtdailystorerntincgst.Name = "txtdailystorerntincgst";
            this.txtdailystorerntincgst.Size = new System.Drawing.Size(146, 22);
            this.txtdailystorerntincgst.TabIndex = 63;
            this.txtdailystorerntincgst.Text = "0";
            this.txtdailystorerntincgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtdailystorerntincgst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtdailystorerntincgst.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.AliceBlue;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(542, 77);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 16);
            this.label12.TabIndex = 23;
            this.label12.Text = "Actual Settlement";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.AliceBlue;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(367, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 16);
            this.label15.TabIndex = 31;
            this.label15.Text = "Transaction Date";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // dtpTransactionDate
            // 
            this.dtpTransactionDate.CustomFormat = "MMM/dd/yyyy";
            this.dtpTransactionDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransactionDate.Location = new System.Drawing.Point(370, 44);
            this.dtpTransactionDate.Name = "dtpTransactionDate";
            this.dtpTransactionDate.Size = new System.Drawing.Size(111, 22);
            this.dtpTransactionDate.TabIndex = 7;
            this.dtpTransactionDate.ValueChanged += new System.EventHandler(this.dtpTransactionDate_ValueChanged);
            // 
            // grpFuel
            // 
            this.grpFuel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpFuel.Controls.Add(this.DiffInFuelCommTxt);
            this.grpFuel.Controls.Add(this.label31);
            this.grpFuel.Controls.Add(this.dgvFuel);
            this.grpFuel.Controls.Add(this.label71);
            this.grpFuel.Controls.Add(this.label72);
            this.grpFuel.Controls.Add(this.txtbronzeltr);
            this.grpFuel.Controls.Add(this.txtActualCommission);
            this.grpFuel.Controls.Add(this.txtGasRemarks);
            this.grpFuel.Controls.Add(this.label7);
            this.grpFuel.Controls.Add(this.label44);
            this.grpFuel.Controls.Add(this.txtdiffinsettlement);
            this.grpFuel.Controls.Add(this.txtdifindrcr);
            this.grpFuel.Controls.Add(this.label50);
            this.grpFuel.Controls.Add(this.label45);
            this.grpFuel.Controls.Add(this.textBox23);
            this.grpFuel.Controls.Add(this.label6);
            this.grpFuel.Controls.Add(this.txtgstreceivedfuel);
            this.grpFuel.Controls.Add(this.label43);
            this.grpFuel.Controls.Add(this.txtfuelcommexgst);
            this.grpFuel.Controls.Add(this.label42);
            this.grpFuel.Controls.Add(this.txtVpdvdd);
            this.grpFuel.Controls.Add(this.txtCardRembsment);
            this.grpFuel.Controls.Add(this.label41);
            this.grpFuel.Controls.Add(this.txtBSDltr);
            this.grpFuel.Controls.Add(this.label40);
            this.grpFuel.Controls.Add(this.label13);
            this.grpFuel.Controls.Add(this.txtActualShort);
            this.grpFuel.Controls.Add(this.txtDiff);
            this.grpFuel.Controls.Add(this.label11);
            this.grpFuel.Controls.Add(this.txttotalFuel);
            this.grpFuel.Controls.Add(this.label29);
            this.grpFuel.Controls.Add(this.txtAirmiles);
            this.grpFuel.Controls.Add(this.label18);
            this.grpFuel.Controls.Add(this.txtdieselltr);
            this.grpFuel.Controls.Add(this.txtvpowerltr);
            this.grpFuel.Controls.Add(this.txtVppltr);
            this.grpFuel.Controls.Add(this.txtSilverltr);
            this.grpFuel.Controls.Add(this.label5);
            this.grpFuel.Controls.Add(this.label4);
            this.grpFuel.Controls.Add(this.label3);
            this.grpFuel.Controls.Add(this.label2);
            this.grpFuel.Controls.Add(this.label1);
            this.grpFuel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.grpFuel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpFuel.ForeColor = System.Drawing.Color.Black;
            this.grpFuel.Location = new System.Drawing.Point(9, 531);
            this.grpFuel.Name = "grpFuel";
            this.grpFuel.Size = new System.Drawing.Size(1073, 192);
            this.grpFuel.TabIndex = 3;
            this.grpFuel.TabStop = false;
            this.grpFuel.Text = "Fuel Transaction";
            // 
            // DiffInFuelCommTxt
            // 
            this.DiffInFuelCommTxt.BackColor = System.Drawing.Color.LightGray;
            this.DiffInFuelCommTxt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DiffInFuelCommTxt.ForeColor = System.Drawing.Color.Black;
            this.DiffInFuelCommTxt.Location = new System.Drawing.Point(678, 83);
            this.DiffInFuelCommTxt.Name = "DiffInFuelCommTxt";
            this.DiffInFuelCommTxt.Size = new System.Drawing.Size(147, 22);
            this.DiffInFuelCommTxt.TabIndex = 58;
            this.DiffInFuelCommTxt.Text = "0";
            this.DiffInFuelCommTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(564, 83);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(102, 15);
            this.label31.TabIndex = 171;
            this.label31.Text = "Diff in fuel Comm";
            // 
            // dgvFuel
            // 
            this.dgvFuel.AllowDrop = true;
            this.dgvFuel.AllowUserToAddRows = false;
            this.dgvFuel.AllowUserToDeleteRows = false;
            this.dgvFuel.AllowUserToResizeColumns = false;
            this.dgvFuel.AllowUserToResizeRows = false;
            this.dgvFuel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvFuel.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFuel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FuelCheckBox});
            this.dgvFuel.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvFuel.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvFuel.Location = new System.Drawing.Point(927, 22);
            this.dgvFuel.MultiSelect = false;
            this.dgvFuel.Name = "dgvFuel";
            this.dgvFuel.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuel.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFuel.RowHeadersVisible = false;
            this.dgvFuel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvFuel.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvFuel.Size = new System.Drawing.Size(219, 83);
            this.dgvFuel.TabIndex = 61;
            this.dgvFuel.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvFuel.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // FuelCheckBox
            // 
            this.FuelCheckBox.HeaderText = "";
            this.FuelCheckBox.Name = "FuelCheckBox";
            this.FuelCheckBox.Width = 5;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(847, 119);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(58, 15);
            this.label71.TabIndex = 164;
            this.label71.Text = "Remarks";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(832, 26);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(73, 15);
            this.label72.TabIndex = 169;
            this.label72.Text = "Select Docs";
            // 
            // txtbronzeltr
            // 
            this.txtbronzeltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbronzeltr.ForeColor = System.Drawing.Color.Black;
            this.txtbronzeltr.Location = new System.Drawing.Point(114, 23);
            this.txtbronzeltr.MaxLength = 12;
            this.txtbronzeltr.Name = "txtbronzeltr";
            this.txtbronzeltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtbronzeltr.Size = new System.Drawing.Size(154, 22);
            this.txtbronzeltr.TabIndex = 48;
            this.txtbronzeltr.Text = "0";
            this.txtbronzeltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtbronzeltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtbronzeltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtActualCommission
            // 
            this.txtActualCommission.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualCommission.ForeColor = System.Drawing.Color.Black;
            this.txtActualCommission.Location = new System.Drawing.Point(678, 48);
            this.txtActualCommission.MaxLength = 10;
            this.txtActualCommission.Name = "txtActualCommission";
            this.txtActualCommission.Size = new System.Drawing.Size(147, 22);
            this.txtActualCommission.TabIndex = 57;
            this.txtActualCommission.Text = "0";
            this.txtActualCommission.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActualCommission.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtActualCommission.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // txtGasRemarks
            // 
            this.txtGasRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGasRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtGasRemarks.Location = new System.Drawing.Point(927, 113);
            this.txtGasRemarks.MaxLength = 150;
            this.txtGasRemarks.Multiline = true;
            this.txtGasRemarks.Name = "txtGasRemarks";
            this.txtGasRemarks.Size = new System.Drawing.Size(219, 73);
            this.txtGasRemarks.TabIndex = 62;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9F);
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(560, 55);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(112, 15);
            this.label44.TabIndex = 110;
            this.label44.Text = "ActualCommission";
            // 
            // txtdiffinsettlement
            // 
            this.txtdiffinsettlement.BackColor = System.Drawing.Color.LightGray;
            this.txtdiffinsettlement.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdiffinsettlement.ForeColor = System.Drawing.Color.Black;
            this.txtdiffinsettlement.Location = new System.Drawing.Point(448, 720);
            this.txtdiffinsettlement.Name = "txtdiffinsettlement";
            this.txtdiffinsettlement.Size = new System.Drawing.Size(146, 22);
            this.txtdiffinsettlement.TabIndex = 125;
            this.txtdiffinsettlement.Text = "0";
            this.txtdiffinsettlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtdifindrcr
            // 
            this.txtdifindrcr.BackColor = System.Drawing.Color.Silver;
            this.txtdifindrcr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdifindrcr.ForeColor = System.Drawing.Color.Black;
            this.txtdifindrcr.Location = new System.Drawing.Point(678, 139);
            this.txtdifindrcr.Name = "txtdifindrcr";
            this.txtdifindrcr.ReadOnly = true;
            this.txtdifindrcr.Size = new System.Drawing.Size(147, 22);
            this.txtdifindrcr.TabIndex = 60;
            this.txtdifindrcr.Text = "0";
            this.txtdifindrcr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.AliceBlue;
            this.label50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(331, 724);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(107, 16);
            this.label50.TabIndex = 124;
            this.label50.Text = "Diff in Settlement";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(558, 146);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(110, 15);
            this.label45.TabIndex = 114;
            this.label45.Text = "Diff in debit / Credit";
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.Gray;
            this.textBox23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.ForeColor = System.Drawing.Color.Black;
            this.textBox23.Location = new System.Drawing.Point(144, 744);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(155, 22);
            this.textBox23.TabIndex = 113;
            this.textBox23.Text = "0";
            this.textBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(15, 744);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 32);
            this.label6.TabIndex = 112;
            this.label6.Text = "Diff Fuel\r\nCommission";
            // 
            // txtgstreceivedfuel
            // 
            this.txtgstreceivedfuel.BackColor = System.Drawing.Color.LightGray;
            this.txtgstreceivedfuel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgstreceivedfuel.ForeColor = System.Drawing.Color.Black;
            this.txtgstreceivedfuel.Location = new System.Drawing.Point(678, 15);
            this.txtgstreceivedfuel.Name = "txtgstreceivedfuel";
            this.txtgstreceivedfuel.Size = new System.Drawing.Size(147, 22);
            this.txtgstreceivedfuel.TabIndex = 56;
            this.txtgstreceivedfuel.Text = "0";
            this.txtgstreceivedfuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(564, 15);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(106, 30);
            this.label43.TabIndex = 108;
            this.label43.Text = "Gst received on\r\nFuel Commission";
            // 
            // txtfuelcommexgst
            // 
            this.txtfuelcommexgst.BackColor = System.Drawing.Color.LightGray;
            this.txtfuelcommexgst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfuelcommexgst.ForeColor = System.Drawing.Color.Black;
            this.txtfuelcommexgst.Location = new System.Drawing.Point(396, 113);
            this.txtfuelcommexgst.Name = "txtfuelcommexgst";
            this.txtfuelcommexgst.Size = new System.Drawing.Size(155, 22);
            this.txtfuelcommexgst.TabIndex = 55;
            this.txtfuelcommexgst.Text = "0";
            this.txtfuelcommexgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(281, 107);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(109, 30);
            this.label42.TabIndex = 106;
            this.label42.Text = "Fuel Commission \r\nExcluding Gst";
            // 
            // txtVpdvdd
            // 
            this.txtVpdvdd.BackColor = System.Drawing.Color.LightGray;
            this.txtVpdvdd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVpdvdd.ForeColor = System.Drawing.Color.Black;
            this.txtVpdvdd.Location = new System.Drawing.Point(396, 76);
            this.txtVpdvdd.Name = "txtVpdvdd";
            this.txtVpdvdd.ReadOnly = true;
            this.txtVpdvdd.Size = new System.Drawing.Size(154, 22);
            this.txtVpdvdd.TabIndex = 54;
            this.txtVpdvdd.Text = "0";
            this.txtVpdvdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCardRembsment
            // 
            this.txtCardRembsment.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCardRembsment.ForeColor = System.Drawing.Color.Black;
            this.txtCardRembsment.Location = new System.Drawing.Point(678, 111);
            this.txtCardRembsment.MaxLength = 10;
            this.txtCardRembsment.Name = "txtCardRembsment";
            this.txtCardRembsment.Size = new System.Drawing.Size(147, 22);
            this.txtCardRembsment.TabIndex = 59;
            this.txtCardRembsment.Text = "0";
            this.txtCardRembsment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCardRembsment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCardRembsment.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(281, 70);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(102, 30);
            this.label41.TabIndex = 104;
            this.label41.Text = "Total  (VPD+VPG)\r\nin ltr";
            // 
            // txtBSDltr
            // 
            this.txtBSDltr.BackColor = System.Drawing.Color.LightGray;
            this.txtBSDltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSDltr.ForeColor = System.Drawing.Color.Black;
            this.txtBSDltr.Location = new System.Drawing.Point(396, 45);
            this.txtBSDltr.Name = "txtBSDltr";
            this.txtBSDltr.ReadOnly = true;
            this.txtBSDltr.Size = new System.Drawing.Size(154, 22);
            this.txtBSDltr.TabIndex = 53;
            this.txtBSDltr.Text = "0";
            this.txtBSDltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(281, 48);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(112, 15);
            this.label40.TabIndex = 102;
            this.label40.Text = "Total  (B+S+D) in ltr";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(726, 213);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 16);
            this.label13.TabIndex = 81;
            this.label13.Text = "Actual Short";
            // 
            // txtActualShort
            // 
            this.txtActualShort.BackColor = System.Drawing.Color.LightGray;
            this.txtActualShort.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualShort.ForeColor = System.Drawing.Color.Black;
            this.txtActualShort.Location = new System.Drawing.Point(806, 209);
            this.txtActualShort.Name = "txtActualShort";
            this.txtActualShort.ReadOnly = true;
            this.txtActualShort.Size = new System.Drawing.Size(65, 22);
            this.txtActualShort.TabIndex = 80;
            this.txtActualShort.Text = "0";
            // 
            // txtDiff
            // 
            this.txtDiff.BackColor = System.Drawing.Color.LightGray;
            this.txtDiff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiff.ForeColor = System.Drawing.Color.Black;
            this.txtDiff.Location = new System.Drawing.Point(655, 209);
            this.txtDiff.Name = "txtDiff";
            this.txtDiff.ReadOnly = true;
            this.txtDiff.Size = new System.Drawing.Size(65, 22);
            this.txtDiff.TabIndex = 79;
            this.txtDiff.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(566, 212);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 15);
            this.label11.TabIndex = 78;
            this.label11.Text = "Diff";
            // 
            // txttotalFuel
            // 
            this.txttotalFuel.BackColor = System.Drawing.Color.LightGray;
            this.txttotalFuel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalFuel.ForeColor = System.Drawing.Color.Black;
            this.txttotalFuel.Location = new System.Drawing.Point(396, 206);
            this.txttotalFuel.Name = "txttotalFuel";
            this.txttotalFuel.ReadOnly = true;
            this.txttotalFuel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txttotalFuel.Size = new System.Drawing.Size(154, 22);
            this.txttotalFuel.TabIndex = 75;
            this.txttotalFuel.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(281, 209);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(33, 15);
            this.label29.TabIndex = 74;
            this.label29.Text = "Total";
            // 
            // txtAirmiles
            // 
            this.txtAirmiles.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAirmiles.ForeColor = System.Drawing.Color.Black;
            this.txtAirmiles.Location = new System.Drawing.Point(114, 209);
            this.txtAirmiles.MaxLength = 12;
            this.txtAirmiles.Name = "txtAirmiles";
            this.txtAirmiles.Size = new System.Drawing.Size(154, 22);
            this.txtAirmiles.TabIndex = 5;
            this.txtAirmiles.Text = "0";
            this.txtAirmiles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAirmiles.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtAirmiles.Leave += new System.EventHandler(this.txtAirmiles_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(17, 212);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 15);
            this.label18.TabIndex = 62;
            this.label18.Text = "AirMilesCash ";
            // 
            // txtdieselltr
            // 
            this.txtdieselltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdieselltr.ForeColor = System.Drawing.Color.Black;
            this.txtdieselltr.Location = new System.Drawing.Point(114, 48);
            this.txtdieselltr.MaxLength = 12;
            this.txtdieselltr.Name = "txtdieselltr";
            this.txtdieselltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtdieselltr.Size = new System.Drawing.Size(154, 22);
            this.txtdieselltr.TabIndex = 49;
            this.txtdieselltr.Text = "0";
            this.txtdieselltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtdieselltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtdieselltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtvpowerltr
            // 
            this.txtvpowerltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvpowerltr.ForeColor = System.Drawing.Color.Black;
            this.txtvpowerltr.Location = new System.Drawing.Point(396, 17);
            this.txtvpowerltr.MaxLength = 12;
            this.txtvpowerltr.Name = "txtvpowerltr";
            this.txtvpowerltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtvpowerltr.Size = new System.Drawing.Size(154, 22);
            this.txtvpowerltr.TabIndex = 52;
            this.txtvpowerltr.Text = "0";
            this.txtvpowerltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvpowerltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtvpowerltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtVppltr
            // 
            this.txtVppltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVppltr.ForeColor = System.Drawing.Color.Black;
            this.txtVppltr.Location = new System.Drawing.Point(114, 103);
            this.txtVppltr.MaxLength = 12;
            this.txtVppltr.Name = "txtVppltr";
            this.txtVppltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVppltr.Size = new System.Drawing.Size(154, 22);
            this.txtVppltr.TabIndex = 51;
            this.txtVppltr.Text = "0";
            this.txtVppltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVppltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtVppltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtSilverltr
            // 
            this.txtSilverltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSilverltr.ForeColor = System.Drawing.Color.Black;
            this.txtSilverltr.Location = new System.Drawing.Point(114, 75);
            this.txtSilverltr.MaxLength = 12;
            this.txtSilverltr.Name = "txtSilverltr";
            this.txtSilverltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSilverltr.Size = new System.Drawing.Size(154, 22);
            this.txtSilverltr.TabIndex = 50;
            this.txtSilverltr.Text = "0";
            this.txtSilverltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSilverltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtSilverltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtfinalsettlement
            // 
            this.txtfinalsettlement.BackColor = System.Drawing.Color.LightGray;
            this.txtfinalsettlement.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfinalsettlement.ForeColor = System.Drawing.Color.Black;
            this.txtfinalsettlement.Location = new System.Drawing.Point(659, 39);
            this.txtfinalsettlement.Name = "txtfinalsettlement";
            this.txtfinalsettlement.Size = new System.Drawing.Size(134, 22);
            this.txtfinalsettlement.TabIndex = 67;
            this.txtfinalsettlement.Text = "0";
            this.txtfinalsettlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(545, 43);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(99, 15);
            this.label49.TabIndex = 122;
            this.label49.Text = "Final Settlement ";
            // 
            // txttotalsettlement
            // 
            this.txttotalsettlement.BackColor = System.Drawing.Color.LightGray;
            this.txttotalsettlement.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalsettlement.ForeColor = System.Drawing.Color.Black;
            this.txttotalsettlement.Location = new System.Drawing.Point(405, 39);
            this.txttotalsettlement.Name = "txttotalsettlement";
            this.txttotalsettlement.Size = new System.Drawing.Size(133, 22);
            this.txttotalsettlement.TabIndex = 65;
            this.txttotalsettlement.Text = "0";
            this.txttotalsettlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(291, 43);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(98, 15);
            this.label47.TabIndex = 118;
            this.label47.Text = "Total Settlement ";
            // 
            // txtgstpaidonrent
            // 
            this.txtgstpaidonrent.BackColor = System.Drawing.Color.LightGray;
            this.txtgstpaidonrent.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgstpaidonrent.ForeColor = System.Drawing.Color.Black;
            this.txtgstpaidonrent.Location = new System.Drawing.Point(122, 79);
            this.txtgstpaidonrent.Name = "txtgstpaidonrent";
            this.txtgstpaidonrent.Size = new System.Drawing.Size(146, 22);
            this.txtgstpaidonrent.TabIndex = 64;
            this.txtgstpaidonrent.Text = "0";
            this.txtgstpaidonrent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtgstpaidonrent.TextChanged += new System.EventHandler(this.txtgstpaidonrent_TextChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(7, 71);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(78, 30);
            this.label46.TabIndex = 116;
            this.label46.Text = "Gst\r\nPaid on Rent";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(705, 16);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 60;
            this.label62.Text = "Remarks";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(101, 42);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(10, 12);
            this.label60.TabIndex = 147;
            this.label60.Text = "+";
            // 
            // txtTabbRemarks
            // 
            this.txtTabbRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTabbRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtTabbRemarks.Location = new System.Drawing.Point(694, 31);
            this.txtTabbRemarks.MaxLength = 150;
            this.txtTabbRemarks.Multiline = true;
            this.txtTabbRemarks.Name = "txtTabbRemarks";
            this.txtTabbRemarks.Size = new System.Drawing.Size(172, 71);
            this.txtTabbRemarks.TabIndex = 19;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(295, 41);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(10, 12);
            this.label59.TabIndex = 146;
            this.label59.Text = "+";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(392, 41);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(10, 12);
            this.label58.TabIndex = 145;
            this.label58.Text = "+";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(489, 41);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(10, 12);
            this.label57.TabIndex = 144;
            this.label57.Text = "+";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(586, 41);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(10, 12);
            this.label56.TabIndex = 143;
            this.label56.Text = "+";
            // 
            // txtCalstoreother
            // 
            this.txtCalstoreother.BackColor = System.Drawing.Color.LightGray;
            this.txtCalstoreother.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalstoreother.ForeColor = System.Drawing.Color.Black;
            this.txtCalstoreother.Location = new System.Drawing.Point(599, 36);
            this.txtCalstoreother.Name = "txtCalstoreother";
            this.txtCalstoreother.ReadOnly = true;
            this.txtCalstoreother.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCalstoreother.Size = new System.Drawing.Size(81, 22);
            this.txtCalstoreother.TabIndex = 18;
            this.txtCalstoreother.Text = "0";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(608, 17);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(89, 16);
            this.label55.TabIndex = 141;
            this.label55.Text = "In Store Other";
            // 
            // txtshelGisftcard
            // 
            this.txtshelGisftcard.BackColor = System.Drawing.Color.White;
            this.txtshelGisftcard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtshelGisftcard.ForeColor = System.Drawing.Color.Black;
            this.txtshelGisftcard.Location = new System.Drawing.Point(502, 36);
            this.txtshelGisftcard.MaxLength = 10;
            this.txtshelGisftcard.Name = "txtshelGisftcard";
            this.txtshelGisftcard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtshelGisftcard.Size = new System.Drawing.Size(81, 22);
            this.txtshelGisftcard.TabIndex = 17;
            this.txtshelGisftcard.Text = "0";
            this.txtshelGisftcard.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtshelGisftcard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtshelGisftcard.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(503, 17);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(99, 16);
            this.label54.TabIndex = 139;
            this.label54.Text = "Shell Gift Cards";
            // 
            // txtgiftcard
            // 
            this.txtgiftcard.BackColor = System.Drawing.Color.White;
            this.txtgiftcard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgiftcard.ForeColor = System.Drawing.Color.Black;
            this.txtgiftcard.Location = new System.Drawing.Point(405, 36);
            this.txtgiftcard.MaxLength = 10;
            this.txtgiftcard.Name = "txtgiftcard";
            this.txtgiftcard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtgiftcard.Size = new System.Drawing.Size(81, 22);
            this.txtgiftcard.TabIndex = 16;
            this.txtgiftcard.Text = "0";
            this.txtgiftcard.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtgiftcard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtgiftcard.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(417, 17);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(66, 16);
            this.label53.TabIndex = 137;
            this.label53.Text = "Gift Cards";
            // 
            // txtphonecard
            // 
            this.txtphonecard.BackColor = System.Drawing.Color.White;
            this.txtphonecard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphonecard.ForeColor = System.Drawing.Color.Black;
            this.txtphonecard.Location = new System.Drawing.Point(308, 36);
            this.txtphonecard.MaxLength = 10;
            this.txtphonecard.Name = "txtphonecard";
            this.txtphonecard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtphonecard.Size = new System.Drawing.Size(81, 22);
            this.txtphonecard.TabIndex = 15;
            this.txtphonecard.Text = "0";
            this.txtphonecard.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtphonecard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtphonecard.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(310, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(83, 16);
            this.label52.TabIndex = 135;
            this.label52.Text = "Phone Cards";
            // 
            // txtscratchlotto
            // 
            this.txtscratchlotto.BackColor = System.Drawing.Color.White;
            this.txtscratchlotto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscratchlotto.ForeColor = System.Drawing.Color.Black;
            this.txtscratchlotto.Location = new System.Drawing.Point(211, 36);
            this.txtscratchlotto.MaxLength = 10;
            this.txtscratchlotto.Name = "txtscratchlotto";
            this.txtscratchlotto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtscratchlotto.Size = new System.Drawing.Size(81, 22);
            this.txtscratchlotto.TabIndex = 14;
            this.txtscratchlotto.Text = "0";
            this.txtscratchlotto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtscratchlotto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtscratchlotto.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(212, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 16);
            this.label8.TabIndex = 133;
            this.label8.Text = "Scratch Lotto";
            // 
            // txtonlinelotto
            // 
            this.txtonlinelotto.BackColor = System.Drawing.Color.White;
            this.txtonlinelotto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtonlinelotto.ForeColor = System.Drawing.Color.Black;
            this.txtonlinelotto.Location = new System.Drawing.Point(114, 36);
            this.txtonlinelotto.MaxLength = 10;
            this.txtonlinelotto.Name = "txtonlinelotto";
            this.txtonlinelotto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtonlinelotto.Size = new System.Drawing.Size(81, 22);
            this.txtonlinelotto.TabIndex = 13;
            this.txtonlinelotto.Text = "0";
            this.txtonlinelotto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtonlinelotto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtonlinelotto.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(115, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 15);
            this.label9.TabIndex = 131;
            this.label9.Text = "Online Lotto";
            // 
            // txttobbaco
            // 
            this.txttobbaco.BackColor = System.Drawing.Color.White;
            this.txttobbaco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttobbaco.ForeColor = System.Drawing.Color.Black;
            this.txttobbaco.Location = new System.Drawing.Point(17, 36);
            this.txttobbaco.MaxLength = 10;
            this.txttobbaco.Name = "txttobbaco";
            this.txttobbaco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txttobbaco.Size = new System.Drawing.Size(81, 22);
            this.txttobbaco.TabIndex = 12;
            this.txttobbaco.Text = "0";
            this.txttobbaco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttobbaco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txttobbaco.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(18, 18);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(56, 16);
            this.label51.TabIndex = 129;
            this.label51.Text = "Tobacco";
            // 
            // txtSalTotal
            // 
            this.txtSalTotal.BackColor = System.Drawing.Color.LightGray;
            this.txtSalTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalTotal.ForeColor = System.Drawing.Color.Black;
            this.txtSalTotal.Location = new System.Drawing.Point(407, 80);
            this.txtSalTotal.Name = "txtSalTotal";
            this.txtSalTotal.ReadOnly = true;
            this.txtSalTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSalTotal.Size = new System.Drawing.Size(154, 22);
            this.txtSalTotal.TabIndex = 25;
            this.txtSalTotal.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(407, 59);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 16);
            this.label24.TabIndex = 64;
            this.label24.Text = "Total";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(117, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 16);
            this.label19.TabIndex = 60;
            this.label19.Text = "Fuel";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(300, 59);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 16);
            this.label20.TabIndex = 58;
            this.label20.Text = "Deposit  Levy";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(213, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 16);
            this.label21.TabIndex = 56;
            this.label21.Text = "GST";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(17, 62);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 16);
            this.label22.TabIndex = 54;
            this.label22.Text = "Discounts (-ve)";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(198, 42);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(10, 12);
            this.label61.TabIndex = 148;
            this.label61.Text = "+";
            // 
            // txtActualSettlement
            // 
            this.txtActualSettlement.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualSettlement.ForeColor = System.Drawing.Color.Black;
            this.txtActualSettlement.Location = new System.Drawing.Point(659, 72);
            this.txtActualSettlement.MaxLength = 10;
            this.txtActualSettlement.Name = "txtActualSettlement";
            this.txtActualSettlement.Size = new System.Drawing.Size(134, 22);
            this.txtActualSettlement.TabIndex = 68;
            this.txtActualSettlement.Text = "0";
            this.txtActualSettlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActualSettlement.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtActualSettlement.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // txtShellCredit
            // 
            this.txtShellCredit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShellCredit.ForeColor = System.Drawing.Color.Black;
            this.txtShellCredit.Location = new System.Drawing.Point(626, 25);
            this.txtShellCredit.MaxLength = 10;
            this.txtShellCredit.Name = "txtShellCredit";
            this.txtShellCredit.Size = new System.Drawing.Size(91, 22);
            this.txtShellCredit.TabIndex = 76;
            this.txtShellCredit.Text = "0";
            this.txtShellCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShellCredit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtShellCredit.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(545, 27);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(75, 16);
            this.label68.TabIndex = 158;
            this.label68.Text = "Shell Credit";
            // 
            // txtShellDebit
            // 
            this.txtShellDebit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShellDebit.ForeColor = System.Drawing.Color.Black;
            this.txtShellDebit.Location = new System.Drawing.Point(454, 24);
            this.txtShellDebit.MaxLength = 10;
            this.txtShellDebit.Name = "txtShellDebit";
            this.txtShellDebit.Size = new System.Drawing.Size(91, 22);
            this.txtShellDebit.TabIndex = 75;
            this.txtShellDebit.Text = "0";
            this.txtShellDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShellDebit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtShellDebit.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(377, 28);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(71, 16);
            this.label69.TabIndex = 156;
            this.label69.Text = "Shell Debit";
            // 
            // txtRadiantcr
            // 
            this.txtRadiantcr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRadiantcr.ForeColor = System.Drawing.Color.Black;
            this.txtRadiantcr.Location = new System.Drawing.Point(283, 25);
            this.txtRadiantcr.MaxLength = 10;
            this.txtRadiantcr.Name = "txtRadiantcr";
            this.txtRadiantcr.Size = new System.Drawing.Size(91, 22);
            this.txtRadiantcr.TabIndex = 74;
            this.txtRadiantcr.Text = "0";
            this.txtRadiantcr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRadiantcr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtRadiantcr.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Arial", 9F);
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(193, 29);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(86, 15);
            this.label67.TabIndex = 154;
            this.label67.Text = "Radiant Credit";
            // 
            // txtfairshare
            // 
            this.txtfairshare.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfairshare.ForeColor = System.Drawing.Color.Black;
            this.txtfairshare.Location = new System.Drawing.Point(405, 72);
            this.txtfairshare.MaxLength = 10;
            this.txtfairshare.Name = "txtfairshare";
            this.txtfairshare.Size = new System.Drawing.Size(133, 22);
            this.txtfairshare.TabIndex = 66;
            this.txtfairshare.Text = "0";
            this.txtfairshare.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfairshare.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtfairshare.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(293, 72);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(68, 16);
            this.label48.TabIndex = 120;
            this.label48.Text = "Fair Share";
            // 
            // txtRadiantdb
            // 
            this.txtRadiantdb.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRadiantdb.ForeColor = System.Drawing.Color.Black;
            this.txtRadiantdb.Location = new System.Drawing.Point(99, 26);
            this.txtRadiantdb.MaxLength = 10;
            this.txtRadiantdb.Name = "txtRadiantdb";
            this.txtRadiantdb.Size = new System.Drawing.Size(91, 22);
            this.txtRadiantdb.TabIndex = 73;
            this.txtRadiantdb.Text = "0";
            this.txtRadiantdb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRadiantdb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtRadiantdb.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9F);
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(12, 28);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(82, 15);
            this.label30.TabIndex = 76;
            this.label30.Text = "Radiant Debit";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(29, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 15);
            this.label23.TabIndex = 52;
            this.label23.Text = "C-Store Sale ";
            // 
            // grpSale
            // 
            this.grpSale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSale.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpSale.BackColor = System.Drawing.Color.AliceBlue;
            this.grpSale.Controls.Add(this.dgvCstore);
            this.grpSale.Controls.Add(this.label17);
            this.grpSale.Controls.Add(this.txtCStoreRemarks);
            this.grpSale.Controls.Add(this.label16);
            this.grpSale.Controls.Add(this.label23);
            this.grpSale.Controls.Add(this.txtCStoreSale);
            this.grpSale.Location = new System.Drawing.Point(500, -1);
            this.grpSale.Name = "grpSale";
            this.grpSale.Size = new System.Drawing.Size(565, 103);
            this.grpSale.TabIndex = 0;
            this.grpSale.TabStop = false;
            this.grpSale.Text = "Sale";
            // 
            // dgvCstore
            // 
            this.dgvCstore.AllowDrop = true;
            this.dgvCstore.AllowUserToAddRows = false;
            this.dgvCstore.AllowUserToDeleteRows = false;
            this.dgvCstore.AllowUserToResizeColumns = false;
            this.dgvCstore.AllowUserToResizeRows = false;
            this.dgvCstore.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCstore.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCstore.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCstore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCstore.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CStoreCheckBox});
            this.dgvCstore.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvCstore.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvCstore.Location = new System.Drawing.Point(445, 14);
            this.dgvCstore.MultiSelect = false;
            this.dgvCstore.Name = "dgvCstore";
            this.dgvCstore.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCstore.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCstore.RowHeadersVisible = false;
            this.dgvCstore.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvCstore.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvCstore.Size = new System.Drawing.Size(222, 83);
            this.dgvCstore.TabIndex = 11;
            this.dgvCstore.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvCstore.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // CStoreCheckBox
            // 
            this.CStoreCheckBox.HeaderText = "";
            this.CStoreCheckBox.Name = "CStoreCheckBox";
            this.CStoreCheckBox.Width = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(29, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 15);
            this.label17.TabIndex = 56;
            this.label17.Text = "Remarks";
            // 
            // txtCStoreRemarks
            // 
            this.txtCStoreRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCStoreRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtCStoreRemarks.Location = new System.Drawing.Point(129, 42);
            this.txtCStoreRemarks.MaxLength = 150;
            this.txtCStoreRemarks.Multiline = true;
            this.txtCStoreRemarks.Name = "txtCStoreRemarks";
            this.txtCStoreRemarks.Size = new System.Drawing.Size(173, 57);
            this.txtCStoreRemarks.TabIndex = 10;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(350, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 15);
            this.label16.TabIndex = 53;
            this.label16.Text = "Select Docs";
            // 
            // grpSettlement
            // 
            this.grpSettlement.Controls.Add(this.SettlementpopBtn);
            this.grpSettlement.Controls.Add(this.label83);
            this.grpSettlement.Controls.Add(this.DiffInSettlementTxt);
            this.grpSettlement.Controls.Add(this.label82);
            this.grpSettlement.Controls.Add(this.dgvSettlement);
            this.grpSettlement.Controls.Add(this.label73);
            this.grpSettlement.Controls.Add(this.label65);
            this.grpSettlement.Controls.Add(this.txtShellRemarks);
            this.grpSettlement.Controls.Add(this.txtgstpaidonrent);
            this.grpSettlement.Controls.Add(this.label46);
            this.grpSettlement.Controls.Add(this.txtfinalsettlement);
            this.grpSettlement.Controls.Add(this.label49);
            this.grpSettlement.Controls.Add(this.txttotalsettlement);
            this.grpSettlement.Controls.Add(this.label47);
            this.grpSettlement.Controls.Add(this.txtdailystorerntincgst);
            this.grpSettlement.Controls.Add(this.label10);
            this.grpSettlement.Controls.Add(this.label12);
            this.grpSettlement.Controls.Add(this.txtfairshare);
            this.grpSettlement.Controls.Add(this.label48);
            this.grpSettlement.Controls.Add(this.txtActualSettlement);
            this.grpSettlement.Location = new System.Drawing.Point(9, 729);
            this.grpSettlement.Name = "grpSettlement";
            this.grpSettlement.Size = new System.Drawing.Size(1186, 179);
            this.grpSettlement.TabIndex = 4;
            this.grpSettlement.TabStop = false;
            this.grpSettlement.Text = "Settlement";
            // 
            // SettlementpopBtn
            // 
            this.SettlementpopBtn.Location = new System.Drawing.Point(660, 131);
            this.SettlementpopBtn.Name = "SettlementpopBtn";
            this.SettlementpopBtn.Size = new System.Drawing.Size(133, 26);
            this.SettlementpopBtn.TabIndex = 70;
            this.SettlementpopBtn.Text = "Open Form";
            this.SettlementpopBtn.UseVisualStyleBackColor = true;
            this.SettlementpopBtn.Click += new System.EventHandler(this.SettlementpopBtn_Click);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(542, 135);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(94, 15);
            this.label83.TabIndex = 176;
            this.label83.Text = "Past Settlement";
            // 
            // DiffInSettlementTxt
            // 
            this.DiffInSettlementTxt.BackColor = System.Drawing.Color.LightGray;
            this.DiffInSettlementTxt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DiffInSettlementTxt.ForeColor = System.Drawing.Color.Black;
            this.DiffInSettlementTxt.Location = new System.Drawing.Point(659, 101);
            this.DiffInSettlementTxt.Name = "DiffInSettlementTxt";
            this.DiffInSettlementTxt.Size = new System.Drawing.Size(134, 22);
            this.DiffInSettlementTxt.TabIndex = 69;
            this.DiffInSettlementTxt.Text = "0";
            this.DiffInSettlementTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Black;
            this.label82.Location = new System.Drawing.Point(545, 105);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(25, 15);
            this.label82.TabIndex = 175;
            this.label82.Text = "Diff";
            // 
            // dgvSettlement
            // 
            this.dgvSettlement.AllowDrop = true;
            this.dgvSettlement.AllowUserToAddRows = false;
            this.dgvSettlement.AllowUserToDeleteRows = false;
            this.dgvSettlement.AllowUserToResizeColumns = false;
            this.dgvSettlement.AllowUserToResizeRows = false;
            this.dgvSettlement.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvSettlement.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSettlement.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvSettlement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSettlement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SettlementCheck});
            this.dgvSettlement.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvSettlement.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvSettlement.Location = new System.Drawing.Point(927, 10);
            this.dgvSettlement.MultiSelect = false;
            this.dgvSettlement.Name = "dgvSettlement";
            this.dgvSettlement.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSettlement.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvSettlement.RowHeadersVisible = false;
            this.dgvSettlement.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvSettlement.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvSettlement.Size = new System.Drawing.Size(222, 83);
            this.dgvSettlement.TabIndex = 71;
            this.dgvSettlement.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvSettlement.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // SettlementCheck
            // 
            this.SettlementCheck.HeaderText = "";
            this.SettlementCheck.Name = "SettlementCheck";
            this.SettlementCheck.Width = 5;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(846, 10);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(73, 15);
            this.label73.TabIndex = 172;
            this.label73.Text = "Select Docs";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(864, 118);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(58, 15);
            this.label65.TabIndex = 150;
            this.label65.Text = "Remarks";
            // 
            // txtShellRemarks
            // 
            this.txtShellRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShellRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtShellRemarks.Location = new System.Drawing.Point(927, 99);
            this.txtShellRemarks.MaxLength = 150;
            this.txtShellRemarks.Multiline = true;
            this.txtShellRemarks.Name = "txtShellRemarks";
            this.txtShellRemarks.Size = new System.Drawing.Size(222, 58);
            this.txtShellRemarks.TabIndex = 72;
            // 
            // txtradiantcrdiff
            // 
            this.txtradiantcrdiff.BackColor = System.Drawing.Color.LightGray;
            this.txtradiantcrdiff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtradiantcrdiff.ForeColor = System.Drawing.Color.Black;
            this.txtradiantcrdiff.Location = new System.Drawing.Point(957, 22);
            this.txtradiantcrdiff.Name = "txtradiantcrdiff";
            this.txtradiantcrdiff.ReadOnly = true;
            this.txtradiantcrdiff.Size = new System.Drawing.Size(91, 22);
            this.txtradiantcrdiff.TabIndex = 78;
            this.txtradiantcrdiff.Text = "0";
            this.txtradiantcrdiff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtradiantcrdiff.TextChanged += new System.EventHandler(this.txtradiantcrdiff_TextChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(891, 26);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(64, 16);
            this.label66.TabIndex = 162;
            this.label66.Text = "Credit Diff";
            this.label66.Click += new System.EventHandler(this.label66_Click);
            // 
            // txtRadiantdbdiff
            // 
            this.txtRadiantdbdiff.BackColor = System.Drawing.Color.LightGray;
            this.txtRadiantdbdiff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRadiantdbdiff.ForeColor = System.Drawing.Color.Black;
            this.txtRadiantdbdiff.Location = new System.Drawing.Point(794, 24);
            this.txtRadiantdbdiff.Name = "txtRadiantdbdiff";
            this.txtRadiantdbdiff.ReadOnly = true;
            this.txtRadiantdbdiff.Size = new System.Drawing.Size(91, 22);
            this.txtRadiantdbdiff.TabIndex = 77;
            this.txtRadiantdbdiff.Text = "0";
            this.txtRadiantdbdiff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(726, 28);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(60, 16);
            this.label70.TabIndex = 161;
            this.label70.Text = "Debit Diff";
            // 
            // grpTabbocco
            // 
            this.grpTabbocco.AutoSize = true;
            this.grpTabbocco.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpTabbocco.Controls.Add(this.dgvTabbacco);
            this.grpTabbocco.Controls.Add(this.label62);
            this.grpTabbocco.Controls.Add(this.label63);
            this.grpTabbocco.Controls.Add(this.txtTabbRemarks);
            this.grpTabbocco.Controls.Add(this.label60);
            this.grpTabbocco.Controls.Add(this.label61);
            this.grpTabbocco.Controls.Add(this.txtDiscount);
            this.grpTabbocco.Controls.Add(this.txtGST);
            this.grpTabbocco.Controls.Add(this.label59);
            this.grpTabbocco.Controls.Add(this.txtDepositLevy);
            this.grpTabbocco.Controls.Add(this.txtFuel);
            this.grpTabbocco.Controls.Add(this.label58);
            this.grpTabbocco.Controls.Add(this.label22);
            this.grpTabbocco.Controls.Add(this.label21);
            this.grpTabbocco.Controls.Add(this.label57);
            this.grpTabbocco.Controls.Add(this.label20);
            this.grpTabbocco.Controls.Add(this.label56);
            this.grpTabbocco.Controls.Add(this.label19);
            this.grpTabbocco.Controls.Add(this.txtCalstoreother);
            this.grpTabbocco.Controls.Add(this.label24);
            this.grpTabbocco.Controls.Add(this.label55);
            this.grpTabbocco.Controls.Add(this.txtSalTotal);
            this.grpTabbocco.Controls.Add(this.txtshelGisftcard);
            this.grpTabbocco.Controls.Add(this.label51);
            this.grpTabbocco.Controls.Add(this.label54);
            this.grpTabbocco.Controls.Add(this.txttobbaco);
            this.grpTabbocco.Controls.Add(this.txtgiftcard);
            this.grpTabbocco.Controls.Add(this.label9);
            this.grpTabbocco.Controls.Add(this.txtonlinelotto);
            this.grpTabbocco.Controls.Add(this.label53);
            this.grpTabbocco.Controls.Add(this.label8);
            this.grpTabbocco.Controls.Add(this.txtscratchlotto);
            this.grpTabbocco.Controls.Add(this.txtphonecard);
            this.grpTabbocco.Controls.Add(this.label52);
            this.grpTabbocco.Location = new System.Drawing.Point(9, 108);
            this.grpTabbocco.Name = "grpTabbocco";
            this.grpTabbocco.Size = new System.Drawing.Size(1155, 121);
            this.grpTabbocco.TabIndex = 1;
            this.grpTabbocco.TabStop = false;
            this.grpTabbocco.Text = "Tobacco";
            // 
            // dgvTabbacco
            // 
            this.dgvTabbacco.AllowDrop = true;
            this.dgvTabbacco.AllowUserToAddRows = false;
            this.dgvTabbacco.AllowUserToDeleteRows = false;
            this.dgvTabbacco.AllowUserToResizeColumns = false;
            this.dgvTabbacco.AllowUserToResizeRows = false;
            this.dgvTabbacco.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvTabbacco.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTabbacco.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvTabbacco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTabbacco.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tabbaco});
            this.dgvTabbacco.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvTabbacco.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvTabbacco.Location = new System.Drawing.Point(927, 16);
            this.dgvTabbacco.MultiSelect = false;
            this.dgvTabbacco.Name = "dgvTabbacco";
            this.dgvTabbacco.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTabbacco.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvTabbacco.RowHeadersVisible = false;
            this.dgvTabbacco.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvTabbacco.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvTabbacco.Size = new System.Drawing.Size(222, 83);
            this.dgvTabbacco.TabIndex = 20;
            this.dgvTabbacco.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvTabbacco.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // Tabbaco
            // 
            this.Tabbaco.HeaderText = "";
            this.Tabbaco.Name = "Tabbaco";
            this.Tabbaco.Width = 5;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(849, 13);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(73, 15);
            this.label63.TabIndex = 67;
            this.label63.Text = "Select Docs";
            // 
            // btnCals
            // 
            this.btnCals.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCals.FlatAppearance.BorderSize = 2;
            this.btnCals.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCals.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCals.Location = new System.Drawing.Point(92, 7);
            this.btnCals.Name = "btnCals";
            this.btnCals.Size = new System.Drawing.Size(78, 32);
            this.btnCals.TabIndex = 1;
            this.btnCals.Text = "&Calculate";
            this.btnCals.UseVisualStyleBackColor = true;
            this.btnCals.Click += new System.EventHandler(this.btnCals_Click);
            // 
            // SubmitBtn
            // 
            this.SubmitBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SubmitBtn.FlatAppearance.BorderSize = 2;
            this.SubmitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SubmitBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SubmitBtn.Location = new System.Drawing.Point(10, 43);
            this.SubmitBtn.Name = "SubmitBtn";
            this.SubmitBtn.Size = new System.Drawing.Size(78, 32);
            this.SubmitBtn.TabIndex = 4;
            this.SubmitBtn.Text = "Submit";
            this.SubmitBtn.UseVisualStyleBackColor = true;
            this.SubmitBtn.Visible = false;
            this.SubmitBtn.Click += new System.EventHandler(this.SubmitBtn_Click);
            // 
            // ClearFormBtn
            // 
            this.ClearFormBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ClearFormBtn.FlatAppearance.BorderSize = 2;
            this.ClearFormBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ClearFormBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClearFormBtn.Location = new System.Drawing.Point(176, 7);
            this.ClearFormBtn.Name = "ClearFormBtn";
            this.ClearFormBtn.Size = new System.Drawing.Size(98, 31);
            this.ClearFormBtn.TabIndex = 2;
            this.ClearFormBtn.Text = "Clear Form";
            this.ClearFormBtn.UseVisualStyleBackColor = true;
            this.ClearFormBtn.Visible = false;
            this.ClearFormBtn.Click += new System.EventHandler(this.ClearFormBtn_Click);
            // 
            // ViewBtn
            // 
            this.ViewBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ViewBtn.FlatAppearance.BorderSize = 2;
            this.ViewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewBtn.Location = new System.Drawing.Point(370, 70);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(111, 32);
            this.ViewBtn.TabIndex = 8;
            this.ViewBtn.Text = "View";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(1190, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(147, 546);
            this.groupBox2.TabIndex = 60;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Approval Summary";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.SummaryGridView);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(6, 1);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 532);
            this.groupBox3.TabIndex = 61;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Approval Summary";
            // 
            // SummaryGridView
            // 
            this.SummaryGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SummaryGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.SummaryGridView.BackgroundColor = System.Drawing.Color.White;
            this.SummaryGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SummaryGridView.Location = new System.Drawing.Point(10, 21);
            this.SummaryGridView.Name = "SummaryGridView";
            this.SummaryGridView.RowHeadersVisible = false;
            this.SummaryGridView.Size = new System.Drawing.Size(137, 507);
            this.SummaryGridView.TabIndex = 0;
            this.SummaryGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // ReferBackBtn
            // 
            this.ReferBackBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ReferBackBtn.FlatAppearance.BorderSize = 2;
            this.ReferBackBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReferBackBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReferBackBtn.Location = new System.Drawing.Point(94, 43);
            this.ReferBackBtn.Name = "ReferBackBtn";
            this.ReferBackBtn.Size = new System.Drawing.Size(100, 32);
            this.ReferBackBtn.TabIndex = 5;
            this.ReferBackBtn.Text = "Refer Back";
            this.ReferBackBtn.UseVisualStyleBackColor = true;
            this.ReferBackBtn.Visible = false;
            this.ReferBackBtn.Click += new System.EventHandler(this.ReferBackBtn_Click);
            // 
            // EscalateBtn
            // 
            this.EscalateBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.EscalateBtn.FlatAppearance.BorderSize = 2;
            this.EscalateBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.EscalateBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EscalateBtn.Location = new System.Drawing.Point(201, 45);
            this.EscalateBtn.Name = "EscalateBtn";
            this.EscalateBtn.Size = new System.Drawing.Size(100, 32);
            this.EscalateBtn.TabIndex = 6;
            this.EscalateBtn.Text = "Escalate";
            this.EscalateBtn.UseVisualStyleBackColor = true;
            this.EscalateBtn.Visible = false;
            this.EscalateBtn.Click += new System.EventHandler(this.EscalateBtn_Click);
            // 
            // StatusComboBox
            // 
            this.StatusComboBox.FormattingEnabled = true;
            this.StatusComboBox.Items.AddRange(new object[] {
            "Submitted",
            "Completed",
            "Closed",
            "Reopen",
            "ReferBack",
            "Escalated"});
            this.StatusComboBox.Location = new System.Drawing.Point(12, 78);
            this.StatusComboBox.Name = "StatusComboBox";
            this.StatusComboBox.Size = new System.Drawing.Size(121, 21);
            this.StatusComboBox.TabIndex = 64;
            this.StatusComboBox.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label69);
            this.groupBox1.Controls.Add(this.txtradiantcrdiff);
            this.groupBox1.Controls.Add(this.txtShellDebit);
            this.groupBox1.Controls.Add(this.label67);
            this.groupBox1.Controls.Add(this.label66);
            this.groupBox1.Controls.Add(this.txtRadiantcr);
            this.groupBox1.Controls.Add(this.txtRadiantdbdiff);
            this.groupBox1.Controls.Add(this.txtShellCredit);
            this.groupBox1.Controls.Add(this.label68);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label70);
            this.groupBox1.Controls.Add(this.txtRadiantdb);
            this.groupBox1.Location = new System.Drawing.Point(12, 892);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1183, 98);
            this.groupBox1.TabIndex = 174;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " Radiant";
            // 
            // txtActualBankDeposit
            // 
            this.txtActualBankDeposit.BackColor = System.Drawing.SystemColors.Window;
            this.txtActualBankDeposit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualBankDeposit.ForeColor = System.Drawing.Color.Black;
            this.txtActualBankDeposit.Location = new System.Drawing.Point(743, 119);
            this.txtActualBankDeposit.Name = "txtActualBankDeposit";
            this.txtActualBankDeposit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtActualBankDeposit.Size = new System.Drawing.Size(147, 22);
            this.txtActualBankDeposit.TabIndex = 44;
            this.txtActualBankDeposit.Text = "0";
            this.txtActualBankDeposit.Leave += new System.EventHandler(this.txtActualBankDeposit_Leave);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(600, 122);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(127, 16);
            this.label39.TabIndex = 100;
            this.label39.Text = "Actual Bank Deposit";
            // 
            // txtleftBankdeposit
            // 
            this.txtleftBankdeposit.BackColor = System.Drawing.Color.LightGray;
            this.txtleftBankdeposit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtleftBankdeposit.ForeColor = System.Drawing.Color.Black;
            this.txtleftBankdeposit.Location = new System.Drawing.Point(743, 88);
            this.txtleftBankdeposit.Name = "txtleftBankdeposit";
            this.txtleftBankdeposit.ReadOnly = true;
            this.txtleftBankdeposit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtleftBankdeposit.Size = new System.Drawing.Size(147, 22);
            this.txtleftBankdeposit.TabIndex = 43;
            this.txtleftBankdeposit.Text = "0";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(596, 92);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(141, 15);
            this.label38.TabIndex = 98;
            this.label38.Text = "Bal left for Bank Deposit ";
            // 
            // txtCashReceived
            // 
            this.txtCashReceived.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashReceived.ForeColor = System.Drawing.Color.Black;
            this.txtCashReceived.Location = new System.Drawing.Point(743, 31);
            this.txtCashReceived.MaxLength = 12;
            this.txtCashReceived.Name = "txtCashReceived";
            this.txtCashReceived.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashReceived.Size = new System.Drawing.Size(147, 22);
            this.txtCashReceived.TabIndex = 41;
            this.txtCashReceived.Text = "0";
            this.txtCashReceived.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashReceived.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashReceived.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9F);
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(596, 38);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(91, 15);
            this.label36.TabIndex = 94;
            this.label36.Text = "Cash Received";
            // 
            // txtGiftcert
            // 
            this.txtGiftcert.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiftcert.ForeColor = System.Drawing.Color.Black;
            this.txtGiftcert.Location = new System.Drawing.Point(427, 225);
            this.txtGiftcert.MaxLength = 12;
            this.txtGiftcert.Name = "txtGiftcert";
            this.txtGiftcert.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtGiftcert.Size = new System.Drawing.Size(155, 22);
            this.txtGiftcert.TabIndex = 40;
            this.txtGiftcert.Text = "0";
            this.txtGiftcert.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGiftcert.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            // 
            // txtCashSort
            // 
            this.txtCashSort.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashSort.ForeColor = System.Drawing.Color.Black;
            this.txtCashSort.Location = new System.Drawing.Point(427, 193);
            this.txtCashSort.MaxLength = 12;
            this.txtCashSort.Name = "txtCashSort";
            this.txtCashSort.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashSort.Size = new System.Drawing.Size(155, 22);
            this.txtCashSort.TabIndex = 39;
            this.txtCashSort.Text = "0";
            this.txtCashSort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashSort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashSort.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // txtCashpaidmgt
            // 
            this.txtCashpaidmgt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashpaidmgt.ForeColor = System.Drawing.Color.Black;
            this.txtCashpaidmgt.Location = new System.Drawing.Point(428, 158);
            this.txtCashpaidmgt.MaxLength = 12;
            this.txtCashpaidmgt.Name = "txtCashpaidmgt";
            this.txtCashpaidmgt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashpaidmgt.Size = new System.Drawing.Size(154, 22);
            this.txtCashpaidmgt.TabIndex = 38;
            this.txtCashpaidmgt.Text = "0";
            this.txtCashpaidmgt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashpaidmgt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashpaidmgt.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(306, 123);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(108, 30);
            this.label32.TabIndex = 88;
            this.label32.Text = "Cash Paid to third \r\nparties/employee";
            // 
            // txtCashPaidEmp
            // 
            this.txtCashPaidEmp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashPaidEmp.ForeColor = System.Drawing.Color.Black;
            this.txtCashPaidEmp.Location = new System.Drawing.Point(427, 127);
            this.txtCashPaidEmp.MaxLength = 12;
            this.txtCashPaidEmp.Name = "txtCashPaidEmp";
            this.txtCashPaidEmp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashPaidEmp.Size = new System.Drawing.Size(155, 22);
            this.txtCashPaidEmp.TabIndex = 37;
            this.txtCashPaidEmp.Text = "0";
            this.txtCashPaidEmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashPaidEmp.TextChanged += new System.EventHandler(this.txtCashPaidEmp_TextChanged);
            this.txtCashPaidEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashPaidEmp.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(304, 88);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(99, 30);
            this.label35.TabIndex = 82;
            this.label35.Text = "Cash Paid for \r\nStore Purchases";
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // txtCashPaid
            // 
            this.txtCashPaid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashPaid.ForeColor = System.Drawing.Color.Black;
            this.txtCashPaid.Location = new System.Drawing.Point(428, 99);
            this.txtCashPaid.MaxLength = 12;
            this.txtCashPaid.Name = "txtCashPaid";
            this.txtCashPaid.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashPaid.Size = new System.Drawing.Size(154, 22);
            this.txtCashPaid.TabIndex = 36;
            this.txtCashPaid.Text = "0";
            this.txtCashPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashPaid.TextChanged += new System.EventHandler(this.txtCashPaid_TextChanged);
            this.txtCashPaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashPaid.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // txtCashRemarks
            // 
            this.txtCashRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtCashRemarks.Location = new System.Drawing.Point(926, 176);
            this.txtCashRemarks.MaxLength = 150;
            this.txtCashRemarks.Multiline = true;
            this.txtCashRemarks.Name = "txtCashRemarks";
            this.txtCashRemarks.Size = new System.Drawing.Size(222, 95);
            this.txtCashRemarks.TabIndex = 47;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(863, 185);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(58, 15);
            this.label74.TabIndex = 168;
            this.label74.Text = "Remarks";
            // 
            // txtcashpayoutStore
            // 
            this.txtcashpayoutStore.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcashpayoutStore.ForeColor = System.Drawing.Color.Black;
            this.txtcashpayoutStore.Location = new System.Drawing.Point(428, 31);
            this.txtcashpayoutStore.MaxLength = 12;
            this.txtcashpayoutStore.Name = "txtcashpayoutStore";
            this.txtcashpayoutStore.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtcashpayoutStore.Size = new System.Drawing.Size(154, 22);
            this.txtcashpayoutStore.TabIndex = 34;
            this.txtcashpayoutStore.Text = "0";
            this.txtcashpayoutStore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcashpayoutStore.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtcashpayoutStore.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(307, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(116, 30);
            this.label26.TabIndex = 70;
            this.label26.Text = "Cash Payout \r\nfor Store Purchases";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(911, 10);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(73, 15);
            this.label64.TabIndex = 149;
            this.label64.Text = "Select Docs";
            // 
            // dgvCash
            // 
            this.dgvCash.AllowDrop = true;
            this.dgvCash.AllowUserToAddRows = false;
            this.dgvCash.AllowUserToDeleteRows = false;
            this.dgvCash.AllowUserToResizeColumns = false;
            this.dgvCash.AllowUserToResizeRows = false;
            this.dgvCash.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCash.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCash.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvCash.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCash.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CashCheckBox});
            this.dgvCash.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvCash.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvCash.Location = new System.Drawing.Point(926, 31);
            this.dgvCash.MultiSelect = false;
            this.dgvCash.Name = "dgvCash";
            this.dgvCash.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCash.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvCash.RowHeadersVisible = false;
            this.dgvCash.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvCash.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvCash.Size = new System.Drawing.Size(222, 105);
            this.dgvCash.TabIndex = 46;
            this.dgvCash.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvCash.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // CashCheckBox
            // 
            this.CashCheckBox.HeaderText = "";
            this.CashCheckBox.Name = "CashCheckBox";
            this.CashCheckBox.Width = 5;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(9, 76);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 30);
            this.label28.TabIndex = 172;
            this.label28.Text = "Cash\r\n(Drive Away)";
            this.label28.Click += new System.EventHandler(this.label28_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(11, 38);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(81, 30);
            this.label27.TabIndex = 173;
            this.label27.Text = "Cash for\r\nBank Deposit";
            // 
            // txtCashBankDep
            // 
            this.txtCashBankDep.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashBankDep.ForeColor = System.Drawing.Color.Black;
            this.txtCashBankDep.Location = new System.Drawing.Point(122, 46);
            this.txtCashBankDep.MaxLength = 12;
            this.txtCashBankDep.Name = "txtCashBankDep";
            this.txtCashBankDep.Size = new System.Drawing.Size(154, 22);
            this.txtCashBankDep.TabIndex = 27;
            this.txtCashBankDep.Text = "0";
            this.txtCashBankDep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCashDriveAway
            // 
            this.txtCashDriveAway.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashDriveAway.ForeColor = System.Drawing.Color.Black;
            this.txtCashDriveAway.Location = new System.Drawing.Point(122, 84);
            this.txtCashDriveAway.MaxLength = 12;
            this.txtCashDriveAway.Name = "txtCashDriveAway";
            this.txtCashDriveAway.Size = new System.Drawing.Size(154, 22);
            this.txtCashDriveAway.TabIndex = 28;
            this.txtCashDriveAway.Text = "0";
            this.txtCashDriveAway.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashDriveAway.TextChanged += new System.EventHandler(this.txtCashDriveAway_TextChanged);
            // 
            // CashDifferenceTxt
            // 
            this.CashDifferenceTxt.BackColor = System.Drawing.Color.LightGray;
            this.CashDifferenceTxt.Enabled = false;
            this.CashDifferenceTxt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CashDifferenceTxt.ForeColor = System.Drawing.Color.Black;
            this.CashDifferenceTxt.Location = new System.Drawing.Point(743, 151);
            this.CashDifferenceTxt.Name = "CashDifferenceTxt";
            this.CashDifferenceTxt.ReadOnly = true;
            this.CashDifferenceTxt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CashDifferenceTxt.Size = new System.Drawing.Size(147, 22);
            this.CashDifferenceTxt.TabIndex = 45;
            this.CashDifferenceTxt.Text = "0";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(600, 154);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(65, 16);
            this.label75.TabIndex = 174;
            this.label75.Text = "Difference";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Arial", 9F);
            this.label76.Location = new System.Drawing.Point(9, 16);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(86, 15);
            this.label76.TabIndex = 176;
            this.label76.Text = "Air Miles Cash";
            // 
            // txtAirMilesCash
            // 
            this.txtAirMilesCash.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAirMilesCash.ForeColor = System.Drawing.Color.Black;
            this.txtAirMilesCash.Location = new System.Drawing.Point(122, 12);
            this.txtAirMilesCash.MaxLength = 12;
            this.txtAirMilesCash.Name = "txtAirMilesCash";
            this.txtAirMilesCash.Size = new System.Drawing.Size(154, 22);
            this.txtAirMilesCash.TabIndex = 26;
            this.txtAirMilesCash.Text = "0";
            this.txtAirMilesCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Black;
            this.label77.Location = new System.Drawing.Point(13, 118);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(72, 15);
            this.label77.TabIndex = 179;
            this.label77.Text = "Debit/Credit";
            // 
            // txtDebitCredit
            // 
            this.txtDebitCredit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDebitCredit.ForeColor = System.Drawing.Color.Black;
            this.txtDebitCredit.Location = new System.Drawing.Point(122, 114);
            this.txtDebitCredit.MaxLength = 12;
            this.txtDebitCredit.Name = "txtDebitCredit";
            this.txtDebitCredit.Size = new System.Drawing.Size(154, 22);
            this.txtDebitCredit.TabIndex = 29;
            this.txtDebitCredit.Text = "0";
            this.txtDebitCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotalBankSection
            // 
            this.txtTotalBankSection.BackColor = System.Drawing.Color.LightGray;
            this.txtTotalBankSection.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalBankSection.ForeColor = System.Drawing.Color.Black;
            this.txtTotalBankSection.Location = new System.Drawing.Point(122, 181);
            this.txtTotalBankSection.Name = "txtTotalBankSection";
            this.txtTotalBankSection.ReadOnly = true;
            this.txtTotalBankSection.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTotalBankSection.Size = new System.Drawing.Size(154, 22);
            this.txtTotalBankSection.TabIndex = 31;
            this.txtTotalBankSection.Text = "0";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(13, 185);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(33, 15);
            this.label78.TabIndex = 180;
            this.label78.Text = "Total";
            // 
            // txtBankDiff
            // 
            this.txtBankDiff.BackColor = System.Drawing.Color.LightGray;
            this.txtBankDiff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBankDiff.ForeColor = System.Drawing.Color.Black;
            this.txtBankDiff.Location = new System.Drawing.Point(122, 207);
            this.txtBankDiff.Name = "txtBankDiff";
            this.txtBankDiff.ReadOnly = true;
            this.txtBankDiff.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBankDiff.Size = new System.Drawing.Size(154, 22);
            this.txtBankDiff.TabIndex = 32;
            this.txtBankDiff.Text = "0";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(13, 211);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(25, 15);
            this.label79.TabIndex = 182;
            this.label79.Text = "Diff";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(596, 65);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(111, 16);
            this.label37.TabIndex = 96;
            this.label37.Text = "Total Lotto payout";
            // 
            // txttotallotopayout
            // 
            this.txttotallotopayout.BackColor = System.Drawing.Color.LightGray;
            this.txttotallotopayout.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotallotopayout.ForeColor = System.Drawing.Color.Black;
            this.txttotallotopayout.Location = new System.Drawing.Point(742, 62);
            this.txttotallotopayout.Name = "txttotallotopayout";
            this.txttotallotopayout.ReadOnly = true;
            this.txttotallotopayout.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txttotallotopayout.Size = new System.Drawing.Size(148, 22);
            this.txttotallotopayout.TabIndex = 42;
            this.txttotallotopayout.Text = "0";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.LightGray;
            this.textBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.ForeColor = System.Drawing.Color.Black;
            this.textBox4.Location = new System.Drawing.Point(121, 235);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox4.Size = new System.Drawing.Size(155, 22);
            this.textBox4.TabIndex = 33;
            this.textBox4.Text = "0";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Black;
            this.label80.Location = new System.Drawing.Point(12, 239);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(72, 15);
            this.label80.TabIndex = 184;
            this.label80.Text = "Actual Short";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Black;
            this.label81.Location = new System.Drawing.Point(307, 158);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(103, 30);
            this.label81.TabIndex = 186;
            this.label81.Text = "Cash Paid to Mgt \r\nDeepak/Ricky";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(302, 62);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(110, 15);
            this.label34.TabIndex = 84;
            this.label34.Text = "Cash Lotto Payout ";
            this.label34.Click += new System.EventHandler(this.label34_Click);
            // 
            // txtCashlottto
            // 
            this.txtCashlottto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashlottto.ForeColor = System.Drawing.Color.Black;
            this.txtCashlottto.Location = new System.Drawing.Point(428, 62);
            this.txtCashlottto.MaxLength = 12;
            this.txtCashlottto.Name = "txtCashlottto";
            this.txtCashlottto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashlottto.Size = new System.Drawing.Size(155, 22);
            this.txtCashlottto.TabIndex = 35;
            this.txtCashlottto.Text = "0";
            this.txtCashlottto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashlottto.TextChanged += new System.EventHandler(this.txtCashlottto_TextChanged);
            this.txtCashlottto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashlottto.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(310, 192);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(102, 30);
            this.label33.TabIndex = 187;
            this.label33.Text = "Cash Short/ \r\nAdded to Change";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(312, 225);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 30);
            this.label14.TabIndex = 188;
            this.label14.Text = "Gift Certificate/ \r\nUS dollar";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(10, 144);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(76, 30);
            this.label25.TabIndex = 190;
            this.label25.Text = "Lotto Winner\r\n+ Gift Card ";
            // 
            // txtLotowinner
            // 
            this.txtLotowinner.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLotowinner.ForeColor = System.Drawing.Color.Black;
            this.txtLotowinner.Location = new System.Drawing.Point(122, 144);
            this.txtLotowinner.MaxLength = 12;
            this.txtLotowinner.Name = "txtLotowinner";
            this.txtLotowinner.Size = new System.Drawing.Size(154, 22);
            this.txtLotowinner.TabIndex = 30;
            this.txtLotowinner.Text = "0";
            this.txtLotowinner.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // grpCash
            // 
            this.grpCash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCash.Controls.Add(this.txtLotowinner);
            this.grpCash.Controls.Add(this.label25);
            this.grpCash.Controls.Add(this.label14);
            this.grpCash.Controls.Add(this.label33);
            this.grpCash.Controls.Add(this.txtCashlottto);
            this.grpCash.Controls.Add(this.label34);
            this.grpCash.Controls.Add(this.label81);
            this.grpCash.Controls.Add(this.label80);
            this.grpCash.Controls.Add(this.textBox4);
            this.grpCash.Controls.Add(this.txttotallotopayout);
            this.grpCash.Controls.Add(this.label37);
            this.grpCash.Controls.Add(this.label79);
            this.grpCash.Controls.Add(this.txtBankDiff);
            this.grpCash.Controls.Add(this.label78);
            this.grpCash.Controls.Add(this.txtTotalBankSection);
            this.grpCash.Controls.Add(this.txtDebitCredit);
            this.grpCash.Controls.Add(this.label77);
            this.grpCash.Controls.Add(this.txtAirMilesCash);
            this.grpCash.Controls.Add(this.label76);
            this.grpCash.Controls.Add(this.label75);
            this.grpCash.Controls.Add(this.CashDifferenceTxt);
            this.grpCash.Controls.Add(this.txtCashDriveAway);
            this.grpCash.Controls.Add(this.txtCashBankDep);
            this.grpCash.Controls.Add(this.label27);
            this.grpCash.Controls.Add(this.label28);
            this.grpCash.Controls.Add(this.dgvCash);
            this.grpCash.Controls.Add(this.label64);
            this.grpCash.Controls.Add(this.label26);
            this.grpCash.Controls.Add(this.txtcashpayoutStore);
            this.grpCash.Controls.Add(this.label74);
            this.grpCash.Controls.Add(this.txtCashRemarks);
            this.grpCash.Controls.Add(this.txtCashPaid);
            this.grpCash.Controls.Add(this.label35);
            this.grpCash.Controls.Add(this.txtCashPaidEmp);
            this.grpCash.Controls.Add(this.label32);
            this.grpCash.Controls.Add(this.txtCashpaidmgt);
            this.grpCash.Controls.Add(this.txtCashSort);
            this.grpCash.Controls.Add(this.txtGiftcert);
            this.grpCash.Controls.Add(this.label36);
            this.grpCash.Controls.Add(this.txtCashReceived);
            this.grpCash.Controls.Add(this.label38);
            this.grpCash.Controls.Add(this.txtleftBankdeposit);
            this.grpCash.Controls.Add(this.label39);
            this.grpCash.Controls.Add(this.txtActualBankDeposit);
            this.grpCash.Location = new System.Drawing.Point(7, 235);
            this.grpCash.Name = "grpCash";
            this.grpCash.Size = new System.Drawing.Size(1075, 290);
            this.grpCash.TabIndex = 2;
            this.grpCash.TabStop = false;
            this.grpCash.Text = "Cash";
            this.grpCash.Enter += new System.EventHandler(this.grpCash_Enter);
            // 
            // frmGasTrans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1354, 733);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.StatusComboBox);
            this.Controls.Add(this.EscalateBtn);
            this.Controls.Add(this.ReferBackBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.ViewBtn);
            this.Controls.Add(this.ClearFormBtn);
            this.Controls.Add(this.SubmitBtn);
            this.Controls.Add(this.btnCals);
            this.Controls.Add(this.grpTabbocco);
            this.Controls.Add(this.dtpTransactionDate);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.grpSettlement);
            this.Controls.Add(this.grpSale);
            this.Controls.Add(this.grpCash);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpFuel);
            this.Controls.Add(this.btnExit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimumSize = new System.Drawing.Size(1364, 726);
            this.Name = "frmGasTrans";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sales Data Form";
            this.Load += new System.EventHandler(this.frmGasTrans_Load);
            this.grpFuel.ResumeLayout(false);
            this.grpFuel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuel)).EndInit();
            this.grpSale.ResumeLayout(false);
            this.grpSale.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCstore)).EndInit();
            this.grpSettlement.ResumeLayout(false);
            this.grpSettlement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlement)).EndInit();
            this.grpTabbocco.ResumeLayout(false);
            this.grpTabbocco.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabbacco)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SummaryGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCash)).EndInit();
            this.grpCash.ResumeLayout(false);
            this.grpCash.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCStoreSale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGST;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDepositLevy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFuel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtdailystorerntincgst;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dtpTransactionDate;
        private System.Windows.Forms.GroupBox grpFuel;
        private System.Windows.Forms.TextBox txtAirmiles;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtdieselltr;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtvpowerltr;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtVppltr;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtSilverltr;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtbronzeltr;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSalTotal;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtActualSettlement;
        private System.Windows.Forms.TextBox txtCardRembsment;
        private System.Windows.Forms.TextBox txttotalFuel;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtRadiantdb;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtActualShort;
        private System.Windows.Forms.TextBox txtDiff;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBSDltr;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtVpdvdd;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtfuelcommexgst;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtgstreceivedfuel;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtActualCommission;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtdifindrcr;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtgstpaidonrent;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtfairshare;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txttotalsettlement;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtfinalsettlement;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtdiffinsettlement;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtCalstoreother;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtshelGisftcard;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtgiftcard;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtphonecard;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtscratchlotto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtonlinelotto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txttobbaco;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtRadiantcr;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtShellCredit;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtShellDebit;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.GroupBox grpSale;
        private System.Windows.Forms.GroupBox grpSettlement;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCStoreRemarks;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtTabbRemarks;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtShellRemarks;
        private System.Windows.Forms.GroupBox grpTabbocco;
        private System.Windows.Forms.TextBox txtradiantcrdiff;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtRadiantdbdiff;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtGasRemarks;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Button btnCals;
        private System.Windows.Forms.DataGridView dgvCstore;
        private System.Windows.Forms.DataGridView dgvFuel;
        private System.Windows.Forms.DataGridView dgvSettlement;
        private System.Windows.Forms.DataGridView dgvTabbacco;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CStoreCheckBox;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Tabbaco;
        private System.Windows.Forms.DataGridViewCheckBoxColumn FuelCheckBox;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SettlementCheck;
        private System.Windows.Forms.Button SubmitBtn;
        private System.Windows.Forms.Button ClearFormBtn;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView SummaryGridView;
        private System.Windows.Forms.Button ReferBackBtn;
        private System.Windows.Forms.Button EscalateBtn;
        private System.Windows.Forms.ComboBox StatusComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtActualBankDeposit;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtleftBankdeposit;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtCashReceived;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtGiftcert;
        private System.Windows.Forms.TextBox txtCashSort;
        private System.Windows.Forms.TextBox txtCashpaidmgt;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtCashPaidEmp;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtCashPaid;
        private System.Windows.Forms.TextBox txtCashRemarks;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox txtcashpayoutStore;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.DataGridView dgvCash;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CashCheckBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCashBankDep;
        private System.Windows.Forms.TextBox txtCashDriveAway;
        private System.Windows.Forms.TextBox CashDifferenceTxt;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox txtAirMilesCash;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox txtDebitCredit;
        private System.Windows.Forms.TextBox txtTotalBankSection;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox txtBankDiff;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txttotallotopayout;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtCashlottto;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtLotowinner;
        private System.Windows.Forms.GroupBox grpCash;
        private System.Windows.Forms.TextBox DiffInFuelCommTxt;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox DiffInSettlementTxt;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Button SettlementpopBtn;
    }
}

