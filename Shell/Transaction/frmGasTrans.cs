﻿using BAL;
using DAL;
using Framework.Data;
using Shell.Master;
using Shell.Transaction;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace Shell
{
    public partial class frmGasTrans : Form
    {
        private string PastSettlements = "";
        public frmGasTrans()
        {
            InitializeComponent();
        }

        private int LastTransactionID()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(dtpTransactionDate.Text);
                var lastTransaction = context.MST_FuelTransaction.FirstOrDefault(s => s.TransactionDate == date);
                if (lastTransaction != null)
                {
                    return TransactionID = lastTransaction.GasTransactionID;
                }
                return 0;
            }
        }

        BAL_FuelTransaction _objDoc;
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Saved!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Updated!");
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Record Deleted!");
        }
        private void btnbrowse_Click_1(object sender, EventArgs e)
        {

            //Ask user to select file.
            OpenFileDialog dlg = new OpenFileDialog();
            //dlg.Filter = "JPEG Files(*.jpeg)| Bmp Files(*.bmp)";
            DialogResult dlgRes = dlg.ShowDialog();

            if (dlgRes != DialogResult.Cancel)
            {
                //Set image in picture box


                //Provide file path in txtImagePath text box.

            }

        }

        private List<DocumentList> DocumentTypes(string docType)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(dtpTransactionDate.Text).ToString("MMddyyyy");
                var docList = context.Mst_DocUpload.Where(s => s.DocsType == docType).Select(s => new DocumentList { DocId = s.UploadId, DocsIdentity = s.DocsIdentity, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();
                return docList;
            }
        }
        public void binddropdown()
        {
            dgvCstore.DataSource = null;
            dgvCash.DataSource = null;
            dgvTabbacco.DataSource = null;
            dgvSettlement.DataSource = null;
            dgvFuel.DataSource = null;
            _objDoc = new BAL.BAL_FuelTransaction();

            var docList = DocumentTypes("Sale");
            if (docList.Count > 0)
            {
                dgvCstore.DataSource = docList;
            }
            var docListTabbocco = DocumentTypes("Tobacco");
            if (docListTabbocco.Count > 0)
            {
                dgvTabbacco.DataSource = docListTabbocco;
            }
            var docListFuel = DocumentTypes("Fuel");
            if (docListFuel.Count > 0)
            {
                dgvFuel.DataSource = docListFuel;
            }
            var docListCash = DocumentTypes("Cash");
            if (docListCash.Count > 0)
            {
                dgvCash.DataSource = docListCash;
            }
            var docListSettlement = DocumentTypes("Settlement");
            if (docListSettlement.Count > 0)
            {
                dgvSettlement.DataSource = docListSettlement;
            }
        }

        private void AddMode()
        {
            binddropdown();
            GridAddColumn();
            if (Utility.BA_Commman._Profile == "FE")
            {
                grpFuel.Enabled = false;
                dgvFuel.Enabled = false;
                grpSettlement.Enabled = false;
                dgvSettlement.Enabled = false;
                grpCash.Enabled = true;
                grpSale.Enabled = true;
                grpTabbocco.Enabled = false;
                dgvTabbacco.Enabled = false;
                //btnAdd.Visible = true;
                //SubmitBtn.Visible = false;
            }
            else if (Utility.BA_Commman._Profile == "BE")
            {
                grpFuel.Enabled = true;
                dgvFuel.Enabled = true;
                grpSettlement.Enabled = true;
                dgvSettlement.Enabled = true;
                grpCash.Enabled = true;
                grpSale.Enabled = true;
                grpTabbocco.Enabled = true;
                dgvTabbacco.Enabled = true;
                // btnAdd.Visible = true;
                //SubmitBtn.Visible = false;
            }
            else if (Utility.BA_Commman._Profile == "Auditor")
            {
                //grpFuel.Enabled = false;
                //dgvFuel.Enabled = false;
                //grpSettlement.Enabled = false;
                //dgvSettlement.Enabled = false;
                //grpCash.Enabled = false;
                //grpSale.Enabled = false;
                //grpTabbocco.Enabled = false;
                //dgvTabbacco.Enabled = false;
            }
            else if (Utility.BA_Commman._Profile == "SupAdm" || Utility.BA_Commman._Profile == "Adm")
            {
                grpFuel.Enabled = true;
                grpSettlement.Enabled = true;
                grpCash.Enabled = true;
                grpSale.Enabled = true;
                grpTabbocco.Enabled = true;
                //btnAdd.Visible = false;
                //SubmitBtn.Visible = true;
            }
            else
            {
                grpFuel.Enabled = true;
                grpSettlement.Enabled = true;
                grpCash.Enabled = true;
                grpSale.Enabled = true;
            }
        }
        private void TicketMode()
        {
            if (ShellComman.Ticket_Request_Type == "Update Request")
            {
                btnAdd.Text = "Update";
                dtpTransactionDate.Text = ShellComman.Shell_Gas_TransactionDate.ToString("MMM/dd/yyyy");
            }
            else if (ShellComman.Ticket_Request_Type == "Delete Request")
            {
                btnAdd.Text = "Delete";
                dtpTransactionDate.Text = ShellComman.Shell_Gas_TransactionDate.ToString("MMM/dd/yyyy");
            }
        }

        private string EODStatus()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    int id = LastTransactionID();
                    TBL_EODRemarks summary = context.TBL_EODRemarks.OrderByDescending(s => s.EODID).FirstOrDefault(s => s.FuelTransactionID == id);
                    if (summary != null)
                    {
                        return summary.ApprovalStatus;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "";
            }
        }
        private void frmGasTrans_Load(object sender, EventArgs e)
        {
            try
            {
                ButtonsAccesibility();
                if (!string.IsNullOrEmpty(ShellComman.Shell_TicketCode))
                {
                    FuelTransactionId = LastTransactionID();
                    TicketMode();
                    binddropdown();
                    LoadGridView();
                    BindRemarksSummary();
                }
                else
                {
                    AddMode();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void GridAddColumn()
        {
            if (dgvCstore.DataSource != null)
            {
                dgvCstore.Columns[1].Visible = false;
                dgvCstore.Columns[3].Visible = false;
            }
            //
            if (dgvTabbacco.DataSource != null)
            {

                dgvTabbacco.Columns[1].Visible = false;
                dgvTabbacco.Columns[3].Visible = false;
            }
            // 
            if (dgvCash.DataSource != null)
            {
                dgvCash.Columns[1].Visible = false;
                dgvCash.Columns[3].Visible = false;

            }
            //
            if (dgvFuel.DataSource != null)
            {
                dgvFuel.Columns[1].Visible = false;
                dgvFuel.Columns[3].Visible = false;
            }
            //
            if (dgvSettlement.DataSource != null)
            {
                dgvSettlement.Columns[1].Visible = false;
                dgvSettlement.Columns[3].Visible = false;
            }
        }
        string str = "0123456789.";
        private void KeyPress_t(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 45)
            {

                if (str.IndexOf(e.KeyChar.ToString()) < 0)
                {
                    if (e.KeyChar.ToString() != "\b")
                    {
                        e.Handled = true;
                    }
                }
            }


        }

        double CstoreSale = 0.0, Discounts = 0.0, Fuel = 0.0, Gst = 0.0, Levy = 0.0, TotalSale = 0.0;

        private void AddSale()
        {
            CstoreSale = float.Parse((txtCStoreSale.Text), CultureInfo.InvariantCulture.NumberFormat);
            Discounts = Convert.ToDouble(txtDiscount.Text);
            Fuel = Convert.ToDouble(txtFuel.Text);
            Gst = Convert.ToDouble(txtGST.Text);
            Levy = Convert.ToDouble(txtDepositLevy.Text);
            TotalSale = (CstoreSale + Fuel + Gst + Levy) - Discounts;
            txtSalTotal.Text = TotalSale.ToString("#.##");
        }

        public void CreateNewEditEODRecord()
        {
            try
            {
                #region User Validations
                if (Utility.BA_Commman._Profile == "FE")
                {
                    if (_cklCstoredoc == "" && string.IsNullOrEmpty(txtCStoreRemarks.Text))
                    {
                        MessageBox.Show("Please Select the C Store Doc Or Enter Remarks");
                        return;

                    }
                    else if (_cklcash == "" && string.IsNullOrEmpty(txtCashRemarks.Text))
                    {
                        MessageBox.Show("Please Select the Cash Doc Or Enter Remarks");
                        return;
                    }

                }
                else if (Utility.BA_Commman._Profile == "BE")
                {
                    if (_cklCstoredoc == "" && string.IsNullOrEmpty(txtCStoreRemarks.Text))
                    {
                        MessageBox.Show("Please Select the C Store Doc Or Enter Remarks");
                        return;

                    }
                    else if (_cklcash == "" && string.IsNullOrEmpty(txtCashRemarks.Text))
                    {
                        MessageBox.Show("Please Select the Cash Doc Or Enter Remarks");
                        return;
                    }
                    else if (_ckltabbacco == "" && string.IsNullOrEmpty(txtTabbRemarks.Text))
                    {
                        MessageBox.Show("Please Select the Tobacco Doc Or Enter Remarks");
                        return;
                    }
                    else if (_cklfuel == "" && string.IsNullOrEmpty(txtGasRemarks.Text))
                    {
                        MessageBox.Show("Please Select the Fuel Doc Or Enter Remarks");
                        return;
                    }
                    else if (_cklsettlement == "" && string.IsNullOrEmpty(txtShellRemarks.Text))
                    {
                        MessageBox.Show("Please Select the Settlement Doc Or Enter Remarks");
                        return;

                    }
                    else if (LastTransactionID() <= 0)
                    {
                        MessageBox.Show("Front End User needs to Submit EOD record first.");
                        return;
                    }
                    else if (!string.IsNullOrEmpty(PastSettlements) && (string.IsNullOrEmpty(txtActualSettlement.Text) || Convert.ToInt32(txtActualSettlement.Text) <= 0))
                    {
                        MessageBox.Show("Once you selected past settlement, you must have to enter actual settlement.");
                        return;
                    }
                }
                #endregion
                FuelTransactionEntities entities = new FuelTransactionEntities();
                entities.GasTransactionID = 0;
                // C-Store SaleDettails
                #region C-Store SaleDetails
                entities.TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
                entities.C_StoreSale = float.Parse((txtCStoreSale.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.C_AttachmentID = _cklCstoredoc;
                entities.C_Store_Remakrs = txtCStoreRemarks.Text;
                entities.EnabledActualBankDeposit = float.Parse((txtActualBankDeposit.Text), CultureInfo.InvariantCulture.NumberFormat);
                #endregion
                #region Cash Part handeled by FE
                entities.CashPayoutForStorePurchase = float.Parse(Convert.ToString(txtcashpayoutStore.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashLottoPayout = float.Parse((txtCashlottto.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashPaidForStorePur = float.Parse(Convert.ToString(txtCashPaid.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashPaidToThirdParties = float.Parse(Convert.ToString(txtCashPaidEmp.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashPaidToMGT = float.Parse((txtCashpaidmgt.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashShort = float.Parse(Convert.ToString(txtCashSort.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.GiftCertificate = float.Parse(Convert.ToString(txtGiftcert.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashReceived = float.Parse((txtCashReceived.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashRemarks = Convert.ToString(txtCashRemarks.Text);
                entities.CashAttachmentID = _cklcash;
                entities.CashDifference = float.Parse((CashDifferenceTxt.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.DebitCredit = float.Parse((txtDebitCredit.Text), CultureInfo.InvariantCulture.NumberFormat);
                #endregion
                #region Fuel Transaction Details
                entities.Bronze = float.Parse(Convert.ToString(txtbronzeltr.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.Diesel = float.Parse(Convert.ToString(txtdieselltr.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.Silver = float.Parse(Convert.ToString(txtSilverltr.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.VPowerPower = float.Parse(Convert.ToString(txtVppltr.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.VPowerDiesel = float.Parse(Convert.ToString(txtvpowerltr.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.AirMilesCash = float.Parse(Convert.ToString(txtAirmiles.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.LottoWinnerGiftCard = float.Parse(Convert.ToString(txtLotowinner.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashForBankDeposit = float.Parse(Convert.ToString(txtCashBankDep.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CashDriveAway = float.Parse(Convert.ToString(txtCashDriveAway.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.FuelAttachmentID = _cklfuel;
                entities.FuelRemarks = Convert.ToString(txtGasRemarks.Text);

                #endregion
                #region Settlement
                entities.RadiantDebit = float.Parse(Convert.ToString(txtRadiantdb.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.RadiantCredit = float.Parse(Convert.ToString(txtRadiantcr.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.ShellDebit = float.Parse(Convert.ToString(txtShellDebit.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.ShellCredit = float.Parse(Convert.ToString(txtShellCredit.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.DailyStoreRentincludingGs = float.Parse(Convert.ToString(txtdailystorerntincgst.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.ActualCommission = float.Parse(Convert.ToString(txtActualCommission.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.CardsReimbursements = float.Parse(Convert.ToString(txtCardRembsment.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.ActualSettlement = float.Parse(Convert.ToString(txtActualSettlement.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.FairShare = float.Parse(Convert.ToString(txtfairshare.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.AttachmentIDShell = _cklsettlement;
                entities.ShellRemarks = Convert.ToString(txtShellRemarks.Text);
                #endregion
                #region Tabbaco
                entities.Tabbacco = float.Parse(Convert.ToString(txttobbaco.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.OnlineLotto = float.Parse(Convert.ToString(txtonlinelotto.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.ScratchLotto = float.Parse(Convert.ToString(txtscratchlotto.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.PhoneCard = float.Parse(Convert.ToString(txtphonecard.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.GiftCard = float.Parse(Convert.ToString(txtgiftcard.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.ShellGifTcar = float.Parse(Convert.ToString(txtshelGisftcard.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.Discount = float.Parse(Convert.ToString(txtDiscount.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.Fuel = float.Parse(Convert.ToString(txtFuel.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.GST = float.Parse(Convert.ToString(txtGST.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.DepositLevy = float.Parse(Convert.ToString(txtDepositLevy.Text), CultureInfo.InvariantCulture.NumberFormat);
                entities.TabbbaccoAttachemntid = _ckltabbacco;
                entities.TabboccoRemarks = Convert.ToString(txtTabbRemarks.Text);
                #endregion
                #region Document Save
                try
                {
                    SaveMultipleDocs("Sale", dgvCstore);
                    SaveMultipleDocs("Tabbocco", dgvTabbacco);
                    SaveMultipleDocs("Cash", dgvCash);
                    SaveMultipleDocs("Fuel", dgvFuel);
                    SaveMultipleDocs("Settlement", dgvSettlement);
                    int i = _objDoc.AddEditFuelTransaction(entities);
                    if (i == 1)
                    {
                        UpdatePastSettlement();
                        string Status = EODStatus();
                        if (Status == string.Empty || Status == ERPEODApprovalStatus.Submitted || Status == ERPEODApprovalStatus.Completed)
                        {
                            MessageBox.Show("Sales Data Form Record Created/Updated Sucessfully.");
                        }
                        else if (Status == ERPEODApprovalStatus.ReferBack)
                        {
                            MessageBox.Show("Sales Data Form Has Been Referred Back.");
                        }
                        else if (Status == ERPEODApprovalStatus.Closed)
                        {
                            MessageBox.Show("Sales Data Form Has Been Closed.");
                        }
                        SubmitBtn.Visible = true;
                        btnAdd.Text = "Update";
                    }
                    else
                    {
                        MessageBox.Show("There is some issue . Please try later!");
                    }
                    // grpFuel.Visible = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void DeleteRecord()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var transaction = context.MST_FuelTransaction.Select(s => new { s.TransactionDate, s.GasTransactionID }).ToList();
                var dataId = transaction.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == ShellComman.Shell_Gas_TransactionDate.ToString("MMddyyyy")).Select(s => s.GasTransactionID).FirstOrDefault();

                if (dataId > 0)
                {
                    var deleteTran = context.MST_FuelTransaction.FirstOrDefault(s => s.GasTransactionID == dataId);
                    context.MST_FuelTransaction.Remove(deleteTran);
                    context.SaveChanges();
                    MessageBox.Show("Transaction Deleted successfully.");
                    var ticketRef = context.TBL_TicketCreatedMaster.FirstOrDefault(s => s.TicketCode == ShellComman.Shell_TicketCode);
                    ticketRef.IsActive = false;
                    ticketRef.IsClosed = false;
                    ticketRef.ModifiedDate = DateTime.Now;
                    ticketRef.ModiefiedBy = 1;
                    context.SaveChanges();
                }

            }
        }
        double Tobbaco = 0.0, Online_Lottoo = 0.0, Scratch_Lotto = 0.0, Phone_Cards = 0.0, Gift_Cards = 0.0, Shell_Gift_Cards = 0.0, Store_Others = 0.0;

        private void btnbrowse_Click(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }


        private string _cklCstoredoc, _ckltabbacco, _cklcash, _cklfuel, _cklsettlement;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnAdd.Text == "Save" && EODStatus() == string.Empty)
                {
                    CreateNewEditEODRecord();

                }
                else if (btnAdd.Text == "Close" && EODStatus() != string.Empty)
                {
                    CreateNewEditEODRecord();
                }
                else if (Utility.BA_Commman._Profile == "FE" && EODStatus() == string.Empty)
                {
                    CreateNewEditEODRecord();
                }
                else if (Utility.BA_Commman._Profile == "BE" && EODStatus() == ERPEODApprovalStatus.Submitted)
                {
                    CreateNewEditEODRecord();
                }
                else if (btnAdd.Text == "Update" && (EODStatus() == ERPEODApprovalStatus.ReferBack || EODStatus() == ERPEODApprovalStatus.Reopen))
                {
                    PasswordConfirmationForm childForm = new PasswordConfirmationForm();
                    ChildColorConfig(childForm);
                    childForm.FormRef = this;
                    childForm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private List<string> docIds = new List<string>();
        private void SaveMultipleDocs(string docType, DataGridView gridView)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    string tranDate = Convert.ToDateTime(dtpTransactionDate.Text).ToString("MMddyyyy");
                    foreach (DataGridViewRow row in gridView.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            int docId = Convert.ToInt32((row.Cells[1].Value));
                            docIds.Add(row.Cells[1].Value.ToString());
                            var docDelete = context.Tbl_FuelTransactionDocuments.Where(s => s.DocumentType == docType).Select(s => new { s }).ToList();
                            foreach (var date in docDelete)
                            {
                                if (Convert.ToDateTime(date.s.TransactionDate).ToString("MMddyyyy") == tranDate)
                                {
                                    context.Tbl_FuelTransactionDocuments.Remove(date.s);
                                }
                            }
                            context.SaveChanges();
                            var docExist = context.Tbl_FuelTransactionDocuments.FirstOrDefault(s => s.UploadedDocID == docId && s.DocumentType == docType);
                            if (docExist == null)
                            {
                                Tbl_FuelTransactionDocuments docsObj = new Tbl_FuelTransactionDocuments();
                                docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                                docsObj.DocumentType = docType;
                                docsObj.TransactionType = docType;
                                docsObj.TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
                                docsObj.IsActive = true;
                                docsObj.CreatedBy = Utility.BA_Commman._userId;
                                docsObj.CreatedDate = DateTime.Now;
                                context.Tbl_FuelTransactionDocuments.Add(docsObj);
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtCStoreSale_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            AddSale();
        }
        private void Key_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            AddCtoreSale();
        }
        private void FuelKey_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            AddFuel();
        }

        private void txtAirmiles_Leave(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            AddAirmiles();
        }

        private void txtRadiantdb_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            AddSettlement();
        }

        private void txtcashpayoutStore_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            AddCash();
        }

        private void txtdailystorerntincgst_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            DailyStoreGST();
        }

        private void DailyStoreGST()
        {
            double dailystorerntincgst = 0.0, gstpaidonrent = 0.0, Fuel = 0.0, difindrcr = 0.0;

            dailystorerntincgst = Convert.ToDouble(txtdailystorerntincgst.Text);
            gstpaidonrent = dailystorerntincgst - (dailystorerntincgst / 1.05);
            txtgstpaidonrent.Text = gstpaidonrent.ToString("0.00");
            Fuel = Convert.ToDouble(txtFuel.Text);
            //txttotalsettlement.Text = (dailystorerntincgst + Fuel).ToString("0.00");
            if (string.IsNullOrEmpty(txtdifindrcr.Text))
            {
                txtdifindrcr.Text = "0";

            }
            difindrcr = Convert.ToDouble(txtdifindrcr.Text);
            // txtfinalsettlement.Text = (dailystorerntincgst + Fuel + difindrcr).ToString("0.00");
        }

        private void FileOpen(ComboBox ddl)
        {
            _objDoc = new BAL.BAL_FuelTransaction();
            string filepath = _objDoc.GetFilePath(ddl.SelectedValue.ToString());
            if (!String.IsNullOrEmpty(filepath))
            //  File.Open(filepath, FileMode.Open);
            {
                PictureBox PictureBox1 = new PictureBox();
                PictureBox1.Image = new Bitmap(filepath);

                // Add the new control to its parent's controls collection
                this.Controls.Add(PictureBox1);
            }
            else
                MessageBox.Show("There is some issue in file.");
        }

        private void dtpTransactionDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                FuelTransactionId = LastTransactionID();
                // cleartextvalue();
                TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
                binddropdown();
                GridAddColumn();
                LoadGridView();
                BindRemarksSummary();
                calculate();
                ButtonsAccesibility();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnTicket_Click(object sender, EventArgs e)
        {
            if (Utility.BA_Commman._Profile == "FE")
            {

                _objDoc.ManageTicket(2, dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
                MessageBox.Show("Ticket Moved to BE User");
            }
            if (Utility.BA_Commman._Profile == "BE")
            {

                _objDoc.ManageTicket(3, dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
                MessageBox.Show("Ticket Moved to Auditor User");
            }

        }

        private void btnTicketBack_Click(object sender, EventArgs e)
        {
            if (Utility.BA_Commman._Profile == "BE")
            {

                _objDoc.ManageTicket(1, dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
                MessageBox.Show("Ticket backed to FE User");
            }
            if (Utility.BA_Commman._Profile != "BE" && Utility.BA_Commman._Profile != "FE")
            {

                _objDoc.ManageTicket(2, dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
                MessageBox.Show("Ticket backed to BE User");
            }

        }
        private void checkgrid(DataGridView dg, string value)
        {
        }
        private void clearcheckgrid(DataGridView dg)
        {
            foreach (DataGridViewRow r in dg.Rows)
            {
                r.Cells[0].Value = false;
            }
        }
        private void cleartextvalue()
        {
            txtdifindrcr.Text = "0";
            #region C-Store Sale Details
            txtCStoreSale.Text = "0";
            // ddlCstoreDocs.SelectedValue = lst[0].C_AttachmentID;
            txtCStoreRemarks.Text = "";
            #endregion
            #region TabbbacoDetial
            txttobbaco.Text = "0";
            txtonlinelotto.Text = "0";
            txtscratchlotto.Text = "0";
            txtphonecard.Text = "0";
            txtgiftcard.Text = "0";
            txtshelGisftcard.Text = "0";
            txtDiscount.Text = "0";
            txtFuel.Text = "0";
            txtGST.Text = "0";
            txtDepositLevy.Text = "0";
            txtTabbRemarks.Text = "";
            #endregion
            #region Fuel Details
            txtbronzeltr.Text = "0";
            txtdieselltr.Text = "0";
            txtSilverltr.Text = "0";
            txtvpowerltr.Text = "0";
            txtVppltr.Text = "0";
            txtAirmiles.Text = "0";
            txtLotowinner.Text = "0";
            txtCashBankDep.Text = "0";
            txtCashDriveAway.Text = "0";
            txtGasRemarks.Text = "";
            #endregion
            #region Cash
            txtcashpayoutStore.Text = "0";
            txtCashlottto.Text = "0";
            txtCashPaid.Text = "0";
            txtCashPaidEmp.Text = "0";
            txtCashpaidmgt.Text = "0";
            txtCashSort.Text = "0";
            txtGiftcert.Text = "0";
            txtCashReceived.Text = "0";
            txtCashRemarks.Text = "";
            #endregion
            #region Settlement
            txtRadiantcr.Text = "0";
            txtRadiantdb.Text = "0";
            txtShellCredit.Text = "0";
            txtShellDebit.Text = "0";
            txtdailystorerntincgst.Text = "0";
            txtActualCommission.Text = "0";
            txtCardRembsment.Text = "0";
            txtActualSettlement.Text = "0";
            txtfairshare.Text = "0";
            txtShellRemarks.Text = "";
            #endregion

            #region Clear Disabled textBox
            //txtcalstoreother.text = string.empty;
            //txtsaltotal.text = string.empty;
            //txttotallotopayout.text = string.empty;
            //txtleftbankdeposit.text = string.empty;
            //txtactualbankdeposit.text = string.empty;
            //txtbsdltr.text = string.empty;
            //txtvpdvdd.text = string.empty;
            //txtfuelcommexgst.text = string.empty;
            //txtgstreceivedfuel.text = string.empty;
            //txttotalfuel.text = string.empty;
            //txtdifindrcr.text = string.empty;
            //txtgstpaidonrent.text = string.empty;
            //txttotalsettlement.text = string.empty;
            //txtfinalsettlement.text = string.empty;
            //txtdiff.text = string.empty;
            //txtactualshort.text = string.empty;
            //txtradiantdbdiff.text = string.empty;
            //txtradiantcrdiff.text = string.empty;
            #endregion
            //clearcheckgrid(dgvCash);
            //clearcheckgrid(dgvCstore);
            //clearcheckgrid(dgvFuel);
            //clearcheckgrid(dgvSettlement);
            //clearcheckgrid(dgvTabbacco);
            //dgvCash.DataSource = null;
            //dgvCstore.DataSource = null;
            //dgvFuel.DataSource = null;
            //dgvSettlement.DataSource = null;
            //dgvTabbacco.DataSource = null;
        }
        private int FuelTransactionId = 0;
        private void LoadGridView()
        {
            BAL_FuelTransaction fuelTransaction = new BAL_FuelTransaction();

            List<FuelTransactionEntities> lst = fuelTransaction.ListFuelTransactions(dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
            int count = lst.Count;
            if (count > 0)
            {
                TransactionExistForTheDate = true;
                #region C-Store Sale Details
                FuelTransactionId = lst[0].GasTransactionID;
                txtCStoreSale.Text = lst[0].C_StoreSale.ToString();
                _cklCstoredoc = lst[0].C_AttachmentID;
                txtCStoreRemarks.Text = lst[0].C_Store_Remakrs;
                #endregion
                #region TabbbacoDetial
                txttobbaco.Text = lst[0].Tabbacco.ToString();
                txtonlinelotto.Text = lst[0].OnlineLotto.ToString();
                txtscratchlotto.Text = lst[0].ScratchLotto.ToString();
                txtphonecard.Text = lst[0].PhoneCard.ToString();
                txtgiftcard.Text = lst[0].GiftCard.ToString();
                txtshelGisftcard.Text = lst[0].ShellGifTcar.ToString();
                txtDiscount.Text = lst[0].Discount.ToString();
                txtFuel.Text = lst[0].Fuel.ToString();
                txtGST.Text = lst[0].GST.ToString();
                txtDepositLevy.Text = lst[0].DepositLevy.ToString();
                txtTabbRemarks.Text = lst[0].TabboccoRemarks;
                _ckltabbacco = lst[0].TabbbaccoAttachemntid;

                #endregion
                #region Fuel Details
                txtbronzeltr.Text = lst[0].Bronze.ToString();
                txtdieselltr.Text = lst[0].Diesel.ToString();
                txtSilverltr.Text = lst[0].Silver.ToString();
                txtvpowerltr.Text = lst[0].VPowerDiesel.ToString();
                txtVppltr.Text = lst[0].VPowerPower.ToString();
                txtAirmiles.Text = lst[0].AirMilesCash.ToString();
                txtLotowinner.Text = lst[0].LottoWinnerGiftCard.ToString();
                txtCashBankDep.Text = lst[0].CashForBankDeposit.ToString();
                txtCashDriveAway.Text = lst[0].CashDriveAway.ToString();
                _cklfuel = lst[0].FuelAttachmentID;
                txtGasRemarks.Text = lst[0].FuelRemarks;
                #endregion
                #region Cash
                txtcashpayoutStore.Text = lst[0].CashPayoutForStorePurchase.ToString();
                txtCashlottto.Text = lst[0].CashLottoPayout.ToString();
                txtCashPaid.Text = lst[0].CashPaidForStorePur.ToString();
                txtCashPaidEmp.Text = lst[0].CashPaidToThirdParties.ToString();
                txtCashpaidmgt.Text = lst[0].CashPaidToMGT.ToString();
                txtCashSort.Text = lst[0].CashShort.ToString();
                txtGiftcert.Text = lst[0].GiftCertificate.ToString();
                txtCashReceived.Text = lst[0].CashReceived.ToString();
                txtCashRemarks.Text = lst[0].CashRemarks;
                txtActualBankDeposit.Text = Convert.ToString(lst[0].EnabledActualBankDeposit);
                _cklcash = lst[0].CashAttachmentID;
                CashDifferenceTxt.Text = lst[0].CashDifference.ToString();
                txtDebitCredit.Text = lst[0].DebitCredit.ToString();
                #endregion
                #region Settlement
                txtRadiantcr.Text = lst[0].RadiantCredit.ToString();
                txtRadiantdb.Text = lst[0].RadiantDebit.ToString();
                txtShellCredit.Text = lst[0].ShellCredit.ToString();
                txtShellDebit.Text = lst[0].ShellDebit.ToString();
                txtdailystorerntincgst.Text = lst[0].DailyStoreRentincludingGs.ToString();
                txtActualCommission.Text = lst[0].ActualCommission.ToString();
                txtCardRembsment.Text = lst[0].CardsReimbursements.ToString();
                txtActualSettlement.Text = lst[0].ActualSettlement.ToString();
                txtfairshare.Text = lst[0].FairShare.ToString();
                _cklsettlement = lst[0].AttachmentIDShell;
                txtShellRemarks.Text = lst[0].ShellRemarks;
                #endregion
                #region Selected Documents
                SavedDocuments("Sale", dgvCstore);
                SavedDocuments("Tabbocco", dgvTabbacco);
                SavedDocuments("Cash", dgvCash);
                SavedDocuments("Fuel", dgvFuel);
                SavedDocuments("Settlement", dgvSettlement);
                #endregion
                btnAdd.Text = "Update";
            }
            else
            {
                btnAdd.Text = "Save";
                cleartextvalue();
                MessageBox.Show("EOD is not created for this date.");
            }


        }
        private void BindRemarksSummary()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var remakrs = (from e in context.TBL_EODRemarks
                               where e.IsActive == true && e.FuelTransactionID == FuelTransactionId
                               select new
                               {
                                   e.EODID,
                                   e.Remarks
                               }).ToList();

                SummaryGridView.DataSource = remakrs;
                SummaryGridView.Columns[0].Visible = false;
            }
        }
        //private bool IsEODRecordSubmitted()
        //{
        //    using (ShellEntities context = new ShellEntities())
        //    {
        //        DateTime date = Convert.ToDateTime(dtpTransactionDate.Text);
        //        var eodData = context.MST_FuelTransaction.FirstOrDefault(s => s.TransactionDate == date);
        //        if (eodData != null)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            return false;
        //        }
        //    }
        //}
        private void ChildColorConfig(Form childForm)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var childConfig = context.TBL_ChilFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                    if (childConfig != null)
                    {
                        childForm.BackColor = Color.FromName(childConfig.BackgroundColor);
                        float fl = (float)childConfig.FontSize;
                        FontStyle fontStyle = (FontStyle)Enum.Parse(typeof(FontStyle), childConfig.FontStyle, true);
                        Font childFont = new Font(childConfig.FontName, fl, fontStyle);
                        childForm.ForeColor = Color.FromName(childConfig.FontColor);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator." + ex.Message);
            }
        }

        public DateTime TransactionDate { get; set; }
        public int TransactionID { get; set; }
        private bool TransactionExistForTheDate = false;
        private void ClearFormBtn_Click(object sender, EventArgs e)
        {
            cleartextvalue();
            btnAdd.Text = "Save";
            SubmitBtn.Visible = false;
        }
        private void ButtonsAccesibility()
        {
            if (EODStatus() == string.Empty)
            {
                if (TransactionExistForTheDate)
                {
                    btnAdd.Visible = true;
                    SubmitBtn.Visible = true;
                    ClearFormBtn.Visible = true;
                    ReferBackBtn.Visible = false;
                }
                else
                {
                    btnAdd.Visible = true;
                    SubmitBtn.Visible = false;
                    ClearFormBtn.Visible = true;
                    ReferBackBtn.Visible = false;
                }

            }
            else if (EODStatus() == ERPEODApprovalStatus.Submitted && (Utility.BA_Commman._Profile == "FE"))
            {
                btnAdd.Visible = false;
                SubmitBtn.Visible = false;
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = false;
            }
            else if (EODStatus() == ERPEODApprovalStatus.Submitted && (Utility.BA_Commman._Profile == "BE"))
            {
                btnAdd.Visible = true;
                btnAdd.Text = "Update";
                SubmitBtn.Visible = true;
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = true;
            }
            else if (EODStatus() == ERPEODApprovalStatus.Completed && (Utility.BA_Commman._Profile == "BE"))
            {
                btnAdd.Visible = false;
                SubmitBtn.Visible = false;
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = false;
            }
            else if ((Utility.BA_Commman._Profile == "Auditor") && (EODStatus() == ERPEODApprovalStatus.Completed))
            {
                btnAdd.Visible = false;
                SubmitBtn.Visible = true;
                SubmitBtn.Text = "Close";
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = true;
                EscalateBtn.Visible = true;
            }
            else if ((Utility.BA_Commman._Profile == "FE") && ReferBackProfile() == "BE" && (EODStatus() == ERPEODApprovalStatus.ReferBack || EODStatus() == ERPEODApprovalStatus.Reopen))
            {
                btnAdd.Visible = false;
                SubmitBtn.Visible = true;
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = false;
            }
            else if ((Utility.BA_Commman._Profile == "BE") && ReferBackProfile() == "Auditor" && (EODStatus() == ERPEODApprovalStatus.ReferBack || EODStatus() == ERPEODApprovalStatus.Reopen))
            {
                btnAdd.Visible = false;
                SubmitBtn.Visible = true;
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = false;
            }
            else if ((Utility.BA_Commman._Profile == "Auditor" || Utility.BA_Commman._Profile == "BE" || Utility.BA_Commman._Profile == "FE") && EODStatus() == ERPEODApprovalStatus.Closed)
            {
                btnAdd.Visible = false;
                SubmitBtn.Visible = false;
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = false;
                //  EscalateBtn.Visible = true;
            }

            else if ((Utility.BA_Commman._Profile == "SupAdm" || Utility.BA_Commman._Profile == "Adm") && (EODStatus() == ERPEODApprovalStatus.Escalated))
            {
                btnAdd.Visible = false;
                SubmitBtn.Visible = true;
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = true;
                EscalateBtn.Visible = false;
                StatusComboBox.Visible = true;
                IsAdminLoggedIn = true;
            }
            else if ((Utility.BA_Commman._Profile == "Auditor") && (EODStatus() == ERPEODApprovalStatus.ReferBack))
            {
                btnAdd.Visible = false;
                SubmitBtn.Visible = false;
                ClearFormBtn.Visible = true;
                ReferBackBtn.Visible = false;
                EscalateBtn.Visible = false;
                StatusComboBox.Visible = false;
                IsAdminLoggedIn = false;
            }
        }

        private bool IsAdminLoggedIn = false;
        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                FuelTransactionId = LastTransactionID();
                TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
                binddropdown();
                GridAddColumn();
                LoadGridView();
                BindRemarksSummary();
                calculate();
                ButtonsAccesibility();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SummaryBtn_Click(object sender, EventArgs e)
        {
            try
            {
                EODSummaryForm summaryForm = new EODSummaryForm();
                summaryForm.TransactionId = LastTransactionID();
                summaryForm.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string ReferBackProfile()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var eodData = (from e in context.MST_FuelTransaction
                               join p in context.MST_ProfileMaster on e.SubmittedRoleId equals p.ProfileId
                               where e.TransactionDate == TransactionDate

                               select p.ProfileCode).FirstOrDefault();
                if (eodData != null)
                {
                    return eodData;
                }
                else
                {
                    return "";
                }
            }
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    int EODId = Convert.ToInt32(SummaryGridView.Rows[e.RowIndex].Cells["EODID"].Value);
                    SummaryPopupForm form = new SummaryPopupForm();
                    form.EODId = EODId;
                    form.Location = new Point(MousePosition.X, MousePosition.Y);
                    form.ShowDialog(this);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void ReferBackToFE()
        {
            try
            {
                if (IsAdminLoggedIn && (EODStatus() != string.Empty))
                {
                    if (string.IsNullOrEmpty(StatusComboBox.Text))
                    {
                        MessageBox.Show("Please select refer back status..!");
                        return;
                    }
                    RemarksPopUpForm remarksform = new RemarksPopUpForm();
                    ChildColorConfig(remarksform);
                    remarksform.FormRef = this;
                    remarksform.AdminReferBackStatus = StatusComboBox.Text;
                    TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
                    remarksform.IsReferBack = true;
                    remarksform.Show();
                }
                else
                {
                    RemarksPopUpForm remarksform = new RemarksPopUpForm();
                    ChildColorConfig(remarksform);
                    remarksform.FormRef = this;
                    TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
                    remarksform.IsReferBack = true;
                    remarksform.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void EscalateToManagement()
        {
            try
            {
                RemarksPopUpForm remarksform = new RemarksPopUpForm();
                ChildColorConfig(remarksform);
                remarksform.FormRef = this;
                TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
                remarksform.IsEscalated = true;
                remarksform.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void ReferBackBtn_Click(object sender, EventArgs e)
        {
            ReferBackToFE();
        }
        private void EscalateBtn_Click(object sender, EventArgs e)
        {
            EscalateToManagement();
        }
        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string status = EODStatus();
                var confirmResult = MessageBox.Show("Are you sure to submit Sales Data Form Record ??",
                                     "Confirm!!",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    RemarksPopUpForm remarksform = new RemarksPopUpForm();
                    ChildColorConfig(remarksform);
                    remarksform.FormRef = this;
                    remarksform.FormStatus = status;
                    TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
                    remarksform.Show();
                }
                else
                {
                    // If 'No', do something here.
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void label15_Click(object sender, EventArgs e)
        {

        }
        private void txtActualBankDeposit_Leave(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            //{
            //    ((TextBoxBase)(sender)).Text = "0";

            //}
            //AddCash();
        }

        private void label35_Click(object sender, EventArgs e)
        {

        }

        private void txtCashPaid_TextChanged(object sender, EventArgs e)
        {

        }

        private void grpCash_Enter(object sender, EventArgs e)
        {

        }

        private void txtCashDriveAway_TextChanged(object sender, EventArgs e)
        {

        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void txtCashPaidEmp_TextChanged(object sender, EventArgs e)
        {

        }

        private void label66_Click(object sender, EventArgs e)
        {

        }

        private void txtradiantcrdiff_TextChanged(object sender, EventArgs e)
        {

        }

        private void label34_Click(object sender, EventArgs e)
        {

        }

        private void txtCashlottto_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtgstpaidonrent_TextChanged(object sender, EventArgs e)
        {

        }

        private void SettlementpopBtn_Click(object sender, EventArgs e)
        {
            using (PastSettlementForm child = new PastSettlementForm())
            {
                if (child.ShowDialog() == DialogResult.OK)
                {
                    string b = child.SelectedDates;
                    txtShellRemarks.Text = child.ActualPastSettlementRemarks;
                }
            }
        }

        private void UpdatePastSettlement()
        {
            if (!string.IsNullOrEmpty(PastSettlements))
            {
                using (ShellEntities context = new ShellEntities())
                {
                    string[] tranDate = PastSettlements.Split(',');

                    foreach (var date in tranDate)
                    {
                        DateTime Tdate = Convert.ToDateTime(date);
                        var fuelTrans = context.MST_FuelTransaction.Where(s => s.TransactionDate == Tdate).FirstOrDefault();
                        if (fuelTrans != null)
                        {
                            fuelTrans.IsPastSettlementDone = true;
                        }
                    }
                    context.SaveChanges();
                }
            }
        }
        private void selectgridvalue(DataGridView dgv, string dgvalue)
        {
            if (!string.IsNullOrEmpty(dgvalue))
            {
                for (int i = 0; i < dgvalue.Split(',').Length; i++)
                {
                    foreach (DataGridViewRow item in dgv.Rows)
                    {
                        if (item.Cells["docid"].Value.ToString() == dgvalue[i].ToString())
                        {
                            item.Cells[1].Value = true;

                        }
                    }
                }

            }
        }
        private void SavedDocuments(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(dtpTransactionDate.Text).ToString("MMddyyyy");
                var docList = context.Tbl_FuelTransactionDocuments.Where(s => s.DocumentType == docType).Select(s => new DocumentList { DocId = s.UploadedDocID, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();

                foreach (DataGridViewRow item in gridView.Rows)
                {
                    var docs = docList.Where(s => s.DocId == Convert.ToInt64(item.Cells["DocId"].Value)).FirstOrDefault();
                    if (docs != null && docs.DocId == Convert.ToInt64(item.Cells["DocId"].Value))
                    {
                        item.Cells[0].Value = true;
                    }
                }
            }

        }
        public void calculate()
        {
            AddCtoreSale();
            lottopayout();
            AddFuel();
            AddAirmiles();
            AddSettlement();
            AddCash();
            CardRem();
            DailyStoreGST();
        }
        private void btnCals_Click(object sender, EventArgs e)
        {
            try
            {
                calculate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void AddCtoreSale()
        {
            Tobbaco = Convert.ToDouble(txttobbaco.Text);
            Online_Lottoo = Convert.ToDouble(txtonlinelotto.Text);
            Scratch_Lotto = Convert.ToDouble(txtscratchlotto.Text);
            Phone_Cards = Convert.ToDouble(txtphonecard.Text);
            Gift_Cards = Convert.ToDouble(txtgiftcard.Text);
            Shell_Gift_Cards = Convert.ToDouble(txtshelGisftcard.Text);
            CstoreSale = Convert.ToDouble(txtCStoreSale.Text);
            Store_Others = CstoreSale - (Tobbaco + Online_Lottoo + Scratch_Lotto + Gift_Cards + Phone_Cards + Shell_Gift_Cards);
            txtCalstoreother.Text = Store_Others.ToString("0.00");
        }
        private void lottopayout()
        {
        }
        private void AddFuel()
        {
            double bronze = 0.0, Siler = 0.0, diesiel = 0.0, VPowerD = 0.0, VpowerGas = 0.0, BSDltr = 0.0, VpDvdd = 0.0, FuelComm = 0.0, Gstreceived = 0.0;

            bronze = Convert.ToDouble(txtbronzeltr.Text);
            Siler = Convert.ToDouble(txtSilverltr.Text);
            diesiel = Convert.ToDouble(txtdieselltr.Text);
            VPowerD = Convert.ToDouble(txtvpowerltr.Text);
            VpowerGas = Convert.ToDouble(txtVppltr.Text);
            txtBSDltr.Text = (BSDltr = (bronze + Siler + diesiel)).ToString();
            txtVpdvdd.Text = (VpDvdd = (VPowerD + VpowerGas)).ToString();
            FuelComm = (BSDltr * 1 / 100) + (VpDvdd * 1.25 / 100);
            Gstreceived = FuelComm * 5 / 100;
            txtfuelcommexgst.Text = FuelComm.ToString("0.00");
            txtgstreceivedfuel.Text = Gstreceived.ToString("0.00");
        }
        private void AddAirmiles()
        {
            AddSale();
            double Airmiles = 0.0, CashBankDep = 0.0, CashDrive = 0.0, LotoWinner = 0.0, CashLotoPayout = 0.0, TotalLottoPayout = 0.0, TotalAirmile = 0.0, difindrcr = 0.0;
            double RadiantDebit = 0.0, RadiantCredit = 0.0;
            RadiantDebit = Convert.ToDouble(txtRadiantdb.Text);
            RadiantCredit = Convert.ToDouble(txtRadiantcr.Text);
            Airmiles = Convert.ToDouble(txtAirmiles.Text);
            CashBankDep = Convert.ToDouble(txtCashBankDep.Text);
            CashDrive = Convert.ToDouble(txtCashDriveAway.Text);
            LotoWinner = Convert.ToDouble(txtLotowinner.Text);
            txttotalFuel.Text = txtFuel.Text.ToString();
            // difindrcr = Convert.ToDouble(txtdifindrcr.Text);
            TotalAirmile = (Airmiles + CashBankDep + CashDrive + LotoWinner + (RadiantCredit + RadiantDebit));
            TotalSale = Convert.ToDouble(txtSalTotal.Text);
            txttotalFuel.Text = TotalAirmile.ToString();
            txtDiff.Text = (TotalAirmile - TotalSale).ToString("0.00");
            Discounts = Convert.ToDouble(txtDiscount.Text);
            txtActualShort.Text = ((TotalAirmile - TotalSale) - Discounts).ToString("0.00");
        }
        private void AddSettlement()
        {
            double RadiantDebit = 0.0, RadiantCredit = 0.0, ShellDebit = 0.0, ShellCredit = 0.0, diff = 0.0, LotoWinner = 0.0, CashLotoPayout = 0.0, TotalLottoPayout = 0.0;
            RadiantDebit = Convert.ToDouble(txtRadiantdb.Text);
            RadiantCredit = Convert.ToDouble(txtRadiantcr.Text);
            ShellDebit = Convert.ToDouble(txtShellDebit.Text);
            ShellCredit = Convert.ToDouble(txtShellCredit.Text);
            LotoWinner = Convert.ToDouble(txtLotowinner.Text);
            CashLotoPayout = Convert.ToDouble(txtCashlottto.Text);
            TotalLottoPayout = CashLotoPayout + LotoWinner;
            txttotallotopayout.Text = TotalLottoPayout.ToString("0.00");
            diff = RadiantDebit - ShellDebit;
            txtRadiantdbdiff.Text = (diff).ToString("0.00");
            diff = RadiantCredit - ShellCredit;
            txtradiantcrdiff.Text = (diff).ToString("0.00");
            AddAirmiles();
        }
        private void AddCash()
        {
            double CashPayoutSp = 0.0, CashPayoutLotto = 0.0, CashPaidSP = 0.0, CashPaidEmployee = 0.0, CashPaidMngt = 0.0,
                CashSort = 0.0, Cashreceived = 0.0, CashBankDep = 0.0,
                BalanceLeftForBankDeposit = 0.0, SumOfTotalBank = 0.0, CashDriveAway = 0.0,
                DebitCredit = 0.0, CashDiff = 0.0, SaleTotal = 0.0,
                Q = 0.0, V = 0.0, M = 0.0, R = 0.0, S = 0.0, T = 0.0, U = 0.0;
            CashPayoutSp = Convert.ToDouble(txtcashpayoutStore.Text);
            CashPayoutLotto = Convert.ToDouble(txtCashlottto.Text);
            CashPaidEmployee = Convert.ToDouble(txtCashPaidEmp.Text);
            CashPaidSP = Convert.ToDouble(txtCashPaid.Text);
            CashPaidMngt = Convert.ToDouble(txtCashpaidmgt.Text);
            CashSort = Convert.ToDouble(txtCashSort.Text);
            Cashreceived = Convert.ToDouble(txtCashReceived.Text);
            CashBankDep = Convert.ToDouble(txtCashBankDep.Text);
            CashDriveAway = Convert.ToDouble(txtCashDriveAway.Text);
            DebitCredit = Convert.ToDouble(txtDebitCredit.Text);
            SaleTotal = Convert.ToDouble(txtSalTotal.Text);
            Q = CashPayoutSp;
            V = Convert.ToDouble(txtCashSort.Text);
            M = Convert.ToDouble(txtLotowinner.Text);
            R = CashPayoutLotto;
            S = CashPaidSP;
            T = CashPaidEmployee;
            U = CashPaidMngt;


            SumOfTotalBank = Convert.ToDouble(txtAirMilesCash.Text) + CashBankDep + CashDriveAway + DebitCredit + M;
            CashDiff = SumOfTotalBank - SaleTotal;
            txtBankDiff.Text = CashDiff.ToString("0.00");
            txtTotalBankSection.Text = SumOfTotalBank.ToString();
            BalanceLeftForBankDeposit = (CashBankDep + Cashreceived) - (Q + R + S + T + U + V);
            //txtleftBankdeposit.Text = ((Cashreceived + CashBankDep) - ((CashPayoutSp + CashPayoutLotto + CashPaidSP + CashPaidEmployee + CashPaidMngt + CashSort))).ToString("0.00");
            txtleftBankdeposit.Text = BalanceLeftForBankDeposit.ToString("0.00");
            // txttotallotopayout.Text = CashPayoutLotto.ToString();
            if (txtActualBankDeposit.Text == txtleftBankdeposit.Text)
            {
                txtActualBankDeposit.Text = txtleftBankdeposit.Text;
            }
            CashDifferenceTxt.Text = (Convert.ToDouble(txtActualBankDeposit.Text) - Convert.ToDouble(txtleftBankdeposit.Text)).ToString();
        }
        private void txtCardRembsment_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }

            CardRem();
        }
        private void CardRem()
        {
            double CardRembsment = 0.0, RadiantDebit = 0.0, RadiantCredit = 0.0, difindrcr = 0.0, totalfuelsale = 0.0,
                dailrentgst = 0.0, actualcommission = 0.0, totalset = 0.0, fairshare = 0.0, debitcredit = 000;
            CardRembsment = Convert.ToDouble(txtCardRembsment.Text);
            RadiantDebit = Convert.ToDouble(txtRadiantdb.Text);
            RadiantCredit = Convert.ToDouble(txtRadiantcr.Text);
            //difindrcr = (CardRembsment - (RadiantCredit + RadiantDebit)); // Radiant has no impact said by Phougat

            totalfuelsale = Convert.ToDouble(txtFuel.Text);
            dailrentgst = Convert.ToDouble(txtdailystorerntincgst.Text);
            fairshare = Convert.ToDouble(txtfairshare.Text);
            actualcommission = Convert.ToDouble(txtActualCommission.Text);
            debitcredit = Convert.ToDouble(txtDebitCredit.Text);
            difindrcr = (CardRembsment - (debitcredit));
            txtdifindrcr.Text = difindrcr.ToString("#.##");
            // totalset = (totalfuelsale + dailrentgst) - (RadiantCredit + RadiantDebit + actualcommission); //Radaint has no impact said by Phougat
            totalset = (totalfuelsale + dailrentgst) - (debitcredit + actualcommission);
            txtfinalsettlement.Text = ((totalset - difindrcr) - fairshare).ToString("#.##");
            txttotalsettlement.Text = totalset.ToString("#.##");
            DiffInFuelCommTxt.Text = ((Convert.ToDouble(txtfuelcommexgst.Text) + Convert.ToDouble(txtgstreceivedfuel.Text)) - Convert.ToDouble(txtActualCommission.Text)).ToString("#.##");
            DiffInSettlementTxt.Text = ((Convert.ToDouble(txtfinalsettlement.Text) - Convert.ToDouble(txtActualSettlement.Text))).ToString("#.##");

            // New Implementation 16-Dec-2017


        }
        private void dgvCstore_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                DataGridView dgv = ((DataGridView)(sender));
                if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[0].Value) == false)
                {
                    dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
                }

                string gridselval = "";
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    if (Convert.ToBoolean(r.Cells[0].Value))
                    {
                        int i = r.Index;

                        gridselval = gridselval + "," + dgv.Rows[i].Cells[1].Value.ToString();
                    }

                }
                if (gridselval.Length > 0)
                {
                    gridselval = gridselval.Remove(0, 1);
                }
                switch (dgv.Name)
                {
                    case "dgvCash":
                        _cklcash = gridselval;
                        break;
                    case "dgvCstore":
                        _cklCstoredoc = gridselval;
                        break;
                    case "dgvSettlement":
                        _cklsettlement = gridselval;
                        break;
                    case "dgvTabbacco":
                        _ckltabbacco = gridselval;
                        break;
                    case "dgvFuel":
                        _cklfuel = gridselval;
                        break;
                }
            }
        }
        private void dgvCstore_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 2)
                {
                    DataGridView dgv = ((DataGridView)(sender));
                    int docId = Convert.ToInt32(dgv.Rows[e.RowIndex].Cells[1].Value);
                    string path = FilePath(docId);
                    if (!string.IsNullOrEmpty(path))
                    {
                        System.Diagnostics.Process.Start("explorer.exe", path);
                    }
                    else
                    {
                        MessageBox.Show("File Path doesn't exist");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private string FilePath(int docId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                Mst_DocUpload doc = context.Mst_DocUpload.FirstOrDefault(s => s.UploadId == docId);
                if (doc != null)
                {
                    return doc.FilePath;
                }
                else
                {
                    return "";
                }
            }
        }
    }
}
