﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using DAL;

namespace Shell
{
    public partial class GasTransactionViewForm : BaseForm
    {
        public GasTransactionViewForm()
        {
            InitializeComponent();
        }
        private void LoadGridView()
        {
            BAL_FuelTransaction fuelTransaction = new BAL_FuelTransaction();
            dgvRecords.Visible = true;
            dgvRecords.AutoGenerateColumns = true;
            DataSet ds = new DataSet();
            int type = 0;
            if (rbtSummary.Checked == true)
            {
                type = 1;

            }
            else if (rbtSalevsGst.Checked == true)
            {
                type = 2;//-- Sale VS GST
            }

            else if (rbtFulecommission.Checked == true)
            {
                type = 3;//-- FuelCommission VS GST

            }
            else if (rbtDbCardSettlemet.Checked == true)
            {
                type = 4;//-- Card SettleMentReport 

            }
            else if (rbtrentgst.Checked == true)
            {
                type = 5;//-- Rent vs Gst Paid Report

            }
            else if (rbtInvoices.Checked == true)
            {
                type = 6;//--Cash Invoices Vs Gst Paid Report 

            }
            else if (rbtMonthlyReconcilation.Checked == true)
            {
                type = 7;//--Monthly Reconcilation Reports

            }
            else if (rbtSale.Checked == true)
            {
                type = 8;//--Monthly Reconcilation Reports

            }
            else if (rbtBankReconcilation.Checked == true)
            {
                type =9;//--Monthly Reconcilation Reports

            }
            else if (rbtInventory.Checked == true)
            {
                type = 10;//--Monthly Reconcilation Reports

            }


            ds = fuelTransaction.ListFuelTransactions(type, dtpFrom.Value, dtpTo.Value);
            dgvRecords.DataSource = ds.Tables[0];


        }



        private void btnDisplayRecord_Click(object sender, EventArgs e)
        {
            try
            {
                LoadGridView();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void dgvRecords_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
