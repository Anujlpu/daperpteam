﻿namespace Shell.Transaction
{
    partial class InventoryManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CategorycomboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ProductcomboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.DateCalender = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.InventorygroupBox = new System.Windows.Forms.GroupBox();
            this.dgvInventory = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.OpeningtextBox = new System.Windows.Forms.TextBox();
            this.ClosingtextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.AddtextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SubstracttextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.Updatebutton = new System.Windows.Forms.Button();
            this.Exportbutton = new System.Windows.Forms.Button();
            this.InventorygroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInventory)).BeginInit();
            this.SuspendLayout();
            // 
            // CategorycomboBox
            // 
            this.CategorycomboBox.FormattingEnabled = true;
            this.CategorycomboBox.Location = new System.Drawing.Point(157, 24);
            this.CategorycomboBox.Name = "CategorycomboBox";
            this.CategorycomboBox.Size = new System.Drawing.Size(202, 21);
            this.CategorycomboBox.TabIndex = 207;
            this.CategorycomboBox.SelectedIndexChanged += new System.EventHandler(this.CategorycomboBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(46, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 15);
            this.label2.TabIndex = 206;
            this.label2.Text = "Category Name";
            // 
            // ProductcomboBox
            // 
            this.ProductcomboBox.FormattingEnabled = true;
            this.ProductcomboBox.Location = new System.Drawing.Point(530, 24);
            this.ProductcomboBox.Name = "ProductcomboBox";
            this.ProductcomboBox.Size = new System.Drawing.Size(202, 21);
            this.ProductcomboBox.TabIndex = 209;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(419, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 15);
            this.label1.TabIndex = 208;
            this.label1.Text = "Product Name";
            // 
            // DateCalender
            // 
            this.DateCalender.CustomFormat = "MMM/dd/yyyy";
            this.DateCalender.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateCalender.Location = new System.Drawing.Point(157, 63);
            this.DateCalender.Name = "DateCalender";
            this.DateCalender.Size = new System.Drawing.Size(202, 20);
            this.DateCalender.TabIndex = 211;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(46, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 210;
            this.label3.Text = "Date";
            // 
            // ViewBtn
            // 
            this.ViewBtn.Location = new System.Drawing.Point(157, 101);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(79, 28);
            this.ViewBtn.TabIndex = 212;
            this.ViewBtn.Text = "View";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // InventorygroupBox
            // 
            this.InventorygroupBox.Controls.Add(this.dgvInventory);
            this.InventorygroupBox.Location = new System.Drawing.Point(12, 320);
            this.InventorygroupBox.Name = "InventorygroupBox";
            this.InventorygroupBox.Size = new System.Drawing.Size(811, 178);
            this.InventorygroupBox.TabIndex = 213;
            this.InventorygroupBox.TabStop = false;
            this.InventorygroupBox.Text = "Inventory Details";
            // 
            // dgvInventory
            // 
            this.dgvInventory.AllowDrop = true;
            this.dgvInventory.AllowUserToAddRows = false;
            this.dgvInventory.AllowUserToDeleteRows = false;
            this.dgvInventory.AllowUserToResizeColumns = false;
            this.dgvInventory.AllowUserToResizeRows = false;
            this.dgvInventory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvInventory.BackgroundColor = System.Drawing.Color.White;
            this.dgvInventory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInventory.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInventory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInventory.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvInventory.Location = new System.Drawing.Point(3, 16);
            this.dgvInventory.MultiSelect = false;
            this.dgvInventory.Name = "dgvInventory";
            this.dgvInventory.ReadOnly = true;
            this.dgvInventory.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvInventory.RowHeadersVisible = false;
            this.dgvInventory.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInventory.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvInventory.Size = new System.Drawing.Size(805, 159);
            this.dgvInventory.TabIndex = 2;
            this.dgvInventory.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInventory_CellClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 214;
            this.label4.Text = "Opening";
            // 
            // OpeningtextBox
            // 
            this.OpeningtextBox.Enabled = false;
            this.OpeningtextBox.Location = new System.Drawing.Point(157, 145);
            this.OpeningtextBox.Name = "OpeningtextBox";
            this.OpeningtextBox.Size = new System.Drawing.Size(202, 20);
            this.OpeningtextBox.TabIndex = 215;
            this.OpeningtextBox.Text = "0";
            // 
            // ClosingtextBox
            // 
            this.ClosingtextBox.Enabled = false;
            this.ClosingtextBox.Location = new System.Drawing.Point(530, 145);
            this.ClosingtextBox.Name = "ClosingtextBox";
            this.ClosingtextBox.Size = new System.Drawing.Size(202, 20);
            this.ClosingtextBox.TabIndex = 217;
            this.ClosingtextBox.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(422, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 216;
            this.label5.Text = "Closing/Balance";
            // 
            // AddtextBox
            // 
            this.AddtextBox.Location = new System.Drawing.Point(157, 190);
            this.AddtextBox.Name = "AddtextBox";
            this.AddtextBox.Size = new System.Drawing.Size(202, 20);
            this.AddtextBox.TabIndex = 219;
            this.AddtextBox.Text = "0";
            this.AddtextBox.Leave += new System.EventHandler(this.AddtextBox_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(49, 190);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 218;
            this.label6.Text = "Add";
            // 
            // SubstracttextBox
            // 
            this.SubstracttextBox.Location = new System.Drawing.Point(530, 190);
            this.SubstracttextBox.Name = "SubstracttextBox";
            this.SubstracttextBox.Size = new System.Drawing.Size(202, 20);
            this.SubstracttextBox.TabIndex = 221;
            this.SubstracttextBox.Text = "0";
            this.SubstracttextBox.Leave += new System.EventHandler(this.SubstracttextBox_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(422, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 220;
            this.label7.Text = "Substract";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(492, 235);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 224;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(375, 235);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 223;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(157, 236);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 222;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // Updatebutton
            // 
            this.Updatebutton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Updatebutton.FlatAppearance.BorderSize = 2;
            this.Updatebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Updatebutton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Updatebutton.Location = new System.Drawing.Point(269, 235);
            this.Updatebutton.Name = "Updatebutton";
            this.Updatebutton.Size = new System.Drawing.Size(90, 32);
            this.Updatebutton.TabIndex = 225;
            this.Updatebutton.Text = "&Update";
            this.Updatebutton.UseVisualStyleBackColor = true;
            this.Updatebutton.Click += new System.EventHandler(this.Updatebutton_Click);
            // 
            // Exportbutton
            // 
            this.Exportbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exportbutton.Location = new System.Drawing.Point(15, 281);
            this.Exportbutton.Name = "Exportbutton";
            this.Exportbutton.Size = new System.Drawing.Size(90, 33);
            this.Exportbutton.TabIndex = 226;
            this.Exportbutton.Text = "Export";
            this.Exportbutton.UseVisualStyleBackColor = true;
            this.Exportbutton.Click += new System.EventHandler(this.Exportbutton_Click);
            // 
            // InventoryManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(980, 543);
            this.Controls.Add(this.Exportbutton);
            this.Controls.Add(this.Updatebutton);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.BtnDelete);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.SubstracttextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.AddtextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ClosingtextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.OpeningtextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.InventorygroupBox);
            this.Controls.Add(this.ViewBtn);
            this.Controls.Add(this.DateCalender);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ProductcomboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CategorycomboBox);
            this.Controls.Add(this.label2);
            this.Name = "InventoryManagementForm";
            this.Text = "Inventory Management";
            this.Load += new System.EventHandler(this.InventoryManagementForm_Load);
            this.InventorygroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInventory)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CategorycomboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ProductcomboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DateCalender;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.GroupBox InventorygroupBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox OpeningtextBox;
        private System.Windows.Forms.TextBox ClosingtextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox AddtextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox SubstracttextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.DataGridView dgvInventory;
        private System.Windows.Forms.Button Updatebutton;
        private System.Windows.Forms.Button Exportbutton;
    }
}