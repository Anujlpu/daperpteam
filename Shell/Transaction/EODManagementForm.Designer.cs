﻿namespace Shell.Transaction
{
    partial class EODManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.CigrateesGroup = new System.Windows.Forms.GroupBox();
            this.EODTobaccoReportTotalSoldTxt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.EODTobaccoReportOtherSoldTxt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.EODTobaccoReportLighterSoldTxt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.EODTobaccoReportECigarSoldTxt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.EODTobaccoReportChewSoldTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.EODTobaccoReportCigarSoldTxt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ActualOpenDiffTxt = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.CigrateesRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.CigrateesDocsDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label73 = new System.Windows.Forms.Label();
            this.CigrateesDiffTxt = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.CigrateesEODSoldTxt = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.CigrateesCountSoldTxt = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.CigrateesCloseTxt = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.CigrateesSubTotaltxt = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.CigrateesAddTxt = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.CigrateesActualOpenTxt = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.CigrateesOpentxt = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.Chewgroup = new System.Windows.Forms.GroupBox();
            this.ChewActualOpenDiffTxt = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.ChewsRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.ChewsDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label72 = new System.Windows.Forms.Label();
            this.ChewDifftxt = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.ChewEODSoldTxt = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.ChewCountSoldTxt = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.ChewCloseTxt = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.ChewSubTotaltxt = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.ChewAddTxt = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.ChewActualopentxt = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.ChewOpentxt = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.Additiongroup = new System.Windows.Forms.GroupBox();
            this.AdditionECigarTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AdditionCigars5PackTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AdditionPackTotalTxt = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.AdditionRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.AdditionDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn6 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label76 = new System.Windows.Forms.Label();
            this.AdditionTotalTxt = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.AdditionOtherTxt = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.AdditionChewsTxt = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.AdditionCigars8PackTxt = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.AdditionCigars10Packtxt = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.Addition10PackTxt = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.Addition8Packtxt = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.WWFgroup = new System.Windows.Forms.GroupBox();
            this.WWFRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.WWFDocsDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn7 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label78 = new System.Windows.Forms.Label();
            this.WWFBalanceTxt = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.WWFExpectedInventoryTxt = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.WWFActualInventoryTxt = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.WWFUsedOutSideTxt = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.WWFSoldTxt = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.WWFAddedTxt = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.WWFCasesTxt = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.WWFLastDatTotalTxt = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.CigarGroup = new System.Windows.Forms.GroupBox();
            this.CigarActualOpenDifftxt = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.CigarRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.CigarDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn8 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label86 = new System.Windows.Forms.Label();
            this.CigarDiffTxt = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.CigarEODSoldTxt = new System.Windows.Forms.TextBox();
            this.label88 = new System.Windows.Forms.Label();
            this.CigarCountSoldtxt = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.CigarCloseTxt = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.CigarSubTotaltxt = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.CigarAddTxt = new System.Windows.Forms.TextBox();
            this.label92 = new System.Windows.Forms.Label();
            this.CigarActualopentxt = new System.Windows.Forms.TextBox();
            this.label93 = new System.Windows.Forms.Label();
            this.CigarOpentxt = new System.Windows.Forms.TextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DateCalender = new System.Windows.Forms.DateTimePicker();
            this.btnExit = new System.Windows.Forms.Button();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ECigarActualOpenDiffTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ECigarRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ECigarDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.ECigarDiffTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.ECigarEODSoldTxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ECigarCountSoldTxt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.ECigarCloseTxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.ECigarSubTotalTxt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ECigarAddTxt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ECigarActualOpenTxt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ECigarOpenTxt = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.CigrateesGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CigrateesDocsDgv)).BeginInit();
            this.Chewgroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChewsDgv)).BeginInit();
            this.Additiongroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AdditionDgv)).BeginInit();
            this.WWFgroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WWFDocsDgv)).BeginInit();
            this.CigarGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CigarDgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ECigarDgv)).BeginInit();
            this.SuspendLayout();
            // 
            // CigrateesGroup
            // 
            this.CigrateesGroup.Controls.Add(this.EODTobaccoReportTotalSoldTxt);
            this.CigrateesGroup.Controls.Add(this.label20);
            this.CigrateesGroup.Controls.Add(this.EODTobaccoReportOtherSoldTxt);
            this.CigrateesGroup.Controls.Add(this.label19);
            this.CigrateesGroup.Controls.Add(this.EODTobaccoReportLighterSoldTxt);
            this.CigrateesGroup.Controls.Add(this.label18);
            this.CigrateesGroup.Controls.Add(this.EODTobaccoReportECigarSoldTxt);
            this.CigrateesGroup.Controls.Add(this.label17);
            this.CigrateesGroup.Controls.Add(this.EODTobaccoReportChewSoldTxt);
            this.CigrateesGroup.Controls.Add(this.label16);
            this.CigrateesGroup.Controls.Add(this.EODTobaccoReportCigarSoldTxt);
            this.CigrateesGroup.Controls.Add(this.label15);
            this.CigrateesGroup.Controls.Add(this.ActualOpenDiffTxt);
            this.CigrateesGroup.Controls.Add(this.label82);
            this.CigrateesGroup.Controls.Add(this.CigrateesRemarksTxt);
            this.CigrateesGroup.Controls.Add(this.label74);
            this.CigrateesGroup.Controls.Add(this.CigrateesDocsDgv);
            this.CigrateesGroup.Controls.Add(this.label73);
            this.CigrateesGroup.Controls.Add(this.CigrateesDiffTxt);
            this.CigrateesGroup.Controls.Add(this.label42);
            this.CigrateesGroup.Controls.Add(this.CigrateesEODSoldTxt);
            this.CigrateesGroup.Controls.Add(this.label41);
            this.CigrateesGroup.Controls.Add(this.CigrateesCountSoldTxt);
            this.CigrateesGroup.Controls.Add(this.label40);
            this.CigrateesGroup.Controls.Add(this.CigrateesCloseTxt);
            this.CigrateesGroup.Controls.Add(this.label39);
            this.CigrateesGroup.Controls.Add(this.CigrateesSubTotaltxt);
            this.CigrateesGroup.Controls.Add(this.label38);
            this.CigrateesGroup.Controls.Add(this.CigrateesAddTxt);
            this.CigrateesGroup.Controls.Add(this.label37);
            this.CigrateesGroup.Controls.Add(this.CigrateesActualOpenTxt);
            this.CigrateesGroup.Controls.Add(this.label35);
            this.CigrateesGroup.Controls.Add(this.CigrateesOpentxt);
            this.CigrateesGroup.Controls.Add(this.label36);
            this.CigrateesGroup.Location = new System.Drawing.Point(6, 384);
            this.CigrateesGroup.Name = "CigrateesGroup";
            this.CigrateesGroup.Size = new System.Drawing.Size(1049, 406);
            this.CigrateesGroup.TabIndex = 4;
            this.CigrateesGroup.TabStop = false;
            this.CigrateesGroup.Text = "Cigratees";
            // 
            // EODTobaccoReportTotalSoldTxt
            // 
            this.EODTobaccoReportTotalSoldTxt.Location = new System.Drawing.Point(460, 55);
            this.EODTobaccoReportTotalSoldTxt.Name = "EODTobaccoReportTotalSoldTxt";
            this.EODTobaccoReportTotalSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.EODTobaccoReportTotalSoldTxt.TabIndex = 261;
            this.EODTobaccoReportTotalSoldTxt.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(273, 58);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(130, 15);
            this.label20.TabIndex = 260;
            this.label20.Text = "EOD Tob. Report Total";
            // 
            // EODTobaccoReportOtherSoldTxt
            // 
            this.EODTobaccoReportOtherSoldTxt.Location = new System.Drawing.Point(460, 190);
            this.EODTobaccoReportOtherSoldTxt.Name = "EODTobaccoReportOtherSoldTxt";
            this.EODTobaccoReportOtherSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.EODTobaccoReportOtherSoldTxt.TabIndex = 257;
            this.EODTobaccoReportOtherSoldTxt.Text = "0";
            this.EODTobaccoReportOtherSoldTxt.Leave += new System.EventHandler(this.TobaccoTotalAutoCalculation_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(272, 193);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(161, 15);
            this.label19.TabIndex = 256;
            this.label19.Text = "EOD Tob. Report Other Sold";
            // 
            // EODTobaccoReportLighterSoldTxt
            // 
            this.EODTobaccoReportLighterSoldTxt.Location = new System.Drawing.Point(460, 163);
            this.EODTobaccoReportLighterSoldTxt.Name = "EODTobaccoReportLighterSoldTxt";
            this.EODTobaccoReportLighterSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.EODTobaccoReportLighterSoldTxt.TabIndex = 255;
            this.EODTobaccoReportLighterSoldTxt.Text = "0";
            this.EODTobaccoReportLighterSoldTxt.Leave += new System.EventHandler(this.TobaccoTotalAutoCalculation_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(272, 166);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(169, 15);
            this.label18.TabIndex = 254;
            this.label18.Text = "EOD Tob. Report Lighter Sold";
            // 
            // EODTobaccoReportECigarSoldTxt
            // 
            this.EODTobaccoReportECigarSoldTxt.Location = new System.Drawing.Point(460, 136);
            this.EODTobaccoReportECigarSoldTxt.Name = "EODTobaccoReportECigarSoldTxt";
            this.EODTobaccoReportECigarSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.EODTobaccoReportECigarSoldTxt.TabIndex = 253;
            this.EODTobaccoReportECigarSoldTxt.Text = "0";
            this.EODTobaccoReportECigarSoldTxt.Leave += new System.EventHandler(this.TobaccoTotalAutoCalculation_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(272, 139);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(172, 15);
            this.label17.TabIndex = 252;
            this.label17.Text = "EOD Tob. Report E-Cigar Sold";
            // 
            // EODTobaccoReportChewSoldTxt
            // 
            this.EODTobaccoReportChewSoldTxt.Location = new System.Drawing.Point(460, 109);
            this.EODTobaccoReportChewSoldTxt.Name = "EODTobaccoReportChewSoldTxt";
            this.EODTobaccoReportChewSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.EODTobaccoReportChewSoldTxt.TabIndex = 251;
            this.EODTobaccoReportChewSoldTxt.Text = "0";
            this.EODTobaccoReportChewSoldTxt.Leave += new System.EventHandler(this.TobaccoTotalAutoCalculation_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(272, 112);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(162, 15);
            this.label16.TabIndex = 250;
            this.label16.Text = "EOD Tob. Report Chew Sold";
            // 
            // EODTobaccoReportCigarSoldTxt
            // 
            this.EODTobaccoReportCigarSoldTxt.Location = new System.Drawing.Point(460, 82);
            this.EODTobaccoReportCigarSoldTxt.Name = "EODTobaccoReportCigarSoldTxt";
            this.EODTobaccoReportCigarSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.EODTobaccoReportCigarSoldTxt.TabIndex = 249;
            this.EODTobaccoReportCigarSoldTxt.Text = "0";
            this.EODTobaccoReportCigarSoldTxt.Leave += new System.EventHandler(this.TobaccoTotalAutoCalculation_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(272, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(160, 15);
            this.label15.TabIndex = 248;
            this.label15.Text = "EOD Tob. Report Cigar Sold";
            // 
            // ActualOpenDiffTxt
            // 
            this.ActualOpenDiffTxt.BackColor = System.Drawing.Color.Red;
            this.ActualOpenDiffTxt.Location = new System.Drawing.Point(124, 163);
            this.ActualOpenDiffTxt.Name = "ActualOpenDiffTxt";
            this.ActualOpenDiffTxt.ReadOnly = true;
            this.ActualOpenDiffTxt.Size = new System.Drawing.Size(125, 21);
            this.ActualOpenDiffTxt.TabIndex = 247;
            this.ActualOpenDiffTxt.Text = "0";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(16, 163);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(94, 15);
            this.label82.TabIndex = 246;
            this.label82.Text = "Actual Open Diff";
            // 
            // CigrateesRemarksTxt
            // 
            this.CigrateesRemarksTxt.Location = new System.Drawing.Point(460, 278);
            this.CigrateesRemarksTxt.Name = "CigrateesRemarksTxt";
            this.CigrateesRemarksTxt.Size = new System.Drawing.Size(280, 101);
            this.CigrateesRemarksTxt.TabIndex = 245;
            this.CigrateesRemarksTxt.Text = "";
            this.CigrateesRemarksTxt.TextChanged += new System.EventHandler(this.CigrateesRemarksTxt_TextChanged);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(272, 281);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(57, 15);
            this.label74.TabIndex = 244;
            this.label74.Text = "Remarks";
            this.label74.Click += new System.EventHandler(this.label74_Click);
            // 
            // CigrateesDocsDgv
            // 
            this.CigrateesDocsDgv.AllowDrop = true;
            this.CigrateesDocsDgv.AllowUserToAddRows = false;
            this.CigrateesDocsDgv.AllowUserToDeleteRows = false;
            this.CigrateesDocsDgv.AllowUserToResizeColumns = false;
            this.CigrateesDocsDgv.AllowUserToResizeRows = false;
            this.CigrateesDocsDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.CigrateesDocsDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CigrateesDocsDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.CigrateesDocsDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CigrateesDocsDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn5});
            this.CigrateesDocsDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.CigrateesDocsDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.CigrateesDocsDgv.Location = new System.Drawing.Point(811, 36);
            this.CigrateesDocsDgv.MultiSelect = false;
            this.CigrateesDocsDgv.Name = "CigrateesDocsDgv";
            this.CigrateesDocsDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CigrateesDocsDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.CigrateesDocsDgv.RowHeadersVisible = false;
            this.CigrateesDocsDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.CigrateesDocsDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CigrateesDocsDgv.Size = new System.Drawing.Size(222, 115);
            this.CigrateesDocsDgv.TabIndex = 242;
            this.CigrateesDocsDgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCommon_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Width = 5;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(666, 36);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(93, 15);
            this.label73.TabIndex = 243;
            this.label73.Text = "Cigratees Docs";
            // 
            // CigrateesDiffTxt
            // 
            this.CigrateesDiffTxt.BackColor = System.Drawing.Color.Red;
            this.CigrateesDiffTxt.Location = new System.Drawing.Point(460, 251);
            this.CigrateesDiffTxt.Name = "CigrateesDiffTxt";
            this.CigrateesDiffTxt.ReadOnly = true;
            this.CigrateesDiffTxt.Size = new System.Drawing.Size(125, 21);
            this.CigrateesDiffTxt.TabIndex = 21;
            this.CigrateesDiffTxt.Text = "0";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(273, 254);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(49, 15);
            this.label42.TabIndex = 20;
            this.label42.Text = "Cig. Diff";
            this.label42.Click += new System.EventHandler(this.label42_Click);
            // 
            // CigrateesEODSoldTxt
            // 
            this.CigrateesEODSoldTxt.Location = new System.Drawing.Point(460, 217);
            this.CigrateesEODSoldTxt.Name = "CigrateesEODSoldTxt";
            this.CigrateesEODSoldTxt.ReadOnly = true;
            this.CigrateesEODSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.CigrateesEODSoldTxt.TabIndex = 19;
            this.CigrateesEODSoldTxt.Text = "0";
            this.CigrateesEODSoldTxt.Leave += new System.EventHandler(this.TobaccoTotalAutoCalculation_Leave);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(273, 220);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(152, 15);
            this.label41.TabIndex = 18;
            this.label41.Text = "EOD Tob. Report Cig. Sold";
            // 
            // CigrateesCountSoldTxt
            // 
            this.CigrateesCountSoldTxt.Location = new System.Drawing.Point(460, 32);
            this.CigrateesCountSoldTxt.Name = "CigrateesCountSoldTxt";
            this.CigrateesCountSoldTxt.ReadOnly = true;
            this.CigrateesCountSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.CigrateesCountSoldTxt.TabIndex = 17;
            this.CigrateesCountSoldTxt.Text = "0";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(272, 36);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(67, 15);
            this.label40.TabIndex = 16;
            this.label40.Text = "Count Sold";
            // 
            // CigrateesCloseTxt
            // 
            this.CigrateesCloseTxt.Location = new System.Drawing.Point(124, 136);
            this.CigrateesCloseTxt.Name = "CigrateesCloseTxt";
            this.CigrateesCloseTxt.Size = new System.Drawing.Size(125, 21);
            this.CigrateesCloseTxt.TabIndex = 15;
            this.CigrateesCloseTxt.Text = "0";
            this.CigrateesCloseTxt.Leave += new System.EventHandler(this.Addition8Packtxt_Leave);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(16, 136);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(38, 15);
            this.label39.TabIndex = 14;
            this.label39.Text = "Close";
            // 
            // CigrateesSubTotaltxt
            // 
            this.CigrateesSubTotaltxt.Location = new System.Drawing.Point(124, 109);
            this.CigrateesSubTotaltxt.Name = "CigrateesSubTotaltxt";
            this.CigrateesSubTotaltxt.ReadOnly = true;
            this.CigrateesSubTotaltxt.Size = new System.Drawing.Size(125, 21);
            this.CigrateesSubTotaltxt.TabIndex = 13;
            this.CigrateesSubTotaltxt.Text = "0";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(16, 109);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(59, 15);
            this.label38.TabIndex = 12;
            this.label38.Text = "Sub Total";
            // 
            // CigrateesAddTxt
            // 
            this.CigrateesAddTxt.Location = new System.Drawing.Point(124, 82);
            this.CigrateesAddTxt.Name = "CigrateesAddTxt";
            this.CigrateesAddTxt.ReadOnly = true;
            this.CigrateesAddTxt.Size = new System.Drawing.Size(125, 21);
            this.CigrateesAddTxt.TabIndex = 11;
            this.CigrateesAddTxt.Text = "0";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(16, 82);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 15);
            this.label37.TabIndex = 10;
            this.label37.Text = "Add";
            // 
            // CigrateesActualOpenTxt
            // 
            this.CigrateesActualOpenTxt.Location = new System.Drawing.Point(124, 55);
            this.CigrateesActualOpenTxt.Name = "CigrateesActualOpenTxt";
            this.CigrateesActualOpenTxt.Size = new System.Drawing.Size(125, 21);
            this.CigrateesActualOpenTxt.TabIndex = 9;
            this.CigrateesActualOpenTxt.Text = "0";
            this.CigrateesActualOpenTxt.Leave += new System.EventHandler(this.Addition8Packtxt_Leave);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(16, 55);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(73, 15);
            this.label35.TabIndex = 8;
            this.label35.Text = "Actual Open";
            // 
            // CigrateesOpentxt
            // 
            this.CigrateesOpentxt.Location = new System.Drawing.Point(124, 28);
            this.CigrateesOpentxt.Name = "CigrateesOpentxt";
            this.CigrateesOpentxt.ReadOnly = true;
            this.CigrateesOpentxt.Size = new System.Drawing.Size(125, 21);
            this.CigrateesOpentxt.TabIndex = 7;
            this.CigrateesOpentxt.Text = "0";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(16, 28);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(37, 15);
            this.label36.TabIndex = 6;
            this.label36.Text = "Open";
            // 
            // Chewgroup
            // 
            this.Chewgroup.Controls.Add(this.ChewActualOpenDiffTxt);
            this.Chewgroup.Controls.Add(this.label83);
            this.Chewgroup.Controls.Add(this.ChewsRemarksTxt);
            this.Chewgroup.Controls.Add(this.label75);
            this.Chewgroup.Controls.Add(this.ChewsDgv);
            this.Chewgroup.Controls.Add(this.label72);
            this.Chewgroup.Controls.Add(this.ChewDifftxt);
            this.Chewgroup.Controls.Add(this.label43);
            this.Chewgroup.Controls.Add(this.ChewEODSoldTxt);
            this.Chewgroup.Controls.Add(this.label44);
            this.Chewgroup.Controls.Add(this.ChewCountSoldTxt);
            this.Chewgroup.Controls.Add(this.label45);
            this.Chewgroup.Controls.Add(this.ChewCloseTxt);
            this.Chewgroup.Controls.Add(this.label46);
            this.Chewgroup.Controls.Add(this.ChewSubTotaltxt);
            this.Chewgroup.Controls.Add(this.label47);
            this.Chewgroup.Controls.Add(this.ChewAddTxt);
            this.Chewgroup.Controls.Add(this.label48);
            this.Chewgroup.Controls.Add(this.ChewActualopentxt);
            this.Chewgroup.Controls.Add(this.label49);
            this.Chewgroup.Controls.Add(this.ChewOpentxt);
            this.Chewgroup.Controls.Add(this.label50);
            this.Chewgroup.Location = new System.Drawing.Point(6, 796);
            this.Chewgroup.Name = "Chewgroup";
            this.Chewgroup.Size = new System.Drawing.Size(1042, 194);
            this.Chewgroup.TabIndex = 22;
            this.Chewgroup.TabStop = false;
            this.Chewgroup.Text = "Chews";
            // 
            // ChewActualOpenDiffTxt
            // 
            this.ChewActualOpenDiffTxt.BackColor = System.Drawing.Color.Red;
            this.ChewActualOpenDiffTxt.Location = new System.Drawing.Point(124, 163);
            this.ChewActualOpenDiffTxt.Name = "ChewActualOpenDiffTxt";
            this.ChewActualOpenDiffTxt.ReadOnly = true;
            this.ChewActualOpenDiffTxt.Size = new System.Drawing.Size(125, 21);
            this.ChewActualOpenDiffTxt.TabIndex = 249;
            this.ChewActualOpenDiffTxt.Text = "0";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(16, 163);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(94, 15);
            this.label83.TabIndex = 248;
            this.label83.Text = "Actual Open Diff";
            // 
            // ChewsRemarksTxt
            // 
            this.ChewsRemarksTxt.Location = new System.Drawing.Point(380, 109);
            this.ChewsRemarksTxt.Name = "ChewsRemarksTxt";
            this.ChewsRemarksTxt.Size = new System.Drawing.Size(205, 45);
            this.ChewsRemarksTxt.TabIndex = 247;
            this.ChewsRemarksTxt.Text = "";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(273, 120);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(57, 15);
            this.label75.TabIndex = 246;
            this.label75.Text = "Remarks";
            // 
            // ChewsDgv
            // 
            this.ChewsDgv.AllowDrop = true;
            this.ChewsDgv.AllowUserToAddRows = false;
            this.ChewsDgv.AllowUserToDeleteRows = false;
            this.ChewsDgv.AllowUserToResizeColumns = false;
            this.ChewsDgv.AllowUserToResizeRows = false;
            this.ChewsDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ChewsDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChewsDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ChewsDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ChewsDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn4});
            this.ChewsDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ChewsDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.ChewsDgv.Location = new System.Drawing.Point(811, 28);
            this.ChewsDgv.MultiSelect = false;
            this.ChewsDgv.Name = "ChewsDgv";
            this.ChewsDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChewsDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.ChewsDgv.RowHeadersVisible = false;
            this.ChewsDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ChewsDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ChewsDgv.Size = new System.Drawing.Size(222, 105);
            this.ChewsDgv.TabIndex = 244;
            this.ChewsDgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCommon_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 5;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(659, 28);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(78, 15);
            this.label72.TabIndex = 245;
            this.label72.Text = "Chews Docs";
            // 
            // ChewDifftxt
            // 
            this.ChewDifftxt.BackColor = System.Drawing.Color.Red;
            this.ChewDifftxt.Location = new System.Drawing.Point(380, 82);
            this.ChewDifftxt.Name = "ChewDifftxt";
            this.ChewDifftxt.ReadOnly = true;
            this.ChewDifftxt.Size = new System.Drawing.Size(125, 21);
            this.ChewDifftxt.TabIndex = 21;
            this.ChewDifftxt.Text = "0";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(272, 82);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(25, 15);
            this.label43.TabIndex = 20;
            this.label43.Text = "Diff";
            // 
            // ChewEODSoldTxt
            // 
            this.ChewEODSoldTxt.Location = new System.Drawing.Point(380, 55);
            this.ChewEODSoldTxt.Name = "ChewEODSoldTxt";
            this.ChewEODSoldTxt.ReadOnly = true;
            this.ChewEODSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.ChewEODSoldTxt.TabIndex = 19;
            this.ChewEODSoldTxt.Text = "0";
            this.ChewEODSoldTxt.Leave += new System.EventHandler(this.AdditionChewsTxt_Leave);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(272, 55);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(61, 15);
            this.label44.TabIndex = 18;
            this.label44.Text = "EOD Sold";
            // 
            // ChewCountSoldTxt
            // 
            this.ChewCountSoldTxt.Location = new System.Drawing.Point(381, 28);
            this.ChewCountSoldTxt.Name = "ChewCountSoldTxt";
            this.ChewCountSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.ChewCountSoldTxt.TabIndex = 17;
            this.ChewCountSoldTxt.Text = "0";
            this.ChewCountSoldTxt.Leave += new System.EventHandler(this.AdditionChewsTxt_Leave);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(273, 28);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(67, 15);
            this.label45.TabIndex = 16;
            this.label45.Text = "Count Sold";
            // 
            // ChewCloseTxt
            // 
            this.ChewCloseTxt.Location = new System.Drawing.Point(124, 136);
            this.ChewCloseTxt.Name = "ChewCloseTxt";
            this.ChewCloseTxt.Size = new System.Drawing.Size(125, 21);
            this.ChewCloseTxt.TabIndex = 15;
            this.ChewCloseTxt.Text = "0";
            this.ChewCloseTxt.Leave += new System.EventHandler(this.AdditionChewsTxt_Leave);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(16, 136);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(38, 15);
            this.label46.TabIndex = 14;
            this.label46.Text = "Close";
            // 
            // ChewSubTotaltxt
            // 
            this.ChewSubTotaltxt.Location = new System.Drawing.Point(124, 109);
            this.ChewSubTotaltxt.Name = "ChewSubTotaltxt";
            this.ChewSubTotaltxt.ReadOnly = true;
            this.ChewSubTotaltxt.Size = new System.Drawing.Size(125, 21);
            this.ChewSubTotaltxt.TabIndex = 13;
            this.ChewSubTotaltxt.Text = "0";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(16, 109);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(59, 15);
            this.label47.TabIndex = 12;
            this.label47.Text = "Sub Total";
            // 
            // ChewAddTxt
            // 
            this.ChewAddTxt.Location = new System.Drawing.Point(124, 82);
            this.ChewAddTxt.Name = "ChewAddTxt";
            this.ChewAddTxt.ReadOnly = true;
            this.ChewAddTxt.Size = new System.Drawing.Size(125, 21);
            this.ChewAddTxt.TabIndex = 11;
            this.ChewAddTxt.Text = "0";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(16, 82);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(28, 15);
            this.label48.TabIndex = 10;
            this.label48.Text = "Add";
            // 
            // ChewActualopentxt
            // 
            this.ChewActualopentxt.Location = new System.Drawing.Point(124, 55);
            this.ChewActualopentxt.Name = "ChewActualopentxt";
            this.ChewActualopentxt.Size = new System.Drawing.Size(125, 21);
            this.ChewActualopentxt.TabIndex = 9;
            this.ChewActualopentxt.Text = "0";
            this.ChewActualopentxt.Leave += new System.EventHandler(this.AdditionChewsTxt_Leave);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(16, 55);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(73, 15);
            this.label49.TabIndex = 8;
            this.label49.Text = "Actual Open";
            // 
            // ChewOpentxt
            // 
            this.ChewOpentxt.Location = new System.Drawing.Point(124, 28);
            this.ChewOpentxt.Name = "ChewOpentxt";
            this.ChewOpentxt.ReadOnly = true;
            this.ChewOpentxt.Size = new System.Drawing.Size(125, 21);
            this.ChewOpentxt.TabIndex = 7;
            this.ChewOpentxt.Text = "0";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(16, 28);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(37, 15);
            this.label50.TabIndex = 6;
            this.label50.Text = "Open";
            // 
            // Additiongroup
            // 
            this.Additiongroup.Controls.Add(this.AdditionECigarTxt);
            this.Additiongroup.Controls.Add(this.label2);
            this.Additiongroup.Controls.Add(this.AdditionCigars5PackTxt);
            this.Additiongroup.Controls.Add(this.label1);
            this.Additiongroup.Controls.Add(this.AdditionPackTotalTxt);
            this.Additiongroup.Controls.Add(this.label81);
            this.Additiongroup.Controls.Add(this.AdditionRemarksTxt);
            this.Additiongroup.Controls.Add(this.label77);
            this.Additiongroup.Controls.Add(this.AdditionDgv);
            this.Additiongroup.Controls.Add(this.label76);
            this.Additiongroup.Controls.Add(this.AdditionTotalTxt);
            this.Additiongroup.Controls.Add(this.label52);
            this.Additiongroup.Controls.Add(this.AdditionOtherTxt);
            this.Additiongroup.Controls.Add(this.label53);
            this.Additiongroup.Controls.Add(this.AdditionChewsTxt);
            this.Additiongroup.Controls.Add(this.label54);
            this.Additiongroup.Controls.Add(this.AdditionCigars8PackTxt);
            this.Additiongroup.Controls.Add(this.label55);
            this.Additiongroup.Controls.Add(this.AdditionCigars10Packtxt);
            this.Additiongroup.Controls.Add(this.label56);
            this.Additiongroup.Controls.Add(this.Addition10PackTxt);
            this.Additiongroup.Controls.Add(this.label57);
            this.Additiongroup.Controls.Add(this.Addition8Packtxt);
            this.Additiongroup.Controls.Add(this.label58);
            this.Additiongroup.Location = new System.Drawing.Point(13, 156);
            this.Additiongroup.Name = "Additiongroup";
            this.Additiongroup.Size = new System.Drawing.Size(1043, 222);
            this.Additiongroup.TabIndex = 23;
            this.Additiongroup.TabStop = false;
            this.Additiongroup.Text = "Addition";
            // 
            // AdditionECigarTxt
            // 
            this.AdditionECigarTxt.Enabled = false;
            this.AdditionECigarTxt.Location = new System.Drawing.Point(123, 190);
            this.AdditionECigarTxt.Name = "AdditionECigarTxt";
            this.AdditionECigarTxt.Size = new System.Drawing.Size(125, 21);
            this.AdditionECigarTxt.TabIndex = 255;
            this.AdditionECigarTxt.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 15);
            this.label2.TabIndex = 254;
            this.label2.Text = "E-Cigar";
            // 
            // AdditionCigars5PackTxt
            // 
            this.AdditionCigars5PackTxt.Enabled = false;
            this.AdditionCigars5PackTxt.Location = new System.Drawing.Point(123, 136);
            this.AdditionCigars5PackTxt.Name = "AdditionCigars5PackTxt";
            this.AdditionCigars5PackTxt.Size = new System.Drawing.Size(125, 21);
            this.AdditionCigars5PackTxt.TabIndex = 253;
            this.AdditionCigars5PackTxt.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 15);
            this.label1.TabIndex = 252;
            this.label1.Text = "Cigars(5pk)";
            // 
            // AdditionPackTotalTxt
            // 
            this.AdditionPackTotalTxt.Location = new System.Drawing.Point(379, 79);
            this.AdditionPackTotalTxt.Name = "AdditionPackTotalTxt";
            this.AdditionPackTotalTxt.ReadOnly = true;
            this.AdditionPackTotalTxt.Size = new System.Drawing.Size(125, 21);
            this.AdditionPackTotalTxt.TabIndex = 251;
            this.AdditionPackTotalTxt.Text = "0";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(271, 79);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(64, 15);
            this.label81.TabIndex = 250;
            this.label81.Text = "Pack Total";
            // 
            // AdditionRemarksTxt
            // 
            this.AdditionRemarksTxt.Location = new System.Drawing.Point(379, 112);
            this.AdditionRemarksTxt.Name = "AdditionRemarksTxt";
            this.AdditionRemarksTxt.Size = new System.Drawing.Size(205, 45);
            this.AdditionRemarksTxt.TabIndex = 249;
            this.AdditionRemarksTxt.Text = "";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(272, 123);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(57, 15);
            this.label77.TabIndex = 248;
            this.label77.Text = "Remarks";
            // 
            // AdditionDgv
            // 
            this.AdditionDgv.AllowDrop = true;
            this.AdditionDgv.AllowUserToAddRows = false;
            this.AdditionDgv.AllowUserToDeleteRows = false;
            this.AdditionDgv.AllowUserToResizeColumns = false;
            this.AdditionDgv.AllowUserToResizeRows = false;
            this.AdditionDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.AdditionDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AdditionDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.AdditionDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AdditionDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn6});
            this.AdditionDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.AdditionDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.AdditionDgv.Location = new System.Drawing.Point(811, 28);
            this.AdditionDgv.MultiSelect = false;
            this.AdditionDgv.Name = "AdditionDgv";
            this.AdditionDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AdditionDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.AdditionDgv.RowHeadersVisible = false;
            this.AdditionDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.AdditionDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.AdditionDgv.Size = new System.Drawing.Size(222, 105);
            this.AdditionDgv.TabIndex = 246;
            this.AdditionDgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCommon_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn6
            // 
            this.dataGridViewCheckBoxColumn6.HeaderText = "";
            this.dataGridViewCheckBoxColumn6.Name = "dataGridViewCheckBoxColumn6";
            this.dataGridViewCheckBoxColumn6.Width = 5;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.Black;
            this.label76.Location = new System.Drawing.Point(659, 28);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(86, 15);
            this.label76.TabIndex = 247;
            this.label76.Text = "Addition  Docs";
            // 
            // AdditionTotalTxt
            // 
            this.AdditionTotalTxt.Location = new System.Drawing.Point(380, 55);
            this.AdditionTotalTxt.Name = "AdditionTotalTxt";
            this.AdditionTotalTxt.ReadOnly = true;
            this.AdditionTotalTxt.Size = new System.Drawing.Size(125, 21);
            this.AdditionTotalTxt.TabIndex = 19;
            this.AdditionTotalTxt.Text = "0";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(272, 55);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(37, 15);
            this.label52.TabIndex = 18;
            this.label52.Text = " Total";
            // 
            // AdditionOtherTxt
            // 
            this.AdditionOtherTxt.Location = new System.Drawing.Point(381, 28);
            this.AdditionOtherTxt.Name = "AdditionOtherTxt";
            this.AdditionOtherTxt.Size = new System.Drawing.Size(125, 21);
            this.AdditionOtherTxt.TabIndex = 17;
            this.AdditionOtherTxt.Text = "0";
            this.AdditionOtherTxt.Leave += new System.EventHandler(this.Addition8Packtxt_Leave);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(273, 28);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(37, 15);
            this.label53.TabIndex = 16;
            this.label53.Text = "Other";
            // 
            // AdditionChewsTxt
            // 
            this.AdditionChewsTxt.Enabled = false;
            this.AdditionChewsTxt.Location = new System.Drawing.Point(123, 163);
            this.AdditionChewsTxt.Name = "AdditionChewsTxt";
            this.AdditionChewsTxt.Size = new System.Drawing.Size(125, 21);
            this.AdditionChewsTxt.TabIndex = 15;
            this.AdditionChewsTxt.Text = "0";
            this.AdditionChewsTxt.Leave += new System.EventHandler(this.AdditionChewsTxt_Leave);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(15, 163);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(44, 15);
            this.label54.TabIndex = 14;
            this.label54.Text = "Chews";
            // 
            // AdditionCigars8PackTxt
            // 
            this.AdditionCigars8PackTxt.Enabled = false;
            this.AdditionCigars8PackTxt.Location = new System.Drawing.Point(124, 109);
            this.AdditionCigars8PackTxt.Name = "AdditionCigars8PackTxt";
            this.AdditionCigars8PackTxt.Size = new System.Drawing.Size(125, 21);
            this.AdditionCigars8PackTxt.TabIndex = 13;
            this.AdditionCigars8PackTxt.Text = "0";
            this.AdditionCigars8PackTxt.Leave += new System.EventHandler(this.AdditionCigars10Packtxt_Leave);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(16, 109);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(70, 15);
            this.label55.TabIndex = 12;
            this.label55.Text = "Cigars(8pk)";
            // 
            // AdditionCigars10Packtxt
            // 
            this.AdditionCigars10Packtxt.Enabled = false;
            this.AdditionCigars10Packtxt.Location = new System.Drawing.Point(124, 82);
            this.AdditionCigars10Packtxt.Name = "AdditionCigars10Packtxt";
            this.AdditionCigars10Packtxt.Size = new System.Drawing.Size(125, 21);
            this.AdditionCigars10Packtxt.TabIndex = 11;
            this.AdditionCigars10Packtxt.Text = "0";
            this.AdditionCigars10Packtxt.Leave += new System.EventHandler(this.AdditionCigars10Packtxt_Leave);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(16, 82);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(77, 15);
            this.label56.TabIndex = 10;
            this.label56.Text = "Cigars(10pk)";
            // 
            // Addition10PackTxt
            // 
            this.Addition10PackTxt.Enabled = false;
            this.Addition10PackTxt.Location = new System.Drawing.Point(124, 55);
            this.Addition10PackTxt.Name = "Addition10PackTxt";
            this.Addition10PackTxt.Size = new System.Drawing.Size(125, 21);
            this.Addition10PackTxt.TabIndex = 9;
            this.Addition10PackTxt.Text = "0";
            this.Addition10PackTxt.Leave += new System.EventHandler(this.Addition8Packtxt_Leave);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(16, 55);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(51, 15);
            this.label57.TabIndex = 8;
            this.label57.Text = "10 Pack";
            // 
            // Addition8Packtxt
            // 
            this.Addition8Packtxt.Enabled = false;
            this.Addition8Packtxt.Location = new System.Drawing.Point(124, 28);
            this.Addition8Packtxt.Name = "Addition8Packtxt";
            this.Addition8Packtxt.Size = new System.Drawing.Size(125, 21);
            this.Addition8Packtxt.TabIndex = 7;
            this.Addition8Packtxt.Text = "0";
            this.Addition8Packtxt.Leave += new System.EventHandler(this.Addition8Packtxt_Leave);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(16, 28);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(44, 15);
            this.label58.TabIndex = 6;
            this.label58.Text = "8 Pack";
            // 
            // WWFgroup
            // 
            this.WWFgroup.Controls.Add(this.WWFRemarksTxt);
            this.WWFgroup.Controls.Add(this.label79);
            this.WWFgroup.Controls.Add(this.WWFDocsDgv);
            this.WWFgroup.Controls.Add(this.label78);
            this.WWFgroup.Controls.Add(this.WWFBalanceTxt);
            this.WWFgroup.Controls.Add(this.label65);
            this.WWFgroup.Controls.Add(this.WWFExpectedInventoryTxt);
            this.WWFgroup.Controls.Add(this.label51);
            this.WWFgroup.Controls.Add(this.WWFActualInventoryTxt);
            this.WWFgroup.Controls.Add(this.label59);
            this.WWFgroup.Controls.Add(this.WWFUsedOutSideTxt);
            this.WWFgroup.Controls.Add(this.label60);
            this.WWFgroup.Controls.Add(this.WWFSoldTxt);
            this.WWFgroup.Controls.Add(this.label61);
            this.WWFgroup.Controls.Add(this.WWFAddedTxt);
            this.WWFgroup.Controls.Add(this.label62);
            this.WWFgroup.Controls.Add(this.WWFCasesTxt);
            this.WWFgroup.Controls.Add(this.label63);
            this.WWFgroup.Controls.Add(this.WWFLastDatTotalTxt);
            this.WWFgroup.Controls.Add(this.label64);
            this.WWFgroup.Location = new System.Drawing.Point(6, 1406);
            this.WWFgroup.Name = "WWFgroup";
            this.WWFgroup.Size = new System.Drawing.Size(1049, 164);
            this.WWFgroup.TabIndex = 242;
            this.WWFgroup.TabStop = false;
            this.WWFgroup.Text = "WWF Calc";
            // 
            // WWFRemarksTxt
            // 
            this.WWFRemarksTxt.Location = new System.Drawing.Point(380, 106);
            this.WWFRemarksTxt.Name = "WWFRemarksTxt";
            this.WWFRemarksTxt.Size = new System.Drawing.Size(205, 45);
            this.WWFRemarksTxt.TabIndex = 251;
            this.WWFRemarksTxt.Text = "";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(273, 117);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(57, 15);
            this.label79.TabIndex = 250;
            this.label79.Text = "Remarks";
            // 
            // WWFDocsDgv
            // 
            this.WWFDocsDgv.AllowDrop = true;
            this.WWFDocsDgv.AllowUserToAddRows = false;
            this.WWFDocsDgv.AllowUserToDeleteRows = false;
            this.WWFDocsDgv.AllowUserToResizeColumns = false;
            this.WWFDocsDgv.AllowUserToResizeRows = false;
            this.WWFDocsDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.WWFDocsDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.WWFDocsDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.WWFDocsDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.WWFDocsDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn7});
            this.WWFDocsDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.WWFDocsDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.WWFDocsDgv.Location = new System.Drawing.Point(818, 20);
            this.WWFDocsDgv.MultiSelect = false;
            this.WWFDocsDgv.Name = "WWFDocsDgv";
            this.WWFDocsDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.WWFDocsDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.WWFDocsDgv.RowHeadersVisible = false;
            this.WWFDocsDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.WWFDocsDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.WWFDocsDgv.Size = new System.Drawing.Size(222, 105);
            this.WWFDocsDgv.TabIndex = 248;
            this.WWFDocsDgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCommon_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn7
            // 
            this.dataGridViewCheckBoxColumn7.HeaderText = "";
            this.dataGridViewCheckBoxColumn7.Name = "dataGridViewCheckBoxColumn7";
            this.dataGridViewCheckBoxColumn7.Width = 5;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Black;
            this.label78.Location = new System.Drawing.Point(666, 20);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(71, 15);
            this.label78.TabIndex = 249;
            this.label78.Text = "WWF  Docs";
            // 
            // WWFBalanceTxt
            // 
            this.WWFBalanceTxt.Location = new System.Drawing.Point(380, 79);
            this.WWFBalanceTxt.Name = "WWFBalanceTxt";
            this.WWFBalanceTxt.Size = new System.Drawing.Size(125, 21);
            this.WWFBalanceTxt.TabIndex = 243;
            this.WWFBalanceTxt.Text = "0";
            this.WWFBalanceTxt.Leave += new System.EventHandler(this.WWFCasesTxt_Leave);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(272, 79);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(52, 15);
            this.label65.TabIndex = 242;
            this.label65.Text = "Balance";
            // 
            // WWFExpectedInventoryTxt
            // 
            this.WWFExpectedInventoryTxt.Location = new System.Drawing.Point(380, 55);
            this.WWFExpectedInventoryTxt.Name = "WWFExpectedInventoryTxt";
            this.WWFExpectedInventoryTxt.Size = new System.Drawing.Size(125, 21);
            this.WWFExpectedInventoryTxt.TabIndex = 19;
            this.WWFExpectedInventoryTxt.Text = "0";
            this.WWFExpectedInventoryTxt.Leave += new System.EventHandler(this.WWFCasesTxt_Leave);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(272, 55);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(109, 15);
            this.label51.TabIndex = 18;
            this.label51.Text = "Expected Inventory";
            // 
            // WWFActualInventoryTxt
            // 
            this.WWFActualInventoryTxt.Location = new System.Drawing.Point(381, 28);
            this.WWFActualInventoryTxt.Name = "WWFActualInventoryTxt";
            this.WWFActualInventoryTxt.Size = new System.Drawing.Size(125, 21);
            this.WWFActualInventoryTxt.TabIndex = 17;
            this.WWFActualInventoryTxt.Text = "0";
            this.WWFActualInventoryTxt.Leave += new System.EventHandler(this.WWFCasesTxt_Leave);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(273, 28);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(91, 15);
            this.label59.TabIndex = 16;
            this.label59.Text = "Actual Inventory";
            // 
            // WWFUsedOutSideTxt
            // 
            this.WWFUsedOutSideTxt.Location = new System.Drawing.Point(124, 136);
            this.WWFUsedOutSideTxt.Name = "WWFUsedOutSideTxt";
            this.WWFUsedOutSideTxt.Size = new System.Drawing.Size(125, 21);
            this.WWFUsedOutSideTxt.TabIndex = 15;
            this.WWFUsedOutSideTxt.Text = "0";
            this.WWFUsedOutSideTxt.Leave += new System.EventHandler(this.WWFCasesTxt_Leave);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(16, 136);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(79, 15);
            this.label60.TabIndex = 14;
            this.label60.Text = "Used outside";
            // 
            // WWFSoldTxt
            // 
            this.WWFSoldTxt.Location = new System.Drawing.Point(124, 109);
            this.WWFSoldTxt.Name = "WWFSoldTxt";
            this.WWFSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.WWFSoldTxt.TabIndex = 13;
            this.WWFSoldTxt.Text = "0";
            this.WWFSoldTxt.Leave += new System.EventHandler(this.WWFCasesTxt_Leave);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(16, 109);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(32, 15);
            this.label61.TabIndex = 12;
            this.label61.Text = "Sold";
            // 
            // WWFAddedTxt
            // 
            this.WWFAddedTxt.Location = new System.Drawing.Point(124, 82);
            this.WWFAddedTxt.Name = "WWFAddedTxt";
            this.WWFAddedTxt.Size = new System.Drawing.Size(125, 21);
            this.WWFAddedTxt.TabIndex = 11;
            this.WWFAddedTxt.Text = "0";
            this.WWFAddedTxt.Leave += new System.EventHandler(this.WWFCasesTxt_Leave);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(16, 82);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(42, 15);
            this.label62.TabIndex = 10;
            this.label62.Text = "Added";
            // 
            // WWFCasesTxt
            // 
            this.WWFCasesTxt.Location = new System.Drawing.Point(124, 55);
            this.WWFCasesTxt.Name = "WWFCasesTxt";
            this.WWFCasesTxt.Size = new System.Drawing.Size(125, 21);
            this.WWFCasesTxt.TabIndex = 9;
            this.WWFCasesTxt.Text = "0";
            this.WWFCasesTxt.Leave += new System.EventHandler(this.WWFCasesTxt_Leave);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(16, 55);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 15);
            this.label63.TabIndex = 8;
            this.label63.Text = "Cases";
            // 
            // WWFLastDatTotalTxt
            // 
            this.WWFLastDatTotalTxt.Location = new System.Drawing.Point(124, 28);
            this.WWFLastDatTotalTxt.Name = "WWFLastDatTotalTxt";
            this.WWFLastDatTotalTxt.ReadOnly = true;
            this.WWFLastDatTotalTxt.Size = new System.Drawing.Size(125, 21);
            this.WWFLastDatTotalTxt.TabIndex = 7;
            this.WWFLastDatTotalTxt.Text = "0";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(16, 28);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(84, 15);
            this.label64.TabIndex = 6;
            this.label64.Text = "Last Day Total";
            // 
            // CigarGroup
            // 
            this.CigarGroup.Controls.Add(this.CigarActualOpenDifftxt);
            this.CigarGroup.Controls.Add(this.label84);
            this.CigarGroup.Controls.Add(this.CigarRemarksTxt);
            this.CigarGroup.Controls.Add(this.label85);
            this.CigarGroup.Controls.Add(this.CigarDgv);
            this.CigarGroup.Controls.Add(this.label86);
            this.CigarGroup.Controls.Add(this.CigarDiffTxt);
            this.CigarGroup.Controls.Add(this.label87);
            this.CigarGroup.Controls.Add(this.CigarEODSoldTxt);
            this.CigarGroup.Controls.Add(this.label88);
            this.CigarGroup.Controls.Add(this.CigarCountSoldtxt);
            this.CigarGroup.Controls.Add(this.label89);
            this.CigarGroup.Controls.Add(this.CigarCloseTxt);
            this.CigarGroup.Controls.Add(this.label90);
            this.CigarGroup.Controls.Add(this.CigarSubTotaltxt);
            this.CigarGroup.Controls.Add(this.label91);
            this.CigarGroup.Controls.Add(this.CigarAddTxt);
            this.CigarGroup.Controls.Add(this.label92);
            this.CigarGroup.Controls.Add(this.CigarActualopentxt);
            this.CigarGroup.Controls.Add(this.label93);
            this.CigarGroup.Controls.Add(this.CigarOpentxt);
            this.CigarGroup.Controls.Add(this.label94);
            this.CigarGroup.Location = new System.Drawing.Point(6, 996);
            this.CigarGroup.Name = "CigarGroup";
            this.CigarGroup.Size = new System.Drawing.Size(1042, 194);
            this.CigarGroup.TabIndex = 250;
            this.CigarGroup.TabStop = false;
            this.CigarGroup.Text = "Cigar";
            // 
            // CigarActualOpenDifftxt
            // 
            this.CigarActualOpenDifftxt.BackColor = System.Drawing.Color.Red;
            this.CigarActualOpenDifftxt.Location = new System.Drawing.Point(124, 163);
            this.CigarActualOpenDifftxt.Name = "CigarActualOpenDifftxt";
            this.CigarActualOpenDifftxt.ReadOnly = true;
            this.CigarActualOpenDifftxt.Size = new System.Drawing.Size(125, 21);
            this.CigarActualOpenDifftxt.TabIndex = 249;
            this.CigarActualOpenDifftxt.Text = "0";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(16, 163);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(94, 15);
            this.label84.TabIndex = 248;
            this.label84.Text = "Actual Open Diff";
            // 
            // CigarRemarksTxt
            // 
            this.CigarRemarksTxt.Location = new System.Drawing.Point(380, 109);
            this.CigarRemarksTxt.Name = "CigarRemarksTxt";
            this.CigarRemarksTxt.Size = new System.Drawing.Size(205, 45);
            this.CigarRemarksTxt.TabIndex = 247;
            this.CigarRemarksTxt.Text = "";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(273, 120);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(57, 15);
            this.label85.TabIndex = 246;
            this.label85.Text = "Remarks";
            // 
            // CigarDgv
            // 
            this.CigarDgv.AllowDrop = true;
            this.CigarDgv.AllowUserToAddRows = false;
            this.CigarDgv.AllowUserToDeleteRows = false;
            this.CigarDgv.AllowUserToResizeColumns = false;
            this.CigarDgv.AllowUserToResizeRows = false;
            this.CigarDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.CigarDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CigarDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.CigarDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CigarDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn8});
            this.CigarDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.CigarDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.CigarDgv.Location = new System.Drawing.Point(811, 28);
            this.CigarDgv.MultiSelect = false;
            this.CigarDgv.Name = "CigarDgv";
            this.CigarDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CigarDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.CigarDgv.RowHeadersVisible = false;
            this.CigarDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.CigarDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CigarDgv.Size = new System.Drawing.Size(222, 105);
            this.CigarDgv.TabIndex = 244;
            this.CigarDgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCommon_CellContentClick);
            // 
            // dataGridViewCheckBoxColumn8
            // 
            this.dataGridViewCheckBoxColumn8.HeaderText = "";
            this.dataGridViewCheckBoxColumn8.Name = "dataGridViewCheckBoxColumn8";
            this.dataGridViewCheckBoxColumn8.Width = 5;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(659, 28);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(69, 15);
            this.label86.TabIndex = 245;
            this.label86.Text = "Cigar Docs";
            // 
            // CigarDiffTxt
            // 
            this.CigarDiffTxt.BackColor = System.Drawing.Color.Red;
            this.CigarDiffTxt.Location = new System.Drawing.Point(380, 82);
            this.CigarDiffTxt.Name = "CigarDiffTxt";
            this.CigarDiffTxt.ReadOnly = true;
            this.CigarDiffTxt.Size = new System.Drawing.Size(125, 21);
            this.CigarDiffTxt.TabIndex = 21;
            this.CigarDiffTxt.Text = "0";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(272, 82);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(25, 15);
            this.label87.TabIndex = 20;
            this.label87.Text = "Diff";
            // 
            // CigarEODSoldTxt
            // 
            this.CigarEODSoldTxt.Location = new System.Drawing.Point(380, 55);
            this.CigarEODSoldTxt.Name = "CigarEODSoldTxt";
            this.CigarEODSoldTxt.ReadOnly = true;
            this.CigarEODSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.CigarEODSoldTxt.TabIndex = 19;
            this.CigarEODSoldTxt.Text = "0";
            this.CigarEODSoldTxt.Leave += new System.EventHandler(this.AdditionCigars10Packtxt_Leave);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(272, 55);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(61, 15);
            this.label88.TabIndex = 18;
            this.label88.Text = "EOD Sold";
            // 
            // CigarCountSoldtxt
            // 
            this.CigarCountSoldtxt.Location = new System.Drawing.Point(381, 28);
            this.CigarCountSoldtxt.Name = "CigarCountSoldtxt";
            this.CigarCountSoldtxt.Size = new System.Drawing.Size(125, 21);
            this.CigarCountSoldtxt.TabIndex = 17;
            this.CigarCountSoldtxt.Text = "0";
            this.CigarCountSoldtxt.Leave += new System.EventHandler(this.AdditionCigars10Packtxt_Leave);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(273, 28);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(67, 15);
            this.label89.TabIndex = 16;
            this.label89.Text = "Count Sold";
            // 
            // CigarCloseTxt
            // 
            this.CigarCloseTxt.Location = new System.Drawing.Point(124, 136);
            this.CigarCloseTxt.Name = "CigarCloseTxt";
            this.CigarCloseTxt.Size = new System.Drawing.Size(125, 21);
            this.CigarCloseTxt.TabIndex = 15;
            this.CigarCloseTxt.Text = "0";
            this.CigarCloseTxt.Leave += new System.EventHandler(this.AdditionCigars10Packtxt_Leave);
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(16, 136);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(38, 15);
            this.label90.TabIndex = 14;
            this.label90.Text = "Close";
            // 
            // CigarSubTotaltxt
            // 
            this.CigarSubTotaltxt.Location = new System.Drawing.Point(124, 109);
            this.CigarSubTotaltxt.Name = "CigarSubTotaltxt";
            this.CigarSubTotaltxt.ReadOnly = true;
            this.CigarSubTotaltxt.Size = new System.Drawing.Size(125, 21);
            this.CigarSubTotaltxt.TabIndex = 13;
            this.CigarSubTotaltxt.Text = "0";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(16, 109);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(59, 15);
            this.label91.TabIndex = 12;
            this.label91.Text = "Sub Total";
            // 
            // CigarAddTxt
            // 
            this.CigarAddTxt.Location = new System.Drawing.Point(124, 82);
            this.CigarAddTxt.Name = "CigarAddTxt";
            this.CigarAddTxt.ReadOnly = true;
            this.CigarAddTxt.Size = new System.Drawing.Size(125, 21);
            this.CigarAddTxt.TabIndex = 11;
            this.CigarAddTxt.Text = "0";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(16, 82);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(28, 15);
            this.label92.TabIndex = 10;
            this.label92.Text = "Add";
            // 
            // CigarActualopentxt
            // 
            this.CigarActualopentxt.Location = new System.Drawing.Point(124, 55);
            this.CigarActualopentxt.Name = "CigarActualopentxt";
            this.CigarActualopentxt.Size = new System.Drawing.Size(125, 21);
            this.CigarActualopentxt.TabIndex = 9;
            this.CigarActualopentxt.Text = "0";
            this.CigarActualopentxt.Leave += new System.EventHandler(this.AdditionCigars10Packtxt_Leave);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(16, 55);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(73, 15);
            this.label93.TabIndex = 8;
            this.label93.Text = "Actual Open";
            // 
            // CigarOpentxt
            // 
            this.CigarOpentxt.Location = new System.Drawing.Point(124, 28);
            this.CigarOpentxt.Name = "CigarOpentxt";
            this.CigarOpentxt.ReadOnly = true;
            this.CigarOpentxt.Size = new System.Drawing.Size(125, 21);
            this.CigarOpentxt.TabIndex = 7;
            this.CigarOpentxt.Text = "0";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(16, 28);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(37, 15);
            this.label94.TabIndex = 6;
            this.label94.Text = "Open";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(20, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 218;
            this.label3.Text = "Date";
            // 
            // DateCalender
            // 
            this.DateCalender.CustomFormat = "MMM/dd/yyyy";
            this.DateCalender.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateCalender.Location = new System.Drawing.Point(131, 20);
            this.DateCalender.Name = "DateCalender";
            this.DateCalender.Size = new System.Drawing.Size(202, 21);
            this.DateCalender.TabIndex = 219;
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(344, 47);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 231;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // ViewBtn
            // 
            this.ViewBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ViewBtn.FlatAppearance.BorderSize = 2;
            this.ViewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewBtn.Location = new System.Drawing.Point(129, 47);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(90, 32);
            this.ViewBtn.TabIndex = 232;
            this.ViewBtn.Text = "&View";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SaveBtn.FlatAppearance.BorderSize = 2;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.Location = new System.Drawing.Point(241, 47);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(88, 32);
            this.SaveBtn.TabIndex = 233;
            this.SaveBtn.Text = "&Save";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SaveBtn);
            this.groupBox1.Controls.Add(this.ViewBtn);
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.DateCalender);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1062, 140);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Master";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ECigarActualOpenDiffTxt);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.ECigarRemarksTxt);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.ECigarDgv);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.ECigarDiffTxt);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.ECigarEODSoldTxt);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.ECigarCountSoldTxt);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.ECigarCloseTxt);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.ECigarSubTotalTxt);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.ECigarAddTxt);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.ECigarActualOpenTxt);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.ECigarOpenTxt);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Location = new System.Drawing.Point(6, 1206);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1042, 194);
            this.groupBox2.TabIndex = 251;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "E-Cigar";
            // 
            // ECigarActualOpenDiffTxt
            // 
            this.ECigarActualOpenDiffTxt.BackColor = System.Drawing.Color.Red;
            this.ECigarActualOpenDiffTxt.Location = new System.Drawing.Point(124, 163);
            this.ECigarActualOpenDiffTxt.Name = "ECigarActualOpenDiffTxt";
            this.ECigarActualOpenDiffTxt.ReadOnly = true;
            this.ECigarActualOpenDiffTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarActualOpenDiffTxt.TabIndex = 249;
            this.ECigarActualOpenDiffTxt.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 15);
            this.label4.TabIndex = 248;
            this.label4.Text = "Actual Open Diff";
            // 
            // ECigarRemarksTxt
            // 
            this.ECigarRemarksTxt.Location = new System.Drawing.Point(380, 109);
            this.ECigarRemarksTxt.Name = "ECigarRemarksTxt";
            this.ECigarRemarksTxt.Size = new System.Drawing.Size(205, 45);
            this.ECigarRemarksTxt.TabIndex = 247;
            this.ECigarRemarksTxt.Text = "";
            this.ECigarRemarksTxt.Leave += new System.EventHandler(this.ECigarActualOpenTxt_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(273, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 15);
            this.label5.TabIndex = 246;
            this.label5.Text = "Remarks";
            // 
            // ECigarDgv
            // 
            this.ECigarDgv.AllowDrop = true;
            this.ECigarDgv.AllowUserToAddRows = false;
            this.ECigarDgv.AllowUserToDeleteRows = false;
            this.ECigarDgv.AllowUserToResizeColumns = false;
            this.ECigarDgv.AllowUserToResizeRows = false;
            this.ECigarDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ECigarDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ECigarDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.ECigarDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ECigarDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1});
            this.ECigarDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ECigarDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.ECigarDgv.Location = new System.Drawing.Point(811, 28);
            this.ECigarDgv.MultiSelect = false;
            this.ECigarDgv.Name = "ECigarDgv";
            this.ECigarDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ECigarDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.ECigarDgv.RowHeadersVisible = false;
            this.ECigarDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ECigarDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ECigarDgv.Size = new System.Drawing.Size(222, 105);
            this.ECigarDgv.TabIndex = 244;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(659, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 15);
            this.label6.TabIndex = 245;
            this.label6.Text = "E-Cigar Docs";
            // 
            // ECigarDiffTxt
            // 
            this.ECigarDiffTxt.BackColor = System.Drawing.Color.Red;
            this.ECigarDiffTxt.Location = new System.Drawing.Point(380, 82);
            this.ECigarDiffTxt.Name = "ECigarDiffTxt";
            this.ECigarDiffTxt.ReadOnly = true;
            this.ECigarDiffTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarDiffTxt.TabIndex = 21;
            this.ECigarDiffTxt.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(272, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 15);
            this.label7.TabIndex = 20;
            this.label7.Text = "Diff";
            // 
            // ECigarEODSoldTxt
            // 
            this.ECigarEODSoldTxt.Location = new System.Drawing.Point(380, 55);
            this.ECigarEODSoldTxt.Name = "ECigarEODSoldTxt";
            this.ECigarEODSoldTxt.ReadOnly = true;
            this.ECigarEODSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarEODSoldTxt.TabIndex = 19;
            this.ECigarEODSoldTxt.Text = "0";
            this.ECigarEODSoldTxt.Leave += new System.EventHandler(this.ECigarActualOpenTxt_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(272, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 15);
            this.label8.TabIndex = 18;
            this.label8.Text = "EOD Sold";
            // 
            // ECigarCountSoldTxt
            // 
            this.ECigarCountSoldTxt.Location = new System.Drawing.Point(381, 28);
            this.ECigarCountSoldTxt.Name = "ECigarCountSoldTxt";
            this.ECigarCountSoldTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarCountSoldTxt.TabIndex = 17;
            this.ECigarCountSoldTxt.Text = "0";
            this.ECigarCountSoldTxt.Leave += new System.EventHandler(this.ECigarActualOpenTxt_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(273, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 16;
            this.label9.Text = "Count Sold";
            // 
            // ECigarCloseTxt
            // 
            this.ECigarCloseTxt.Location = new System.Drawing.Point(124, 136);
            this.ECigarCloseTxt.Name = "ECigarCloseTxt";
            this.ECigarCloseTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarCloseTxt.TabIndex = 15;
            this.ECigarCloseTxt.Text = "0";
            this.ECigarCloseTxt.Leave += new System.EventHandler(this.ECigarActualOpenTxt_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 136);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 15);
            this.label10.TabIndex = 14;
            this.label10.Text = "Close";
            // 
            // ECigarSubTotalTxt
            // 
            this.ECigarSubTotalTxt.Location = new System.Drawing.Point(124, 109);
            this.ECigarSubTotalTxt.Name = "ECigarSubTotalTxt";
            this.ECigarSubTotalTxt.ReadOnly = true;
            this.ECigarSubTotalTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarSubTotalTxt.TabIndex = 13;
            this.ECigarSubTotalTxt.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 15);
            this.label11.TabIndex = 12;
            this.label11.Text = "Sub Total";
            // 
            // ECigarAddTxt
            // 
            this.ECigarAddTxt.Location = new System.Drawing.Point(124, 82);
            this.ECigarAddTxt.Name = "ECigarAddTxt";
            this.ECigarAddTxt.ReadOnly = true;
            this.ECigarAddTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarAddTxt.TabIndex = 11;
            this.ECigarAddTxt.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 82);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 15);
            this.label12.TabIndex = 10;
            this.label12.Text = "Add";
            // 
            // ECigarActualOpenTxt
            // 
            this.ECigarActualOpenTxt.Location = new System.Drawing.Point(124, 55);
            this.ECigarActualOpenTxt.Name = "ECigarActualOpenTxt";
            this.ECigarActualOpenTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarActualOpenTxt.TabIndex = 9;
            this.ECigarActualOpenTxt.Text = "0";
            this.ECigarActualOpenTxt.Leave += new System.EventHandler(this.ECigarActualOpenTxt_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 15);
            this.label13.TabIndex = 8;
            this.label13.Text = "Actual Open";
            // 
            // ECigarOpenTxt
            // 
            this.ECigarOpenTxt.Location = new System.Drawing.Point(124, 28);
            this.ECigarOpenTxt.Name = "ECigarOpenTxt";
            this.ECigarOpenTxt.ReadOnly = true;
            this.ECigarOpenTxt.Size = new System.Drawing.Size(125, 21);
            this.ECigarOpenTxt.TabIndex = 7;
            this.ECigarOpenTxt.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(16, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 15);
            this.label14.TabIndex = 6;
            this.label14.Text = "Open";
            // 
            // EODManagementForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1098, 749);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.CigarGroup);
            this.Controls.Add(this.WWFgroup);
            this.Controls.Add(this.Additiongroup);
            this.Controls.Add(this.Chewgroup);
            this.Controls.Add(this.CigrateesGroup);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(1114, 726);
            this.Name = "EODManagementForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EOD Inventory Management";
            this.Load += new System.EventHandler(this.MasterInventoryManagementForm_Load);
            this.CigrateesGroup.ResumeLayout(false);
            this.CigrateesGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CigrateesDocsDgv)).EndInit();
            this.Chewgroup.ResumeLayout(false);
            this.Chewgroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChewsDgv)).EndInit();
            this.Additiongroup.ResumeLayout(false);
            this.Additiongroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AdditionDgv)).EndInit();
            this.WWFgroup.ResumeLayout(false);
            this.WWFgroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WWFDocsDgv)).EndInit();
            this.CigarGroup.ResumeLayout(false);
            this.CigarGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CigarDgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ECigarDgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox CigrateesGroup;
        private System.Windows.Forms.TextBox CigrateesActualOpenTxt;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox CigrateesOpentxt;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox CigrateesAddTxt;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox CigrateesCloseTxt;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox CigrateesSubTotaltxt;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox CigrateesDiffTxt;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox CigrateesEODSoldTxt;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox CigrateesCountSoldTxt;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox Chewgroup;
        private System.Windows.Forms.TextBox ChewDifftxt;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox ChewEODSoldTxt;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox ChewCountSoldTxt;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox ChewCloseTxt;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox ChewSubTotaltxt;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox ChewAddTxt;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox ChewActualopentxt;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox ChewOpentxt;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.GroupBox Additiongroup;
        private System.Windows.Forms.TextBox AdditionTotalTxt;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox AdditionOtherTxt;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox AdditionChewsTxt;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox AdditionCigars8PackTxt;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox AdditionCigars10Packtxt;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox Addition10PackTxt;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox Addition8Packtxt;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.GroupBox WWFgroup;
        private System.Windows.Forms.TextBox WWFExpectedInventoryTxt;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox WWFActualInventoryTxt;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox WWFUsedOutSideTxt;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox WWFSoldTxt;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox WWFAddedTxt;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox WWFCasesTxt;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox WWFLastDatTotalTxt;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox WWFBalanceTxt;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.DataGridView CigrateesDocsDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.DataGridView ChewsDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.RichTextBox CigrateesRemarksTxt;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.RichTextBox ChewsRemarksTxt;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.DataGridView AdditionDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn6;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.RichTextBox AdditionRemarksTxt;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.DataGridView WWFDocsDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn7;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.RichTextBox WWFRemarksTxt;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox AdditionPackTotalTxt;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox ActualOpenDiffTxt;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox ChewActualOpenDiffTxt;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.GroupBox CigarGroup;
        private System.Windows.Forms.TextBox CigarActualOpenDifftxt;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.RichTextBox CigarRemarksTxt;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.DataGridView CigarDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn8;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.TextBox CigarDiffTxt;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.TextBox CigarEODSoldTxt;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox CigarCountSoldtxt;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.TextBox CigarCloseTxt;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TextBox CigarSubTotaltxt;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox CigarAddTxt;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox CigarActualopentxt;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TextBox CigarOpentxt;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker DateCalender;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox AdditionCigars5PackTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AdditionECigarTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox ECigarActualOpenDiffTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox ECigarRemarksTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView ECigarDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ECigarDiffTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox ECigarEODSoldTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox ECigarCountSoldTxt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ECigarCloseTxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox ECigarSubTotalTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox ECigarAddTxt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox ECigarActualOpenTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox ECigarOpenTxt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox EODTobaccoReportCigarSoldTxt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox EODTobaccoReportChewSoldTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox EODTobaccoReportOtherSoldTxt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox EODTobaccoReportLighterSoldTxt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox EODTobaccoReportECigarSoldTxt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox EODTobaccoReportTotalSoldTxt;
        private System.Windows.Forms.Label label20;
    }
}