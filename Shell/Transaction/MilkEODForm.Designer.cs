﻿namespace Shell.Transaction
{
    partial class MilkEODForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle101 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle102 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle103 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle104 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle105 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle106 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle107 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle108 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.MilkAddition4LTextBox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.MilkAdditionRemarksTextBox = new System.Windows.Forms.RichTextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.MilkAdditionDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label25 = new System.Windows.Forms.Label();
            this.MilkAdditionTotalTextBox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.MilkAdditionOtherTextBox = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.MilkAddition2LTextBox = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.MilkAddition1LTextBox = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.MilkAddition473mlTextBox = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.MilkAddition310mlTextBox = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Milk310mlActualOpenDiffTextBox = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.Milk310mlRemarksTextBox = new System.Windows.Forms.RichTextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.Milk310mlDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label66 = new System.Windows.Forms.Label();
            this.Milk310mlDiffTextBox = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.Milk310mlEODSoldTextBox = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.Milk310mlCountSoldTextBox = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.Milk310mlCloseTextBox = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.Milk310mlSubTotalTextBox = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.Milk310mlAddTextBox = new System.Windows.Forms.TextBox();
            this.label80 = new System.Windows.Forms.Label();
            this.Milk310mlActualOpenTextBox = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.Milk310mlOpenTextBox = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Milk473mlActualOpenDiffTextBox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Milk473mlRemarksTextBox = new System.Windows.Forms.RichTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.Milk473mlDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn9 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label27 = new System.Windows.Forms.Label();
            this.Milk473mlDiffTextBox = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.Milk473mlEODSoldTextBox = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.Milk473mlCountSoldTextBox = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.Milk473mlCloseTextBox = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.Milk473mlSubTotalTextBox = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.Milk473mlAddTextBox = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.Milk473mlActualOpenTextBox = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.Milk473mlOpenTextBox = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Milk1LActualOpenDiffTextBox = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.Milk1LRemarksTextBox = new System.Windows.Forms.RichTextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.Milk1LDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label107 = new System.Windows.Forms.Label();
            this.Milk1LDiffTextBox = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.Milk1LEODSoldTextBox = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.Milk1LCountSoldTextBox = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.Milk1LCloseTextBox = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.Milk1LSubTotalTextBox = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.Milk1LAddTextBox = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.Milk1LActualOpenTextBox = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.Milk1LOpenTextBox = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Milk2LActualOpenDiffTextBox = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.Milk2LRemarksTextBox = new System.Windows.Forms.RichTextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.Milk2LDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn11 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label118 = new System.Windows.Forms.Label();
            this.Milk2LDiffTextBox = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.Milk2LEODSoldTextBox = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.Milk2LCountSoldTextBox = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.Milk2LCloseTextBox = new System.Windows.Forms.TextBox();
            this.label122 = new System.Windows.Forms.Label();
            this.Milk2LSubTotalTextBox = new System.Windows.Forms.TextBox();
            this.label123 = new System.Windows.Forms.Label();
            this.Milk2LAddTextBox = new System.Windows.Forms.TextBox();
            this.label124 = new System.Windows.Forms.Label();
            this.Milk2LActualOpenTextBox = new System.Windows.Forms.TextBox();
            this.label125 = new System.Windows.Forms.Label();
            this.Milk2LOpenTextBox = new System.Windows.Forms.TextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Milk4LActualOpenDiffTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Milk4LRemarksTextBox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Milk4LDGV = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.Milk4LDiffTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Milk4LEODSoldTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Milk4LCountSoldTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Milk4LCloseTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Milk4LSubTotalTextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Milk4LAddTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Milk4LActualOpenTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Milk4LOpenTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.DateCalender = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MilkAdditionDgv)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk310mlDGV)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk473mlDGV)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk1LDGV)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk2LDGV)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk4LDGV)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.MilkAddition4LTextBox);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.MilkAdditionRemarksTextBox);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.MilkAdditionDgv);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.MilkAdditionTotalTextBox);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.MilkAdditionOtherTextBox);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.MilkAddition2LTextBox);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.MilkAddition1LTextBox);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.MilkAddition473mlTextBox);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.MilkAddition310mlTextBox);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Location = new System.Drawing.Point(11, 179);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1043, 222);
            this.groupBox3.TabIndex = 253;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Milk Addition";
            // 
            // MilkAddition4LTextBox
            // 
            this.MilkAddition4LTextBox.Location = new System.Drawing.Point(123, 136);
            this.MilkAddition4LTextBox.Name = "MilkAddition4LTextBox";
            this.MilkAddition4LTextBox.Size = new System.Drawing.Size(125, 20);
            this.MilkAddition4LTextBox.TabIndex = 253;
            this.MilkAddition4LTextBox.Text = "0";
            this.MilkAddition4LTextBox.Leave += new System.EventHandler(this.MilkAddition310mlTextBox_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 136);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(22, 13);
            this.label22.TabIndex = 252;
            this.label22.Text = "4 L";
            // 
            // MilkAdditionRemarksTextBox
            // 
            this.MilkAdditionRemarksTextBox.Location = new System.Drawing.Point(811, 139);
            this.MilkAdditionRemarksTextBox.Name = "MilkAdditionRemarksTextBox";
            this.MilkAdditionRemarksTextBox.Size = new System.Drawing.Size(222, 66);
            this.MilkAdditionRemarksTextBox.TabIndex = 249;
            this.MilkAdditionRemarksTextBox.Text = "";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(659, 154);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(49, 13);
            this.label24.TabIndex = 248;
            this.label24.Text = "Remarks";
            // 
            // MilkAdditionDgv
            // 
            this.MilkAdditionDgv.AllowDrop = true;
            this.MilkAdditionDgv.AllowUserToAddRows = false;
            this.MilkAdditionDgv.AllowUserToDeleteRows = false;
            this.MilkAdditionDgv.AllowUserToResizeColumns = false;
            this.MilkAdditionDgv.AllowUserToResizeRows = false;
            this.MilkAdditionDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.MilkAdditionDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle97.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle97.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle97.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle97.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle97.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MilkAdditionDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle97;
            this.MilkAdditionDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MilkAdditionDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn2});
            this.MilkAdditionDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.MilkAdditionDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.MilkAdditionDgv.Location = new System.Drawing.Point(811, 28);
            this.MilkAdditionDgv.MultiSelect = false;
            this.MilkAdditionDgv.Name = "MilkAdditionDgv";
            this.MilkAdditionDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle98.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle98.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle98.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle98.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle98.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle98.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MilkAdditionDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle98;
            this.MilkAdditionDgv.RowHeadersVisible = false;
            this.MilkAdditionDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.MilkAdditionDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.MilkAdditionDgv.Size = new System.Drawing.Size(222, 105);
            this.MilkAdditionDgv.TabIndex = 246;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 5;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(659, 28);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(109, 15);
            this.label25.TabIndex = 247;
            this.label25.Text = "Milk Addition  Docs";
            // 
            // MilkAdditionTotalTextBox
            // 
            this.MilkAdditionTotalTextBox.Location = new System.Drawing.Point(123, 190);
            this.MilkAdditionTotalTextBox.Name = "MilkAdditionTotalTextBox";
            this.MilkAdditionTotalTextBox.ReadOnly = true;
            this.MilkAdditionTotalTextBox.Size = new System.Drawing.Size(125, 20);
            this.MilkAdditionTotalTextBox.TabIndex = 19;
            this.MilkAdditionTotalTextBox.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(15, 190);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 13);
            this.label26.TabIndex = 18;
            this.label26.Text = " Total";
            // 
            // MilkAdditionOtherTextBox
            // 
            this.MilkAdditionOtherTextBox.Location = new System.Drawing.Point(123, 163);
            this.MilkAdditionOtherTextBox.Name = "MilkAdditionOtherTextBox";
            this.MilkAdditionOtherTextBox.Size = new System.Drawing.Size(125, 20);
            this.MilkAdditionOtherTextBox.TabIndex = 15;
            this.MilkAdditionOtherTextBox.Text = "0";
            this.MilkAdditionOtherTextBox.Leave += new System.EventHandler(this.MilkAddition310mlTextBox_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(15, 163);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(33, 13);
            this.label28.TabIndex = 14;
            this.label28.Text = "Other";
            // 
            // MilkAddition2LTextBox
            // 
            this.MilkAddition2LTextBox.Location = new System.Drawing.Point(124, 109);
            this.MilkAddition2LTextBox.Name = "MilkAddition2LTextBox";
            this.MilkAddition2LTextBox.Size = new System.Drawing.Size(125, 20);
            this.MilkAddition2LTextBox.TabIndex = 13;
            this.MilkAddition2LTextBox.Text = "0";
            this.MilkAddition2LTextBox.Leave += new System.EventHandler(this.MilkAddition310mlTextBox_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(16, 109);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(22, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "2 L";
            // 
            // MilkAddition1LTextBox
            // 
            this.MilkAddition1LTextBox.Location = new System.Drawing.Point(124, 82);
            this.MilkAddition1LTextBox.Name = "MilkAddition1LTextBox";
            this.MilkAddition1LTextBox.Size = new System.Drawing.Size(125, 20);
            this.MilkAddition1LTextBox.TabIndex = 11;
            this.MilkAddition1LTextBox.Text = "0";
            this.MilkAddition1LTextBox.Leave += new System.EventHandler(this.MilkAddition310mlTextBox_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(16, 82);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(22, 13);
            this.label30.TabIndex = 10;
            this.label30.Text = "1 L";
            // 
            // MilkAddition473mlTextBox
            // 
            this.MilkAddition473mlTextBox.Location = new System.Drawing.Point(124, 55);
            this.MilkAddition473mlTextBox.Name = "MilkAddition473mlTextBox";
            this.MilkAddition473mlTextBox.Size = new System.Drawing.Size(125, 20);
            this.MilkAddition473mlTextBox.TabIndex = 9;
            this.MilkAddition473mlTextBox.Text = "0";
            this.MilkAddition473mlTextBox.Leave += new System.EventHandler(this.MilkAddition310mlTextBox_Leave);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 55);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 8;
            this.label31.Text = "473 ml";
            // 
            // MilkAddition310mlTextBox
            // 
            this.MilkAddition310mlTextBox.Location = new System.Drawing.Point(124, 28);
            this.MilkAddition310mlTextBox.Name = "MilkAddition310mlTextBox";
            this.MilkAddition310mlTextBox.Size = new System.Drawing.Size(125, 20);
            this.MilkAddition310mlTextBox.TabIndex = 7;
            this.MilkAddition310mlTextBox.Text = "0";
            this.MilkAddition310mlTextBox.Leave += new System.EventHandler(this.MilkAddition310mlTextBox_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(16, 28);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(38, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "355 ml";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Milk310mlActualOpenDiffTextBox);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.Milk310mlRemarksTextBox);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Controls.Add(this.Milk310mlDGV);
            this.groupBox4.Controls.Add(this.label66);
            this.groupBox4.Controls.Add(this.Milk310mlDiffTextBox);
            this.groupBox4.Controls.Add(this.label67);
            this.groupBox4.Controls.Add(this.Milk310mlEODSoldTextBox);
            this.groupBox4.Controls.Add(this.label68);
            this.groupBox4.Controls.Add(this.Milk310mlCountSoldTextBox);
            this.groupBox4.Controls.Add(this.label69);
            this.groupBox4.Controls.Add(this.Milk310mlCloseTextBox);
            this.groupBox4.Controls.Add(this.label70);
            this.groupBox4.Controls.Add(this.Milk310mlSubTotalTextBox);
            this.groupBox4.Controls.Add(this.label71);
            this.groupBox4.Controls.Add(this.Milk310mlAddTextBox);
            this.groupBox4.Controls.Add(this.label80);
            this.groupBox4.Controls.Add(this.Milk310mlActualOpenTextBox);
            this.groupBox4.Controls.Add(this.label95);
            this.groupBox4.Controls.Add(this.Milk310mlOpenTextBox);
            this.groupBox4.Controls.Add(this.label96);
            this.groupBox4.Location = new System.Drawing.Point(12, 427);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1042, 194);
            this.groupBox4.TabIndex = 257;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "355 ml";
            // 
            // Milk310mlActualOpenDiffTextBox
            // 
            this.Milk310mlActualOpenDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk310mlActualOpenDiffTextBox.Location = new System.Drawing.Point(124, 163);
            this.Milk310mlActualOpenDiffTextBox.Name = "Milk310mlActualOpenDiffTextBox";
            this.Milk310mlActualOpenDiffTextBox.ReadOnly = true;
            this.Milk310mlActualOpenDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlActualOpenDiffTextBox.TabIndex = 249;
            this.Milk310mlActualOpenDiffTextBox.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(16, 163);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 13);
            this.label33.TabIndex = 248;
            this.label33.Text = "Actual Open Diff";
            // 
            // Milk310mlRemarksTextBox
            // 
            this.Milk310mlRemarksTextBox.Location = new System.Drawing.Point(380, 109);
            this.Milk310mlRemarksTextBox.Name = "Milk310mlRemarksTextBox";
            this.Milk310mlRemarksTextBox.Size = new System.Drawing.Size(205, 45);
            this.Milk310mlRemarksTextBox.TabIndex = 247;
            this.Milk310mlRemarksTextBox.Text = "";
            this.Milk310mlRemarksTextBox.Leave += new System.EventHandler(this.Milk310mlActualOpenTextBox_Leave);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(273, 120);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(49, 13);
            this.label34.TabIndex = 246;
            this.label34.Text = "Remarks";
            // 
            // Milk310mlDGV
            // 
            this.Milk310mlDGV.AllowDrop = true;
            this.Milk310mlDGV.AllowUserToAddRows = false;
            this.Milk310mlDGV.AllowUserToDeleteRows = false;
            this.Milk310mlDGV.AllowUserToResizeColumns = false;
            this.Milk310mlDGV.AllowUserToResizeRows = false;
            this.Milk310mlDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.Milk310mlDGV.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle99.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle99.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle99.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle99.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle99.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle99.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk310mlDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle99;
            this.Milk310mlDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Milk310mlDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn3});
            this.Milk310mlDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.Milk310mlDGV.GridColor = System.Drawing.SystemColors.Desktop;
            this.Milk310mlDGV.Location = new System.Drawing.Point(811, 28);
            this.Milk310mlDGV.MultiSelect = false;
            this.Milk310mlDGV.Name = "Milk310mlDGV";
            this.Milk310mlDGV.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle100.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle100.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle100.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle100.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle100.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk310mlDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle100;
            this.Milk310mlDGV.RowHeadersVisible = false;
            this.Milk310mlDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Milk310mlDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Milk310mlDGV.Size = new System.Drawing.Size(222, 105);
            this.Milk310mlDGV.TabIndex = 244;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 5;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(659, 28);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(77, 15);
            this.label66.TabIndex = 245;
            this.label66.Text = "355 ml Docs";
            // 
            // Milk310mlDiffTextBox
            // 
            this.Milk310mlDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk310mlDiffTextBox.Location = new System.Drawing.Point(380, 82);
            this.Milk310mlDiffTextBox.Name = "Milk310mlDiffTextBox";
            this.Milk310mlDiffTextBox.ReadOnly = true;
            this.Milk310mlDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlDiffTextBox.TabIndex = 21;
            this.Milk310mlDiffTextBox.Text = "0";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(272, 82);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(23, 13);
            this.label67.TabIndex = 20;
            this.label67.Text = "Diff";
            // 
            // Milk310mlEODSoldTextBox
            // 
            this.Milk310mlEODSoldTextBox.Location = new System.Drawing.Point(380, 55);
            this.Milk310mlEODSoldTextBox.Name = "Milk310mlEODSoldTextBox";
            this.Milk310mlEODSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlEODSoldTextBox.TabIndex = 19;
            this.Milk310mlEODSoldTextBox.Text = "0";
            this.Milk310mlEODSoldTextBox.Leave += new System.EventHandler(this.Milk310mlActualOpenTextBox_Leave);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(272, 55);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(54, 13);
            this.label68.TabIndex = 18;
            this.label68.Text = "EOD Sold";
            // 
            // Milk310mlCountSoldTextBox
            // 
            this.Milk310mlCountSoldTextBox.Location = new System.Drawing.Point(381, 28);
            this.Milk310mlCountSoldTextBox.Name = "Milk310mlCountSoldTextBox";
            this.Milk310mlCountSoldTextBox.ReadOnly = true;
            this.Milk310mlCountSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlCountSoldTextBox.TabIndex = 17;
            this.Milk310mlCountSoldTextBox.Text = "0";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(273, 28);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(59, 13);
            this.label69.TabIndex = 16;
            this.label69.Text = "Count Sold";
            // 
            // Milk310mlCloseTextBox
            // 
            this.Milk310mlCloseTextBox.Location = new System.Drawing.Point(124, 136);
            this.Milk310mlCloseTextBox.Name = "Milk310mlCloseTextBox";
            this.Milk310mlCloseTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlCloseTextBox.TabIndex = 15;
            this.Milk310mlCloseTextBox.Text = "0";
            this.Milk310mlCloseTextBox.Leave += new System.EventHandler(this.Milk310mlActualOpenTextBox_Leave);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(16, 136);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(33, 13);
            this.label70.TabIndex = 14;
            this.label70.Text = "Close";
            // 
            // Milk310mlSubTotalTextBox
            // 
            this.Milk310mlSubTotalTextBox.Location = new System.Drawing.Point(124, 109);
            this.Milk310mlSubTotalTextBox.Name = "Milk310mlSubTotalTextBox";
            this.Milk310mlSubTotalTextBox.ReadOnly = true;
            this.Milk310mlSubTotalTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlSubTotalTextBox.TabIndex = 13;
            this.Milk310mlSubTotalTextBox.Text = "0";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(16, 109);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(53, 13);
            this.label71.TabIndex = 12;
            this.label71.Text = "Sub Total";
            // 
            // Milk310mlAddTextBox
            // 
            this.Milk310mlAddTextBox.Location = new System.Drawing.Point(124, 82);
            this.Milk310mlAddTextBox.Name = "Milk310mlAddTextBox";
            this.Milk310mlAddTextBox.ReadOnly = true;
            this.Milk310mlAddTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlAddTextBox.TabIndex = 11;
            this.Milk310mlAddTextBox.Text = "0";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(16, 82);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(26, 13);
            this.label80.TabIndex = 10;
            this.label80.Text = "Add";
            // 
            // Milk310mlActualOpenTextBox
            // 
            this.Milk310mlActualOpenTextBox.Location = new System.Drawing.Point(124, 55);
            this.Milk310mlActualOpenTextBox.Name = "Milk310mlActualOpenTextBox";
            this.Milk310mlActualOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlActualOpenTextBox.TabIndex = 9;
            this.Milk310mlActualOpenTextBox.Text = "0";
            this.Milk310mlActualOpenTextBox.Leave += new System.EventHandler(this.Milk310mlActualOpenTextBox_Leave);
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(16, 55);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(66, 13);
            this.label95.TabIndex = 8;
            this.label95.Text = "Actual Open";
            // 
            // Milk310mlOpenTextBox
            // 
            this.Milk310mlOpenTextBox.Location = new System.Drawing.Point(124, 28);
            this.Milk310mlOpenTextBox.Name = "Milk310mlOpenTextBox";
            this.Milk310mlOpenTextBox.ReadOnly = true;
            this.Milk310mlOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk310mlOpenTextBox.TabIndex = 7;
            this.Milk310mlOpenTextBox.Text = "0";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(16, 28);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(33, 13);
            this.label96.TabIndex = 6;
            this.label96.Text = "Open";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Milk473mlActualOpenDiffTextBox);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.Milk473mlRemarksTextBox);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.Milk473mlDGV);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.Milk473mlDiffTextBox);
            this.groupBox5.Controls.Add(this.label97);
            this.groupBox5.Controls.Add(this.Milk473mlEODSoldTextBox);
            this.groupBox5.Controls.Add(this.label98);
            this.groupBox5.Controls.Add(this.Milk473mlCountSoldTextBox);
            this.groupBox5.Controls.Add(this.label99);
            this.groupBox5.Controls.Add(this.Milk473mlCloseTextBox);
            this.groupBox5.Controls.Add(this.label100);
            this.groupBox5.Controls.Add(this.Milk473mlSubTotalTextBox);
            this.groupBox5.Controls.Add(this.label101);
            this.groupBox5.Controls.Add(this.Milk473mlAddTextBox);
            this.groupBox5.Controls.Add(this.label102);
            this.groupBox5.Controls.Add(this.Milk473mlActualOpenTextBox);
            this.groupBox5.Controls.Add(this.label103);
            this.groupBox5.Controls.Add(this.Milk473mlOpenTextBox);
            this.groupBox5.Controls.Add(this.label104);
            this.groupBox5.Location = new System.Drawing.Point(12, 627);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1042, 194);
            this.groupBox5.TabIndex = 258;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "473 ml";
            // 
            // Milk473mlActualOpenDiffTextBox
            // 
            this.Milk473mlActualOpenDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk473mlActualOpenDiffTextBox.Location = new System.Drawing.Point(124, 163);
            this.Milk473mlActualOpenDiffTextBox.Name = "Milk473mlActualOpenDiffTextBox";
            this.Milk473mlActualOpenDiffTextBox.ReadOnly = true;
            this.Milk473mlActualOpenDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlActualOpenDiffTextBox.TabIndex = 249;
            this.Milk473mlActualOpenDiffTextBox.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 163);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(85, 13);
            this.label21.TabIndex = 248;
            this.label21.Text = "Actual Open Diff";
            // 
            // Milk473mlRemarksTextBox
            // 
            this.Milk473mlRemarksTextBox.Location = new System.Drawing.Point(380, 109);
            this.Milk473mlRemarksTextBox.Name = "Milk473mlRemarksTextBox";
            this.Milk473mlRemarksTextBox.Size = new System.Drawing.Size(205, 45);
            this.Milk473mlRemarksTextBox.TabIndex = 247;
            this.Milk473mlRemarksTextBox.Text = "";
            this.Milk473mlRemarksTextBox.Leave += new System.EventHandler(this.Milk473mlActualOpenTextBox_Leave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(273, 120);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(49, 13);
            this.label23.TabIndex = 246;
            this.label23.Text = "Remarks";
            // 
            // Milk473mlDGV
            // 
            this.Milk473mlDGV.AllowDrop = true;
            this.Milk473mlDGV.AllowUserToAddRows = false;
            this.Milk473mlDGV.AllowUserToDeleteRows = false;
            this.Milk473mlDGV.AllowUserToResizeColumns = false;
            this.Milk473mlDGV.AllowUserToResizeRows = false;
            this.Milk473mlDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.Milk473mlDGV.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle101.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle101.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle101.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle101.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle101.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle101.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle101.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk473mlDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle101;
            this.Milk473mlDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Milk473mlDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn9});
            this.Milk473mlDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.Milk473mlDGV.GridColor = System.Drawing.SystemColors.Desktop;
            this.Milk473mlDGV.Location = new System.Drawing.Point(811, 28);
            this.Milk473mlDGV.MultiSelect = false;
            this.Milk473mlDGV.Name = "Milk473mlDGV";
            this.Milk473mlDGV.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle102.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle102.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle102.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle102.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle102.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle102.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle102.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk473mlDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle102;
            this.Milk473mlDGV.RowHeadersVisible = false;
            this.Milk473mlDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Milk473mlDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Milk473mlDGV.Size = new System.Drawing.Size(222, 105);
            this.Milk473mlDGV.TabIndex = 244;
            // 
            // dataGridViewCheckBoxColumn9
            // 
            this.dataGridViewCheckBoxColumn9.HeaderText = "";
            this.dataGridViewCheckBoxColumn9.Name = "dataGridViewCheckBoxColumn9";
            this.dataGridViewCheckBoxColumn9.Width = 5;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(659, 28);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 15);
            this.label27.TabIndex = 245;
            this.label27.Text = "473 ml Docs";
            // 
            // Milk473mlDiffTextBox
            // 
            this.Milk473mlDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk473mlDiffTextBox.Location = new System.Drawing.Point(380, 82);
            this.Milk473mlDiffTextBox.Name = "Milk473mlDiffTextBox";
            this.Milk473mlDiffTextBox.ReadOnly = true;
            this.Milk473mlDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlDiffTextBox.TabIndex = 21;
            this.Milk473mlDiffTextBox.Text = "0";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(272, 82);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(23, 13);
            this.label97.TabIndex = 20;
            this.label97.Text = "Diff";
            // 
            // Milk473mlEODSoldTextBox
            // 
            this.Milk473mlEODSoldTextBox.Location = new System.Drawing.Point(380, 55);
            this.Milk473mlEODSoldTextBox.Name = "Milk473mlEODSoldTextBox";
            this.Milk473mlEODSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlEODSoldTextBox.TabIndex = 19;
            this.Milk473mlEODSoldTextBox.Text = "0";
            this.Milk473mlEODSoldTextBox.Leave += new System.EventHandler(this.Milk473mlActualOpenTextBox_Leave);
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(272, 55);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(54, 13);
            this.label98.TabIndex = 18;
            this.label98.Text = "EOD Sold";
            // 
            // Milk473mlCountSoldTextBox
            // 
            this.Milk473mlCountSoldTextBox.Location = new System.Drawing.Point(381, 28);
            this.Milk473mlCountSoldTextBox.Name = "Milk473mlCountSoldTextBox";
            this.Milk473mlCountSoldTextBox.ReadOnly = true;
            this.Milk473mlCountSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlCountSoldTextBox.TabIndex = 17;
            this.Milk473mlCountSoldTextBox.Text = "0";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(273, 28);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(59, 13);
            this.label99.TabIndex = 16;
            this.label99.Text = "Count Sold";
            // 
            // Milk473mlCloseTextBox
            // 
            this.Milk473mlCloseTextBox.Location = new System.Drawing.Point(124, 136);
            this.Milk473mlCloseTextBox.Name = "Milk473mlCloseTextBox";
            this.Milk473mlCloseTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlCloseTextBox.TabIndex = 15;
            this.Milk473mlCloseTextBox.Text = "0";
            this.Milk473mlCloseTextBox.Leave += new System.EventHandler(this.Milk473mlActualOpenTextBox_Leave);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(16, 136);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(33, 13);
            this.label100.TabIndex = 14;
            this.label100.Text = "Close";
            // 
            // Milk473mlSubTotalTextBox
            // 
            this.Milk473mlSubTotalTextBox.Location = new System.Drawing.Point(124, 109);
            this.Milk473mlSubTotalTextBox.Name = "Milk473mlSubTotalTextBox";
            this.Milk473mlSubTotalTextBox.ReadOnly = true;
            this.Milk473mlSubTotalTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlSubTotalTextBox.TabIndex = 13;
            this.Milk473mlSubTotalTextBox.Text = "0";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(16, 109);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(53, 13);
            this.label101.TabIndex = 12;
            this.label101.Text = "Sub Total";
            // 
            // Milk473mlAddTextBox
            // 
            this.Milk473mlAddTextBox.Location = new System.Drawing.Point(124, 82);
            this.Milk473mlAddTextBox.Name = "Milk473mlAddTextBox";
            this.Milk473mlAddTextBox.ReadOnly = true;
            this.Milk473mlAddTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlAddTextBox.TabIndex = 11;
            this.Milk473mlAddTextBox.Text = "0";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(16, 82);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(26, 13);
            this.label102.TabIndex = 10;
            this.label102.Text = "Add";
            // 
            // Milk473mlActualOpenTextBox
            // 
            this.Milk473mlActualOpenTextBox.Location = new System.Drawing.Point(124, 55);
            this.Milk473mlActualOpenTextBox.Name = "Milk473mlActualOpenTextBox";
            this.Milk473mlActualOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlActualOpenTextBox.TabIndex = 9;
            this.Milk473mlActualOpenTextBox.Text = "0";
            this.Milk473mlActualOpenTextBox.Leave += new System.EventHandler(this.Milk473mlActualOpenTextBox_Leave);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(16, 55);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(66, 13);
            this.label103.TabIndex = 8;
            this.label103.Text = "Actual Open";
            // 
            // Milk473mlOpenTextBox
            // 
            this.Milk473mlOpenTextBox.Location = new System.Drawing.Point(124, 28);
            this.Milk473mlOpenTextBox.Name = "Milk473mlOpenTextBox";
            this.Milk473mlOpenTextBox.ReadOnly = true;
            this.Milk473mlOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk473mlOpenTextBox.TabIndex = 7;
            this.Milk473mlOpenTextBox.Text = "0";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(16, 28);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(33, 13);
            this.label104.TabIndex = 6;
            this.label104.Text = "Open";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Milk1LActualOpenDiffTextBox);
            this.groupBox6.Controls.Add(this.label105);
            this.groupBox6.Controls.Add(this.Milk1LRemarksTextBox);
            this.groupBox6.Controls.Add(this.label106);
            this.groupBox6.Controls.Add(this.Milk1LDGV);
            this.groupBox6.Controls.Add(this.label107);
            this.groupBox6.Controls.Add(this.Milk1LDiffTextBox);
            this.groupBox6.Controls.Add(this.label108);
            this.groupBox6.Controls.Add(this.Milk1LEODSoldTextBox);
            this.groupBox6.Controls.Add(this.label109);
            this.groupBox6.Controls.Add(this.Milk1LCountSoldTextBox);
            this.groupBox6.Controls.Add(this.label110);
            this.groupBox6.Controls.Add(this.Milk1LCloseTextBox);
            this.groupBox6.Controls.Add(this.label111);
            this.groupBox6.Controls.Add(this.Milk1LSubTotalTextBox);
            this.groupBox6.Controls.Add(this.label112);
            this.groupBox6.Controls.Add(this.Milk1LAddTextBox);
            this.groupBox6.Controls.Add(this.label113);
            this.groupBox6.Controls.Add(this.Milk1LActualOpenTextBox);
            this.groupBox6.Controls.Add(this.label114);
            this.groupBox6.Controls.Add(this.Milk1LOpenTextBox);
            this.groupBox6.Controls.Add(this.label115);
            this.groupBox6.Location = new System.Drawing.Point(12, 827);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1042, 194);
            this.groupBox6.TabIndex = 259;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "1 Ltr";
            // 
            // Milk1LActualOpenDiffTextBox
            // 
            this.Milk1LActualOpenDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk1LActualOpenDiffTextBox.Location = new System.Drawing.Point(124, 163);
            this.Milk1LActualOpenDiffTextBox.Name = "Milk1LActualOpenDiffTextBox";
            this.Milk1LActualOpenDiffTextBox.ReadOnly = true;
            this.Milk1LActualOpenDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LActualOpenDiffTextBox.TabIndex = 249;
            this.Milk1LActualOpenDiffTextBox.Text = "0";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(16, 163);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(85, 13);
            this.label105.TabIndex = 248;
            this.label105.Text = "Actual Open Diff";
            // 
            // Milk1LRemarksTextBox
            // 
            this.Milk1LRemarksTextBox.Location = new System.Drawing.Point(380, 109);
            this.Milk1LRemarksTextBox.Name = "Milk1LRemarksTextBox";
            this.Milk1LRemarksTextBox.Size = new System.Drawing.Size(205, 45);
            this.Milk1LRemarksTextBox.TabIndex = 247;
            this.Milk1LRemarksTextBox.Text = "";
            this.Milk1LRemarksTextBox.Leave += new System.EventHandler(this.Milk1LActualOpenTextBox_Leave);
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(273, 120);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(49, 13);
            this.label106.TabIndex = 246;
            this.label106.Text = "Remarks";
            // 
            // Milk1LDGV
            // 
            this.Milk1LDGV.AllowDrop = true;
            this.Milk1LDGV.AllowUserToAddRows = false;
            this.Milk1LDGV.AllowUserToDeleteRows = false;
            this.Milk1LDGV.AllowUserToResizeColumns = false;
            this.Milk1LDGV.AllowUserToResizeRows = false;
            this.Milk1LDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.Milk1LDGV.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle103.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle103.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle103.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle103.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle103.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle103.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk1LDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle103;
            this.Milk1LDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Milk1LDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn10});
            this.Milk1LDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.Milk1LDGV.GridColor = System.Drawing.SystemColors.Desktop;
            this.Milk1LDGV.Location = new System.Drawing.Point(811, 28);
            this.Milk1LDGV.MultiSelect = false;
            this.Milk1LDGV.Name = "Milk1LDGV";
            this.Milk1LDGV.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle104.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle104.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle104.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle104.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle104.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle104.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk1LDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle104;
            this.Milk1LDGV.RowHeadersVisible = false;
            this.Milk1LDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Milk1LDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Milk1LDGV.Size = new System.Drawing.Size(222, 105);
            this.Milk1LDGV.TabIndex = 244;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "";
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Width = 5;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(659, 28);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(60, 15);
            this.label107.TabIndex = 245;
            this.label107.Text = "1Ltr Docs";
            // 
            // Milk1LDiffTextBox
            // 
            this.Milk1LDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk1LDiffTextBox.Location = new System.Drawing.Point(380, 82);
            this.Milk1LDiffTextBox.Name = "Milk1LDiffTextBox";
            this.Milk1LDiffTextBox.ReadOnly = true;
            this.Milk1LDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LDiffTextBox.TabIndex = 21;
            this.Milk1LDiffTextBox.Text = "0";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(272, 82);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(23, 13);
            this.label108.TabIndex = 20;
            this.label108.Text = "Diff";
            // 
            // Milk1LEODSoldTextBox
            // 
            this.Milk1LEODSoldTextBox.Location = new System.Drawing.Point(380, 55);
            this.Milk1LEODSoldTextBox.Name = "Milk1LEODSoldTextBox";
            this.Milk1LEODSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LEODSoldTextBox.TabIndex = 19;
            this.Milk1LEODSoldTextBox.Text = "0";
            this.Milk1LEODSoldTextBox.Leave += new System.EventHandler(this.Milk1LActualOpenTextBox_Leave);
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(272, 55);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(54, 13);
            this.label109.TabIndex = 18;
            this.label109.Text = "EOD Sold";
            // 
            // Milk1LCountSoldTextBox
            // 
            this.Milk1LCountSoldTextBox.Location = new System.Drawing.Point(381, 28);
            this.Milk1LCountSoldTextBox.Name = "Milk1LCountSoldTextBox";
            this.Milk1LCountSoldTextBox.ReadOnly = true;
            this.Milk1LCountSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LCountSoldTextBox.TabIndex = 17;
            this.Milk1LCountSoldTextBox.Text = "0";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(273, 28);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(59, 13);
            this.label110.TabIndex = 16;
            this.label110.Text = "Count Sold";
            // 
            // Milk1LCloseTextBox
            // 
            this.Milk1LCloseTextBox.Location = new System.Drawing.Point(124, 136);
            this.Milk1LCloseTextBox.Name = "Milk1LCloseTextBox";
            this.Milk1LCloseTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LCloseTextBox.TabIndex = 15;
            this.Milk1LCloseTextBox.Text = "0";
            this.Milk1LCloseTextBox.Leave += new System.EventHandler(this.Milk1LActualOpenTextBox_Leave);
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(16, 136);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(33, 13);
            this.label111.TabIndex = 14;
            this.label111.Text = "Close";
            // 
            // Milk1LSubTotalTextBox
            // 
            this.Milk1LSubTotalTextBox.Location = new System.Drawing.Point(124, 109);
            this.Milk1LSubTotalTextBox.Name = "Milk1LSubTotalTextBox";
            this.Milk1LSubTotalTextBox.ReadOnly = true;
            this.Milk1LSubTotalTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LSubTotalTextBox.TabIndex = 13;
            this.Milk1LSubTotalTextBox.Text = "0";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(16, 109);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(53, 13);
            this.label112.TabIndex = 12;
            this.label112.Text = "Sub Total";
            // 
            // Milk1LAddTextBox
            // 
            this.Milk1LAddTextBox.Location = new System.Drawing.Point(124, 82);
            this.Milk1LAddTextBox.Name = "Milk1LAddTextBox";
            this.Milk1LAddTextBox.ReadOnly = true;
            this.Milk1LAddTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LAddTextBox.TabIndex = 11;
            this.Milk1LAddTextBox.Text = "0";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(16, 82);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(26, 13);
            this.label113.TabIndex = 10;
            this.label113.Text = "Add";
            // 
            // Milk1LActualOpenTextBox
            // 
            this.Milk1LActualOpenTextBox.Location = new System.Drawing.Point(124, 55);
            this.Milk1LActualOpenTextBox.Name = "Milk1LActualOpenTextBox";
            this.Milk1LActualOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LActualOpenTextBox.TabIndex = 9;
            this.Milk1LActualOpenTextBox.Text = "0";
            this.Milk1LActualOpenTextBox.Leave += new System.EventHandler(this.Milk1LActualOpenTextBox_Leave);
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(16, 55);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(66, 13);
            this.label114.TabIndex = 8;
            this.label114.Text = "Actual Open";
            // 
            // Milk1LOpenTextBox
            // 
            this.Milk1LOpenTextBox.Location = new System.Drawing.Point(124, 28);
            this.Milk1LOpenTextBox.Name = "Milk1LOpenTextBox";
            this.Milk1LOpenTextBox.ReadOnly = true;
            this.Milk1LOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk1LOpenTextBox.TabIndex = 7;
            this.Milk1LOpenTextBox.Text = "0";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(16, 28);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(33, 13);
            this.label115.TabIndex = 6;
            this.label115.Text = "Open";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.Milk2LActualOpenDiffTextBox);
            this.groupBox7.Controls.Add(this.label116);
            this.groupBox7.Controls.Add(this.Milk2LRemarksTextBox);
            this.groupBox7.Controls.Add(this.label117);
            this.groupBox7.Controls.Add(this.Milk2LDGV);
            this.groupBox7.Controls.Add(this.label118);
            this.groupBox7.Controls.Add(this.Milk2LDiffTextBox);
            this.groupBox7.Controls.Add(this.label119);
            this.groupBox7.Controls.Add(this.Milk2LEODSoldTextBox);
            this.groupBox7.Controls.Add(this.label120);
            this.groupBox7.Controls.Add(this.Milk2LCountSoldTextBox);
            this.groupBox7.Controls.Add(this.label121);
            this.groupBox7.Controls.Add(this.Milk2LCloseTextBox);
            this.groupBox7.Controls.Add(this.label122);
            this.groupBox7.Controls.Add(this.Milk2LSubTotalTextBox);
            this.groupBox7.Controls.Add(this.label123);
            this.groupBox7.Controls.Add(this.Milk2LAddTextBox);
            this.groupBox7.Controls.Add(this.label124);
            this.groupBox7.Controls.Add(this.Milk2LActualOpenTextBox);
            this.groupBox7.Controls.Add(this.label125);
            this.groupBox7.Controls.Add(this.Milk2LOpenTextBox);
            this.groupBox7.Controls.Add(this.label126);
            this.groupBox7.Location = new System.Drawing.Point(12, 1027);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1042, 194);
            this.groupBox7.TabIndex = 260;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "2 Ltr";
            // 
            // Milk2LActualOpenDiffTextBox
            // 
            this.Milk2LActualOpenDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk2LActualOpenDiffTextBox.Location = new System.Drawing.Point(124, 163);
            this.Milk2LActualOpenDiffTextBox.Name = "Milk2LActualOpenDiffTextBox";
            this.Milk2LActualOpenDiffTextBox.ReadOnly = true;
            this.Milk2LActualOpenDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LActualOpenDiffTextBox.TabIndex = 249;
            this.Milk2LActualOpenDiffTextBox.Text = "0";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(16, 163);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(85, 13);
            this.label116.TabIndex = 248;
            this.label116.Text = "Actual Open Diff";
            // 
            // Milk2LRemarksTextBox
            // 
            this.Milk2LRemarksTextBox.Location = new System.Drawing.Point(380, 109);
            this.Milk2LRemarksTextBox.Name = "Milk2LRemarksTextBox";
            this.Milk2LRemarksTextBox.Size = new System.Drawing.Size(205, 45);
            this.Milk2LRemarksTextBox.TabIndex = 247;
            this.Milk2LRemarksTextBox.Text = "";
            this.Milk2LRemarksTextBox.Leave += new System.EventHandler(this.Milk2LActualOpenTextBox_Leave);
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(273, 120);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(49, 13);
            this.label117.TabIndex = 246;
            this.label117.Text = "Remarks";
            // 
            // Milk2LDGV
            // 
            this.Milk2LDGV.AllowDrop = true;
            this.Milk2LDGV.AllowUserToAddRows = false;
            this.Milk2LDGV.AllowUserToDeleteRows = false;
            this.Milk2LDGV.AllowUserToResizeColumns = false;
            this.Milk2LDGV.AllowUserToResizeRows = false;
            this.Milk2LDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.Milk2LDGV.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle105.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle105.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle105.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle105.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle105.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle105.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk2LDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle105;
            this.Milk2LDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Milk2LDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn11});
            this.Milk2LDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.Milk2LDGV.GridColor = System.Drawing.SystemColors.Desktop;
            this.Milk2LDGV.Location = new System.Drawing.Point(811, 28);
            this.Milk2LDGV.MultiSelect = false;
            this.Milk2LDGV.Name = "Milk2LDGV";
            this.Milk2LDGV.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle106.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle106.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle106.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle106.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle106.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle106.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk2LDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle106;
            this.Milk2LDGV.RowHeadersVisible = false;
            this.Milk2LDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Milk2LDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Milk2LDGV.Size = new System.Drawing.Size(222, 105);
            this.Milk2LDGV.TabIndex = 244;
            // 
            // dataGridViewCheckBoxColumn11
            // 
            this.dataGridViewCheckBoxColumn11.HeaderText = "";
            this.dataGridViewCheckBoxColumn11.Name = "dataGridViewCheckBoxColumn11";
            this.dataGridViewCheckBoxColumn11.Width = 5;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.Black;
            this.label118.Location = new System.Drawing.Point(659, 28);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(60, 15);
            this.label118.TabIndex = 245;
            this.label118.Text = "2Ltr Docs";
            // 
            // Milk2LDiffTextBox
            // 
            this.Milk2LDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk2LDiffTextBox.Location = new System.Drawing.Point(380, 82);
            this.Milk2LDiffTextBox.Name = "Milk2LDiffTextBox";
            this.Milk2LDiffTextBox.ReadOnly = true;
            this.Milk2LDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LDiffTextBox.TabIndex = 21;
            this.Milk2LDiffTextBox.Text = "0";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(272, 82);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(23, 13);
            this.label119.TabIndex = 20;
            this.label119.Text = "Diff";
            // 
            // Milk2LEODSoldTextBox
            // 
            this.Milk2LEODSoldTextBox.Location = new System.Drawing.Point(380, 55);
            this.Milk2LEODSoldTextBox.Name = "Milk2LEODSoldTextBox";
            this.Milk2LEODSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LEODSoldTextBox.TabIndex = 19;
            this.Milk2LEODSoldTextBox.Text = "0";
            this.Milk2LEODSoldTextBox.Leave += new System.EventHandler(this.Milk2LActualOpenTextBox_Leave);
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(272, 55);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(54, 13);
            this.label120.TabIndex = 18;
            this.label120.Text = "EOD Sold";
            // 
            // Milk2LCountSoldTextBox
            // 
            this.Milk2LCountSoldTextBox.Location = new System.Drawing.Point(381, 28);
            this.Milk2LCountSoldTextBox.Name = "Milk2LCountSoldTextBox";
            this.Milk2LCountSoldTextBox.ReadOnly = true;
            this.Milk2LCountSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LCountSoldTextBox.TabIndex = 17;
            this.Milk2LCountSoldTextBox.Text = "0";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(273, 28);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(59, 13);
            this.label121.TabIndex = 16;
            this.label121.Text = "Count Sold";
            // 
            // Milk2LCloseTextBox
            // 
            this.Milk2LCloseTextBox.Location = new System.Drawing.Point(124, 136);
            this.Milk2LCloseTextBox.Name = "Milk2LCloseTextBox";
            this.Milk2LCloseTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LCloseTextBox.TabIndex = 15;
            this.Milk2LCloseTextBox.Text = "0";
            this.Milk2LCloseTextBox.Leave += new System.EventHandler(this.Milk2LActualOpenTextBox_Leave);
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(16, 136);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(33, 13);
            this.label122.TabIndex = 14;
            this.label122.Text = "Close";
            // 
            // Milk2LSubTotalTextBox
            // 
            this.Milk2LSubTotalTextBox.Location = new System.Drawing.Point(124, 109);
            this.Milk2LSubTotalTextBox.Name = "Milk2LSubTotalTextBox";
            this.Milk2LSubTotalTextBox.ReadOnly = true;
            this.Milk2LSubTotalTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LSubTotalTextBox.TabIndex = 13;
            this.Milk2LSubTotalTextBox.Text = "0";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(16, 109);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(53, 13);
            this.label123.TabIndex = 12;
            this.label123.Text = "Sub Total";
            // 
            // Milk2LAddTextBox
            // 
            this.Milk2LAddTextBox.Location = new System.Drawing.Point(124, 82);
            this.Milk2LAddTextBox.Name = "Milk2LAddTextBox";
            this.Milk2LAddTextBox.ReadOnly = true;
            this.Milk2LAddTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LAddTextBox.TabIndex = 11;
            this.Milk2LAddTextBox.Text = "0";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(16, 82);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(26, 13);
            this.label124.TabIndex = 10;
            this.label124.Text = "Add";
            // 
            // Milk2LActualOpenTextBox
            // 
            this.Milk2LActualOpenTextBox.Location = new System.Drawing.Point(124, 55);
            this.Milk2LActualOpenTextBox.Name = "Milk2LActualOpenTextBox";
            this.Milk2LActualOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LActualOpenTextBox.TabIndex = 9;
            this.Milk2LActualOpenTextBox.Text = "0";
            this.Milk2LActualOpenTextBox.Leave += new System.EventHandler(this.Milk2LActualOpenTextBox_Leave);
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(16, 55);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(66, 13);
            this.label125.TabIndex = 8;
            this.label125.Text = "Actual Open";
            // 
            // Milk2LOpenTextBox
            // 
            this.Milk2LOpenTextBox.Location = new System.Drawing.Point(124, 28);
            this.Milk2LOpenTextBox.Name = "Milk2LOpenTextBox";
            this.Milk2LOpenTextBox.ReadOnly = true;
            this.Milk2LOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk2LOpenTextBox.TabIndex = 7;
            this.Milk2LOpenTextBox.Text = "0";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(16, 28);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(33, 13);
            this.label126.TabIndex = 6;
            this.label126.Text = "Open";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Milk4LActualOpenDiffTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Milk4LRemarksTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Milk4LDGV);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Milk4LDiffTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Milk4LEODSoldTextBox);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Milk4LCountSoldTextBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Milk4LCloseTextBox);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.Milk4LSubTotalTextBox);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.Milk4LAddTextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.Milk4LActualOpenTextBox);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.Milk4LOpenTextBox);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Location = new System.Drawing.Point(12, 1227);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1042, 194);
            this.groupBox1.TabIndex = 261;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "4 Ltr";
            // 
            // Milk4LActualOpenDiffTextBox
            // 
            this.Milk4LActualOpenDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk4LActualOpenDiffTextBox.Location = new System.Drawing.Point(124, 163);
            this.Milk4LActualOpenDiffTextBox.Name = "Milk4LActualOpenDiffTextBox";
            this.Milk4LActualOpenDiffTextBox.ReadOnly = true;
            this.Milk4LActualOpenDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LActualOpenDiffTextBox.TabIndex = 249;
            this.Milk4LActualOpenDiffTextBox.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 248;
            this.label1.Text = "Actual Open Diff";
            // 
            // Milk4LRemarksTextBox
            // 
            this.Milk4LRemarksTextBox.Location = new System.Drawing.Point(380, 109);
            this.Milk4LRemarksTextBox.Name = "Milk4LRemarksTextBox";
            this.Milk4LRemarksTextBox.Size = new System.Drawing.Size(205, 45);
            this.Milk4LRemarksTextBox.TabIndex = 247;
            this.Milk4LRemarksTextBox.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(273, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 246;
            this.label2.Text = "Remarks";
            // 
            // Milk4LDGV
            // 
            this.Milk4LDGV.AllowDrop = true;
            this.Milk4LDGV.AllowUserToAddRows = false;
            this.Milk4LDGV.AllowUserToDeleteRows = false;
            this.Milk4LDGV.AllowUserToResizeColumns = false;
            this.Milk4LDGV.AllowUserToResizeRows = false;
            this.Milk4LDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.Milk4LDGV.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle107.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle107.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle107.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle107.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle107.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle107.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk4LDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle107;
            this.Milk4LDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Milk4LDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1});
            this.Milk4LDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.Milk4LDGV.GridColor = System.Drawing.SystemColors.Desktop;
            this.Milk4LDGV.Location = new System.Drawing.Point(811, 28);
            this.Milk4LDGV.MultiSelect = false;
            this.Milk4LDGV.Name = "Milk4LDGV";
            this.Milk4LDGV.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle108.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle108.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle108.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle108.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle108.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle108.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Milk4LDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle108;
            this.Milk4LDGV.RowHeadersVisible = false;
            this.Milk4LDGV.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Milk4LDGV.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Milk4LDGV.Size = new System.Drawing.Size(222, 105);
            this.Milk4LDGV.TabIndex = 244;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(659, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 15);
            this.label3.TabIndex = 245;
            this.label3.Text = "4Ltr Docs";
            // 
            // Milk4LDiffTextBox
            // 
            this.Milk4LDiffTextBox.BackColor = System.Drawing.Color.Red;
            this.Milk4LDiffTextBox.Location = new System.Drawing.Point(380, 82);
            this.Milk4LDiffTextBox.Name = "Milk4LDiffTextBox";
            this.Milk4LDiffTextBox.ReadOnly = true;
            this.Milk4LDiffTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LDiffTextBox.TabIndex = 21;
            this.Milk4LDiffTextBox.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(272, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Diff";
            // 
            // Milk4LEODSoldTextBox
            // 
            this.Milk4LEODSoldTextBox.Location = new System.Drawing.Point(380, 55);
            this.Milk4LEODSoldTextBox.Name = "Milk4LEODSoldTextBox";
            this.Milk4LEODSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LEODSoldTextBox.TabIndex = 19;
            this.Milk4LEODSoldTextBox.Text = "0";
            this.Milk4LEODSoldTextBox.Leave += new System.EventHandler(this.Milk4LActualOpenTextBox_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(272, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "EOD Sold";
            // 
            // Milk4LCountSoldTextBox
            // 
            this.Milk4LCountSoldTextBox.Location = new System.Drawing.Point(381, 28);
            this.Milk4LCountSoldTextBox.Name = "Milk4LCountSoldTextBox";
            this.Milk4LCountSoldTextBox.ReadOnly = true;
            this.Milk4LCountSoldTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LCountSoldTextBox.TabIndex = 17;
            this.Milk4LCountSoldTextBox.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(273, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Count Sold";
            // 
            // Milk4LCloseTextBox
            // 
            this.Milk4LCloseTextBox.Location = new System.Drawing.Point(124, 136);
            this.Milk4LCloseTextBox.Name = "Milk4LCloseTextBox";
            this.Milk4LCloseTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LCloseTextBox.TabIndex = 15;
            this.Milk4LCloseTextBox.Text = "0";
            this.Milk4LCloseTextBox.Leave += new System.EventHandler(this.Milk4LActualOpenTextBox_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Close";
            // 
            // Milk4LSubTotalTextBox
            // 
            this.Milk4LSubTotalTextBox.Location = new System.Drawing.Point(124, 109);
            this.Milk4LSubTotalTextBox.Name = "Milk4LSubTotalTextBox";
            this.Milk4LSubTotalTextBox.ReadOnly = true;
            this.Milk4LSubTotalTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LSubTotalTextBox.TabIndex = 13;
            this.Milk4LSubTotalTextBox.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Sub Total";
            // 
            // Milk4LAddTextBox
            // 
            this.Milk4LAddTextBox.Location = new System.Drawing.Point(124, 82);
            this.Milk4LAddTextBox.Name = "Milk4LAddTextBox";
            this.Milk4LAddTextBox.ReadOnly = true;
            this.Milk4LAddTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LAddTextBox.TabIndex = 11;
            this.Milk4LAddTextBox.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Add";
            // 
            // Milk4LActualOpenTextBox
            // 
            this.Milk4LActualOpenTextBox.Location = new System.Drawing.Point(124, 55);
            this.Milk4LActualOpenTextBox.Name = "Milk4LActualOpenTextBox";
            this.Milk4LActualOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LActualOpenTextBox.TabIndex = 9;
            this.Milk4LActualOpenTextBox.Text = "0";
            this.Milk4LActualOpenTextBox.Leave += new System.EventHandler(this.Milk4LActualOpenTextBox_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Actual Open";
            // 
            // Milk4LOpenTextBox
            // 
            this.Milk4LOpenTextBox.Location = new System.Drawing.Point(124, 28);
            this.Milk4LOpenTextBox.Name = "Milk4LOpenTextBox";
            this.Milk4LOpenTextBox.ReadOnly = true;
            this.Milk4LOpenTextBox.Size = new System.Drawing.Size(125, 20);
            this.Milk4LOpenTextBox.TabIndex = 7;
            this.Milk4LOpenTextBox.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Open";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.SaveBtn);
            this.groupBox2.Controls.Add(this.ViewBtn);
            this.groupBox2.Controls.Add(this.btnExit);
            this.groupBox2.Controls.Add(this.DateCalender);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(12, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1043, 123);
            this.groupBox2.TabIndex = 262;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Master";
            // 
            // SaveBtn
            // 
            this.SaveBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SaveBtn.FlatAppearance.BorderSize = 2;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.Location = new System.Drawing.Point(241, 47);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(88, 32);
            this.SaveBtn.TabIndex = 233;
            this.SaveBtn.Text = "&Save";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // ViewBtn
            // 
            this.ViewBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ViewBtn.FlatAppearance.BorderSize = 2;
            this.ViewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewBtn.Location = new System.Drawing.Point(129, 47);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(90, 32);
            this.ViewBtn.TabIndex = 232;
            this.ViewBtn.Text = "&View";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(344, 47);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 231;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // DateCalender
            // 
            this.DateCalender.CustomFormat = "MMM/dd/yyyy";
            this.DateCalender.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateCalender.Location = new System.Drawing.Point(131, 20);
            this.DateCalender.Name = "DateCalender";
            this.DateCalender.Size = new System.Drawing.Size(202, 20);
            this.DateCalender.TabIndex = 219;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.AliceBlue;
            this.label12.Font = new System.Drawing.Font("Arial", 9F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(20, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 15);
            this.label12.TabIndex = 218;
            this.label12.Text = "Date";
            // 
            // MilkEODForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1079, 531);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Name = "MilkEODForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Milk EOD Form";
            this.Load += new System.EventHandler(this.MilkEODForm_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MilkAdditionDgv)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk310mlDGV)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk473mlDGV)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk1LDGV)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk2LDGV)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Milk4LDGV)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox MilkAddition4LTextBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.RichTextBox MilkAdditionRemarksTextBox;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridView MilkAdditionDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox MilkAdditionTotalTextBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox MilkAdditionOtherTextBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox MilkAddition2LTextBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox MilkAddition1LTextBox;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox MilkAddition473mlTextBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox MilkAddition310mlTextBox;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox Milk310mlActualOpenDiffTextBox;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.RichTextBox Milk310mlRemarksTextBox;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.DataGridView Milk310mlDGV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox Milk310mlDiffTextBox;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox Milk310mlEODSoldTextBox;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox Milk310mlCountSoldTextBox;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox Milk310mlCloseTextBox;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox Milk310mlSubTotalTextBox;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox Milk310mlAddTextBox;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox Milk310mlActualOpenTextBox;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox Milk310mlOpenTextBox;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox Milk473mlActualOpenDiffTextBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RichTextBox Milk473mlRemarksTextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridView Milk473mlDGV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn9;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Milk473mlDiffTextBox;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox Milk473mlEODSoldTextBox;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox Milk473mlCountSoldTextBox;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox Milk473mlCloseTextBox;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox Milk473mlSubTotalTextBox;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox Milk473mlAddTextBox;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TextBox Milk473mlActualOpenTextBox;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox Milk473mlOpenTextBox;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox Milk1LActualOpenDiffTextBox;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.RichTextBox Milk1LRemarksTextBox;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.DataGridView Milk1LDGV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox Milk1LDiffTextBox;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TextBox Milk1LEODSoldTextBox;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox Milk1LCountSoldTextBox;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox Milk1LCloseTextBox;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox Milk1LSubTotalTextBox;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox Milk1LAddTextBox;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox Milk1LActualOpenTextBox;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox Milk1LOpenTextBox;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox Milk2LActualOpenDiffTextBox;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.RichTextBox Milk2LRemarksTextBox;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.DataGridView Milk2LDGV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn11;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox Milk2LDiffTextBox;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox Milk2LEODSoldTextBox;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TextBox Milk2LCountSoldTextBox;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox Milk2LCloseTextBox;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.TextBox Milk2LSubTotalTextBox;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.TextBox Milk2LAddTextBox;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.TextBox Milk2LActualOpenTextBox;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.TextBox Milk2LOpenTextBox;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Milk4LActualOpenDiffTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox Milk4LRemarksTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView Milk4LDGV;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Milk4LDiffTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Milk4LEODSoldTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Milk4LCountSoldTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Milk4LCloseTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Milk4LSubTotalTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Milk4LAddTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Milk4LActualOpenTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox Milk4LOpenTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DateTimePicker DateCalender;
        private System.Windows.Forms.Label label12;
    }
}