﻿using ExportToExcel;
using Framework;
using Framework.Data;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Shell.Transaction
{
    public partial class InventoryManagementForm : BaseForm
    {
        public InventoryManagementForm()
        {
            InitializeComponent();
        }
        #region Private Methods
        private void BindCategories()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    CategorycomboBox.ValueMember = "CategoryID";
                    CategorycomboBox.DisplayMember = "CategoryName";
                    var categorymstList = context.MST_CategoriesMaster.Where(s => s.IsActive == true).Select(s => new { s.CategoryID, s.CategoryName }).ToList();
                    categorymstList.Insert(0, new { CategoryID = 0, CategoryName = "--Select--" });
                    CategorycomboBox.DataSource = categorymstList;


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.InnerException);
            }

        }

        private void BindProduct(int categoryId)
        {
            try
            {

                using (ShellEntities context = new ShellEntities())
                {
                    ProductcomboBox.ValueMember = "ProductId";
                    ProductcomboBox.DisplayMember = "ProductCode";
                    var productList = context.Mst_ProductMaster.Where(s => s.IsActive == true && s.ProductCategory == categoryId).Select(s => new { s.ProductId, s.ProductCode }).ToList();
                    productList.Insert(0, new { ProductId = (long)0, ProductCode = "--Select--" });
                    ProductcomboBox.DataSource = productList;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.InnerException);
            }
        }

        private void BindGrid()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text).Add(DateTime.Now.TimeOfDay);
                var inventory = (from s in context.Tbl_Inventory
                                 join c in context.MST_CategoriesMaster on s.CategoryID equals c.CategoryID
                                 join p in context.Mst_ProductMaster on s.ProductID equals p.ProductId
                                 where s.IsActive == true
                                 select new
                                 {
                                     s.InventoryID,
                                     s.EntryDate,
                                     c.CategoryName,
                                     p.ProductCode,
                                     s.Opening,
                                     s.Add,
                                     s.Substract,
                                     s.Closing
                                 }).ToList();
                dgvInventory.DataSource = inventory;
            }
        }
        private void SaveInventory()
        {
            using (ShellEntities context = new ShellEntities())
            {
                int catId = Convert.ToInt32(CategorycomboBox.SelectedValue);
                int prodId = Convert.ToInt32(ProductcomboBox.SelectedValue);
                DateTime date = Convert.ToDateTime(DateCalender.Text).Add(DateTime.Now.TimeOfDay);
                var inventory = context.Tbl_Inventory.FirstOrDefault(s => s.CategoryID == catId && s.ProductID == prodId && s.IsActive == true);
                if (inventory != null)
                {
                    if (date.ToString("MMddyyyy") == Convert.ToDateTime(inventory.EntryDate).ToString("MMddyyyy"))
                    {
                        inventory.CreatedBy = 1;
                        inventory.Opening = Convert.ToInt32(OpeningtextBox.Text);
                        inventory.Closing = Convert.ToInt32(ClosingtextBox.Text);
                        inventory.Add = Convert.ToInt32(AddtextBox.Text);
                        inventory.Substract = Convert.ToInt32(SubstracttextBox.Text);
                        inventory.IsActive = true;
                    }
                    else
                    {
                        inventory = new Tbl_Inventory();
                        inventory.CategoryID = catId;
                        inventory.ProductID = prodId;
                        inventory.EntryDate = date;
                        inventory.CreatedBy = 1;
                        inventory.Opening = Convert.ToInt32(OpeningtextBox.Text);
                        inventory.Closing = Convert.ToInt32(ClosingtextBox.Text);
                        inventory.Add = Convert.ToInt32(AddtextBox.Text);
                        inventory.Substract = Convert.ToInt32(SubstracttextBox.Text);
                        inventory.IsActive = true;
                        context.Tbl_Inventory.Add(inventory);
                    }
                }

                context.SaveChanges();
                MessageBox.Show("Inventory Saved");
                BindGrid();
            }
        }

        private void UpdateInventory(long id)
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text).Add(DateTime.Now.TimeOfDay);
                Tbl_Inventory inventory = context.Tbl_Inventory.FirstOrDefault(s => s.InventoryID == id && s.IsActive == true);
                inventory.CategoryID = Convert.ToInt32(CategorycomboBox.SelectedValue);
                inventory.ProductID = Convert.ToInt32(ProductcomboBox.SelectedValue);
                inventory.EntryDate = date;
                inventory.ModifiedBy = 1;
                inventory.ModifiedDate = DateTime.Now;
                inventory.Opening = Convert.ToInt32(OpeningtextBox.Text);
                inventory.Closing = Convert.ToInt32(ClosingtextBox.Text)- Convert.ToInt32(SubstracttextBox.Text);
                inventory.Add = Convert.ToInt32(AddtextBox.Text);
                inventory.Substract = Convert.ToInt32(SubstracttextBox.Text);
                inventory.IsActive = true;
                context.SaveChanges();
                MessageBox.Show("Inventory Updated");
                BindGrid();
            }
        }

        private void DeleteInventory(long id)
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text).Add(DateTime.Now.TimeOfDay);
                Tbl_Inventory inventory = context.Tbl_Inventory.FirstOrDefault(s => s.InventoryID == id);
                inventory.ModifiedBy = 1;
                inventory.ModifiedDate = DateTime.Now;
                inventory.IsActive = false;
                context.SaveChanges();
                MessageBox.Show("Inventory Deleted");
                BindGrid();
            }
        }
        private void FillDetails(long id)
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text).Add(DateTime.Now.TimeOfDay);
                Tbl_Inventory inventory = context.Tbl_Inventory.FirstOrDefault(s => s.InventoryID == id);
                CategorycomboBox.SelectedValue = inventory.CategoryID;
                BindProduct((int)inventory.CategoryID);
                ProductcomboBox.SelectedValue = inventory.ProductID;
                DateCalender.Text = Convert.ToDateTime(inventory.EntryDate).ToString("MMM/dd/yyyy");
                OpeningtextBox.Text = inventory.Opening.ToString();
                ClosingtextBox.Text = inventory.Closing.ToString();
                AddtextBox.Text = inventory.Add.ToString();
                SubstracttextBox.Text = inventory.Substract.ToString();

            }
        }
        private void LoadOpeningDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                int prodid = 0;
                int catId = 0;
                if (string.IsNullOrEmpty(DateCalender.Text))
                {
                    MessageBox.Show("Please select date !");
                    return;
                }
                if (CategorycomboBox.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select category !");
                    return;
                }
                else
                {
                    catId = Convert.ToInt32(CategorycomboBox.SelectedValue);
                }
                if (ProductcomboBox.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select product !");
                    return;
                }
                else
                {
                    prodid = Convert.ToInt32(ProductcomboBox.SelectedValue);
                }

                DateTime date = Convert.ToDateTime(DateCalender.Text);

                var inventory = context.Tbl_Inventory.Where(s => s.IsActive == true).Select(s => new { s.InventoryID, s.EntryDate, s.CategoryID, s.ProductID, s.Opening, s.Closing }).ToList();
                var inventoryEntry = inventory.Where(s => Convert.ToDateTime(s.EntryDate).ToString("yyyyMMdd") == date.ToString("yyyyMMdd") && s.ProductID == prodid && s.CategoryID == catId).FirstOrDefault();
                var balanceData = (from i in context.Tbl_Inventory where i.IsActive == true && i.CategoryID == catId && i.ProductID == prodid orderby i.EntryDate descending select i.Closing).FirstOrDefault();
                if (inventoryEntry != null)
                {
                    OpeningtextBox.Text = inventoryEntry.Opening.ToString();
                    BtnSave.Enabled = false;
                }

                if (balanceData != null)
                {
                    ClosingtextBox.Text = balanceData.ToString();
                    BtnSave.Enabled = true;
                }
                else
                {
                    MessageBox.Show("1st entry for inventory management.");
                }
            }
        }
        private long InventoryId = 0;

        #endregion
        #region Form Events
        private void InventoryManagementForm_Load(object sender, EventArgs e)
        {
            try
            {
                BindCategories();
                BindGrid();
                BtnSave.Enabled = true;
                Updatebutton.Enabled = false;
                BtnDelete.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                LoadOpeningDetails();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void dgvInventory_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dgvInventory.Rows[e.RowIndex];
                    InventoryId = Convert.ToInt64(row.Cells["InventoryID"].Value);
                    FillDetails(InventoryId);
                    BtnSave.Enabled = false;
                    Updatebutton.Enabled = true;
                    BtnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveInventory();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void Updatebutton_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateInventory(InventoryId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteInventory(InventoryId);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void AddtextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                ClosingtextBox.Text = "0";
                ClosingtextBox.Text = ((Convert.ToInt32(ClosingtextBox.Text) + Convert.ToInt32(AddtextBox.Text))).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void SubstracttextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                ClosingtextBox.Text = "0";
                ClosingtextBox.Text = ((Convert.ToInt32(ClosingtextBox.Text)) - Convert.ToInt32(SubstracttextBox.Text)).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void CategorycomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (CategorycomboBox.SelectedIndex > 0)
                {
                    int categoryId = Convert.ToInt32(CategorycomboBox.SelectedValue);
                    BindProduct(categoryId);
                }
                ClosingtextBox.Text = ((Convert.ToInt32(ClosingtextBox.Text)) - Convert.ToInt32(SubstracttextBox.Text)).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        private void Exportbutton_Click(object sender, EventArgs e)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text).Add(DateTime.Now.TimeOfDay);
                    var inventory = (from s in context.Tbl_Inventory
                                     join c in context.MST_CategoriesMaster on s.CategoryID equals c.CategoryID
                                     join p in context.Mst_ProductMaster on s.ProductID equals p.ProductId
                                     where s.IsActive == true
                                     select new
                                     {
                                         s.EntryDate,
                                         c.CategoryName,
                                         p.ProductCode,
                                         s.Opening,
                                         s.Add,
                                         s.Substract,
                                         s.Closing
                                     }).ToList();
                    if (inventory.Count > 0)
                    {
                        CreateExcelFile.CreateExcelDocument(inventory, Path.Combine(ShellComman.ReportExelPath, DateTime.Now.ToString("yyyy_MMM_dd_hhmmss")) + ".xls");
                    }
                    else
                    {
                        MessageBox.Show("There is no record to export.");
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        #endregion

    }
}
