﻿using BAL;
using Framework;
using Framework.Data;
using System;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
using DAL;

namespace Shell.Transaction
{
    public partial class PNLEntryForm : BaseForm
    {
        public PNLEntryForm()
        {
            InitializeComponent();
        }
        #region Private Events
        private void CalculateTotal()
        {
            var total = (Convert.ToDouble(txtAmount.Text) + Convert.ToDouble(OthertextBox.Text));
            txtInvoiceBal.Text = total.ToString();
        }
        private string TransactionType()
        {
            string TransactionType = string.Empty;
            if (rbtCredit.Checked == true)
                TransactionType = "Credit";
            else
                TransactionType = "Debit";

            return TransactionType;
        }
        private void BindPaymentType()
        {
            ddlPaymnetType.DisplayMember = "PaymenttypeCode";
            ddlPaymnetType.ValueMember = "PaymentTypeId";
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            ddlPaymnetType.DataSource = _objVendor.GetPaymentlist();
        }
        private void BindPNLDropDown()
        {
            ddlPNLCode.DisplayMember = "PNLCode";
            ddlPNLCode.ValueMember = "PNLID";
            BAL_PNL _ObjPNL = new BAL_PNL();
            var list = _ObjPNL.GetPNLlist();
            list.Insert(0, new PNLEntity { PNLID = 0, PNLCode = "--Select--" });
            ddlPNLCode.DataSource = list;

        }
        private void BindDocumentsGrid()
        {
            DateTime entryDate = Convert.ToDateTime(PNLEntryDate.Text);
            dgvPNLDocs.DataSource = null;
            using (ShellEntities context = new ShellEntities())
            {
                var docs = context.Mst_DocUpload.Where(s => s.DocsType == "PNL").Select(s => new { s.UploadId, s.DocsIdentity, s.TransactionDate }).ToList();
                dgvPNLDocs.DataSource = docs.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == entryDate.ToString("MMddyyyy")).ToList();

                dgvPNLDocs.Columns["UploadId"].Visible = false;
            }
        }
        private void BindPNLEntryGrid()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var docs = (from m in context.Tbl_PNLEntry
                            join p in context.MST_PaymentType on m.PaymentType equals p.PaymnetTypeId
                            where m.IsActive == true && m.IsClosed == false
                            select new
                            {
                                m.PNLEntryID,
                                m.EntryDate,
                                p.PaymenttypeName,
                                m.TransactionType,
                                m.TotalAmount
                            }).ToList();
                dgvPNLEntryList.DataSource = docs;
            }
        }
        private List<string> docIds = new List<string>();
        private void SavePNLEntry()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime entryDate = Convert.ToDateTime(PNLEntryDate.Text).Add(DateTime.Now.TimeOfDay);
                Tbl_PNLEntry pnlObj = new Tbl_PNLEntry();
                pnlObj.EntryDate = entryDate;
                pnlObj.PNLID = Convert.ToInt32(ddlPNLCode.SelectedValue);
                pnlObj.PaymentType = Convert.ToInt32(ddlPaymnetType.SelectedValue);
                pnlObj.TransactionType = TransactionType();
                pnlObj.TotalAmount = Convert.ToDouble(txtInvoiceBal.Text);
                pnlObj.Remarks = txtRemarks.Text;
                pnlObj.IsClosed = false;
                pnlObj.IsActive = true;
                context.Tbl_PNLEntry.Add(pnlObj);
                context.SaveChanges();
                foreach (DataGridViewRow row in dgvPNLDocs.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {
                        docIds.Add(row.Cells[1].Value.ToString());
                        Tbl_PNLEntryDocuments docsObj = new Tbl_PNLEntryDocuments();
                        docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                        docsObj.PNLID = pnlObj.PNLEntryID;
                        docsObj.IsActive = true;
                        context.Tbl_PNLEntryDocuments.Add(docsObj);
                    }
                }
                context.SaveChanges();
                BindPNLEntryGrid();
            }
        }

        private void FillPNLEntry(long id)
        {
            using (ShellEntities context = new ShellEntities())
            {
                var Tbl_PNLEntry = (from m in context.Tbl_PNLEntry
                                    join p in context.MST_PaymentType on m.PaymentType equals p.PaymnetTypeId
                                    where m.IsActive == true && m.IsClosed == false && m.PNLEntryID == id
                                    select new
                                    {
                                        m.PNLID,
                                        m.EntryDate,
                                        m.PaymentType,
                                        m.TransactionType,
                                        m.TotalAmount,
                                        m.InvoiceNo,
                                        m.Remarks
                                    }).FirstOrDefault();

                var pnlDocs = (from d in context.Tbl_PNLEntryDocuments
                               join m in context.Mst_DocUpload on d.UploadedDocID equals m.UploadId
                               where d.PNLID == id
                               select new
                               {
                                   d.UploadedDocID,
                                   m.DocsIdentity,
                                   m.TransactionDate

                               }).ToList();

                ddlPNLCode.SelectedValue = Tbl_PNLEntry.PNLID;
                ddlPaymnetType.SelectedValue = Tbl_PNLEntry.PaymentType;

                if (Tbl_PNLEntry.TransactionType == "Credit")
                {
                    rbtCredit.Checked = true;
                }

                else
                {
                    rbtDebit.Checked = true;
                }
                PNLEntryDate.Text = Convert.ToDateTime(Tbl_PNLEntry.EntryDate).ToString("ddMMMyyyy");
                txtInvoiceNo.Text = Tbl_PNLEntry.InvoiceNo;
                txtInvoiceBal.Text = Tbl_PNLEntry.TotalAmount.ToString();
                txtRemarks.Text = Tbl_PNLEntry.Remarks;

                foreach (DataGridViewRow row in dgvPNLDocs.Rows)
                {
                    foreach (var docId in pnlDocs)
                    {
                        if (row.Cells[1].Value.ToString() == docId.UploadedDocID.ToString())
                        {
                            row.Cells[0].Value = true;
                        }
                    }
                }
            }
        }
        private void UpdatePNLEntry(long id)
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime entryDate = Convert.ToDateTime(PNLEntryDate.Text).Add(DateTime.Now.TimeOfDay);
                Tbl_PNLEntry pnlObj = (from m in context.Tbl_PNLEntry
                                       join p in context.MST_PaymentType on m.PaymentType equals p.PaymnetTypeId
                                       where m.IsActive == true && m.IsClosed == false && m.PNLEntryID == id
                                       select m).FirstOrDefault();
                pnlObj.EntryDate = entryDate;
                pnlObj.PNLID = Convert.ToInt32(ddlPNLCode.SelectedValue);
                pnlObj.PaymentType = Convert.ToInt32(ddlPaymnetType.SelectedValue);
                pnlObj.TransactionType = TransactionType();
                pnlObj.TotalAmount = Convert.ToDouble(txtInvoiceBal.Text);
                pnlObj.Remarks = txtRemarks.Text;
                pnlObj.IsClosed = false;
                pnlObj.IsActive = true;
                context.SaveChanges();

                foreach (DataGridViewRow row in dgvPNLDocs.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {
                        int docId = Convert.ToInt32((row.Cells[1].Value));
                        docIds.Add(row.Cells[1].Value.ToString());
                        var docExist = context.Tbl_PNLEntryDocuments.FirstOrDefault(s => s.UploadedDocID == docId && s.PNLID == pnlObj.PNLID);
                        if (docExist == null)
                        {
                            Tbl_PNLEntryDocuments docsObj = new Tbl_PNLEntryDocuments();
                            docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                            docsObj.PNLID = pnlObj.PNLID;
                            docsObj.IsActive = true;
                            docsObj.CreatedBy = 1;
                            docsObj.CreatedDate = DateTime.Now;
                            context.Tbl_PNLEntryDocuments.Add(docsObj);
                        }

                    }

                    context.SaveChanges();
                }
                BindPNLEntryGrid();
            }
        }

        private void DeletePNLEntry(long id)
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime entryDate = Convert.ToDateTime(PNLEntryDate.Text).Add(DateTime.Now.TimeOfDay);
                Tbl_PNLEntry pnlObj = context.Tbl_PNLEntry.Where(s => s.PNLEntryID == id).FirstOrDefault();
                pnlObj.IsClosed = false;
                pnlObj.IsActive = false;
                context.SaveChanges();
            }
        }
        #endregion
        #region Form Events
        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtInvoiceBal_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAmount_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateTotal();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void OthertextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateTotal();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SavePNLEntry();
                BindPNLEntryGrid();
                MessageBox.Show("Data Saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void PNLEntryForm_Load(object sender, EventArgs e)
        {
            try
            {
                BindPaymentType();
                BindPNLDropDown();
                BindDocumentsGrid();
                BindPNLEntryGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        private void dgvPNLEntryList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow row = this.dgvPNLEntryList.Rows[e.RowIndex];
                long id = Convert.ToInt64(row.Cells["PNLEntryID"].Value);
                PNLEntryID = id;
                FillPNLEntry(id);

                btnUpdate.Enabled = true;
                BtnDelete.Enabled = true;
                BtnSave.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
                BtnSave.Enabled = true;
                if (PNLEntryID > 0)
                {
                    UpdatePNLEntry(PNLEntryID);
                    BindPNLEntryGrid();
                    MessageBox.Show("Data Updated.");
                }
                else
                {
                    MessageBox.Show("Please click on grid cell to update the record");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {

            try
            {
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
                BtnSave.Enabled = true;
                if (PNLEntryID > 0)
                {
                    DeletePNLEntry(PNLEntryID);
                    BindPNLEntryGrid();
                    MessageBox.Show("Data Deleted.");
                }
                else
                {
                    MessageBox.Show("Please click on grid cell to delete the record");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private long PNLEntryID = 0;

    }
}
