﻿namespace Shell.Transaction
{
    partial class PastSettlementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.ToDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.FromDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.grpDocumentList = new System.Windows.Forms.GroupBox();
            this.SaveAsbtn = new System.Windows.Forms.Button();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.dgvSettlement = new System.Windows.Forms.DataGridView();
            this.CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.groupBox1.SuspendLayout();
            this.grpDocumentList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlement)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ViewBtn);
            this.groupBox1.Controls.Add(this.ToDatePicker);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.FromDatePicker);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(723, 117);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Past Settlement";
            // 
            // ViewBtn
            // 
            this.ViewBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ViewBtn.FlatAppearance.BorderSize = 2;
            this.ViewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewBtn.Location = new System.Drawing.Point(255, 68);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(111, 32);
            this.ViewBtn.TabIndex = 9;
            this.ViewBtn.Text = "View";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // ToDatePicker
            // 
            this.ToDatePicker.Location = new System.Drawing.Point(345, 31);
            this.ToDatePicker.Name = "ToDatePicker";
            this.ToDatePicker.Size = new System.Drawing.Size(174, 22);
            this.ToDatePicker.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(286, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "To";
            // 
            // FromDatePicker
            // 
            this.FromDatePicker.Location = new System.Drawing.Point(77, 32);
            this.FromDatePicker.Name = "FromDatePicker";
            this.FromDatePicker.Size = new System.Drawing.Size(174, 22);
            this.FromDatePicker.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "From";
            // 
            // grpDocumentList
            // 
            this.grpDocumentList.Controls.Add(this.dgvSettlement);
            this.grpDocumentList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDocumentList.Location = new System.Drawing.Point(12, 157);
            this.grpDocumentList.Name = "grpDocumentList";
            this.grpDocumentList.Size = new System.Drawing.Size(724, 206);
            this.grpDocumentList.TabIndex = 62;
            this.grpDocumentList.TabStop = false;
            this.grpDocumentList.Text = "Open Settlement";
            // 
            // SaveAsbtn
            // 
            this.SaveAsbtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SaveAsbtn.FlatAppearance.BorderSize = 2;
            this.SaveAsbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveAsbtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveAsbtn.Location = new System.Drawing.Point(276, 369);
            this.SaveAsbtn.Name = "SaveAsbtn";
            this.SaveAsbtn.Size = new System.Drawing.Size(111, 32);
            this.SaveAsbtn.TabIndex = 10;
            this.SaveAsbtn.Text = "Save as";
            this.SaveAsbtn.UseVisualStyleBackColor = true;
            this.SaveAsbtn.Click += new System.EventHandler(this.SaveAsbtn_Click);
            // 
            // ExitBtn
            // 
            this.ExitBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ExitBtn.FlatAppearance.BorderSize = 2;
            this.ExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.Location = new System.Drawing.Point(406, 370);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(111, 32);
            this.ExitBtn.TabIndex = 63;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = true;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // dgvSettlement
            // 
            this.dgvSettlement.AllowDrop = true;
            this.dgvSettlement.AllowUserToAddRows = false;
            this.dgvSettlement.AllowUserToDeleteRows = false;
            this.dgvSettlement.AllowUserToResizeColumns = false;
            this.dgvSettlement.AllowUserToResizeRows = false;
            this.dgvSettlement.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSettlement.BackgroundColor = System.Drawing.Color.White;
            this.dgvSettlement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSettlement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckBox});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSettlement.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSettlement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSettlement.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSettlement.Location = new System.Drawing.Point(3, 16);
            this.dgvSettlement.MultiSelect = false;
            this.dgvSettlement.Name = "dgvSettlement";
            this.dgvSettlement.ReadOnly = true;
            this.dgvSettlement.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvSettlement.RowHeadersVisible = false;
            this.dgvSettlement.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvSettlement.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSettlement.Size = new System.Drawing.Size(718, 187);
            this.dgvSettlement.TabIndex = 2;
            this.dgvSettlement.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSettlement_CellContentClick);
            // 
            // CheckBox
            // 
            this.CheckBox.FalseValue = "false";
            this.CheckBox.HeaderText = "Select";
            this.CheckBox.MinimumWidth = 15;
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.ReadOnly = true;
            this.CheckBox.ToolTipText = "Select Settlement";
            this.CheckBox.TrueValue = "true";
            this.CheckBox.Width = 49;
            // 
            // PastSettlementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(748, 414);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.SaveAsbtn);
            this.Controls.Add(this.grpDocumentList);
            this.Controls.Add(this.groupBox1);
            this.Name = "PastSettlementForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Past Settlement";
            this.Load += new System.EventHandler(this.PastSettlementForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpDocumentList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlement)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker FromDatePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker ToDatePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.GroupBox grpDocumentList;
        private System.Windows.Forms.Button SaveAsbtn;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.DataGridView dgvSettlement;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox;
    }
}