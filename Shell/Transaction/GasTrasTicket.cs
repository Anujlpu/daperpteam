﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Transaction
{
    public partial class GasTrasTicket : Form
    {
        public GasTrasTicket()
        {
            InitializeComponent();
        }

        private void DisableOpenGridButton()
        {
           if (Utility.BA_Commman._Profile == "SupAdm" || Utility.BA_Commman._Profile == "Adm")
            {
                dataGridViewTickets.Columns["Open"].Visible = true;
                
            }
            else
            {
                dataGridViewTickets.Columns["Open"].Visible = false;
            }
        }
        private void BindOpenTickets()
        {

            try
            {

                using (ShellEntities context = new ShellEntities())
                {
                    var ticketMasterGas = context.TBL_TicketCreatedMaster.Where(s => s.IsActive == true && s.IsClosed == false).Select(s => new { s.TicketCode, TransactionDate = s.TicketDate, s.TransactionType, s.Reason, s.TicketRemarks }).ToList();
                    dataGridViewTickets.DataSource = ticketMasterGas;
                    DisableOpenGridButton();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void BindFilterTickets()
        {
            try
            {
                string ticketDateTran = Convert.ToDateTime(dateTimePickerTranDate.Text).ToString("MMddyyyy");
                string ticketCode = Convert.ToDateTime(dateTimePickerTranDate.Text).ToString("MMddyyyy") + "_" + comboBoxTranType.Text;

                using (ShellEntities context = new ShellEntities())
                {
                    var ticketMasterGas = context.TBL_TicketCreatedMaster.ToList();
                    ticketMasterGas = ticketMasterGas.Where(s => Convert.ToDateTime(s.TicketDate).ToString("MMddyyy") == ticketDateTran && s.IsActive == true && s.IsClosed == false).ToList();
                    if (comboBoxTranType.SelectedIndex > 0)
                    {
                        ticketMasterGas = ticketMasterGas.Where(s => s.TransactionType == comboBoxTranType.Text).ToList();
                    }
                    if (ReasoncomboBox.SelectedIndex > 0)
                    {
                        ticketMasterGas = ticketMasterGas.Where(s => s.Reason == ReasoncomboBox.Text).ToList();
                    }
                    if (ticketMasterGas.Count > 0)
                    {
                        var ticketList = ticketMasterGas.Select(s => new { s.TicketCode, TransactionDate = s.TicketDate, s.TransactionType, s.Reason, s.TicketRemarks }).ToList();
                        dataGridViewTickets.DataSource = ticketList;
                        DisableOpenGridButton();
                    }
                    else
                    {
                        dataGridViewTickets.DataSource = null;
                        MessageBox.Show("There is no pending tickets");
                    }



                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void CreateTicket()
        {
            try
            {
                if (comboBoxTranType.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select transaction type.");
                    return;
                }
                if (ReasoncomboBox.SelectedIndex <= 0)
                {
                    MessageBox.Show("Please select reason.");
                    return;
                }
                string ticketDateTran = Convert.ToDateTime(dateTimePickerTranDate.Text).ToString("MMddyyyy");
                string ticketCode = Convert.ToDateTime(dateTimePickerTranDate.Text).ToString("MMddyyyy") + "_" + comboBoxTranType.Text;
                using (ShellEntities context = new ShellEntities())
                {
                    TBL_TicketCreatedMaster ticketMasterGas = context.TBL_TicketCreatedMaster.FirstOrDefault(s => s.TicketCode == ticketCode);
                    if (ticketMasterGas == null)
                    {
                        ticketMasterGas = new TBL_TicketCreatedMaster();
                        ticketMasterGas.TransactionType = comboBoxTranType.Text;
                        ticketMasterGas.TicketDate = Convert.ToDateTime(dateTimePickerTranDate.Text);
                        ticketMasterGas.Reason = ReasoncomboBox.Text;
                        ticketMasterGas.IsActive = true;
                        ticketMasterGas.IsClosed = false;
                        ticketMasterGas.CreatedBy = 1;
                        ticketMasterGas.CreatedDate = DateTime.Now;
                        ticketMasterGas.TicketRemarks = richTextBoxRemarks.Text;
                        ticketMasterGas.TicketCode = ticketCode;
                        context.TBL_TicketCreatedMaster.Add(ticketMasterGas);
                        context.SaveChanges();
                        textBoxTicketCode.Text = ticketMasterGas.TicketCode;
                        MessageBox.Show("Ticket created.");
                    }

                    else
                    {
                        MessageBox.Show("Ticket Code : " + ticketMasterGas.TicketCode + " is already open for this date, please close this ticket first.");
                    }


                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UpdateTicket()
        {
            using (ShellEntities context = new ShellEntities())
            {
                TBL_TicketCreatedMaster ticketMasterGas = context.TBL_TicketCreatedMaster.FirstOrDefault(s => s.TicketCode == ticketCode);
                if (ticketMasterGas != null)
                {
                    ticketMasterGas.TransactionType = comboBoxTranType.Text;
                    ticketMasterGas.TicketDate = Convert.ToDateTime(dateTimePickerTranDate.Text);
                    ticketMasterGas.Reason = ReasoncomboBox.Text;
                    ticketMasterGas.IsActive = true;
                    ticketMasterGas.IsClosed = false;
                    ticketMasterGas.ModiefiedBy = 1;
                    ticketMasterGas.ModifiedDate = DateTime.Now;
                    ticketMasterGas.TicketRemarks = richTextBoxRemarks.Text;
                    ticketMasterGas.TicketCode = ticketCode;
                    context.SaveChanges();
                    textBoxTicketCode.Text = ticketMasterGas.TicketCode;
                    MessageBox.Show("Ticket updated.");
                }
                else
                {
                    MessageBox.Show("Ticket code doesn't exist");
                }
            }

        }

        private void DeleteTicket()
        {
            using (ShellEntities context = new ShellEntities())
            {
                TBL_TicketCreatedMaster ticketMasterGas = context.TBL_TicketCreatedMaster.FirstOrDefault(s => s.TicketCode == ticketCode);
                if (ticketMasterGas != null)
                {
                    ticketMasterGas.IsActive = false;
                    ticketMasterGas.ModiefiedBy = 1;
                    ticketMasterGas.ModifiedDate = DateTime.Now;
                    context.SaveChanges();
                    MessageBox.Show("Ticket deleted.");
                }
                else
                {
                    MessageBox.Show("Ticket code doesn't exist");
                }
            }
        }
        private void FillDetails(string ticketCode)
        {
            using (ShellEntities context = new ShellEntities())
            {
                TBL_TicketCreatedMaster ticketMasterGas = context.TBL_TicketCreatedMaster.FirstOrDefault(s => s.TicketCode == ticketCode && s.IsActive == true && s.IsClosed == false);
                if (ticketMasterGas != null)
                {
                    comboBoxTranType.Text = ticketMasterGas.TransactionType;
                    dateTimePickerTranDate.Text = ticketMasterGas.TicketDate.ToString();
                    ReasoncomboBox.Text = ticketMasterGas.Reason;
                    richTextBoxRemarks.Text = ticketMasterGas.TicketRemarks;
                    textBoxTicketCode.Text = ticketMasterGas.TicketCode;
                }
                else
                {
                    MessageBox.Show("Ticket code doesn't exist");
                }


            }
        }
        private bool ValidateTransaction()
        {
            if (comboBoxTranType.SelectedIndex > 0)
            {
                try
                {
                    using (ShellEntities context = new ShellEntities())
                    {
                        string tranDate = Convert.ToDateTime(dateTimePickerTranDate.Text).ToString("MMddyyyy");
                        var gasTrans = context.MST_FuelTransaction.ToList();
                        var gasTransSingle = gasTrans.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).FirstOrDefault();
                        if (gasTransSingle != null)
                        {
                            switch (comboBoxTranType.Text)
                            {
                                case "Sale":
                                    if (gasTransSingle.C_StoreSale != null)
                                    {
                                        return true;
                                    }

                                    break;
                                case "Tabbocco":
                                    if (gasTransSingle.Tabbacco != null)
                                    {
                                        return true;
                                    }
                                    break;
                                case "Cash":
                                    if (gasTransSingle.CashPaidForStorePur != null)
                                    {
                                        return true;
                                    }
                                    break;
                                case "Fuel":
                                    if (gasTransSingle.Bronze != null)
                                    {
                                        return true;
                                    }
                                    break;
                                case "Settlement":
                                    if (gasTransSingle.RadiantDebit != null)
                                    {
                                        return true;
                                    }
                                    break;
                            }

                        }
                        else
                        {
                            MessageBox.Show("Already tickets are open..");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            else
            {
                MessageBox.Show("Please select transaction type of gas.");
            }

            return true;
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateTransaction())
            {
                CreateTicket();
                BtnSave.Enabled = true;
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
            }
            else
            {
                MessageBox.Show("Please contact admin.");
            }


        }

        string ticketCode = "";
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateTicket();
                BindOpenTickets();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteTicket();
                BindOpenTickets();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                BindFilterTickets();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GasTrasTicket_Load(object sender, EventArgs e)
        {
            try
            {
                BindOpenTickets();
                BtnSave.Enabled = true;
                btnUpdate.Enabled = false;
                BtnDelete.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dateTimePickerTranDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                //  BindTickets();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridViewTickets_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0 && e.ColumnIndex > 0)
                {
                    ticketCode = dataGridViewTickets.Rows[e.RowIndex].Cells[1].Value.ToString();
                    FillDetails(ticketCode);
                    BtnSave.Enabled = false;
                    btnUpdate.Enabled = true;
                    BtnDelete.Enabled = true;
                }
                else if (e.RowIndex >= 0 && e.ColumnIndex ==0)
                {
                    using (ShellEntities context = new ShellEntities())
                    {
                        string code = dataGridViewTickets.Rows[e.RowIndex].Cells[1].Value.ToString();
                        TBL_TicketCreatedMaster ticketMasterGas = context.TBL_TicketCreatedMaster.FirstOrDefault(s => s.TicketCode == code);
                        if (ticketMasterGas != null)
                        {
                            ShellComman.Shell_TicketCode = code;
                            ShellComman.Shell_Gas_TransactionDate = Convert.ToDateTime(ticketMasterGas.TicketDate);
                            ShellComman.Ticket_Request_Type = ticketMasterGas.Reason;
                            this.Close();
                            frmGasTrans childForm = new frmGasTrans();
                            MDIParentShell parent = new MDIParentShell();
                            childForm.MdiParent = parent.MdiParent;
                            childForm.Show();
                        }
                        else
                        {
                            MessageBox.Show("Ticket number is invalid..");
                        }
                    }
                }


               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
