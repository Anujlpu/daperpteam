﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;

namespace Shell
{
    public partial class FrmPaymnetToEmployee : Form
    {
        private int FormType { get; set; }
        public FrmPaymnetToEmployee(int type)
        {
            InitializeComponent();
            FormType = type;

        }


        private void TXTKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }


        }
        public int _paymnetId;
        int paymnetType = 0;
        int role = 0;
        private void FrmPaymnetToEmployee_Load(object sender, EventArgs e)
        {
            int paymnetType = 0;
            int role = 0;
            if (FormType == 1)
            {
                this.Name = "Payment To Management";
                lblEmpName.Text = "Mngt Person";
                grpManageVendor.Text = "Cash To Management";
                paymnetType = 2;
                role = 2;

            }
            else
            {
                this.Name = "Payment To Employee";
                lblEmpName.Text = "Employee Name";
                grpManageVendor.Text = "Cash To Employee";
                paymnetType = 3;
                role = 3;
            }
            BindGridView(paymnetType);
            binddropdown(role);
        }


        private void BtnDelete_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount No");
                return;
            }

            BAL_PaymentToVendor _obj = new BAL_PaymentToVendor();
            int Result = _obj.DeletePayment(_paymnetId, Utility.BA_Commman._userId);
            if (Result == 1)
            {
                MessageBox.Show("Invoice Registered Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount No");
                return;
            }

            BAL_PaymentToVendor _obj = new BAL_PaymentToVendor();
            int Result = _obj.Managepaidpayment(_paymnetId, dtpPayment.Value, 1, int.Parse(ddlEmpName.SelectedValue.ToString()), int.Parse(ddlPaymnetType.SelectedValue.ToString()), Convert.ToDouble(txtAmount.Text), txtRemarks.Text, "", txtVoucherNo.Text, Utility.BA_Commman._userId, 2, 0, 0);
            if (Result == 1)
            {
                MessageBox.Show("Invoice Registered Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtVoucherNo.Text))
            {
                MessageBox.Show("Please Enter the Invoice No.");
                return;
            }
            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount No");
                return;
            }
            if (pdfUploadbox.Text == "" && txtRemarks.Text == "")
            {
                MessageBox.Show("Pdf Not Uploaded. Please Enter Remarks!");
                txtRemarks.Focus();
                return;
            }
            if (this.Name == "Payment To Employee")
                paymnetType = 3;
            else
                paymnetType = 2;
            BAL_PaymentToVendor _obj = new BAL_PaymentToVendor();
            int Result = _obj.Managepaidpayment(0, dtpPayment.Value, paymnetType, int.Parse(ddlEmpName.SelectedValue.ToString()), int.Parse(ddlPaymnetType.SelectedValue.ToString()), Convert.ToDouble(txtAmount.Text), txtRemarks.Text, "", txtVoucherNo.Text, Utility.BA_Commman._userId, 1, 0, 0);
            if (Result == 1)
            {
                MessageBox.Show("Invoice Registered Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        public void ClearValue()
        {
            txtAmount.Text = "0";
            txtRemarks.Text = "";
            txtVoucherNo.Text = "";
            BtnDelete.Enabled = false;
            btnUpdate.Enabled = false;
            binddropdown(paymnetType);
            BindGridView(role);
        }
        private void btnbrowse_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void BindGridView(int paymnetType)
        {
            BAL_PaymentToVendor _objEmployee = new BAL_PaymentToVendor();
            dgvVendorList.DataSource = _objEmployee.GetPaidPaymentlist().Where(p => p.TransactionType == paymnetType).ToList();


        }
        public void binddropdown(int Role)
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            ddlPaymnetType.DataSource = _objVendor.GetPaymentlist();
            ddlPaymnetType.DisplayMember = "PaymenttypeCode";
            ddlPaymnetType.ValueMember = "PaymentTypeId";
            BAL_Employee _objemp = new BAL_Employee();
            IEnumerable<DAL.EmployeeEntity> lst;
            if (_objemp.GetEmployeelist().Count > 0)
            {
                if (Role == 2)

                    ddlEmpName.DataSource = _objemp.GetEmployeelist(Role);
                else
                    ddlEmpName.DataSource = _objemp.GetEmployeelist(3);



                ddlEmpName.DisplayMember = "EmployeeName";
                ddlEmpName.ValueMember = "EmployeeId";
            }
        }
        string str = "0123456789.";

        private void dgvVendorList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    txtAmount.Text = dgvVendorList.Rows[e.RowIndex].Cells["Amount"].Value.ToString();
                    txtRemarks.Text = dgvVendorList.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();
                    txtVoucherNo.Text = dgvVendorList.Rows[e.RowIndex].Cells["InvoiceNo"].Value.ToString();
                    _paymnetId = int.Parse(dgvVendorList.Rows[e.RowIndex].Cells["PaymentId"].Value.ToString());
                    BtnDelete.Enabled = true;
                    btnUpdate.Enabled = true;
                }
                else
                {
                    BtnDelete.Enabled = false;
                    btnUpdate.Enabled = false;
                }
            }
        }

        private void btnbrowse_Click_1(object sender, EventArgs e)
        {


            OpenFileDialog Openpdf = new OpenFileDialog();
            Openpdf.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (Openpdf.ShowDialog() == DialogResult.OK)
            {
                string pdfLog = Openpdf.FileName.ToString();
                pdfUploadbox.Text = pdfLog;
            }

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (pdfUploadbox.Text == "" && txtRemarks.Text == "")
            {
                MessageBox.Show("file Not Uploaded. Please Enter Remarks!");
                txtRemarks.Focus();
                return;
            }
        }
    }
}
