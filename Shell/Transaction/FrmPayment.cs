﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
namespace Shell
{
    public partial class FrmPayment : Form
    {
        public FrmPayment()
        {
            InitializeComponent();
        }
        public void binddropdown()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            ddlPaymentType.DataSource = _objVendor.GetPaymentlist();
            ddlPaymentType.DisplayMember = "PaymenttypeCode";
            ddlPaymentType.ValueMember = "PaymentTypeId";
            ddlVendorName.DataSource = _objVendor.GetVendorlist();
            ddlVendorName.DisplayMember = "VendorName";
            ddlVendorName.ValueMember = "VendorId";
        }
        string str = "0123456789.";
        private void TXTKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }


        }
        public int _paymnetId;
        private void FrmPayment_Load(object sender, EventArgs e)
        {
            binddropdown();
            BindGridView("0", "");
        }
        public void BindGridView(string Searchvalue, string Searchtype)
        {
            BAL_PaymentToVendor _objEmployee = new BAL_PaymentToVendor();
            if (Searchvalue == "0")
            {
                dgvVendorList.DataSource = _objEmployee.GetPaidPaymentlist();
            }

            else if (Searchvalue == "Invoice")
            {
                dgvVendorList.DataSource = _objEmployee.GetPaidPaymentlist().Where(p => p.InvoiceNo == Searchvalue).ToList();
            }
            else if (Searchvalue == "Date")
            {
                dgvVendorList.DataSource = _objEmployee.GetPaidPaymentlist().Where(p => p.PaymentDate == Convert.ToDateTime(Searchvalue)).ToList();
            }
            else if (Searchvalue == "Paytmenttype")
            {
                dgvVendorList.DataSource = _objEmployee.GetPaidPaymentlist().Where(p => p.PaymentType.ToString() == ddlPaymentType.SelectedValue.ToString()).ToList();
            }
        }
        public void FillInvoiceAmt()
        {
            BAL_PaymentToVendor _objEmployee = new BAL_PaymentToVendor();
            _objEmployee.GetPaidPaymentlist();
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtInoiceNo.Text))
            {
                MessageBox.Show("Please Enter the Invoice No.");
                return;
            }
            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount No");
                return;
            }
            if (pdfUploadbox.Text == "")
            {
                MessageBox.Show("Pdf Not Uploaded. Please Enter Remarks!");
                txtRemarks.Focus();
                return;
            }
            BAL_PaymentToVendor _obj = new BAL_PaymentToVendor();
            int Result = _obj.DeletePayment(_paymnetId, Utility.BA_Commman._userId);
            if (Result == 1)
            {
                MessageBox.Show("Invoice Registered Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtInoiceNo.Text))
            {
                MessageBox.Show("Please Enter the Invoice No.");
                return;
            }
            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount No");
                return;
            } if (pdfUploadbox.Text == "" && txtRemarks.Text == "")
            {
                MessageBox.Show("Pdf Not Uploaded. Please Enter Remarks!");
                txtRemarks.Focus();
                return;
            }

            BAL_PaymentToVendor _obj = new BAL_PaymentToVendor();
            int Result = _obj.Managepaidpayment(_paymnetId, dtpPaymentDate.Value, 1, int.Parse(ddlVendorName.SelectedValue.ToString()), int.Parse(ddlPaymentType.SelectedValue.ToString()), Convert.ToDouble(txtAmount.Text), txtRemarks.Text, "", txtInoiceNo.Text, Utility.BA_Commman._userId, 2, Convert.ToDouble(txtcashpaidamt.Text), Convert.ToDouble(txtcashPayoutAmt.Text));
            if (Result == 1)
            {
                MessageBox.Show("Invoice Updated Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtInoiceNo.Text))
            {
                MessageBox.Show("Please Enter the Invoice No.");
                return;
            }
            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount No");
                return;
            }
            if (pdfUploadbox.Text == "" && txtRemarks.Text == "")
            {
                MessageBox.Show("Pdf Not Uploaded. Please Enter Remarks!");
                txtRemarks.Focus();
                return;
            }

            BAL_PaymentToVendor _obj = new BAL_PaymentToVendor();
            int Result = _obj.Managepaidpayment(0, dtpPaymentDate.Value, 1, int.Parse(ddlVendorName.SelectedValue.ToString()), int.Parse(ddlPaymentType.SelectedValue.ToString()), Convert.ToDouble(txtAmount.Text), txtRemarks.Text, "", txtInoiceNo.Text, Utility.BA_Commman._userId, 1, Convert.ToDouble(txtcashpaidamt.Text), Convert.ToDouble(txtcashPayoutAmt.Text));
            if (Result == 1)
            {
                MessageBox.Show("Invoice Registered Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        public void ClearValue()
        {
            txtAmount.Text = "0";
            txtRemarks.Text = "";
            txtInoiceNo.Text = "";
            txtInvoiceBal.Text = "0";
            txtcashPayoutAmt.Text = "0";
            txtcashpaidamt.Text = "0";
            BtnDelete.Enabled = false;
            button1.Enabled = false;
            BtnSave.Enabled = true;
            binddropdown();
            BindGridView("0", "");
        }
        private void btnbrowse_Click(object sender, EventArgs e)
        {

            OpenFileDialog Openpdf = new OpenFileDialog();
            Openpdf.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (Openpdf.ShowDialog() == DialogResult.OK)
            {
                string pdfLog = Openpdf.FileName.ToString();
                pdfUploadbox.Text = pdfLog;
            }


        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dgvVendorList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    txtAmount.Text = dgvVendorList.Rows[e.RowIndex].Cells["Amount"].Value.ToString();
                    txtRemarks.Text = dgvVendorList.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();
                    txtInoiceNo.Text = dgvVendorList.Rows[e.RowIndex].Cells["InvoiceNo"].Value.ToString();
                    txtcashpaidamt.Text = dgvVendorList.Rows[e.RowIndex].Cells["CashAmount"].Value.ToString();
                    txtcashPayoutAmt.Text = dgvVendorList.Rows[e.RowIndex].Cells["CashPayoutAmount"].Value.ToString();
                    _paymnetId = int.Parse(dgvVendorList.Rows[e.RowIndex].Cells["Paymentid"].Value.ToString());
                    BtnDelete.Enabled = true;
                    button1.Enabled = true;
                    BtnSave.Enabled = false;
                    UpdateBalance();
                }
                else
                {
                    BtnDelete.Enabled = false;
                    button1.Enabled = false;
                    BtnSave.Enabled = true;
                }
            }
        }

        private void txtInoiceNo_Leave(object sender, EventArgs e)
        {
            BindGridView("Inoice", txtInoiceNo.Text);
        }


        private void UpdateBalance()
        {
            double InvAmount = 0.0, CashAmt = 0.0, Payoutamt = 0.0, BalAmt = 0.0;

            InvAmount = Convert.ToDouble(txtAmount.Text);
            CashAmt = Convert.ToDouble(txtcashpaidamt.Text);
            Payoutamt = Convert.ToDouble(txtcashPayoutAmt.Text);
            if ((CashAmt + Payoutamt) > InvAmount)
            {
                MessageBox.Show("Paid Amount Can not be greater than Invoice Amount");
                txtcashpaidamt.Text = "0";
                txtcashPayoutAmt.Text = "0";
                txtInvoiceBal.Text = "0";
                return;
            }
            BalAmt = (InvAmount - (CashAmt + Payoutamt));
            txtInvoiceBal.Text = BalAmt.ToString("0.00");

        }
        private void txtAmount_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            //Changetxt();
            UpdateBalance();


        }

        private void Changetxt()
        {
            if (ddlPaymentType.SelectedValue.ToString() == "1")
            {
                txtcashPayoutAmt.Text = "0";
                txtcashPayoutAmt.ReadOnly = true;
            }
            else if (ddlPaymentType.SelectedValue.ToString() == "2")
            {
                txtcashPayoutAmt.Text = "0"; txtcashpaidamt.Text = "0";
                txtcashPayoutAmt.ReadOnly = false;
                txtcashpaidamt.ReadOnly = false;
            }
            else if (ddlPaymentType.SelectedValue.ToString() == "3")
            {
                txtcashpaidamt.Text = "0";
                txtcashPayoutAmt.ReadOnly = false;
                txtcashpaidamt.ReadOnly = true;
            }
        }

        private void ddlPaymentType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Changetxt();
            BindGridView("Paytmenttype", ddlPaymentType.SelectedText);
        }

        private void dtpPaymentDate_ValueChanged(object sender, EventArgs e)
        {
            BindGridView("Date", dtpPaymentDate.Value.ToString());
        }
    }
}
