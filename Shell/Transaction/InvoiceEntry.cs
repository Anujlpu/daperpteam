﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using DAL;
using Framework.Data;
using Framework;

namespace Shell
{
    public partial class InvoiceEntry : BaseForm
    {
        public InvoiceEntry()
        {
            InitializeComponent();
        }
        BAL_InvoiceEntries obj = new BAL_InvoiceEntries();
        BAL_FuelTransaction _objDoc;
        public void bindGridview(char IsEdit)
        {

            dgvInvoiceList.DataSource = obj.getInvoiceDetails(IsEdit, 0).Tables[0];
        }
        private void UpdateBalance()
        {
            double InvAmount = 0.0, CashAmt = 0.0, Payoutamt = 0.0, BalAmt = 0.0;

            InvAmount = Convert.ToDouble(txtAmount.Text);
            CashAmt = Convert.ToDouble(txtcashpaidamt.Text);
            Payoutamt = Convert.ToDouble(txtcashPayoutAmt.Text);
            if ((CashAmt + Payoutamt) > InvAmount)
            {
                MessageBox.Show("Paid Amount Can not be greater than Invoice Amount");
                txtcashpaidamt.Text = "0.00";
                txtcashPayoutAmt.Text = "0.00";
                txtInvoiceBal.Text = "0.00";
                return;
            }
            BalAmt = (InvAmount - (CashAmt + Payoutamt));
            txtInvoiceBal.Text = BalAmt.ToString("0.00");

        }
        private void txtAmount_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0.00";

            }
            //Changetxt();
            UpdateBalance();


        }
        public void binddropdown1()
        {
            dgvUploadedDocs.DataSource = null;
            _objDoc = new BAL.BAL_FuelTransaction();

            List<DocsEntity> lst = _objDoc.ListGetDocsList(dtpPayment.Value, "Invoices");
            if (lst.Count > 0)
            {
                dgvUploadedDocs.DataSource = lst;
            }
        }
        private void GridAddColumn()
        {
            if (dgvUploadedDocs.DataSource != null)
            {
                dgvUploadedDocs.Columns[0].Visible = false;
                dgvUploadedDocs.Columns.Insert(1, new DataGridViewCheckBoxColumn());
                dgvUploadedDocs.Columns[1].HeaderText = "Select";
                dgvUploadedDocs.Columns[1].ReadOnly = false;
                dgvUploadedDocs.Columns[2].ReadOnly = true;
                dgvUploadedDocs.Columns[1].Width = 50;
                dgvUploadedDocs.Columns[2].Visible = false;
                dgvUploadedDocs.Columns[3].Visible = false;
                dgvUploadedDocs.Columns[4].Visible = false;
            }

        }
        private void InvoiceEntry_Load(object sender, EventArgs e)
        {
            try
            {
                binddropdown();
                bindGridview('V');
                binddropdown1();
                GridAddColumn();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        private string _cklInvoice = string.Empty;

        private void dgvCstore_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                DataGridView dgv = ((DataGridView)(sender));
                if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[0].Value) == false)
                {
                    dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
                }

                string gridselval = "";
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    if (Convert.ToBoolean(r.Cells[0].Value))
                    {
                        int i = r.Index;

                        gridselval = gridselval + "," + dgv.Rows[i].Cells[1].Value.ToString();

                    }

                }
                if (gridselval.Length > 0)
                {
                    gridselval = gridselval.Remove(0, 1);
                }

                _cklInvoice = gridselval;

            }
        }

        public void binddropdown()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var paymentType = _objVendor.GetPaymentlist();
            paymentType.Insert(0, new PaymentEntity { PaymentTypeId = 0, PaymentTypeCode = "--Select--" });
            ddlPaymnetType.DataSource = paymentType;
            ddlPaymnetType.DisplayMember = "PaymenttypeCode";
            ddlPaymnetType.ValueMember = "PaymentTypeId";
            var vendors = _objVendor.GetVendorlist();
            vendors.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            ddlVendorName.DataSource = vendors;
            ddlVendorName.DisplayMember = "VendorName";
            ddlVendorName.ValueMember = "VendorID";
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtInvoiceNo.Text))
                {
                    MessageBox.Show("Please Enter the invoice No"); txtInvoiceNo.Focus();
                    return;
                }

                int Result = obj.DeleteInvoice(InvoiceId);
                if (Result == 1)
                {
                    MessageBox.Show("Entries Deleted Sucessfully");

                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        private List<string> docIds = new List<string>();
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtInvoiceNo.Text))
                {
                    MessageBox.Show("Please Enter the invoice No"); txtInvoiceNo.Focus();
                    return;
                }
                if (_cklInvoice == "" && string.IsNullOrEmpty(txtRemarks.Text))
                {
                    MessageBox.Show("Please Select the invoice or Enter Remarks");
                    return;

                }
                if (String.IsNullOrEmpty(txtInvoiceBal.Text) || txtInvoiceBal.Text == "0")
                {
                    MessageBox.Show("Enter the Invoice Amount");
                    return;
                }
                string TransactionType = string.Empty;
                if (rbtCredit.Checked == true)
                    TransactionType = "Credit";
                else
                    TransactionType = "Debit";

                int Result = 0;
                Result = obj.UpdateInvoice(InvoiceId, dtpPayment.Value, int.Parse(ddlVendorName.SelectedValue.ToString()), txtInvoiceNo.Text, 0, int.Parse(ddlPaymnetType.SelectedValue.ToString()), TransactionType, Convert.ToDouble(txtGst.Text), Convert.ToDouble(txtAmount.Text)
                    , Convert.ToDouble(txtcashpaidamt.Text), Convert.ToDouble(txtcashPayoutAmt.Text), Convert.ToDouble(txtInvoiceBal.Text), txtRemarks.Text, true, _cklInvoice);
                if (Result == 1)
                {
                    using (ShellEntities context = new ShellEntities())
                    {
                        foreach (DataGridViewRow row in dgvUploadedDocs.Rows)
                        {
                            if (Convert.ToBoolean(row.Cells[0].Value) == true)
                            {
                                int docId = Convert.ToInt32((row.Cells[1].Value));
                                docIds.Add(row.Cells[1].Value.ToString());
                                var docExist = context.Tbl_InvoiceEntryDocuments.FirstOrDefault(s => s.UploadedDocID == docId && s.InvoiceID == InvoiceId);
                                if (docExist == null)
                                {
                                    Tbl_InvoiceEntryDocuments docsObj = new Tbl_InvoiceEntryDocuments();
                                    docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                                    docsObj.InvoiceID = InvoiceId;
                                    docsObj.IsActive = true;
                                    docsObj.CreatedBy = 1;
                                    docsObj.CreatedDate = DateTime.Now;
                                    context.Tbl_InvoiceEntryDocuments.Add(docsObj);
                                }

                            }
                        }
                        context.SaveChanges();
                    }
                    MessageBox.Show("Entries Created Sucessfully");

                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        public void ClearValue()
        {
            bindGridview('V');
            binddropdown();
            txtInvoiceNo.Text = "";
            txtRemarks.Text = "";
            txtInvoiceBal.Text = "";
            txtGst.Text = "";
            txtcashPayoutAmt.Text = "";
            txtcashpaidamt.Text = "";
            txtAmount.Text = "";
            binddropdown();

        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtInvoiceNo.Text))
                {
                    MessageBox.Show("Please Enter the invoice No"); txtInvoiceNo.Focus();
                    return;
                }
                if (_cklInvoice == "" && string.IsNullOrEmpty(txtRemarks.Text))
                {
                    MessageBox.Show("Please Select the invoice or Enter Remarks");
                    return;

                }
                if (String.IsNullOrEmpty(txtInvoiceBal.Text) || txtInvoiceBal.Text == "0")
                {
                    MessageBox.Show("Enter the Invoice Amount");
                    return;
                }
                string TransactionType = string.Empty;
                if (rbtCredit.Checked == true)
                    TransactionType = "Credit";
                else
                    TransactionType = "Debit";

                int Result = 0;
                Result = obj.InsertInvoice(dtpPayment.Value, int.Parse(ddlVendorName.SelectedValue.ToString()), txtInvoiceNo.Text, 0, int.Parse(ddlPaymnetType.SelectedValue.ToString()), TransactionType, Convert.ToDouble(txtGst.Text), Convert.ToDouble(txtAmount.Text)
                   , Convert.ToDouble(txtcashpaidamt.Text), Convert.ToDouble(txtcashPayoutAmt.Text), Convert.ToDouble(txtInvoiceBal.Text), txtRemarks.Text, _cklInvoice);
                if (Result == 1)
                {
                    using (ShellEntities context = new ShellEntities())
                    {
                        var lastInvoice = context.Tbl_Invoices.Where(s => s.IsActive == true && s.IsClosed == false).OrderByDescending(s => s.InvoiceId).Select(s => s.InvoiceId).FirstOrDefault();
                        foreach (DataGridViewRow row in dgvUploadedDocs.Rows)
                        {
                            if (Convert.ToBoolean(row.Cells[0].Value) == true)
                            {
                                docIds.Add(row.Cells[1].Value.ToString());
                                Tbl_InvoiceEntryDocuments docsObj = new Tbl_InvoiceEntryDocuments();
                                docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                                docsObj.InvoiceID = lastInvoice;
                                docsObj.IsActive = true;
                                docsObj.CreatedBy = 1;
                                docsObj.CreatedDate = DateTime.Now;
                                context.Tbl_InvoiceEntryDocuments.Add(docsObj);
                            }
                        }
                        context.SaveChanges();
                    }
                    MessageBox.Show("Invoice Created Sucessfully");

                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        private void txtVendorCode_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnbrowse_Click(object sender, EventArgs e)
        {

        }
        private int InvoiceId = 0;
        private void dgvInvoiceList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dg == DialogResult.Yes)
                    {
                        InvoiceId = int.Parse(dgvInvoiceList.Rows[e.RowIndex].Cells[0].Value.ToString());
                        if (dgvInvoiceList.Rows[e.RowIndex].Cells["Isclosed"].Value.ToString().ToUpper() == "TRUE")
                        {
                            MessageBox.Show("Entries has been Closed User can Not Modify it.");
                            return;
                        }
                        DataSet ds = obj.getInvoiceDetails('E', InvoiceId);
                        if (ds.Tables[0].Rows.Count > 0)
                        {

                            ddlPaymnetType.SelectedValue = int.Parse(ds.Tables[0].Rows[0]["PaymentType"].ToString());
                            if (ds.Tables[0].Rows[0]["VendorId"].ToString() != "0")
                            {
                                ddlVendorName.SelectedValue = int.Parse(ds.Tables[0].Rows[0]["VendorId"].ToString());
                                //rbtVendor.Checked = true;
                                //rbtPNL.Checked = false;
                            }
                            else
                            {
                                //ddlPnlType.SelectedValue = int.Parse(ds.Tables[0].Rows[0]["PNLType"].ToString());
                                //rbtPNL.Checked = true;
                                //rbtVendor.Checked = false;
                            }
                            string TransactionType = ds.Tables[0].Rows[0]["TransactionType"].ToString();
                            if (TransactionType.Trim() == "Debit")
                                rbtDebit.Checked = true;
                            else
                                rbtCredit.Checked = false;
                            txtInvoiceNo.Text = ds.Tables[0].Rows[0]["InvoiceNo"].ToString();
                            txtGst.Text = ds.Tables[0].Rows[0]["GST"].ToString();
                            txtAmount.Text = ds.Tables[0].Rows[0]["TotalAmount"].ToString();
                            txtcashpaidamt.Text = ds.Tables[0].Rows[0]["Cash"].ToString();
                            txtcashPayoutAmt.Text = ds.Tables[0].Rows[0]["Payout"].ToString();
                            txtRemarks.Text = ds.Tables[0].Rows[0]["Remarks"].ToString();
                            string dgvalue = ds.Tables[0].Rows[0]["UploadPath"].ToString();
                            if (!string.IsNullOrEmpty(dgvalue))
                            {
                                for (int i = 0; i < dgvalue.Split(',').Length; i++)
                                {
                                    foreach (DataGridViewRow item in dgvUploadedDocs.Rows)
                                    {
                                        if (item.Cells["docid"].Value.ToString() == dgvalue[i].ToString())
                                        {
                                            item.Cells[0].Value = true;

                                        }
                                    }
                                }

                            }
                            BtnDelete.Enabled = true;
                            btnUpdate.Enabled = true;
                            BtnSave.Enabled = false;
                            UpdateBalance();
                        }
                    }
                    else
                    {
                        BtnDelete.Enabled = false;
                        btnUpdate.Enabled = false;
                        BtnSave.Enabled = true;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        string str = "0123456789.";
        private void TXTKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }


        }

        private void rbtPNL_CheckedChanged(object sender, EventArgs e)
        {
            //if (rbtPNL.Checked == true)
            //{
            //    ddlPnlType.Enabled = true;
            //    ddlVendorName.Enabled = false;
            //}

        }

        private void rbtVendor_CheckedChanged(object sender, EventArgs e)
        {

            //if (rbtVendor.Checked == true)
            //{
            //    ddlPnlType.Enabled = false;
            //    ddlVendorName.Enabled = false;
            //}
        }



    }
}
