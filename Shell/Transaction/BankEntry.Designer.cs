﻿namespace Shell
{
    partial class BankEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpManageVendor = new System.Windows.Forms.GroupBox();
            this.BankListcomboBox = new System.Windows.Forms.ComboBox();
            this.dgvCstore = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbtDebit = new System.Windows.Forms.RadioButton();
            this.rbtcredit = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.dtpPayment = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grpCashMngtList = new System.Windows.Forms.GroupBox();
            this.dgvBankentries = new System.Windows.Forms.DataGridView();
            this.grpManageVendor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCstore)).BeginInit();
            this.panel2.SuspendLayout();
            this.grpCashMngtList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankentries)).BeginInit();
            this.SuspendLayout();
            // 
            // grpManageVendor
            // 
            this.grpManageVendor.Controls.Add(this.BankListcomboBox);
            this.grpManageVendor.Controls.Add(this.dgvCstore);
            this.grpManageVendor.Controls.Add(this.label16);
            this.grpManageVendor.Controls.Add(this.panel2);
            this.grpManageVendor.Controls.Add(this.label2);
            this.grpManageVendor.Controls.Add(this.txtAmount);
            this.grpManageVendor.Controls.Add(this.label6);
            this.grpManageVendor.Controls.Add(this.label62);
            this.grpManageVendor.Controls.Add(this.txtRemarks);
            this.grpManageVendor.Controls.Add(this.dtpPayment);
            this.grpManageVendor.Controls.Add(this.label1);
            this.grpManageVendor.Controls.Add(this.btnExit);
            this.grpManageVendor.Controls.Add(this.label10);
            this.grpManageVendor.Controls.Add(this.label8);
            this.grpManageVendor.Controls.Add(this.BtnDelete);
            this.grpManageVendor.Controls.Add(this.BtnSave);
            this.grpManageVendor.Controls.Add(this.btnUpdate);
            this.grpManageVendor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageVendor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageVendor.ForeColor = System.Drawing.Color.Black;
            this.grpManageVendor.Location = new System.Drawing.Point(14, 14);
            this.grpManageVendor.Name = "grpManageVendor";
            this.grpManageVendor.Size = new System.Drawing.Size(456, 560);
            this.grpManageVendor.TabIndex = 48;
            this.grpManageVendor.TabStop = false;
            this.grpManageVendor.Text = "Bank Statement Entry";
            // 
            // BankListcomboBox
            // 
            this.BankListcomboBox.FormattingEnabled = true;
            this.BankListcomboBox.Location = new System.Drawing.Point(141, 89);
            this.BankListcomboBox.Name = "BankListcomboBox";
            this.BankListcomboBox.Size = new System.Drawing.Size(235, 23);
            this.BankListcomboBox.TabIndex = 205;
            // 
            // dgvCstore
            // 
            this.dgvCstore.AllowDrop = true;
            this.dgvCstore.AllowUserToAddRows = false;
            this.dgvCstore.AllowUserToDeleteRows = false;
            this.dgvCstore.AllowUserToResizeColumns = false;
            this.dgvCstore.AllowUserToResizeRows = false;
            this.dgvCstore.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCstore.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCstore.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCstore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCstore.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvCstore.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvCstore.Location = new System.Drawing.Point(141, 210);
            this.dgvCstore.MultiSelect = false;
            this.dgvCstore.Name = "dgvCstore";
            this.dgvCstore.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCstore.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCstore.RowHeadersVisible = false;
            this.dgvCstore.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvCstore.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvCstore.Size = new System.Drawing.Size(236, 145);
            this.dgvCstore.TabIndex = 202;
            this.dgvCstore.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(12, 222);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 15);
            this.label16.TabIndex = 201;
            this.label16.Text = "Select Docs";
            this.label16.Click += new System.EventHandler(this.label16_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.rbtDebit);
            this.panel2.Controls.Add(this.rbtcredit);
            this.panel2.Location = new System.Drawing.Point(141, 119);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(235, 29);
            this.panel2.TabIndex = 204;
            // 
            // rbtDebit
            // 
            this.rbtDebit.AutoSize = true;
            this.rbtDebit.Checked = true;
            this.rbtDebit.Location = new System.Drawing.Point(14, 2);
            this.rbtDebit.Name = "rbtDebit";
            this.rbtDebit.Size = new System.Drawing.Size(63, 22);
            this.rbtDebit.TabIndex = 4;
            this.rbtDebit.TabStop = true;
            this.rbtDebit.Text = "Debit";
            this.rbtDebit.UseVisualStyleBackColor = true;
            // 
            // rbtcredit
            // 
            this.rbtcredit.AutoSize = true;
            this.rbtcredit.Location = new System.Drawing.Point(105, 3);
            this.rbtcredit.Name = "rbtcredit";
            this.rbtcredit.Size = new System.Drawing.Size(68, 22);
            this.rbtcredit.TabIndex = 5;
            this.rbtcredit.Text = "Credit";
            this.rbtcredit.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 15);
            this.label2.TabIndex = 196;
            this.label2.Text = "Bank Name";
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.ForeColor = System.Drawing.Color.Black;
            this.txtAmount.Location = new System.Drawing.Point(141, 156);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(235, 26);
            this.txtAmount.TabIndex = 185;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(12, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 15);
            this.label6.TabIndex = 184;
            this.label6.Text = "Total Amount";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(12, 373);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 183;
            this.label62.Text = "Remarks";
            this.label62.Click += new System.EventHandler(this.label62_Click);
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(141, 373);
            this.txtRemarks.MaxLength = 150;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(235, 74);
            this.txtRemarks.TabIndex = 182;
            this.txtRemarks.TextChanged += new System.EventHandler(this.txtRemarks_TextChanged);
            // 
            // dtpPayment
            // 
            this.dtpPayment.CustomFormat = "MM/dd/yyyy";
            this.dtpPayment.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPayment.Location = new System.Drawing.Point(141, 59);
            this.dtpPayment.Name = "dtpPayment";
            this.dtpPayment.Size = new System.Drawing.Size(235, 21);
            this.dtpPayment.TabIndex = 85;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 15);
            this.label1.TabIndex = 84;
            this.label1.Text = "Statement Date";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(346, 500);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(103, 37);
            this.btnExit.TabIndex = 78;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.AliceBlue;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(988, 240);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 15);
            this.label10.TabIndex = 76;
            this.label10.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.AliceBlue;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(988, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 15);
            this.label8.TabIndex = 75;
            this.label8.Text = "*";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(234, 500);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(105, 37);
            this.BtnDelete.TabIndex = 15;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(13, 500);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(105, 37);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUpdate.FlatAppearance.BorderSize = 2;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(122, 500);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(105, 37);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // grpCashMngtList
            // 
            this.grpCashMngtList.Controls.Add(this.dgvBankentries);
            this.grpCashMngtList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCashMngtList.Location = new System.Drawing.Point(495, 14);
            this.grpCashMngtList.Name = "grpCashMngtList";
            this.grpCashMngtList.Size = new System.Drawing.Size(611, 563);
            this.grpCashMngtList.TabIndex = 50;
            this.grpCashMngtList.TabStop = false;
            this.grpCashMngtList.Text = "Bank Statements";
            // 
            // dgvBankentries
            // 
            this.dgvBankentries.AllowDrop = true;
            this.dgvBankentries.AllowUserToAddRows = false;
            this.dgvBankentries.AllowUserToDeleteRows = false;
            this.dgvBankentries.AllowUserToResizeColumns = false;
            this.dgvBankentries.AllowUserToResizeRows = false;
            this.dgvBankentries.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvBankentries.BackgroundColor = System.Drawing.Color.White;
            this.dgvBankentries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBankentries.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBankentries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBankentries.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvBankentries.Location = new System.Drawing.Point(3, 18);
            this.dgvBankentries.MultiSelect = false;
            this.dgvBankentries.Name = "dgvBankentries";
            this.dgvBankentries.ReadOnly = true;
            this.dgvBankentries.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvBankentries.RowHeadersVisible = false;
            this.dgvBankentries.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvBankentries.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvBankentries.Size = new System.Drawing.Size(604, 541);
            this.dgvBankentries.TabIndex = 1;
            this.dgvBankentries.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBankentries_CellClick);
            // 
            // BankEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1113, 577);
            this.Controls.Add(this.grpCashMngtList);
            this.Controls.Add(this.grpManageVendor);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "BankEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BankEntry";
            this.Load += new System.EventHandler(this.BankEntry_Load);
            this.grpManageVendor.ResumeLayout(false);
            this.grpManageVendor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCstore)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.grpCashMngtList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankentries)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpManageVendor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.DateTimePicker dtpPayment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.GroupBox grpCashMngtList;
        private System.Windows.Forms.DataGridView dgvBankentries;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbtDebit;
        private System.Windows.Forms.RadioButton rbtcredit;
        private System.Windows.Forms.DataGridView dgvCstore;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox BankListcomboBox;
    }
}