﻿namespace Shell
{
    partial class FrmPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpManageVendor = new System.Windows.Forms.GroupBox();
            this.pdfUploadbox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtInvoiceBal = new System.Windows.Forms.TextBox();
            this.txtcashPayoutAmt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcashpaidamt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.btnbrowse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpPaymentDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.ddlVendorName = new System.Windows.Forms.ComboBox();
            this.txtInoiceNo = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ddlPaymentType = new System.Windows.Forms.ComboBox();
            this.grpCashMngtList = new System.Windows.Forms.GroupBox();
            this.dgvVendorList = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.grpManageVendor.SuspendLayout();
            this.grpCashMngtList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendorList)).BeginInit();
            this.SuspendLayout();
            // 
            // grpManageVendor
            // 
            this.grpManageVendor.Controls.Add(this.pdfUploadbox);
            this.grpManageVendor.Controls.Add(this.label8);
            this.grpManageVendor.Controls.Add(this.txtInvoiceBal);
            this.grpManageVendor.Controls.Add(this.txtcashPayoutAmt);
            this.grpManageVendor.Controls.Add(this.label5);
            this.grpManageVendor.Controls.Add(this.txtcashpaidamt);
            this.grpManageVendor.Controls.Add(this.label3);
            this.grpManageVendor.Controls.Add(this.btnExit);
            this.grpManageVendor.Controls.Add(this.txtAmount);
            this.grpManageVendor.Controls.Add(this.label6);
            this.grpManageVendor.Controls.Add(this.label62);
            this.grpManageVendor.Controls.Add(this.txtRemarks);
            this.grpManageVendor.Controls.Add(this.btnbrowse);
            this.grpManageVendor.Controls.Add(this.label4);
            this.grpManageVendor.Controls.Add(this.dtpPaymentDate);
            this.grpManageVendor.Controls.Add(this.label7);
            this.grpManageVendor.Controls.Add(this.ddlVendorName);
            this.grpManageVendor.Controls.Add(this.txtInoiceNo);
            this.grpManageVendor.Controls.Add(this.label15);
            this.grpManageVendor.Controls.Add(this.BtnDelete);
            this.grpManageVendor.Controls.Add(this.BtnSave);
            this.grpManageVendor.Controls.Add(this.label2);
            this.grpManageVendor.Controls.Add(this.label1);
            this.grpManageVendor.Controls.Add(this.button1);
            this.grpManageVendor.Controls.Add(this.ddlPaymentType);
            this.grpManageVendor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageVendor.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageVendor.ForeColor = System.Drawing.Color.Black;
            this.grpManageVendor.Location = new System.Drawing.Point(12, 12);
            this.grpManageVendor.Name = "grpManageVendor";
            this.grpManageVendor.Size = new System.Drawing.Size(800, 233);
            this.grpManageVendor.TabIndex = 0;
            this.grpManageVendor.TabStop = false;
            this.grpManageVendor.Text = "Cash Management";
            // 
            // pdfUploadbox
            // 
            this.pdfUploadbox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pdfUploadbox.ForeColor = System.Drawing.Color.Black;
            this.pdfUploadbox.Location = new System.Drawing.Point(692, 110);
            this.pdfUploadbox.Name = "pdfUploadbox";
            this.pdfUploadbox.Size = new System.Drawing.Size(40, 26);
            this.pdfUploadbox.TabIndex = 170;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.AliceBlue;
            this.label8.Font = new System.Drawing.Font("Arial", 9F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(333, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 15);
            this.label8.TabIndex = 89;
            this.label8.Text = "Balance";
            // 
            // txtInvoiceBal
            // 
            this.txtInvoiceBal.BackColor = System.Drawing.Color.LightGray;
            this.txtInvoiceBal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceBal.ForeColor = System.Drawing.Color.Black;
            this.txtInvoiceBal.Location = new System.Drawing.Point(484, 85);
            this.txtInvoiceBal.Name = "txtInvoiceBal";
            this.txtInvoiceBal.ReadOnly = true;
            this.txtInvoiceBal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtInvoiceBal.Size = new System.Drawing.Size(202, 22);
            this.txtInvoiceBal.TabIndex = 7;
            this.txtInvoiceBal.Text = "0";
            // 
            // txtcashPayoutAmt
            // 
            this.txtcashPayoutAmt.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcashPayoutAmt.ForeColor = System.Drawing.Color.Black;
            this.txtcashPayoutAmt.Location = new System.Drawing.Point(484, 56);
            this.txtcashPayoutAmt.Name = "txtcashPayoutAmt";
            this.txtcashPayoutAmt.Size = new System.Drawing.Size(202, 26);
            this.txtcashPayoutAmt.TabIndex = 6;
            this.txtcashPayoutAmt.Text = "0";
            this.txtcashPayoutAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcashPayoutAmt.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(333, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 15);
            this.label5.TabIndex = 86;
            this.label5.Text = "Cash Payout Amount";
            // 
            // txtcashpaidamt
            // 
            this.txtcashpaidamt.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcashpaidamt.ForeColor = System.Drawing.Color.Black;
            this.txtcashpaidamt.Location = new System.Drawing.Point(484, 24);
            this.txtcashpaidamt.Name = "txtcashpaidamt";
            this.txtcashpaidamt.Size = new System.Drawing.Size(202, 26);
            this.txtcashpaidamt.TabIndex = 5;
            this.txtcashpaidamt.Text = "0";
            this.txtcashpaidamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcashpaidamt.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(333, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 15);
            this.label3.TabIndex = 84;
            this.label3.Text = "Cash Paid Amount";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(539, 194);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(90, 32);
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.ForeColor = System.Drawing.Color.Black;
            this.txtAmount.Location = new System.Drawing.Point(109, 153);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(202, 26);
            this.txtAmount.TabIndex = 4;
            this.txtAmount.Text = "0";
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            this.txtAmount.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(17, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 15);
            this.label6.TabIndex = 82;
            this.label6.Text = "Invoice Amount";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(333, 137);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 81;
            this.label62.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(484, 140);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(221, 39);
            this.txtRemarks.TabIndex = 9;
            // 
            // btnbrowse
            // 
            this.btnbrowse.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnbrowse.FlatAppearance.BorderSize = 2;
            this.btnbrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbrowse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbrowse.Location = new System.Drawing.Point(484, 110);
            this.btnbrowse.Name = "btnbrowse";
            this.btnbrowse.Size = new System.Drawing.Size(202, 24);
            this.btnbrowse.TabIndex = 8;
            this.btnbrowse.Text = "B&rowse";
            this.btnbrowse.UseVisualStyleBackColor = true;
            this.btnbrowse.Click += new System.EventHandler(this.btnbrowse_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(333, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 15);
            this.label4.TabIndex = 79;
            this.label4.Text = "Upload Attachment";
            // 
            // dtpPaymentDate
            // 
            this.dtpPaymentDate.CustomFormat = "MM/dd/yyyy";
            this.dtpPaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPaymentDate.Location = new System.Drawing.Point(109, 27);
            this.dtpPaymentDate.Name = "dtpPaymentDate";
            this.dtpPaymentDate.Size = new System.Drawing.Size(202, 26);
            this.dtpPaymentDate.TabIndex = 0;
            this.dtpPaymentDate.ValueChanged += new System.EventHandler(this.dtpPaymentDate_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.AliceBlue;
            this.label7.Font = new System.Drawing.Font("Arial", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(17, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 76;
            this.label7.Text = "Payment Date";
            // 
            // ddlVendorName
            // 
            this.ddlVendorName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVendorName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlVendorName.FormattingEnabled = true;
            this.ddlVendorName.Items.AddRange(new object[] {
            "--Select--",
            "PR",
            "Forign Worker",
            "Student Open Work Permit",
            "Closed Work Permit",
            "Citizen",
            "Work Permit",
            "Tourist",
            "Visitor",
            "Others"});
            this.ddlVendorName.Location = new System.Drawing.Point(109, 59);
            this.ddlVendorName.Name = "ddlVendorName";
            this.ddlVendorName.Size = new System.Drawing.Size(202, 22);
            this.ddlVendorName.TabIndex = 1;
            // 
            // txtInoiceNo
            // 
            this.txtInoiceNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInoiceNo.ForeColor = System.Drawing.Color.Black;
            this.txtInoiceNo.Location = new System.Drawing.Point(109, 121);
            this.txtInoiceNo.MaxLength = 30;
            this.txtInoiceNo.Name = "txtInoiceNo";
            this.txtInoiceNo.Size = new System.Drawing.Size(202, 26);
            this.txtInoiceNo.TabIndex = 3;
            this.txtInoiceNo.Leave += new System.EventHandler(this.txtInoiceNo_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(17, 94);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 15);
            this.label15.TabIndex = 55;
            this.label15.Text = "Payment Type";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(443, 194);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 12;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(253, 194);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 10;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(17, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 36;
            this.label2.Text = "Vendor Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(17, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Invoice No";
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(347, 194);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 32);
            this.button1.TabIndex = 11;
            this.button1.Text = "&Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ddlPaymentType
            // 
            this.ddlPaymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPaymentType.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPaymentType.FormattingEnabled = true;
            this.ddlPaymentType.Items.AddRange(new object[] {
            "--Select--",
            "PR",
            "Forign Worker",
            "Student Open Work Permit",
            "Closed Work Permit",
            "Citizen",
            "Work Permit",
            "Tourist",
            "Visitor",
            "Others"});
            this.ddlPaymentType.Location = new System.Drawing.Point(109, 92);
            this.ddlPaymentType.Name = "ddlPaymentType";
            this.ddlPaymentType.Size = new System.Drawing.Size(202, 22);
            this.ddlPaymentType.TabIndex = 2;
            this.ddlPaymentType.SelectionChangeCommitted += new System.EventHandler(this.ddlPaymentType_SelectionChangeCommitted);
            // 
            // grpCashMngtList
            // 
            this.grpCashMngtList.Controls.Add(this.dgvVendorList);
            this.grpCashMngtList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCashMngtList.Location = new System.Drawing.Point(12, 251);
            this.grpCashMngtList.Name = "grpCashMngtList";
            this.grpCashMngtList.Size = new System.Drawing.Size(806, 234);
            this.grpCashMngtList.TabIndex = 47;
            this.grpCashMngtList.TabStop = false;
            this.grpCashMngtList.Text = "Payment List";
            // 
            // dgvVendorList
            // 
            this.dgvVendorList.AllowDrop = true;
            this.dgvVendorList.AllowUserToAddRows = false;
            this.dgvVendorList.AllowUserToDeleteRows = false;
            this.dgvVendorList.AllowUserToResizeColumns = false;
            this.dgvVendorList.AllowUserToResizeRows = false;
            this.dgvVendorList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvVendorList.BackgroundColor = System.Drawing.Color.White;
            this.dgvVendorList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVendorList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvVendorList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVendorList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvVendorList.Location = new System.Drawing.Point(3, 16);
            this.dgvVendorList.MultiSelect = false;
            this.dgvVendorList.Name = "dgvVendorList";
            this.dgvVendorList.ReadOnly = true;
            this.dgvVendorList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvVendorList.RowHeadersVisible = false;
            this.dgvVendorList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvVendorList.Size = new System.Drawing.Size(800, 215);
            this.dgvVendorList.TabIndex = 1;
            this.dgvVendorList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVendorList_CellContentClick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FrmPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(830, 487);
            this.Controls.Add(this.grpCashMngtList);
            this.Controls.Add(this.grpManageVendor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmPayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Payment To Vendor";
            this.Load += new System.EventHandler(this.FrmPayment_Load);
            this.grpManageVendor.ResumeLayout(false);
            this.grpManageVendor.PerformLayout();
            this.grpCashMngtList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendorList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpManageVendor;
        private System.Windows.Forms.TextBox txtInoiceNo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox ddlPaymentType;
        private System.Windows.Forms.ComboBox ddlVendorName;
        private System.Windows.Forms.DateTimePicker dtpPaymentDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnbrowse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox grpCashMngtList;
        private System.Windows.Forms.DataGridView dgvVendorList;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtcashPayoutAmt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtcashpaidamt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtInvoiceBal;
        private System.Windows.Forms.TextBox pdfUploadbox;
    }
}