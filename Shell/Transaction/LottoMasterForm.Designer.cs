﻿namespace Shell.Transaction
{
    partial class LottoMasterForm 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            this.LottoGroup = new System.Windows.Forms.GroupBox();
            this.LottoUnActivatedClosing30100Txt = new System.Windows.Forms.TextBox();
            this.label218 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing3050Txt = new System.Windows.Forms.TextBox();
            this.label219 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing20Txt = new System.Windows.Forms.TextBox();
            this.label220 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing10Txt = new System.Windows.Forms.TextBox();
            this.label221 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing7Txt = new System.Windows.Forms.TextBox();
            this.label222 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing5Txt = new System.Windows.Forms.TextBox();
            this.label223 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing4Txt = new System.Windows.Forms.TextBox();
            this.label224 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing3Txt = new System.Windows.Forms.TextBox();
            this.label225 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing2Txt = new System.Windows.Forms.TextBox();
            this.label226 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing1100Txt = new System.Windows.Forms.TextBox();
            this.label227 = new System.Windows.Forms.Label();
            this.LottoUnActivatedClosing150Txt = new System.Windows.Forms.TextBox();
            this.label228 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn30100Txt = new System.Windows.Forms.TextBox();
            this.label206 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn3050Txt = new System.Windows.Forms.TextBox();
            this.label205 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn20Txt = new System.Windows.Forms.TextBox();
            this.label204 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn10Txt = new System.Windows.Forms.TextBox();
            this.label203 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn7Txt = new System.Windows.Forms.TextBox();
            this.label202 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn5Txt = new System.Windows.Forms.TextBox();
            this.label201 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn4Txt = new System.Windows.Forms.TextBox();
            this.label200 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn3Txt = new System.Windows.Forms.TextBox();
            this.label199 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn2Txt = new System.Windows.Forms.TextBox();
            this.label198 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn1100Txt = new System.Windows.Forms.TextBox();
            this.label197 = new System.Windows.Forms.Label();
            this.LottoUnActivatedReturn150Txt = new System.Windows.Forms.TextBox();
            this.label196 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract30100Txt = new System.Windows.Forms.TextBox();
            this.label187 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd30100Txt = new System.Windows.Forms.TextBox();
            this.label188 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening30100Txt = new System.Windows.Forms.TextBox();
            this.label189 = new System.Windows.Forms.Label();
            this.LottoUnactivatedSubstract1100Txt = new System.Windows.Forms.TextBox();
            this.label184 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd1100Txt = new System.Windows.Forms.TextBox();
            this.label185 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening1100Txt = new System.Windows.Forms.TextBox();
            this.label186 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract30Txt = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd30Txt = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening30Txt = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract20Txt = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd20Txt = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening20Txt = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract10Txt = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd10Txt = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening10Txt = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract7Txt = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd7Txt = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening7Txt = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract5Txt = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd5Txt = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening5Txt = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract4Txt = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd4Txt = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening4Txt = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract3Txt = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd3Txt = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening3Txt = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.LottoUnActivatedSubtract2Txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd2Txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening2Txt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.UnActivatedDocsDgv = new System.Windows.Forms.DataGridView();
            this.CashCheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label68 = new System.Windows.Forms.Label();
            this.LottoUnActivatedRemakrsTxt = new System.Windows.Forms.RichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.LottoTotalUnActivatedTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.LottoUnactivatedSubstract150Txt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.LottoUnActivatedAdd150Txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LottoUnActivatedOpening150Txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LottoUnActivatedBtn = new System.Windows.Forms.Button();
            this.ActivatedDocsDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label69 = new System.Windows.Forms.Label();
            this.LottoActivatedRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.LottoActivatedTotalTxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.DateCalender = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.OnlineGroup = new System.Windows.Forms.GroupBox();
            this.label80 = new System.Windows.Forms.Label();
            this.LottoReportPayoutTxt = new System.Windows.Forms.TextBox();
            this.LottoBackofficePayoutTxt = new System.Windows.Forms.TextBox();
            this.LottoAdjustCPayout = new System.Windows.Forms.TextBox();
            this.LottoEOS1PayoutTxt = new System.Windows.Forms.TextBox();
            this.LottoShiftDiffPayoutTxt = new System.Windows.Forms.TextBox();
            this.LottoEOS2PayoutTxt = new System.Windows.Forms.TextBox();
            this.TSoldPayoutTxt = new System.Windows.Forms.TextBox();
            this.LottoEOS3PayoutTxt = new System.Windows.Forms.TextBox();
            this.ScratchWinOnlineDocsDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label26 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.LottoOnlineReportTxt = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.LottoScratchOnlineRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.LottoOnlineBackOfficetxt = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.LottoOnlineAdjustCanceltxt = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.LottoOnlineEOS1Txt = new System.Windows.Forms.TextBox();
            this.LottoOnlineShiftsDiffTxt = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.LottoOnlineEOS2Txt = new System.Windows.Forms.TextBox();
            this.LottoOnlineTSoldTxt = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.LottoOnlineEOS3Txt = new System.Windows.Forms.TextBox();
            this.ScratchWingroup = new System.Windows.Forms.GroupBox();
            this.TotalActivatedInvTxt = new System.Windows.Forms.TextBox();
            this.label240 = new System.Windows.Forms.Label();
            this.LottoScrachWinTotalSubtotalTxt = new System.Windows.Forms.TextBox();
            this.label183 = new System.Windows.Forms.Label();
            this.LottoScratchRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label182 = new System.Windows.Forms.Label();
            this.ScratchWinNormalDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label176 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalDiffTxt = new System.Windows.Forms.TextBox();
            this.label181 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalEODSoldDTxt = new System.Windows.Forms.TextBox();
            this.label180 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalEODSoldTxt = new System.Windows.Forms.TextBox();
            this.label179 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalSoldTxt = new System.Windows.Forms.TextBox();
            this.label178 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalActualCLoseTxt = new System.Windows.Forms.TextBox();
            this.label177 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalAdditionalQtyTxt = new System.Windows.Forms.TextBox();
            this.label175 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalOpeningDiffTxt = new System.Windows.Forms.TextBox();
            this.label174 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalActualOpenTxt = new System.Windows.Forms.TextBox();
            this.label173 = new System.Windows.Forms.Label();
            this.LottoScratchWinTotalOpenTxt = new System.Windows.Forms.TextBox();
            this.label172 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal30Txt = new System.Windows.Forms.TextBox();
            this.LottoScratch30DiffTxt = new System.Windows.Forms.TextBox();
            this.label162 = new System.Windows.Forms.Label();
            this.LottoReportSold30Txt = new System.Windows.Forms.TextBox();
            this.label163 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold30Txt = new System.Windows.Forms.TextBox();
            this.label164 = new System.Windows.Forms.Label();
            this.LottoScratchSold30Txt = new System.Windows.Forms.TextBox();
            this.label165 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose30Txt = new System.Windows.Forms.TextBox();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty30Txt = new System.Windows.Forms.TextBox();
            this.label168 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff30Txt = new System.Windows.Forms.TextBox();
            this.label169 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen30txt = new System.Windows.Forms.TextBox();
            this.label170 = new System.Windows.Forms.Label();
            this.LottoScratchOpen30Txt = new System.Windows.Forms.TextBox();
            this.label171 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal20Txt = new System.Windows.Forms.TextBox();
            this.LottoScratch20DiffTxt = new System.Windows.Forms.TextBox();
            this.label152 = new System.Windows.Forms.Label();
            this.LottoReportSold20Txt = new System.Windows.Forms.TextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold20Txt = new System.Windows.Forms.TextBox();
            this.label154 = new System.Windows.Forms.Label();
            this.LottoScratchSold20Txt = new System.Windows.Forms.TextBox();
            this.label155 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose20Txt = new System.Windows.Forms.TextBox();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty20Txt = new System.Windows.Forms.TextBox();
            this.label158 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff20Txt = new System.Windows.Forms.TextBox();
            this.label159 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen20txt = new System.Windows.Forms.TextBox();
            this.label160 = new System.Windows.Forms.Label();
            this.LottoScratchOpen20Txt = new System.Windows.Forms.TextBox();
            this.label161 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal10Txt = new System.Windows.Forms.TextBox();
            this.LottoScratch10DiffTxt = new System.Windows.Forms.TextBox();
            this.label142 = new System.Windows.Forms.Label();
            this.LottoReportSold10Txt = new System.Windows.Forms.TextBox();
            this.label143 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold10Txt = new System.Windows.Forms.TextBox();
            this.label144 = new System.Windows.Forms.Label();
            this.LottoScratchSold10Txt = new System.Windows.Forms.TextBox();
            this.label145 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose10Txt = new System.Windows.Forms.TextBox();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty10Txt = new System.Windows.Forms.TextBox();
            this.label148 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff10Txt = new System.Windows.Forms.TextBox();
            this.label149 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen10txt = new System.Windows.Forms.TextBox();
            this.label150 = new System.Windows.Forms.Label();
            this.LottoScratchOpen10Txt = new System.Windows.Forms.TextBox();
            this.label151 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal7Txt = new System.Windows.Forms.TextBox();
            this.LottoScratch7DiffTxt = new System.Windows.Forms.TextBox();
            this.label132 = new System.Windows.Forms.Label();
            this.LottoReportSold7Txt = new System.Windows.Forms.TextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold7Txt = new System.Windows.Forms.TextBox();
            this.label134 = new System.Windows.Forms.Label();
            this.LottoScratchSold7Txt = new System.Windows.Forms.TextBox();
            this.label135 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose7Txt = new System.Windows.Forms.TextBox();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty7Txt = new System.Windows.Forms.TextBox();
            this.label138 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff7Txt = new System.Windows.Forms.TextBox();
            this.label139 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen7txt = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.LottoScratchOpen7Txt = new System.Windows.Forms.TextBox();
            this.label141 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal5Txt = new System.Windows.Forms.TextBox();
            this.LottoScratch5DiffTxt = new System.Windows.Forms.TextBox();
            this.label122 = new System.Windows.Forms.Label();
            this.LottoReportSold5Txt = new System.Windows.Forms.TextBox();
            this.label123 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold5Txt = new System.Windows.Forms.TextBox();
            this.label124 = new System.Windows.Forms.Label();
            this.LottoScratchSold5Txt = new System.Windows.Forms.TextBox();
            this.label125 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose5Txt = new System.Windows.Forms.TextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty5Txt = new System.Windows.Forms.TextBox();
            this.label128 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff5Txt = new System.Windows.Forms.TextBox();
            this.label129 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen5txt = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.LottoScratchOpen5Txt = new System.Windows.Forms.TextBox();
            this.label131 = new System.Windows.Forms.Label();
            this.LottoScratch4DiffTxt = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.LottoReportSold4Txt = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold4Txt = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.LottoScratchSold4Txt = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose4Txt = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal4Txt = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty4Txt = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff4Txt = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen4txt = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.LottoScratchOpen4Txt = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.LottoScratch3DiffTxt = new System.Windows.Forms.TextBox();
            this.label102 = new System.Windows.Forms.Label();
            this.LottoReportSold3Txt = new System.Windows.Forms.TextBox();
            this.label103 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold3Txt = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.LottoScratchSold3Txt = new System.Windows.Forms.TextBox();
            this.label105 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose3Txt = new System.Windows.Forms.TextBox();
            this.label106 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal3Txt = new System.Windows.Forms.TextBox();
            this.label107 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty3Txt = new System.Windows.Forms.TextBox();
            this.label108 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff3Txt = new System.Windows.Forms.TextBox();
            this.label109 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen3txt = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.LottoScratchOpen3Txt = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.LottoScratch2DiffTxt = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.LottoReportSold2Txt = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold2Txt = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.LottoScratchSold2Txt = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose2Txt = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal2Txt = new System.Windows.Forms.TextBox();
            this.label97 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty2Txt = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff2Txt = new System.Windows.Forms.TextBox();
            this.label99 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen2txt = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.LottoScratchOpen2Txt = new System.Windows.Forms.TextBox();
            this.label101 = new System.Windows.Forms.Label();
            this.LottoScratch1DiffTxt = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.LottoReportSold1Txt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.LottoScratchReportSold1Txt = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.LottoScratchSold1Txt = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.LottoScratchActualClose1Txt = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.LottoScratchSubtotal1Txt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.LottoScratchAddQty1Txt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.LottoScratchOpeningDiff1Txt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.LottoScratchActualOpen1txt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.LottoScratchOpen1Txt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LottoActivatedClosing30100Txt = new System.Windows.Forms.TextBox();
            this.label229 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing3050Txt = new System.Windows.Forms.TextBox();
            this.label230 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing20Txt = new System.Windows.Forms.TextBox();
            this.label231 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing10Txt = new System.Windows.Forms.TextBox();
            this.label232 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing7Txt = new System.Windows.Forms.TextBox();
            this.label233 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing5Txt = new System.Windows.Forms.TextBox();
            this.label234 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing4Txt = new System.Windows.Forms.TextBox();
            this.label235 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing3Txt = new System.Windows.Forms.TextBox();
            this.label236 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing2Txt = new System.Windows.Forms.TextBox();
            this.label237 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing1100Txt = new System.Windows.Forms.TextBox();
            this.label238 = new System.Windows.Forms.Label();
            this.LottoActivatedClosing150Txt = new System.Windows.Forms.TextBox();
            this.label239 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn30100Txt = new System.Windows.Forms.TextBox();
            this.label207 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn350Txt = new System.Windows.Forms.TextBox();
            this.label208 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn20Txt = new System.Windows.Forms.TextBox();
            this.label209 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn10Txt = new System.Windows.Forms.TextBox();
            this.label210 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn7Txt = new System.Windows.Forms.TextBox();
            this.label211 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn5Txt = new System.Windows.Forms.TextBox();
            this.label212 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn4Txt = new System.Windows.Forms.TextBox();
            this.label213 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn3Txt = new System.Windows.Forms.TextBox();
            this.label214 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn2Txt = new System.Windows.Forms.TextBox();
            this.label215 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn1100Txt = new System.Windows.Forms.TextBox();
            this.label216 = new System.Windows.Forms.Label();
            this.LottoActivatedReturn150Txt = new System.Windows.Forms.TextBox();
            this.label217 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract30100Txt = new System.Windows.Forms.TextBox();
            this.label193 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd30100Txt = new System.Windows.Forms.TextBox();
            this.label194 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening30100Txt = new System.Windows.Forms.TextBox();
            this.label195 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract1100Txt = new System.Windows.Forms.TextBox();
            this.label190 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd1100Txt = new System.Windows.Forms.TextBox();
            this.label191 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening1100Txt = new System.Windows.Forms.TextBox();
            this.label192 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract30Txt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd30Txt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening30Txt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract20Txt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd20Txt = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening20Txt = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract10Txt = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd10Txt = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening10Txt = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract7Txt = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd7Txt = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening7Txt = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract5Txt = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd5Txt = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening5Txt = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract4Txt = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd4Txt = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening4Txt = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract3Txt = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd3Txt = new System.Windows.Forms.TextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening3Txt = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract2Txt = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd2Txt = new System.Windows.Forms.TextBox();
            this.label81 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening2Txt = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.LottoActivatedSubstract150Txt = new System.Windows.Forms.TextBox();
            this.label83 = new System.Windows.Forms.Label();
            this.LottoActivatedAdd150Txt = new System.Windows.Forms.TextBox();
            this.label84 = new System.Windows.Forms.Label();
            this.LottoActivatedOpening150Txt = new System.Windows.Forms.TextBox();
            this.label85 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.AmountDueTxt = new System.Windows.Forms.TextBox();
            this.RedemptionsTxt = new System.Windows.Forms.TextBox();
            this.CreditsTxt = new System.Windows.Forms.TextBox();
            this.RetailerBonusTxt = new System.Windows.Forms.TextBox();
            this.ActivationTxt = new System.Windows.Forms.TextBox();
            this.OnlineSoldtxt = new System.Windows.Forms.TextBox();
            this.LottoWeeklyDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label93 = new System.Windows.Forms.Label();
            this.LottoWeeklyRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label94 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.LottoGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UnActivatedDocsDgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActivatedDocsDgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.OnlineGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScratchWinOnlineDocsDgv)).BeginInit();
            this.ScratchWingroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScratchWinNormalDgv)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LottoWeeklyDgv)).BeginInit();
            this.SuspendLayout();
            // 
            // LottoGroup
            // 
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing30100Txt);
            this.LottoGroup.Controls.Add(this.label218);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing3050Txt);
            this.LottoGroup.Controls.Add(this.label219);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing20Txt);
            this.LottoGroup.Controls.Add(this.label220);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing10Txt);
            this.LottoGroup.Controls.Add(this.label221);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing7Txt);
            this.LottoGroup.Controls.Add(this.label222);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing5Txt);
            this.LottoGroup.Controls.Add(this.label223);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing4Txt);
            this.LottoGroup.Controls.Add(this.label224);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing3Txt);
            this.LottoGroup.Controls.Add(this.label225);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing2Txt);
            this.LottoGroup.Controls.Add(this.label226);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing1100Txt);
            this.LottoGroup.Controls.Add(this.label227);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedClosing150Txt);
            this.LottoGroup.Controls.Add(this.label228);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn30100Txt);
            this.LottoGroup.Controls.Add(this.label206);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn3050Txt);
            this.LottoGroup.Controls.Add(this.label205);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn20Txt);
            this.LottoGroup.Controls.Add(this.label204);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn10Txt);
            this.LottoGroup.Controls.Add(this.label203);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn7Txt);
            this.LottoGroup.Controls.Add(this.label202);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn5Txt);
            this.LottoGroup.Controls.Add(this.label201);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn4Txt);
            this.LottoGroup.Controls.Add(this.label200);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn3Txt);
            this.LottoGroup.Controls.Add(this.label199);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn2Txt);
            this.LottoGroup.Controls.Add(this.label198);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn1100Txt);
            this.LottoGroup.Controls.Add(this.label197);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedReturn150Txt);
            this.LottoGroup.Controls.Add(this.label196);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract30100Txt);
            this.LottoGroup.Controls.Add(this.label187);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd30100Txt);
            this.LottoGroup.Controls.Add(this.label188);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening30100Txt);
            this.LottoGroup.Controls.Add(this.label189);
            this.LottoGroup.Controls.Add(this.LottoUnactivatedSubstract1100Txt);
            this.LottoGroup.Controls.Add(this.label184);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd1100Txt);
            this.LottoGroup.Controls.Add(this.label185);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening1100Txt);
            this.LottoGroup.Controls.Add(this.label186);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract30Txt);
            this.LottoGroup.Controls.Add(this.label47);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd30Txt);
            this.LottoGroup.Controls.Add(this.label48);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening30Txt);
            this.LottoGroup.Controls.Add(this.label49);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract20Txt);
            this.LottoGroup.Controls.Add(this.label50);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd20Txt);
            this.LottoGroup.Controls.Add(this.label51);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening20Txt);
            this.LottoGroup.Controls.Add(this.label52);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract10Txt);
            this.LottoGroup.Controls.Add(this.label53);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd10Txt);
            this.LottoGroup.Controls.Add(this.label54);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening10Txt);
            this.LottoGroup.Controls.Add(this.label55);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract7Txt);
            this.LottoGroup.Controls.Add(this.label38);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd7Txt);
            this.LottoGroup.Controls.Add(this.label39);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening7Txt);
            this.LottoGroup.Controls.Add(this.label40);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract5Txt);
            this.LottoGroup.Controls.Add(this.label41);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd5Txt);
            this.LottoGroup.Controls.Add(this.label42);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening5Txt);
            this.LottoGroup.Controls.Add(this.label43);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract4Txt);
            this.LottoGroup.Controls.Add(this.label44);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd4Txt);
            this.LottoGroup.Controls.Add(this.label45);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening4Txt);
            this.LottoGroup.Controls.Add(this.label46);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract3Txt);
            this.LottoGroup.Controls.Add(this.label35);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd3Txt);
            this.LottoGroup.Controls.Add(this.label36);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening3Txt);
            this.LottoGroup.Controls.Add(this.label37);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedSubtract2Txt);
            this.LottoGroup.Controls.Add(this.label1);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd2Txt);
            this.LottoGroup.Controls.Add(this.label2);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening2Txt);
            this.LottoGroup.Controls.Add(this.label8);
            this.LottoGroup.Controls.Add(this.UnActivatedDocsDgv);
            this.LottoGroup.Controls.Add(this.label68);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedRemakrsTxt);
            this.LottoGroup.Controls.Add(this.label14);
            this.LottoGroup.Controls.Add(this.LottoTotalUnActivatedTxt);
            this.LottoGroup.Controls.Add(this.label7);
            this.LottoGroup.Controls.Add(this.LottoUnactivatedSubstract150Txt);
            this.LottoGroup.Controls.Add(this.label6);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedAdd150Txt);
            this.LottoGroup.Controls.Add(this.label5);
            this.LottoGroup.Controls.Add(this.LottoUnActivatedOpening150Txt);
            this.LottoGroup.Controls.Add(this.label4);
            this.LottoGroup.Location = new System.Drawing.Point(12, 144);
            this.LottoGroup.Name = "LottoGroup";
            this.LottoGroup.Size = new System.Drawing.Size(1233, 330);
            this.LottoGroup.TabIndex = 2;
            this.LottoGroup.TabStop = false;
            this.LottoGroup.Text = "Lotto Books UnActivated";
            // 
            // LottoUnActivatedClosing30100Txt
            // 
            this.LottoUnActivatedClosing30100Txt.Location = new System.Drawing.Point(774, 277);
            this.LottoUnActivatedClosing30100Txt.Name = "LottoUnActivatedClosing30100Txt";
            this.LottoUnActivatedClosing30100Txt.ReadOnly = true;
            this.LottoUnActivatedClosing30100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing30100Txt.TabIndex = 343;
            this.LottoUnActivatedClosing30100Txt.Text = "0";
            // 
            // label218
            // 
            this.label218.AutoSize = true;
            this.label218.Location = new System.Drawing.Point(672, 280);
            this.label218.Name = "label218";
            this.label218.Size = new System.Drawing.Size(89, 13);
            this.label218.TabIndex = 342;
            this.label218.Text = "Closing $30-[100]";
            // 
            // LottoUnActivatedClosing3050Txt
            // 
            this.LottoUnActivatedClosing3050Txt.Location = new System.Drawing.Point(774, 254);
            this.LottoUnActivatedClosing3050Txt.Name = "LottoUnActivatedClosing3050Txt";
            this.LottoUnActivatedClosing3050Txt.ReadOnly = true;
            this.LottoUnActivatedClosing3050Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing3050Txt.TabIndex = 341;
            this.LottoUnActivatedClosing3050Txt.Text = "0";
            // 
            // label219
            // 
            this.label219.AutoSize = true;
            this.label219.Location = new System.Drawing.Point(672, 257);
            this.label219.Name = "label219";
            this.label219.Size = new System.Drawing.Size(83, 13);
            this.label219.TabIndex = 340;
            this.label219.Text = "Closing $30-[50]";
            // 
            // LottoUnActivatedClosing20Txt
            // 
            this.LottoUnActivatedClosing20Txt.Location = new System.Drawing.Point(774, 229);
            this.LottoUnActivatedClosing20Txt.Name = "LottoUnActivatedClosing20Txt";
            this.LottoUnActivatedClosing20Txt.ReadOnly = true;
            this.LottoUnActivatedClosing20Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing20Txt.TabIndex = 339;
            this.LottoUnActivatedClosing20Txt.Text = "0";
            // 
            // label220
            // 
            this.label220.AutoSize = true;
            this.label220.Location = new System.Drawing.Point(672, 232);
            this.label220.Name = "label220";
            this.label220.Size = new System.Drawing.Size(62, 13);
            this.label220.TabIndex = 338;
            this.label220.Text = "Closing $20";
            // 
            // LottoUnActivatedClosing10Txt
            // 
            this.LottoUnActivatedClosing10Txt.Location = new System.Drawing.Point(774, 203);
            this.LottoUnActivatedClosing10Txt.Name = "LottoUnActivatedClosing10Txt";
            this.LottoUnActivatedClosing10Txt.ReadOnly = true;
            this.LottoUnActivatedClosing10Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing10Txt.TabIndex = 337;
            this.LottoUnActivatedClosing10Txt.Text = "0";
            // 
            // label221
            // 
            this.label221.AutoSize = true;
            this.label221.Location = new System.Drawing.Point(672, 206);
            this.label221.Name = "label221";
            this.label221.Size = new System.Drawing.Size(62, 13);
            this.label221.TabIndex = 336;
            this.label221.Text = "Closing $10";
            // 
            // LottoUnActivatedClosing7Txt
            // 
            this.LottoUnActivatedClosing7Txt.Location = new System.Drawing.Point(774, 177);
            this.LottoUnActivatedClosing7Txt.Name = "LottoUnActivatedClosing7Txt";
            this.LottoUnActivatedClosing7Txt.ReadOnly = true;
            this.LottoUnActivatedClosing7Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing7Txt.TabIndex = 335;
            this.LottoUnActivatedClosing7Txt.Text = "0";
            // 
            // label222
            // 
            this.label222.AutoSize = true;
            this.label222.Location = new System.Drawing.Point(672, 180);
            this.label222.Name = "label222";
            this.label222.Size = new System.Drawing.Size(56, 13);
            this.label222.TabIndex = 334;
            this.label222.Text = "Closing $7";
            // 
            // LottoUnActivatedClosing5Txt
            // 
            this.LottoUnActivatedClosing5Txt.Location = new System.Drawing.Point(774, 150);
            this.LottoUnActivatedClosing5Txt.Name = "LottoUnActivatedClosing5Txt";
            this.LottoUnActivatedClosing5Txt.ReadOnly = true;
            this.LottoUnActivatedClosing5Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing5Txt.TabIndex = 333;
            this.LottoUnActivatedClosing5Txt.Text = "0";
            // 
            // label223
            // 
            this.label223.AutoSize = true;
            this.label223.Location = new System.Drawing.Point(672, 153);
            this.label223.Name = "label223";
            this.label223.Size = new System.Drawing.Size(56, 13);
            this.label223.TabIndex = 332;
            this.label223.Text = "Closing $5";
            // 
            // LottoUnActivatedClosing4Txt
            // 
            this.LottoUnActivatedClosing4Txt.Location = new System.Drawing.Point(774, 124);
            this.LottoUnActivatedClosing4Txt.Name = "LottoUnActivatedClosing4Txt";
            this.LottoUnActivatedClosing4Txt.ReadOnly = true;
            this.LottoUnActivatedClosing4Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing4Txt.TabIndex = 331;
            this.LottoUnActivatedClosing4Txt.Text = "0";
            // 
            // label224
            // 
            this.label224.AutoSize = true;
            this.label224.Location = new System.Drawing.Point(672, 127);
            this.label224.Name = "label224";
            this.label224.Size = new System.Drawing.Size(56, 13);
            this.label224.TabIndex = 330;
            this.label224.Text = "Closing $4";
            // 
            // LottoUnActivatedClosing3Txt
            // 
            this.LottoUnActivatedClosing3Txt.Location = new System.Drawing.Point(774, 99);
            this.LottoUnActivatedClosing3Txt.Name = "LottoUnActivatedClosing3Txt";
            this.LottoUnActivatedClosing3Txt.ReadOnly = true;
            this.LottoUnActivatedClosing3Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing3Txt.TabIndex = 329;
            this.LottoUnActivatedClosing3Txt.Text = "0";
            // 
            // label225
            // 
            this.label225.AutoSize = true;
            this.label225.Location = new System.Drawing.Point(672, 102);
            this.label225.Name = "label225";
            this.label225.Size = new System.Drawing.Size(56, 13);
            this.label225.TabIndex = 328;
            this.label225.Text = "Closing $3";
            // 
            // LottoUnActivatedClosing2Txt
            // 
            this.LottoUnActivatedClosing2Txt.Location = new System.Drawing.Point(774, 73);
            this.LottoUnActivatedClosing2Txt.Name = "LottoUnActivatedClosing2Txt";
            this.LottoUnActivatedClosing2Txt.ReadOnly = true;
            this.LottoUnActivatedClosing2Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing2Txt.TabIndex = 327;
            this.LottoUnActivatedClosing2Txt.Text = "0";
            // 
            // label226
            // 
            this.label226.AutoSize = true;
            this.label226.Location = new System.Drawing.Point(672, 76);
            this.label226.Name = "label226";
            this.label226.Size = new System.Drawing.Size(56, 13);
            this.label226.TabIndex = 326;
            this.label226.Text = "Closing $2";
            // 
            // LottoUnActivatedClosing1100Txt
            // 
            this.LottoUnActivatedClosing1100Txt.Location = new System.Drawing.Point(774, 47);
            this.LottoUnActivatedClosing1100Txt.Name = "LottoUnActivatedClosing1100Txt";
            this.LottoUnActivatedClosing1100Txt.ReadOnly = true;
            this.LottoUnActivatedClosing1100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing1100Txt.TabIndex = 325;
            this.LottoUnActivatedClosing1100Txt.Text = "0";
            // 
            // label227
            // 
            this.label227.AutoSize = true;
            this.label227.Location = new System.Drawing.Point(672, 50);
            this.label227.Name = "label227";
            this.label227.Size = new System.Drawing.Size(83, 13);
            this.label227.TabIndex = 324;
            this.label227.Text = "Closing $1-[100]";
            // 
            // LottoUnActivatedClosing150Txt
            // 
            this.LottoUnActivatedClosing150Txt.Location = new System.Drawing.Point(774, 20);
            this.LottoUnActivatedClosing150Txt.Name = "LottoUnActivatedClosing150Txt";
            this.LottoUnActivatedClosing150Txt.ReadOnly = true;
            this.LottoUnActivatedClosing150Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedClosing150Txt.TabIndex = 323;
            this.LottoUnActivatedClosing150Txt.Text = "0";
            // 
            // label228
            // 
            this.label228.AutoSize = true;
            this.label228.Location = new System.Drawing.Point(672, 23);
            this.label228.Name = "label228";
            this.label228.Size = new System.Drawing.Size(77, 13);
            this.label228.TabIndex = 322;
            this.label228.Text = "Closing $1-[50]";
            // 
            // LottoUnActivatedReturn30100Txt
            // 
            this.LottoUnActivatedReturn30100Txt.Location = new System.Drawing.Point(611, 277);
            this.LottoUnActivatedReturn30100Txt.Name = "LottoUnActivatedReturn30100Txt";
            this.LottoUnActivatedReturn30100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn30100Txt.TabIndex = 321;
            this.LottoUnActivatedReturn30100Txt.Text = "0";
            this.LottoUnActivatedReturn30100Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label206
            // 
            this.label206.AutoSize = true;
            this.label206.Location = new System.Drawing.Point(509, 280);
            this.label206.Name = "label206";
            this.label206.Size = new System.Drawing.Size(87, 13);
            this.label206.TabIndex = 320;
            this.label206.Text = "Return $30-[100]";
            // 
            // LottoUnActivatedReturn3050Txt
            // 
            this.LottoUnActivatedReturn3050Txt.Location = new System.Drawing.Point(611, 254);
            this.LottoUnActivatedReturn3050Txt.Name = "LottoUnActivatedReturn3050Txt";
            this.LottoUnActivatedReturn3050Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn3050Txt.TabIndex = 319;
            this.LottoUnActivatedReturn3050Txt.Text = "0";
            this.LottoUnActivatedReturn3050Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label205
            // 
            this.label205.AutoSize = true;
            this.label205.Location = new System.Drawing.Point(509, 257);
            this.label205.Name = "label205";
            this.label205.Size = new System.Drawing.Size(81, 13);
            this.label205.TabIndex = 318;
            this.label205.Text = "Return $30-[50]";
            // 
            // LottoUnActivatedReturn20Txt
            // 
            this.LottoUnActivatedReturn20Txt.Location = new System.Drawing.Point(611, 229);
            this.LottoUnActivatedReturn20Txt.Name = "LottoUnActivatedReturn20Txt";
            this.LottoUnActivatedReturn20Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn20Txt.TabIndex = 317;
            this.LottoUnActivatedReturn20Txt.Text = "0";
            this.LottoUnActivatedReturn20Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label204
            // 
            this.label204.AutoSize = true;
            this.label204.Location = new System.Drawing.Point(509, 232);
            this.label204.Name = "label204";
            this.label204.Size = new System.Drawing.Size(60, 13);
            this.label204.TabIndex = 316;
            this.label204.Text = "Return $20";
            // 
            // LottoUnActivatedReturn10Txt
            // 
            this.LottoUnActivatedReturn10Txt.Location = new System.Drawing.Point(611, 203);
            this.LottoUnActivatedReturn10Txt.Name = "LottoUnActivatedReturn10Txt";
            this.LottoUnActivatedReturn10Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn10Txt.TabIndex = 315;
            this.LottoUnActivatedReturn10Txt.Text = "0";
            this.LottoUnActivatedReturn10Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label203
            // 
            this.label203.AutoSize = true;
            this.label203.Location = new System.Drawing.Point(509, 206);
            this.label203.Name = "label203";
            this.label203.Size = new System.Drawing.Size(60, 13);
            this.label203.TabIndex = 314;
            this.label203.Text = "Return $10";
            // 
            // LottoUnActivatedReturn7Txt
            // 
            this.LottoUnActivatedReturn7Txt.Location = new System.Drawing.Point(611, 177);
            this.LottoUnActivatedReturn7Txt.Name = "LottoUnActivatedReturn7Txt";
            this.LottoUnActivatedReturn7Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn7Txt.TabIndex = 313;
            this.LottoUnActivatedReturn7Txt.Text = "0";
            this.LottoUnActivatedReturn7Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label202
            // 
            this.label202.AutoSize = true;
            this.label202.Location = new System.Drawing.Point(509, 180);
            this.label202.Name = "label202";
            this.label202.Size = new System.Drawing.Size(54, 13);
            this.label202.TabIndex = 312;
            this.label202.Text = "Return $7";
            // 
            // LottoUnActivatedReturn5Txt
            // 
            this.LottoUnActivatedReturn5Txt.Location = new System.Drawing.Point(611, 150);
            this.LottoUnActivatedReturn5Txt.Name = "LottoUnActivatedReturn5Txt";
            this.LottoUnActivatedReturn5Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn5Txt.TabIndex = 311;
            this.LottoUnActivatedReturn5Txt.Text = "0";
            this.LottoUnActivatedReturn5Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label201
            // 
            this.label201.AutoSize = true;
            this.label201.Location = new System.Drawing.Point(509, 153);
            this.label201.Name = "label201";
            this.label201.Size = new System.Drawing.Size(54, 13);
            this.label201.TabIndex = 310;
            this.label201.Text = "Return $5";
            // 
            // LottoUnActivatedReturn4Txt
            // 
            this.LottoUnActivatedReturn4Txt.Location = new System.Drawing.Point(611, 124);
            this.LottoUnActivatedReturn4Txt.Name = "LottoUnActivatedReturn4Txt";
            this.LottoUnActivatedReturn4Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn4Txt.TabIndex = 309;
            this.LottoUnActivatedReturn4Txt.Text = "0";
            this.LottoUnActivatedReturn4Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(509, 127);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(54, 13);
            this.label200.TabIndex = 308;
            this.label200.Text = "Return $4";
            // 
            // LottoUnActivatedReturn3Txt
            // 
            this.LottoUnActivatedReturn3Txt.Location = new System.Drawing.Point(611, 99);
            this.LottoUnActivatedReturn3Txt.Name = "LottoUnActivatedReturn3Txt";
            this.LottoUnActivatedReturn3Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn3Txt.TabIndex = 307;
            this.LottoUnActivatedReturn3Txt.Text = "0";
            this.LottoUnActivatedReturn3Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label199
            // 
            this.label199.AutoSize = true;
            this.label199.Location = new System.Drawing.Point(509, 102);
            this.label199.Name = "label199";
            this.label199.Size = new System.Drawing.Size(54, 13);
            this.label199.TabIndex = 306;
            this.label199.Text = "Return $3";
            // 
            // LottoUnActivatedReturn2Txt
            // 
            this.LottoUnActivatedReturn2Txt.Location = new System.Drawing.Point(611, 73);
            this.LottoUnActivatedReturn2Txt.Name = "LottoUnActivatedReturn2Txt";
            this.LottoUnActivatedReturn2Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn2Txt.TabIndex = 305;
            this.LottoUnActivatedReturn2Txt.Text = "0";
            this.LottoUnActivatedReturn2Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label198
            // 
            this.label198.AutoSize = true;
            this.label198.Location = new System.Drawing.Point(509, 76);
            this.label198.Name = "label198";
            this.label198.Size = new System.Drawing.Size(54, 13);
            this.label198.TabIndex = 304;
            this.label198.Text = "Return $2";
            // 
            // LottoUnActivatedReturn1100Txt
            // 
            this.LottoUnActivatedReturn1100Txt.Location = new System.Drawing.Point(611, 47);
            this.LottoUnActivatedReturn1100Txt.Name = "LottoUnActivatedReturn1100Txt";
            this.LottoUnActivatedReturn1100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn1100Txt.TabIndex = 303;
            this.LottoUnActivatedReturn1100Txt.Text = "0";
            this.LottoUnActivatedReturn1100Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label197
            // 
            this.label197.AutoSize = true;
            this.label197.Location = new System.Drawing.Point(509, 50);
            this.label197.Name = "label197";
            this.label197.Size = new System.Drawing.Size(81, 13);
            this.label197.TabIndex = 302;
            this.label197.Text = "Return $1-[100]";
            // 
            // LottoUnActivatedReturn150Txt
            // 
            this.LottoUnActivatedReturn150Txt.Location = new System.Drawing.Point(611, 20);
            this.LottoUnActivatedReturn150Txt.Name = "LottoUnActivatedReturn150Txt";
            this.LottoUnActivatedReturn150Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedReturn150Txt.TabIndex = 301;
            this.LottoUnActivatedReturn150Txt.Text = "0";
            this.LottoUnActivatedReturn150Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label196
            // 
            this.label196.AutoSize = true;
            this.label196.Location = new System.Drawing.Point(509, 23);
            this.label196.Name = "label196";
            this.label196.Size = new System.Drawing.Size(75, 13);
            this.label196.TabIndex = 300;
            this.label196.Text = "Return $1-[50]";
            // 
            // LottoUnActivatedSubtract30100Txt
            // 
            this.LottoUnActivatedSubtract30100Txt.Location = new System.Drawing.Point(449, 277);
            this.LottoUnActivatedSubtract30100Txt.Name = "LottoUnActivatedSubtract30100Txt";
            this.LottoUnActivatedSubtract30100Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedSubtract30100Txt.TabIndex = 299;
            this.LottoUnActivatedSubtract30100Txt.Text = "0";
            this.LottoUnActivatedSubtract30100Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Location = new System.Drawing.Point(347, 284);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(95, 13);
            this.label187.TabIndex = 298;
            this.label187.Text = "Subtract $30-[100]";
            // 
            // LottoUnActivatedAdd30100Txt
            // 
            this.LottoUnActivatedAdd30100Txt.Location = new System.Drawing.Point(275, 284);
            this.LottoUnActivatedAdd30100Txt.Name = "LottoUnActivatedAdd30100Txt";
            this.LottoUnActivatedAdd30100Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedAdd30100Txt.TabIndex = 297;
            this.LottoUnActivatedAdd30100Txt.Text = "0";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Location = new System.Drawing.Point(175, 284);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(74, 13);
            this.label188.TabIndex = 296;
            this.label188.Text = "Add $30-[100]";
            // 
            // LottoUnActivatedOpening30100Txt
            // 
            this.LottoUnActivatedOpening30100Txt.Location = new System.Drawing.Point(107, 277);
            this.LottoUnActivatedOpening30100Txt.Name = "LottoUnActivatedOpening30100Txt";
            this.LottoUnActivatedOpening30100Txt.ReadOnly = true;
            this.LottoUnActivatedOpening30100Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedOpening30100Txt.TabIndex = 295;
            this.LottoUnActivatedOpening30100Txt.Text = "0";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(4, 280);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(95, 13);
            this.label189.TabIndex = 294;
            this.label189.Text = "Opening $30-[100]";
            // 
            // LottoUnactivatedSubstract1100Txt
            // 
            this.LottoUnactivatedSubstract1100Txt.Location = new System.Drawing.Point(450, 50);
            this.LottoUnactivatedSubstract1100Txt.Name = "LottoUnactivatedSubstract1100Txt";
            this.LottoUnactivatedSubstract1100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnactivatedSubstract1100Txt.TabIndex = 293;
            this.LottoUnactivatedSubstract1100Txt.Text = "0";
            this.LottoUnactivatedSubstract1100Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Location = new System.Drawing.Point(346, 50);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(89, 13);
            this.label184.TabIndex = 292;
            this.label184.Text = "Subtract $1-[100]";
            // 
            // LottoUnActivatedAdd1100Txt
            // 
            this.LottoUnActivatedAdd1100Txt.Location = new System.Drawing.Point(278, 53);
            this.LottoUnActivatedAdd1100Txt.Name = "LottoUnActivatedAdd1100Txt";
            this.LottoUnActivatedAdd1100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedAdd1100Txt.TabIndex = 291;
            this.LottoUnActivatedAdd1100Txt.Text = "0";
            this.LottoUnActivatedAdd1100Txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllTextBox_KeyPress);
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(176, 53);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(68, 13);
            this.label185.TabIndex = 290;
            this.label185.Text = "Add $1-[100]";
            // 
            // LottoUnActivatedOpening1100Txt
            // 
            this.LottoUnActivatedOpening1100Txt.Location = new System.Drawing.Point(108, 53);
            this.LottoUnActivatedOpening1100Txt.Name = "LottoUnActivatedOpening1100Txt";
            this.LottoUnActivatedOpening1100Txt.ReadOnly = true;
            this.LottoUnActivatedOpening1100Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedOpening1100Txt.TabIndex = 289;
            this.LottoUnActivatedOpening1100Txt.Text = "0";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(5, 53);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(89, 13);
            this.label186.TabIndex = 288;
            this.label186.Text = "Opening $1-[100]";
            // 
            // LottoUnActivatedSubtract30Txt
            // 
            this.LottoUnActivatedSubtract30Txt.Location = new System.Drawing.Point(449, 251);
            this.LottoUnActivatedSubtract30Txt.Name = "LottoUnActivatedSubtract30Txt";
            this.LottoUnActivatedSubtract30Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedSubtract30Txt.TabIndex = 287;
            this.LottoUnActivatedSubtract30Txt.Text = "0";
            this.LottoUnActivatedSubtract30Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(346, 258);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(89, 13);
            this.label47.TabIndex = 286;
            this.label47.Text = "Subtract $30-[50]";
            // 
            // LottoUnActivatedAdd30Txt
            // 
            this.LottoUnActivatedAdd30Txt.Location = new System.Drawing.Point(275, 258);
            this.LottoUnActivatedAdd30Txt.Name = "LottoUnActivatedAdd30Txt";
            this.LottoUnActivatedAdd30Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedAdd30Txt.TabIndex = 285;
            this.LottoUnActivatedAdd30Txt.Text = "0";
            this.LottoUnActivatedAdd30Txt.TextChanged += new System.EventHandler(this.LottoUnActivatedAdd30Txt_TextChanged);
            this.LottoUnActivatedAdd30Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(175, 258);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(68, 13);
            this.label48.TabIndex = 284;
            this.label48.Text = "Add $30-[50]";
            this.label48.Click += new System.EventHandler(this.label48_Click);
            // 
            // LottoUnActivatedOpening30Txt
            // 
            this.LottoUnActivatedOpening30Txt.Location = new System.Drawing.Point(107, 251);
            this.LottoUnActivatedOpening30Txt.Name = "LottoUnActivatedOpening30Txt";
            this.LottoUnActivatedOpening30Txt.ReadOnly = true;
            this.LottoUnActivatedOpening30Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedOpening30Txt.TabIndex = 283;
            this.LottoUnActivatedOpening30Txt.Text = "0";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(4, 254);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(89, 13);
            this.label49.TabIndex = 282;
            this.label49.Text = "Opening $30-[50]";
            // 
            // LottoUnActivatedSubtract20Txt
            // 
            this.LottoUnActivatedSubtract20Txt.Location = new System.Drawing.Point(449, 225);
            this.LottoUnActivatedSubtract20Txt.Name = "LottoUnActivatedSubtract20Txt";
            this.LottoUnActivatedSubtract20Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedSubtract20Txt.TabIndex = 281;
            this.LottoUnActivatedSubtract20Txt.Text = "0";
            this.LottoUnActivatedSubtract20Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(347, 225);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(68, 13);
            this.label50.TabIndex = 280;
            this.label50.Text = "Subtract $20";
            // 
            // LottoUnActivatedAdd20Txt
            // 
            this.LottoUnActivatedAdd20Txt.Location = new System.Drawing.Point(275, 233);
            this.LottoUnActivatedAdd20Txt.Name = "LottoUnActivatedAdd20Txt";
            this.LottoUnActivatedAdd20Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedAdd20Txt.TabIndex = 279;
            this.LottoUnActivatedAdd20Txt.Text = "0";
            this.LottoUnActivatedAdd20Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(176, 233);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(47, 13);
            this.label51.TabIndex = 278;
            this.label51.Text = "Add $20";
            // 
            // LottoUnActivatedOpening20Txt
            // 
            this.LottoUnActivatedOpening20Txt.Location = new System.Drawing.Point(107, 225);
            this.LottoUnActivatedOpening20Txt.Name = "LottoUnActivatedOpening20Txt";
            this.LottoUnActivatedOpening20Txt.ReadOnly = true;
            this.LottoUnActivatedOpening20Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedOpening20Txt.TabIndex = 277;
            this.LottoUnActivatedOpening20Txt.Text = "0";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(4, 225);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(68, 13);
            this.label52.TabIndex = 276;
            this.label52.Text = "Opening $20";
            // 
            // LottoUnActivatedSubtract10Txt
            // 
            this.LottoUnActivatedSubtract10Txt.Location = new System.Drawing.Point(449, 200);
            this.LottoUnActivatedSubtract10Txt.Name = "LottoUnActivatedSubtract10Txt";
            this.LottoUnActivatedSubtract10Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedSubtract10Txt.TabIndex = 275;
            this.LottoUnActivatedSubtract10Txt.Text = "0";
            this.LottoUnActivatedSubtract10Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(347, 203);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(68, 13);
            this.label53.TabIndex = 274;
            this.label53.Text = "Subtract $10";
            // 
            // LottoUnActivatedAdd10Txt
            // 
            this.LottoUnActivatedAdd10Txt.Location = new System.Drawing.Point(276, 206);
            this.LottoUnActivatedAdd10Txt.Name = "LottoUnActivatedAdd10Txt";
            this.LottoUnActivatedAdd10Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedAdd10Txt.TabIndex = 273;
            this.LottoUnActivatedAdd10Txt.Text = "0";
            this.LottoUnActivatedAdd10Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(175, 206);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(47, 13);
            this.label54.TabIndex = 272;
            this.label54.Text = "Add $10";
            // 
            // LottoUnActivatedOpening10Txt
            // 
            this.LottoUnActivatedOpening10Txt.Location = new System.Drawing.Point(107, 199);
            this.LottoUnActivatedOpening10Txt.Name = "LottoUnActivatedOpening10Txt";
            this.LottoUnActivatedOpening10Txt.ReadOnly = true;
            this.LottoUnActivatedOpening10Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedOpening10Txt.TabIndex = 271;
            this.LottoUnActivatedOpening10Txt.Text = "0";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(4, 199);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(68, 13);
            this.label55.TabIndex = 270;
            this.label55.Text = "Opening $10";
            // 
            // LottoUnActivatedSubtract7Txt
            // 
            this.LottoUnActivatedSubtract7Txt.Location = new System.Drawing.Point(450, 174);
            this.LottoUnActivatedSubtract7Txt.Name = "LottoUnActivatedSubtract7Txt";
            this.LottoUnActivatedSubtract7Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedSubtract7Txt.TabIndex = 269;
            this.LottoUnActivatedSubtract7Txt.Text = "0";
            this.LottoUnActivatedSubtract7Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(347, 179);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(62, 13);
            this.label38.TabIndex = 268;
            this.label38.Text = "Subtract $7";
            // 
            // LottoUnActivatedAdd7Txt
            // 
            this.LottoUnActivatedAdd7Txt.Location = new System.Drawing.Point(276, 180);
            this.LottoUnActivatedAdd7Txt.Name = "LottoUnActivatedAdd7Txt";
            this.LottoUnActivatedAdd7Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedAdd7Txt.TabIndex = 267;
            this.LottoUnActivatedAdd7Txt.Text = "0";
            this.LottoUnActivatedAdd7Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(175, 180);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(41, 13);
            this.label39.TabIndex = 266;
            this.label39.Text = "Add $7";
            this.label39.Click += new System.EventHandler(this.label39_Click);
            // 
            // LottoUnActivatedOpening7Txt
            // 
            this.LottoUnActivatedOpening7Txt.Location = new System.Drawing.Point(107, 176);
            this.LottoUnActivatedOpening7Txt.Name = "LottoUnActivatedOpening7Txt";
            this.LottoUnActivatedOpening7Txt.ReadOnly = true;
            this.LottoUnActivatedOpening7Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedOpening7Txt.TabIndex = 265;
            this.LottoUnActivatedOpening7Txt.Text = "0";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(3, 176);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(62, 13);
            this.label40.TabIndex = 264;
            this.label40.Text = "Opening $7";
            // 
            // LottoUnActivatedSubtract5Txt
            // 
            this.LottoUnActivatedSubtract5Txt.Location = new System.Drawing.Point(450, 148);
            this.LottoUnActivatedSubtract5Txt.Name = "LottoUnActivatedSubtract5Txt";
            this.LottoUnActivatedSubtract5Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedSubtract5Txt.TabIndex = 263;
            this.LottoUnActivatedSubtract5Txt.Text = "0";
            this.LottoUnActivatedSubtract5Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(346, 148);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(62, 13);
            this.label41.TabIndex = 262;
            this.label41.Text = "Subtract $5";
            // 
            // LottoUnActivatedAdd5Txt
            // 
            this.LottoUnActivatedAdd5Txt.Location = new System.Drawing.Point(276, 156);
            this.LottoUnActivatedAdd5Txt.Name = "LottoUnActivatedAdd5Txt";
            this.LottoUnActivatedAdd5Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedAdd5Txt.TabIndex = 261;
            this.LottoUnActivatedAdd5Txt.Text = "0";
            this.LottoUnActivatedAdd5Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(176, 155);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 13);
            this.label42.TabIndex = 260;
            this.label42.Text = "Add $5";
            // 
            // LottoUnActivatedOpening5Txt
            // 
            this.LottoUnActivatedOpening5Txt.Location = new System.Drawing.Point(107, 152);
            this.LottoUnActivatedOpening5Txt.Name = "LottoUnActivatedOpening5Txt";
            this.LottoUnActivatedOpening5Txt.ReadOnly = true;
            this.LottoUnActivatedOpening5Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedOpening5Txt.TabIndex = 259;
            this.LottoUnActivatedOpening5Txt.Text = "0";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(5, 155);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(62, 13);
            this.label43.TabIndex = 258;
            this.label43.Text = "Opening $5";
            // 
            // LottoUnActivatedSubtract4Txt
            // 
            this.LottoUnActivatedSubtract4Txt.Location = new System.Drawing.Point(450, 124);
            this.LottoUnActivatedSubtract4Txt.Name = "LottoUnActivatedSubtract4Txt";
            this.LottoUnActivatedSubtract4Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedSubtract4Txt.TabIndex = 257;
            this.LottoUnActivatedSubtract4Txt.Text = "0";
            this.LottoUnActivatedSubtract4Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(346, 124);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(62, 13);
            this.label44.TabIndex = 256;
            this.label44.Text = "Subtract $4";
            // 
            // LottoUnActivatedAdd4Txt
            // 
            this.LottoUnActivatedAdd4Txt.Location = new System.Drawing.Point(276, 129);
            this.LottoUnActivatedAdd4Txt.Name = "LottoUnActivatedAdd4Txt";
            this.LottoUnActivatedAdd4Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedAdd4Txt.TabIndex = 255;
            this.LottoUnActivatedAdd4Txt.Text = "0";
            this.LottoUnActivatedAdd4Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(176, 131);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 13);
            this.label45.TabIndex = 254;
            this.label45.Text = "Add $4";
            // 
            // LottoUnActivatedOpening4Txt
            // 
            this.LottoUnActivatedOpening4Txt.Location = new System.Drawing.Point(107, 129);
            this.LottoUnActivatedOpening4Txt.Name = "LottoUnActivatedOpening4Txt";
            this.LottoUnActivatedOpening4Txt.ReadOnly = true;
            this.LottoUnActivatedOpening4Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedOpening4Txt.TabIndex = 253;
            this.LottoUnActivatedOpening4Txt.Text = "0";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(4, 129);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(62, 13);
            this.label46.TabIndex = 252;
            this.label46.Text = "Opening $4";
            // 
            // LottoUnActivatedSubtract3Txt
            // 
            this.LottoUnActivatedSubtract3Txt.Location = new System.Drawing.Point(449, 102);
            this.LottoUnActivatedSubtract3Txt.Name = "LottoUnActivatedSubtract3Txt";
            this.LottoUnActivatedSubtract3Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedSubtract3Txt.TabIndex = 251;
            this.LottoUnActivatedSubtract3Txt.Text = "0";
            this.LottoUnActivatedSubtract3Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(346, 105);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(62, 13);
            this.label35.TabIndex = 250;
            this.label35.Text = "Subtract $3";
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // LottoUnActivatedAdd3Txt
            // 
            this.LottoUnActivatedAdd3Txt.Location = new System.Drawing.Point(277, 105);
            this.LottoUnActivatedAdd3Txt.Name = "LottoUnActivatedAdd3Txt";
            this.LottoUnActivatedAdd3Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedAdd3Txt.TabIndex = 249;
            this.LottoUnActivatedAdd3Txt.Text = "0";
            this.LottoUnActivatedAdd3Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(176, 109);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 248;
            this.label36.Text = "Add $3";
            // 
            // LottoUnActivatedOpening3Txt
            // 
            this.LottoUnActivatedOpening3Txt.Location = new System.Drawing.Point(107, 106);
            this.LottoUnActivatedOpening3Txt.Name = "LottoUnActivatedOpening3Txt";
            this.LottoUnActivatedOpening3Txt.ReadOnly = true;
            this.LottoUnActivatedOpening3Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedOpening3Txt.TabIndex = 247;
            this.LottoUnActivatedOpening3Txt.Text = "0";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(4, 106);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(62, 13);
            this.label37.TabIndex = 246;
            this.label37.Text = "Opening $3";
            // 
            // LottoUnActivatedSubtract2Txt
            // 
            this.LottoUnActivatedSubtract2Txt.Location = new System.Drawing.Point(450, 78);
            this.LottoUnActivatedSubtract2Txt.Name = "LottoUnActivatedSubtract2Txt";
            this.LottoUnActivatedSubtract2Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedSubtract2Txt.TabIndex = 245;
            this.LottoUnActivatedSubtract2Txt.Text = "0";
            this.LottoUnActivatedSubtract2Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(347, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 244;
            this.label1.Text = "Subtract $2";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // LottoUnActivatedAdd2Txt
            // 
            this.LottoUnActivatedAdd2Txt.Location = new System.Drawing.Point(277, 79);
            this.LottoUnActivatedAdd2Txt.Name = "LottoUnActivatedAdd2Txt";
            this.LottoUnActivatedAdd2Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoUnActivatedAdd2Txt.TabIndex = 243;
            this.LottoUnActivatedAdd2Txt.Text = "0";
            this.LottoUnActivatedAdd2Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(175, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 242;
            this.label2.Text = "Add $2";
            // 
            // LottoUnActivatedOpening2Txt
            // 
            this.LottoUnActivatedOpening2Txt.Location = new System.Drawing.Point(107, 82);
            this.LottoUnActivatedOpening2Txt.Name = "LottoUnActivatedOpening2Txt";
            this.LottoUnActivatedOpening2Txt.ReadOnly = true;
            this.LottoUnActivatedOpening2Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedOpening2Txt.TabIndex = 241;
            this.LottoUnActivatedOpening2Txt.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 240;
            this.label8.Text = "Opening $2";
            // 
            // UnActivatedDocsDgv
            // 
            this.UnActivatedDocsDgv.AllowDrop = true;
            this.UnActivatedDocsDgv.AllowUserToAddRows = false;
            this.UnActivatedDocsDgv.AllowUserToDeleteRows = false;
            this.UnActivatedDocsDgv.AllowUserToResizeColumns = false;
            this.UnActivatedDocsDgv.AllowUserToResizeRows = false;
            this.UnActivatedDocsDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.UnActivatedDocsDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.UnActivatedDocsDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.UnActivatedDocsDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UnActivatedDocsDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CashCheckBox});
            this.UnActivatedDocsDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.UnActivatedDocsDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.UnActivatedDocsDgv.Location = new System.Drawing.Point(1005, 19);
            this.UnActivatedDocsDgv.MultiSelect = false;
            this.UnActivatedDocsDgv.Name = "UnActivatedDocsDgv";
            this.UnActivatedDocsDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.UnActivatedDocsDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.UnActivatedDocsDgv.RowHeadersVisible = false;
            this.UnActivatedDocsDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.UnActivatedDocsDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.UnActivatedDocsDgv.Size = new System.Drawing.Size(222, 105);
            this.UnActivatedDocsDgv.TabIndex = 238;
            // 
            // CashCheckBox
            // 
            this.CashCheckBox.HeaderText = "";
            this.CashCheckBox.Name = "CashCheckBox";
            this.CashCheckBox.Width = 5;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(868, 19);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(107, 15);
            this.label68.TabIndex = 239;
            this.label68.Text = "Un-Activated Docs";
            // 
            // LottoUnActivatedRemakrsTxt
            // 
            this.LottoUnActivatedRemakrsTxt.Location = new System.Drawing.Point(1005, 130);
            this.LottoUnActivatedRemakrsTxt.Name = "LottoUnActivatedRemakrsTxt";
            this.LottoUnActivatedRemakrsTxt.Size = new System.Drawing.Size(222, 77);
            this.LottoUnActivatedRemakrsTxt.TabIndex = 236;
            this.LottoUnActivatedRemakrsTxt.Text = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(938, 131);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 234;
            this.label14.Text = "Remarks";
            // 
            // LottoTotalUnActivatedTxt
            // 
            this.LottoTotalUnActivatedTxt.Location = new System.Drawing.Point(1005, 213);
            this.LottoTotalUnActivatedTxt.Name = "LottoTotalUnActivatedTxt";
            this.LottoTotalUnActivatedTxt.ReadOnly = true;
            this.LottoTotalUnActivatedTxt.Size = new System.Drawing.Size(105, 20);
            this.LottoTotalUnActivatedTxt.TabIndex = 225;
            this.LottoTotalUnActivatedTxt.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(868, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 224;
            this.label7.Text = "Total Unactivated Lotto";
            // 
            // LottoUnactivatedSubstract150Txt
            // 
            this.LottoUnactivatedSubstract150Txt.Location = new System.Drawing.Point(450, 20);
            this.LottoUnactivatedSubstract150Txt.Name = "LottoUnactivatedSubstract150Txt";
            this.LottoUnactivatedSubstract150Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnactivatedSubstract150Txt.TabIndex = 5;
            this.LottoUnactivatedSubstract150Txt.Text = "0";
            this.LottoUnactivatedSubstract150Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(346, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Subtract $1-[50]";
            // 
            // LottoUnActivatedAdd150Txt
            // 
            this.LottoUnActivatedAdd150Txt.Location = new System.Drawing.Point(278, 23);
            this.LottoUnActivatedAdd150Txt.Name = "LottoUnActivatedAdd150Txt";
            this.LottoUnActivatedAdd150Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoUnActivatedAdd150Txt.TabIndex = 3;
            this.LottoUnActivatedAdd150Txt.Text = "0";
            this.LottoUnActivatedAdd150Txt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.AllTextBox_KeyPress);
            this.LottoUnActivatedAdd150Txt.Leave += new System.EventHandler(this.LottoUnActivatedAdd1Txt_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(176, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Add $1-[50]";
            // 
            // LottoUnActivatedOpening150Txt
            // 
            this.LottoUnActivatedOpening150Txt.Location = new System.Drawing.Point(108, 23);
            this.LottoUnActivatedOpening150Txt.Name = "LottoUnActivatedOpening150Txt";
            this.LottoUnActivatedOpening150Txt.ReadOnly = true;
            this.LottoUnActivatedOpening150Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoUnActivatedOpening150Txt.TabIndex = 1;
            this.LottoUnActivatedOpening150Txt.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Opening $1-[50]";
            // 
            // LottoUnActivatedBtn
            // 
            this.LottoUnActivatedBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LottoUnActivatedBtn.FlatAppearance.BorderSize = 2;
            this.LottoUnActivatedBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LottoUnActivatedBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LottoUnActivatedBtn.Location = new System.Drawing.Point(239, 56);
            this.LottoUnActivatedBtn.Name = "LottoUnActivatedBtn";
            this.LottoUnActivatedBtn.Size = new System.Drawing.Size(90, 32);
            this.LottoUnActivatedBtn.TabIndex = 233;
            this.LottoUnActivatedBtn.Text = "&Save";
            this.LottoUnActivatedBtn.UseVisualStyleBackColor = true;
            this.LottoUnActivatedBtn.Click += new System.EventHandler(this.LottoUnActivatedBtn_Click);
            // 
            // ActivatedDocsDgv
            // 
            this.ActivatedDocsDgv.AllowDrop = true;
            this.ActivatedDocsDgv.AllowUserToAddRows = false;
            this.ActivatedDocsDgv.AllowUserToDeleteRows = false;
            this.ActivatedDocsDgv.AllowUserToResizeColumns = false;
            this.ActivatedDocsDgv.AllowUserToResizeRows = false;
            this.ActivatedDocsDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ActivatedDocsDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ActivatedDocsDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.ActivatedDocsDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ActivatedDocsDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1});
            this.ActivatedDocsDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ActivatedDocsDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.ActivatedDocsDgv.Location = new System.Drawing.Point(1005, 33);
            this.ActivatedDocsDgv.MultiSelect = false;
            this.ActivatedDocsDgv.Name = "ActivatedDocsDgv";
            this.ActivatedDocsDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ActivatedDocsDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.ActivatedDocsDgv.RowHeadersVisible = false;
            this.ActivatedDocsDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ActivatedDocsDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ActivatedDocsDgv.Size = new System.Drawing.Size(222, 105);
            this.ActivatedDocsDgv.TabIndex = 240;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 5;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(875, 39);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(87, 15);
            this.label69.TabIndex = 241;
            this.label69.Text = "Activated Docs";
            // 
            // LottoActivatedRemarksTxt
            // 
            this.LottoActivatedRemarksTxt.Location = new System.Drawing.Point(1005, 144);
            this.LottoActivatedRemarksTxt.Name = "LottoActivatedRemarksTxt";
            this.LottoActivatedRemarksTxt.Size = new System.Drawing.Size(222, 77);
            this.LottoActivatedRemarksTxt.TabIndex = 237;
            this.LottoActivatedRemarksTxt.Text = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(875, 147);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 235;
            this.label15.Text = "Remarks";
            // 
            // LottoActivatedTotalTxt
            // 
            this.LottoActivatedTotalTxt.Location = new System.Drawing.Point(1005, 227);
            this.LottoActivatedTotalTxt.Name = "LottoActivatedTotalTxt";
            this.LottoActivatedTotalTxt.ReadOnly = true;
            this.LottoActivatedTotalTxt.Size = new System.Drawing.Size(105, 20);
            this.LottoActivatedTotalTxt.TabIndex = 225;
            this.LottoActivatedTotalTxt.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(875, 230);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 13);
            this.label10.TabIndex = 224;
            this.label10.Text = "Total Activated Lotto";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ViewBtn);
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.DateCalender);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.LottoUnActivatedBtn);
            this.groupBox1.Location = new System.Drawing.Point(12, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1233, 101);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Master";
            // 
            // ViewBtn
            // 
            this.ViewBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ViewBtn.FlatAppearance.BorderSize = 2;
            this.ViewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewBtn.Location = new System.Drawing.Point(133, 56);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(90, 32);
            this.ViewBtn.TabIndex = 232;
            this.ViewBtn.Text = "&View";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(349, 56);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 231;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // DateCalender
            // 
            this.DateCalender.CustomFormat = "MMM/dd/yyyy";
            this.DateCalender.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateCalender.Location = new System.Drawing.Point(131, 20);
            this.DateCalender.MaxDate = new System.DateTime(2215, 12, 1, 0, 0, 0, 0);
            this.DateCalender.Name = "DateCalender";
            this.DateCalender.Size = new System.Drawing.Size(202, 20);
            this.DateCalender.TabIndex = 219;
            this.DateCalender.Value = new System.DateTime(2018, 4, 6, 16, 9, 36, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(20, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 218;
            this.label3.Text = "Date";
            // 
            // OnlineGroup
            // 
            this.OnlineGroup.Controls.Add(this.label80);
            this.OnlineGroup.Controls.Add(this.LottoReportPayoutTxt);
            this.OnlineGroup.Controls.Add(this.LottoBackofficePayoutTxt);
            this.OnlineGroup.Controls.Add(this.LottoAdjustCPayout);
            this.OnlineGroup.Controls.Add(this.LottoEOS1PayoutTxt);
            this.OnlineGroup.Controls.Add(this.LottoShiftDiffPayoutTxt);
            this.OnlineGroup.Controls.Add(this.LottoEOS2PayoutTxt);
            this.OnlineGroup.Controls.Add(this.TSoldPayoutTxt);
            this.OnlineGroup.Controls.Add(this.LottoEOS3PayoutTxt);
            this.OnlineGroup.Controls.Add(this.ScratchWinOnlineDocsDgv);
            this.OnlineGroup.Controls.Add(this.label26);
            this.OnlineGroup.Controls.Add(this.label70);
            this.OnlineGroup.Controls.Add(this.label27);
            this.OnlineGroup.Controls.Add(this.LottoOnlineReportTxt);
            this.OnlineGroup.Controls.Add(this.label28);
            this.OnlineGroup.Controls.Add(this.LottoScratchOnlineRemarksTxt);
            this.OnlineGroup.Controls.Add(this.LottoOnlineBackOfficetxt);
            this.OnlineGroup.Controls.Add(this.label67);
            this.OnlineGroup.Controls.Add(this.label29);
            this.OnlineGroup.Controls.Add(this.LottoOnlineAdjustCanceltxt);
            this.OnlineGroup.Controls.Add(this.label30);
            this.OnlineGroup.Controls.Add(this.LottoOnlineEOS1Txt);
            this.OnlineGroup.Controls.Add(this.LottoOnlineShiftsDiffTxt);
            this.OnlineGroup.Controls.Add(this.label31);
            this.OnlineGroup.Controls.Add(this.label34);
            this.OnlineGroup.Controls.Add(this.LottoOnlineEOS2Txt);
            this.OnlineGroup.Controls.Add(this.LottoOnlineTSoldTxt);
            this.OnlineGroup.Controls.Add(this.label32);
            this.OnlineGroup.Controls.Add(this.label33);
            this.OnlineGroup.Controls.Add(this.LottoOnlineEOS3Txt);
            this.OnlineGroup.Location = new System.Drawing.Point(18, 1615);
            this.OnlineGroup.Name = "OnlineGroup";
            this.OnlineGroup.Size = new System.Drawing.Size(1088, 258);
            this.OnlineGroup.TabIndex = 246;
            this.OnlineGroup.TabStop = false;
            this.OnlineGroup.Text = "Lotto Scratch Online";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(284, 17);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(75, 13);
            this.label80.TabIndex = 255;
            this.label80.Text = "Online PayOut";
            // 
            // LottoReportPayoutTxt
            // 
            this.LottoReportPayoutTxt.Location = new System.Drawing.Point(287, 41);
            this.LottoReportPayoutTxt.Name = "LottoReportPayoutTxt";
            this.LottoReportPayoutTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoReportPayoutTxt.TabIndex = 247;
            this.LottoReportPayoutTxt.Text = "0";
            this.LottoReportPayoutTxt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // LottoBackofficePayoutTxt
            // 
            this.LottoBackofficePayoutTxt.Location = new System.Drawing.Point(287, 68);
            this.LottoBackofficePayoutTxt.Name = "LottoBackofficePayoutTxt";
            this.LottoBackofficePayoutTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoBackofficePayoutTxt.TabIndex = 248;
            this.LottoBackofficePayoutTxt.Text = "0";
            this.LottoBackofficePayoutTxt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // LottoAdjustCPayout
            // 
            this.LottoAdjustCPayout.Location = new System.Drawing.Point(287, 95);
            this.LottoAdjustCPayout.Name = "LottoAdjustCPayout";
            this.LottoAdjustCPayout.Size = new System.Drawing.Size(125, 20);
            this.LottoAdjustCPayout.TabIndex = 249;
            this.LottoAdjustCPayout.Text = "0";
            this.LottoAdjustCPayout.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // LottoEOS1PayoutTxt
            // 
            this.LottoEOS1PayoutTxt.Location = new System.Drawing.Point(287, 122);
            this.LottoEOS1PayoutTxt.Name = "LottoEOS1PayoutTxt";
            this.LottoEOS1PayoutTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoEOS1PayoutTxt.TabIndex = 250;
            this.LottoEOS1PayoutTxt.Text = "0";
            this.LottoEOS1PayoutTxt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // LottoShiftDiffPayoutTxt
            // 
            this.LottoShiftDiffPayoutTxt.BackColor = System.Drawing.Color.Red;
            this.LottoShiftDiffPayoutTxt.Location = new System.Drawing.Point(287, 219);
            this.LottoShiftDiffPayoutTxt.Name = "LottoShiftDiffPayoutTxt";
            this.LottoShiftDiffPayoutTxt.ReadOnly = true;
            this.LottoShiftDiffPayoutTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoShiftDiffPayoutTxt.TabIndex = 254;
            this.LottoShiftDiffPayoutTxt.Text = "0";
            // 
            // LottoEOS2PayoutTxt
            // 
            this.LottoEOS2PayoutTxt.Location = new System.Drawing.Point(287, 146);
            this.LottoEOS2PayoutTxt.Name = "LottoEOS2PayoutTxt";
            this.LottoEOS2PayoutTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoEOS2PayoutTxt.TabIndex = 251;
            this.LottoEOS2PayoutTxt.Text = "0";
            this.LottoEOS2PayoutTxt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // TSoldPayoutTxt
            // 
            this.TSoldPayoutTxt.Location = new System.Drawing.Point(287, 197);
            this.TSoldPayoutTxt.Name = "TSoldPayoutTxt";
            this.TSoldPayoutTxt.ReadOnly = true;
            this.TSoldPayoutTxt.Size = new System.Drawing.Size(125, 20);
            this.TSoldPayoutTxt.TabIndex = 253;
            this.TSoldPayoutTxt.Text = "0";
            // 
            // LottoEOS3PayoutTxt
            // 
            this.LottoEOS3PayoutTxt.Location = new System.Drawing.Point(287, 170);
            this.LottoEOS3PayoutTxt.Name = "LottoEOS3PayoutTxt";
            this.LottoEOS3PayoutTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoEOS3PayoutTxt.TabIndex = 252;
            this.LottoEOS3PayoutTxt.Text = "0";
            this.LottoEOS3PayoutTxt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // ScratchWinOnlineDocsDgv
            // 
            this.ScratchWinOnlineDocsDgv.AllowDrop = true;
            this.ScratchWinOnlineDocsDgv.AllowUserToAddRows = false;
            this.ScratchWinOnlineDocsDgv.AllowUserToDeleteRows = false;
            this.ScratchWinOnlineDocsDgv.AllowUserToResizeColumns = false;
            this.ScratchWinOnlineDocsDgv.AllowUserToResizeRows = false;
            this.ScratchWinOnlineDocsDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ScratchWinOnlineDocsDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ScratchWinOnlineDocsDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.ScratchWinOnlineDocsDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ScratchWinOnlineDocsDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn2});
            this.ScratchWinOnlineDocsDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ScratchWinOnlineDocsDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.ScratchWinOnlineDocsDgv.Location = new System.Drawing.Point(818, 41);
            this.ScratchWinOnlineDocsDgv.MultiSelect = false;
            this.ScratchWinOnlineDocsDgv.Name = "ScratchWinOnlineDocsDgv";
            this.ScratchWinOnlineDocsDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ScratchWinOnlineDocsDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.ScratchWinOnlineDocsDgv.RowHeadersVisible = false;
            this.ScratchWinOnlineDocsDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ScratchWinOnlineDocsDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ScratchWinOnlineDocsDgv.Size = new System.Drawing.Size(222, 105);
            this.ScratchWinOnlineDocsDgv.TabIndex = 245;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 5;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(128, 17);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(61, 13);
            this.label26.TabIndex = 22;
            this.label26.Text = "Online Sold";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(815, 18);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(143, 15);
            this.label70.TabIndex = 246;
            this.label70.Text = "Scratch Win Online Docs";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(14, 44);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(109, 13);
            this.label27.TabIndex = 23;
            this.label27.Text = "Lotto Terminal Report";
            // 
            // LottoOnlineReportTxt
            // 
            this.LottoOnlineReportTxt.Location = new System.Drawing.Point(131, 44);
            this.LottoOnlineReportTxt.Name = "LottoOnlineReportTxt";
            this.LottoOnlineReportTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoOnlineReportTxt.TabIndex = 24;
            this.LottoOnlineReportTxt.Text = "0";
            this.LottoOnlineReportTxt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(17, 71);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(63, 13);
            this.label28.TabIndex = 25;
            this.label28.Text = "Back Office";
            // 
            // LottoScratchOnlineRemarksTxt
            // 
            this.LottoScratchOnlineRemarksTxt.Location = new System.Drawing.Point(818, 161);
            this.LottoScratchOnlineRemarksTxt.Name = "LottoScratchOnlineRemarksTxt";
            this.LottoScratchOnlineRemarksTxt.Size = new System.Drawing.Size(222, 57);
            this.LottoScratchOnlineRemarksTxt.TabIndex = 242;
            this.LottoScratchOnlineRemarksTxt.Text = "";
            // 
            // LottoOnlineBackOfficetxt
            // 
            this.LottoOnlineBackOfficetxt.Location = new System.Drawing.Point(131, 71);
            this.LottoOnlineBackOfficetxt.Name = "LottoOnlineBackOfficetxt";
            this.LottoOnlineBackOfficetxt.Size = new System.Drawing.Size(125, 20);
            this.LottoOnlineBackOfficetxt.TabIndex = 26;
            this.LottoOnlineBackOfficetxt.Text = "0";
            this.LottoOnlineBackOfficetxt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(711, 172);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(49, 13);
            this.label67.TabIndex = 241;
            this.label67.Text = "Remarks";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(23, 98);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 13);
            this.label29.TabIndex = 27;
            this.label29.Text = "Cancel";
            // 
            // LottoOnlineAdjustCanceltxt
            // 
            this.LottoOnlineAdjustCanceltxt.Location = new System.Drawing.Point(131, 98);
            this.LottoOnlineAdjustCanceltxt.Name = "LottoOnlineAdjustCanceltxt";
            this.LottoOnlineAdjustCanceltxt.Size = new System.Drawing.Size(125, 20);
            this.LottoOnlineAdjustCanceltxt.TabIndex = 28;
            this.LottoOnlineAdjustCanceltxt.Text = "0";
            this.LottoOnlineAdjustCanceltxt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(23, 125);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(38, 13);
            this.label30.TabIndex = 29;
            this.label30.Text = "EOS-1";
            // 
            // LottoOnlineEOS1Txt
            // 
            this.LottoOnlineEOS1Txt.Location = new System.Drawing.Point(131, 125);
            this.LottoOnlineEOS1Txt.Name = "LottoOnlineEOS1Txt";
            this.LottoOnlineEOS1Txt.Size = new System.Drawing.Size(125, 20);
            this.LottoOnlineEOS1Txt.TabIndex = 30;
            this.LottoOnlineEOS1Txt.Text = "0";
            this.LottoOnlineEOS1Txt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // LottoOnlineShiftsDiffTxt
            // 
            this.LottoOnlineShiftsDiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoOnlineShiftsDiffTxt.Location = new System.Drawing.Point(131, 222);
            this.LottoOnlineShiftsDiffTxt.Name = "LottoOnlineShiftsDiffTxt";
            this.LottoOnlineShiftsDiffTxt.ReadOnly = true;
            this.LottoOnlineShiftsDiffTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoOnlineShiftsDiffTxt.TabIndex = 38;
            this.LottoOnlineShiftsDiffTxt.Text = "0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(23, 149);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 31;
            this.label31.Text = "EOS-2";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(23, 222);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 13);
            this.label34.TabIndex = 37;
            this.label34.Text = "Total Diff.";
            // 
            // LottoOnlineEOS2Txt
            // 
            this.LottoOnlineEOS2Txt.Location = new System.Drawing.Point(131, 149);
            this.LottoOnlineEOS2Txt.Name = "LottoOnlineEOS2Txt";
            this.LottoOnlineEOS2Txt.Size = new System.Drawing.Size(125, 20);
            this.LottoOnlineEOS2Txt.TabIndex = 32;
            this.LottoOnlineEOS2Txt.Text = "0";
            this.LottoOnlineEOS2Txt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // LottoOnlineTSoldTxt
            // 
            this.LottoOnlineTSoldTxt.Location = new System.Drawing.Point(131, 200);
            this.LottoOnlineTSoldTxt.Name = "LottoOnlineTSoldTxt";
            this.LottoOnlineTSoldTxt.ReadOnly = true;
            this.LottoOnlineTSoldTxt.Size = new System.Drawing.Size(125, 20);
            this.LottoOnlineTSoldTxt.TabIndex = 36;
            this.LottoOnlineTSoldTxt.Text = "0";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(23, 173);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(38, 13);
            this.label32.TabIndex = 33;
            this.label32.Text = "EOS-3";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(23, 200);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(55, 13);
            this.label33.TabIndex = 35;
            this.label33.Text = "Total Sold";
            // 
            // LottoOnlineEOS3Txt
            // 
            this.LottoOnlineEOS3Txt.Location = new System.Drawing.Point(131, 173);
            this.LottoOnlineEOS3Txt.Name = "LottoOnlineEOS3Txt";
            this.LottoOnlineEOS3Txt.Size = new System.Drawing.Size(125, 20);
            this.LottoOnlineEOS3Txt.TabIndex = 34;
            this.LottoOnlineEOS3Txt.Text = "0";
            this.LottoOnlineEOS3Txt.Leave += new System.EventHandler(this.LottoOnlineReportTxt_Leave);
            // 
            // ScratchWingroup
            // 
            this.ScratchWingroup.Controls.Add(this.TotalActivatedInvTxt);
            this.ScratchWingroup.Controls.Add(this.label240);
            this.ScratchWingroup.Controls.Add(this.LottoScrachWinTotalSubtotalTxt);
            this.ScratchWingroup.Controls.Add(this.label183);
            this.ScratchWingroup.Controls.Add(this.LottoScratchRemarksTxt);
            this.ScratchWingroup.Controls.Add(this.label182);
            this.ScratchWingroup.Controls.Add(this.ScratchWinNormalDgv);
            this.ScratchWingroup.Controls.Add(this.label176);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalDiffTxt);
            this.ScratchWingroup.Controls.Add(this.label181);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalEODSoldDTxt);
            this.ScratchWingroup.Controls.Add(this.label180);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalEODSoldTxt);
            this.ScratchWingroup.Controls.Add(this.label179);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalSoldTxt);
            this.ScratchWingroup.Controls.Add(this.label178);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalActualCLoseTxt);
            this.ScratchWingroup.Controls.Add(this.label177);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalAdditionalQtyTxt);
            this.ScratchWingroup.Controls.Add(this.label175);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalOpeningDiffTxt);
            this.ScratchWingroup.Controls.Add(this.label174);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalActualOpenTxt);
            this.ScratchWingroup.Controls.Add(this.label173);
            this.ScratchWingroup.Controls.Add(this.LottoScratchWinTotalOpenTxt);
            this.ScratchWingroup.Controls.Add(this.label172);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal30Txt);
            this.ScratchWingroup.Controls.Add(this.LottoScratch30DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label162);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold30Txt);
            this.ScratchWingroup.Controls.Add(this.label163);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold30Txt);
            this.ScratchWingroup.Controls.Add(this.label164);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold30Txt);
            this.ScratchWingroup.Controls.Add(this.label165);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose30Txt);
            this.ScratchWingroup.Controls.Add(this.label166);
            this.ScratchWingroup.Controls.Add(this.label167);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty30Txt);
            this.ScratchWingroup.Controls.Add(this.label168);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff30Txt);
            this.ScratchWingroup.Controls.Add(this.label169);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen30txt);
            this.ScratchWingroup.Controls.Add(this.label170);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen30Txt);
            this.ScratchWingroup.Controls.Add(this.label171);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal20Txt);
            this.ScratchWingroup.Controls.Add(this.LottoScratch20DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label152);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold20Txt);
            this.ScratchWingroup.Controls.Add(this.label153);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold20Txt);
            this.ScratchWingroup.Controls.Add(this.label154);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold20Txt);
            this.ScratchWingroup.Controls.Add(this.label155);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose20Txt);
            this.ScratchWingroup.Controls.Add(this.label156);
            this.ScratchWingroup.Controls.Add(this.label157);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty20Txt);
            this.ScratchWingroup.Controls.Add(this.label158);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff20Txt);
            this.ScratchWingroup.Controls.Add(this.label159);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen20txt);
            this.ScratchWingroup.Controls.Add(this.label160);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen20Txt);
            this.ScratchWingroup.Controls.Add(this.label161);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal10Txt);
            this.ScratchWingroup.Controls.Add(this.LottoScratch10DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label142);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold10Txt);
            this.ScratchWingroup.Controls.Add(this.label143);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold10Txt);
            this.ScratchWingroup.Controls.Add(this.label144);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold10Txt);
            this.ScratchWingroup.Controls.Add(this.label145);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose10Txt);
            this.ScratchWingroup.Controls.Add(this.label146);
            this.ScratchWingroup.Controls.Add(this.label147);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty10Txt);
            this.ScratchWingroup.Controls.Add(this.label148);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff10Txt);
            this.ScratchWingroup.Controls.Add(this.label149);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen10txt);
            this.ScratchWingroup.Controls.Add(this.label150);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen10Txt);
            this.ScratchWingroup.Controls.Add(this.label151);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal7Txt);
            this.ScratchWingroup.Controls.Add(this.LottoScratch7DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label132);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold7Txt);
            this.ScratchWingroup.Controls.Add(this.label133);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold7Txt);
            this.ScratchWingroup.Controls.Add(this.label134);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold7Txt);
            this.ScratchWingroup.Controls.Add(this.label135);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose7Txt);
            this.ScratchWingroup.Controls.Add(this.label136);
            this.ScratchWingroup.Controls.Add(this.label137);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty7Txt);
            this.ScratchWingroup.Controls.Add(this.label138);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff7Txt);
            this.ScratchWingroup.Controls.Add(this.label139);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen7txt);
            this.ScratchWingroup.Controls.Add(this.label140);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen7Txt);
            this.ScratchWingroup.Controls.Add(this.label141);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal5Txt);
            this.ScratchWingroup.Controls.Add(this.LottoScratch5DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label122);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold5Txt);
            this.ScratchWingroup.Controls.Add(this.label123);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold5Txt);
            this.ScratchWingroup.Controls.Add(this.label124);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold5Txt);
            this.ScratchWingroup.Controls.Add(this.label125);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose5Txt);
            this.ScratchWingroup.Controls.Add(this.label126);
            this.ScratchWingroup.Controls.Add(this.label127);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty5Txt);
            this.ScratchWingroup.Controls.Add(this.label128);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff5Txt);
            this.ScratchWingroup.Controls.Add(this.label129);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen5txt);
            this.ScratchWingroup.Controls.Add(this.label130);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen5Txt);
            this.ScratchWingroup.Controls.Add(this.label131);
            this.ScratchWingroup.Controls.Add(this.LottoScratch4DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label112);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold4Txt);
            this.ScratchWingroup.Controls.Add(this.label113);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold4Txt);
            this.ScratchWingroup.Controls.Add(this.label114);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold4Txt);
            this.ScratchWingroup.Controls.Add(this.label115);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose4Txt);
            this.ScratchWingroup.Controls.Add(this.label116);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal4Txt);
            this.ScratchWingroup.Controls.Add(this.label117);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty4Txt);
            this.ScratchWingroup.Controls.Add(this.label118);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff4Txt);
            this.ScratchWingroup.Controls.Add(this.label119);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen4txt);
            this.ScratchWingroup.Controls.Add(this.label120);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen4Txt);
            this.ScratchWingroup.Controls.Add(this.label121);
            this.ScratchWingroup.Controls.Add(this.LottoScratch3DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label102);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold3Txt);
            this.ScratchWingroup.Controls.Add(this.label103);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold3Txt);
            this.ScratchWingroup.Controls.Add(this.label104);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold3Txt);
            this.ScratchWingroup.Controls.Add(this.label105);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose3Txt);
            this.ScratchWingroup.Controls.Add(this.label106);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal3Txt);
            this.ScratchWingroup.Controls.Add(this.label107);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty3Txt);
            this.ScratchWingroup.Controls.Add(this.label108);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff3Txt);
            this.ScratchWingroup.Controls.Add(this.label109);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen3txt);
            this.ScratchWingroup.Controls.Add(this.label110);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen3Txt);
            this.ScratchWingroup.Controls.Add(this.label111);
            this.ScratchWingroup.Controls.Add(this.LottoScratch2DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label66);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold2Txt);
            this.ScratchWingroup.Controls.Add(this.label71);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold2Txt);
            this.ScratchWingroup.Controls.Add(this.label91);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold2Txt);
            this.ScratchWingroup.Controls.Add(this.label95);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose2Txt);
            this.ScratchWingroup.Controls.Add(this.label96);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal2Txt);
            this.ScratchWingroup.Controls.Add(this.label97);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty2Txt);
            this.ScratchWingroup.Controls.Add(this.label98);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff2Txt);
            this.ScratchWingroup.Controls.Add(this.label99);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen2txt);
            this.ScratchWingroup.Controls.Add(this.label100);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen2Txt);
            this.ScratchWingroup.Controls.Add(this.label101);
            this.ScratchWingroup.Controls.Add(this.LottoScratch1DiffTxt);
            this.ScratchWingroup.Controls.Add(this.label25);
            this.ScratchWingroup.Controls.Add(this.LottoReportSold1Txt);
            this.ScratchWingroup.Controls.Add(this.label24);
            this.ScratchWingroup.Controls.Add(this.LottoScratchReportSold1Txt);
            this.ScratchWingroup.Controls.Add(this.label23);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSold1Txt);
            this.ScratchWingroup.Controls.Add(this.label22);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualClose1Txt);
            this.ScratchWingroup.Controls.Add(this.label21);
            this.ScratchWingroup.Controls.Add(this.LottoScratchSubtotal1Txt);
            this.ScratchWingroup.Controls.Add(this.label20);
            this.ScratchWingroup.Controls.Add(this.LottoScratchAddQty1Txt);
            this.ScratchWingroup.Controls.Add(this.label19);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpeningDiff1Txt);
            this.ScratchWingroup.Controls.Add(this.label18);
            this.ScratchWingroup.Controls.Add(this.LottoScratchActualOpen1txt);
            this.ScratchWingroup.Controls.Add(this.label17);
            this.ScratchWingroup.Controls.Add(this.LottoScratchOpen1Txt);
            this.ScratchWingroup.Controls.Add(this.label16);
            this.ScratchWingroup.Location = new System.Drawing.Point(12, 867);
            this.ScratchWingroup.Name = "ScratchWingroup";
            this.ScratchWingroup.Size = new System.Drawing.Size(1088, 719);
            this.ScratchWingroup.TabIndex = 244;
            this.ScratchWingroup.TabStop = false;
            this.ScratchWingroup.Text = "Lotto Scratch and Win ";
            // 
            // TotalActivatedInvTxt
            // 
            this.TotalActivatedInvTxt.Location = new System.Drawing.Point(549, 487);
            this.TotalActivatedInvTxt.Name = "TotalActivatedInvTxt";
            this.TotalActivatedInvTxt.ReadOnly = true;
            this.TotalActivatedInvTxt.Size = new System.Drawing.Size(47, 20);
            this.TotalActivatedInvTxt.TabIndex = 256;
            this.TotalActivatedInvTxt.Text = "0";
            // 
            // label240
            // 
            this.label240.AutoSize = true;
            this.label240.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label240.Location = new System.Drawing.Point(366, 491);
            this.label240.Name = "label240";
            this.label240.Size = new System.Drawing.Size(172, 12);
            this.label240.TabIndex = 255;
            this.label240.Text = "Total Activated Inv (Front & Back)";
            // 
            // LottoScrachWinTotalSubtotalTxt
            // 
            this.LottoScrachWinTotalSubtotalTxt.Location = new System.Drawing.Point(131, 595);
            this.LottoScrachWinTotalSubtotalTxt.Name = "LottoScrachWinTotalSubtotalTxt";
            this.LottoScrachWinTotalSubtotalTxt.ReadOnly = true;
            this.LottoScrachWinTotalSubtotalTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScrachWinTotalSubtotalTxt.TabIndex = 254;
            this.LottoScrachWinTotalSubtotalTxt.Text = "0";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label183.Location = new System.Drawing.Point(16, 599);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(75, 12);
            this.label183.TabIndex = 253;
            this.label183.Text = "Total Subtotal";
            // 
            // LottoScratchRemarksTxt
            // 
            this.LottoScratchRemarksTxt.Location = new System.Drawing.Point(824, 649);
            this.LottoScratchRemarksTxt.Name = "LottoScratchRemarksTxt";
            this.LottoScratchRemarksTxt.Size = new System.Drawing.Size(222, 56);
            this.LottoScratchRemarksTxt.TabIndex = 252;
            this.LottoScratchRemarksTxt.Text = "";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(717, 649);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(49, 13);
            this.label182.TabIndex = 251;
            this.label182.Text = "Remarks";
            // 
            // ScratchWinNormalDgv
            // 
            this.ScratchWinNormalDgv.AllowDrop = true;
            this.ScratchWinNormalDgv.AllowUserToAddRows = false;
            this.ScratchWinNormalDgv.AllowUserToDeleteRows = false;
            this.ScratchWinNormalDgv.AllowUserToResizeColumns = false;
            this.ScratchWinNormalDgv.AllowUserToResizeRows = false;
            this.ScratchWinNormalDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ScratchWinNormalDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ScratchWinNormalDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.ScratchWinNormalDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ScratchWinNormalDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn3});
            this.ScratchWinNormalDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ScratchWinNormalDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.ScratchWinNormalDgv.Location = new System.Drawing.Point(824, 538);
            this.ScratchWinNormalDgv.MultiSelect = false;
            this.ScratchWinNormalDgv.Name = "ScratchWinNormalDgv";
            this.ScratchWinNormalDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ScratchWinNormalDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.ScratchWinNormalDgv.RowHeadersVisible = false;
            this.ScratchWinNormalDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ScratchWinNormalDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ScratchWinNormalDgv.Size = new System.Drawing.Size(222, 105);
            this.ScratchWinNormalDgv.TabIndex = 249;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 5;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label176.ForeColor = System.Drawing.Color.Black;
            this.label176.Location = new System.Drawing.Point(821, 502);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(104, 15);
            this.label176.TabIndex = 250;
            this.label176.Text = "Scratch Win Docs";
            // 
            // LottoScratchWinTotalDiffTxt
            // 
            this.LottoScratchWinTotalDiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchWinTotalDiffTxt.Location = new System.Drawing.Point(293, 522);
            this.LottoScratchWinTotalDiffTxt.Name = "LottoScratchWinTotalDiffTxt";
            this.LottoScratchWinTotalDiffTxt.ReadOnly = true;
            this.LottoScratchWinTotalDiffTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalDiffTxt.TabIndex = 202;
            this.LottoScratchWinTotalDiffTxt.Text = "0";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label181.Location = new System.Drawing.Point(194, 522);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(52, 12);
            this.label181.TabIndex = 201;
            this.label181.Text = "Total Diff";
            // 
            // LottoScratchWinTotalEODSoldDTxt
            // 
            this.LottoScratchWinTotalEODSoldDTxt.Location = new System.Drawing.Point(293, 487);
            this.LottoScratchWinTotalEODSoldDTxt.Name = "LottoScratchWinTotalEODSoldDTxt";
            this.LottoScratchWinTotalEODSoldDTxt.ReadOnly = true;
            this.LottoScratchWinTotalEODSoldDTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalEODSoldDTxt.TabIndex = 200;
            this.LottoScratchWinTotalEODSoldDTxt.Text = "0";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label180.Location = new System.Drawing.Point(194, 491);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(90, 12);
            this.label180.TabIndex = 198;
            this.label180.Text = "Total EOD Sold $";
            // 
            // LottoScratchWinTotalEODSoldTxt
            // 
            this.LottoScratchWinTotalEODSoldTxt.Location = new System.Drawing.Point(131, 673);
            this.LottoScratchWinTotalEODSoldTxt.Name = "LottoScratchWinTotalEODSoldTxt";
            this.LottoScratchWinTotalEODSoldTxt.ReadOnly = true;
            this.LottoScratchWinTotalEODSoldTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalEODSoldTxt.TabIndex = 197;
            this.LottoScratchWinTotalEODSoldTxt.Text = "0";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label179.Location = new System.Drawing.Point(17, 677);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(81, 12);
            this.label179.TabIndex = 196;
            this.label179.Text = "Total EOD Sold";
            // 
            // LottoScratchWinTotalSoldTxt
            // 
            this.LottoScratchWinTotalSoldTxt.Location = new System.Drawing.Point(131, 647);
            this.LottoScratchWinTotalSoldTxt.Name = "LottoScratchWinTotalSoldTxt";
            this.LottoScratchWinTotalSoldTxt.ReadOnly = true;
            this.LottoScratchWinTotalSoldTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalSoldTxt.TabIndex = 195;
            this.LottoScratchWinTotalSoldTxt.Text = "0";
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label178.Location = new System.Drawing.Point(17, 651);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(55, 12);
            this.label178.TabIndex = 194;
            this.label178.Text = "Total Sold";
            // 
            // LottoScratchWinTotalActualCLoseTxt
            // 
            this.LottoScratchWinTotalActualCLoseTxt.Location = new System.Drawing.Point(131, 621);
            this.LottoScratchWinTotalActualCLoseTxt.Name = "LottoScratchWinTotalActualCLoseTxt";
            this.LottoScratchWinTotalActualCLoseTxt.ReadOnly = true;
            this.LottoScratchWinTotalActualCLoseTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalActualCLoseTxt.TabIndex = 193;
            this.LottoScratchWinTotalActualCLoseTxt.Text = "0";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label177.Location = new System.Drawing.Point(17, 625);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(98, 12);
            this.label177.TabIndex = 192;
            this.label177.Text = "Total Actual Close";
            // 
            // LottoScratchWinTotalAdditionalQtyTxt
            // 
            this.LottoScratchWinTotalAdditionalQtyTxt.Location = new System.Drawing.Point(131, 569);
            this.LottoScratchWinTotalAdditionalQtyTxt.Name = "LottoScratchWinTotalAdditionalQtyTxt";
            this.LottoScratchWinTotalAdditionalQtyTxt.ReadOnly = true;
            this.LottoScratchWinTotalAdditionalQtyTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalAdditionalQtyTxt.TabIndex = 189;
            this.LottoScratchWinTotalAdditionalQtyTxt.Text = "0";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label175.Location = new System.Drawing.Point(16, 573);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(105, 12);
            this.label175.TabIndex = 188;
            this.label175.Text = "Total Additional Qty";
            // 
            // LottoScratchWinTotalOpeningDiffTxt
            // 
            this.LottoScratchWinTotalOpeningDiffTxt.Location = new System.Drawing.Point(131, 543);
            this.LottoScratchWinTotalOpeningDiffTxt.Name = "LottoScratchWinTotalOpeningDiffTxt";
            this.LottoScratchWinTotalOpeningDiffTxt.ReadOnly = true;
            this.LottoScratchWinTotalOpeningDiffTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalOpeningDiffTxt.TabIndex = 187;
            this.LottoScratchWinTotalOpeningDiffTxt.Text = "0";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label174.Location = new System.Drawing.Point(17, 547);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(96, 12);
            this.label174.TabIndex = 186;
            this.label174.Text = "Total Opening Diff";
            // 
            // LottoScratchWinTotalActualOpenTxt
            // 
            this.LottoScratchWinTotalActualOpenTxt.Location = new System.Drawing.Point(131, 518);
            this.LottoScratchWinTotalActualOpenTxt.Name = "LottoScratchWinTotalActualOpenTxt";
            this.LottoScratchWinTotalActualOpenTxt.ReadOnly = true;
            this.LottoScratchWinTotalActualOpenTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalActualOpenTxt.TabIndex = 185;
            this.LottoScratchWinTotalActualOpenTxt.Text = "0";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label173.Location = new System.Drawing.Point(19, 523);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(95, 12);
            this.label173.TabIndex = 184;
            this.label173.Text = "Total Actual Open";
            // 
            // LottoScratchWinTotalOpenTxt
            // 
            this.LottoScratchWinTotalOpenTxt.Location = new System.Drawing.Point(131, 487);
            this.LottoScratchWinTotalOpenTxt.Name = "LottoScratchWinTotalOpenTxt";
            this.LottoScratchWinTotalOpenTxt.ReadOnly = true;
            this.LottoScratchWinTotalOpenTxt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchWinTotalOpenTxt.TabIndex = 183;
            this.LottoScratchWinTotalOpenTxt.Text = "0";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label172.Location = new System.Drawing.Point(21, 491);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(59, 12);
            this.label172.TabIndex = 182;
            this.label172.Text = "Total Open";
            // 
            // LottoScratchSubtotal30Txt
            // 
            this.LottoScratchSubtotal30Txt.Location = new System.Drawing.Point(611, 437);
            this.LottoScratchSubtotal30Txt.Name = "LottoScratchSubtotal30Txt";
            this.LottoScratchSubtotal30Txt.ReadOnly = true;
            this.LottoScratchSubtotal30Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal30Txt.TabIndex = 181;
            this.LottoScratchSubtotal30Txt.Text = "0";
            // 
            // LottoScratch30DiffTxt
            // 
            this.LottoScratch30DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch30DiffTxt.Location = new System.Drawing.Point(874, 467);
            this.LottoScratch30DiffTxt.Name = "LottoScratch30DiffTxt";
            this.LottoScratch30DiffTxt.ReadOnly = true;
            this.LottoScratch30DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch30DiffTxt.TabIndex = 180;
            this.LottoScratch30DiffTxt.Text = "0";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label162.Location = new System.Drawing.Point(805, 467);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(66, 12);
            this.label162.TabIndex = 179;
            this.label162.Text = "Diff Qty 30$";
            // 
            // LottoReportSold30Txt
            // 
            this.LottoReportSold30Txt.Location = new System.Drawing.Point(743, 468);
            this.LottoReportSold30Txt.Name = "LottoReportSold30Txt";
            this.LottoReportSold30Txt.ReadOnly = true;
            this.LottoReportSold30Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoReportSold30Txt.TabIndex = 178;
            this.LottoReportSold30Txt.Text = "0";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(911, 444);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(94, 13);
            this.label163.TabIndex = 177;
            this.label163.Text = "EOD Sold Qty 30$";
            // 
            // LottoScratchReportSold30Txt
            // 
            this.LottoScratchReportSold30Txt.Location = new System.Drawing.Point(1011, 440);
            this.LottoScratchReportSold30Txt.Name = "LottoScratchReportSold30Txt";
            this.LottoScratchReportSold30Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold30Txt.TabIndex = 176;
            this.LottoScratchReportSold30Txt.Text = "0";
            this.LottoScratchReportSold30Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen30txt_Leave);
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label164.Location = new System.Drawing.Point(659, 470);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(80, 12);
            this.label164.TabIndex = 175;
            this.label164.Text = "EOD Sold$ 30$";
            // 
            // LottoScratchSold30Txt
            // 
            this.LottoScratchSold30Txt.Location = new System.Drawing.Point(874, 441);
            this.LottoScratchSold30Txt.Name = "LottoScratchSold30Txt";
            this.LottoScratchSold30Txt.ReadOnly = true;
            this.LottoScratchSold30Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold30Txt.TabIndex = 174;
            this.LottoScratchSold30Txt.Text = "0";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(806, 444);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(49, 13);
            this.label165.TabIndex = 173;
            this.label165.Text = "Sold 30$";
            // 
            // LottoScratchActualClose30Txt
            // 
            this.LottoScratchActualClose30Txt.Location = new System.Drawing.Point(744, 440);
            this.LottoScratchActualClose30Txt.Name = "LottoScratchActualClose30Txt";
            this.LottoScratchActualClose30Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose30Txt.TabIndex = 172;
            this.LottoScratchActualClose30Txt.Text = "0";
            this.LottoScratchActualClose30Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen30txt_Leave);
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(660, 444);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(87, 13);
            this.label166.TabIndex = 171;
            this.label166.Text = "Actual Close 30$";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(537, 443);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(74, 13);
            this.label167.TabIndex = 170;
            this.label167.Text = "Sub Total 30$";
            // 
            // LottoScratchAddQty30Txt
            // 
            this.LottoScratchAddQty30Txt.Location = new System.Drawing.Point(488, 440);
            this.LottoScratchAddQty30Txt.Name = "LottoScratchAddQty30Txt";
            this.LottoScratchAddQty30Txt.ReadOnly = true;
            this.LottoScratchAddQty30Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty30Txt.TabIndex = 169;
            this.LottoScratchAddQty30Txt.Text = "0";
            this.LottoScratchAddQty30Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen30txt_Leave);
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(422, 443);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(66, 13);
            this.label168.TabIndex = 168;
            this.label168.Text = "Add-Qty 30$";
            // 
            // LottoScratchOpeningDiff30Txt
            // 
            this.LottoScratchOpeningDiff30Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff30Txt.Location = new System.Drawing.Point(372, 441);
            this.LottoScratchOpeningDiff30Txt.Name = "LottoScratchOpeningDiff30Txt";
            this.LottoScratchOpeningDiff30Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff30Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff30Txt.TabIndex = 167;
            this.LottoScratchOpeningDiff30Txt.Text = "0";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(284, 444);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(90, 13);
            this.label169.TabIndex = 166;
            this.label169.Text = "Opening Diff. 30$";
            // 
            // LottoScratchActualOpen30txt
            // 
            this.LottoScratchActualOpen30txt.Location = new System.Drawing.Point(222, 440);
            this.LottoScratchActualOpen30txt.Name = "LottoScratchActualOpen30txt";
            this.LottoScratchActualOpen30txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen30txt.TabIndex = 165;
            this.LottoScratchActualOpen30txt.Text = "0";
            this.LottoScratchActualOpen30txt.Leave += new System.EventHandler(this.LottoScratchActualOpen30txt_Leave);
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(139, 444);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(87, 13);
            this.label170.TabIndex = 164;
            this.label170.Text = "Actual Open 30$";
            // 
            // LottoScratchOpen30Txt
            // 
            this.LottoScratchOpen30Txt.Location = new System.Drawing.Point(81, 440);
            this.LottoScratchOpen30Txt.Name = "LottoScratchOpen30Txt";
            this.LottoScratchOpen30Txt.ReadOnly = true;
            this.LottoScratchOpen30Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen30Txt.TabIndex = 163;
            this.LottoScratchOpen30Txt.Text = "0";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(23, 444);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(54, 13);
            this.label171.TabIndex = 162;
            this.label171.Text = "Open 30$";
            // 
            // LottoScratchSubtotal20Txt
            // 
            this.LottoScratchSubtotal20Txt.Location = new System.Drawing.Point(611, 387);
            this.LottoScratchSubtotal20Txt.Name = "LottoScratchSubtotal20Txt";
            this.LottoScratchSubtotal20Txt.ReadOnly = true;
            this.LottoScratchSubtotal20Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal20Txt.TabIndex = 161;
            this.LottoScratchSubtotal20Txt.Text = "0";
            // 
            // LottoScratch20DiffTxt
            // 
            this.LottoScratch20DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch20DiffTxt.Location = new System.Drawing.Point(874, 416);
            this.LottoScratch20DiffTxt.Name = "LottoScratch20DiffTxt";
            this.LottoScratch20DiffTxt.ReadOnly = true;
            this.LottoScratch20DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch20DiffTxt.TabIndex = 160;
            this.LottoScratch20DiffTxt.Text = "0";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.Location = new System.Drawing.Point(805, 420);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(66, 12);
            this.label152.TabIndex = 159;
            this.label152.Text = "Diff Qty 20$";
            // 
            // LottoReportSold20Txt
            // 
            this.LottoReportSold20Txt.Location = new System.Drawing.Point(743, 416);
            this.LottoReportSold20Txt.Name = "LottoReportSold20Txt";
            this.LottoReportSold20Txt.ReadOnly = true;
            this.LottoReportSold20Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoReportSold20Txt.TabIndex = 158;
            this.LottoReportSold20Txt.Text = "0";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(911, 394);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(94, 13);
            this.label153.TabIndex = 157;
            this.label153.Text = "EOD Sold Qty 20$";
            // 
            // LottoScratchReportSold20Txt
            // 
            this.LottoScratchReportSold20Txt.Location = new System.Drawing.Point(1011, 390);
            this.LottoScratchReportSold20Txt.Name = "LottoScratchReportSold20Txt";
            this.LottoScratchReportSold20Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold20Txt.TabIndex = 156;
            this.LottoScratchReportSold20Txt.Text = "0";
            this.LottoScratchReportSold20Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen20txt_Leave);
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label154.Location = new System.Drawing.Point(659, 418);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(80, 12);
            this.label154.TabIndex = 155;
            this.label154.Text = "EOD Sold$ 20$";
            // 
            // LottoScratchSold20Txt
            // 
            this.LottoScratchSold20Txt.Location = new System.Drawing.Point(874, 391);
            this.LottoScratchSold20Txt.Name = "LottoScratchSold20Txt";
            this.LottoScratchSold20Txt.ReadOnly = true;
            this.LottoScratchSold20Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold20Txt.TabIndex = 154;
            this.LottoScratchSold20Txt.Text = "0";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(806, 394);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(49, 13);
            this.label155.TabIndex = 153;
            this.label155.Text = "Sold 20$";
            // 
            // LottoScratchActualClose20Txt
            // 
            this.LottoScratchActualClose20Txt.Location = new System.Drawing.Point(744, 390);
            this.LottoScratchActualClose20Txt.Name = "LottoScratchActualClose20Txt";
            this.LottoScratchActualClose20Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose20Txt.TabIndex = 152;
            this.LottoScratchActualClose20Txt.Text = "0";
            this.LottoScratchActualClose20Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen20txt_Leave);
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(660, 394);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(87, 13);
            this.label156.TabIndex = 151;
            this.label156.Text = "Actual Close 20$";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(537, 393);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(74, 13);
            this.label157.TabIndex = 150;
            this.label157.Text = "Sub Total 20$";
            // 
            // LottoScratchAddQty20Txt
            // 
            this.LottoScratchAddQty20Txt.Location = new System.Drawing.Point(488, 387);
            this.LottoScratchAddQty20Txt.Name = "LottoScratchAddQty20Txt";
            this.LottoScratchAddQty20Txt.ReadOnly = true;
            this.LottoScratchAddQty20Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty20Txt.TabIndex = 149;
            this.LottoScratchAddQty20Txt.Text = "0";
            this.LottoScratchAddQty20Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen20txt_Leave);
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(422, 393);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(66, 13);
            this.label158.TabIndex = 148;
            this.label158.Text = "Add-Qty 20$";
            // 
            // LottoScratchOpeningDiff20Txt
            // 
            this.LottoScratchOpeningDiff20Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff20Txt.Location = new System.Drawing.Point(372, 391);
            this.LottoScratchOpeningDiff20Txt.Name = "LottoScratchOpeningDiff20Txt";
            this.LottoScratchOpeningDiff20Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff20Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff20Txt.TabIndex = 147;
            this.LottoScratchOpeningDiff20Txt.Text = "0";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(284, 394);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(90, 13);
            this.label159.TabIndex = 146;
            this.label159.Text = "Opening Diff. 20$";
            // 
            // LottoScratchActualOpen20txt
            // 
            this.LottoScratchActualOpen20txt.Location = new System.Drawing.Point(222, 390);
            this.LottoScratchActualOpen20txt.Name = "LottoScratchActualOpen20txt";
            this.LottoScratchActualOpen20txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen20txt.TabIndex = 145;
            this.LottoScratchActualOpen20txt.Text = "0";
            this.LottoScratchActualOpen20txt.Leave += new System.EventHandler(this.LottoScratchActualOpen20txt_Leave);
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(139, 394);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(87, 13);
            this.label160.TabIndex = 144;
            this.label160.Text = "Actual Open 20$";
            // 
            // LottoScratchOpen20Txt
            // 
            this.LottoScratchOpen20Txt.Location = new System.Drawing.Point(81, 390);
            this.LottoScratchOpen20Txt.Name = "LottoScratchOpen20Txt";
            this.LottoScratchOpen20Txt.ReadOnly = true;
            this.LottoScratchOpen20Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen20Txt.TabIndex = 143;
            this.LottoScratchOpen20Txt.Text = "0";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(23, 394);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(54, 13);
            this.label161.TabIndex = 142;
            this.label161.Text = "Open 20$";
            // 
            // LottoScratchSubtotal10Txt
            // 
            this.LottoScratchSubtotal10Txt.Location = new System.Drawing.Point(611, 337);
            this.LottoScratchSubtotal10Txt.Name = "LottoScratchSubtotal10Txt";
            this.LottoScratchSubtotal10Txt.ReadOnly = true;
            this.LottoScratchSubtotal10Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal10Txt.TabIndex = 141;
            this.LottoScratchSubtotal10Txt.Text = "0";
            // 
            // LottoScratch10DiffTxt
            // 
            this.LottoScratch10DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch10DiffTxt.Location = new System.Drawing.Point(874, 365);
            this.LottoScratch10DiffTxt.Name = "LottoScratch10DiffTxt";
            this.LottoScratch10DiffTxt.ReadOnly = true;
            this.LottoScratch10DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch10DiffTxt.TabIndex = 140;
            this.LottoScratch10DiffTxt.Text = "0";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.Location = new System.Drawing.Point(805, 369);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(66, 12);
            this.label142.TabIndex = 139;
            this.label142.Text = "Diff Qty 10$";
            // 
            // LottoReportSold10Txt
            // 
            this.LottoReportSold10Txt.Location = new System.Drawing.Point(743, 366);
            this.LottoReportSold10Txt.Name = "LottoReportSold10Txt";
            this.LottoReportSold10Txt.ReadOnly = true;
            this.LottoReportSold10Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoReportSold10Txt.TabIndex = 138;
            this.LottoReportSold10Txt.Text = "0";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(911, 344);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(94, 13);
            this.label143.TabIndex = 137;
            this.label143.Text = "EOD Sold Qty 10$";
            // 
            // LottoScratchReportSold10Txt
            // 
            this.LottoScratchReportSold10Txt.Location = new System.Drawing.Point(1011, 337);
            this.LottoScratchReportSold10Txt.Name = "LottoScratchReportSold10Txt";
            this.LottoScratchReportSold10Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold10Txt.TabIndex = 136;
            this.LottoScratchReportSold10Txt.Text = "0";
            this.LottoScratchReportSold10Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen10txt_Leave);
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.Location = new System.Drawing.Point(659, 368);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(80, 12);
            this.label144.TabIndex = 135;
            this.label144.Text = "EOD Sold$ 10$";
            // 
            // LottoScratchSold10Txt
            // 
            this.LottoScratchSold10Txt.Location = new System.Drawing.Point(874, 341);
            this.LottoScratchSold10Txt.Name = "LottoScratchSold10Txt";
            this.LottoScratchSold10Txt.ReadOnly = true;
            this.LottoScratchSold10Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold10Txt.TabIndex = 134;
            this.LottoScratchSold10Txt.Text = "0";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(806, 344);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(49, 13);
            this.label145.TabIndex = 133;
            this.label145.Text = "Sold 10$";
            // 
            // LottoScratchActualClose10Txt
            // 
            this.LottoScratchActualClose10Txt.Location = new System.Drawing.Point(744, 340);
            this.LottoScratchActualClose10Txt.Name = "LottoScratchActualClose10Txt";
            this.LottoScratchActualClose10Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose10Txt.TabIndex = 132;
            this.LottoScratchActualClose10Txt.Text = "0";
            this.LottoScratchActualClose10Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen10txt_Leave);
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(660, 344);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(87, 13);
            this.label146.TabIndex = 131;
            this.label146.Text = "Actual Close 10$";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(537, 343);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(74, 13);
            this.label147.TabIndex = 130;
            this.label147.Text = "Sub Total 10$";
            // 
            // LottoScratchAddQty10Txt
            // 
            this.LottoScratchAddQty10Txt.Location = new System.Drawing.Point(488, 341);
            this.LottoScratchAddQty10Txt.Name = "LottoScratchAddQty10Txt";
            this.LottoScratchAddQty10Txt.ReadOnly = true;
            this.LottoScratchAddQty10Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty10Txt.TabIndex = 129;
            this.LottoScratchAddQty10Txt.Text = "0";
            this.LottoScratchAddQty10Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen10txt_Leave);
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(422, 343);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(66, 13);
            this.label148.TabIndex = 128;
            this.label148.Text = "Add-Qty 10$";
            // 
            // LottoScratchOpeningDiff10Txt
            // 
            this.LottoScratchOpeningDiff10Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff10Txt.Location = new System.Drawing.Point(372, 340);
            this.LottoScratchOpeningDiff10Txt.Name = "LottoScratchOpeningDiff10Txt";
            this.LottoScratchOpeningDiff10Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff10Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff10Txt.TabIndex = 127;
            this.LottoScratchOpeningDiff10Txt.Text = "0";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(284, 344);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(90, 13);
            this.label149.TabIndex = 126;
            this.label149.Text = "Opening Diff. 10$";
            // 
            // LottoScratchActualOpen10txt
            // 
            this.LottoScratchActualOpen10txt.Location = new System.Drawing.Point(222, 340);
            this.LottoScratchActualOpen10txt.Name = "LottoScratchActualOpen10txt";
            this.LottoScratchActualOpen10txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen10txt.TabIndex = 125;
            this.LottoScratchActualOpen10txt.Text = "0";
            this.LottoScratchActualOpen10txt.Leave += new System.EventHandler(this.LottoScratchActualOpen10txt_Leave);
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(139, 344);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(87, 13);
            this.label150.TabIndex = 124;
            this.label150.Text = "Actual Open 10$";
            // 
            // LottoScratchOpen10Txt
            // 
            this.LottoScratchOpen10Txt.Location = new System.Drawing.Point(81, 340);
            this.LottoScratchOpen10Txt.Name = "LottoScratchOpen10Txt";
            this.LottoScratchOpen10Txt.ReadOnly = true;
            this.LottoScratchOpen10Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen10Txt.TabIndex = 123;
            this.LottoScratchOpen10Txt.Text = "0";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(23, 344);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(54, 13);
            this.label151.TabIndex = 122;
            this.label151.Text = "Open 10$";
            // 
            // LottoScratchSubtotal7Txt
            // 
            this.LottoScratchSubtotal7Txt.Location = new System.Drawing.Point(611, 284);
            this.LottoScratchSubtotal7Txt.Name = "LottoScratchSubtotal7Txt";
            this.LottoScratchSubtotal7Txt.ReadOnly = true;
            this.LottoScratchSubtotal7Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal7Txt.TabIndex = 121;
            this.LottoScratchSubtotal7Txt.Text = "0";
            // 
            // LottoScratch7DiffTxt
            // 
            this.LottoScratch7DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch7DiffTxt.Location = new System.Drawing.Point(874, 315);
            this.LottoScratch7DiffTxt.Name = "LottoScratch7DiffTxt";
            this.LottoScratch7DiffTxt.ReadOnly = true;
            this.LottoScratch7DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch7DiffTxt.TabIndex = 120;
            this.LottoScratch7DiffTxt.Text = "0";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(805, 319);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(60, 12);
            this.label132.TabIndex = 119;
            this.label132.Text = "Diff Qty 7$";
            // 
            // LottoReportSold7Txt
            // 
            this.LottoReportSold7Txt.Location = new System.Drawing.Point(744, 315);
            this.LottoReportSold7Txt.Name = "LottoReportSold7Txt";
            this.LottoReportSold7Txt.ReadOnly = true;
            this.LottoReportSold7Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoReportSold7Txt.TabIndex = 118;
            this.LottoReportSold7Txt.Text = "0";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(911, 291);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(88, 13);
            this.label133.TabIndex = 117;
            this.label133.Text = "EOD Sold Qty 7$";
            // 
            // LottoScratchReportSold7Txt
            // 
            this.LottoScratchReportSold7Txt.Location = new System.Drawing.Point(1011, 284);
            this.LottoScratchReportSold7Txt.Name = "LottoScratchReportSold7Txt";
            this.LottoScratchReportSold7Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold7Txt.TabIndex = 116;
            this.LottoScratchReportSold7Txt.Text = "0";
            this.LottoScratchReportSold7Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen7txt_Leave);
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(660, 317);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(74, 12);
            this.label134.TabIndex = 115;
            this.label134.Text = "EOD Sold$ 7$";
            // 
            // LottoScratchSold7Txt
            // 
            this.LottoScratchSold7Txt.Location = new System.Drawing.Point(874, 288);
            this.LottoScratchSold7Txt.Name = "LottoScratchSold7Txt";
            this.LottoScratchSold7Txt.ReadOnly = true;
            this.LottoScratchSold7Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold7Txt.TabIndex = 114;
            this.LottoScratchSold7Txt.Text = "0";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(806, 291);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(43, 13);
            this.label135.TabIndex = 113;
            this.label135.Text = "Sold 7$";
            // 
            // LottoScratchActualClose7Txt
            // 
            this.LottoScratchActualClose7Txt.Location = new System.Drawing.Point(744, 287);
            this.LottoScratchActualClose7Txt.Name = "LottoScratchActualClose7Txt";
            this.LottoScratchActualClose7Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose7Txt.TabIndex = 112;
            this.LottoScratchActualClose7Txt.Text = "0";
            this.LottoScratchActualClose7Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen7txt_Leave);
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(660, 291);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(81, 13);
            this.label136.TabIndex = 111;
            this.label136.Text = "Actual Close 7$";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(537, 290);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(68, 13);
            this.label137.TabIndex = 110;
            this.label137.Text = "Sub Total 7$";
            // 
            // LottoScratchAddQty7Txt
            // 
            this.LottoScratchAddQty7Txt.Location = new System.Drawing.Point(488, 287);
            this.LottoScratchAddQty7Txt.Name = "LottoScratchAddQty7Txt";
            this.LottoScratchAddQty7Txt.ReadOnly = true;
            this.LottoScratchAddQty7Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty7Txt.TabIndex = 109;
            this.LottoScratchAddQty7Txt.Text = "0";
            this.LottoScratchAddQty7Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen7txt_Leave);
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(422, 290);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(60, 13);
            this.label138.TabIndex = 108;
            this.label138.Text = "Add-Qty 7$";
            // 
            // LottoScratchOpeningDiff7Txt
            // 
            this.LottoScratchOpeningDiff7Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff7Txt.Location = new System.Drawing.Point(372, 287);
            this.LottoScratchOpeningDiff7Txt.Name = "LottoScratchOpeningDiff7Txt";
            this.LottoScratchOpeningDiff7Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff7Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff7Txt.TabIndex = 107;
            this.LottoScratchOpeningDiff7Txt.Text = "0";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(284, 291);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(84, 13);
            this.label139.TabIndex = 106;
            this.label139.Text = "Opening Diff. 7$";
            // 
            // LottoScratchActualOpen7txt
            // 
            this.LottoScratchActualOpen7txt.Location = new System.Drawing.Point(222, 287);
            this.LottoScratchActualOpen7txt.Name = "LottoScratchActualOpen7txt";
            this.LottoScratchActualOpen7txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen7txt.TabIndex = 105;
            this.LottoScratchActualOpen7txt.Text = "0";
            this.LottoScratchActualOpen7txt.Leave += new System.EventHandler(this.LottoScratchActualOpen7txt_Leave);
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(139, 291);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(81, 13);
            this.label140.TabIndex = 104;
            this.label140.Text = "Actual Open 7$";
            // 
            // LottoScratchOpen7Txt
            // 
            this.LottoScratchOpen7Txt.Location = new System.Drawing.Point(81, 287);
            this.LottoScratchOpen7Txt.Name = "LottoScratchOpen7Txt";
            this.LottoScratchOpen7Txt.ReadOnly = true;
            this.LottoScratchOpen7Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen7Txt.TabIndex = 103;
            this.LottoScratchOpen7Txt.Text = "0";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(23, 291);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(48, 13);
            this.label141.TabIndex = 102;
            this.label141.Text = "Open 7$";
            // 
            // LottoScratchSubtotal5Txt
            // 
            this.LottoScratchSubtotal5Txt.Location = new System.Drawing.Point(611, 233);
            this.LottoScratchSubtotal5Txt.Name = "LottoScratchSubtotal5Txt";
            this.LottoScratchSubtotal5Txt.ReadOnly = true;
            this.LottoScratchSubtotal5Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal5Txt.TabIndex = 101;
            this.LottoScratchSubtotal5Txt.Text = "0";
            // 
            // LottoScratch5DiffTxt
            // 
            this.LottoScratch5DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch5DiffTxt.Location = new System.Drawing.Point(874, 262);
            this.LottoScratch5DiffTxt.Name = "LottoScratch5DiffTxt";
            this.LottoScratch5DiffTxt.ReadOnly = true;
            this.LottoScratch5DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch5DiffTxt.TabIndex = 100;
            this.LottoScratch5DiffTxt.Text = "0";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.Location = new System.Drawing.Point(805, 263);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(60, 12);
            this.label122.TabIndex = 99;
            this.label122.Text = "Diff Qty 5$";
            // 
            // LottoReportSold5Txt
            // 
            this.LottoReportSold5Txt.Location = new System.Drawing.Point(744, 263);
            this.LottoReportSold5Txt.Name = "LottoReportSold5Txt";
            this.LottoReportSold5Txt.ReadOnly = true;
            this.LottoReportSold5Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoReportSold5Txt.TabIndex = 98;
            this.LottoReportSold5Txt.Text = "0";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(911, 240);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(88, 13);
            this.label123.TabIndex = 97;
            this.label123.Text = "EOD Sold Qty 5$";
            // 
            // LottoScratchReportSold5Txt
            // 
            this.LottoScratchReportSold5Txt.Location = new System.Drawing.Point(1011, 233);
            this.LottoScratchReportSold5Txt.Name = "LottoScratchReportSold5Txt";
            this.LottoScratchReportSold5Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold5Txt.TabIndex = 96;
            this.LottoScratchReportSold5Txt.Text = "0";
            this.LottoScratchReportSold5Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen5txt_Leave);
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.Location = new System.Drawing.Point(660, 265);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(74, 12);
            this.label124.TabIndex = 95;
            this.label124.Text = "EOD Sold$ 5$";
            // 
            // LottoScratchSold5Txt
            // 
            this.LottoScratchSold5Txt.Location = new System.Drawing.Point(874, 237);
            this.LottoScratchSold5Txt.Name = "LottoScratchSold5Txt";
            this.LottoScratchSold5Txt.ReadOnly = true;
            this.LottoScratchSold5Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold5Txt.TabIndex = 94;
            this.LottoScratchSold5Txt.Text = "0";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(806, 240);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(43, 13);
            this.label125.TabIndex = 93;
            this.label125.Text = "Sold 5$";
            // 
            // LottoScratchActualClose5Txt
            // 
            this.LottoScratchActualClose5Txt.Location = new System.Drawing.Point(744, 236);
            this.LottoScratchActualClose5Txt.Name = "LottoScratchActualClose5Txt";
            this.LottoScratchActualClose5Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose5Txt.TabIndex = 92;
            this.LottoScratchActualClose5Txt.Text = "0";
            this.LottoScratchActualClose5Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen5txt_Leave);
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(660, 240);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(81, 13);
            this.label126.TabIndex = 91;
            this.label126.Text = "Actual Close 5$";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(537, 239);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(68, 13);
            this.label127.TabIndex = 90;
            this.label127.Text = "Sub Total 5$";
            // 
            // LottoScratchAddQty5Txt
            // 
            this.LottoScratchAddQty5Txt.Location = new System.Drawing.Point(488, 236);
            this.LottoScratchAddQty5Txt.Name = "LottoScratchAddQty5Txt";
            this.LottoScratchAddQty5Txt.ReadOnly = true;
            this.LottoScratchAddQty5Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty5Txt.TabIndex = 89;
            this.LottoScratchAddQty5Txt.Text = "0";
            this.LottoScratchAddQty5Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen5txt_Leave);
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(422, 239);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(60, 13);
            this.label128.TabIndex = 88;
            this.label128.Text = "Add-Qty 5$";
            // 
            // LottoScratchOpeningDiff5Txt
            // 
            this.LottoScratchOpeningDiff5Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff5Txt.Location = new System.Drawing.Point(372, 236);
            this.LottoScratchOpeningDiff5Txt.Name = "LottoScratchOpeningDiff5Txt";
            this.LottoScratchOpeningDiff5Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff5Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff5Txt.TabIndex = 87;
            this.LottoScratchOpeningDiff5Txt.Text = "0";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(284, 240);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(84, 13);
            this.label129.TabIndex = 86;
            this.label129.Text = "Opening Diff. 5$";
            // 
            // LottoScratchActualOpen5txt
            // 
            this.LottoScratchActualOpen5txt.Location = new System.Drawing.Point(222, 236);
            this.LottoScratchActualOpen5txt.Name = "LottoScratchActualOpen5txt";
            this.LottoScratchActualOpen5txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen5txt.TabIndex = 85;
            this.LottoScratchActualOpen5txt.Text = "0";
            this.LottoScratchActualOpen5txt.Leave += new System.EventHandler(this.LottoScratchActualOpen5txt_Leave);
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(139, 240);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(81, 13);
            this.label130.TabIndex = 84;
            this.label130.Text = "Actual Open 5$";
            // 
            // LottoScratchOpen5Txt
            // 
            this.LottoScratchOpen5Txt.Location = new System.Drawing.Point(81, 236);
            this.LottoScratchOpen5Txt.Name = "LottoScratchOpen5Txt";
            this.LottoScratchOpen5Txt.ReadOnly = true;
            this.LottoScratchOpen5Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen5Txt.TabIndex = 83;
            this.LottoScratchOpen5Txt.Text = "0";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(23, 240);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(48, 13);
            this.label131.TabIndex = 82;
            this.label131.Text = "Open 5$";
            // 
            // LottoScratch4DiffTxt
            // 
            this.LottoScratch4DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch4DiffTxt.Location = new System.Drawing.Point(874, 211);
            this.LottoScratch4DiffTxt.Name = "LottoScratch4DiffTxt";
            this.LottoScratch4DiffTxt.ReadOnly = true;
            this.LottoScratch4DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch4DiffTxt.TabIndex = 81;
            this.LottoScratch4DiffTxt.Text = "0";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(805, 214);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(60, 12);
            this.label112.TabIndex = 80;
            this.label112.Text = "Diff Qty 4$";
            // 
            // LottoReportSold4Txt
            // 
            this.LottoReportSold4Txt.Location = new System.Drawing.Point(745, 212);
            this.LottoReportSold4Txt.Name = "LottoReportSold4Txt";
            this.LottoReportSold4Txt.ReadOnly = true;
            this.LottoReportSold4Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoReportSold4Txt.TabIndex = 79;
            this.LottoReportSold4Txt.Text = "0";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(911, 191);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(88, 13);
            this.label113.TabIndex = 78;
            this.label113.Text = "EOD Sold Qty 4$";
            // 
            // LottoScratchReportSold4Txt
            // 
            this.LottoScratchReportSold4Txt.Location = new System.Drawing.Point(1011, 187);
            this.LottoScratchReportSold4Txt.Name = "LottoScratchReportSold4Txt";
            this.LottoScratchReportSold4Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold4Txt.TabIndex = 77;
            this.LottoScratchReportSold4Txt.Text = "0";
            this.LottoScratchReportSold4Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen4txt_Leave);
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(661, 214);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(74, 12);
            this.label114.TabIndex = 76;
            this.label114.Text = "EOD Sold$ 4$";
            // 
            // LottoScratchSold4Txt
            // 
            this.LottoScratchSold4Txt.Location = new System.Drawing.Point(874, 188);
            this.LottoScratchSold4Txt.Name = "LottoScratchSold4Txt";
            this.LottoScratchSold4Txt.ReadOnly = true;
            this.LottoScratchSold4Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold4Txt.TabIndex = 75;
            this.LottoScratchSold4Txt.Text = "0";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(806, 191);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(43, 13);
            this.label115.TabIndex = 74;
            this.label115.Text = "Sold 4$";
            // 
            // LottoScratchActualClose4Txt
            // 
            this.LottoScratchActualClose4Txt.Location = new System.Drawing.Point(744, 187);
            this.LottoScratchActualClose4Txt.Name = "LottoScratchActualClose4Txt";
            this.LottoScratchActualClose4Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose4Txt.TabIndex = 73;
            this.LottoScratchActualClose4Txt.Text = "0";
            this.LottoScratchActualClose4Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen4txt_Leave);
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(660, 191);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(81, 13);
            this.label116.TabIndex = 72;
            this.label116.Text = "Actual Close 4$";
            // 
            // LottoScratchSubtotal4Txt
            // 
            this.LottoScratchSubtotal4Txt.Location = new System.Drawing.Point(611, 187);
            this.LottoScratchSubtotal4Txt.Name = "LottoScratchSubtotal4Txt";
            this.LottoScratchSubtotal4Txt.ReadOnly = true;
            this.LottoScratchSubtotal4Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal4Txt.TabIndex = 71;
            this.LottoScratchSubtotal4Txt.Text = "0";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(537, 190);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(68, 13);
            this.label117.TabIndex = 70;
            this.label117.Text = "Sub Total 4$";
            // 
            // LottoScratchAddQty4Txt
            // 
            this.LottoScratchAddQty4Txt.Location = new System.Drawing.Point(488, 187);
            this.LottoScratchAddQty4Txt.Name = "LottoScratchAddQty4Txt";
            this.LottoScratchAddQty4Txt.ReadOnly = true;
            this.LottoScratchAddQty4Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty4Txt.TabIndex = 69;
            this.LottoScratchAddQty4Txt.Text = "0";
            this.LottoScratchAddQty4Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen4txt_Leave);
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(422, 190);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(60, 13);
            this.label118.TabIndex = 68;
            this.label118.Text = "Add-Qty 4$";
            // 
            // LottoScratchOpeningDiff4Txt
            // 
            this.LottoScratchOpeningDiff4Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff4Txt.Location = new System.Drawing.Point(372, 187);
            this.LottoScratchOpeningDiff4Txt.Name = "LottoScratchOpeningDiff4Txt";
            this.LottoScratchOpeningDiff4Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff4Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff4Txt.TabIndex = 67;
            this.LottoScratchOpeningDiff4Txt.Text = "0";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(284, 191);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(84, 13);
            this.label119.TabIndex = 66;
            this.label119.Text = "Opening Diff. 4$";
            // 
            // LottoScratchActualOpen4txt
            // 
            this.LottoScratchActualOpen4txt.Location = new System.Drawing.Point(222, 187);
            this.LottoScratchActualOpen4txt.Name = "LottoScratchActualOpen4txt";
            this.LottoScratchActualOpen4txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen4txt.TabIndex = 65;
            this.LottoScratchActualOpen4txt.Text = "0";
            this.LottoScratchActualOpen4txt.Leave += new System.EventHandler(this.LottoScratchActualOpen4txt_Leave);
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(139, 191);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(81, 13);
            this.label120.TabIndex = 64;
            this.label120.Text = "Actual Open 4$";
            // 
            // LottoScratchOpen4Txt
            // 
            this.LottoScratchOpen4Txt.Location = new System.Drawing.Point(81, 187);
            this.LottoScratchOpen4Txt.Name = "LottoScratchOpen4Txt";
            this.LottoScratchOpen4Txt.ReadOnly = true;
            this.LottoScratchOpen4Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen4Txt.TabIndex = 63;
            this.LottoScratchOpen4Txt.Text = "0";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(23, 191);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(48, 13);
            this.label121.TabIndex = 62;
            this.label121.Text = "Open 4$";
            // 
            // LottoScratch3DiffTxt
            // 
            this.LottoScratch3DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch3DiffTxt.Location = new System.Drawing.Point(874, 160);
            this.LottoScratch3DiffTxt.Name = "LottoScratch3DiffTxt";
            this.LottoScratch3DiffTxt.ReadOnly = true;
            this.LottoScratch3DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch3DiffTxt.TabIndex = 61;
            this.LottoScratch3DiffTxt.Text = "0";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(805, 164);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(60, 12);
            this.label102.TabIndex = 60;
            this.label102.Text = "Diff Qty 3$";
            // 
            // LottoReportSold3Txt
            // 
            this.LottoReportSold3Txt.Location = new System.Drawing.Point(744, 160);
            this.LottoReportSold3Txt.Name = "LottoReportSold3Txt";
            this.LottoReportSold3Txt.ReadOnly = true;
            this.LottoReportSold3Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoReportSold3Txt.TabIndex = 59;
            this.LottoReportSold3Txt.Text = "0";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(911, 138);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(88, 13);
            this.label103.TabIndex = 58;
            this.label103.Text = "EOD Sold Qty 3$";
            // 
            // LottoScratchReportSold3Txt
            // 
            this.LottoScratchReportSold3Txt.Location = new System.Drawing.Point(1011, 134);
            this.LottoScratchReportSold3Txt.Name = "LottoScratchReportSold3Txt";
            this.LottoScratchReportSold3Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold3Txt.TabIndex = 57;
            this.LottoScratchReportSold3Txt.Text = "0";
            this.LottoScratchReportSold3Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen3txt_Leave);
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(662, 164);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(74, 12);
            this.label104.TabIndex = 56;
            this.label104.Text = "EOD Sold$ 3$";
            // 
            // LottoScratchSold3Txt
            // 
            this.LottoScratchSold3Txt.Location = new System.Drawing.Point(874, 135);
            this.LottoScratchSold3Txt.Name = "LottoScratchSold3Txt";
            this.LottoScratchSold3Txt.ReadOnly = true;
            this.LottoScratchSold3Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold3Txt.TabIndex = 55;
            this.LottoScratchSold3Txt.Text = "0";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(806, 138);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(43, 13);
            this.label105.TabIndex = 54;
            this.label105.Text = "Sold 3$";
            // 
            // LottoScratchActualClose3Txt
            // 
            this.LottoScratchActualClose3Txt.Location = new System.Drawing.Point(744, 134);
            this.LottoScratchActualClose3Txt.Name = "LottoScratchActualClose3Txt";
            this.LottoScratchActualClose3Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose3Txt.TabIndex = 53;
            this.LottoScratchActualClose3Txt.Text = "0";
            this.LottoScratchActualClose3Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen3txt_Leave);
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(660, 138);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(81, 13);
            this.label106.TabIndex = 52;
            this.label106.Text = "Actual Close 3$";
            // 
            // LottoScratchSubtotal3Txt
            // 
            this.LottoScratchSubtotal3Txt.Location = new System.Drawing.Point(611, 134);
            this.LottoScratchSubtotal3Txt.Name = "LottoScratchSubtotal3Txt";
            this.LottoScratchSubtotal3Txt.ReadOnly = true;
            this.LottoScratchSubtotal3Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal3Txt.TabIndex = 51;
            this.LottoScratchSubtotal3Txt.Text = "0";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(537, 137);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(68, 13);
            this.label107.TabIndex = 50;
            this.label107.Text = "Sub Total 3$";
            // 
            // LottoScratchAddQty3Txt
            // 
            this.LottoScratchAddQty3Txt.Location = new System.Drawing.Point(488, 134);
            this.LottoScratchAddQty3Txt.Name = "LottoScratchAddQty3Txt";
            this.LottoScratchAddQty3Txt.ReadOnly = true;
            this.LottoScratchAddQty3Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty3Txt.TabIndex = 49;
            this.LottoScratchAddQty3Txt.Text = "0";
            this.LottoScratchAddQty3Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen3txt_Leave);
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(422, 137);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(60, 13);
            this.label108.TabIndex = 48;
            this.label108.Text = "Add-Qty 3$";
            // 
            // LottoScratchOpeningDiff3Txt
            // 
            this.LottoScratchOpeningDiff3Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff3Txt.Location = new System.Drawing.Point(372, 134);
            this.LottoScratchOpeningDiff3Txt.Name = "LottoScratchOpeningDiff3Txt";
            this.LottoScratchOpeningDiff3Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff3Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff3Txt.TabIndex = 47;
            this.LottoScratchOpeningDiff3Txt.Text = "0";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(284, 138);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(84, 13);
            this.label109.TabIndex = 46;
            this.label109.Text = "Opening Diff. 3$";
            // 
            // LottoScratchActualOpen3txt
            // 
            this.LottoScratchActualOpen3txt.Location = new System.Drawing.Point(222, 134);
            this.LottoScratchActualOpen3txt.Name = "LottoScratchActualOpen3txt";
            this.LottoScratchActualOpen3txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen3txt.TabIndex = 45;
            this.LottoScratchActualOpen3txt.Text = "0";
            this.LottoScratchActualOpen3txt.Leave += new System.EventHandler(this.LottoScratchActualOpen3txt_Leave);
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(139, 138);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(81, 13);
            this.label110.TabIndex = 44;
            this.label110.Text = "Actual Open 3$";
            // 
            // LottoScratchOpen3Txt
            // 
            this.LottoScratchOpen3Txt.Location = new System.Drawing.Point(81, 134);
            this.LottoScratchOpen3Txt.Name = "LottoScratchOpen3Txt";
            this.LottoScratchOpen3Txt.ReadOnly = true;
            this.LottoScratchOpen3Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen3Txt.TabIndex = 43;
            this.LottoScratchOpen3Txt.Text = "0";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(23, 138);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(48, 13);
            this.label111.TabIndex = 42;
            this.label111.Text = "Open 3$";
            // 
            // LottoScratch2DiffTxt
            // 
            this.LottoScratch2DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch2DiffTxt.Location = new System.Drawing.Point(874, 109);
            this.LottoScratch2DiffTxt.Name = "LottoScratch2DiffTxt";
            this.LottoScratch2DiffTxt.ReadOnly = true;
            this.LottoScratch2DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch2DiffTxt.TabIndex = 41;
            this.LottoScratch2DiffTxt.Text = "0";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(805, 112);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(60, 12);
            this.label66.TabIndex = 40;
            this.label66.Text = "Diff Qty 2$";
            // 
            // LottoReportSold2Txt
            // 
            this.LottoReportSold2Txt.Location = new System.Drawing.Point(743, 110);
            this.LottoReportSold2Txt.Name = "LottoReportSold2Txt";
            this.LottoReportSold2Txt.ReadOnly = true;
            this.LottoReportSold2Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoReportSold2Txt.TabIndex = 39;
            this.LottoReportSold2Txt.Text = "0";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(911, 89);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(88, 13);
            this.label71.TabIndex = 38;
            this.label71.Text = "EOD Sold Qty 2$";
            // 
            // LottoScratchReportSold2Txt
            // 
            this.LottoScratchReportSold2Txt.Location = new System.Drawing.Point(1011, 81);
            this.LottoScratchReportSold2Txt.Name = "LottoScratchReportSold2Txt";
            this.LottoScratchReportSold2Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold2Txt.TabIndex = 37;
            this.LottoScratchReportSold2Txt.Text = "0";
            this.LottoScratchReportSold2Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen2txt_Leave);
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(659, 112);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(74, 12);
            this.label91.TabIndex = 36;
            this.label91.Text = "EOD Sold$ 2$";
            // 
            // LottoScratchSold2Txt
            // 
            this.LottoScratchSold2Txt.Location = new System.Drawing.Point(874, 86);
            this.LottoScratchSold2Txt.Name = "LottoScratchSold2Txt";
            this.LottoScratchSold2Txt.ReadOnly = true;
            this.LottoScratchSold2Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold2Txt.TabIndex = 35;
            this.LottoScratchSold2Txt.Text = "0";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(806, 89);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(43, 13);
            this.label95.TabIndex = 34;
            this.label95.Text = "Sold 2$";
            // 
            // LottoScratchActualClose2Txt
            // 
            this.LottoScratchActualClose2Txt.Location = new System.Drawing.Point(744, 85);
            this.LottoScratchActualClose2Txt.Name = "LottoScratchActualClose2Txt";
            this.LottoScratchActualClose2Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose2Txt.TabIndex = 33;
            this.LottoScratchActualClose2Txt.Text = "0";
            this.LottoScratchActualClose2Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen2txt_Leave);
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(660, 89);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(81, 13);
            this.label96.TabIndex = 32;
            this.label96.Text = "Actual Close 2$";
            // 
            // LottoScratchSubtotal2Txt
            // 
            this.LottoScratchSubtotal2Txt.Location = new System.Drawing.Point(611, 85);
            this.LottoScratchSubtotal2Txt.Name = "LottoScratchSubtotal2Txt";
            this.LottoScratchSubtotal2Txt.ReadOnly = true;
            this.LottoScratchSubtotal2Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal2Txt.TabIndex = 31;
            this.LottoScratchSubtotal2Txt.Text = "0";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(537, 88);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(68, 13);
            this.label97.TabIndex = 30;
            this.label97.Text = "Sub Total 2$";
            // 
            // LottoScratchAddQty2Txt
            // 
            this.LottoScratchAddQty2Txt.Location = new System.Drawing.Point(488, 85);
            this.LottoScratchAddQty2Txt.Name = "LottoScratchAddQty2Txt";
            this.LottoScratchAddQty2Txt.ReadOnly = true;
            this.LottoScratchAddQty2Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty2Txt.TabIndex = 29;
            this.LottoScratchAddQty2Txt.Text = "0";
            this.LottoScratchAddQty2Txt.Leave += new System.EventHandler(this.LottoScratchActualOpen2txt_Leave);
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(422, 88);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(60, 13);
            this.label98.TabIndex = 28;
            this.label98.Text = "Add-Qty 2$";
            // 
            // LottoScratchOpeningDiff2Txt
            // 
            this.LottoScratchOpeningDiff2Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff2Txt.Location = new System.Drawing.Point(372, 85);
            this.LottoScratchOpeningDiff2Txt.Name = "LottoScratchOpeningDiff2Txt";
            this.LottoScratchOpeningDiff2Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff2Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff2Txt.TabIndex = 27;
            this.LottoScratchOpeningDiff2Txt.Text = "0";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(284, 89);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(84, 13);
            this.label99.TabIndex = 26;
            this.label99.Text = "Opening Diff. 2$";
            // 
            // LottoScratchActualOpen2txt
            // 
            this.LottoScratchActualOpen2txt.Location = new System.Drawing.Point(222, 85);
            this.LottoScratchActualOpen2txt.Name = "LottoScratchActualOpen2txt";
            this.LottoScratchActualOpen2txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen2txt.TabIndex = 25;
            this.LottoScratchActualOpen2txt.Text = "0";
            this.LottoScratchActualOpen2txt.Leave += new System.EventHandler(this.LottoScratchActualOpen2txt_Leave);
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(139, 89);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(81, 13);
            this.label100.TabIndex = 24;
            this.label100.Text = "Actual Open 2$";
            // 
            // LottoScratchOpen2Txt
            // 
            this.LottoScratchOpen2Txt.Location = new System.Drawing.Point(81, 85);
            this.LottoScratchOpen2Txt.Name = "LottoScratchOpen2Txt";
            this.LottoScratchOpen2Txt.ReadOnly = true;
            this.LottoScratchOpen2Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen2Txt.TabIndex = 23;
            this.LottoScratchOpen2Txt.Text = "0";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(23, 89);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(48, 13);
            this.label101.TabIndex = 22;
            this.label101.Text = "Open 2$";
            // 
            // LottoScratch1DiffTxt
            // 
            this.LottoScratch1DiffTxt.BackColor = System.Drawing.Color.Red;
            this.LottoScratch1DiffTxt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.LottoScratch1DiffTxt.Location = new System.Drawing.Point(874, 53);
            this.LottoScratch1DiffTxt.Name = "LottoScratch1DiffTxt";
            this.LottoScratch1DiffTxt.ReadOnly = true;
            this.LottoScratch1DiffTxt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratch1DiffTxt.TabIndex = 21;
            this.LottoScratch1DiffTxt.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(805, 57);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(60, 12);
            this.label25.TabIndex = 20;
            this.label25.Text = "Diff Qty 1$";
            // 
            // LottoReportSold1Txt
            // 
            this.LottoReportSold1Txt.Location = new System.Drawing.Point(742, 57);
            this.LottoReportSold1Txt.Name = "LottoReportSold1Txt";
            this.LottoReportSold1Txt.ReadOnly = true;
            this.LottoReportSold1Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoReportSold1Txt.TabIndex = 19;
            this.LottoReportSold1Txt.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(911, 33);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "EOD Sold Qty 1$";
            // 
            // LottoScratchReportSold1Txt
            // 
            this.LottoScratchReportSold1Txt.Location = new System.Drawing.Point(1011, 29);
            this.LottoScratchReportSold1Txt.Name = "LottoScratchReportSold1Txt";
            this.LottoScratchReportSold1Txt.Size = new System.Drawing.Size(47, 20);
            this.LottoScratchReportSold1Txt.TabIndex = 17;
            this.LottoScratchReportSold1Txt.Text = "0";
            this.LottoScratchReportSold1Txt.Leave += new System.EventHandler(this.LottoOnlin1RowChange_Leave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(658, 59);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 12);
            this.label23.TabIndex = 16;
            this.label23.Text = "EOD Sold$ 1$";
            // 
            // LottoScratchSold1Txt
            // 
            this.LottoScratchSold1Txt.Location = new System.Drawing.Point(874, 30);
            this.LottoScratchSold1Txt.Name = "LottoScratchSold1Txt";
            this.LottoScratchSold1Txt.ReadOnly = true;
            this.LottoScratchSold1Txt.Size = new System.Drawing.Size(31, 20);
            this.LottoScratchSold1Txt.TabIndex = 15;
            this.LottoScratchSold1Txt.Text = "0";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(806, 33);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 13);
            this.label22.TabIndex = 14;
            this.label22.Text = "Sold 1$";
            // 
            // LottoScratchActualClose1Txt
            // 
            this.LottoScratchActualClose1Txt.Location = new System.Drawing.Point(744, 29);
            this.LottoScratchActualClose1Txt.Name = "LottoScratchActualClose1Txt";
            this.LottoScratchActualClose1Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchActualClose1Txt.TabIndex = 13;
            this.LottoScratchActualClose1Txt.Text = "0";
            this.LottoScratchActualClose1Txt.Leave += new System.EventHandler(this.LottoOnlin1RowChange_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(660, 33);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 12;
            this.label21.Text = "Actual Close 1$";
            // 
            // LottoScratchSubtotal1Txt
            // 
            this.LottoScratchSubtotal1Txt.Location = new System.Drawing.Point(611, 29);
            this.LottoScratchSubtotal1Txt.Name = "LottoScratchSubtotal1Txt";
            this.LottoScratchSubtotal1Txt.ReadOnly = true;
            this.LottoScratchSubtotal1Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoScratchSubtotal1Txt.TabIndex = 11;
            this.LottoScratchSubtotal1Txt.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(537, 32);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 13);
            this.label20.TabIndex = 10;
            this.label20.Text = "Sub Total 1$";
            // 
            // LottoScratchAddQty1Txt
            // 
            this.LottoScratchAddQty1Txt.Location = new System.Drawing.Point(488, 30);
            this.LottoScratchAddQty1Txt.Name = "LottoScratchAddQty1Txt";
            this.LottoScratchAddQty1Txt.ReadOnly = true;
            this.LottoScratchAddQty1Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchAddQty1Txt.TabIndex = 9;
            this.LottoScratchAddQty1Txt.Text = "0";
            this.LottoScratchAddQty1Txt.Leave += new System.EventHandler(this.LottoOnlin1RowChange_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(422, 32);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "Add-Qty 1$";
            // 
            // LottoScratchOpeningDiff1Txt
            // 
            this.LottoScratchOpeningDiff1Txt.BackColor = System.Drawing.Color.Red;
            this.LottoScratchOpeningDiff1Txt.Location = new System.Drawing.Point(372, 30);
            this.LottoScratchOpeningDiff1Txt.Name = "LottoScratchOpeningDiff1Txt";
            this.LottoScratchOpeningDiff1Txt.ReadOnly = true;
            this.LottoScratchOpeningDiff1Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoScratchOpeningDiff1Txt.TabIndex = 7;
            this.LottoScratchOpeningDiff1Txt.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(284, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(84, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Opening Diff. 1$";
            // 
            // LottoScratchActualOpen1txt
            // 
            this.LottoScratchActualOpen1txt.Location = new System.Drawing.Point(222, 29);
            this.LottoScratchActualOpen1txt.Name = "LottoScratchActualOpen1txt";
            this.LottoScratchActualOpen1txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchActualOpen1txt.TabIndex = 5;
            this.LottoScratchActualOpen1txt.Text = "0";
            this.LottoScratchActualOpen1txt.Leave += new System.EventHandler(this.LottoOnlin1RowChange_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(139, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Actual Open 1$";
            // 
            // LottoScratchOpen1Txt
            // 
            this.LottoScratchOpen1Txt.Location = new System.Drawing.Point(81, 29);
            this.LottoScratchOpen1Txt.Name = "LottoScratchOpen1Txt";
            this.LottoScratchOpen1Txt.ReadOnly = true;
            this.LottoScratchOpen1Txt.Size = new System.Drawing.Size(43, 20);
            this.LottoScratchOpen1Txt.TabIndex = 3;
            this.LottoScratchOpen1Txt.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Open 1$";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LottoActivatedClosing30100Txt);
            this.groupBox2.Controls.Add(this.label229);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing3050Txt);
            this.groupBox2.Controls.Add(this.label230);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing20Txt);
            this.groupBox2.Controls.Add(this.label231);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing10Txt);
            this.groupBox2.Controls.Add(this.label232);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing7Txt);
            this.groupBox2.Controls.Add(this.label233);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing5Txt);
            this.groupBox2.Controls.Add(this.label234);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing4Txt);
            this.groupBox2.Controls.Add(this.label235);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing3Txt);
            this.groupBox2.Controls.Add(this.label236);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing2Txt);
            this.groupBox2.Controls.Add(this.label237);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing1100Txt);
            this.groupBox2.Controls.Add(this.label238);
            this.groupBox2.Controls.Add(this.LottoActivatedClosing150Txt);
            this.groupBox2.Controls.Add(this.label239);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn30100Txt);
            this.groupBox2.Controls.Add(this.label207);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn350Txt);
            this.groupBox2.Controls.Add(this.label208);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn20Txt);
            this.groupBox2.Controls.Add(this.label209);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn10Txt);
            this.groupBox2.Controls.Add(this.label210);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn7Txt);
            this.groupBox2.Controls.Add(this.label211);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn5Txt);
            this.groupBox2.Controls.Add(this.label212);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn4Txt);
            this.groupBox2.Controls.Add(this.label213);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn3Txt);
            this.groupBox2.Controls.Add(this.label214);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn2Txt);
            this.groupBox2.Controls.Add(this.label215);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn1100Txt);
            this.groupBox2.Controls.Add(this.label216);
            this.groupBox2.Controls.Add(this.LottoActivatedReturn150Txt);
            this.groupBox2.Controls.Add(this.label217);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract30100Txt);
            this.groupBox2.Controls.Add(this.label193);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd30100Txt);
            this.groupBox2.Controls.Add(this.label194);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening30100Txt);
            this.groupBox2.Controls.Add(this.label195);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract1100Txt);
            this.groupBox2.Controls.Add(this.label190);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd1100Txt);
            this.groupBox2.Controls.Add(this.label191);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening1100Txt);
            this.groupBox2.Controls.Add(this.label192);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract30Txt);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd30Txt);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening30Txt);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract20Txt);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd20Txt);
            this.groupBox2.Controls.Add(this.label56);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening20Txt);
            this.groupBox2.Controls.Add(this.label57);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract10Txt);
            this.groupBox2.Controls.Add(this.label58);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd10Txt);
            this.groupBox2.Controls.Add(this.label59);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening10Txt);
            this.groupBox2.Controls.Add(this.label60);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract7Txt);
            this.groupBox2.Controls.Add(this.label61);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd7Txt);
            this.groupBox2.Controls.Add(this.label62);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening7Txt);
            this.groupBox2.Controls.Add(this.label63);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract5Txt);
            this.groupBox2.Controls.Add(this.label64);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd5Txt);
            this.groupBox2.Controls.Add(this.label65);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening5Txt);
            this.groupBox2.Controls.Add(this.label72);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract4Txt);
            this.groupBox2.Controls.Add(this.label73);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd4Txt);
            this.groupBox2.Controls.Add(this.label74);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening4Txt);
            this.groupBox2.Controls.Add(this.label75);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract3Txt);
            this.groupBox2.Controls.Add(this.label76);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd3Txt);
            this.groupBox2.Controls.Add(this.label77);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening3Txt);
            this.groupBox2.Controls.Add(this.label78);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract2Txt);
            this.groupBox2.Controls.Add(this.label79);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd2Txt);
            this.groupBox2.Controls.Add(this.label81);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening2Txt);
            this.groupBox2.Controls.Add(this.label82);
            this.groupBox2.Controls.Add(this.LottoActivatedSubstract150Txt);
            this.groupBox2.Controls.Add(this.label83);
            this.groupBox2.Controls.Add(this.LottoActivatedAdd150Txt);
            this.groupBox2.Controls.Add(this.label84);
            this.groupBox2.Controls.Add(this.LottoActivatedOpening150Txt);
            this.groupBox2.Controls.Add(this.label85);
            this.groupBox2.Controls.Add(this.ActivatedDocsDgv);
            this.groupBox2.Controls.Add(this.label69);
            this.groupBox2.Controls.Add(this.LottoActivatedRemarksTxt);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.LottoActivatedTotalTxt);
            this.groupBox2.Location = new System.Drawing.Point(12, 491);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1233, 354);
            this.groupBox2.TabIndex = 247;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Lotto Tickets Activated";
            // 
            // LottoActivatedClosing30100Txt
            // 
            this.LottoActivatedClosing30100Txt.Location = new System.Drawing.Point(774, 288);
            this.LottoActivatedClosing30100Txt.Name = "LottoActivatedClosing30100Txt";
            this.LottoActivatedClosing30100Txt.ReadOnly = true;
            this.LottoActivatedClosing30100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing30100Txt.TabIndex = 397;
            this.LottoActivatedClosing30100Txt.Text = "0";
            // 
            // label229
            // 
            this.label229.AutoSize = true;
            this.label229.Location = new System.Drawing.Point(672, 291);
            this.label229.Name = "label229";
            this.label229.Size = new System.Drawing.Size(89, 13);
            this.label229.TabIndex = 396;
            this.label229.Text = "Closing $30-[100]";
            // 
            // LottoActivatedClosing3050Txt
            // 
            this.LottoActivatedClosing3050Txt.Location = new System.Drawing.Point(774, 259);
            this.LottoActivatedClosing3050Txt.Name = "LottoActivatedClosing3050Txt";
            this.LottoActivatedClosing3050Txt.ReadOnly = true;
            this.LottoActivatedClosing3050Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing3050Txt.TabIndex = 395;
            this.LottoActivatedClosing3050Txt.Text = "0";
            // 
            // label230
            // 
            this.label230.AutoSize = true;
            this.label230.Location = new System.Drawing.Point(672, 262);
            this.label230.Name = "label230";
            this.label230.Size = new System.Drawing.Size(83, 13);
            this.label230.TabIndex = 394;
            this.label230.Text = "Closing $30-[50]";
            // 
            // LottoActivatedClosing20Txt
            // 
            this.LottoActivatedClosing20Txt.Location = new System.Drawing.Point(774, 234);
            this.LottoActivatedClosing20Txt.Name = "LottoActivatedClosing20Txt";
            this.LottoActivatedClosing20Txt.ReadOnly = true;
            this.LottoActivatedClosing20Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing20Txt.TabIndex = 393;
            this.LottoActivatedClosing20Txt.Text = "0";
            // 
            // label231
            // 
            this.label231.AutoSize = true;
            this.label231.Location = new System.Drawing.Point(672, 237);
            this.label231.Name = "label231";
            this.label231.Size = new System.Drawing.Size(62, 13);
            this.label231.TabIndex = 392;
            this.label231.Text = "Closing $20";
            // 
            // LottoActivatedClosing10Txt
            // 
            this.LottoActivatedClosing10Txt.Location = new System.Drawing.Point(774, 208);
            this.LottoActivatedClosing10Txt.Name = "LottoActivatedClosing10Txt";
            this.LottoActivatedClosing10Txt.ReadOnly = true;
            this.LottoActivatedClosing10Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing10Txt.TabIndex = 391;
            this.LottoActivatedClosing10Txt.Text = "0";
            // 
            // label232
            // 
            this.label232.AutoSize = true;
            this.label232.Location = new System.Drawing.Point(672, 211);
            this.label232.Name = "label232";
            this.label232.Size = new System.Drawing.Size(62, 13);
            this.label232.TabIndex = 390;
            this.label232.Text = "Closing $10";
            // 
            // LottoActivatedClosing7Txt
            // 
            this.LottoActivatedClosing7Txt.Location = new System.Drawing.Point(774, 182);
            this.LottoActivatedClosing7Txt.Name = "LottoActivatedClosing7Txt";
            this.LottoActivatedClosing7Txt.ReadOnly = true;
            this.LottoActivatedClosing7Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing7Txt.TabIndex = 389;
            this.LottoActivatedClosing7Txt.Text = "0";
            // 
            // label233
            // 
            this.label233.AutoSize = true;
            this.label233.Location = new System.Drawing.Point(672, 185);
            this.label233.Name = "label233";
            this.label233.Size = new System.Drawing.Size(56, 13);
            this.label233.TabIndex = 388;
            this.label233.Text = "Closing $7";
            // 
            // LottoActivatedClosing5Txt
            // 
            this.LottoActivatedClosing5Txt.Location = new System.Drawing.Point(774, 155);
            this.LottoActivatedClosing5Txt.Name = "LottoActivatedClosing5Txt";
            this.LottoActivatedClosing5Txt.ReadOnly = true;
            this.LottoActivatedClosing5Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing5Txt.TabIndex = 387;
            this.LottoActivatedClosing5Txt.Text = "0";
            // 
            // label234
            // 
            this.label234.AutoSize = true;
            this.label234.Location = new System.Drawing.Point(672, 158);
            this.label234.Name = "label234";
            this.label234.Size = new System.Drawing.Size(56, 13);
            this.label234.TabIndex = 386;
            this.label234.Text = "Closing $5";
            // 
            // LottoActivatedClosing4Txt
            // 
            this.LottoActivatedClosing4Txt.Location = new System.Drawing.Point(774, 129);
            this.LottoActivatedClosing4Txt.Name = "LottoActivatedClosing4Txt";
            this.LottoActivatedClosing4Txt.ReadOnly = true;
            this.LottoActivatedClosing4Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing4Txt.TabIndex = 385;
            this.LottoActivatedClosing4Txt.Text = "0";
            // 
            // label235
            // 
            this.label235.AutoSize = true;
            this.label235.Location = new System.Drawing.Point(672, 132);
            this.label235.Name = "label235";
            this.label235.Size = new System.Drawing.Size(56, 13);
            this.label235.TabIndex = 384;
            this.label235.Text = "Closing $4";
            // 
            // LottoActivatedClosing3Txt
            // 
            this.LottoActivatedClosing3Txt.Location = new System.Drawing.Point(774, 104);
            this.LottoActivatedClosing3Txt.Name = "LottoActivatedClosing3Txt";
            this.LottoActivatedClosing3Txt.ReadOnly = true;
            this.LottoActivatedClosing3Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing3Txt.TabIndex = 383;
            this.LottoActivatedClosing3Txt.Text = "0";
            // 
            // label236
            // 
            this.label236.AutoSize = true;
            this.label236.Location = new System.Drawing.Point(672, 107);
            this.label236.Name = "label236";
            this.label236.Size = new System.Drawing.Size(56, 13);
            this.label236.TabIndex = 382;
            this.label236.Text = "Closing $3";
            // 
            // LottoActivatedClosing2Txt
            // 
            this.LottoActivatedClosing2Txt.Location = new System.Drawing.Point(774, 78);
            this.LottoActivatedClosing2Txt.Name = "LottoActivatedClosing2Txt";
            this.LottoActivatedClosing2Txt.ReadOnly = true;
            this.LottoActivatedClosing2Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing2Txt.TabIndex = 381;
            this.LottoActivatedClosing2Txt.Text = "0";
            // 
            // label237
            // 
            this.label237.AutoSize = true;
            this.label237.Location = new System.Drawing.Point(672, 81);
            this.label237.Name = "label237";
            this.label237.Size = new System.Drawing.Size(56, 13);
            this.label237.TabIndex = 380;
            this.label237.Text = "Closing $2";
            // 
            // LottoActivatedClosing1100Txt
            // 
            this.LottoActivatedClosing1100Txt.Location = new System.Drawing.Point(774, 52);
            this.LottoActivatedClosing1100Txt.Name = "LottoActivatedClosing1100Txt";
            this.LottoActivatedClosing1100Txt.ReadOnly = true;
            this.LottoActivatedClosing1100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing1100Txt.TabIndex = 379;
            this.LottoActivatedClosing1100Txt.Text = "0";
            // 
            // label238
            // 
            this.label238.AutoSize = true;
            this.label238.Location = new System.Drawing.Point(672, 55);
            this.label238.Name = "label238";
            this.label238.Size = new System.Drawing.Size(83, 13);
            this.label238.TabIndex = 378;
            this.label238.Text = "Closing $1-[100]";
            // 
            // LottoActivatedClosing150Txt
            // 
            this.LottoActivatedClosing150Txt.Location = new System.Drawing.Point(774, 25);
            this.LottoActivatedClosing150Txt.Name = "LottoActivatedClosing150Txt";
            this.LottoActivatedClosing150Txt.ReadOnly = true;
            this.LottoActivatedClosing150Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedClosing150Txt.TabIndex = 377;
            this.LottoActivatedClosing150Txt.Text = "0";
            // 
            // label239
            // 
            this.label239.AutoSize = true;
            this.label239.Location = new System.Drawing.Point(672, 28);
            this.label239.Name = "label239";
            this.label239.Size = new System.Drawing.Size(77, 13);
            this.label239.TabIndex = 376;
            this.label239.Text = "Closing $1-[50]";
            // 
            // LottoActivatedReturn30100Txt
            // 
            this.LottoActivatedReturn30100Txt.Location = new System.Drawing.Point(587, 288);
            this.LottoActivatedReturn30100Txt.Name = "LottoActivatedReturn30100Txt";
            this.LottoActivatedReturn30100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn30100Txt.TabIndex = 375;
            this.LottoActivatedReturn30100Txt.Text = "0";
            this.LottoActivatedReturn30100Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label207
            // 
            this.label207.AutoSize = true;
            this.label207.Location = new System.Drawing.Point(485, 291);
            this.label207.Name = "label207";
            this.label207.Size = new System.Drawing.Size(87, 13);
            this.label207.TabIndex = 374;
            this.label207.Text = "Return $30-[100]";
            // 
            // LottoActivatedReturn350Txt
            // 
            this.LottoActivatedReturn350Txt.Location = new System.Drawing.Point(587, 259);
            this.LottoActivatedReturn350Txt.Name = "LottoActivatedReturn350Txt";
            this.LottoActivatedReturn350Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn350Txt.TabIndex = 373;
            this.LottoActivatedReturn350Txt.Text = "0";
            this.LottoActivatedReturn350Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label208
            // 
            this.label208.AutoSize = true;
            this.label208.Location = new System.Drawing.Point(485, 262);
            this.label208.Name = "label208";
            this.label208.Size = new System.Drawing.Size(81, 13);
            this.label208.TabIndex = 372;
            this.label208.Text = "Return $30-[50]";
            // 
            // LottoActivatedReturn20Txt
            // 
            this.LottoActivatedReturn20Txt.Location = new System.Drawing.Point(587, 234);
            this.LottoActivatedReturn20Txt.Name = "LottoActivatedReturn20Txt";
            this.LottoActivatedReturn20Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn20Txt.TabIndex = 371;
            this.LottoActivatedReturn20Txt.Text = "0";
            this.LottoActivatedReturn20Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label209
            // 
            this.label209.AutoSize = true;
            this.label209.Location = new System.Drawing.Point(485, 237);
            this.label209.Name = "label209";
            this.label209.Size = new System.Drawing.Size(60, 13);
            this.label209.TabIndex = 370;
            this.label209.Text = "Return $20";
            // 
            // LottoActivatedReturn10Txt
            // 
            this.LottoActivatedReturn10Txt.Location = new System.Drawing.Point(587, 208);
            this.LottoActivatedReturn10Txt.Name = "LottoActivatedReturn10Txt";
            this.LottoActivatedReturn10Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn10Txt.TabIndex = 369;
            this.LottoActivatedReturn10Txt.Text = "0";
            this.LottoActivatedReturn10Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label210
            // 
            this.label210.AutoSize = true;
            this.label210.Location = new System.Drawing.Point(485, 211);
            this.label210.Name = "label210";
            this.label210.Size = new System.Drawing.Size(60, 13);
            this.label210.TabIndex = 368;
            this.label210.Text = "Return $10";
            // 
            // LottoActivatedReturn7Txt
            // 
            this.LottoActivatedReturn7Txt.Location = new System.Drawing.Point(587, 182);
            this.LottoActivatedReturn7Txt.Name = "LottoActivatedReturn7Txt";
            this.LottoActivatedReturn7Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn7Txt.TabIndex = 367;
            this.LottoActivatedReturn7Txt.Text = "0";
            this.LottoActivatedReturn7Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label211
            // 
            this.label211.AutoSize = true;
            this.label211.Location = new System.Drawing.Point(485, 185);
            this.label211.Name = "label211";
            this.label211.Size = new System.Drawing.Size(54, 13);
            this.label211.TabIndex = 366;
            this.label211.Text = "Return $7";
            // 
            // LottoActivatedReturn5Txt
            // 
            this.LottoActivatedReturn5Txt.Location = new System.Drawing.Point(587, 155);
            this.LottoActivatedReturn5Txt.Name = "LottoActivatedReturn5Txt";
            this.LottoActivatedReturn5Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn5Txt.TabIndex = 365;
            this.LottoActivatedReturn5Txt.Text = "0";
            this.LottoActivatedReturn5Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label212
            // 
            this.label212.AutoSize = true;
            this.label212.Location = new System.Drawing.Point(485, 158);
            this.label212.Name = "label212";
            this.label212.Size = new System.Drawing.Size(54, 13);
            this.label212.TabIndex = 364;
            this.label212.Text = "Return $5";
            // 
            // LottoActivatedReturn4Txt
            // 
            this.LottoActivatedReturn4Txt.Location = new System.Drawing.Point(587, 129);
            this.LottoActivatedReturn4Txt.Name = "LottoActivatedReturn4Txt";
            this.LottoActivatedReturn4Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn4Txt.TabIndex = 363;
            this.LottoActivatedReturn4Txt.Text = "0";
            this.LottoActivatedReturn4Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label213
            // 
            this.label213.AutoSize = true;
            this.label213.Location = new System.Drawing.Point(485, 132);
            this.label213.Name = "label213";
            this.label213.Size = new System.Drawing.Size(54, 13);
            this.label213.TabIndex = 362;
            this.label213.Text = "Return $4";
            // 
            // LottoActivatedReturn3Txt
            // 
            this.LottoActivatedReturn3Txt.Location = new System.Drawing.Point(587, 104);
            this.LottoActivatedReturn3Txt.Name = "LottoActivatedReturn3Txt";
            this.LottoActivatedReturn3Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn3Txt.TabIndex = 361;
            this.LottoActivatedReturn3Txt.Text = "0";
            this.LottoActivatedReturn3Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label214
            // 
            this.label214.AutoSize = true;
            this.label214.Location = new System.Drawing.Point(485, 107);
            this.label214.Name = "label214";
            this.label214.Size = new System.Drawing.Size(54, 13);
            this.label214.TabIndex = 360;
            this.label214.Text = "Return $3";
            // 
            // LottoActivatedReturn2Txt
            // 
            this.LottoActivatedReturn2Txt.Location = new System.Drawing.Point(587, 78);
            this.LottoActivatedReturn2Txt.Name = "LottoActivatedReturn2Txt";
            this.LottoActivatedReturn2Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn2Txt.TabIndex = 359;
            this.LottoActivatedReturn2Txt.Text = "0";
            this.LottoActivatedReturn2Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label215
            // 
            this.label215.AutoSize = true;
            this.label215.Location = new System.Drawing.Point(485, 81);
            this.label215.Name = "label215";
            this.label215.Size = new System.Drawing.Size(54, 13);
            this.label215.TabIndex = 358;
            this.label215.Text = "Return $2";
            // 
            // LottoActivatedReturn1100Txt
            // 
            this.LottoActivatedReturn1100Txt.Location = new System.Drawing.Point(587, 52);
            this.LottoActivatedReturn1100Txt.Name = "LottoActivatedReturn1100Txt";
            this.LottoActivatedReturn1100Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn1100Txt.TabIndex = 357;
            this.LottoActivatedReturn1100Txt.Text = "0";
            this.LottoActivatedReturn1100Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label216
            // 
            this.label216.AutoSize = true;
            this.label216.Location = new System.Drawing.Point(485, 55);
            this.label216.Name = "label216";
            this.label216.Size = new System.Drawing.Size(81, 13);
            this.label216.TabIndex = 356;
            this.label216.Text = "Return $1-[100]";
            // 
            // LottoActivatedReturn150Txt
            // 
            this.LottoActivatedReturn150Txt.Location = new System.Drawing.Point(587, 25);
            this.LottoActivatedReturn150Txt.Name = "LottoActivatedReturn150Txt";
            this.LottoActivatedReturn150Txt.Size = new System.Drawing.Size(44, 20);
            this.LottoActivatedReturn150Txt.TabIndex = 355;
            this.LottoActivatedReturn150Txt.Text = "0";
            this.LottoActivatedReturn150Txt.Leave += new System.EventHandler(this.LottoActivatedReturn150Txt_Leave);
            // 
            // label217
            // 
            this.label217.AutoSize = true;
            this.label217.Location = new System.Drawing.Point(485, 28);
            this.label217.Name = "label217";
            this.label217.Size = new System.Drawing.Size(75, 13);
            this.label217.TabIndex = 354;
            this.label217.Text = "Return $1-[50]";
            // 
            // LottoActivatedSubstract30100Txt
            // 
            this.LottoActivatedSubstract30100Txt.Location = new System.Drawing.Point(422, 288);
            this.LottoActivatedSubstract30100Txt.Name = "LottoActivatedSubstract30100Txt";
            this.LottoActivatedSubstract30100Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedSubstract30100Txt.TabIndex = 353;
            this.LottoActivatedSubstract30100Txt.Text = "0";
            this.LottoActivatedSubstract30100Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(320, 288);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(95, 13);
            this.label193.TabIndex = 352;
            this.label193.Text = "Subtract $30-[100]";
            // 
            // LottoActivatedAdd30100Txt
            // 
            this.LottoActivatedAdd30100Txt.Location = new System.Drawing.Point(258, 284);
            this.LottoActivatedAdd30100Txt.Name = "LottoActivatedAdd30100Txt";
            this.LottoActivatedAdd30100Txt.ReadOnly = true;
            this.LottoActivatedAdd30100Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedAdd30100Txt.TabIndex = 351;
            this.LottoActivatedAdd30100Txt.Text = "0";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(183, 288);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(74, 13);
            this.label194.TabIndex = 350;
            this.label194.Text = "Add $30-[100]";
            // 
            // LottoActivatedOpening30100Txt
            // 
            this.LottoActivatedOpening30100Txt.Location = new System.Drawing.Point(118, 284);
            this.LottoActivatedOpening30100Txt.Name = "LottoActivatedOpening30100Txt";
            this.LottoActivatedOpening30100Txt.ReadOnly = true;
            this.LottoActivatedOpening30100Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedOpening30100Txt.TabIndex = 349;
            this.LottoActivatedOpening30100Txt.Text = "0";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(16, 284);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(95, 13);
            this.label195.TabIndex = 348;
            this.label195.Text = "Opening $30-[100]";
            // 
            // LottoActivatedSubstract1100Txt
            // 
            this.LottoActivatedSubstract1100Txt.Location = new System.Drawing.Point(421, 49);
            this.LottoActivatedSubstract1100Txt.Name = "LottoActivatedSubstract1100Txt";
            this.LottoActivatedSubstract1100Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedSubstract1100Txt.TabIndex = 347;
            this.LottoActivatedSubstract1100Txt.Text = "0";
            this.LottoActivatedSubstract1100Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(320, 51);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(89, 13);
            this.label190.TabIndex = 346;
            this.label190.Text = "Subtract $1-[100]";
            // 
            // LottoActivatedAdd1100Txt
            // 
            this.LottoActivatedAdd1100Txt.Location = new System.Drawing.Point(258, 47);
            this.LottoActivatedAdd1100Txt.Name = "LottoActivatedAdd1100Txt";
            this.LottoActivatedAdd1100Txt.ReadOnly = true;
            this.LottoActivatedAdd1100Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedAdd1100Txt.TabIndex = 345;
            this.LottoActivatedAdd1100Txt.Text = "0";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Location = new System.Drawing.Point(182, 51);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(68, 13);
            this.label191.TabIndex = 344;
            this.label191.Text = "Add $1-[100]";
            // 
            // LottoActivatedOpening1100Txt
            // 
            this.LottoActivatedOpening1100Txt.Location = new System.Drawing.Point(119, 51);
            this.LottoActivatedOpening1100Txt.Name = "LottoActivatedOpening1100Txt";
            this.LottoActivatedOpening1100Txt.ReadOnly = true;
            this.LottoActivatedOpening1100Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedOpening1100Txt.TabIndex = 343;
            this.LottoActivatedOpening1100Txt.Text = "0";
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(16, 51);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(89, 13);
            this.label192.TabIndex = 342;
            this.label192.Text = "Opening $1-[100]";
            // 
            // LottoActivatedSubstract30Txt
            // 
            this.LottoActivatedSubstract30Txt.Location = new System.Drawing.Point(422, 263);
            this.LottoActivatedSubstract30Txt.Name = "LottoActivatedSubstract30Txt";
            this.LottoActivatedSubstract30Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedSubstract30Txt.TabIndex = 341;
            this.LottoActivatedSubstract30Txt.Text = "0";
            this.LottoActivatedSubstract30Txt.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.LottoActivatedSubstract30Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(320, 263);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 340;
            this.label9.Text = "Subtract $30-[50]";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // LottoActivatedAdd30Txt
            // 
            this.LottoActivatedAdd30Txt.Location = new System.Drawing.Point(258, 259);
            this.LottoActivatedAdd30Txt.Name = "LottoActivatedAdd30Txt";
            this.LottoActivatedAdd30Txt.ReadOnly = true;
            this.LottoActivatedAdd30Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedAdd30Txt.TabIndex = 339;
            this.LottoActivatedAdd30Txt.Text = "0";
            this.LottoActivatedAdd30Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(183, 263);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 338;
            this.label11.Text = "Add $30-[50]";
            // 
            // LottoActivatedOpening30Txt
            // 
            this.LottoActivatedOpening30Txt.Location = new System.Drawing.Point(118, 259);
            this.LottoActivatedOpening30Txt.Name = "LottoActivatedOpening30Txt";
            this.LottoActivatedOpening30Txt.ReadOnly = true;
            this.LottoActivatedOpening30Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedOpening30Txt.TabIndex = 337;
            this.LottoActivatedOpening30Txt.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 259);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 336;
            this.label12.Text = "Opening $30-[50]";
            // 
            // LottoActivatedSubstract20Txt
            // 
            this.LottoActivatedSubstract20Txt.Location = new System.Drawing.Point(422, 238);
            this.LottoActivatedSubstract20Txt.Name = "LottoActivatedSubstract20Txt";
            this.LottoActivatedSubstract20Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedSubstract20Txt.TabIndex = 335;
            this.LottoActivatedSubstract20Txt.Text = "0";
            this.LottoActivatedSubstract20Txt.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            this.LottoActivatedSubstract20Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(320, 241);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 334;
            this.label13.Text = "Subtract $20";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // LottoActivatedAdd20Txt
            // 
            this.LottoActivatedAdd20Txt.Location = new System.Drawing.Point(258, 233);
            this.LottoActivatedAdd20Txt.Name = "LottoActivatedAdd20Txt";
            this.LottoActivatedAdd20Txt.ReadOnly = true;
            this.LottoActivatedAdd20Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedAdd20Txt.TabIndex = 333;
            this.LottoActivatedAdd20Txt.Text = "0";
            this.LottoActivatedAdd20Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(183, 235);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(47, 13);
            this.label56.TabIndex = 332;
            this.label56.Text = "Add $20";
            // 
            // LottoActivatedOpening20Txt
            // 
            this.LottoActivatedOpening20Txt.Location = new System.Drawing.Point(118, 234);
            this.LottoActivatedOpening20Txt.Name = "LottoActivatedOpening20Txt";
            this.LottoActivatedOpening20Txt.ReadOnly = true;
            this.LottoActivatedOpening20Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedOpening20Txt.TabIndex = 331;
            this.LottoActivatedOpening20Txt.Text = "0";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(15, 234);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(68, 13);
            this.label57.TabIndex = 330;
            this.label57.Text = "Opening $20";
            // 
            // LottoActivatedSubstract10Txt
            // 
            this.LottoActivatedSubstract10Txt.Location = new System.Drawing.Point(422, 215);
            this.LottoActivatedSubstract10Txt.Name = "LottoActivatedSubstract10Txt";
            this.LottoActivatedSubstract10Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedSubstract10Txt.TabIndex = 329;
            this.LottoActivatedSubstract10Txt.Text = "0";
            this.LottoActivatedSubstract10Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(320, 218);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(68, 13);
            this.label58.TabIndex = 328;
            this.label58.Text = "Subtract $10";
            // 
            // LottoActivatedAdd10Txt
            // 
            this.LottoActivatedAdd10Txt.Location = new System.Drawing.Point(258, 208);
            this.LottoActivatedAdd10Txt.Name = "LottoActivatedAdd10Txt";
            this.LottoActivatedAdd10Txt.ReadOnly = true;
            this.LottoActivatedAdd10Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedAdd10Txt.TabIndex = 327;
            this.LottoActivatedAdd10Txt.Text = "0";
            this.LottoActivatedAdd10Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(181, 209);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(47, 13);
            this.label59.TabIndex = 326;
            this.label59.Text = "Add $10";
            // 
            // LottoActivatedOpening10Txt
            // 
            this.LottoActivatedOpening10Txt.Location = new System.Drawing.Point(118, 208);
            this.LottoActivatedOpening10Txt.Name = "LottoActivatedOpening10Txt";
            this.LottoActivatedOpening10Txt.ReadOnly = true;
            this.LottoActivatedOpening10Txt.Size = new System.Drawing.Size(45, 20);
            this.LottoActivatedOpening10Txt.TabIndex = 325;
            this.LottoActivatedOpening10Txt.Text = "0";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(16, 208);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(68, 13);
            this.label60.TabIndex = 324;
            this.label60.Text = "Opening $10";
            // 
            // LottoActivatedSubstract7Txt
            // 
            this.LottoActivatedSubstract7Txt.Location = new System.Drawing.Point(421, 189);
            this.LottoActivatedSubstract7Txt.Name = "LottoActivatedSubstract7Txt";
            this.LottoActivatedSubstract7Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedSubstract7Txt.TabIndex = 323;
            this.LottoActivatedSubstract7Txt.Text = "0";
            this.LottoActivatedSubstract7Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(320, 189);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(62, 13);
            this.label61.TabIndex = 322;
            this.label61.Text = "Subtract $7";
            // 
            // LottoActivatedAdd7Txt
            // 
            this.LottoActivatedAdd7Txt.Location = new System.Drawing.Point(258, 182);
            this.LottoActivatedAdd7Txt.Name = "LottoActivatedAdd7Txt";
            this.LottoActivatedAdd7Txt.ReadOnly = true;
            this.LottoActivatedAdd7Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedAdd7Txt.TabIndex = 321;
            this.LottoActivatedAdd7Txt.Text = "0";
            this.LottoActivatedAdd7Txt.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            this.LottoActivatedAdd7Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(182, 184);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(41, 13);
            this.label62.TabIndex = 320;
            this.label62.Text = "Add $7";
            this.label62.Click += new System.EventHandler(this.label62_Click);
            // 
            // LottoActivatedOpening7Txt
            // 
            this.LottoActivatedOpening7Txt.Location = new System.Drawing.Point(119, 183);
            this.LottoActivatedOpening7Txt.Name = "LottoActivatedOpening7Txt";
            this.LottoActivatedOpening7Txt.ReadOnly = true;
            this.LottoActivatedOpening7Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedOpening7Txt.TabIndex = 319;
            this.LottoActivatedOpening7Txt.Text = "0";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(15, 183);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(62, 13);
            this.label63.TabIndex = 318;
            this.label63.Text = "Opening $7";
            // 
            // LottoActivatedSubstract5Txt
            // 
            this.LottoActivatedSubstract5Txt.Location = new System.Drawing.Point(421, 161);
            this.LottoActivatedSubstract5Txt.Name = "LottoActivatedSubstract5Txt";
            this.LottoActivatedSubstract5Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedSubstract5Txt.TabIndex = 317;
            this.LottoActivatedSubstract5Txt.Text = "0";
            this.LottoActivatedSubstract5Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(320, 161);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(62, 13);
            this.label64.TabIndex = 316;
            this.label64.Text = "Subtract $5";
            // 
            // LottoActivatedAdd5Txt
            // 
            this.LottoActivatedAdd5Txt.Location = new System.Drawing.Point(258, 156);
            this.LottoActivatedAdd5Txt.Name = "LottoActivatedAdd5Txt";
            this.LottoActivatedAdd5Txt.ReadOnly = true;
            this.LottoActivatedAdd5Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedAdd5Txt.TabIndex = 315;
            this.LottoActivatedAdd5Txt.Text = "0";
            this.LottoActivatedAdd5Txt.TextChanged += new System.EventHandler(this.LottoActivatedAdd5txt_TextChanged);
            this.LottoActivatedAdd5Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(181, 161);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 314;
            this.label65.Text = "Add $5";
            this.label65.Click += new System.EventHandler(this.label65_Click);
            // 
            // LottoActivatedOpening5Txt
            // 
            this.LottoActivatedOpening5Txt.Location = new System.Drawing.Point(119, 160);
            this.LottoActivatedOpening5Txt.Name = "LottoActivatedOpening5Txt";
            this.LottoActivatedOpening5Txt.ReadOnly = true;
            this.LottoActivatedOpening5Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedOpening5Txt.TabIndex = 313;
            this.LottoActivatedOpening5Txt.Text = "0";
            this.LottoActivatedOpening5Txt.TextChanged += new System.EventHandler(this.textBox15_TextChanged);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(16, 160);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(62, 13);
            this.label72.TabIndex = 312;
            this.label72.Text = "Opening $5";
            this.label72.Click += new System.EventHandler(this.label72_Click);
            // 
            // LottoActivatedSubstract4Txt
            // 
            this.LottoActivatedSubstract4Txt.Location = new System.Drawing.Point(421, 135);
            this.LottoActivatedSubstract4Txt.Name = "LottoActivatedSubstract4Txt";
            this.LottoActivatedSubstract4Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedSubstract4Txt.TabIndex = 311;
            this.LottoActivatedSubstract4Txt.Text = "0";
            this.LottoActivatedSubstract4Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(320, 134);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(62, 13);
            this.label73.TabIndex = 310;
            this.label73.Text = "Subtract $4";
            // 
            // LottoActivatedAdd4Txt
            // 
            this.LottoActivatedAdd4Txt.Location = new System.Drawing.Point(258, 130);
            this.LottoActivatedAdd4Txt.Name = "LottoActivatedAdd4Txt";
            this.LottoActivatedAdd4Txt.ReadOnly = true;
            this.LottoActivatedAdd4Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedAdd4Txt.TabIndex = 309;
            this.LottoActivatedAdd4Txt.Text = "0";
            this.LottoActivatedAdd4Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(182, 134);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(41, 13);
            this.label74.TabIndex = 308;
            this.label74.Text = "Add $4";
            // 
            // LottoActivatedOpening4Txt
            // 
            this.LottoActivatedOpening4Txt.Location = new System.Drawing.Point(119, 134);
            this.LottoActivatedOpening4Txt.Name = "LottoActivatedOpening4Txt";
            this.LottoActivatedOpening4Txt.ReadOnly = true;
            this.LottoActivatedOpening4Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedOpening4Txt.TabIndex = 307;
            this.LottoActivatedOpening4Txt.Text = "0";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(16, 134);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(62, 13);
            this.label75.TabIndex = 306;
            this.label75.Text = "Opening $4";
            // 
            // LottoActivatedSubstract3Txt
            // 
            this.LottoActivatedSubstract3Txt.Location = new System.Drawing.Point(421, 104);
            this.LottoActivatedSubstract3Txt.Name = "LottoActivatedSubstract3Txt";
            this.LottoActivatedSubstract3Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedSubstract3Txt.TabIndex = 305;
            this.LottoActivatedSubstract3Txt.Text = "0";
            this.LottoActivatedSubstract3Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(320, 110);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(62, 13);
            this.label76.TabIndex = 304;
            this.label76.Text = "Subtract $3";
            // 
            // LottoActivatedAdd3Txt
            // 
            this.LottoActivatedAdd3Txt.Location = new System.Drawing.Point(258, 106);
            this.LottoActivatedAdd3Txt.Name = "LottoActivatedAdd3Txt";
            this.LottoActivatedAdd3Txt.ReadOnly = true;
            this.LottoActivatedAdd3Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedAdd3Txt.TabIndex = 303;
            this.LottoActivatedAdd3Txt.Text = "0";
            this.LottoActivatedAdd3Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(182, 107);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(41, 13);
            this.label77.TabIndex = 302;
            this.label77.Text = "Add $3";
            // 
            // LottoActivatedOpening3Txt
            // 
            this.LottoActivatedOpening3Txt.Location = new System.Drawing.Point(119, 104);
            this.LottoActivatedOpening3Txt.Name = "LottoActivatedOpening3Txt";
            this.LottoActivatedOpening3Txt.ReadOnly = true;
            this.LottoActivatedOpening3Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedOpening3Txt.TabIndex = 301;
            this.LottoActivatedOpening3Txt.Text = "0";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(16, 104);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(62, 13);
            this.label78.TabIndex = 300;
            this.label78.Text = "Opening $3";
            // 
            // LottoActivatedSubstract2Txt
            // 
            this.LottoActivatedSubstract2Txt.Location = new System.Drawing.Point(421, 76);
            this.LottoActivatedSubstract2Txt.Name = "LottoActivatedSubstract2Txt";
            this.LottoActivatedSubstract2Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedSubstract2Txt.TabIndex = 299;
            this.LottoActivatedSubstract2Txt.Text = "0";
            this.LottoActivatedSubstract2Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(320, 76);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(62, 13);
            this.label79.TabIndex = 298;
            this.label79.Text = "Subtract $2";
            // 
            // LottoActivatedAdd2Txt
            // 
            this.LottoActivatedAdd2Txt.Location = new System.Drawing.Point(258, 73);
            this.LottoActivatedAdd2Txt.Name = "LottoActivatedAdd2Txt";
            this.LottoActivatedAdd2Txt.ReadOnly = true;
            this.LottoActivatedAdd2Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedAdd2Txt.TabIndex = 297;
            this.LottoActivatedAdd2Txt.Text = "0";
            this.LottoActivatedAdd2Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(180, 77);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(41, 13);
            this.label81.TabIndex = 296;
            this.label81.Text = "Add $2";
            // 
            // LottoActivatedOpening2Txt
            // 
            this.LottoActivatedOpening2Txt.Location = new System.Drawing.Point(119, 76);
            this.LottoActivatedOpening2Txt.Name = "LottoActivatedOpening2Txt";
            this.LottoActivatedOpening2Txt.ReadOnly = true;
            this.LottoActivatedOpening2Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedOpening2Txt.TabIndex = 295;
            this.LottoActivatedOpening2Txt.Text = "0";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(16, 76);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(62, 13);
            this.label82.TabIndex = 294;
            this.label82.Text = "Opening $2";
            // 
            // LottoActivatedSubstract150Txt
            // 
            this.LottoActivatedSubstract150Txt.Location = new System.Drawing.Point(421, 23);
            this.LottoActivatedSubstract150Txt.Name = "LottoActivatedSubstract150Txt";
            this.LottoActivatedSubstract150Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedSubstract150Txt.TabIndex = 293;
            this.LottoActivatedSubstract150Txt.Text = "0";
            this.LottoActivatedSubstract150Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(320, 25);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(83, 13);
            this.label83.TabIndex = 292;
            this.label83.Text = "Subtract $1-[50]";
            // 
            // LottoActivatedAdd150Txt
            // 
            this.LottoActivatedAdd150Txt.Location = new System.Drawing.Point(258, 21);
            this.LottoActivatedAdd150Txt.Name = "LottoActivatedAdd150Txt";
            this.LottoActivatedAdd150Txt.ReadOnly = true;
            this.LottoActivatedAdd150Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedAdd150Txt.TabIndex = 291;
            this.LottoActivatedAdd150Txt.Text = "0";
            this.LottoActivatedAdd150Txt.Leave += new System.EventHandler(this.LottoActivatedAdd1Txt_Leave);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(182, 25);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(62, 13);
            this.label84.TabIndex = 290;
            this.label84.Text = "Add $1-[50]";
            // 
            // LottoActivatedOpening150Txt
            // 
            this.LottoActivatedOpening150Txt.Location = new System.Drawing.Point(119, 25);
            this.LottoActivatedOpening150Txt.Name = "LottoActivatedOpening150Txt";
            this.LottoActivatedOpening150Txt.ReadOnly = true;
            this.LottoActivatedOpening150Txt.Size = new System.Drawing.Size(46, 20);
            this.LottoActivatedOpening150Txt.TabIndex = 289;
            this.LottoActivatedOpening150Txt.Text = "0";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(16, 25);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(83, 13);
            this.label85.TabIndex = 288;
            this.label85.Text = "Opening $1-[50]";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.AmountDueTxt);
            this.groupBox3.Controls.Add(this.RedemptionsTxt);
            this.groupBox3.Controls.Add(this.CreditsTxt);
            this.groupBox3.Controls.Add(this.RetailerBonusTxt);
            this.groupBox3.Controls.Add(this.ActivationTxt);
            this.groupBox3.Controls.Add(this.OnlineSoldtxt);
            this.groupBox3.Controls.Add(this.LottoWeeklyDgv);
            this.groupBox3.Controls.Add(this.label93);
            this.groupBox3.Controls.Add(this.LottoWeeklyRemarksTxt);
            this.groupBox3.Controls.Add(this.label94);
            this.groupBox3.Controls.Add(this.label86);
            this.groupBox3.Controls.Add(this.label87);
            this.groupBox3.Controls.Add(this.label88);
            this.groupBox3.Controls.Add(this.label89);
            this.groupBox3.Controls.Add(this.label90);
            this.groupBox3.Controls.Add(this.label92);
            this.groupBox3.Location = new System.Drawing.Point(18, 1898);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1062, 228);
            this.groupBox3.TabIndex = 256;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Lotto Weekly Invoices";
            // 
            // AmountDueTxt
            // 
            this.AmountDueTxt.Location = new System.Drawing.Point(118, 180);
            this.AmountDueTxt.Name = "AmountDueTxt";
            this.AmountDueTxt.Size = new System.Drawing.Size(125, 20);
            this.AmountDueTxt.TabIndex = 258;
            this.AmountDueTxt.Text = "0";
            // 
            // RedemptionsTxt
            // 
            this.RedemptionsTxt.Location = new System.Drawing.Point(119, 148);
            this.RedemptionsTxt.Name = "RedemptionsTxt";
            this.RedemptionsTxt.Size = new System.Drawing.Size(125, 20);
            this.RedemptionsTxt.TabIndex = 257;
            this.RedemptionsTxt.Text = "0";
            // 
            // CreditsTxt
            // 
            this.CreditsTxt.Location = new System.Drawing.Point(119, 116);
            this.CreditsTxt.Name = "CreditsTxt";
            this.CreditsTxt.Size = new System.Drawing.Size(125, 20);
            this.CreditsTxt.TabIndex = 256;
            this.CreditsTxt.Text = "0";
            // 
            // RetailerBonusTxt
            // 
            this.RetailerBonusTxt.Location = new System.Drawing.Point(118, 84);
            this.RetailerBonusTxt.Name = "RetailerBonusTxt";
            this.RetailerBonusTxt.Size = new System.Drawing.Size(125, 20);
            this.RetailerBonusTxt.TabIndex = 255;
            this.RetailerBonusTxt.Text = "0";
            // 
            // ActivationTxt
            // 
            this.ActivationTxt.Location = new System.Drawing.Point(118, 52);
            this.ActivationTxt.Name = "ActivationTxt";
            this.ActivationTxt.Size = new System.Drawing.Size(125, 20);
            this.ActivationTxt.TabIndex = 254;
            this.ActivationTxt.Text = "0";
            // 
            // OnlineSoldtxt
            // 
            this.OnlineSoldtxt.Location = new System.Drawing.Point(119, 20);
            this.OnlineSoldtxt.Name = "OnlineSoldtxt";
            this.OnlineSoldtxt.Size = new System.Drawing.Size(125, 20);
            this.OnlineSoldtxt.TabIndex = 253;
            this.OnlineSoldtxt.Text = "0";
            // 
            // LottoWeeklyDgv
            // 
            this.LottoWeeklyDgv.AllowDrop = true;
            this.LottoWeeklyDgv.AllowUserToAddRows = false;
            this.LottoWeeklyDgv.AllowUserToDeleteRows = false;
            this.LottoWeeklyDgv.AllowUserToResizeColumns = false;
            this.LottoWeeklyDgv.AllowUserToResizeRows = false;
            this.LottoWeeklyDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.LottoWeeklyDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LottoWeeklyDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this.LottoWeeklyDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LottoWeeklyDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn4});
            this.LottoWeeklyDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.LottoWeeklyDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.LottoWeeklyDgv.Location = new System.Drawing.Point(818, 36);
            this.LottoWeeklyDgv.MultiSelect = false;
            this.LottoWeeklyDgv.Name = "LottoWeeklyDgv";
            this.LottoWeeklyDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.LottoWeeklyDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.LottoWeeklyDgv.RowHeadersVisible = false;
            this.LottoWeeklyDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.LottoWeeklyDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.LottoWeeklyDgv.Size = new System.Drawing.Size(222, 105);
            this.LottoWeeklyDgv.TabIndex = 249;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 5;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Black;
            this.label93.Location = new System.Drawing.Point(815, 13);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(156, 15);
            this.label93.TabIndex = 250;
            this.label93.Text = "Lotto Weekly Invoices Docs";
            // 
            // LottoWeeklyRemarksTxt
            // 
            this.LottoWeeklyRemarksTxt.Location = new System.Drawing.Point(818, 159);
            this.LottoWeeklyRemarksTxt.Name = "LottoWeeklyRemarksTxt";
            this.LottoWeeklyRemarksTxt.Size = new System.Drawing.Size(222, 57);
            this.LottoWeeklyRemarksTxt.TabIndex = 248;
            this.LottoWeeklyRemarksTxt.Text = "";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(711, 170);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(49, 13);
            this.label94.TabIndex = 247;
            this.label94.Text = "Remarks";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.BackColor = System.Drawing.Color.AliceBlue;
            this.label86.Font = new System.Drawing.Font("Arial", 9F);
            this.label86.ForeColor = System.Drawing.Color.Black;
            this.label86.Location = new System.Drawing.Point(18, 185);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(75, 15);
            this.label86.TabIndex = 64;
            this.label86.Text = "Amount Due";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.BackColor = System.Drawing.Color.AliceBlue;
            this.label87.Font = new System.Drawing.Font("Arial", 9F);
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(19, 89);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(89, 15);
            this.label87.TabIndex = 62;
            this.label87.Text = "Retailer Bonus";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.BackColor = System.Drawing.Color.AliceBlue;
            this.label88.Font = new System.Drawing.Font("Arial", 9F);
            this.label88.ForeColor = System.Drawing.Color.Black;
            this.label88.Location = new System.Drawing.Point(19, 153);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(82, 15);
            this.label88.TabIndex = 60;
            this.label88.Text = "Redemptions";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.BackColor = System.Drawing.Color.AliceBlue;
            this.label89.Font = new System.Drawing.Font("Arial", 9F);
            this.label89.ForeColor = System.Drawing.Color.Black;
            this.label89.Location = new System.Drawing.Point(19, 121);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(47, 15);
            this.label89.TabIndex = 58;
            this.label89.Text = "Credits";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.BackColor = System.Drawing.Color.AliceBlue;
            this.label90.Font = new System.Drawing.Font("Arial", 9F);
            this.label90.ForeColor = System.Drawing.Color.Black;
            this.label90.Location = new System.Drawing.Point(19, 57);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(65, 15);
            this.label90.TabIndex = 56;
            this.label90.Text = "Activations";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.BackColor = System.Drawing.Color.AliceBlue;
            this.label92.Font = new System.Drawing.Font("Arial", 9F);
            this.label92.ForeColor = System.Drawing.Color.Black;
            this.label92.Location = new System.Drawing.Point(19, 25);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(71, 15);
            this.label92.TabIndex = 52;
            this.label92.Text = "Online Sold";
            // 
            // LottoMasterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1274, 692);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.OnlineGroup);
            this.Controls.Add(this.ScratchWingroup);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.LottoGroup);
            this.Name = "LottoMasterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lotto Master Form";
            this.Load += new System.EventHandler(this.LottoMasterForm_Load);
            this.LottoGroup.ResumeLayout(false);
            this.LottoGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UnActivatedDocsDgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActivatedDocsDgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.OnlineGroup.ResumeLayout(false);
            this.OnlineGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScratchWinOnlineDocsDgv)).EndInit();
            this.ScratchWingroup.ResumeLayout(false);
            this.ScratchWingroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ScratchWinNormalDgv)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LottoWeeklyDgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox LottoGroup;
        private System.Windows.Forms.DataGridView ActivatedDocsDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.DataGridView UnActivatedDocsDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CashCheckBox;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.RichTextBox LottoActivatedRemarksTxt;
        private System.Windows.Forms.RichTextBox LottoUnActivatedRemakrsTxt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button LottoUnActivatedBtn;
        private System.Windows.Forms.TextBox LottoTotalUnActivatedTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox LottoActivatedTotalTxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox LottoUnactivatedSubstract150Txt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd150Txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening150Txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DateTimePicker DateCalender;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox OnlineGroup;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox LottoReportPayoutTxt;
        private System.Windows.Forms.TextBox LottoBackofficePayoutTxt;
        private System.Windows.Forms.TextBox LottoAdjustCPayout;
        private System.Windows.Forms.TextBox LottoEOS1PayoutTxt;
        private System.Windows.Forms.TextBox LottoShiftDiffPayoutTxt;
        private System.Windows.Forms.TextBox LottoEOS2PayoutTxt;
        private System.Windows.Forms.TextBox TSoldPayoutTxt;
        private System.Windows.Forms.TextBox LottoEOS3PayoutTxt;
        private System.Windows.Forms.DataGridView ScratchWinOnlineDocsDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox LottoOnlineReportTxt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.RichTextBox LottoScratchOnlineRemarksTxt;
        private System.Windows.Forms.TextBox LottoOnlineBackOfficetxt;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox LottoOnlineAdjustCanceltxt;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox LottoOnlineEOS1Txt;
        private System.Windows.Forms.TextBox LottoOnlineShiftsDiffTxt;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox LottoOnlineEOS2Txt;
        private System.Windows.Forms.TextBox LottoOnlineTSoldTxt;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox LottoOnlineEOS3Txt;
        private System.Windows.Forms.GroupBox ScratchWingroup;
        private System.Windows.Forms.TextBox LottoScratch1DiffTxt;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox LottoReportSold1Txt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox LottoScratchReportSold1Txt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox LottoScratchSold1Txt;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox LottoScratchActualClose1Txt;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox LottoScratchSubtotal1Txt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox LottoScratchAddQty1Txt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff1Txt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox LottoScratchActualOpen1txt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox LottoScratchOpen1Txt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract2Txt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd2Txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening2Txt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract3Txt;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd3Txt;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening3Txt;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract7Txt;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd7Txt;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening7Txt;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract5Txt;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd5Txt;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening5Txt;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract4Txt;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd4Txt;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening4Txt;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract30Txt;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd30Txt;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening30Txt;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract20Txt;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd20Txt;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening20Txt;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract10Txt;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd10Txt;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening10Txt;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox LottoActivatedSubstract30Txt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox LottoActivatedAdd30Txt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox LottoActivatedOpening30Txt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox LottoActivatedSubstract20Txt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox LottoActivatedAdd20Txt;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox LottoActivatedOpening20Txt;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox LottoActivatedSubstract10Txt;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox LottoActivatedAdd10Txt;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox LottoActivatedOpening10Txt;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox LottoActivatedSubstract7Txt;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox LottoActivatedAdd7Txt;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox LottoActivatedOpening7Txt;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox LottoActivatedSubstract5Txt;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox LottoActivatedAdd5Txt;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox LottoActivatedOpening5Txt;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox LottoActivatedSubstract4Txt;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.TextBox LottoActivatedAdd4Txt;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox LottoActivatedOpening4Txt;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox LottoActivatedSubstract3Txt;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox LottoActivatedAdd3Txt;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox LottoActivatedOpening3Txt;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox LottoActivatedSubstract2Txt;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.TextBox LottoActivatedAdd2Txt;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.TextBox LottoActivatedOpening2Txt;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.TextBox LottoActivatedSubstract150Txt;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox LottoActivatedAdd150Txt;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox LottoActivatedOpening150Txt;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.DataGridView LottoWeeklyDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.RichTextBox LottoWeeklyRemarksTxt;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TextBox AmountDueTxt;
        private System.Windows.Forms.TextBox RedemptionsTxt;
        private System.Windows.Forms.TextBox CreditsTxt;
        private System.Windows.Forms.TextBox RetailerBonusTxt;
        private System.Windows.Forms.TextBox ActivationTxt;
        private System.Windows.Forms.TextBox OnlineSoldtxt;
        private System.Windows.Forms.TextBox LottoScratch2DiffTxt;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox LottoReportSold2Txt;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox LottoScratchReportSold2Txt;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox LottoScratchSold2Txt;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.TextBox LottoScratchActualClose2Txt;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox LottoScratchSubtotal2Txt;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox LottoScratchAddQty2Txt;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff2Txt;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox LottoScratchActualOpen2txt;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TextBox LottoScratchOpen2Txt;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox LottoScratch3DiffTxt;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.TextBox LottoReportSold3Txt;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.TextBox LottoScratchReportSold3Txt;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TextBox LottoScratchSold3Txt;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.TextBox LottoScratchActualClose3Txt;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.TextBox LottoScratchSubtotal3Txt;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox LottoScratchAddQty3Txt;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff3Txt;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.TextBox LottoScratchActualOpen3txt;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox LottoScratchOpen3Txt;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.TextBox LottoScratch4DiffTxt;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox LottoReportSold4Txt;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.TextBox LottoScratchReportSold4Txt;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.TextBox LottoScratchSold4Txt;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.TextBox LottoScratchActualClose4Txt;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox LottoScratchSubtotal4Txt;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.TextBox LottoScratchAddQty4Txt;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff4Txt;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.TextBox LottoScratchActualOpen4txt;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.TextBox LottoScratchOpen4Txt;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.TextBox LottoScratchSubtotal5Txt;
        private System.Windows.Forms.TextBox LottoScratch5DiffTxt;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.TextBox LottoReportSold5Txt;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.TextBox LottoScratchReportSold5Txt;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.TextBox LottoScratchSold5Txt;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.TextBox LottoScratchActualClose5Txt;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox LottoScratchAddQty5Txt;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff5Txt;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.TextBox LottoScratchActualOpen5txt;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TextBox LottoScratchOpen5Txt;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.TextBox LottoScratchSubtotal7Txt;
        private System.Windows.Forms.TextBox LottoScratch7DiffTxt;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.TextBox LottoReportSold7Txt;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.TextBox LottoScratchReportSold7Txt;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.TextBox LottoScratchSold7Txt;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.TextBox LottoScratchActualClose7Txt;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.TextBox LottoScratchAddQty7Txt;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff7Txt;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.TextBox LottoScratchActualOpen7txt;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.TextBox LottoScratchOpen7Txt;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.TextBox LottoScratchSubtotal10Txt;
        private System.Windows.Forms.TextBox LottoScratch10DiffTxt;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.TextBox LottoReportSold10Txt;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.TextBox LottoScratchReportSold10Txt;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.TextBox LottoScratchSold10Txt;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.TextBox LottoScratchActualClose10Txt;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.TextBox LottoScratchAddQty10Txt;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff10Txt;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.TextBox LottoScratchActualOpen10txt;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.TextBox LottoScratchOpen10Txt;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.TextBox LottoScratchSubtotal20Txt;
        private System.Windows.Forms.TextBox LottoScratch20DiffTxt;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.TextBox LottoReportSold20Txt;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.TextBox LottoScratchReportSold20Txt;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.TextBox LottoScratchSold20Txt;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.TextBox LottoScratchActualClose20Txt;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.TextBox LottoScratchAddQty20Txt;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff20Txt;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.TextBox LottoScratchActualOpen20txt;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.TextBox LottoScratchOpen20Txt;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.TextBox LottoScratchSubtotal30Txt;
        private System.Windows.Forms.TextBox LottoScratch30DiffTxt;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.TextBox LottoReportSold30Txt;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.TextBox LottoScratchReportSold30Txt;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.TextBox LottoScratchSold30Txt;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.TextBox LottoScratchActualClose30Txt;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.TextBox LottoScratchAddQty30Txt;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.TextBox LottoScratchOpeningDiff30Txt;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.TextBox LottoScratchActualOpen30txt;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.TextBox LottoScratchOpen30Txt;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.TextBox LottoScratchWinTotalOpenTxt;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.TextBox LottoScratchWinTotalActualOpenTxt;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.TextBox LottoScratchWinTotalOpeningDiffTxt;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.TextBox LottoScratchWinTotalAdditionalQtyTxt;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.TextBox LottoScratchWinTotalActualCLoseTxt;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.TextBox LottoScratchWinTotalSoldTxt;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.TextBox LottoScratchWinTotalEODSoldTxt;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.TextBox LottoScratchWinTotalEODSoldDTxt;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.TextBox LottoScratchWinTotalDiffTxt;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.DataGridView ScratchWinNormalDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.RichTextBox LottoScratchRemarksTxt;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.TextBox LottoScrachWinTotalSubtotalTxt;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.TextBox LottoUnactivatedSubstract1100Txt;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd1100Txt;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening1100Txt;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.TextBox LottoUnActivatedSubtract30100Txt;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.TextBox LottoUnActivatedAdd30100Txt;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.TextBox LottoUnActivatedOpening30100Txt;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.TextBox LottoActivatedSubstract1100Txt;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.TextBox LottoActivatedAdd1100Txt;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.TextBox LottoActivatedOpening1100Txt;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.TextBox LottoActivatedSubstract30100Txt;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.TextBox LottoActivatedAdd30100Txt;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.TextBox LottoActivatedOpening30100Txt;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn150Txt;
        private System.Windows.Forms.Label label196;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn4Txt;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn3Txt;
        private System.Windows.Forms.Label label199;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn2Txt;
        private System.Windows.Forms.Label label198;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn1100Txt;
        private System.Windows.Forms.Label label197;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn30100Txt;
        private System.Windows.Forms.Label label206;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn3050Txt;
        private System.Windows.Forms.Label label205;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn20Txt;
        private System.Windows.Forms.Label label204;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn10Txt;
        private System.Windows.Forms.Label label203;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn7Txt;
        private System.Windows.Forms.Label label202;
        private System.Windows.Forms.TextBox LottoUnActivatedReturn5Txt;
        private System.Windows.Forms.Label label201;
        private System.Windows.Forms.TextBox LottoActivatedReturn30100Txt;
        private System.Windows.Forms.Label label207;
        private System.Windows.Forms.TextBox LottoActivatedReturn350Txt;
        private System.Windows.Forms.Label label208;
        private System.Windows.Forms.TextBox LottoActivatedReturn20Txt;
        private System.Windows.Forms.Label label209;
        private System.Windows.Forms.TextBox LottoActivatedReturn10Txt;
        private System.Windows.Forms.Label label210;
        private System.Windows.Forms.TextBox LottoActivatedReturn7Txt;
        private System.Windows.Forms.Label label211;
        private System.Windows.Forms.TextBox LottoActivatedReturn5Txt;
        private System.Windows.Forms.Label label212;
        private System.Windows.Forms.TextBox LottoActivatedReturn4Txt;
        private System.Windows.Forms.Label label213;
        private System.Windows.Forms.TextBox LottoActivatedReturn3Txt;
        private System.Windows.Forms.Label label214;
        private System.Windows.Forms.TextBox LottoActivatedReturn2Txt;
        private System.Windows.Forms.Label label215;
        private System.Windows.Forms.TextBox LottoActivatedReturn1100Txt;
        private System.Windows.Forms.Label label216;
        private System.Windows.Forms.TextBox LottoActivatedReturn150Txt;
        private System.Windows.Forms.Label label217;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing30100Txt;
        private System.Windows.Forms.Label label218;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing3050Txt;
        private System.Windows.Forms.Label label219;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing20Txt;
        private System.Windows.Forms.Label label220;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing10Txt;
        private System.Windows.Forms.Label label221;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing7Txt;
        private System.Windows.Forms.Label label222;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing5Txt;
        private System.Windows.Forms.Label label223;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing4Txt;
        private System.Windows.Forms.Label label224;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing3Txt;
        private System.Windows.Forms.Label label225;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing2Txt;
        private System.Windows.Forms.Label label226;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing1100Txt;
        private System.Windows.Forms.Label label227;
        private System.Windows.Forms.TextBox LottoUnActivatedClosing150Txt;
        private System.Windows.Forms.Label label228;
        private System.Windows.Forms.TextBox LottoActivatedClosing30100Txt;
        private System.Windows.Forms.Label label229;
        private System.Windows.Forms.TextBox LottoActivatedClosing3050Txt;
        private System.Windows.Forms.Label label230;
        private System.Windows.Forms.TextBox LottoActivatedClosing20Txt;
        private System.Windows.Forms.Label label231;
        private System.Windows.Forms.TextBox LottoActivatedClosing10Txt;
        private System.Windows.Forms.Label label232;
        private System.Windows.Forms.TextBox LottoActivatedClosing7Txt;
        private System.Windows.Forms.Label label233;
        private System.Windows.Forms.TextBox LottoActivatedClosing5Txt;
        private System.Windows.Forms.Label label234;
        private System.Windows.Forms.TextBox LottoActivatedClosing4Txt;
        private System.Windows.Forms.Label label235;
        private System.Windows.Forms.TextBox LottoActivatedClosing3Txt;
        private System.Windows.Forms.Label label236;
        private System.Windows.Forms.TextBox LottoActivatedClosing2Txt;
        private System.Windows.Forms.Label label237;
        private System.Windows.Forms.TextBox LottoActivatedClosing1100Txt;
        private System.Windows.Forms.Label label238;
        private System.Windows.Forms.TextBox LottoActivatedClosing150Txt;
        private System.Windows.Forms.Label label239;
        private System.Windows.Forms.TextBox TotalActivatedInvTxt;
        private System.Windows.Forms.Label label240;
    }
}