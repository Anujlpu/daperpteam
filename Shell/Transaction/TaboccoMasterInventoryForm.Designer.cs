﻿namespace Shell.Transaction
{
    partial class TaboccoMasterInventoryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.TobaccoGroup = new System.Windows.Forms.GroupBox();
            this.Tobacco10SReasonCmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Tobacco8SReasonCmb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TobaccoSite10s = new System.Windows.Forms.ComboBox();
            this.TobaccoSite8s = new System.Windows.Forms.ComboBox();
            this.TobaccoVendor10sLabel = new System.Windows.Forms.Label();
            this.TobaccoVendor8sLabel = new System.Windows.Forms.Label();
            this.TobaccoTotalPacksTxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TobaccoRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label77 = new System.Windows.Forms.Label();
            this.TobaccoSubstract10STxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TobaccoAdd10STxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TobaccoBalance10STxt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TobaccoSubstract8STxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TobaccoAdd8STxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TaboccoBalance8Stxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TobaccoDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn10 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.TobaccoLabelDocs = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.ViewBtn = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.DateCalender = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.ChewgroupBox = new System.Windows.Forms.GroupBox();
            this.ChewReasonCombo = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ChewSiteCombobox = new System.Windows.Forms.ComboBox();
            this.ChewSiteLabel = new System.Windows.Forms.Label();
            this.ChewRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ChewSubstractTxt = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.ChewAddTxt = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.ChewBalanceTxt = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ChewDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Cigar5sReasonCombo = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.CigarSite5sCmb = new System.Windows.Forms.ComboBox();
            this.CigarSite5sLabel = new System.Windows.Forms.Label();
            this.Cigar8sReasonCombo = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.CigarSite8sCmb = new System.Windows.Forms.ComboBox();
            this.CigarSite8sLabel = new System.Windows.Forms.Label();
            this.Cigar10sReasonCombo = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.CigarSite10sCmb = new System.Windows.Forms.ComboBox();
            this.CigarSite10sLabel = new System.Windows.Forms.Label();
            this.CigarSubstract5sTxt = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.CigarAdd5sTxt = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.CigarBalance5sTxt = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.CigarSubstract8sTxt = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.CigarAdd8sTxt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.CigarBalance8sTxt = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.CigarTotalPacksTxt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.CigarRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.CigarSubstract10sTxt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.CigarAdd10sTxt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.CigarBalance10sTxt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.CigarDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn2 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ECigarReasonCmb = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.ECigarSiteCmb = new System.Windows.Forms.ComboBox();
            this.ECigarSiteLabel = new System.Windows.Forms.Label();
            this.ECigarRemarks = new System.Windows.Forms.RichTextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.ECigarSubstractTxt = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.ECigarAddTxt = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.ECigarBalanceTxt = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.ECigarDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn3 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.SinglesReasonCmb = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.SinglesSiteCmb = new System.Windows.Forms.ComboBox();
            this.SinglesSiteLabel = new System.Windows.Forms.Label();
            this.SingleRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.SingleSubstractTxt = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.SingleAddTxt = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.SinglesBalanceTxt = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.SinglesDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn4 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label40 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.MilkReason4LCmb = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.MilkSite4LCmb = new System.Windows.Forms.ComboBox();
            this.MilkSite4LLabel = new System.Windows.Forms.Label();
            this.MilkSubtract4LTxt = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.MilkAdd4LTxt = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.MilkBalance4LTxt = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.MilkReason2LCmb = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.MilkSite2LCmb = new System.Windows.Forms.ComboBox();
            this.MilkSite2LLabel = new System.Windows.Forms.Label();
            this.MilkSubtract2LTxt = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.MilkAdd2LTxt = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.MilkBalance2LTxt = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.MilkReason1LCmb = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this.MilkSite1LCmb = new System.Windows.Forms.ComboBox();
            this.MilkSite1LLabel = new System.Windows.Forms.Label();
            this.MilkSubtract1LTxt = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.MilkAdd1LTxt = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.MilkBalance1LTxt = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.MilkReason473mlCmb = new System.Windows.Forms.ComboBox();
            this.label51 = new System.Windows.Forms.Label();
            this.MilkSite473mlCmb = new System.Windows.Forms.ComboBox();
            this.MilkSite473Label = new System.Windows.Forms.Label();
            this.MilkSubtract473mlTxt = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.MilkAdd473mlTxt = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.MilkBalance473mlTxt = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.MilkReason310mlCmb = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.MilkSite310mlCmb = new System.Windows.Forms.ComboBox();
            this.MilkSite310Label = new System.Windows.Forms.Label();
            this.MilkRemarksTxt = new System.Windows.Forms.RichTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.MilkSubtract310mlTxt = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.MilkAdd310mlTxt = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.MilkBalance310mlTxt = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.MilkDgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn5 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label50 = new System.Windows.Forms.Label();
            this.TobaccoGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TobaccoDgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.ChewgroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChewDgv)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CigarDgv)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ECigarDgv)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SinglesDgv)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MilkDgv)).BeginInit();
            this.SuspendLayout();
            // 
            // TobaccoGroup
            // 
            this.TobaccoGroup.Controls.Add(this.Tobacco10SReasonCmb);
            this.TobaccoGroup.Controls.Add(this.label2);
            this.TobaccoGroup.Controls.Add(this.Tobacco8SReasonCmb);
            this.TobaccoGroup.Controls.Add(this.label1);
            this.TobaccoGroup.Controls.Add(this.TobaccoSite10s);
            this.TobaccoGroup.Controls.Add(this.TobaccoSite8s);
            this.TobaccoGroup.Controls.Add(this.TobaccoVendor10sLabel);
            this.TobaccoGroup.Controls.Add(this.TobaccoVendor8sLabel);
            this.TobaccoGroup.Controls.Add(this.TobaccoTotalPacksTxt);
            this.TobaccoGroup.Controls.Add(this.label10);
            this.TobaccoGroup.Controls.Add(this.TobaccoRemarksTxt);
            this.TobaccoGroup.Controls.Add(this.label77);
            this.TobaccoGroup.Controls.Add(this.TobaccoSubstract10STxt);
            this.TobaccoGroup.Controls.Add(this.label7);
            this.TobaccoGroup.Controls.Add(this.TobaccoAdd10STxt);
            this.TobaccoGroup.Controls.Add(this.label8);
            this.TobaccoGroup.Controls.Add(this.TobaccoBalance10STxt);
            this.TobaccoGroup.Controls.Add(this.label9);
            this.TobaccoGroup.Controls.Add(this.TobaccoSubstract8STxt);
            this.TobaccoGroup.Controls.Add(this.label6);
            this.TobaccoGroup.Controls.Add(this.TobaccoAdd8STxt);
            this.TobaccoGroup.Controls.Add(this.label5);
            this.TobaccoGroup.Controls.Add(this.TaboccoBalance8Stxt);
            this.TobaccoGroup.Controls.Add(this.label4);
            this.TobaccoGroup.Controls.Add(this.TobaccoDgv);
            this.TobaccoGroup.Controls.Add(this.TobaccoLabelDocs);
            this.TobaccoGroup.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TobaccoGroup.Location = new System.Drawing.Point(31, 201);
            this.TobaccoGroup.Name = "TobaccoGroup";
            this.TobaccoGroup.Size = new System.Drawing.Size(1260, 255);
            this.TobaccoGroup.TabIndex = 252;
            this.TobaccoGroup.TabStop = false;
            this.TobaccoGroup.Text = "Tobacco";
            // 
            // Tobacco10SReasonCmb
            // 
            this.Tobacco10SReasonCmb.Enabled = false;
            this.Tobacco10SReasonCmb.FormattingEnabled = true;
            this.Tobacco10SReasonCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.Tobacco10SReasonCmb.Location = new System.Drawing.Point(590, 80);
            this.Tobacco10SReasonCmb.Name = "Tobacco10SReasonCmb";
            this.Tobacco10SReasonCmb.Size = new System.Drawing.Size(140, 23);
            this.Tobacco10SReasonCmb.TabIndex = 265;
            this.Tobacco10SReasonCmb.SelectedIndexChanged += new System.EventHandler(this.Tobacco10SReasonCmb_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(519, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 15);
            this.label2.TabIndex = 264;
            this.label2.Text = "Reason";
            // 
            // Tobacco8SReasonCmb
            // 
            this.Tobacco8SReasonCmb.Enabled = false;
            this.Tobacco8SReasonCmb.FormattingEnabled = true;
            this.Tobacco8SReasonCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.Tobacco8SReasonCmb.Location = new System.Drawing.Point(590, 48);
            this.Tobacco8SReasonCmb.Name = "Tobacco8SReasonCmb";
            this.Tobacco8SReasonCmb.Size = new System.Drawing.Size(140, 23);
            this.Tobacco8SReasonCmb.TabIndex = 263;
            this.Tobacco8SReasonCmb.SelectedIndexChanged += new System.EventHandler(this.Tobacco8SReasonCmb_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(519, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.TabIndex = 262;
            this.label1.Text = "Reason";
            // 
            // TobaccoSite10s
            // 
            this.TobaccoSite10s.Enabled = false;
            this.TobaccoSite10s.FormattingEnabled = true;
            this.TobaccoSite10s.Location = new System.Drawing.Point(806, 84);
            this.TobaccoSite10s.Name = "TobaccoSite10s";
            this.TobaccoSite10s.Size = new System.Drawing.Size(140, 23);
            this.TobaccoSite10s.TabIndex = 261;
            // 
            // TobaccoSite8s
            // 
            this.TobaccoSite8s.Enabled = false;
            this.TobaccoSite8s.FormattingEnabled = true;
            this.TobaccoSite8s.Location = new System.Drawing.Point(806, 50);
            this.TobaccoSite8s.Name = "TobaccoSite8s";
            this.TobaccoSite8s.Size = new System.Drawing.Size(139, 23);
            this.TobaccoSite8s.TabIndex = 260;
            // 
            // TobaccoVendor10sLabel
            // 
            this.TobaccoVendor10sLabel.AutoSize = true;
            this.TobaccoVendor10sLabel.Location = new System.Drawing.Point(751, 88);
            this.TobaccoVendor10sLabel.Name = "TobaccoVendor10sLabel";
            this.TobaccoVendor10sLabel.Size = new System.Drawing.Size(28, 15);
            this.TobaccoVendor10sLabel.TabIndex = 259;
            this.TobaccoVendor10sLabel.Text = "Site";
            // 
            // TobaccoVendor8sLabel
            // 
            this.TobaccoVendor8sLabel.AutoSize = true;
            this.TobaccoVendor8sLabel.Location = new System.Drawing.Point(751, 52);
            this.TobaccoVendor8sLabel.Name = "TobaccoVendor8sLabel";
            this.TobaccoVendor8sLabel.Size = new System.Drawing.Size(28, 15);
            this.TobaccoVendor8sLabel.TabIndex = 258;
            this.TobaccoVendor8sLabel.Text = "Site";
            // 
            // TobaccoTotalPacksTxt
            // 
            this.TobaccoTotalPacksTxt.Location = new System.Drawing.Point(147, 110);
            this.TobaccoTotalPacksTxt.Name = "TobaccoTotalPacksTxt";
            this.TobaccoTotalPacksTxt.ReadOnly = true;
            this.TobaccoTotalPacksTxt.Size = new System.Drawing.Size(53, 21);
            this.TobaccoTotalPacksTxt.TabIndex = 257;
            this.TobaccoTotalPacksTxt.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(55, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 15);
            this.label10.TabIndex = 256;
            this.label10.Text = "Total Packs";
            // 
            // TobaccoRemarksTxt
            // 
            this.TobaccoRemarksTxt.Location = new System.Drawing.Point(147, 140);
            this.TobaccoRemarksTxt.Name = "TobaccoRemarksTxt";
            this.TobaccoRemarksTxt.Size = new System.Drawing.Size(356, 92);
            this.TobaccoRemarksTxt.TabIndex = 255;
            this.TobaccoRemarksTxt.Text = "";
            this.TobaccoRemarksTxt.Leave += new System.EventHandler(this.TobaccoAdd8STxt_Leave);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(56, 143);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(57, 15);
            this.label77.TabIndex = 254;
            this.label77.Text = "Remarks";
            // 
            // TobaccoSubstract10STxt
            // 
            this.TobaccoSubstract10STxt.Location = new System.Drawing.Point(451, 80);
            this.TobaccoSubstract10STxt.Name = "TobaccoSubstract10STxt";
            this.TobaccoSubstract10STxt.Size = new System.Drawing.Size(51, 21);
            this.TobaccoSubstract10STxt.TabIndex = 252;
            this.TobaccoSubstract10STxt.Text = "0";
            this.TobaccoSubstract10STxt.Leave += new System.EventHandler(this.TobaccoAdd8STxt_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(353, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 15);
            this.label7.TabIndex = 251;
            this.label7.Text = "Subtract 10\'s";
            // 
            // TobaccoAdd10STxt
            // 
            this.TobaccoAdd10STxt.Location = new System.Drawing.Point(295, 80);
            this.TobaccoAdd10STxt.Name = "TobaccoAdd10STxt";
            this.TobaccoAdd10STxt.Size = new System.Drawing.Size(51, 21);
            this.TobaccoAdd10STxt.TabIndex = 250;
            this.TobaccoAdd10STxt.Text = "0";
            this.TobaccoAdd10STxt.Leave += new System.EventHandler(this.TobaccoAdd8STxt_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(227, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(54, 15);
            this.label8.TabIndex = 249;
            this.label8.Text = "Add 10\'s";
            // 
            // TobaccoBalance10STxt
            // 
            this.TobaccoBalance10STxt.Location = new System.Drawing.Point(148, 80);
            this.TobaccoBalance10STxt.Name = "TobaccoBalance10STxt";
            this.TobaccoBalance10STxt.ReadOnly = true;
            this.TobaccoBalance10STxt.Size = new System.Drawing.Size(53, 21);
            this.TobaccoBalance10STxt.TabIndex = 248;
            this.TobaccoBalance10STxt.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(56, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 15);
            this.label9.TabIndex = 247;
            this.label9.Text = "Balance 10\'s";
            // 
            // TobaccoSubstract8STxt
            // 
            this.TobaccoSubstract8STxt.Location = new System.Drawing.Point(453, 50);
            this.TobaccoSubstract8STxt.Name = "TobaccoSubstract8STxt";
            this.TobaccoSubstract8STxt.Size = new System.Drawing.Size(51, 21);
            this.TobaccoSubstract8STxt.TabIndex = 245;
            this.TobaccoSubstract8STxt.Text = "0";
            this.TobaccoSubstract8STxt.Leave += new System.EventHandler(this.TobaccoAdd8STxt_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(355, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 15);
            this.label6.TabIndex = 244;
            this.label6.Text = "Subtract  8\'s";
            // 
            // TobaccoAdd8STxt
            // 
            this.TobaccoAdd8STxt.Location = new System.Drawing.Point(296, 50);
            this.TobaccoAdd8STxt.Name = "TobaccoAdd8STxt";
            this.TobaccoAdd8STxt.Size = new System.Drawing.Size(51, 21);
            this.TobaccoAdd8STxt.TabIndex = 243;
            this.TobaccoAdd8STxt.Text = "0";
            this.TobaccoAdd8STxt.Leave += new System.EventHandler(this.TobaccoAdd8STxt_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(229, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 15);
            this.label5.TabIndex = 242;
            this.label5.Text = "Add  8\'s";
            // 
            // TaboccoBalance8Stxt
            // 
            this.TaboccoBalance8Stxt.Location = new System.Drawing.Point(149, 50);
            this.TaboccoBalance8Stxt.Name = "TaboccoBalance8Stxt";
            this.TaboccoBalance8Stxt.ReadOnly = true;
            this.TaboccoBalance8Stxt.Size = new System.Drawing.Size(53, 21);
            this.TaboccoBalance8Stxt.TabIndex = 241;
            this.TaboccoBalance8Stxt.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 15);
            this.label4.TabIndex = 240;
            this.label4.Text = "Balance 8\'s";
            // 
            // TobaccoDgv
            // 
            this.TobaccoDgv.AllowDrop = true;
            this.TobaccoDgv.AllowUserToAddRows = false;
            this.TobaccoDgv.AllowUserToDeleteRows = false;
            this.TobaccoDgv.AllowUserToResizeColumns = false;
            this.TobaccoDgv.AllowUserToResizeRows = false;
            this.TobaccoDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.TobaccoDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TobaccoDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TobaccoDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TobaccoDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn10});
            this.TobaccoDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.TobaccoDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.TobaccoDgv.Location = new System.Drawing.Point(967, 48);
            this.TobaccoDgv.MultiSelect = false;
            this.TobaccoDgv.Name = "TobaccoDgv";
            this.TobaccoDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TobaccoDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.TobaccoDgv.RowHeadersVisible = false;
            this.TobaccoDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.TobaccoDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TobaccoDgv.Size = new System.Drawing.Size(229, 110);
            this.TobaccoDgv.TabIndex = 238;
            // 
            // dataGridViewCheckBoxColumn10
            // 
            this.dataGridViewCheckBoxColumn10.HeaderText = "";
            this.dataGridViewCheckBoxColumn10.Name = "dataGridViewCheckBoxColumn10";
            this.dataGridViewCheckBoxColumn10.Width = 5;
            // 
            // TobaccoLabelDocs
            // 
            this.TobaccoLabelDocs.AutoSize = true;
            this.TobaccoLabelDocs.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TobaccoLabelDocs.ForeColor = System.Drawing.Color.Black;
            this.TobaccoLabelDocs.Location = new System.Drawing.Point(961, 18);
            this.TobaccoLabelDocs.Name = "TobaccoLabelDocs";
            this.TobaccoLabelDocs.Size = new System.Drawing.Size(85, 15);
            this.TobaccoLabelDocs.TabIndex = 239;
            this.TobaccoLabelDocs.Text = "Tobacco Docs";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SaveBtn);
            this.groupBox1.Controls.Add(this.ViewBtn);
            this.groupBox1.Controls.Add(this.btnExit);
            this.groupBox1.Controls.Add(this.DateCalender);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(31, 14);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1260, 141);
            this.groupBox1.TabIndex = 253;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Master";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // SaveBtn
            // 
            this.SaveBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SaveBtn.FlatAppearance.BorderSize = 2;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.Location = new System.Drawing.Point(281, 54);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(103, 37);
            this.SaveBtn.TabIndex = 233;
            this.SaveBtn.Text = "&Save";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // ViewBtn
            // 
            this.ViewBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ViewBtn.FlatAppearance.BorderSize = 2;
            this.ViewBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewBtn.Location = new System.Drawing.Point(150, 54);
            this.ViewBtn.Name = "ViewBtn";
            this.ViewBtn.Size = new System.Drawing.Size(105, 37);
            this.ViewBtn.TabIndex = 232;
            this.ViewBtn.Text = "&View";
            this.ViewBtn.UseVisualStyleBackColor = true;
            this.ViewBtn.Click += new System.EventHandler(this.ViewBtn_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(401, 54);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(103, 37);
            this.btnExit.TabIndex = 231;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // DateCalender
            // 
            this.DateCalender.CustomFormat = "MMM/dd/yyyy";
            this.DateCalender.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateCalender.Location = new System.Drawing.Point(153, 23);
            this.DateCalender.Name = "DateCalender";
            this.DateCalender.Size = new System.Drawing.Size(235, 21);
            this.DateCalender.TabIndex = 219;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(23, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 218;
            this.label3.Text = "Date";
            // 
            // ChewgroupBox
            // 
            this.ChewgroupBox.Controls.Add(this.ChewReasonCombo);
            this.ChewgroupBox.Controls.Add(this.label11);
            this.ChewgroupBox.Controls.Add(this.ChewSiteCombobox);
            this.ChewgroupBox.Controls.Add(this.ChewSiteLabel);
            this.ChewgroupBox.Controls.Add(this.ChewRemarksTxt);
            this.ChewgroupBox.Controls.Add(this.label12);
            this.ChewgroupBox.Controls.Add(this.ChewSubstractTxt);
            this.ChewgroupBox.Controls.Add(this.label18);
            this.ChewgroupBox.Controls.Add(this.ChewAddTxt);
            this.ChewgroupBox.Controls.Add(this.label19);
            this.ChewgroupBox.Controls.Add(this.ChewBalanceTxt);
            this.ChewgroupBox.Controls.Add(this.label20);
            this.ChewgroupBox.Controls.Add(this.ChewDgv);
            this.ChewgroupBox.Controls.Add(this.label21);
            this.ChewgroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChewgroupBox.Location = new System.Drawing.Point(31, 463);
            this.ChewgroupBox.Name = "ChewgroupBox";
            this.ChewgroupBox.Size = new System.Drawing.Size(1260, 228);
            this.ChewgroupBox.TabIndex = 258;
            this.ChewgroupBox.TabStop = false;
            this.ChewgroupBox.Text = "Chew";
            // 
            // ChewReasonCombo
            // 
            this.ChewReasonCombo.Enabled = false;
            this.ChewReasonCombo.FormattingEnabled = true;
            this.ChewReasonCombo.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.ChewReasonCombo.Location = new System.Drawing.Point(590, 44);
            this.ChewReasonCombo.Name = "ChewReasonCombo";
            this.ChewReasonCombo.Size = new System.Drawing.Size(140, 23);
            this.ChewReasonCombo.TabIndex = 267;
            this.ChewReasonCombo.SelectedIndexChanged += new System.EventHandler(this.ChewReasonCombo_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(519, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 15);
            this.label11.TabIndex = 266;
            this.label11.Text = "Reason";
            // 
            // ChewSiteCombobox
            // 
            this.ChewSiteCombobox.Enabled = false;
            this.ChewSiteCombobox.FormattingEnabled = true;
            this.ChewSiteCombobox.Location = new System.Drawing.Point(806, 45);
            this.ChewSiteCombobox.Name = "ChewSiteCombobox";
            this.ChewSiteCombobox.Size = new System.Drawing.Size(139, 23);
            this.ChewSiteCombobox.TabIndex = 265;
            // 
            // ChewSiteLabel
            // 
            this.ChewSiteLabel.AutoSize = true;
            this.ChewSiteLabel.Location = new System.Drawing.Point(751, 47);
            this.ChewSiteLabel.Name = "ChewSiteLabel";
            this.ChewSiteLabel.Size = new System.Drawing.Size(28, 15);
            this.ChewSiteLabel.TabIndex = 264;
            this.ChewSiteLabel.Text = "Site";
            // 
            // ChewRemarksTxt
            // 
            this.ChewRemarksTxt.Location = new System.Drawing.Point(147, 99);
            this.ChewRemarksTxt.Name = "ChewRemarksTxt";
            this.ChewRemarksTxt.Size = new System.Drawing.Size(355, 108);
            this.ChewRemarksTxt.TabIndex = 255;
            this.ChewRemarksTxt.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(55, 130);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 15);
            this.label12.TabIndex = 254;
            this.label12.Text = "Remarks";
            // 
            // ChewSubstractTxt
            // 
            this.ChewSubstractTxt.Location = new System.Drawing.Point(453, 50);
            this.ChewSubstractTxt.Name = "ChewSubstractTxt";
            this.ChewSubstractTxt.Size = new System.Drawing.Size(51, 21);
            this.ChewSubstractTxt.TabIndex = 245;
            this.ChewSubstractTxt.Text = "0";
            this.ChewSubstractTxt.Leave += new System.EventHandler(this.ChewAddTxt_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(355, 53);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 15);
            this.label18.TabIndex = 244;
            this.label18.Text = "Subtract";
            // 
            // ChewAddTxt
            // 
            this.ChewAddTxt.Location = new System.Drawing.Point(296, 50);
            this.ChewAddTxt.Name = "ChewAddTxt";
            this.ChewAddTxt.Size = new System.Drawing.Size(51, 21);
            this.ChewAddTxt.TabIndex = 243;
            this.ChewAddTxt.Text = "0";
            this.ChewAddTxt.Leave += new System.EventHandler(this.ChewAddTxt_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(229, 53);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(28, 15);
            this.label19.TabIndex = 242;
            this.label19.Text = "Add";
            // 
            // ChewBalanceTxt
            // 
            this.ChewBalanceTxt.Location = new System.Drawing.Point(149, 50);
            this.ChewBalanceTxt.Name = "ChewBalanceTxt";
            this.ChewBalanceTxt.ReadOnly = true;
            this.ChewBalanceTxt.Size = new System.Drawing.Size(53, 21);
            this.ChewBalanceTxt.TabIndex = 241;
            this.ChewBalanceTxt.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(57, 53);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 15);
            this.label20.TabIndex = 240;
            this.label20.Text = "Balance";
            // 
            // ChewDgv
            // 
            this.ChewDgv.AllowDrop = true;
            this.ChewDgv.AllowUserToAddRows = false;
            this.ChewDgv.AllowUserToDeleteRows = false;
            this.ChewDgv.AllowUserToResizeColumns = false;
            this.ChewDgv.AllowUserToResizeRows = false;
            this.ChewDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ChewDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChewDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.ChewDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ChewDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1});
            this.ChewDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ChewDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.ChewDgv.Location = new System.Drawing.Point(967, 45);
            this.ChewDgv.MultiSelect = false;
            this.ChewDgv.Name = "ChewDgv";
            this.ChewDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ChewDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.ChewDgv.RowHeadersVisible = false;
            this.ChewDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ChewDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ChewDgv.Size = new System.Drawing.Size(229, 110);
            this.ChewDgv.TabIndex = 238;
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 5;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(961, 18);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 15);
            this.label21.TabIndex = 239;
            this.label21.Text = "Chew Docs";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Cigar5sReasonCombo);
            this.groupBox2.Controls.Add(this.label43);
            this.groupBox2.Controls.Add(this.CigarSite5sCmb);
            this.groupBox2.Controls.Add(this.CigarSite5sLabel);
            this.groupBox2.Controls.Add(this.Cigar8sReasonCombo);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.CigarSite8sCmb);
            this.groupBox2.Controls.Add(this.CigarSite8sLabel);
            this.groupBox2.Controls.Add(this.Cigar10sReasonCombo);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.CigarSite10sCmb);
            this.groupBox2.Controls.Add(this.CigarSite10sLabel);
            this.groupBox2.Controls.Add(this.CigarSubstract5sTxt);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.CigarAdd5sTxt);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.CigarBalance5sTxt);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.CigarSubstract8sTxt);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.CigarAdd8sTxt);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.CigarBalance8sTxt);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.CigarTotalPacksTxt);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.CigarRemarksTxt);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.CigarSubstract10sTxt);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.CigarAdd10sTxt);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.CigarBalance10sTxt);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.CigarDgv);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(30, 698);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1260, 255);
            this.groupBox2.TabIndex = 259;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cigar";
            // 
            // Cigar5sReasonCombo
            // 
            this.Cigar5sReasonCombo.Enabled = false;
            this.Cigar5sReasonCombo.FormattingEnabled = true;
            this.Cigar5sReasonCombo.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.Cigar5sReasonCombo.Location = new System.Drawing.Point(591, 125);
            this.Cigar5sReasonCombo.Name = "Cigar5sReasonCombo";
            this.Cigar5sReasonCombo.Size = new System.Drawing.Size(140, 23);
            this.Cigar5sReasonCombo.TabIndex = 281;
            this.Cigar5sReasonCombo.SelectedIndexChanged += new System.EventHandler(this.Cigar5sReasonCombo_SelectedIndexChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(520, 129);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(50, 15);
            this.label43.TabIndex = 280;
            this.label43.Text = "Reason";
            // 
            // CigarSite5sCmb
            // 
            this.CigarSite5sCmb.Enabled = false;
            this.CigarSite5sCmb.FormattingEnabled = true;
            this.CigarSite5sCmb.Location = new System.Drawing.Point(807, 126);
            this.CigarSite5sCmb.Name = "CigarSite5sCmb";
            this.CigarSite5sCmb.Size = new System.Drawing.Size(139, 23);
            this.CigarSite5sCmb.TabIndex = 279;
            // 
            // CigarSite5sLabel
            // 
            this.CigarSite5sLabel.AutoSize = true;
            this.CigarSite5sLabel.Location = new System.Drawing.Point(752, 128);
            this.CigarSite5sLabel.Name = "CigarSite5sLabel";
            this.CigarSite5sLabel.Size = new System.Drawing.Size(28, 15);
            this.CigarSite5sLabel.TabIndex = 278;
            this.CigarSite5sLabel.Text = "Site";
            // 
            // Cigar8sReasonCombo
            // 
            this.Cigar8sReasonCombo.Enabled = false;
            this.Cigar8sReasonCombo.FormattingEnabled = true;
            this.Cigar8sReasonCombo.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.Cigar8sReasonCombo.Location = new System.Drawing.Point(591, 92);
            this.Cigar8sReasonCombo.Name = "Cigar8sReasonCombo";
            this.Cigar8sReasonCombo.Size = new System.Drawing.Size(140, 23);
            this.Cigar8sReasonCombo.TabIndex = 277;
            this.Cigar8sReasonCombo.SelectedIndexChanged += new System.EventHandler(this.Cigar8sReasonCombo_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(520, 97);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(50, 15);
            this.label41.TabIndex = 276;
            this.label41.Text = "Reason";
            // 
            // CigarSite8sCmb
            // 
            this.CigarSite8sCmb.Enabled = false;
            this.CigarSite8sCmb.FormattingEnabled = true;
            this.CigarSite8sCmb.Location = new System.Drawing.Point(807, 93);
            this.CigarSite8sCmb.Name = "CigarSite8sCmb";
            this.CigarSite8sCmb.Size = new System.Drawing.Size(139, 23);
            this.CigarSite8sCmb.TabIndex = 275;
            // 
            // CigarSite8sLabel
            // 
            this.CigarSite8sLabel.AutoSize = true;
            this.CigarSite8sLabel.Location = new System.Drawing.Point(752, 96);
            this.CigarSite8sLabel.Name = "CigarSite8sLabel";
            this.CigarSite8sLabel.Size = new System.Drawing.Size(28, 15);
            this.CigarSite8sLabel.TabIndex = 274;
            this.CigarSite8sLabel.Text = "Site";
            // 
            // Cigar10sReasonCombo
            // 
            this.Cigar10sReasonCombo.Enabled = false;
            this.Cigar10sReasonCombo.FormattingEnabled = true;
            this.Cigar10sReasonCombo.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.Cigar10sReasonCombo.Location = new System.Drawing.Point(591, 50);
            this.Cigar10sReasonCombo.Name = "Cigar10sReasonCombo";
            this.Cigar10sReasonCombo.Size = new System.Drawing.Size(140, 23);
            this.Cigar10sReasonCombo.TabIndex = 273;
            this.Cigar10sReasonCombo.SelectedIndexChanged += new System.EventHandler(this.Cigar10sReasonCombo_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(520, 54);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(50, 15);
            this.label29.TabIndex = 272;
            this.label29.Text = "Reason";
            // 
            // CigarSite10sCmb
            // 
            this.CigarSite10sCmb.Enabled = false;
            this.CigarSite10sCmb.FormattingEnabled = true;
            this.CigarSite10sCmb.Location = new System.Drawing.Point(807, 51);
            this.CigarSite10sCmb.Name = "CigarSite10sCmb";
            this.CigarSite10sCmb.Size = new System.Drawing.Size(139, 23);
            this.CigarSite10sCmb.TabIndex = 271;
            // 
            // CigarSite10sLabel
            // 
            this.CigarSite10sLabel.AutoSize = true;
            this.CigarSite10sLabel.Location = new System.Drawing.Point(752, 53);
            this.CigarSite10sLabel.Name = "CigarSite10sLabel";
            this.CigarSite10sLabel.Size = new System.Drawing.Size(28, 15);
            this.CigarSite10sLabel.TabIndex = 270;
            this.CigarSite10sLabel.Text = "Site";
            // 
            // CigarSubstract5sTxt
            // 
            this.CigarSubstract5sTxt.Location = new System.Drawing.Point(453, 119);
            this.CigarSubstract5sTxt.Name = "CigarSubstract5sTxt";
            this.CigarSubstract5sTxt.Size = new System.Drawing.Size(51, 21);
            this.CigarSubstract5sTxt.TabIndex = 269;
            this.CigarSubstract5sTxt.Text = "0";
            this.CigarSubstract5sTxt.Leave += new System.EventHandler(this.CigarAdd10sTxt_Leave);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(355, 122);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 15);
            this.label26.TabIndex = 268;
            this.label26.Text = "Subtract 5\'s";
            // 
            // CigarAdd5sTxt
            // 
            this.CigarAdd5sTxt.Location = new System.Drawing.Point(296, 119);
            this.CigarAdd5sTxt.Name = "CigarAdd5sTxt";
            this.CigarAdd5sTxt.Size = new System.Drawing.Size(51, 21);
            this.CigarAdd5sTxt.TabIndex = 267;
            this.CigarAdd5sTxt.Text = "0";
            this.CigarAdd5sTxt.Leave += new System.EventHandler(this.CigarAdd10sTxt_Leave);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(229, 122);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 15);
            this.label27.TabIndex = 266;
            this.label27.Text = "Add 5\'s";
            // 
            // CigarBalance5sTxt
            // 
            this.CigarBalance5sTxt.Location = new System.Drawing.Point(149, 119);
            this.CigarBalance5sTxt.Name = "CigarBalance5sTxt";
            this.CigarBalance5sTxt.ReadOnly = true;
            this.CigarBalance5sTxt.Size = new System.Drawing.Size(53, 21);
            this.CigarBalance5sTxt.TabIndex = 265;
            this.CigarBalance5sTxt.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(57, 122);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 15);
            this.label28.TabIndex = 264;
            this.label28.Text = "Balance 5\'s";
            // 
            // CigarSubstract8sTxt
            // 
            this.CigarSubstract8sTxt.Location = new System.Drawing.Point(453, 89);
            this.CigarSubstract8sTxt.Name = "CigarSubstract8sTxt";
            this.CigarSubstract8sTxt.Size = new System.Drawing.Size(51, 21);
            this.CigarSubstract8sTxt.TabIndex = 263;
            this.CigarSubstract8sTxt.Text = "0";
            this.CigarSubstract8sTxt.Leave += new System.EventHandler(this.CigarAdd10sTxt_Leave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(355, 92);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 15);
            this.label23.TabIndex = 262;
            this.label23.Text = "Subtract 8\'s";
            // 
            // CigarAdd8sTxt
            // 
            this.CigarAdd8sTxt.Location = new System.Drawing.Point(296, 89);
            this.CigarAdd8sTxt.Name = "CigarAdd8sTxt";
            this.CigarAdd8sTxt.Size = new System.Drawing.Size(51, 21);
            this.CigarAdd8sTxt.TabIndex = 261;
            this.CigarAdd8sTxt.Text = "0";
            this.CigarAdd8sTxt.Leave += new System.EventHandler(this.CigarAdd10sTxt_Leave);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(229, 92);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 15);
            this.label24.TabIndex = 260;
            this.label24.Text = "Add 8\'s";
            // 
            // CigarBalance8sTxt
            // 
            this.CigarBalance8sTxt.Location = new System.Drawing.Point(149, 89);
            this.CigarBalance8sTxt.Name = "CigarBalance8sTxt";
            this.CigarBalance8sTxt.ReadOnly = true;
            this.CigarBalance8sTxt.Size = new System.Drawing.Size(53, 21);
            this.CigarBalance8sTxt.TabIndex = 259;
            this.CigarBalance8sTxt.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(57, 92);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 15);
            this.label25.TabIndex = 258;
            this.label25.Text = "Balance 8\'s";
            // 
            // CigarTotalPacksTxt
            // 
            this.CigarTotalPacksTxt.Location = new System.Drawing.Point(149, 157);
            this.CigarTotalPacksTxt.Name = "CigarTotalPacksTxt";
            this.CigarTotalPacksTxt.ReadOnly = true;
            this.CigarTotalPacksTxt.Size = new System.Drawing.Size(53, 21);
            this.CigarTotalPacksTxt.TabIndex = 257;
            this.CigarTotalPacksTxt.Text = "0";
            this.CigarTotalPacksTxt.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(57, 160);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 15);
            this.label13.TabIndex = 256;
            this.label13.Text = "Total Packs";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // CigarRemarksTxt
            // 
            this.CigarRemarksTxt.Location = new System.Drawing.Point(966, 156);
            this.CigarRemarksTxt.Name = "CigarRemarksTxt";
            this.CigarRemarksTxt.Size = new System.Drawing.Size(241, 93);
            this.CigarRemarksTxt.TabIndex = 255;
            this.CigarRemarksTxt.Text = "";
            this.CigarRemarksTxt.Leave += new System.EventHandler(this.CigarAdd10sTxt_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(890, 159);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 15);
            this.label14.TabIndex = 254;
            this.label14.Text = "Remarks";
            // 
            // CigarSubstract10sTxt
            // 
            this.CigarSubstract10sTxt.Location = new System.Drawing.Point(453, 50);
            this.CigarSubstract10sTxt.Name = "CigarSubstract10sTxt";
            this.CigarSubstract10sTxt.Size = new System.Drawing.Size(51, 21);
            this.CigarSubstract10sTxt.TabIndex = 245;
            this.CigarSubstract10sTxt.Text = "0";
            this.CigarSubstract10sTxt.Leave += new System.EventHandler(this.CigarAdd10sTxt_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(355, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 15);
            this.label15.TabIndex = 244;
            this.label15.Text = "Subtract 10\'s";
            // 
            // CigarAdd10sTxt
            // 
            this.CigarAdd10sTxt.Location = new System.Drawing.Point(296, 50);
            this.CigarAdd10sTxt.Name = "CigarAdd10sTxt";
            this.CigarAdd10sTxt.Size = new System.Drawing.Size(51, 21);
            this.CigarAdd10sTxt.TabIndex = 243;
            this.CigarAdd10sTxt.Text = "0";
            this.CigarAdd10sTxt.Leave += new System.EventHandler(this.CigarAdd10sTxt_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(229, 53);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 15);
            this.label16.TabIndex = 242;
            this.label16.Text = "Add 10\'s";
            // 
            // CigarBalance10sTxt
            // 
            this.CigarBalance10sTxt.Location = new System.Drawing.Point(149, 50);
            this.CigarBalance10sTxt.Name = "CigarBalance10sTxt";
            this.CigarBalance10sTxt.ReadOnly = true;
            this.CigarBalance10sTxt.Size = new System.Drawing.Size(53, 21);
            this.CigarBalance10sTxt.TabIndex = 241;
            this.CigarBalance10sTxt.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(57, 53);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 15);
            this.label17.TabIndex = 240;
            this.label17.Text = "Balance 10\'s";
            // 
            // CigarDgv
            // 
            this.CigarDgv.AllowDrop = true;
            this.CigarDgv.AllowUserToAddRows = false;
            this.CigarDgv.AllowUserToDeleteRows = false;
            this.CigarDgv.AllowUserToResizeColumns = false;
            this.CigarDgv.AllowUserToResizeRows = false;
            this.CigarDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.CigarDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CigarDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.CigarDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CigarDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn2});
            this.CigarDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.CigarDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.CigarDgv.Location = new System.Drawing.Point(966, 28);
            this.CigarDgv.MultiSelect = false;
            this.CigarDgv.Name = "CigarDgv";
            this.CigarDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.CigarDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.CigarDgv.RowHeadersVisible = false;
            this.CigarDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.CigarDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CigarDgv.Size = new System.Drawing.Size(241, 109);
            this.CigarDgv.TabIndex = 238;
            // 
            // dataGridViewCheckBoxColumn2
            // 
            this.dataGridViewCheckBoxColumn2.HeaderText = "";
            this.dataGridViewCheckBoxColumn2.Name = "dataGridViewCheckBoxColumn2";
            this.dataGridViewCheckBoxColumn2.Width = 5;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(961, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 15);
            this.label22.TabIndex = 239;
            this.label22.Text = "Cigar  Docs";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ECigarReasonCmb);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.ECigarSiteCmb);
            this.groupBox3.Controls.Add(this.ECigarSiteLabel);
            this.groupBox3.Controls.Add(this.ECigarRemarks);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.ECigarSubstractTxt);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.ECigarAddTxt);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.ECigarBalanceTxt);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.ECigarDgv);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(30, 960);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1260, 228);
            this.groupBox3.TabIndex = 259;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "E-Cigar";
            // 
            // ECigarReasonCmb
            // 
            this.ECigarReasonCmb.Enabled = false;
            this.ECigarReasonCmb.FormattingEnabled = true;
            this.ECigarReasonCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.ECigarReasonCmb.Location = new System.Drawing.Point(591, 48);
            this.ECigarReasonCmb.Name = "ECigarReasonCmb";
            this.ECigarReasonCmb.Size = new System.Drawing.Size(140, 23);
            this.ECigarReasonCmb.TabIndex = 277;
            this.ECigarReasonCmb.SelectedIndexChanged += new System.EventHandler(this.ECigarReasonCmb_SelectedIndexChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(520, 53);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(50, 15);
            this.label35.TabIndex = 276;
            this.label35.Text = "Reason";
            // 
            // ECigarSiteCmb
            // 
            this.ECigarSiteCmb.Enabled = false;
            this.ECigarSiteCmb.FormattingEnabled = true;
            this.ECigarSiteCmb.Location = new System.Drawing.Point(807, 50);
            this.ECigarSiteCmb.Name = "ECigarSiteCmb";
            this.ECigarSiteCmb.Size = new System.Drawing.Size(139, 23);
            this.ECigarSiteCmb.TabIndex = 275;
            // 
            // ECigarSiteLabel
            // 
            this.ECigarSiteLabel.AutoSize = true;
            this.ECigarSiteLabel.Location = new System.Drawing.Point(752, 52);
            this.ECigarSiteLabel.Name = "ECigarSiteLabel";
            this.ECigarSiteLabel.Size = new System.Drawing.Size(28, 15);
            this.ECigarSiteLabel.TabIndex = 274;
            this.ECigarSiteLabel.Text = "Site";
            // 
            // ECigarRemarks
            // 
            this.ECigarRemarks.Location = new System.Drawing.Point(147, 104);
            this.ECigarRemarks.Name = "ECigarRemarks";
            this.ECigarRemarks.Size = new System.Drawing.Size(356, 92);
            this.ECigarRemarks.TabIndex = 255;
            this.ECigarRemarks.Text = "";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(55, 135);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 15);
            this.label30.TabIndex = 254;
            this.label30.Text = "Remarks";
            // 
            // ECigarSubstractTxt
            // 
            this.ECigarSubstractTxt.Location = new System.Drawing.Point(453, 50);
            this.ECigarSubstractTxt.Name = "ECigarSubstractTxt";
            this.ECigarSubstractTxt.Size = new System.Drawing.Size(51, 21);
            this.ECigarSubstractTxt.TabIndex = 245;
            this.ECigarSubstractTxt.Text = "0";
            this.ECigarSubstractTxt.Leave += new System.EventHandler(this.ECigarAddTxt_Leave);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(355, 53);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(52, 15);
            this.label31.TabIndex = 244;
            this.label31.Text = "Subtract";
            // 
            // ECigarAddTxt
            // 
            this.ECigarAddTxt.Location = new System.Drawing.Point(296, 50);
            this.ECigarAddTxt.Name = "ECigarAddTxt";
            this.ECigarAddTxt.Size = new System.Drawing.Size(51, 21);
            this.ECigarAddTxt.TabIndex = 243;
            this.ECigarAddTxt.Text = "0";
            this.ECigarAddTxt.Leave += new System.EventHandler(this.ECigarAddTxt_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(229, 53);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 15);
            this.label32.TabIndex = 242;
            this.label32.Text = "Add";
            // 
            // ECigarBalanceTxt
            // 
            this.ECigarBalanceTxt.Location = new System.Drawing.Point(149, 50);
            this.ECigarBalanceTxt.Name = "ECigarBalanceTxt";
            this.ECigarBalanceTxt.ReadOnly = true;
            this.ECigarBalanceTxt.Size = new System.Drawing.Size(53, 21);
            this.ECigarBalanceTxt.TabIndex = 241;
            this.ECigarBalanceTxt.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(57, 53);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 15);
            this.label33.TabIndex = 240;
            this.label33.Text = "Balance";
            // 
            // ECigarDgv
            // 
            this.ECigarDgv.AllowDrop = true;
            this.ECigarDgv.AllowUserToAddRows = false;
            this.ECigarDgv.AllowUserToDeleteRows = false;
            this.ECigarDgv.AllowUserToResizeColumns = false;
            this.ECigarDgv.AllowUserToResizeRows = false;
            this.ECigarDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ECigarDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ECigarDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.ECigarDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ECigarDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn3});
            this.ECigarDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.ECigarDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.ECigarDgv.Location = new System.Drawing.Point(966, 50);
            this.ECigarDgv.MultiSelect = false;
            this.ECigarDgv.Name = "ECigarDgv";
            this.ECigarDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.ECigarDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.ECigarDgv.RowHeadersVisible = false;
            this.ECigarDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.ECigarDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ECigarDgv.Size = new System.Drawing.Size(241, 112);
            this.ECigarDgv.TabIndex = 238;
            // 
            // dataGridViewCheckBoxColumn3
            // 
            this.dataGridViewCheckBoxColumn3.HeaderText = "";
            this.dataGridViewCheckBoxColumn3.Name = "dataGridViewCheckBoxColumn3";
            this.dataGridViewCheckBoxColumn3.Width = 5;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(961, 18);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(81, 15);
            this.label34.TabIndex = 239;
            this.label34.Text = "E-Cigar Docs";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.SinglesReasonCmb);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.SinglesSiteCmb);
            this.groupBox4.Controls.Add(this.SinglesSiteLabel);
            this.groupBox4.Controls.Add(this.SingleRemarksTxt);
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Controls.Add(this.SingleSubstractTxt);
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.SingleAddTxt);
            this.groupBox4.Controls.Add(this.label38);
            this.groupBox4.Controls.Add(this.SinglesBalanceTxt);
            this.groupBox4.Controls.Add(this.label39);
            this.groupBox4.Controls.Add(this.SinglesDgv);
            this.groupBox4.Controls.Add(this.label40);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(30, 1207);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1260, 263);
            this.groupBox4.TabIndex = 260;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Singles & Papers";
            // 
            // SinglesReasonCmb
            // 
            this.SinglesReasonCmb.Enabled = false;
            this.SinglesReasonCmb.FormattingEnabled = true;
            this.SinglesReasonCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.SinglesReasonCmb.Location = new System.Drawing.Point(591, 48);
            this.SinglesReasonCmb.Name = "SinglesReasonCmb";
            this.SinglesReasonCmb.Size = new System.Drawing.Size(140, 23);
            this.SinglesReasonCmb.TabIndex = 281;
            this.SinglesReasonCmb.SelectedIndexChanged += new System.EventHandler(this.SinglesReasonCmb_SelectedIndexChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(520, 53);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(50, 15);
            this.label42.TabIndex = 280;
            this.label42.Text = "Reason";
            // 
            // SinglesSiteCmb
            // 
            this.SinglesSiteCmb.Enabled = false;
            this.SinglesSiteCmb.FormattingEnabled = true;
            this.SinglesSiteCmb.Location = new System.Drawing.Point(807, 50);
            this.SinglesSiteCmb.Name = "SinglesSiteCmb";
            this.SinglesSiteCmb.Size = new System.Drawing.Size(139, 23);
            this.SinglesSiteCmb.TabIndex = 279;
            // 
            // SinglesSiteLabel
            // 
            this.SinglesSiteLabel.AutoSize = true;
            this.SinglesSiteLabel.Location = new System.Drawing.Point(752, 52);
            this.SinglesSiteLabel.Name = "SinglesSiteLabel";
            this.SinglesSiteLabel.Size = new System.Drawing.Size(28, 15);
            this.SinglesSiteLabel.TabIndex = 278;
            this.SinglesSiteLabel.Text = "Site";
            // 
            // SingleRemarksTxt
            // 
            this.SingleRemarksTxt.Location = new System.Drawing.Point(148, 98);
            this.SingleRemarksTxt.Name = "SingleRemarksTxt";
            this.SingleRemarksTxt.Size = new System.Drawing.Size(355, 92);
            this.SingleRemarksTxt.TabIndex = 255;
            this.SingleRemarksTxt.Text = "";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(56, 152);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(57, 15);
            this.label36.TabIndex = 254;
            this.label36.Text = "Remarks";
            // 
            // SingleSubstractTxt
            // 
            this.SingleSubstractTxt.Location = new System.Drawing.Point(453, 50);
            this.SingleSubstractTxt.Name = "SingleSubstractTxt";
            this.SingleSubstractTxt.Size = new System.Drawing.Size(51, 21);
            this.SingleSubstractTxt.TabIndex = 245;
            this.SingleSubstractTxt.Text = "0";
            this.SingleSubstractTxt.Leave += new System.EventHandler(this.SingleAddTxt_Leave);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(355, 53);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(52, 15);
            this.label37.TabIndex = 244;
            this.label37.Text = "Subtract";
            // 
            // SingleAddTxt
            // 
            this.SingleAddTxt.Location = new System.Drawing.Point(296, 50);
            this.SingleAddTxt.Name = "SingleAddTxt";
            this.SingleAddTxt.Size = new System.Drawing.Size(51, 21);
            this.SingleAddTxt.TabIndex = 243;
            this.SingleAddTxt.Text = "0";
            this.SingleAddTxt.Leave += new System.EventHandler(this.SingleAddTxt_Leave);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(229, 53);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(28, 15);
            this.label38.TabIndex = 242;
            this.label38.Text = "Add";
            // 
            // SinglesBalanceTxt
            // 
            this.SinglesBalanceTxt.Location = new System.Drawing.Point(149, 50);
            this.SinglesBalanceTxt.Name = "SinglesBalanceTxt";
            this.SinglesBalanceTxt.ReadOnly = true;
            this.SinglesBalanceTxt.Size = new System.Drawing.Size(53, 21);
            this.SinglesBalanceTxt.TabIndex = 241;
            this.SinglesBalanceTxt.Text = "0";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(57, 53);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(52, 15);
            this.label39.TabIndex = 240;
            this.label39.Text = "Balance";
            // 
            // SinglesDgv
            // 
            this.SinglesDgv.AllowDrop = true;
            this.SinglesDgv.AllowUserToAddRows = false;
            this.SinglesDgv.AllowUserToDeleteRows = false;
            this.SinglesDgv.AllowUserToResizeColumns = false;
            this.SinglesDgv.AllowUserToResizeRows = false;
            this.SinglesDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.SinglesDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SinglesDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.SinglesDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.SinglesDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn4});
            this.SinglesDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.SinglesDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.SinglesDgv.Location = new System.Drawing.Point(966, 46);
            this.SinglesDgv.MultiSelect = false;
            this.SinglesDgv.Name = "SinglesDgv";
            this.SinglesDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.SinglesDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.SinglesDgv.RowHeadersVisible = false;
            this.SinglesDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.SinglesDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.SinglesDgv.Size = new System.Drawing.Size(241, 111);
            this.SinglesDgv.TabIndex = 238;
            // 
            // dataGridViewCheckBoxColumn4
            // 
            this.dataGridViewCheckBoxColumn4.HeaderText = "";
            this.dataGridViewCheckBoxColumn4.Name = "dataGridViewCheckBoxColumn4";
            this.dataGridViewCheckBoxColumn4.Width = 5;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(961, 18);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(127, 15);
            this.label40.TabIndex = 239;
            this.label40.Text = "Singles & Papers Docs";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.MilkReason4LCmb);
            this.groupBox5.Controls.Add(this.label66);
            this.groupBox5.Controls.Add(this.MilkSite4LCmb);
            this.groupBox5.Controls.Add(this.MilkSite4LLabel);
            this.groupBox5.Controls.Add(this.MilkSubtract4LTxt);
            this.groupBox5.Controls.Add(this.label68);
            this.groupBox5.Controls.Add(this.MilkAdd4LTxt);
            this.groupBox5.Controls.Add(this.label69);
            this.groupBox5.Controls.Add(this.MilkBalance4LTxt);
            this.groupBox5.Controls.Add(this.label70);
            this.groupBox5.Controls.Add(this.MilkReason2LCmb);
            this.groupBox5.Controls.Add(this.label61);
            this.groupBox5.Controls.Add(this.MilkSite2LCmb);
            this.groupBox5.Controls.Add(this.MilkSite2LLabel);
            this.groupBox5.Controls.Add(this.MilkSubtract2LTxt);
            this.groupBox5.Controls.Add(this.label63);
            this.groupBox5.Controls.Add(this.MilkAdd2LTxt);
            this.groupBox5.Controls.Add(this.label64);
            this.groupBox5.Controls.Add(this.MilkBalance2LTxt);
            this.groupBox5.Controls.Add(this.label65);
            this.groupBox5.Controls.Add(this.MilkReason1LCmb);
            this.groupBox5.Controls.Add(this.label56);
            this.groupBox5.Controls.Add(this.MilkSite1LCmb);
            this.groupBox5.Controls.Add(this.MilkSite1LLabel);
            this.groupBox5.Controls.Add(this.MilkSubtract1LTxt);
            this.groupBox5.Controls.Add(this.label58);
            this.groupBox5.Controls.Add(this.MilkAdd1LTxt);
            this.groupBox5.Controls.Add(this.label59);
            this.groupBox5.Controls.Add(this.MilkBalance1LTxt);
            this.groupBox5.Controls.Add(this.label60);
            this.groupBox5.Controls.Add(this.MilkReason473mlCmb);
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Controls.Add(this.MilkSite473mlCmb);
            this.groupBox5.Controls.Add(this.MilkSite473Label);
            this.groupBox5.Controls.Add(this.MilkSubtract473mlTxt);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Controls.Add(this.MilkAdd473mlTxt);
            this.groupBox5.Controls.Add(this.label54);
            this.groupBox5.Controls.Add(this.MilkBalance473mlTxt);
            this.groupBox5.Controls.Add(this.label55);
            this.groupBox5.Controls.Add(this.MilkReason310mlCmb);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this.MilkSite310mlCmb);
            this.groupBox5.Controls.Add(this.MilkSite310Label);
            this.groupBox5.Controls.Add(this.MilkRemarksTxt);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this.MilkSubtract310mlTxt);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this.MilkAdd310mlTxt);
            this.groupBox5.Controls.Add(this.label48);
            this.groupBox5.Controls.Add(this.MilkBalance310mlTxt);
            this.groupBox5.Controls.Add(this.label49);
            this.groupBox5.Controls.Add(this.MilkDgv);
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(30, 1488);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1260, 325);
            this.groupBox5.TabIndex = 282;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Milk";
            this.groupBox5.Visible = false;
            // 
            // MilkReason4LCmb
            // 
            this.MilkReason4LCmb.Enabled = false;
            this.MilkReason4LCmb.FormattingEnabled = true;
            this.MilkReason4LCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.MilkReason4LCmb.Location = new System.Drawing.Point(574, 185);
            this.MilkReason4LCmb.Name = "MilkReason4LCmb";
            this.MilkReason4LCmb.Size = new System.Drawing.Size(140, 23);
            this.MilkReason4LCmb.TabIndex = 321;
            this.MilkReason4LCmb.SelectedIndexChanged += new System.EventHandler(this.MilkReason4LCmb_SelectedIndexChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(470, 188);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(67, 15);
            this.label66.TabIndex = 320;
            this.label66.Text = "Reason 4L";
            // 
            // MilkSite4LCmb
            // 
            this.MilkSite4LCmb.Enabled = false;
            this.MilkSite4LCmb.FormattingEnabled = true;
            this.MilkSite4LCmb.Location = new System.Drawing.Point(807, 183);
            this.MilkSite4LCmb.Name = "MilkSite4LCmb";
            this.MilkSite4LCmb.Size = new System.Drawing.Size(139, 23);
            this.MilkSite4LCmb.TabIndex = 319;
            // 
            // MilkSite4LLabel
            // 
            this.MilkSite4LLabel.AutoSize = true;
            this.MilkSite4LLabel.Location = new System.Drawing.Point(722, 187);
            this.MilkSite4LLabel.Name = "MilkSite4LLabel";
            this.MilkSite4LLabel.Size = new System.Drawing.Size(45, 15);
            this.MilkSite4LLabel.TabIndex = 318;
            this.MilkSite4LLabel.Text = "Site 4L";
            // 
            // MilkSubtract4LTxt
            // 
            this.MilkSubtract4LTxt.Location = new System.Drawing.Point(412, 185);
            this.MilkSubtract4LTxt.Name = "MilkSubtract4LTxt";
            this.MilkSubtract4LTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkSubtract4LTxt.TabIndex = 317;
            this.MilkSubtract4LTxt.Text = "0";
            this.MilkSubtract4LTxt.Leave += new System.EventHandler(this.MilkAdd4LTxt_Leave);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(308, 186);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(69, 15);
            this.label68.TabIndex = 316;
            this.label68.Text = "Subtract 4L";
            // 
            // MilkAdd4LTxt
            // 
            this.MilkAdd4LTxt.Location = new System.Drawing.Point(255, 185);
            this.MilkAdd4LTxt.Name = "MilkAdd4LTxt";
            this.MilkAdd4LTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkAdd4LTxt.TabIndex = 315;
            this.MilkAdd4LTxt.Text = "0";
            this.MilkAdd4LTxt.Leave += new System.EventHandler(this.MilkAdd4LTxt_Leave);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(169, 187);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(45, 15);
            this.label69.TabIndex = 314;
            this.label69.Text = "Add 4L";
            // 
            // MilkBalance4LTxt
            // 
            this.MilkBalance4LTxt.Location = new System.Drawing.Point(108, 185);
            this.MilkBalance4LTxt.Name = "MilkBalance4LTxt";
            this.MilkBalance4LTxt.ReadOnly = true;
            this.MilkBalance4LTxt.Size = new System.Drawing.Size(53, 21);
            this.MilkBalance4LTxt.TabIndex = 313;
            this.MilkBalance4LTxt.Text = "0";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(5, 187);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(69, 15);
            this.label70.TabIndex = 312;
            this.label70.Text = "Balance 4L";
            // 
            // MilkReason2LCmb
            // 
            this.MilkReason2LCmb.Enabled = false;
            this.MilkReason2LCmb.FormattingEnabled = true;
            this.MilkReason2LCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.MilkReason2LCmb.Location = new System.Drawing.Point(574, 151);
            this.MilkReason2LCmb.Name = "MilkReason2LCmb";
            this.MilkReason2LCmb.Size = new System.Drawing.Size(140, 23);
            this.MilkReason2LCmb.TabIndex = 311;
            this.MilkReason2LCmb.SelectedIndexChanged += new System.EventHandler(this.MilkReason2LCmb_SelectedIndexChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(470, 155);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(67, 15);
            this.label61.TabIndex = 310;
            this.label61.Text = "Reason 2L";
            // 
            // MilkSite2LCmb
            // 
            this.MilkSite2LCmb.Enabled = false;
            this.MilkSite2LCmb.FormattingEnabled = true;
            this.MilkSite2LCmb.Location = new System.Drawing.Point(807, 150);
            this.MilkSite2LCmb.Name = "MilkSite2LCmb";
            this.MilkSite2LCmb.Size = new System.Drawing.Size(139, 23);
            this.MilkSite2LCmb.TabIndex = 309;
            // 
            // MilkSite2LLabel
            // 
            this.MilkSite2LLabel.AutoSize = true;
            this.MilkSite2LLabel.Location = new System.Drawing.Point(722, 153);
            this.MilkSite2LLabel.Name = "MilkSite2LLabel";
            this.MilkSite2LLabel.Size = new System.Drawing.Size(45, 15);
            this.MilkSite2LLabel.TabIndex = 308;
            this.MilkSite2LLabel.Text = "Site 2L";
            // 
            // MilkSubtract2LTxt
            // 
            this.MilkSubtract2LTxt.Location = new System.Drawing.Point(412, 151);
            this.MilkSubtract2LTxt.Name = "MilkSubtract2LTxt";
            this.MilkSubtract2LTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkSubtract2LTxt.TabIndex = 307;
            this.MilkSubtract2LTxt.Text = "0";
            this.MilkSubtract2LTxt.Leave += new System.EventHandler(this.MilkAdd2LTxt_Leave);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(308, 152);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(69, 15);
            this.label63.TabIndex = 306;
            this.label63.Text = "Subtract 2L";
            // 
            // MilkAdd2LTxt
            // 
            this.MilkAdd2LTxt.Location = new System.Drawing.Point(255, 151);
            this.MilkAdd2LTxt.Name = "MilkAdd2LTxt";
            this.MilkAdd2LTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkAdd2LTxt.TabIndex = 305;
            this.MilkAdd2LTxt.Text = "0";
            this.MilkAdd2LTxt.Leave += new System.EventHandler(this.MilkAdd2LTxt_Leave);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(169, 153);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(45, 15);
            this.label64.TabIndex = 304;
            this.label64.Text = "Add 2L";
            // 
            // MilkBalance2LTxt
            // 
            this.MilkBalance2LTxt.Location = new System.Drawing.Point(108, 151);
            this.MilkBalance2LTxt.Name = "MilkBalance2LTxt";
            this.MilkBalance2LTxt.ReadOnly = true;
            this.MilkBalance2LTxt.Size = new System.Drawing.Size(53, 21);
            this.MilkBalance2LTxt.TabIndex = 303;
            this.MilkBalance2LTxt.Text = "0";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(5, 153);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(69, 15);
            this.label65.TabIndex = 302;
            this.label65.Text = "Balance 2L";
            // 
            // MilkReason1LCmb
            // 
            this.MilkReason1LCmb.Enabled = false;
            this.MilkReason1LCmb.FormattingEnabled = true;
            this.MilkReason1LCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.MilkReason1LCmb.Location = new System.Drawing.Point(574, 118);
            this.MilkReason1LCmb.Name = "MilkReason1LCmb";
            this.MilkReason1LCmb.Size = new System.Drawing.Size(140, 23);
            this.MilkReason1LCmb.TabIndex = 301;
            this.MilkReason1LCmb.SelectedIndexChanged += new System.EventHandler(this.MilkReason1LCmb_SelectedIndexChanged);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(470, 121);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 15);
            this.label56.TabIndex = 300;
            this.label56.Text = "Reason 1L";
            // 
            // MilkSite1LCmb
            // 
            this.MilkSite1LCmb.Enabled = false;
            this.MilkSite1LCmb.FormattingEnabled = true;
            this.MilkSite1LCmb.Location = new System.Drawing.Point(807, 117);
            this.MilkSite1LCmb.Name = "MilkSite1LCmb";
            this.MilkSite1LCmb.Size = new System.Drawing.Size(139, 23);
            this.MilkSite1LCmb.TabIndex = 299;
            // 
            // MilkSite1LLabel
            // 
            this.MilkSite1LLabel.AutoSize = true;
            this.MilkSite1LLabel.Location = new System.Drawing.Point(722, 120);
            this.MilkSite1LLabel.Name = "MilkSite1LLabel";
            this.MilkSite1LLabel.Size = new System.Drawing.Size(45, 15);
            this.MilkSite1LLabel.TabIndex = 298;
            this.MilkSite1LLabel.Text = "Site 1L";
            // 
            // MilkSubtract1LTxt
            // 
            this.MilkSubtract1LTxt.Location = new System.Drawing.Point(412, 118);
            this.MilkSubtract1LTxt.Name = "MilkSubtract1LTxt";
            this.MilkSubtract1LTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkSubtract1LTxt.TabIndex = 297;
            this.MilkSubtract1LTxt.Text = "0";
            this.MilkSubtract1LTxt.Leave += new System.EventHandler(this.MilkAdd1LTxt_Leave);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(308, 119);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(69, 15);
            this.label58.TabIndex = 296;
            this.label58.Text = "Subtract 1L";
            // 
            // MilkAdd1LTxt
            // 
            this.MilkAdd1LTxt.Location = new System.Drawing.Point(255, 118);
            this.MilkAdd1LTxt.Name = "MilkAdd1LTxt";
            this.MilkAdd1LTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkAdd1LTxt.TabIndex = 295;
            this.MilkAdd1LTxt.Text = "0";
            this.MilkAdd1LTxt.Leave += new System.EventHandler(this.MilkAdd1LTxt_Leave);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(169, 120);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(45, 15);
            this.label59.TabIndex = 294;
            this.label59.Text = "Add 1L";
            // 
            // MilkBalance1LTxt
            // 
            this.MilkBalance1LTxt.Location = new System.Drawing.Point(108, 118);
            this.MilkBalance1LTxt.Name = "MilkBalance1LTxt";
            this.MilkBalance1LTxt.ReadOnly = true;
            this.MilkBalance1LTxt.Size = new System.Drawing.Size(53, 21);
            this.MilkBalance1LTxt.TabIndex = 293;
            this.MilkBalance1LTxt.Text = "0";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(5, 120);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(69, 15);
            this.label60.TabIndex = 292;
            this.label60.Text = "Balance 1L";
            // 
            // MilkReason473mlCmb
            // 
            this.MilkReason473mlCmb.Enabled = false;
            this.MilkReason473mlCmb.FormattingEnabled = true;
            this.MilkReason473mlCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.MilkReason473mlCmb.Location = new System.Drawing.Point(574, 84);
            this.MilkReason473mlCmb.Name = "MilkReason473mlCmb";
            this.MilkReason473mlCmb.Size = new System.Drawing.Size(140, 23);
            this.MilkReason473mlCmb.TabIndex = 291;
            this.MilkReason473mlCmb.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(470, 88);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(88, 15);
            this.label51.TabIndex = 290;
            this.label51.Text = "Reason 473ml";
            // 
            // MilkSite473mlCmb
            // 
            this.MilkSite473mlCmb.Enabled = false;
            this.MilkSite473mlCmb.FormattingEnabled = true;
            this.MilkSite473mlCmb.Location = new System.Drawing.Point(807, 83);
            this.MilkSite473mlCmb.Name = "MilkSite473mlCmb";
            this.MilkSite473mlCmb.Size = new System.Drawing.Size(139, 23);
            this.MilkSite473mlCmb.TabIndex = 289;
            // 
            // MilkSite473Label
            // 
            this.MilkSite473Label.AutoSize = true;
            this.MilkSite473Label.Location = new System.Drawing.Point(722, 87);
            this.MilkSite473Label.Name = "MilkSite473Label";
            this.MilkSite473Label.Size = new System.Drawing.Size(66, 15);
            this.MilkSite473Label.TabIndex = 288;
            this.MilkSite473Label.Text = "Site 473ml";
            // 
            // MilkSubtract473mlTxt
            // 
            this.MilkSubtract473mlTxt.Location = new System.Drawing.Point(412, 84);
            this.MilkSubtract473mlTxt.Name = "MilkSubtract473mlTxt";
            this.MilkSubtract473mlTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkSubtract473mlTxt.TabIndex = 287;
            this.MilkSubtract473mlTxt.Text = "0";
            this.MilkSubtract473mlTxt.Leave += new System.EventHandler(this.MilkAdd473mlTxt_Leave);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(308, 85);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(90, 15);
            this.label53.TabIndex = 286;
            this.label53.Text = "Subtract 473ml";
            // 
            // MilkAdd473mlTxt
            // 
            this.MilkAdd473mlTxt.Location = new System.Drawing.Point(255, 84);
            this.MilkAdd473mlTxt.Name = "MilkAdd473mlTxt";
            this.MilkAdd473mlTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkAdd473mlTxt.TabIndex = 285;
            this.MilkAdd473mlTxt.Text = "0";
            this.MilkAdd473mlTxt.Leave += new System.EventHandler(this.MilkAdd473mlTxt_Leave);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(169, 87);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(66, 15);
            this.label54.TabIndex = 284;
            this.label54.Text = "Add 473ml";
            // 
            // MilkBalance473mlTxt
            // 
            this.MilkBalance473mlTxt.Location = new System.Drawing.Point(108, 84);
            this.MilkBalance473mlTxt.Name = "MilkBalance473mlTxt";
            this.MilkBalance473mlTxt.ReadOnly = true;
            this.MilkBalance473mlTxt.Size = new System.Drawing.Size(53, 21);
            this.MilkBalance473mlTxt.TabIndex = 283;
            this.MilkBalance473mlTxt.Text = "0";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(5, 87);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(90, 15);
            this.label55.TabIndex = 282;
            this.label55.Text = "Balance 473ml";
            // 
            // MilkReason310mlCmb
            // 
            this.MilkReason310mlCmb.Enabled = false;
            this.MilkReason310mlCmb.FormattingEnabled = true;
            this.MilkReason310mlCmb.Items.AddRange(new object[] {
            "--Select--",
            "Front Office Transfer",
            "Other Site Transfer"});
            this.MilkReason310mlCmb.Location = new System.Drawing.Point(574, 51);
            this.MilkReason310mlCmb.Name = "MilkReason310mlCmb";
            this.MilkReason310mlCmb.Size = new System.Drawing.Size(140, 23);
            this.MilkReason310mlCmb.TabIndex = 281;
            this.MilkReason310mlCmb.SelectedIndexChanged += new System.EventHandler(this.MilkReason310mlCmb_SelectedIndexChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(470, 54);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(88, 15);
            this.label44.TabIndex = 280;
            this.label44.Text = "Reason 310ml";
            // 
            // MilkSite310mlCmb
            // 
            this.MilkSite310mlCmb.Enabled = false;
            this.MilkSite310mlCmb.FormattingEnabled = true;
            this.MilkSite310mlCmb.Location = new System.Drawing.Point(807, 50);
            this.MilkSite310mlCmb.Name = "MilkSite310mlCmb";
            this.MilkSite310mlCmb.Size = new System.Drawing.Size(139, 23);
            this.MilkSite310mlCmb.TabIndex = 279;
            // 
            // MilkSite310Label
            // 
            this.MilkSite310Label.AutoSize = true;
            this.MilkSite310Label.Location = new System.Drawing.Point(722, 53);
            this.MilkSite310Label.Name = "MilkSite310Label";
            this.MilkSite310Label.Size = new System.Drawing.Size(66, 15);
            this.MilkSite310Label.TabIndex = 278;
            this.MilkSite310Label.Text = "Site 310ml";
            this.MilkSite310Label.Click += new System.EventHandler(this.label45_Click);
            // 
            // MilkRemarksTxt
            // 
            this.MilkRemarksTxt.Location = new System.Drawing.Point(966, 204);
            this.MilkRemarksTxt.Name = "MilkRemarksTxt";
            this.MilkRemarksTxt.Size = new System.Drawing.Size(258, 92);
            this.MilkRemarksTxt.TabIndex = 255;
            this.MilkRemarksTxt.Text = "";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(965, 182);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(57, 15);
            this.label46.TabIndex = 254;
            this.label46.Text = "Remarks";
            // 
            // MilkSubtract310mlTxt
            // 
            this.MilkSubtract310mlTxt.Location = new System.Drawing.Point(412, 51);
            this.MilkSubtract310mlTxt.Name = "MilkSubtract310mlTxt";
            this.MilkSubtract310mlTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkSubtract310mlTxt.TabIndex = 245;
            this.MilkSubtract310mlTxt.Text = "0";
            this.MilkSubtract310mlTxt.Leave += new System.EventHandler(this.MilkAdd310mlTxt_Leave);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(308, 52);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(90, 15);
            this.label47.TabIndex = 244;
            this.label47.Text = "Subtract 310ml";
            // 
            // MilkAdd310mlTxt
            // 
            this.MilkAdd310mlTxt.Location = new System.Drawing.Point(255, 51);
            this.MilkAdd310mlTxt.Name = "MilkAdd310mlTxt";
            this.MilkAdd310mlTxt.Size = new System.Drawing.Size(51, 21);
            this.MilkAdd310mlTxt.TabIndex = 243;
            this.MilkAdd310mlTxt.Text = "0";
            this.MilkAdd310mlTxt.Leave += new System.EventHandler(this.MilkAdd310mlTxt_Leave);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(169, 53);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(66, 15);
            this.label48.TabIndex = 242;
            this.label48.Text = "Add 310ml";
            // 
            // MilkBalance310mlTxt
            // 
            this.MilkBalance310mlTxt.Location = new System.Drawing.Point(108, 51);
            this.MilkBalance310mlTxt.Name = "MilkBalance310mlTxt";
            this.MilkBalance310mlTxt.ReadOnly = true;
            this.MilkBalance310mlTxt.Size = new System.Drawing.Size(53, 21);
            this.MilkBalance310mlTxt.TabIndex = 241;
            this.MilkBalance310mlTxt.Text = "0";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(5, 53);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(90, 15);
            this.label49.TabIndex = 240;
            this.label49.Text = "Balance 310ml";
            // 
            // MilkDgv
            // 
            this.MilkDgv.AllowDrop = true;
            this.MilkDgv.AllowUserToAddRows = false;
            this.MilkDgv.AllowUserToDeleteRows = false;
            this.MilkDgv.AllowUserToResizeColumns = false;
            this.MilkDgv.AllowUserToResizeRows = false;
            this.MilkDgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.MilkDgv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MilkDgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.MilkDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.MilkDgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn5});
            this.MilkDgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.MilkDgv.GridColor = System.Drawing.SystemColors.Desktop;
            this.MilkDgv.Location = new System.Drawing.Point(966, 46);
            this.MilkDgv.MultiSelect = false;
            this.MilkDgv.Name = "MilkDgv";
            this.MilkDgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.MilkDgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.MilkDgv.RowHeadersVisible = false;
            this.MilkDgv.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.MilkDgv.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.MilkDgv.Size = new System.Drawing.Size(258, 121);
            this.MilkDgv.TabIndex = 238;
            // 
            // dataGridViewCheckBoxColumn5
            // 
            this.dataGridViewCheckBoxColumn5.HeaderText = "";
            this.dataGridViewCheckBoxColumn5.Name = "dataGridViewCheckBoxColumn5";
            this.dataGridViewCheckBoxColumn5.Width = 5;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(961, 18);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(60, 15);
            this.label50.TabIndex = 239;
            this.label50.Text = "Milk Docs";
            // 
            // TaboccoMasterInventoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1324, 587);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.ChewgroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TobaccoGroup);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "TaboccoMasterInventoryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tobacco Inventory Form";
            this.Load += new System.EventHandler(this.TaboccoMasterInventoryForm_Load);
            this.TobaccoGroup.ResumeLayout(false);
            this.TobaccoGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TobaccoDgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ChewgroupBox.ResumeLayout(false);
            this.ChewgroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChewDgv)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CigarDgv)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ECigarDgv)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SinglesDgv)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MilkDgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox TobaccoGroup;
        private System.Windows.Forms.DataGridView TobaccoDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn10;
        private System.Windows.Forms.Label TobaccoLabelDocs;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button ViewBtn;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DateTimePicker DateCalender;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TobaccoSubstract8STxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TobaccoAdd8STxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TaboccoBalance8Stxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TobaccoSubstract10STxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TobaccoAdd10STxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TobaccoBalance10STxt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RichTextBox TobaccoRemarksTxt;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox TobaccoTotalPacksTxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox ChewgroupBox;
        private System.Windows.Forms.RichTextBox ChewRemarksTxt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox ChewSubstractTxt;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox ChewAddTxt;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox ChewBalanceTxt;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DataGridView ChewDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox CigarTotalPacksTxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RichTextBox CigarRemarksTxt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox CigarSubstract10sTxt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox CigarAdd10sTxt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox CigarBalance10sTxt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridView CigarDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox CigarSubstract8sTxt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox CigarAdd8sTxt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox CigarBalance8sTxt;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox CigarSubstract5sTxt;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox CigarAdd5sTxt;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox CigarBalance5sTxt;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox ECigarRemarks;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox ECigarSubstractTxt;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox ECigarAddTxt;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox ECigarBalanceTxt;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.DataGridView ECigarDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn3;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RichTextBox SingleRemarksTxt;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox SingleSubstractTxt;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox SingleAddTxt;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox SinglesBalanceTxt;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DataGridView SinglesDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn4;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label TobaccoVendor10sLabel;
        private System.Windows.Forms.Label TobaccoVendor8sLabel;
        private System.Windows.Forms.ComboBox TobaccoSite10s;
        private System.Windows.Forms.ComboBox TobaccoSite8s;
        private System.Windows.Forms.ComboBox Tobacco10SReasonCmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox Tobacco8SReasonCmb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ChewReasonCombo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ChewSiteCombobox;
        private System.Windows.Forms.Label ChewSiteLabel;
        private System.Windows.Forms.ComboBox Cigar5sReasonCombo;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox CigarSite5sCmb;
        private System.Windows.Forms.Label CigarSite5sLabel;
        private System.Windows.Forms.ComboBox Cigar8sReasonCombo;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox CigarSite8sCmb;
        private System.Windows.Forms.Label CigarSite8sLabel;
        private System.Windows.Forms.ComboBox Cigar10sReasonCombo;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox CigarSite10sCmb;
        private System.Windows.Forms.Label CigarSite10sLabel;
        private System.Windows.Forms.ComboBox ECigarReasonCmb;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox ECigarSiteCmb;
        private System.Windows.Forms.Label ECigarSiteLabel;
        private System.Windows.Forms.ComboBox SinglesReasonCmb;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ComboBox SinglesSiteCmb;
        private System.Windows.Forms.Label SinglesSiteLabel;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox MilkReason310mlCmb;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox MilkSite310mlCmb;
        private System.Windows.Forms.Label MilkSite310Label;
        private System.Windows.Forms.RichTextBox MilkRemarksTxt;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox MilkSubtract310mlTxt;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox MilkAdd310mlTxt;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox MilkBalance310mlTxt;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.DataGridView MilkDgv;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn5;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox MilkReason473mlCmb;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox MilkSite473mlCmb;
        private System.Windows.Forms.Label MilkSite473Label;
        private System.Windows.Forms.TextBox MilkSubtract473mlTxt;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox MilkAdd473mlTxt;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox MilkBalance473mlTxt;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.ComboBox MilkReason1LCmb;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox MilkSite1LCmb;
        private System.Windows.Forms.Label MilkSite1LLabel;
        private System.Windows.Forms.TextBox MilkSubtract1LTxt;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox MilkAdd1LTxt;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox MilkBalance1LTxt;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ComboBox MilkReason2LCmb;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.ComboBox MilkSite2LCmb;
        private System.Windows.Forms.Label MilkSite2LLabel;
        private System.Windows.Forms.TextBox MilkSubtract2LTxt;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox MilkAdd2LTxt;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox MilkBalance2LTxt;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.ComboBox MilkReason4LCmb;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ComboBox MilkSite4LCmb;
        private System.Windows.Forms.Label MilkSite4LLabel;
        private System.Windows.Forms.TextBox MilkSubtract4LTxt;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox MilkAdd4LTxt;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox MilkBalance4LTxt;
        private System.Windows.Forms.Label label70;
    }
}