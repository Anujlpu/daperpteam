﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Transaction
{
    public partial class LottoWeeklyForm : Form
    {
        public LottoWeeklyForm()
        {
            InitializeComponent();
        }

        #region Private Methods
        private void BindWeeklyLotto()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var lottoList = context.Tbl_WeeklyLotto.Where(s => s.IsActive == true).Select(s => new
                {
                    s.ID,
                    s.InvoiceDate,
                    s.OnlineSold,
                    s.Activation,
                    s.Credit,
                    s.Redemptions,
                    s.RetailerBonus,
                    s.AmountDue
                }).ToList();

                dgvLotto.DataSource = lottoList;
                dgvLotto.Columns["ID"].Visible = false;
            }
        }
        private void CreateWeeklyLotto()
        {
            using (ShellEntities context = new ShellEntities())
            {
                Tbl_WeeklyLotto lotto = new Tbl_WeeklyLotto();
                lotto.InvoiceDate = Convert.ToDateTime(dtpInvoiceDate.Text);
                lotto.IsActive = true;
                lotto.CreatedDate = DateTime.Now;
                lotto.CreatedBy = 1;
                lotto.Redemptions = Convert.ToDouble(txtRedemtions.Text);
                lotto.AmountDue = Convert.ToDouble(txtAmountDue.Text);
                lotto.RetailerBonus = Convert.ToDouble(txtRetailerBonus.Text);
                lotto.OnlineSold = Convert.ToInt32(txtOnlineSold.Text);
                lotto.Activation = Convert.ToInt32(txtActivations.Text);
                lotto.Credit = Convert.ToInt32(txtCredits.Text);
                context.Tbl_WeeklyLotto.Add(lotto);
                context.SaveChanges();
            }
        }
        private void UpdateWeeklyLotto()
        {
            using (ShellEntities context = new ShellEntities())
            {
                Tbl_WeeklyLotto lotto = context.Tbl_WeeklyLotto.FirstOrDefault(s => s.ID == LottoID && s.IsActive == true);
                lotto.InvoiceDate = Convert.ToDateTime(dtpInvoiceDate.Text);
                lotto.IsActive = true;
                lotto.CreatedDate = DateTime.Now;
                lotto.CreatedBy = 1;
                lotto.Redemptions = Convert.ToDouble(txtRedemtions.Text);
                lotto.AmountDue = Convert.ToDouble(txtAmountDue.Text);
                lotto.RetailerBonus = Convert.ToDouble(txtRetailerBonus.Text);
                lotto.OnlineSold = Convert.ToInt32(txtOnlineSold.Text);
                lotto.Activation = Convert.ToInt32(txtActivations.Text);
                lotto.Credit = Convert.ToInt32(txtCredits.Text);
                context.SaveChanges();
            }
        }
        private void DeleteWeeklyLotto()
        {
            using (ShellEntities context = new ShellEntities())
            {
                Tbl_WeeklyLotto lotto = context.Tbl_WeeklyLotto.FirstOrDefault(s => s.ID == LottoID && s.IsActive == true);
                lotto.IsActive = false;
                context.SaveChanges();
            }
        }

        private void FillFields()
        {
            using (ShellEntities context = new ShellEntities())
            {
                Tbl_WeeklyLotto lotto = context.Tbl_WeeklyLotto.FirstOrDefault(s => s.ID == LottoID && s.IsActive == true);
                lotto.InvoiceDate = Convert.ToDateTime(dtpInvoiceDate.Text);
                lotto.IsActive = true;
                lotto.CreatedDate = DateTime.Now;
                lotto.CreatedBy = 1;
                txtRedemtions.Text = lotto.Redemptions.ToString();
                txtAmountDue.Text = lotto.AmountDue.ToString();
                txtRetailerBonus.Text = lotto.RetailerBonus.ToString();
                txtOnlineSold.Text = lotto.OnlineSold.ToString();
                txtActivations.Text = lotto.Activation.ToString();
                txtCredits.Text = lotto.Credit.ToString();
            }
        }

        private int LottoID = 0;
        #endregion

        #region Form Events
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CreateWeeklyLotto();
                BindWeeklyLotto();
                MessageBox.Show("Weekly invoice for lotto created.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateWeeklyLotto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteWeeklyLotto();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LottoWeeklyForm_Load(object sender, EventArgs e)
        {
            try
            {
                BindWeeklyLotto();
                button1.Enabled = false;
                BtnDelete.Enabled = false;
                BtnSave.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgvLotto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    LottoID = Convert.ToInt32(this.dgvLotto.Rows[e.RowIndex].Cells[0].Value);
                    FillFields();
                }

                button1.Enabled = true;
                BtnDelete.Enabled = true;
                BtnSave.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
            }
        }
        #endregion
    }
}
