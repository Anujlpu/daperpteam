﻿using DAL;
using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Transaction
{
    public partial class LottoMasterForm : BaseForm
    {
        int totalOpen = 0;
        int totalActualOpen = 0;
        int totalOpeningDiff = 0;
        int totalAdditionQty = 0;
        int totalSubtotal = 0;
        int totalActualClose = 0;
        int totalSoldQty = 0;
        int totalEodSoldQty = 0;
        int totalSoldValue = 0;
        int totalDiff = 0;
        public LottoMasterForm()
        {
            InitializeComponent();
        }
        List<string> docIds = new List<string>();
        #region Private Methods
        private void SaveMultipleDocs(string docType, DataGridView gridView)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime tranDate = Convert.ToDateTime(DateCalender.Text);
                    foreach (DataGridViewRow row in gridView.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            int docId = Convert.ToInt32((row.Cells[1].Value));
                            docIds.Add(row.Cells[1].Value.ToString());
                            var docDelete = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType && s.TransactionDate == tranDate).Select(s => new { s }).ToList();
                            foreach (var date in docDelete)
                            {
                                if (Convert.ToDateTime(date.s.TransactionDate) == tranDate)
                                {
                                    context.Tbl_EODCommonDocuments.Remove(date.s);
                                }
                            }
                            context.SaveChanges();
                            var docExist = context.Tbl_EODCommonDocuments.FirstOrDefault(s => s.UploadedDocID == docId && s.DocumentType == docType);
                            if (docExist == null)
                            {
                                Tbl_EODCommonDocuments docsObj = new Tbl_EODCommonDocuments();
                                docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                                docsObj.DocumentType = docType;
                                docsObj.InventoryType = docType;
                                docsObj.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                                docsObj.IsActive = true;
                                docsObj.CreatedBy = Utility.BA_Commman._userId;
                                docsObj.CreatedDate = DateTime.Now;
                                context.Tbl_EODCommonDocuments.Add(docsObj);
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LoadSavedDocuments(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType).Select(s => new DocumentList { DocId = s.UploadedDocID, TransactionDate = s.TransactionDate }).OrderByDescending(s => s.TransactionDate).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();

                foreach (DataGridViewRow item in gridView.Rows)
                {
                    var docs = docList.Where(s => s.DocId == Convert.ToInt64(item.Cells["DocId"].Value)).FirstOrDefault();
                    if (docs != null && docs.DocId == Convert.ToInt64(item.Cells["DocId"].Value))
                    {
                        item.Cells[0].Value = true;
                    }
                }
            }

        }
        private void BindSavedDocumentsGrid(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType).Select(s => new DocumentList { DocId = s.UploadedDocID, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();
                var selectedDocs = (from p in docList
                                    join s in context.Mst_DocUpload on p.DocId equals s.UploadId
                                    select new DocumentList
                                    {
                                        DocId = p.DocId,
                                        DocsIdentity = s.DocsIdentity,
                                        TransactionDate = p.TransactionDate
                                    }).ToList();
                if (selectedDocs.Count > 0)
                {
                    gridView.DataSource = selectedDocs;

                    gridView.Columns[1].Visible = false;
                    gridView.Columns[3].Visible = false;
                }

            }
        }
        private void IntializeDocumentsGrid(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Mst_DocUpload.Where(s => s.DocsType == docType).Select(s => new DocumentList { DocId = s.UploadId, DocsIdentity = s.DocsIdentity, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();
                gridView.DataSource = docList;
                gridView.Columns[1].Visible = false;
                gridView.Columns[3].Visible = false;
            }
        }
        private void BindDocumentsForAllGrid()
        {
            BindSavedDocumentsGrid(DocumentTypeConstants.LottoTypeUnActivated, UnActivatedDocsDgv);
            BindSavedDocumentsGrid(DocumentTypeConstants.LottoTypeActivated, ActivatedDocsDgv);
            BindSavedDocumentsGrid(DocumentTypeConstants.LottoScratchAndWinDoc, ScratchWinNormalDgv);
            BindSavedDocumentsGrid(DocumentTypeConstants.LottoScratchAndWinOnline, ScratchWinOnlineDocsDgv);
            BindSavedDocumentsGrid(DocumentTypeConstants.LottoWeekly, LottoWeeklyDgv);
        }

        private void TotalActivatedInventory()
        {
            int opening1 = 0, opening1_100 = 0, opening2 = 0, opening3 = 0, opening4 = 0, opening5 = 0, opening7 = 0,
                opening10 = 0, opening20 = 0, opening30 = 0, opening30_100 = 0, totalUnActivated = 0, totalActivated = 0;
            // Closing UnActivated
            opening1 = Convert.ToInt32(LottoUnActivatedClosing150Txt.Text);
            opening1_100 = Convert.ToInt32(LottoUnActivatedClosing1100Txt.Text);
            opening2 = Convert.ToInt32(LottoUnActivatedClosing2Txt.Text);
            opening3 = Convert.ToInt32(LottoUnActivatedClosing3Txt.Text);
            opening4 = Convert.ToInt32(LottoUnActivatedClosing4Txt.Text);
            opening5 = Convert.ToInt32(LottoUnActivatedClosing5Txt.Text);
            opening7 = Convert.ToInt32(LottoUnActivatedClosing7Txt.Text);
            opening10 = Convert.ToInt32(LottoUnActivatedClosing10Txt.Text);
            opening20 = Convert.ToInt32(LottoUnActivatedClosing20Txt.Text);
            opening30 = Convert.ToInt32(LottoUnActivatedClosing3050Txt.Text);
            opening30_100 = Convert.ToInt32(LottoUnActivatedClosing30100Txt.Text);
            totalUnActivated = (opening1 + opening1_100 + opening2 + opening3 + opening4 + opening5 + opening7 + opening10 + opening20 + opening30 + opening30_100);
            // Closing Activated

            opening1 = Convert.ToInt32(LottoActivatedClosing150Txt.Text);
            opening1_100 = Convert.ToInt32(LottoActivatedClosing1100Txt.Text);
            opening2 = Convert.ToInt32(LottoActivatedClosing2Txt.Text);
            opening3 = Convert.ToInt32(LottoActivatedClosing3Txt.Text);
            opening4 = Convert.ToInt32(LottoActivatedClosing4Txt.Text);
            opening5 = Convert.ToInt32(LottoActivatedClosing5Txt.Text);
            opening7 = Convert.ToInt32(LottoActivatedClosing7Txt.Text);
            opening10 = Convert.ToInt32(LottoActivatedClosing10Txt.Text);
            opening20 = Convert.ToInt32(LottoActivatedClosing20Txt.Text);
            opening30 = Convert.ToInt32(LottoActivatedClosing3050Txt.Text);
            opening30_100 = Convert.ToInt32(LottoActivatedClosing30100Txt.Text);

            totalActivated = (opening1 + opening1_100 + opening2 + opening3 + opening4 + opening5 + opening7 + opening10 + opening20 + opening30 + opening30_100);
            // Scratch Win Actual Close

            double actualClose1 = 0.00, actualClose2 = 0.00, actualClose3 = 0.00,
                actualClose4 = 0.00, actualClose5 = 0.00, actualClose7 = 0.00,
                actualClose10 = 0.00, actualClose20 = 0.00, actualClose30 = 0.00, totalActualClose = 0.00;
            actualClose1 = Convert.ToDouble(LottoScratchSubtotal1Txt.Text);
            actualClose2 = Convert.ToDouble(LottoScratchSubtotal2Txt.Text);
            actualClose3 = Convert.ToDouble(LottoScratchSubtotal3Txt.Text);
            actualClose4 = Convert.ToDouble(LottoScratchSubtotal4Txt.Text);
            actualClose5 = Convert.ToDouble(LottoScratchSubtotal5Txt.Text);
            actualClose7 = Convert.ToDouble(LottoScratchSubtotal7Txt.Text);
            actualClose10 = Convert.ToDouble(LottoScratchSubtotal10Txt.Text);
            actualClose20 = Convert.ToDouble(LottoScratchSubtotal20Txt.Text);
            actualClose30 = Convert.ToDouble(LottoScratchSubtotal30Txt.Text);
            totalActualClose = (actualClose1 + actualClose2 + actualClose3 + actualClose4 + actualClose5 + actualClose7 + actualClose10 + actualClose20 + actualClose30);

            int totalActivatedInv = (totalUnActivated + totalActivated + Convert.ToInt32(totalActualClose));

            TotalActivatedInvTxt.Text = totalActivatedInv.ToString();
        }
        private void IntialDocumentsLoad()
        {
            IntializeDocumentsGrid(DocumentTypeConstants.LottoTypeUnActivated, UnActivatedDocsDgv);
            IntializeDocumentsGrid(DocumentTypeConstants.LottoTypeActivated, ActivatedDocsDgv);
            IntializeDocumentsGrid(DocumentTypeConstants.LottoScratchAndWinDoc, ScratchWinNormalDgv);
            IntializeDocumentsGrid(DocumentTypeConstants.LottoScratchAndWinOnline, ScratchWinOnlineDocsDgv);
            IntializeDocumentsGrid(DocumentTypeConstants.LottoWeekly, LottoWeeklyDgv);
        }
        #region Lotto UnActivated

        private void unActivatedLottoFormula()
        {
            LottoUnActivatedPreviousDayOpening();
            int opening1 = 0, add1 = 0, substract1 = 0, prevOpening1 = 0, opening2 = 0, add2 = 0, substract2 = 0, opening3 = 0, add3 = 0, substract3 = 0
                    , opening4 = 0, add4 = 0, substract4 = 0, opening5 = 0, add5 = 0, substract5 = 0,
                    opening7 = 0, add7 = 0, substract7 = 0,
                    opening10 = 0, add10 = 0, substract10 = 0,
                    opening20 = 0, add20 = 0, substract20 = 0,
                    opening30 = 0, add30 = 0, substract30 = 0, total = 0,
                    prevOpening2 = 0, prevOpening3 = 0, prevOpening4 = 0, prevOpening5 = 0,
                    prevOpening7 = 0, prevOpening10 = 0, prevOpening20 = 0,
                    prevOpening30 = 0, opening1_100 = 0, add1_100 = 0, substract1_100 = 0,
                     opening30_100 = 0, add30_100 = 0, substract30_100 = 0,
                     prevOpening1_100 = 0, prevOpening30_100 = 0,
                     return1_50 = 0, return1_100 = 0, return2 = 0, return3 = 0,
                     return4 = 0, return5 = 0, return7 = 0, return10 = 0, return20 = 0,
                     return30_50 = 0, return30_100 = 0,
                      closing1_50 = 0, closing1_100 = 0, closing2 = 0, closing3 = 0,
                     closing4 = 0, closing5 = 0, closing7 = 0, closing10 = 0, closing20 = 0,
                     closing30_50 = 0, closing30_100 = 0;
            prevOpening1 = Convert.ToInt32(LottoUnActivatedOpening150Txt.Text);
            add1 = Convert.ToInt32(LottoUnActivatedAdd150Txt.Text);
            substract1 = Convert.ToInt32(LottoUnactivatedSubstract150Txt.Text);
            return1_50 = Convert.ToInt32(LottoUnActivatedReturn150Txt.Text);
            opening1 = (prevOpening1 + add1) - (substract1 + return1_50);
            prevOpening1_100 = Convert.ToInt32(LottoUnActivatedOpening1100Txt.Text);
            add1_100 = Convert.ToInt32(LottoUnActivatedAdd1100Txt.Text);
            substract1_100 = Convert.ToInt32(LottoUnactivatedSubstract1100Txt.Text);
            return1_100 = Convert.ToInt32(LottoUnActivatedReturn1100Txt.Text);
            opening1_100 = (prevOpening1_100 + add1_100) - (substract1_100 + return1_100);
            prevOpening2 = Convert.ToInt32(LottoUnActivatedOpening2Txt.Text);
            add2 = Convert.ToInt32(LottoUnActivatedAdd2Txt.Text);
            substract2 = Convert.ToInt32(LottoUnActivatedSubtract2Txt.Text);
            return2 = Convert.ToInt32(LottoUnActivatedReturn2Txt.Text);
            opening2 = (prevOpening2 + add2) - (substract2 + return2);
            prevOpening3 = Convert.ToInt32(LottoUnActivatedOpening3Txt.Text);
            add3 = Convert.ToInt32(LottoUnActivatedAdd3Txt.Text);
            substract3 = Convert.ToInt32(LottoUnActivatedSubtract3Txt.Text);
            return3 = Convert.ToInt32(LottoUnActivatedReturn3Txt.Text);
            opening3 = (prevOpening3 + add3) - (substract3 - return3);
            prevOpening4 = Convert.ToInt32(LottoUnActivatedOpening4Txt.Text);
            add4 = Convert.ToInt32(LottoUnActivatedAdd4Txt.Text);
            substract4 = Convert.ToInt32(LottoUnActivatedSubtract4Txt.Text);
            return4 = Convert.ToInt32(LottoUnActivatedReturn4Txt.Text);
            opening4 = (prevOpening4 + add4) - (substract4 - return4);
            prevOpening5 = Convert.ToInt32(LottoUnActivatedOpening5Txt.Text);
            add5 = Convert.ToInt32(LottoUnActivatedAdd5Txt.Text);
            substract5 = Convert.ToInt32(LottoUnActivatedSubtract5Txt.Text);
            return5 = Convert.ToInt32(LottoUnActivatedReturn5Txt.Text);
            opening5 = (prevOpening5 + add5) - (substract5 - return5);
            prevOpening7 = Convert.ToInt32(LottoUnActivatedOpening7Txt.Text);
            add7 = Convert.ToInt32(LottoUnActivatedAdd7Txt.Text);
            substract7 = Convert.ToInt32(LottoUnActivatedSubtract7Txt.Text);
            return7 = Convert.ToInt32(LottoUnActivatedReturn7Txt.Text);
            opening7 = (prevOpening7 + add7) - (substract7 - return7);
            prevOpening10 = Convert.ToInt32(LottoUnActivatedOpening10Txt.Text);
            add10 = Convert.ToInt32(LottoUnActivatedAdd10Txt.Text);
            substract10 = Convert.ToInt32(LottoUnActivatedSubtract10Txt.Text);
            return10 = Convert.ToInt32(LottoUnActivatedReturn10Txt.Text);
            opening10 = (prevOpening10 + add10) - (substract10 - return10);
            prevOpening20 = Convert.ToInt32(LottoUnActivatedOpening20Txt.Text);
            add20 = Convert.ToInt32(LottoUnActivatedAdd20Txt.Text);
            substract20 = Convert.ToInt32(LottoUnActivatedSubtract20Txt.Text);
            return20 = Convert.ToInt32(LottoUnActivatedReturn20Txt.Text);
            opening20 = (prevOpening20 + add20) - (substract20 - return20);
            prevOpening30 = Convert.ToInt32(LottoUnActivatedOpening30Txt.Text);
            add30 = Convert.ToInt32(LottoUnActivatedAdd30Txt.Text);
            substract30 = Convert.ToInt32(LottoUnActivatedSubtract30Txt.Text);
            return30_50 = Convert.ToInt32(LottoUnActivatedReturn3050Txt.Text);
            opening30 = (prevOpening30 + add30) - (substract30 - return30_50);
            prevOpening30_100 = Convert.ToInt32(LottoUnActivatedOpening30100Txt.Text);
            add30_100 = Convert.ToInt32(LottoUnActivatedAdd30100Txt.Text);
            substract30_100 = Convert.ToInt32(LottoUnActivatedSubtract30100Txt.Text);
            return30_100 = Convert.ToInt32(LottoUnActivatedReturn30100Txt.Text);
            opening30_100 = (prevOpening30_100 + add30_100) - (substract30_100 - return30_100);
            total = (opening1 + opening2 + opening3 + opening4 + opening5 + opening7 + opening10 + opening20 + opening30 + opening1_100 + opening30_100);
            LottoTotalUnActivatedTxt.Text = total.ToString();

            //Opening still zero

            LottoUnActivatedClosing150Txt.Text = opening1.ToString();
            LottoUnActivatedClosing1100Txt.Text = opening1_100.ToString();
            LottoUnActivatedClosing2Txt.Text = opening2.ToString();
            LottoUnActivatedClosing3Txt.Text = opening3.ToString();
            LottoUnActivatedClosing4Txt.Text = opening4.ToString();
            LottoUnActivatedClosing5Txt.Text = opening5.ToString();
            LottoUnActivatedClosing7Txt.Text = opening7.ToString();
            LottoUnActivatedClosing10Txt.Text = opening10.ToString();
            LottoUnActivatedClosing20Txt.Text = opening20.ToString();
            LottoUnActivatedClosing3050Txt.Text = opening30.ToString();
            LottoUnActivatedClosing30100Txt.Text = opening30_100.ToString();
        }
        private void LottoUnActivatedPreviousDayOpening()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var inventoryMaster = context.Tbl_LottoEODMasterInventory.Where(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.LottoTypeUnActivated).FirstOrDefault();
                    if (inventoryMaster != null)
                    {

                        LottoUnActivatedOpening150Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening1__50.ToString()) ? "0" : inventoryMaster.Opening1__50.ToString();
                        LottoUnActivatedOpening1100Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening1__100.ToString()) ? "0" : inventoryMaster.Opening1__100.ToString();
                        LottoUnActivatedOpening2Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening2_.ToString()) ? "0" : inventoryMaster.Opening2_.ToString();
                        LottoUnActivatedOpening3Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening3_.ToString()) ? "0" : inventoryMaster.Opening3_.ToString();
                        LottoUnActivatedOpening4Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening4_.ToString()) ? "0" : inventoryMaster.Opening4_.ToString();
                        LottoUnActivatedOpening5Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening5_.ToString()) ? "0" : inventoryMaster.Opening5_.ToString();
                        LottoUnActivatedOpening7Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening7_.ToString()) ? "0" : inventoryMaster.Opening7_.ToString();
                        LottoUnActivatedOpening10Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening10_.ToString()) ? "0" : inventoryMaster.Opening10_.ToString();
                        LottoUnActivatedOpening20Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening20_.ToString()) ? "0" : inventoryMaster.Opening20_.ToString();
                        LottoUnActivatedOpening30Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening30__50.ToString()) ? "0" : inventoryMaster.Opening30__50.ToString();
                        LottoUnActivatedOpening30100Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening30__100.ToString()) ? "0" : inventoryMaster.Opening30__100.ToString();
                    }
                    else
                    {

                        LottoUnActivatedOpening150Txt.Text = "0";
                        LottoUnActivatedOpening1100Txt.Text = "0";
                        LottoUnActivatedOpening2Txt.Text = "0";
                        LottoUnActivatedOpening3Txt.Text = "0";
                        LottoUnActivatedOpening4Txt.Text = "0";
                        LottoUnActivatedOpening5Txt.Text = "0";
                        LottoUnActivatedOpening7Txt.Text = "0";
                        LottoUnActivatedOpening10Txt.Text = "0";
                        LottoUnActivatedOpening20Txt.Text = "0";
                        LottoUnActivatedOpening30Txt.Text = "0";
                        LottoUnActivatedOpening30100Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void SaveLottoUnActivated()
        {
            try
            {
                int opening1 = 0, add1 = 0, substract1 = 0, opening2 = 0, add2 = 0, substract2 = 0, opening3 = 0, add3 = 0, substract3 = 0
                    , opening4 = 0, add4 = 0, substract4 = 0, opening5 = 0, add5 = 0, substract5 = 0,
                    opening7 = 0, add7 = 0, substract7 = 0,
                    opening10 = 0, add10 = 0, substract10 = 0,
                    opening20 = 0, add20 = 0, substract20 = 0,
                    opening30 = 0, add30 = 0, substract30 = 0,
                     opening30_100 = 0, add30_100 = 0, substract30_100 = 0,
                    opening100 = 0, add100 = 0, substract100 = 0,
                    closing1_50, closing1_100, closing2, closing3, closing4,
                    closing5, closing7, closing10, closing20, closing30_50, closing30_100,
                    return1_50, return1_100, return2, return3, return4,
                    return5, return7, return10, return20, return30_50, return30_100;

                opening1 = Convert.ToInt32(LottoUnActivatedOpening150Txt.Text);
                add1 = Convert.ToInt32(LottoUnActivatedAdd150Txt.Text);
                substract1 = Convert.ToInt32(LottoUnactivatedSubstract150Txt.Text);
                return1_50 = Convert.ToInt32(LottoUnActivatedReturn150Txt.Text);
                closing1_50 = Convert.ToInt32(LottoUnActivatedClosing150Txt.Text);
                opening100 = Convert.ToInt32(LottoUnActivatedOpening1100Txt.Text);
                add100 = Convert.ToInt32(LottoUnActivatedAdd1100Txt.Text);
                substract100 = Convert.ToInt32(LottoUnactivatedSubstract1100Txt.Text);
                return1_100 = Convert.ToInt32(LottoUnActivatedReturn1100Txt.Text);
                closing1_100 = Convert.ToInt32(LottoUnActivatedClosing1100Txt.Text);
                opening2 = Convert.ToInt32(LottoUnActivatedOpening2Txt.Text);
                add2 = Convert.ToInt32(LottoUnActivatedAdd2Txt.Text);
                substract2 = Convert.ToInt32(LottoUnActivatedSubtract2Txt.Text);
                return2 = Convert.ToInt32(LottoUnActivatedReturn2Txt.Text);
                closing2 = Convert.ToInt32(LottoUnActivatedClosing2Txt.Text);
                opening3 = Convert.ToInt32(LottoUnActivatedOpening3Txt.Text);
                add3 = Convert.ToInt32(LottoUnActivatedAdd3Txt.Text);
                substract3 = Convert.ToInt32(LottoUnActivatedSubtract3Txt.Text);
                return3 = Convert.ToInt32(LottoUnActivatedReturn3Txt.Text);
                closing3 = Convert.ToInt32(LottoUnActivatedClosing3Txt.Text);
                opening4 = Convert.ToInt32(LottoUnActivatedOpening4Txt.Text);
                add4 = Convert.ToInt32(LottoUnActivatedAdd4Txt.Text);
                substract4 = Convert.ToInt32(LottoUnActivatedSubtract4Txt.Text);
                return4 = Convert.ToInt32(LottoUnActivatedReturn4Txt.Text);
                closing4 = Convert.ToInt32(LottoUnActivatedClosing4Txt.Text);
                opening5 = Convert.ToInt32(LottoUnActivatedOpening5Txt.Text);
                add5 = Convert.ToInt32(LottoUnActivatedAdd5Txt.Text);
                substract5 = Convert.ToInt32(LottoUnActivatedSubtract5Txt.Text);
                return5 = Convert.ToInt32(LottoUnActivatedReturn5Txt.Text);
                closing5 = Convert.ToInt32(LottoUnActivatedClosing5Txt.Text);
                opening7 = Convert.ToInt32(LottoUnActivatedOpening7Txt.Text);
                add7 = Convert.ToInt32(LottoUnActivatedAdd7Txt.Text);
                substract7 = Convert.ToInt32(LottoUnActivatedSubtract7Txt.Text);
                return7 = Convert.ToInt32(LottoUnActivatedReturn7Txt.Text);
                closing7 = Convert.ToInt32(LottoUnActivatedClosing7Txt.Text);
                opening10 = Convert.ToInt32(LottoUnActivatedOpening10Txt.Text);
                add10 = Convert.ToInt32(LottoUnActivatedAdd10Txt.Text);
                substract10 = Convert.ToInt32(LottoUnActivatedSubtract10Txt.Text);
                return10 = Convert.ToInt32(LottoUnActivatedReturn10Txt.Text);
                closing10 = Convert.ToInt32(LottoUnActivatedClosing10Txt.Text);
                opening20 = Convert.ToInt32(LottoUnActivatedOpening20Txt.Text);
                add20 = Convert.ToInt32(LottoUnActivatedAdd20Txt.Text);
                substract20 = Convert.ToInt32(LottoUnActivatedSubtract20Txt.Text);
                return20 = Convert.ToInt32(LottoUnActivatedReturn20Txt.Text);
                closing20 = Convert.ToInt32(LottoUnActivatedClosing20Txt.Text);
                opening30 = Convert.ToInt32(LottoUnActivatedOpening30Txt.Text);
                add30 = Convert.ToInt32(LottoUnActivatedAdd30Txt.Text);
                substract30 = Convert.ToInt32(LottoUnActivatedSubtract30Txt.Text);
                return30_50 = Convert.ToInt32(LottoUnActivatedReturn3050Txt.Text);
                closing30_50 = Convert.ToInt32(LottoUnActivatedClosing3050Txt.Text);
                opening30_100 = Convert.ToInt32(LottoUnActivatedOpening30100Txt.Text);
                add30_100 = Convert.ToInt32(LottoUnActivatedAdd30100Txt.Text);
                substract30_100 = Convert.ToInt32(LottoUnActivatedSubtract30100Txt.Text);
                return30_100 = Convert.ToInt32(LottoUnActivatedReturn30100Txt.Text);
                closing30_100 = Convert.ToInt32(LottoUnActivatedClosing30100Txt.Text);

                // Opening Formula


                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var unactivatedLotto = context.Tbl_LottoEODMasterInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.LottoTypeUnActivated);
                    if (unactivatedLotto == null)
                    {
                        unactivatedLotto = new Tbl_LottoEODMasterInventory();
                        unactivatedLotto.Add1__50 = add1;
                        unactivatedLotto.Opening1__50 = opening1;
                        unactivatedLotto.Substract1__50 = substract1;
                        unactivatedLotto.Return1__50 = return1_50;
                        unactivatedLotto.Closing1__50 = closing1_50;
                        unactivatedLotto.Add1__100 = add100;
                        unactivatedLotto.Opening1__100 = opening100;
                        unactivatedLotto.Substract1__100 = substract100;
                        unactivatedLotto.Return1__100 = return1_100;
                        unactivatedLotto.Closing1__100 = closing1_100;
                        unactivatedLotto.Add2_ = add2;
                        unactivatedLotto.Opening2_ = opening2;
                        unactivatedLotto.Substract2_ = substract2;
                        unactivatedLotto.Return2_ = return2;
                        unactivatedLotto.Closing2_ = closing2;
                        unactivatedLotto.Add3_ = add3;
                        unactivatedLotto.Opening3_ = opening3;
                        unactivatedLotto.Substract3_ = substract3;
                        unactivatedLotto.Return3_ = return3;
                        unactivatedLotto.Closing3_ = closing3;
                        unactivatedLotto.Add4_ = add4;
                        unactivatedLotto.Opening4_ = opening4;
                        unactivatedLotto.Substract4_ = substract4;
                        unactivatedLotto.Return4_ = return4;
                        unactivatedLotto.Closing4_ = closing4;
                        unactivatedLotto.Add5_ = add5;
                        unactivatedLotto.Opening5_ = opening5;
                        unactivatedLotto.Substract5_ = substract5;
                        unactivatedLotto.Return5_ = return5;
                        unactivatedLotto.Closing5_ = closing5;
                        unactivatedLotto.Add7_ = add7;
                        unactivatedLotto.Opening7_ = opening7;
                        unactivatedLotto.Substract7_ = substract7;
                        unactivatedLotto.Return7_ = return7;
                        unactivatedLotto.Closing7_ = closing7;
                        unactivatedLotto.Add10_ = add10;
                        unactivatedLotto.Opening10_ = opening10;
                        unactivatedLotto.Substract10_ = substract10;
                        unactivatedLotto.Return10_ = return10;
                        unactivatedLotto.Closing10_ = closing10;
                        unactivatedLotto.Add20_ = add20;
                        unactivatedLotto.Opening20_ = opening20;
                        unactivatedLotto.Substract20_ = substract20;
                        unactivatedLotto.Return20_ = return20;
                        unactivatedLotto.Closing20_ = closing20;
                        unactivatedLotto.Add30__50 = add30;
                        unactivatedLotto.Opening30__50 = opening30;
                        unactivatedLotto.Substract30__50 = substract30;
                        unactivatedLotto.Return30__50 = return30_50;
                        unactivatedLotto.Closing30__50 = closing30_50;
                        unactivatedLotto.Add30__100 = add30_100;
                        unactivatedLotto.Opening30__100 = opening30_100;
                        unactivatedLotto.Substract30__100 = substract30_100;
                        unactivatedLotto.Return30__100 = return30_100;
                        unactivatedLotto.Closing30__100 = closing30_100;
                        unactivatedLotto.IsActive = true;
                        unactivatedLotto.CreatedBy = Utility.BA_Commman._userId;
                        unactivatedLotto.CreatedDate = DateTime.Now;
                        unactivatedLotto.Remarks = LottoUnActivatedRemakrsTxt.Text;
                        unactivatedLotto.TransactionDate = date;
                        unactivatedLotto.TotalUnActivatedLotto = Convert.ToInt32(LottoTotalUnActivatedTxt.Text);
                        unactivatedLotto.InventoryType = DocumentTypeConstants.LottoTypeUnActivated;
                        context.Tbl_LottoEODMasterInventory.Add(unactivatedLotto);
                    }
                    else
                    {
                        unactivatedLotto.Add1__50 = add1;
                        unactivatedLotto.Opening1__50 = opening1;
                        unactivatedLotto.Substract1__50 = substract1;
                        unactivatedLotto.Return1__50 = return1_50;
                        unactivatedLotto.Closing1__50 = closing1_50;
                        unactivatedLotto.Add1__100 = add100;
                        unactivatedLotto.Opening1__100 = opening100;
                        unactivatedLotto.Substract1__100 = substract100;
                        unactivatedLotto.Return1__100 = return1_100;
                        unactivatedLotto.Closing1__100 = closing1_100;
                        unactivatedLotto.Add2_ = add2;
                        unactivatedLotto.Opening2_ = opening2;
                        unactivatedLotto.Substract2_ = substract2;
                        unactivatedLotto.Return2_ = return2;
                        unactivatedLotto.Closing2_ = closing2;
                        unactivatedLotto.Add3_ = add3;
                        unactivatedLotto.Opening3_ = opening3;
                        unactivatedLotto.Substract3_ = substract3;
                        unactivatedLotto.Return3_ = return3;
                        unactivatedLotto.Closing3_ = closing3;
                        unactivatedLotto.Add4_ = add4;
                        unactivatedLotto.Opening4_ = opening4;
                        unactivatedLotto.Substract4_ = substract4;
                        unactivatedLotto.Return4_ = return4;
                        unactivatedLotto.Closing4_ = closing4;
                        unactivatedLotto.Add5_ = add5;
                        unactivatedLotto.Opening5_ = opening5;
                        unactivatedLotto.Substract5_ = substract5;
                        unactivatedLotto.Return5_ = return5;
                        unactivatedLotto.Closing5_ = closing5;
                        unactivatedLotto.Add7_ = add7;
                        unactivatedLotto.Opening7_ = opening7;
                        unactivatedLotto.Substract7_ = substract7;
                        unactivatedLotto.Return7_ = return7;
                        unactivatedLotto.Closing7_ = closing7;
                        unactivatedLotto.Add10_ = add10;
                        unactivatedLotto.Opening10_ = opening10;
                        unactivatedLotto.Substract10_ = substract10;
                        unactivatedLotto.Return10_ = return10;
                        unactivatedLotto.Closing10_ = closing10;
                        unactivatedLotto.Add20_ = add20;
                        unactivatedLotto.Opening20_ = opening20;
                        unactivatedLotto.Substract20_ = substract20;
                        unactivatedLotto.Return20_ = return20;
                        unactivatedLotto.Closing20_ = closing20;
                        unactivatedLotto.Add30__50 = add30;
                        unactivatedLotto.Opening30__50 = opening30;
                        unactivatedLotto.Substract30__50 = substract30;
                        unactivatedLotto.Return30__50 = return30_50;
                        unactivatedLotto.Closing30__50 = closing30_50;
                        unactivatedLotto.Add30__100 = add30_100;
                        unactivatedLotto.Opening30__100 = opening30_100;
                        unactivatedLotto.Substract30__100 = substract30_100;
                        unactivatedLotto.Return30__100 = return30_100;
                        unactivatedLotto.Closing30__100 = closing30_100;
                        unactivatedLotto.IsActive = true;
                        unactivatedLotto.ModifiedBy = Utility.BA_Commman._userId;
                        unactivatedLotto.ModifiedDate = DateTime.Now;
                        unactivatedLotto.Remarks = LottoUnActivatedRemakrsTxt.Text;
                        unactivatedLotto.TotalUnActivatedLotto = Convert.ToInt32(LottoTotalUnActivatedTxt.Text);
                    }
                    context.SaveChanges();
                    SaveMultipleDocs(DocumentTypeConstants.LottoTypeUnActivated, UnActivatedDocsDgv);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.InnerException);
            }
        }
        private void BindLottoUnActivatedData()
        {

            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);

                var lottoUnActivated = context.Tbl_LottoEODMasterInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.LottoTypeUnActivated);
                if (lottoUnActivated != null)
                {
                    LottoUnActivatedOpening150Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening1__50.ToString()) ? "0" : lottoUnActivated.Opening1__50.ToString();
                    LottoUnActivatedAdd150Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add1__50.ToString()) ? "0" : lottoUnActivated.Add1__50.ToString();
                    LottoUnactivatedSubstract150Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract1__50.ToString()) ? "0" : lottoUnActivated.Substract1__50.ToString();
                    LottoUnActivatedOpening1100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening1__100.ToString()) ? "0" : lottoUnActivated.Opening1__100.ToString();
                    LottoUnActivatedAdd1100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add1__100.ToString()) ? "0" : lottoUnActivated.Add1__100.ToString();
                    LottoUnactivatedSubstract1100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract1__100.ToString()) ? "0" : lottoUnActivated.Substract1__100.ToString();
                    LottoUnActivatedOpening2Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening2_.ToString()) ? "0" : lottoUnActivated.Opening2_.ToString();
                    LottoUnActivatedAdd2Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add2_.ToString()) ? "0" : lottoUnActivated.Add2_.ToString();
                    LottoUnActivatedSubtract2Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract2_.ToString()) ? "0" : lottoUnActivated.Substract2_.ToString();
                    LottoUnActivatedOpening3Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening3_.ToString()) ? "0" : lottoUnActivated.Opening3_.ToString();
                    LottoUnActivatedAdd3Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add3_.ToString()) ? "0" : lottoUnActivated.Add3_.ToString();
                    LottoUnActivatedSubtract3Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract3_.ToString()) ? "0" : lottoUnActivated.Substract3_.ToString();
                    LottoUnActivatedOpening4Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening4_.ToString()) ? "0" : lottoUnActivated.Opening4_.ToString();
                    LottoUnActivatedAdd4Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add4_.ToString()) ? "0" : lottoUnActivated.Add4_.ToString();
                    LottoUnActivatedSubtract4Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract4_.ToString()) ? "0" : lottoUnActivated.Substract4_.ToString();
                    LottoUnActivatedOpening5Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening5_.ToString()) ? "0" : lottoUnActivated.Opening5_.ToString();
                    LottoUnActivatedAdd5Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add5_.ToString()) ? "0" : lottoUnActivated.Add5_.ToString();
                    LottoUnActivatedSubtract5Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract5_.ToString()) ? "0" : lottoUnActivated.Substract5_.ToString();
                    LottoUnActivatedOpening7Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening7_.ToString()) ? "0" : lottoUnActivated.Opening7_.ToString();
                    LottoUnActivatedAdd7Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add7_.ToString()) ? "0" : lottoUnActivated.Add7_.ToString();
                    LottoUnActivatedSubtract7Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract7_.ToString()) ? "0" : lottoUnActivated.Substract7_.ToString();
                    LottoUnActivatedOpening10Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening10_.ToString()) ? "0" : lottoUnActivated.Opening10_.ToString();
                    LottoUnActivatedAdd10Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add10_.ToString()) ? "0" : lottoUnActivated.Add10_.ToString();
                    LottoUnActivatedSubtract10Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract10_.ToString()) ? "0" : lottoUnActivated.Substract10_.ToString();
                    LottoUnActivatedOpening20Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening20_.ToString()) ? "0" : lottoUnActivated.Opening20_.ToString();
                    LottoUnActivatedAdd20Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add20_.ToString()) ? "0" : lottoUnActivated.Add20_.ToString();
                    LottoUnActivatedSubtract20Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract20_.ToString()) ? "0" : lottoUnActivated.Substract20_.ToString();
                    LottoUnActivatedOpening30Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening30__50.ToString()) ? "0" : lottoUnActivated.Opening30__50.ToString();
                    LottoUnActivatedAdd30Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add30__50.ToString()) ? "0" : lottoUnActivated.Add30__50.ToString();
                    LottoUnActivatedSubtract30Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract30__50.ToString()) ? "0" : lottoUnActivated.Substract30__50.ToString();
                    LottoUnActivatedOpening30100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Opening30__100.ToString()) ? "0" : lottoUnActivated.Opening30__100.ToString();
                    LottoUnActivatedAdd30100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Add30__100.ToString()) ? "0" : lottoUnActivated.Add30__100.ToString();
                    LottoUnActivatedSubtract30100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Substract30__100.ToString()) ? "0" : lottoUnActivated.Substract30__100.ToString();
                    LottoTotalUnActivatedTxt.Text = string.IsNullOrEmpty(lottoUnActivated.TotalUnActivatedLotto.ToString()) ? "0" : lottoUnActivated.TotalUnActivatedLotto.ToString();

                    LottoUnActivatedReturn150Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return1__50.ToString()) ? "0" : lottoUnActivated.Return1__50.ToString();
                    LottoUnActivatedReturn1100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return1__100.ToString()) ? "0" : lottoUnActivated.Return1__100.ToString();
                    LottoUnActivatedReturn2Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return2_.ToString()) ? "0" : lottoUnActivated.Return2_.ToString();
                    LottoUnActivatedReturn3Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return3_.ToString()) ? "0" : lottoUnActivated.Return3_.ToString();
                    LottoUnActivatedReturn4Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return4_.ToString()) ? "0" : lottoUnActivated.Return4_.ToString();
                    LottoUnActivatedReturn5Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return5_.ToString()) ? "0" : lottoUnActivated.Return5_.ToString();
                    LottoUnActivatedReturn7Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return7_.ToString()) ? "0" : lottoUnActivated.Return7_.ToString();
                    LottoUnActivatedReturn10Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return10_.ToString()) ? "0" : lottoUnActivated.Return10_.ToString();
                    LottoUnActivatedReturn20Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return20_.ToString()) ? "0" : lottoUnActivated.Return20_.ToString();
                    LottoUnActivatedReturn3050Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return30__50.ToString()) ? "0" : lottoUnActivated.Return30__50.ToString();
                    LottoUnActivatedReturn30100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Return30__100.ToString()) ? "0" : lottoUnActivated.Return30__100.ToString();

                    LottoUnActivatedClosing150Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing1__50.ToString()) ? "0" : lottoUnActivated.Closing1__50.ToString();
                    LottoUnActivatedClosing1100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing1__100.ToString()) ? "0" : lottoUnActivated.Closing1__100.ToString();
                    LottoUnActivatedClosing2Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing2_.ToString()) ? "0" : lottoUnActivated.Closing2_.ToString();
                    LottoUnActivatedClosing3Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing3_.ToString()) ? "0" : lottoUnActivated.Closing3_.ToString();
                    LottoUnActivatedClosing4Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing4_.ToString()) ? "0" : lottoUnActivated.Closing4_.ToString();
                    LottoUnActivatedClosing5Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing5_.ToString()) ? "0" : lottoUnActivated.Closing5_.ToString();
                    LottoUnActivatedClosing7Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing7_.ToString()) ? "0" : lottoUnActivated.Closing7_.ToString();
                    LottoUnActivatedClosing10Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing10_.ToString()) ? "0" : lottoUnActivated.Closing10_.ToString();
                    LottoUnActivatedClosing20Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing20_.ToString()) ? "0" : lottoUnActivated.Closing20_.ToString();
                    LottoUnActivatedClosing3050Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing30__50.ToString()) ? "0" : lottoUnActivated.Closing30__50.ToString();
                    LottoUnActivatedClosing30100Txt.Text = string.IsNullOrEmpty(lottoUnActivated.Closing30__100.ToString()) ? "0" : lottoUnActivated.Closing30__100.ToString();
                }
                else
                {
                    LottoUnActivatedOpening150Txt.Text = "0";
                    LottoUnActivatedOpening1100Txt.Text = "0";
                    LottoUnActivatedAdd150Txt.Text = "0";
                    LottoUnActivatedAdd1100Txt.Text = "0";
                    LottoUnactivatedSubstract150Txt.Text = "0";
                    LottoUnactivatedSubstract1100Txt.Text = "0";
                    LottoUnActivatedOpening2Txt.Text = "0";
                    LottoUnActivatedAdd2Txt.Text = "0";
                    LottoUnActivatedSubtract2Txt.Text = "0";
                    LottoUnActivatedOpening3Txt.Text = "0";
                    LottoUnActivatedAdd3Txt.Text = "0";
                    LottoUnActivatedSubtract3Txt.Text = "0";
                    LottoUnActivatedOpening4Txt.Text = "0";
                    LottoUnActivatedAdd4Txt.Text = "0";
                    LottoUnActivatedSubtract4Txt.Text = "0";
                    LottoUnActivatedOpening5Txt.Text = "0";
                    LottoUnActivatedAdd5Txt.Text = "0";
                    LottoUnActivatedSubtract5Txt.Text = "0";
                    LottoUnActivatedOpening7Txt.Text = "0";
                    LottoUnActivatedAdd7Txt.Text = "0";
                    LottoUnActivatedSubtract7Txt.Text = "0";
                    LottoUnActivatedOpening10Txt.Text = "0";
                    LottoUnActivatedAdd10Txt.Text = "0";
                    LottoUnActivatedSubtract10Txt.Text = "0";
                    LottoUnActivatedOpening20Txt.Text = "0";
                    LottoUnActivatedAdd20Txt.Text = "0";
                    LottoUnActivatedSubtract20Txt.Text = "0";
                    LottoUnActivatedOpening30Txt.Text = "0";
                    LottoUnActivatedOpening30100Txt.Text = "0";
                    LottoUnActivatedAdd30Txt.Text = "0";
                    LottoUnActivatedAdd30100Txt.Text = "0";
                    LottoUnActivatedSubtract30Txt.Text = "0";
                    LottoUnActivatedSubtract30100Txt.Text = "0";
                    LottoTotalUnActivatedTxt.Text = "0";

                    LottoUnActivatedReturn150Txt.Text = "0";
                    LottoUnActivatedReturn1100Txt.Text = "0";
                    LottoUnActivatedReturn2Txt.Text = "0";
                    LottoUnActivatedReturn3Txt.Text = "0";
                    LottoUnActivatedReturn4Txt.Text = "0";
                    LottoUnActivatedReturn5Txt.Text = "0";
                    LottoUnActivatedReturn7Txt.Text = "0";
                    LottoUnActivatedReturn10Txt.Text = "0";
                    LottoUnActivatedReturn20Txt.Text = "0";
                    LottoUnActivatedReturn3050Txt.Text = "0";
                    LottoUnActivatedReturn30100Txt.Text = "0";

                    LottoUnActivatedClosing150Txt.Text = "0";
                    LottoUnActivatedClosing1100Txt.Text = "0";
                    LottoUnActivatedClosing2Txt.Text = "0";
                    LottoUnActivatedClosing3Txt.Text = "0";
                    LottoUnActivatedClosing4Txt.Text = "0";
                    LottoUnActivatedClosing5Txt.Text = "0";
                    LottoUnActivatedClosing7Txt.Text = "0";
                    LottoUnActivatedClosing10Txt.Text = "0";
                    LottoUnActivatedClosing20Txt.Text = "0";
                    LottoUnActivatedClosing3050Txt.Text = "0";
                    LottoUnActivatedClosing30100Txt.Text = "0";

                }
                BindLottoUnActivatedDocuments();
            }
        }
        private void BindLottoUnActivatedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.LottoTypeUnActivated, UnActivatedDocsDgv);
        }
        private void AdditionToActivatedSectionAutoPopulate()
        {
            int substract1_50 = 0, substract1_100 = 0, substract2 = 0,
                substract3 = 0, substract4 = 0, substract5 = 0
                , substract7 = 0, substract10 = 0,
                substract20 = 0, substract30_50 = 0, substract30_100 = 0;
            substract1_50 = Convert.ToInt32(LottoUnactivatedSubstract150Txt.Text);
            substract1_100 = Convert.ToInt32(LottoUnactivatedSubstract1100Txt.Text);
            substract2 = Convert.ToInt32(LottoUnActivatedSubtract2Txt.Text);
            substract3 = Convert.ToInt32(LottoUnActivatedSubtract3Txt.Text);
            substract4 = Convert.ToInt32(LottoUnActivatedSubtract4Txt.Text);
            substract5 = Convert.ToInt32(LottoUnActivatedSubtract5Txt.Text);
            substract7 = Convert.ToInt32(LottoUnActivatedSubtract7Txt.Text);
            substract10 = Convert.ToInt32(LottoUnActivatedSubtract10Txt.Text);
            substract20 = Convert.ToInt32(LottoUnActivatedSubtract20Txt.Text);
            substract30_50 = Convert.ToInt32(LottoUnActivatedSubtract30Txt.Text);
            substract30_100 = Convert.ToInt32(LottoUnActivatedSubtract30100Txt.Text);
            LottoActivatedAdd150Txt.Text = (substract1_50 * 50).ToString();
            LottoActivatedAdd1100Txt.Text = (substract1_100 * 100).ToString();
            LottoActivatedAdd2Txt.Text = (substract2 * 50).ToString();
            LottoActivatedAdd3Txt.Text = (substract3 * 50).ToString();
            LottoActivatedAdd4Txt.Text = (substract4 * 30).ToString();
            LottoActivatedAdd5Txt.Text = (substract5 * 20).ToString();
            LottoActivatedAdd7Txt.Text = (substract7 * 20).ToString();
            LottoActivatedAdd10Txt.Text = (substract10 * 10).ToString();
            LottoActivatedAdd20Txt.Text = (substract20 * 10).ToString();
            LottoActivatedAdd30Txt.Text = (substract30_50 * 50).ToString();
            LottoActivatedAdd30100Txt.Text = (substract30_100 * 100).ToString();

        }
        #endregion
        #region Lotto Activated
        private void LottoActivatedPreviousDayOpening()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var inventoryMaster = context.Tbl_LottoEODMasterInventory.Where(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.LottoTypeActivated).FirstOrDefault();
                    if (inventoryMaster != null)
                    {

                        LottoActivatedOpening150Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening1__50.ToString()) ? "0" : inventoryMaster.Opening1__50.ToString();
                        LottoActivatedOpening1100Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening1__100.ToString()) ? "0" : inventoryMaster.Opening1__100.ToString();
                        LottoActivatedOpening2Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening2_.ToString()) ? "0" : inventoryMaster.Opening2_.ToString();
                        LottoActivatedOpening3Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening3_.ToString()) ? "0" : inventoryMaster.Opening3_.ToString();
                        LottoActivatedOpening4Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening4_.ToString()) ? "0" : inventoryMaster.Opening4_.ToString();
                        LottoActivatedOpening5Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening5_.ToString()) ? "0" : inventoryMaster.Opening5_.ToString();
                        LottoActivatedOpening7Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening7_.ToString()) ? "0" : inventoryMaster.Opening7_.ToString();
                        LottoActivatedOpening10Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening10_.ToString()) ? "0" : inventoryMaster.Opening10_.ToString();
                        LottoActivatedOpening20Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening20_.ToString()) ? "0" : inventoryMaster.Opening20_.ToString();
                        LottoActivatedOpening30Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening30__50.ToString()) ? "0" : inventoryMaster.Opening30__50.ToString();
                        LottoActivatedOpening30100Txt.Text = string.IsNullOrEmpty(inventoryMaster.Opening30__100.ToString()) ? "0" : inventoryMaster.Opening30__100.ToString();
                    }
                    else
                    {

                        LottoActivatedOpening150Txt.Text = "0";
                        LottoActivatedOpening1100Txt.Text = "0";
                        LottoActivatedOpening2Txt.Text = "0";
                        LottoActivatedOpening3Txt.Text = "0";
                        LottoActivatedOpening4Txt.Text = "0";
                        LottoActivatedOpening5Txt.Text = "0";
                        LottoUnActivatedOpening5Txt.Text = "0";
                        LottoActivatedOpening7Txt.Text = "0";
                        LottoActivatedOpening10Txt.Text = "0";
                        LottoActivatedOpening20Txt.Text = "0";
                        LottoActivatedOpening30Txt.Text = "0";
                        LottoActivatedOpening30100Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void SaveLottoActivated()
        {
            try
            {
                int opening1 = 0, add1 = 0, substract1 = 0, opening2 = 0, add2 = 0, substract2 = 0, opening3 = 0, add3 = 0, substract3 = 0
                    , opening4 = 0, add4 = 0, substract4 = 0, opening5 = 0, add5 = 0, substract5 = 0,
                    opening7 = 0, add7 = 0, substract7 = 0,
                    opening10 = 0, add10 = 0, substract10 = 0,
                    opening20 = 0, add20 = 0, substract20 = 0,
                    opening30 = 0, add30 = 0, substract30 = 0,
                    opening100 = 0, add100 = 0, substract100 = 0,
                     opening30_100 = 0, add30_100 = 0, substract30_100 = 0,
                     closing1_50, closing1_100, closing2, closing3, closing4,
                    closing5, closing7, closing10, closing20, closing30_50, closing30_100,
                    return1_50, return1_100, return2, return3, return4,
                    return5, return7, return10, return20, return30_50, return30_100;
                opening1 = Convert.ToInt32(LottoActivatedOpening150Txt.Text);
                add1 = Convert.ToInt32(LottoActivatedAdd150Txt.Text);
                substract1 = Convert.ToInt32(LottoActivatedSubstract150Txt.Text);
                opening100 = Convert.ToInt32(LottoActivatedOpening1100Txt.Text);
                add100 = Convert.ToInt32(LottoActivatedAdd1100Txt.Text);
                substract100 = Convert.ToInt32(LottoActivatedSubstract1100Txt.Text);
                opening2 = Convert.ToInt32(LottoActivatedOpening2Txt.Text);
                add2 = Convert.ToInt32(LottoActivatedAdd2Txt.Text);
                substract2 = Convert.ToInt32(LottoActivatedSubstract2Txt.Text);
                opening3 = Convert.ToInt32(LottoActivatedOpening3Txt.Text);
                add3 = Convert.ToInt32(LottoActivatedAdd3Txt.Text);
                substract3 = Convert.ToInt32(LottoActivatedSubstract3Txt.Text);
                opening4 = Convert.ToInt32(LottoActivatedOpening4Txt.Text);
                add4 = Convert.ToInt32(LottoActivatedAdd4Txt.Text);
                substract4 = Convert.ToInt32(LottoActivatedSubstract4Txt.Text);
                opening5 = Convert.ToInt32(LottoActivatedOpening5Txt.Text);
                add5 = Convert.ToInt32(LottoActivatedAdd5Txt.Text);
                substract5 = Convert.ToInt32(LottoActivatedSubstract5Txt.Text);
                opening7 = Convert.ToInt32(LottoActivatedOpening7Txt.Text);
                add7 = Convert.ToInt32(LottoActivatedAdd7Txt.Text);
                substract7 = Convert.ToInt32(LottoActivatedSubstract7Txt.Text);
                opening10 = Convert.ToInt32(LottoActivatedOpening10Txt.Text);
                add10 = Convert.ToInt32(LottoActivatedAdd10Txt.Text);
                substract10 = Convert.ToInt32(LottoActivatedSubstract10Txt.Text);
                opening20 = Convert.ToInt32(LottoActivatedOpening20Txt.Text);
                add20 = Convert.ToInt32(LottoActivatedAdd20Txt.Text);
                substract20 = Convert.ToInt32(LottoActivatedSubstract20Txt.Text);
                opening30 = Convert.ToInt32(LottoActivatedOpening30Txt.Text);
                add30 = Convert.ToInt32(LottoActivatedAdd30Txt.Text);
                substract30 = Convert.ToInt32(LottoActivatedSubstract30Txt.Text);
                opening30_100 = Convert.ToInt32(LottoActivatedOpening30100Txt.Text);
                add30_100 = Convert.ToInt32(LottoActivatedAdd30100Txt.Text);
                substract30_100 = Convert.ToInt32(LottoActivatedSubstract30100Txt.Text);

                // Return And Closing
                return1_50 = Convert.ToInt32(LottoActivatedReturn150Txt.Text);
                return1_100 = Convert.ToInt32(LottoActivatedReturn1100Txt.Text);
                return2 = Convert.ToInt32(LottoActivatedReturn2Txt.Text);
                return3 = Convert.ToInt32(LottoActivatedReturn3Txt.Text);
                return4 = Convert.ToInt32(LottoActivatedReturn4Txt.Text);
                return5 = Convert.ToInt32(LottoActivatedReturn5Txt.Text);
                return7 = Convert.ToInt32(LottoActivatedReturn7Txt.Text);
                return10 = Convert.ToInt32(LottoActivatedReturn10Txt.Text);
                return20 = Convert.ToInt32(LottoActivatedReturn20Txt.Text);
                return30_50 = Convert.ToInt32(LottoUnActivatedReturn3050Txt.Text);
                return30_100 = Convert.ToInt32(LottoUnActivatedReturn30100Txt.Text);
                closing1_50 = Convert.ToInt32(LottoActivatedClosing150Txt.Text);
                closing1_100 = Convert.ToInt32(LottoActivatedClosing1100Txt.Text);
                closing2 = Convert.ToInt32(LottoActivatedClosing2Txt.Text);
                closing3 = Convert.ToInt32(LottoActivatedClosing3Txt.Text);
                closing4 = Convert.ToInt32(LottoActivatedClosing4Txt.Text);
                closing5 = Convert.ToInt32(LottoActivatedClosing5Txt.Text);
                closing7 = Convert.ToInt32(LottoActivatedClosing7Txt.Text);
                closing10 = Convert.ToInt32(LottoActivatedClosing10Txt.Text);
                closing20 = Convert.ToInt32(LottoActivatedClosing20Txt.Text);
                closing30_50 = Convert.ToInt32(LottoActivatedClosing3050Txt.Text);
                closing30_100 = Convert.ToInt32(LottoActivatedClosing30100Txt.Text);

                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var activatedLotto = context.Tbl_LottoEODMasterInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.LottoTypeActivated);
                    if (activatedLotto == null)
                    {
                        activatedLotto = new Tbl_LottoEODMasterInventory();
                        activatedLotto.Add1__50 = add1;
                        activatedLotto.Opening1__50 = opening1;
                        activatedLotto.Substract1__50 = substract1;
                        activatedLotto.Add1__100 = add100;
                        activatedLotto.Opening1__100 = opening100;
                        activatedLotto.Substract1__100 = substract100;
                        activatedLotto.Add2_ = add2;
                        activatedLotto.Opening2_ = opening2;
                        activatedLotto.Substract2_ = substract2;
                        activatedLotto.Add3_ = add3;
                        activatedLotto.Opening3_ = opening3;
                        activatedLotto.Substract3_ = substract3;
                        activatedLotto.Add4_ = add4;
                        activatedLotto.Opening4_ = opening4;
                        activatedLotto.Substract4_ = substract4;
                        activatedLotto.Add5_ = add5;
                        activatedLotto.Opening5_ = opening5;
                        activatedLotto.Substract5_ = substract5;
                        activatedLotto.Add7_ = add7;
                        activatedLotto.Opening7_ = opening7;
                        activatedLotto.Substract7_ = substract7;
                        activatedLotto.Add10_ = add10;
                        activatedLotto.Opening10_ = opening10;
                        activatedLotto.Substract10_ = substract10;
                        activatedLotto.Add20_ = add20;
                        activatedLotto.Opening20_ = opening20;
                        activatedLotto.Substract20_ = substract20;
                        activatedLotto.Add30__50 = add30;
                        activatedLotto.Opening30__50 = opening30;
                        activatedLotto.Substract30__50 = substract30;
                        activatedLotto.Opening30__100 = opening30_100;
                        activatedLotto.Add30__100 = add30_100;
                        activatedLotto.Substract30__100 = substract30_100;
                        activatedLotto.Return1__50 = return1_50;
                        activatedLotto.Return1__100 = return1_100;
                        activatedLotto.Return2_ = return2;
                        activatedLotto.Return3_ = return3;
                        activatedLotto.Return4_ = return4;
                        activatedLotto.Return5_ = return5;
                        activatedLotto.Return7_ = return7;
                        activatedLotto.Return10_ = return10;
                        activatedLotto.Return20_ = return20;
                        activatedLotto.Return30__50 = return30_50;
                        activatedLotto.Return30__100 = return30_100;
                        activatedLotto.Closing1__50 = closing1_50;
                        activatedLotto.Closing1__100 = closing1_100;
                        activatedLotto.Closing2_ = closing2;
                        activatedLotto.Closing3_ = closing3;
                        activatedLotto.Closing4_ = closing4;
                        activatedLotto.Closing5_ = closing5;
                        activatedLotto.Closing7_ = closing7;
                        activatedLotto.Closing10_ = closing10;
                        activatedLotto.Closing20_ = closing20;
                        activatedLotto.Closing30__50 = closing30_50;
                        activatedLotto.Closing30__100 = closing30_100;
                        activatedLotto.IsActive = true;
                        activatedLotto.CreatedBy = Utility.BA_Commman._userId;
                        activatedLotto.CreatedDate = DateTime.Now;
                        activatedLotto.Remarks = LottoActivatedRemarksTxt.Text;
                        activatedLotto.TransactionDate = date;
                        activatedLotto.TotalActivatedLotto = Convert.ToInt32(LottoActivatedTotalTxt.Text);
                        activatedLotto.InventoryType = DocumentTypeConstants.LottoTypeActivated;
                        context.Tbl_LottoEODMasterInventory.Add(activatedLotto);
                    }
                    else
                    {
                        activatedLotto.Add1__50 = add1;
                        activatedLotto.Opening1__50 = opening1;
                        activatedLotto.Substract1__50 = substract1;
                        activatedLotto.Add1__100 = add100;
                        activatedLotto.Opening1__100 = opening100;
                        activatedLotto.Substract1__100 = substract100;
                        activatedLotto.Add2_ = add2;
                        activatedLotto.Opening2_ = opening2;
                        activatedLotto.Substract2_ = substract2;
                        activatedLotto.Add3_ = add3;
                        activatedLotto.Opening3_ = opening3;
                        activatedLotto.Substract3_ = substract3;
                        activatedLotto.Add4_ = add4;
                        activatedLotto.Opening4_ = opening4;
                        activatedLotto.Substract4_ = substract4;
                        activatedLotto.Add5_ = add5;
                        activatedLotto.Opening5_ = opening5;
                        activatedLotto.Substract5_ = substract5;
                        activatedLotto.Add7_ = add7;
                        activatedLotto.Opening7_ = opening7;
                        activatedLotto.Substract7_ = substract7;
                        activatedLotto.Add10_ = add10;
                        activatedLotto.Opening10_ = opening10;
                        activatedLotto.Substract10_ = substract10;
                        activatedLotto.Add20_ = add20;
                        activatedLotto.Opening20_ = opening20;
                        activatedLotto.Substract20_ = substract20;
                        activatedLotto.Add30__50 = add30;
                        activatedLotto.Opening30__50 = opening30;
                        activatedLotto.Substract30__50 = substract30;
                        activatedLotto.Opening30__100 = opening30_100;
                        activatedLotto.Add30__100 = add30_100;
                        activatedLotto.Substract30__100 = substract30_100;
                        activatedLotto.IsActive = true;
                        activatedLotto.ModifiedBy = Utility.BA_Commman._userId;
                        activatedLotto.ModifiedDate = DateTime.Now;
                        activatedLotto.Remarks = LottoActivatedRemarksTxt.Text;
                        activatedLotto.TotalActivatedLotto = Convert.ToInt32(LottoActivatedTotalTxt.Text);
                        activatedLotto.Return1__50 = return1_50;
                        activatedLotto.Return1__100 = return1_100;
                        activatedLotto.Return2_ = return2;
                        activatedLotto.Return3_ = return3;
                        activatedLotto.Return4_ = return4;
                        activatedLotto.Return5_ = return5;
                        activatedLotto.Return7_ = return7;
                        activatedLotto.Return10_ = return10;
                        activatedLotto.Return20_ = return20;
                        activatedLotto.Return30__50 = return30_50;
                        activatedLotto.Return30__100 = return30_100;
                        activatedLotto.Closing1__50 = closing1_50;
                        activatedLotto.Closing1__100 = closing1_100;
                        activatedLotto.Closing2_ = closing2;
                        activatedLotto.Closing3_ = closing3;
                        activatedLotto.Closing4_ = closing4;
                        activatedLotto.Closing5_ = closing5;
                        activatedLotto.Closing7_ = closing7;
                        activatedLotto.Closing10_ = closing10;
                        activatedLotto.Closing20_ = closing20;
                        activatedLotto.Closing30__50 = closing30_50;
                        activatedLotto.Closing30__100 = closing30_100;
                    }
                    context.SaveChanges();
                    SaveMultipleDocs(DocumentTypeConstants.LottoTypeActivated, ActivatedDocsDgv);

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.InnerException);
            }
        }
        private void BindLottoActivatedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.LottoTypeActivated, ActivatedDocsDgv);
        }
        private void BindLottoActivatedData()
        {

            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);

                var lottoEODInventory = context.Tbl_LottoEODMasterInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.LottoTypeActivated);
                if (lottoEODInventory != null)
                {
                    LottoActivatedOpening150Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening1__50.ToString()) ? "0" : lottoEODInventory.Opening1__50.ToString();
                    LottoActivatedAdd150Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add1__50.ToString()) ? "0" : lottoEODInventory.Add1__50.ToString();
                    LottoActivatedSubstract1100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract1__50.ToString()) ? "0" : lottoEODInventory.Substract1__50.ToString();
                    LottoActivatedOpening1100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening1__100.ToString()) ? "0" : lottoEODInventory.Opening1__100.ToString();
                    LottoActivatedAdd1100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add1__100.ToString()) ? "0" : lottoEODInventory.Add1__100.ToString();
                    LottoActivatedSubstract150Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract1__100.ToString()) ? "0" : lottoEODInventory.Substract1__100.ToString();
                    LottoActivatedOpening2Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening2_.ToString()) ? "0" : lottoEODInventory.Opening2_.ToString();
                    LottoActivatedAdd2Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add2_.ToString()) ? "0" : lottoEODInventory.Add2_.ToString();
                    LottoActivatedSubstract2Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract2_.ToString()) ? "0" : lottoEODInventory.Substract2_.ToString();
                    LottoActivatedOpening3Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening3_.ToString()) ? "0" : lottoEODInventory.Opening3_.ToString();
                    LottoActivatedAdd3Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add3_.ToString()) ? "0" : lottoEODInventory.Add3_.ToString();
                    LottoActivatedSubstract3Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract3_.ToString()) ? "0" : lottoEODInventory.Substract3_.ToString();
                    LottoActivatedOpening4Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening4_.ToString()) ? "0" : lottoEODInventory.Opening4_.ToString();
                    LottoActivatedAdd4Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add4_.ToString()) ? "0" : lottoEODInventory.Add4_.ToString();
                    LottoActivatedSubstract4Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract4_.ToString()) ? "0" : lottoEODInventory.Substract4_.ToString();
                    LottoActivatedOpening5Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening5_.ToString()) ? "0" : lottoEODInventory.Opening5_.ToString();
                    LottoActivatedAdd5Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add5_.ToString()) ? "0" : lottoEODInventory.Add5_.ToString();
                    LottoActivatedSubstract5Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract5_.ToString()) ? "0" : lottoEODInventory.Substract5_.ToString();
                    LottoActivatedOpening7Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening7_.ToString()) ? "0" : lottoEODInventory.Opening7_.ToString();
                    LottoActivatedAdd7Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add7_.ToString()) ? "0" : lottoEODInventory.Add7_.ToString();
                    LottoActivatedSubstract7Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract7_.ToString()) ? "0" : lottoEODInventory.Substract7_.ToString();
                    LottoActivatedOpening10Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening10_.ToString()) ? "0" : lottoEODInventory.Opening10_.ToString();
                    LottoActivatedAdd10Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add10_.ToString()) ? "0" : lottoEODInventory.Add10_.ToString();
                    LottoActivatedSubstract10Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract10_.ToString()) ? "0" : lottoEODInventory.Substract10_.ToString();
                    LottoActivatedOpening20Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening20_.ToString()) ? "0" : lottoEODInventory.Opening20_.ToString();
                    LottoActivatedAdd20Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add20_.ToString()) ? "0" : lottoEODInventory.Add20_.ToString();
                    LottoActivatedSubstract20Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract20_.ToString()) ? "0" : lottoEODInventory.Substract20_.ToString();
                    LottoActivatedOpening30Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening30__50.ToString()) ? "0" : lottoEODInventory.Opening30__50.ToString();
                    LottoActivatedAdd30Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add30__50.ToString()) ? "0" : lottoEODInventory.Opening30__50.ToString();
                    LottoActivatedSubstract30Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract30__50.ToString()) ? "0" : lottoEODInventory.Substract30__50.ToString();
                    LottoActivatedOpening30100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Opening30__100.ToString()) ? "0" : lottoEODInventory.Opening30__100.ToString();
                    LottoActivatedAdd30100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Add30__100.ToString()) ? "0" : lottoEODInventory.Add30__100.ToString();
                    LottoActivatedSubstract30100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Substract30__100.ToString()) ? "0" : lottoEODInventory.Substract30__100.ToString();
                    LottoActivatedTotalTxt.Text = string.IsNullOrEmpty(lottoEODInventory.TotalActivatedLotto.ToString()) ? "0" : lottoEODInventory.TotalActivatedLotto.ToString();

                    LottoActivatedReturn150Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return1__50.ToString()) ? "0" : lottoEODInventory.Return1__50.ToString();
                    LottoActivatedReturn1100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return1__100.ToString()) ? "0" : lottoEODInventory.Return1__100.ToString();
                    LottoActivatedReturn2Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return2_.ToString()) ? "0" : lottoEODInventory.Return2_.ToString();
                    LottoActivatedReturn3Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return3_.ToString()) ? "0" : lottoEODInventory.Return3_.ToString();
                    LottoActivatedReturn4Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return4_.ToString()) ? "0" : lottoEODInventory.Return4_.ToString();
                    LottoActivatedReturn5Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return5_.ToString()) ? "0" : lottoEODInventory.Return5_.ToString();
                    LottoActivatedReturn7Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return7_.ToString()) ? "0" : lottoEODInventory.Return7_.ToString();
                    LottoActivatedReturn10Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return10_.ToString()) ? "0" : lottoEODInventory.Return10_.ToString();
                    LottoActivatedReturn20Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return20_.ToString()) ? "0" : lottoEODInventory.Return20_.ToString();
                    LottoActivatedReturn350Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return30__50.ToString()) ? "0" : lottoEODInventory.Return30__50.ToString();
                    LottoActivatedReturn30100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Return30__100.ToString()) ? "0" : lottoEODInventory.Return30__100.ToString();

                    LottoActivatedClosing150Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing1__50.ToString()) ? "0" : lottoEODInventory.Closing1__50.ToString();
                    LottoActivatedClosing1100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing1__100.ToString()) ? "0" : lottoEODInventory.Closing1__100.ToString();
                    LottoActivatedClosing2Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing2_.ToString()) ? "0" : lottoEODInventory.Closing2_.ToString();
                    LottoActivatedClosing3Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing3_.ToString()) ? "0" : lottoEODInventory.Closing3_.ToString();
                    LottoActivatedClosing4Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing4_.ToString()) ? "0" : lottoEODInventory.Closing4_.ToString();
                    LottoActivatedClosing5Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing5_.ToString()) ? "0" : lottoEODInventory.Closing5_.ToString();
                    LottoActivatedClosing7Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing7_.ToString()) ? "0" : lottoEODInventory.Closing7_.ToString();
                    LottoActivatedClosing10Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing10_.ToString()) ? "0" : lottoEODInventory.Closing10_.ToString();
                    LottoActivatedClosing20Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing20_.ToString()) ? "0" : lottoEODInventory.Closing20_.ToString();
                    LottoActivatedClosing3050Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing30__50.ToString()) ? "0" : lottoEODInventory.Closing30__50.ToString();
                    LottoActivatedClosing30100Txt.Text = string.IsNullOrEmpty(lottoEODInventory.Closing30__100.ToString()) ? "0" : lottoEODInventory.Closing30__100.ToString();
                }
                else
                {
                    LottoActivatedOpening150Txt.Text = "0";
                    LottoActivatedOpening1100Txt.Text = "0";
                    LottoActivatedAdd150Txt.Text = "0";
                    LottoActivatedAdd1100Txt.Text = "0";
                    LottoActivatedSubstract150Txt.Text = "0";
                    LottoActivatedSubstract1100Txt.Text = "0";
                    LottoActivatedOpening2Txt.Text = "0";
                    LottoActivatedAdd2Txt.Text = "0";
                    LottoActivatedSubstract2Txt.Text = "0";
                    LottoActivatedOpening3Txt.Text = "0";
                    LottoActivatedAdd3Txt.Text = "0";
                    LottoActivatedSubstract3Txt.Text = "0";
                    LottoActivatedOpening4Txt.Text = "0";
                    LottoActivatedAdd4Txt.Text = "0";
                    LottoActivatedSubstract4Txt.Text = "0";
                    LottoActivatedOpening5Txt.Text = "0";
                    LottoActivatedAdd5Txt.Text = "0";
                    LottoActivatedSubstract5Txt.Text = "0";
                    LottoActivatedOpening7Txt.Text = "0";
                    LottoActivatedAdd7Txt.Text = "0";
                    LottoActivatedSubstract7Txt.Text = "0";
                    LottoActivatedOpening10Txt.Text = "0";
                    LottoActivatedAdd10Txt.Text = "0";
                    LottoActivatedSubstract10Txt.Text = "0";
                    LottoActivatedOpening20Txt.Text = "0";
                    LottoActivatedAdd20Txt.Text = "0";
                    LottoActivatedSubstract20Txt.Text = "0";
                    LottoActivatedOpening30Txt.Text = "0";
                    LottoActivatedAdd30Txt.Text = "0";
                    LottoActivatedSubstract30Txt.Text = "0";
                    LottoActivatedOpening30100Txt.Text = "0";
                    LottoActivatedAdd30100Txt.Text = "0";
                    LottoActivatedSubstract30100Txt.Text = "0";
                    LottoActivatedTotalTxt.Text = "0";

                    LottoActivatedReturn150Txt.Text = "0";
                    LottoActivatedReturn1100Txt.Text = "0";
                    LottoActivatedReturn2Txt.Text = "0";
                    LottoActivatedReturn3Txt.Text = "0";
                    LottoActivatedReturn4Txt.Text = "0";
                    LottoActivatedReturn5Txt.Text = "0";
                    LottoActivatedReturn7Txt.Text = "0";
                    LottoActivatedReturn10Txt.Text = "0";
                    LottoActivatedReturn20Txt.Text = "0";
                    LottoActivatedReturn350Txt.Text = "0";
                    LottoActivatedReturn30100Txt.Text = "0";

                    LottoActivatedClosing150Txt.Text = "0";
                    LottoActivatedClosing1100Txt.Text = "0";
                    LottoActivatedClosing2Txt.Text = "0";
                    LottoActivatedClosing3Txt.Text = "0";
                    LottoActivatedClosing4Txt.Text = "0";
                    LottoActivatedClosing5Txt.Text = "0";
                    LottoActivatedClosing7Txt.Text = "0";
                    LottoActivatedClosing10Txt.Text = "0";
                    LottoActivatedClosing20Txt.Text = "0";
                    LottoActivatedClosing3050Txt.Text = "0";
                    LottoActivatedClosing30100Txt.Text = "0";
                }
                BindLottoActivatedDocuments();
            }
        }
        private void ActivatedLottoFormula()
        {
            LottoActivatedPreviousDayOpening();
            int opening1 = 0, add1 = 0, substract1 = 0, prevOpening1 = 0, opening2 = 0, add2 = 0, substract2 = 0, opening3 = 0, add3 = 0, substract3 = 0
                    , opening4 = 0, add4 = 0, substract4 = 0, opening5 = 0, add5 = 0, substract5 = 0,
                    opening7 = 0, add7 = 0, substract7 = 0,
                    opening10 = 0, add10 = 0, substract10 = 0,
                    opening20 = 0, add20 = 0, substract20 = 0,
                    opening30 = 0, add30 = 0, substract30 = 0, total = 0,
                    prevOpening2 = 0, prevOpening3 = 0, prevOpening4 = 0, prevOpening5 = 0,
                    prevOpening7 = 0, prevOpening10 = 0, prevOpening20 = 0,
                    prevOpening30 = 0,
                     opening1_100 = 0, add1_100 = 0, substract1_100 = 0,
                     opening30_100 = 0, add30_100 = 0, substract30_100 = 0,
                     prevOpening1_100 = 0, prevOpening30_100 = 0,
                      return1_50 = 0, return1_100 = 0, return2 = 0, return3 = 0, return4 = 0, return5 = 0, return7 = 0, return10 = 0, return20 = 0,
                     return30_50 = 0, return30_100 = 0; ;
            prevOpening1 = Convert.ToInt32(LottoActivatedOpening150Txt.Text);
            add1 = Convert.ToInt32(LottoActivatedAdd150Txt.Text);
            substract1 = Convert.ToInt32(LottoActivatedSubstract150Txt.Text);
            return1_50 = Convert.ToInt32(LottoActivatedReturn150Txt.Text);
            opening1 = (prevOpening1 + add1) - (substract1 + return1_50);
            prevOpening1_100 = Convert.ToInt32(LottoActivatedOpening1100Txt.Text);
            add1_100 = Convert.ToInt32(LottoActivatedAdd1100Txt.Text);
            substract1_100 = Convert.ToInt32(LottoActivatedSubstract1100Txt.Text);
            return1_100 = Convert.ToInt32(LottoActivatedReturn1100Txt.Text);
            opening1_100 = (prevOpening1_100 + add1_100) - (substract1_100 + return1_100);

            prevOpening2 = Convert.ToInt32(LottoActivatedOpening2Txt.Text);
            add2 = Convert.ToInt32(LottoActivatedAdd2Txt.Text);
            substract2 = Convert.ToInt32(LottoActivatedSubstract2Txt.Text);
            return2 = Convert.ToInt32(LottoActivatedReturn2Txt.Text);
            opening2 = (prevOpening2 + add2) - (substract2 + return2);
            prevOpening3 = Convert.ToInt32(LottoActivatedOpening3Txt.Text);
            add3 = Convert.ToInt32(LottoActivatedAdd3Txt.Text);
            substract3 = Convert.ToInt32(LottoActivatedSubstract3Txt.Text);
            return3 = Convert.ToInt32(LottoActivatedReturn3Txt.Text);
            opening3 = (prevOpening3 + add3) - (substract3 + return3);
            prevOpening4 = Convert.ToInt32(LottoActivatedOpening4Txt.Text);
            add4 = Convert.ToInt32(LottoActivatedAdd4Txt.Text);
            substract4 = Convert.ToInt32(LottoActivatedSubstract4Txt.Text);
            return4 = Convert.ToInt32(LottoActivatedReturn4Txt.Text);
            opening4 = (prevOpening4 + add4) - (substract4 + return4);
            prevOpening5 = Convert.ToInt32(LottoActivatedOpening5Txt.Text);
            add5 = Convert.ToInt32(LottoActivatedAdd5Txt.Text);
            substract5 = Convert.ToInt32(LottoActivatedSubstract5Txt.Text);
            return5 = Convert.ToInt32(LottoActivatedReturn5Txt.Text);
            opening5 = (prevOpening5 + add5) - (substract5 + return5);
            prevOpening7 = Convert.ToInt32(LottoActivatedOpening7Txt.Text);
            add7 = Convert.ToInt32(LottoActivatedAdd7Txt.Text);
            substract7 = Convert.ToInt32(LottoActivatedSubstract7Txt.Text);
            return7 = Convert.ToInt32(LottoActivatedReturn7Txt.Text);
            opening7 = (prevOpening7 + add7) - (substract7 + return7);
            prevOpening10 = Convert.ToInt32(LottoActivatedOpening10Txt.Text);
            add10 = Convert.ToInt32(LottoActivatedAdd10Txt.Text);
            substract10 = Convert.ToInt32(LottoActivatedSubstract10Txt.Text);
            return10 = Convert.ToInt32(LottoActivatedReturn10Txt.Text);
            opening10 = (prevOpening10 + add10) - (substract10 + return10);
            prevOpening20 = Convert.ToInt32(LottoActivatedOpening20Txt.Text);
            add20 = Convert.ToInt32(LottoActivatedAdd20Txt.Text);
            substract20 = Convert.ToInt32(LottoActivatedSubstract20Txt.Text);
            return20 = Convert.ToInt32(LottoActivatedReturn20Txt.Text);
            opening20 = (prevOpening20 + add20) - (substract20 + return20);
            prevOpening30 = Convert.ToInt32(LottoActivatedOpening30Txt.Text);
            add30 = Convert.ToInt32(LottoActivatedAdd30Txt.Text);
            substract30 = Convert.ToInt32(LottoActivatedSubstract30Txt.Text);
            return30_50 = Convert.ToInt32(LottoUnActivatedReturn3050Txt.Text);
            opening30 = (prevOpening30 + add30) - (substract30 + return30_50);
            prevOpening30_100 = Convert.ToInt32(LottoActivatedOpening30100Txt.Text);
            add30_100 = Convert.ToInt32(LottoActivatedAdd30100Txt.Text);
            substract30_100 = Convert.ToInt32(LottoActivatedSubstract30100Txt.Text);
            return30_100 = Convert.ToInt32(LottoUnActivatedReturn30100Txt.Text);
            opening30_100 = (prevOpening30_100 + add30_100) - (substract30_100 + return30_100);
            total = (opening1 + opening2 + opening3 + opening4 + opening5 + opening7 + opening10 + opening20 + opening30 + opening1_100 + opening30_100);
            LottoActivatedTotalTxt.Text = total.ToString();

            //Opening still zero

            LottoActivatedClosing150Txt.Text = opening1.ToString();
            LottoActivatedClosing1100Txt.Text = opening1_100.ToString();
            LottoActivatedClosing2Txt.Text = opening2.ToString();
            LottoActivatedClosing3Txt.Text = opening3.ToString();
            LottoActivatedClosing4Txt.Text = opening4.ToString();
            LottoActivatedClosing5Txt.Text = opening5.ToString();
            LottoActivatedClosing7Txt.Text = opening7.ToString();
            LottoActivatedClosing10Txt.Text = opening10.ToString();
            LottoActivatedClosing20Txt.Text = opening20.ToString();
            LottoActivatedClosing3050Txt.Text = opening30.ToString();
            LottoActivatedClosing30100Txt.Text = opening30_100.ToString();


        }
        private void AdditionToScratchAndWinAddQty()
        {
            int substract1_50 = 0, substract1_100 = 0, substract2 = 0,
               substract3 = 0, substract4 = 0, substract5 = 0
               , substract7 = 0, substract10 = 0,
               substract20 = 0, substract30_50 = 0, substract30_100 = 0;
            substract1_50 = Convert.ToInt32(LottoActivatedSubstract150Txt.Text);
            substract1_100 = Convert.ToInt32(LottoActivatedSubstract1100Txt.Text);
            substract2 = Convert.ToInt32(LottoActivatedSubstract2Txt.Text);
            substract3 = Convert.ToInt32(LottoActivatedSubstract3Txt.Text);
            substract4 = Convert.ToInt32(LottoActivatedSubstract4Txt.Text);
            substract5 = Convert.ToInt32(LottoActivatedSubstract5Txt.Text);
            substract7 = Convert.ToInt32(LottoActivatedSubstract7Txt.Text);
            substract10 = Convert.ToInt32(LottoActivatedSubstract10Txt.Text);
            substract20 = Convert.ToInt32(LottoActivatedSubstract20Txt.Text);
            substract30_50 = Convert.ToInt32(LottoActivatedSubstract30Txt.Text);
            substract30_100 = Convert.ToInt32(LottoUnActivatedSubtract30100Txt.Text);
            LottoScratchAddQty1Txt.Text = (substract1_50 + substract1_100).ToString();
            LottoScratchAddQty2Txt.Text = (substract2).ToString();
            LottoScratchAddQty3Txt.Text = (substract3).ToString();
            LottoScratchAddQty4Txt.Text = (substract4).ToString();
            LottoScratchAddQty5Txt.Text = (substract5).ToString();
            LottoScratchAddQty7Txt.Text = (substract7).ToString();
            LottoScratchAddQty10Txt.Text = (substract10).ToString();
            LottoScratchAddQty20Txt.Text = (substract20).ToString();
            LottoScratchAddQty30Txt.Text = (substract30_50 + substract30_100).ToString();
        }
        #endregion
        #region Scratch and Win

        #region Previous and Current Day Opening
        private bool isPreviosDayLottoScratchOpeningAvailable = false;
        private void Scratch1PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "1").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void Scratch2PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "2").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void Scratch3PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "3").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }

        private void Scratch4PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "4").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }

        private void Scratch5PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "5").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }

        private void Scratch7PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "7").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }

        private void Scratch10PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "10").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }

        private void Scratch20PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "20").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }

        private void Scratch30PreviosDayActualClose()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    date = date.AddDays(-1);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "30").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        isPreviosDayLottoScratchOpeningAvailable = true;
                        LottoScratchOpen1Txt.Text = string.IsNullOrEmpty(lottoScratchWin.ActualClose.ToString()) ? "0" : lottoScratchWin.ActualClose.ToString();

                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;
                        LottoScratchOpen1Txt.Text = "0";
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }

        #endregion
        #region Auto Calculation
        private void ScratchWin1AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen1Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen1txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff1Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty1Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal1Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose1Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold1Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold1Txt.Text);
            reportSold_1 = reportSold * 1;
            LottoReportSold1Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch1DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void ScratchWin2AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen2Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen2txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff2Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty2Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal2Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose2Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold2Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold2Txt.Text);
            reportSold_1 = reportSold * 2;
            LottoReportSold2Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch2DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void ScratchWin3AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen3Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen3txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff3Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty3Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal3Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose3Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold3Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold3Txt.Text);
            reportSold_1 = reportSold * 3;
            LottoReportSold3Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch3DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void ScratchWin4AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen4Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen4txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff4Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty4Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal4Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose4Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold4Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold4Txt.Text);
            reportSold_1 = reportSold * 4;
            LottoReportSold4Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch4DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void ScratchWin5AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen5Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen5txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff5Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty5Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal5Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose5Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold5Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold5Txt.Text);
            reportSold_1 = reportSold * 5;
            LottoReportSold5Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch5DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void ScratchWin7AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen7Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen7txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff7Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty7Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal7Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose7Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold7Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold7Txt.Text);
            reportSold_1 = reportSold * 7;
            LottoReportSold7Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch7DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void ScratchWin10AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen10Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen10txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff10Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty10Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal10Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose10Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold10Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold10Txt.Text);
            reportSold_1 = reportSold * 10;
            LottoReportSold10Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch10DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void ScratchWin20AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen20Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen20txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff20Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty20Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal20Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose20Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold20Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold20Txt.Text);
            reportSold_1 = reportSold * 20;
            LottoReportSold20Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch20DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void ScratchWin30AutoCalculation()
        {
            // Opening Diff
            double openC = 0.00, actualOpenC = 0.00, openingDiffC = 0.00;
            openC = Convert.ToDouble(LottoScratchOpen30Txt.Text);
            actualOpenC = Convert.ToDouble(LottoScratchActualOpen30txt.Text);
            openingDiffC = actualOpenC - openC;
            LottoScratchOpeningDiff30Txt.Text = string.IsNullOrEmpty(openingDiffC.ToString()) ? "0" : openingDiffC.ToString();

            //Subtotal
            double addQtyC = 0.00, subTotal = 0.00;
            addQtyC = Convert.ToDouble(LottoScratchAddQty30Txt.Text);
            subTotal = actualOpenC + addQtyC;
            LottoScratchSubtotal30Txt.Text = string.IsNullOrEmpty(subTotal.ToString()) ? "0" : subTotal.ToString();

            // Sold
            double actualCloseC = 0.00, soldC = 0.00;
            actualCloseC = Convert.ToDouble(LottoScratchActualClose30Txt.Text);
            soldC = subTotal - actualCloseC;
            LottoScratchSold30Txt.Text = string.IsNullOrEmpty(soldC.ToString()) ? "0" : soldC.ToString();
            // Report Sold$
            double reportSold = 0.00, diff = 0.00, reportSold_1 = 0.00;
            reportSold = Convert.ToDouble(LottoScratchReportSold30Txt.Text);
            reportSold_1 = reportSold * 30;
            LottoReportSold30Txt.Text = string.IsNullOrEmpty(reportSold_1.ToString()) ? "0" : reportSold_1.ToString();

            // Diff
            diff = reportSold - soldC;
            LottoScratch30DiffTxt.Text = string.IsNullOrEmpty(diff.ToString()) ? "0" : diff.ToString();

        }
        private void TotalSumCalculation()
        {

            #region Total Open

            // Total Open
            double open1 = 0.00, open2 = 0.00, open3 = 0.00, open4 = 0.00, open5 = 0.00, open7 = 0.00, open10 = 0.00, open20 = 0.00, open30 = 0.00;
            open1 = Convert.ToDouble(LottoScratchOpen1Txt.Text);
            open2 = Convert.ToDouble(LottoScratchOpen2Txt.Text);
            open3 = Convert.ToDouble(LottoScratchOpen3Txt.Text);
            open4 = Convert.ToDouble(LottoScratchOpen4Txt.Text);
            open5 = Convert.ToDouble(LottoScratchOpen5Txt.Text);
            open7 = Convert.ToDouble(LottoScratchOpen7Txt.Text);
            open10 = Convert.ToDouble(LottoScratchOpen10Txt.Text);
            open20 = Convert.ToDouble(LottoScratchOpen20Txt.Text);
            open30 = Convert.ToDouble(LottoScratchOpen30Txt.Text);
            LottoScratchWinTotalOpenTxt.Text = (open1 + open2 + open3 + open4 + open5 + open7 + open10 + open20 + open30).ToString();

            #endregion

            #region Actual Open
            // Actual Open

            double actualOpen1 = 0.00, actualOpen2 = 0.00, actualOpen3 = 0.00, actualOpen4 = 0.00, actualOpen5 = 0.00, actualOpen7 = 0.00, actualOpen10 = 0.00, actualOpen20 = 0.00, actualOpen30 = 0.00;
            actualOpen1 = Convert.ToDouble(LottoScratchActualOpen1txt.Text);
            actualOpen2 = Convert.ToDouble(LottoScratchActualOpen2txt.Text);
            actualOpen3 = Convert.ToDouble(LottoScratchActualOpen3txt.Text);
            actualOpen4 = Convert.ToDouble(LottoScratchActualOpen4txt.Text);
            actualOpen5 = Convert.ToDouble(LottoScratchActualOpen5txt.Text);
            actualOpen7 = Convert.ToDouble(LottoScratchActualOpen7txt.Text);
            actualOpen10 = Convert.ToDouble(LottoScratchActualOpen10txt.Text);
            actualOpen20 = Convert.ToDouble(LottoScratchActualOpen20txt.Text);
            actualOpen30 = Convert.ToDouble(LottoScratchActualOpen30txt.Text);
            LottoScratchWinTotalActualOpenTxt.Text = (actualOpen1 + actualOpen2 + actualOpen3 + actualOpen4 + actualOpen5 + actualOpen7 + actualOpen10 + actualOpen20 + actualOpen30).ToString();

            #endregion

            #region Opening Diff
            // Opening Diff
            double openingDiff1 = 0.00, openingDiff2 = 0.00, openingDiff3 = 0.00, openingDiff4 = 0.00, openingDiff5 = 0.00, openingDiff7 = 0.00, openingDiff10 = 0.00, openingDiff20 = 0.00, openingDiff30 = 0.00;
            openingDiff1 = Convert.ToDouble(LottoScratchOpeningDiff1Txt.Text);
            openingDiff2 = Convert.ToDouble(LottoScratchOpeningDiff2Txt.Text);
            openingDiff3 = Convert.ToDouble(LottoScratchOpeningDiff3Txt.Text);
            openingDiff4 = Convert.ToDouble(LottoScratchOpeningDiff4Txt.Text);
            openingDiff5 = Convert.ToDouble(LottoScratchOpeningDiff5Txt.Text);
            openingDiff7 = Convert.ToDouble(LottoScratchOpeningDiff7Txt.Text);
            openingDiff10 = Convert.ToDouble(LottoScratchOpeningDiff10Txt.Text);
            openingDiff20 = Convert.ToDouble(LottoScratchOpeningDiff20Txt.Text);
            openingDiff30 = Convert.ToDouble(LottoScratchOpeningDiff30Txt.Text);
            LottoScratchWinTotalOpeningDiffTxt.Text = (openingDiff1 + openingDiff2 + openingDiff3 + openingDiff4 + openingDiff5 + openingDiff7 + openingDiff10 + openingDiff20 + openingDiff30).ToString();

            #endregion

            #region Add Qty

            // Additional Qty
            double addQty1 = 0.00, addQty2 = 0.00, addQty3 = 0.00, addQty4 = 0.00, addQty5 = 0.00, addQty7 = 0.00, addQty10 = 0.00, addQty20 = 0.00, addQty30 = 0.00;
            addQty1 = Convert.ToDouble(LottoScratchAddQty1Txt.Text);
            addQty2 = Convert.ToDouble(LottoScratchAddQty2Txt.Text);
            addQty3 = Convert.ToDouble(LottoScratchAddQty3Txt.Text);
            addQty4 = Convert.ToDouble(LottoScratchAddQty4Txt.Text);
            addQty5 = Convert.ToDouble(LottoScratchAddQty5Txt.Text);
            addQty7 = Convert.ToDouble(LottoScratchAddQty7Txt.Text);
            addQty10 = Convert.ToDouble(LottoScratchAddQty10Txt.Text);
            addQty20 = Convert.ToDouble(LottoScratchAddQty20Txt.Text);
            addQty30 = Convert.ToDouble(LottoScratchAddQty30Txt.Text);
            LottoScratchWinTotalAdditionalQtyTxt.Text = (addQty1 + addQty2 + addQty3 + addQty4 + addQty5 + addQty7 + addQty10 + addQty20 + addQty30).ToString();

            #endregion

            #region SubTotal

            //SubTotal
            double subTotal1 = 0.00, subTotal2 = 0.00, subTotal3 = 0.00, subTotal4 = 0.00, subTotal5 = 0.00, subTotal7 = 0.00, subTotal10 = 0.00, subTotal20 = 0.00, subTotal30 = 0.00;
            subTotal1 = Convert.ToDouble(LottoScratchSubtotal1Txt.Text);
            subTotal2 = Convert.ToDouble(LottoScratchSubtotal2Txt.Text);
            subTotal3 = Convert.ToDouble(LottoScratchSubtotal3Txt.Text);
            subTotal4 = Convert.ToDouble(LottoScratchSubtotal4Txt.Text);
            subTotal5 = Convert.ToDouble(LottoScratchSubtotal5Txt.Text);
            subTotal7 = Convert.ToDouble(LottoScratchSubtotal7Txt.Text);
            subTotal10 = Convert.ToDouble(LottoScratchSubtotal10Txt.Text);
            subTotal20 = Convert.ToDouble(LottoScratchSubtotal20Txt.Text);
            subTotal30 = Convert.ToDouble(LottoScratchSubtotal30Txt.Text);
            LottoScrachWinTotalSubtotalTxt.Text = (subTotal1 + subTotal2 + subTotal3 + subTotal4 + subTotal5 + subTotal7 + subTotal10 + subTotal20 + subTotal30).ToString();

            #endregion

            #region Actual Close


            // Actual Close
            double actualClose1 = 0.00, actualClose2 = 0.00, actualClose3 = 0.00, actualClose4 = 0.00, actualClose5 = 0.00, actualClose7 = 0.00, actualClose10 = 0.00, actualClose20 = 0.00, actualClose30 = 0.00;
            actualClose1 = Convert.ToDouble(LottoScratchSubtotal1Txt.Text);
            actualClose2 = Convert.ToDouble(LottoScratchSubtotal2Txt.Text);
            actualClose3 = Convert.ToDouble(LottoScratchSubtotal3Txt.Text);
            actualClose4 = Convert.ToDouble(LottoScratchSubtotal4Txt.Text);
            actualClose5 = Convert.ToDouble(LottoScratchSubtotal5Txt.Text);
            actualClose7 = Convert.ToDouble(LottoScratchSubtotal7Txt.Text);
            actualClose10 = Convert.ToDouble(LottoScratchSubtotal10Txt.Text);
            actualClose20 = Convert.ToDouble(LottoScratchSubtotal20Txt.Text);
            actualClose30 = Convert.ToDouble(LottoScratchSubtotal30Txt.Text);
            LottoScratchWinTotalActualCLoseTxt.Text = (actualClose1 + actualClose2 + actualClose3 + actualClose4 + actualClose5 + actualClose7 + actualClose10 + actualClose20 + actualClose30).ToString();

            #endregion

            #region Sold
            // Sold
            double sold1 = 0.00, sold2 = 0.00, sold3 = 0.00, sold4 = 0.00, sold5 = 0.00, sold7 = 0.00, sold10 = 0.00, sold20 = 0.00, sold30 = 0.00;
            sold1 = Convert.ToDouble(LottoScratchSold1Txt.Text);
            sold2 = Convert.ToDouble(LottoScratchSold2Txt.Text);
            sold3 = Convert.ToDouble(LottoScratchSold3Txt.Text);
            sold4 = Convert.ToDouble(LottoScratchSold4Txt.Text);
            sold5 = Convert.ToDouble(LottoScratchSold5Txt.Text);
            sold7 = Convert.ToDouble(LottoScratchSold7Txt.Text);
            sold10 = Convert.ToDouble(LottoScratchSold10Txt.Text);
            sold20 = Convert.ToDouble(LottoScratchSold20Txt.Text);
            sold30 = Convert.ToDouble(LottoScratchSold30Txt.Text);
            LottoScratchWinTotalSoldTxt.Text = (sold1 + sold2 + sold3 + sold4 + sold5 + sold7 + sold10 + sold20 + sold30).ToString();

            #endregion

            #region EOD Sold
            // EOD Sold

            double Eodsold1 = 0.00, Eodsold2 = 0.00, Eodsold3 = 0.00, Eodsold4 = 0.00, Eodsold5 = 0.00, Eodsold7 = 0.00, Eodsold10 = 0.00, Eodsold20 = 0.00, Eodsold30 = 0.00;
            Eodsold1 = Convert.ToDouble(LottoScratchReportSold1Txt.Text);
            Eodsold2 = Convert.ToDouble(LottoScratchReportSold2Txt.Text);
            Eodsold3 = Convert.ToDouble(LottoScratchReportSold3Txt.Text);
            Eodsold4 = Convert.ToDouble(LottoScratchReportSold4Txt.Text);
            Eodsold5 = Convert.ToDouble(LottoScratchReportSold5Txt.Text);
            Eodsold7 = Convert.ToDouble(LottoScratchReportSold7Txt.Text);
            Eodsold10 = Convert.ToDouble(LottoScratchReportSold10Txt.Text);
            Eodsold20 = Convert.ToDouble(LottoScratchReportSold20Txt.Text);
            Eodsold30 = Convert.ToDouble(LottoScratchReportSold30Txt.Text);
            LottoScratchWinTotalEODSoldTxt.Text = (Eodsold1 + Eodsold2 + Eodsold3 + Eodsold4 + Eodsold5 + Eodsold7 + Eodsold10 + Eodsold20 + Eodsold30).ToString();

            #endregion

            #region EOD Sold Value

            // EOD Sold Value

            double EodsoldValue1 = 0.00, EodsoldValue2 = 0.00, EodsoldValue3 = 0.00, EodsoldValue4 = 0.00, EodsoldValue5 = 0.00, EodsoldValue7 = 0.00, EodsoldValue10 = 0.00, EodsoldValue20 = 0.00, EodsoldValue30 = 0.00;
            EodsoldValue1 = Convert.ToDouble(LottoReportSold1Txt.Text);
            EodsoldValue2 = Convert.ToDouble(LottoReportSold2Txt.Text);
            EodsoldValue3 = Convert.ToDouble(LottoReportSold3Txt.Text);
            EodsoldValue4 = Convert.ToDouble(LottoReportSold4Txt.Text);
            EodsoldValue5 = Convert.ToDouble(LottoReportSold5Txt.Text);
            EodsoldValue7 = Convert.ToDouble(LottoReportSold7Txt.Text);
            EodsoldValue10 = Convert.ToDouble(LottoReportSold10Txt.Text);
            EodsoldValue20 = Convert.ToDouble(LottoReportSold20Txt.Text);
            EodsoldValue30 = Convert.ToDouble(LottoReportSold30Txt.Text);
            LottoScratchWinTotalEODSoldDTxt.Text = (EodsoldValue1 + EodsoldValue2 + EodsoldValue3 + EodsoldValue4 + EodsoldValue5 + EodsoldValue7 + EodsoldValue10 + EodsoldValue20 + EodsoldValue30).ToString();

            #endregion



            // Diff

            double diffQty1 = 0.00, diffQty2 = 0.00, diffQty3 = 0.00, diffQty4 = 0.00, diffQty5 = 0.00, diffQty7 = 0.00, diffQty10 = 0.00, diffQty20 = 0.00, diffQty30 = 0.00;
            diffQty1 = Convert.ToDouble(LottoScratch1DiffTxt.Text);
            diffQty2 = Convert.ToDouble(LottoScratch2DiffTxt.Text);
            diffQty3 = Convert.ToDouble(LottoScratch3DiffTxt.Text);
            diffQty4 = Convert.ToDouble(LottoScratch4DiffTxt.Text);
            diffQty5 = Convert.ToDouble(LottoScratch5DiffTxt.Text);
            diffQty7 = Convert.ToDouble(LottoScratch7DiffTxt.Text);
            diffQty10 = Convert.ToDouble(LottoScratch10DiffTxt.Text);
            diffQty20 = Convert.ToDouble(LottoScratch20DiffTxt.Text);
            diffQty30 = Convert.ToDouble(LottoScratch30DiffTxt.Text);
            LottoScratchWinTotalDiffTxt.Text = (diffQty1 + diffQty2 + diffQty3 + diffQty4 + diffQty5 + diffQty7 + diffQty10 + diffQty20 + diffQty30).ToString();

        }

        #endregion

        #region Load Data
        private void LoadScratchWin1Data()
        {
            using (ShellEntities context = new ShellEntities())
            {
                Scratch1PreviosDayActualClose();
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var lottoScratchWin = context.Tbl_LottoScratchWin.FirstOrDefault(s => s.TransactionDate == date && s.LottoScratchType == "1");
                if (lottoScratchWin != null)
                {

                    LottoScratchActualOpen1txt.Text = lottoScratchWin.ActualOpen.ToString();
                    LottoScratchOpeningDiff1Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                    LottoScratchAddQty1Txt.Text = lottoScratchWin.AddQty.ToString();
                    LottoScratchSubtotal1Txt.Text = lottoScratchWin.SubTotal.ToString();
                    LottoScratchActualClose1Txt.Text = lottoScratchWin.ActualClose.ToString();
                    LottoScratchSold1Txt.Text = lottoScratchWin.Sold.ToString();
                    LottoScratchReportSold1Txt.Text = lottoScratchWin.ReportSold.ToString();
                    LottoReportSold1Txt.Text = lottoScratchWin.ReportSold_.ToString();
                    LottoScratch1DiffTxt.Text = lottoScratchWin.Difference.ToString();
                    LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;

                }
                LoadSavedDocuments(DocumentTypeConstants.LottoScratchAndWinDoc, ScratchWinNormalDgv);
            }
        }
        private void LoadScratchWin2Data()
        {
            try
            {
                Scratch2PreviosDayActualClose();
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "2").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {

                        LottoScratchActualOpen2txt.Text = lottoScratchWin.ActualOpen.ToString();
                        LottoScratchOpeningDiff2Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                        LottoScratchAddQty2Txt.Text = lottoScratchWin.AddQty.ToString();
                        LottoScratchSubtotal2Txt.Text = lottoScratchWin.SubTotal.ToString();
                        LottoScratchActualClose2Txt.Text = lottoScratchWin.ActualClose.ToString();
                        LottoScratchSold2Txt.Text = lottoScratchWin.Sold.ToString();
                        LottoScratchReportSold2Txt.Text = lottoScratchWin.ReportSold.ToString();
                        LottoReportSold2Txt.Text = lottoScratchWin.ReportSold_.ToString();
                        LottoScratch2DiffTxt.Text = lottoScratchWin.Difference.ToString();
                        LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void LoadScratchWin3Data()
        {
            try
            {
                Scratch3PreviosDayActualClose();
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "3").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {

                        LottoScratchActualOpen3txt.Text = lottoScratchWin.ActualOpen.ToString();
                        LottoScratchOpeningDiff3Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                        LottoScratchAddQty3Txt.Text = lottoScratchWin.AddQty.ToString();
                        LottoScratchSubtotal3Txt.Text = lottoScratchWin.SubTotal.ToString();
                        LottoScratchActualClose3Txt.Text = lottoScratchWin.ActualClose.ToString();
                        LottoScratchSold3Txt.Text = lottoScratchWin.Sold.ToString();
                        LottoScratchReportSold3Txt.Text = lottoScratchWin.ReportSold.ToString();
                        LottoReportSold3Txt.Text = lottoScratchWin.ReportSold_.ToString();
                        LottoScratch3DiffTxt.Text = lottoScratchWin.Difference.ToString();
                        LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;
                    }
                    else
                    {
                        isPreviosDayLottoScratchOpeningAvailable = false;

                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void LoadScratchWin4Data()
        {
            try
            {
                Scratch4PreviosDayActualClose();
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "4").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {

                        LottoScratchActualOpen4txt.Text = lottoScratchWin.ActualOpen.ToString();
                        LottoScratchOpeningDiff4Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                        LottoScratchAddQty4Txt.Text = lottoScratchWin.AddQty.ToString();
                        LottoScratchSubtotal4Txt.Text = lottoScratchWin.SubTotal.ToString();
                        LottoScratchActualClose4Txt.Text = lottoScratchWin.ActualClose.ToString();
                        LottoScratchSold4Txt.Text = lottoScratchWin.Sold.ToString();
                        LottoScratchReportSold4Txt.Text = lottoScratchWin.ReportSold.ToString();
                        LottoReportSold4Txt.Text = lottoScratchWin.ReportSold_.ToString();
                        LottoScratch4DiffTxt.Text = lottoScratchWin.Difference.ToString();
                        LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void LoadScratchWin5Data()
        {
            try
            {
                Scratch5PreviosDayActualClose();
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "5").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        LottoScratchActualOpen5txt.Text = lottoScratchWin.ActualOpen.ToString();
                        LottoScratchOpeningDiff5Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                        LottoScratchAddQty5Txt.Text = lottoScratchWin.AddQty.ToString();
                        LottoScratchSubtotal5Txt.Text = lottoScratchWin.SubTotal.ToString();
                        LottoScratchActualClose5Txt.Text = lottoScratchWin.ActualClose.ToString();
                        LottoScratchSold5Txt.Text = lottoScratchWin.Sold.ToString();
                        LottoScratchReportSold5Txt.Text = lottoScratchWin.ReportSold.ToString();
                        LottoReportSold5Txt.Text = lottoScratchWin.ReportSold_.ToString();
                        LottoScratch5DiffTxt.Text = lottoScratchWin.Difference.ToString();
                        LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;

                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void LoadScratchWin7Data()
        {
            try
            {
                Scratch7PreviosDayActualClose();
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "7").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {

                        LottoScratchActualOpen7txt.Text = lottoScratchWin.ActualOpen.ToString();
                        LottoScratchOpeningDiff7Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                        LottoScratchAddQty7Txt.Text = lottoScratchWin.AddQty.ToString();
                        LottoScratchSubtotal7Txt.Text = lottoScratchWin.SubTotal.ToString();
                        LottoScratchActualClose7Txt.Text = lottoScratchWin.ActualClose.ToString();
                        LottoScratchSold7Txt.Text = lottoScratchWin.Sold.ToString();
                        LottoScratchReportSold7Txt.Text = lottoScratchWin.ReportSold.ToString();
                        LottoReportSold7Txt.Text = lottoScratchWin.ReportSold_.ToString();
                        LottoScratch7DiffTxt.Text = lottoScratchWin.Difference.ToString();
                        LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void LoadScratchWin10Data()
        {
            try
            {
                Scratch10PreviosDayActualClose();
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "10").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {

                        LottoScratchActualOpen10txt.Text = lottoScratchWin.ActualOpen.ToString();
                        LottoScratchOpeningDiff10Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                        LottoScratchAddQty10Txt.Text = lottoScratchWin.AddQty.ToString();
                        LottoScratchSubtotal10Txt.Text = lottoScratchWin.SubTotal.ToString();
                        LottoScratchActualClose10Txt.Text = lottoScratchWin.ActualClose.ToString();
                        LottoScratchSold10Txt.Text = lottoScratchWin.Sold.ToString();
                        LottoScratchReportSold10Txt.Text = lottoScratchWin.ReportSold.ToString();
                        LottoReportSold10Txt.Text = lottoScratchWin.ReportSold_.ToString();
                        LottoScratch10DiffTxt.Text = lottoScratchWin.Difference.ToString();
                        LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void LoadScratchWin20Data()
        {
            try
            {
                Scratch20PreviosDayActualClose();
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "20").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {
                        LottoScratchActualOpen20txt.Text = lottoScratchWin.ActualOpen.ToString();
                        LottoScratchOpeningDiff20Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                        LottoScratchAddQty20Txt.Text = lottoScratchWin.AddQty.ToString();
                        LottoScratchSubtotal20Txt.Text = lottoScratchWin.SubTotal.ToString();
                        LottoScratchActualClose20Txt.Text = lottoScratchWin.ActualClose.ToString();
                        LottoScratchSold20Txt.Text = lottoScratchWin.Sold.ToString();
                        LottoScratchReportSold20Txt.Text = lottoScratchWin.ReportSold.ToString();
                        LottoReportSold20Txt.Text = lottoScratchWin.ReportSold_.ToString();
                        LottoScratch20DiffTxt.Text = lottoScratchWin.Difference.ToString();
                        LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;

                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void LoadScratchWin30Data()
        {
            try
            {
                Scratch30PreviosDayActualClose();
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "30").FirstOrDefault();
                    if (lottoScratchWin != null)
                    {

                        LottoScratchActualOpen30txt.Text = lottoScratchWin.ActualOpen.ToString();
                        LottoScratchOpeningDiff30Txt.Text = lottoScratchWin.OpeningDifference.ToString();
                        LottoScratchAddQty30Txt.Text = lottoScratchWin.AddQty.ToString();
                        LottoScratchSubtotal30Txt.Text = lottoScratchWin.SubTotal.ToString();
                        LottoScratchActualClose30Txt.Text = lottoScratchWin.ActualClose.ToString();
                        LottoScratchSold30Txt.Text = lottoScratchWin.Sold.ToString();
                        LottoScratchReportSold30Txt.Text = lottoScratchWin.ReportSold.ToString();
                        LottoReportSold30Txt.Text = lottoScratchWin.ReportSold_.ToString();
                        LottoScratch30DiffTxt.Text = lottoScratchWin.Difference.ToString();
                        LottoScratchRemarksTxt.Text = lottoScratchWin.Remarks;
                    }

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
        }
        private void LoadScratchAndWinSectionData()
        {
            LoadScratchWin1Data();
            LoadScratchWin2Data();
            LoadScratchWin3Data();
            LoadScratchWin4Data();
            LoadScratchWin5Data();
            LoadScratchWin7Data();
            LoadScratchWin10Data();
            LoadScratchWin20Data();
            LoadScratchWin30Data();
        }
        #endregion

        #region Save Data
        private void SaveScratchWin1()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen1Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen1txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff1Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty1Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal1Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose1Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold1Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold1Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold1Txt.Text);
                diff = Convert.ToInt32(LottoScratch1DiffTxt.Text);
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "1").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "1";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }
        private void SaveScratchWin2()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen2Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen2txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff2Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty2Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal2Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose2Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold2Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold2Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold2Txt.Text);
                diff = Convert.ToInt32(LottoScratch2DiffTxt.Text);
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "2").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "2";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }
        private void SaveScratchWin3()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen3Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen3txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff3Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty3Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal3Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose3Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold3Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold3Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold3Txt.Text);
                diff = Convert.ToInt32(LottoScratch3DiffTxt.Text);
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "3").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "3";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }
        private void SaveScratchWin4()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen4Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen4txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff4Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty4Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal4Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose4Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold4Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold4Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold4Txt.Text);
                diff = Convert.ToInt32(LottoScratch4DiffTxt.Text);
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "4").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "4";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }
        private void SaveScratchWin5()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen5Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen5txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff5Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty5Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal5Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose5Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold5Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold5Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold5Txt.Text);
                diff = Convert.ToInt32(LottoScratch5DiffTxt.Text);
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "5").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "5";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }
        private void SaveScratchWin7()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen7Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen7txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff7Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty7Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal7Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose7Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold7Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold7Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold7Txt.Text);
                diff = Convert.ToInt32(LottoScratch7DiffTxt.Text);
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "7").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "7";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }
        private void SaveScratchWin10()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen10Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen10txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff10Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty10Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal10Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose10Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold10Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold10Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold10Txt.Text);
                diff = Convert.ToInt32(LottoScratch10DiffTxt.Text);
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "10").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "10";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }
        private void SaveScratchWin20()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen20Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen20txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff20Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty20Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal20Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose20Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold20Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold20Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold20Txt.Text);
                diff = Convert.ToInt32(LottoScratch20DiffTxt.Text);
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "20").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "20";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }
        private void SaveScratchWin30()
        {

            try
            {
                int open = 0, actualOpen = 0, OpeningDiff = 0, addQty = 0, subTotal = 0, actualClose = 0, sold = 0, reportSold = 0, reportSoldDollar = 0, diff = 0;
                open = Convert.ToInt32(LottoScratchOpen30Txt.Text);
                actualOpen = Convert.ToInt32(LottoScratchActualOpen30txt.Text);
                OpeningDiff = Convert.ToInt32(LottoScratchOpeningDiff30Txt.Text);
                addQty = Convert.ToInt32(LottoScratchAddQty30Txt.Text);
                subTotal = Convert.ToInt32(LottoScratchSubtotal30Txt.Text);
                actualClose = Convert.ToInt32(LottoScratchActualClose30Txt.Text);
                sold = Convert.ToInt32(LottoScratchSold30Txt.Text);
                reportSold = Convert.ToInt32(LottoScratchReportSold30Txt.Text);
                reportSoldDollar = Convert.ToInt32(LottoReportSold30Txt.Text);
                diff = Convert.ToInt32(LottoScratch30DiffTxt.Text);



                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchWin = context.Tbl_LottoScratchWin.Where(s => s.TransactionDate == date && s.LottoScratchType == "30").FirstOrDefault();
                    if (lottoScratchWin == null)
                    {
                        lottoScratchWin = new Tbl_LottoScratchWin();
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.TransactionDate = date.Date;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.CreatedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.LottoScratchType = "30";
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                        context.Tbl_LottoScratchWin.Add(lottoScratchWin);
                    }
                    else
                    {
                        lottoScratchWin.Open = open;
                        lottoScratchWin.ActualOpen = actualOpen;
                        lottoScratchWin.OpeningDifference = OpeningDiff;
                        lottoScratchWin.AddQty = addQty;
                        lottoScratchWin.SubTotal = subTotal;
                        lottoScratchWin.ActualClose = actualClose;
                        lottoScratchWin.Sold = sold;
                        lottoScratchWin.ReportSold = reportSold;
                        lottoScratchWin.ReportSold_ = reportSoldDollar;
                        lottoScratchWin.Difference = diff;
                        lottoScratchWin.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchWin.ModifiedDate = DateTime.Now;
                        lottoScratchWin.Remarks = LottoScratchRemarksTxt.Text;
                        lottoScratchWin.TotalOpen = totalOpen;
                        lottoScratchWin.TotalActualOpen = totalActualOpen;
                        lottoScratchWin.TotalOpeningDiff = totalOpeningDiff;
                        lottoScratchWin.TotalAdditionalQty = totalAdditionQty;
                        lottoScratchWin.TotalSubtotal = totalSubtotal;
                        lottoScratchWin.TotalActualClose = totalActualClose;
                        lottoScratchWin.TotalSold = totalSoldQty;
                        lottoScratchWin.TotalEODSoldQty = totalEodSoldQty;
                        lottoScratchWin.TotalEODSoldValue = totalSoldValue;
                    }
                    context.SaveChanges();


                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }

        }

        private void SetValues()
        {
            totalOpen = Convert.ToInt32(LottoScratchWinTotalOpenTxt.Text);
            totalActualOpen = Convert.ToInt32(LottoScratchWinTotalActualOpenTxt.Text);
            totalOpeningDiff = Convert.ToInt32(LottoScratchWinTotalOpeningDiffTxt.Text);
            totalAdditionQty = Convert.ToInt32(LottoScratchWinTotalAdditionalQtyTxt.Text);
            totalSubtotal = Convert.ToInt32(LottoScrachWinTotalSubtotalTxt.Text);
            totalActualClose = Convert.ToInt32(LottoScratchWinTotalActualCLoseTxt.Text);
            totalSoldQty = Convert.ToInt32(LottoScratchWinTotalSoldTxt.Text);
            totalEodSoldQty = Convert.ToInt32(LottoScratchWinTotalEODSoldTxt.Text);
            totalSoldValue = Convert.ToInt32(LottoScratchWinTotalEODSoldDTxt.Text);
            totalDiff = Convert.ToInt32(LottoScratchWinTotalDiffTxt.Text);
        }
        private void SaveScratchAndWinSectionData()
        {
            SaveScratchWin1();
            SaveScratchWin2();
            SaveScratchWin3();
            SaveScratchWin4();
            SaveScratchWin5();
            SaveScratchWin7();
            SaveScratchWin10();
            SaveScratchWin20();
            SaveScratchWin30();
            SaveMultipleDocs("Lotto Scratch And Win", ScratchWinNormalDgv);
        }
        #endregion

        #endregion
        #region Lotto Scratch And Win Online
        private void LoadScratchWinOnlineData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var lottoScratchWin = context.Tbl_LottoScratchWinOnline.FirstOrDefault(s => s.TransactionDate == date);
                if (lottoScratchWin != null)
                {
                    LottoOnlineReportTxt.Text = lottoScratchWin.LottoReport.ToString();
                    LottoOnlineBackOfficetxt.Text = lottoScratchWin.BackOffice.ToString();
                    LottoOnlineAdjustCanceltxt.Text = lottoScratchWin.AdjustCancel.ToString();
                    LottoOnlineEOS1Txt.Text = lottoScratchWin.EOS1.ToString();
                    LottoOnlineEOS2Txt.Text = lottoScratchWin.EOS2.ToString();
                    LottoOnlineEOS3Txt.Text = lottoScratchWin.EOS3.ToString();
                    LottoOnlineTSoldTxt.Text = lottoScratchWin.TSold.ToString();
                    LottoOnlineShiftsDiffTxt.Text = lottoScratchWin.ShiftDifference.ToString();
                    LottoReportPayoutTxt.Text = lottoScratchWin.LottoReportOnline.ToString();
                    LottoBackofficePayoutTxt.Text = lottoScratchWin.BackOfficeOnline.ToString();
                    LottoAdjustCPayout.Text = lottoScratchWin.AdjustCancelOnline.ToString();
                    LottoEOS1PayoutTxt.Text = lottoScratchWin.EOS1Online.ToString();
                    LottoEOS2PayoutTxt.Text = lottoScratchWin.EOS2Online.ToString();
                    LottoEOS3PayoutTxt.Text = lottoScratchWin.EOS3Online.ToString();
                    TSoldPayoutTxt.Text = lottoScratchWin.TSoldOnline.ToString();
                    LottoShiftDiffPayoutTxt.Text = lottoScratchWin.ShiftDifferenceOnline.ToString();
                    LottoScratchOnlineRemarksTxt.Text = lottoScratchWin.Remarks;
                }
                LoadSavedDocuments(DocumentTypeConstants.LottoScratchAndWinOnline, ScratchWinOnlineDocsDgv);
            }
        }
        private void LottoOnlineAutoCalculation()
        {
            // T.Sold 

            double lottoReportS = 0.00, backOfficeS = 0.00, TSold = 0.00, ShiftDiff = 0.00,
                adjustCancelS = 0.00, adjustCancelP = 0.00, lottoReportP = 0.00, backOfficeP = 0.00, tSoldP = 0.00, shiftDiffP = 0.00;

            lottoReportS = Convert.ToDouble(LottoOnlineReportTxt.Text);
            adjustCancelS = Convert.ToDouble(LottoOnlineAdjustCanceltxt.Text);
            backOfficeS = Convert.ToDouble(LottoOnlineBackOfficetxt.Text);
            adjustCancelP = Convert.ToDouble(LottoAdjustCPayout.Text);
            lottoReportP = Convert.ToDouble(LottoReportPayoutTxt.Text);
            backOfficeP = Convert.ToDouble(LottoBackofficePayoutTxt.Text);
            TSold = lottoReportS - adjustCancelS;

            ShiftDiff = backOfficeS - lottoReportS;
            tSoldP = lottoReportP - adjustCancelP;
            shiftDiffP = backOfficeP - lottoReportP;
            LottoOnlineShiftsDiffTxt.Text = string.IsNullOrEmpty(ShiftDiff.ToString()) ? "0" : ShiftDiff.ToString();
            LottoOnlineTSoldTxt.Text = string.IsNullOrEmpty(TSold.ToString()) ? "0" : TSold.ToString();
            TSoldPayoutTxt.Text = string.IsNullOrEmpty(tSoldP.ToString()) ? "0" : tSoldP.ToString();
            LottoShiftDiffPayoutTxt.Text = string.IsNullOrEmpty(shiftDiffP.ToString()) ? "0" : shiftDiffP.ToString();
        }
        private void SaveLottoAndWinScratchOnlineData()
        {
            #region Scratch Win Online
            try
            {
                int lottoReport = 0, backOffice = 0, adjustCancel = 0, EOS1 = 0, EOS2 = 0, EOS3 = 0, tSold = 0, shiftDiff = 0,
                     backOfficeOnline = 0, adjustCancelOnline = 0, EOS1Online = 0, EOS2Online = 0, EOS3Online = 0;
                double lottoReportOnline = 0.00, tSoldOnline = 0.00, shiftDiffOnline = 0.00;

                lottoReport = Convert.ToInt32(LottoOnlineReportTxt.Text);
                backOffice = Convert.ToInt32(LottoOnlineBackOfficetxt.Text);
                adjustCancel = Convert.ToInt32(LottoOnlineAdjustCanceltxt.Text);
                EOS1 = Convert.ToInt32(LottoOnlineEOS1Txt.Text);
                EOS2 = Convert.ToInt32(LottoOnlineEOS2Txt.Text);
                EOS3 = Convert.ToInt32(LottoOnlineEOS3Txt.Text);
                tSold = Convert.ToInt32(LottoOnlineTSoldTxt.Text);
                shiftDiff = Convert.ToInt32(LottoOnlineShiftsDiffTxt.Text);
                lottoReportOnline = Convert.ToDouble(LottoReportPayoutTxt.Text);
                backOfficeOnline = Convert.ToInt32(LottoBackofficePayoutTxt.Text);
                adjustCancelOnline = Convert.ToInt32(LottoAdjustCPayout.Text);
                EOS1Online = Convert.ToInt32(LottoEOS1PayoutTxt.Text);
                EOS2Online = Convert.ToInt32(LottoEOS2PayoutTxt.Text);
                EOS3Online = Convert.ToInt32(LottoEOS3PayoutTxt.Text);
                tSoldOnline = Convert.ToDouble(TSoldPayoutTxt.Text);
                shiftDiffOnline = Convert.ToDouble(LottoShiftDiffPayoutTxt.Text);

                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);
                    var lottoScratchOnline = context.Tbl_LottoScratchWinOnline.Where(s => s.TransactionDate == date).FirstOrDefault();
                    if (lottoScratchOnline == null)
                    {
                        lottoScratchOnline = new Tbl_LottoScratchWinOnline();
                        lottoScratchOnline.LottoReport = lottoReport;
                        lottoScratchOnline.BackOffice = backOffice;
                        lottoScratchOnline.AdjustCancel = adjustCancel;
                        lottoScratchOnline.EOS1 = EOS1;
                        lottoScratchOnline.EOS2 = EOS2;
                        lottoScratchOnline.EOS3 = EOS3;
                        lottoScratchOnline.TSold = tSold;
                        lottoScratchOnline.ShiftDifference = shiftDiff;
                        lottoScratchOnline.LottoReportOnline = lottoReportOnline;
                        lottoScratchOnline.BackOfficeOnline = backOfficeOnline;
                        lottoScratchOnline.AdjustCancelOnline = adjustCancelOnline;
                        lottoScratchOnline.EOS1Online = EOS1Online;
                        lottoScratchOnline.EOS2Online = EOS2Online;
                        lottoScratchOnline.EOS3Online = EOS3Online;
                        lottoScratchOnline.TSoldOnline = tSoldOnline;
                        lottoScratchOnline.ShiftDifferenceOnline = shiftDiffOnline;
                        lottoScratchOnline.TransactionDate = date.Date;
                        lottoScratchOnline.CreatedBy = Utility.BA_Commman._userId;
                        lottoScratchOnline.CreatedDate = DateTime.Now;
                        lottoScratchOnline.Remarks = LottoScratchOnlineRemarksTxt.Text;
                        context.Tbl_LottoScratchWinOnline.Add(lottoScratchOnline);
                    }
                    else
                    {
                        lottoScratchOnline.LottoReport = lottoReport;
                        lottoScratchOnline.BackOffice = backOffice;
                        lottoScratchOnline.AdjustCancel = adjustCancel;
                        lottoScratchOnline.EOS1 = EOS1;
                        lottoScratchOnline.EOS2 = EOS2;
                        lottoScratchOnline.EOS3 = EOS3;
                        lottoScratchOnline.TSold = tSold;
                        lottoScratchOnline.ShiftDifference = shiftDiff;
                        lottoScratchOnline.LottoReportOnline = lottoReportOnline;
                        lottoScratchOnline.BackOfficeOnline = backOfficeOnline;
                        lottoScratchOnline.AdjustCancelOnline = adjustCancelOnline;
                        lottoScratchOnline.EOS1Online = EOS1Online;
                        lottoScratchOnline.EOS2Online = EOS2Online;
                        lottoScratchOnline.EOS3Online = EOS3Online;
                        lottoScratchOnline.TSoldOnline = tSoldOnline;
                        lottoScratchOnline.ShiftDifferenceOnline = shiftDiffOnline;
                        lottoScratchOnline.ModifiedBy = Utility.BA_Commman._userId;
                        lottoScratchOnline.ModifiedDate = DateTime.Now;
                        lottoScratchOnline.Remarks = LottoScratchOnlineRemarksTxt.Text;
                    }
                    context.SaveChanges();
                    SaveMultipleDocs("Lotto Scratch And Win Online", ScratchWinOnlineDocsDgv);

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + e.Message);
            }
            #endregion
        }
        #endregion
        #region Lotto Weekly
        private void SaveLottoWeeklyDocuments()
        {
            SaveMultipleDocs(DocumentTypeConstants.LottoWeekly, LottoWeeklyDgv);
        }
        private void BindLottoWeeklySavedRecords()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var weeklyLotto = context.Tbl_WeeklyLotto.FirstOrDefault(s => s.InvoiceDate == date);
                if (weeklyLotto != null)
                {
                    RedemptionsTxt.Text = string.IsNullOrEmpty(weeklyLotto.Redemptions.ToString()) ? "0" : weeklyLotto.Redemptions.ToString();
                    AmountDueTxt.Text = string.IsNullOrEmpty(weeklyLotto.AmountDue.ToString()) ? "0" : weeklyLotto.AmountDue.ToString();
                    RetailerBonusTxt.Text = string.IsNullOrEmpty(weeklyLotto.RetailerBonus.ToString()) ? "0" : weeklyLotto.RetailerBonus.ToString();
                    ActivationTxt.Text = string.IsNullOrEmpty(weeklyLotto.Activation.ToString()) ? "0" : weeklyLotto.Activation.ToString();
                    OnlineSoldtxt.Text = string.IsNullOrEmpty(weeklyLotto.OnlineSold.ToString()) ? "0" : weeklyLotto.OnlineSold.ToString();
                    CreditsTxt.Text = string.IsNullOrEmpty(weeklyLotto.Credit.ToString()) ? "0" : weeklyLotto.Credit.ToString();
                }
                BindLottoWeeklySavedDocuments();
            }
        }
        private void BindLottoWeeklySavedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.LottoWeekly, LottoWeeklyDgv);
        }
        private void SaveWeeklyLotto()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var weeklyLotto = context.Tbl_WeeklyLotto.FirstOrDefault(s => s.InvoiceDate == date);
                if (weeklyLotto == null)
                {
                    weeklyLotto = new Tbl_WeeklyLotto();
                    weeklyLotto.InvoiceDate = date;
                    weeklyLotto.IsActive = true;
                    weeklyLotto.CreatedDate = DateTime.Now;
                    weeklyLotto.CreatedBy = 1;
                    weeklyLotto.Redemptions = Convert.ToDouble(RedemptionsTxt.Text);
                    weeklyLotto.AmountDue = Convert.ToDouble(AmountDueTxt.Text);
                    weeklyLotto.RetailerBonus = Convert.ToDouble(RetailerBonusTxt.Text);
                    weeklyLotto.OnlineSold = Convert.ToInt32(OnlineSoldtxt.Text);
                    weeklyLotto.Activation = Convert.ToInt32(ActivationTxt.Text);
                    weeklyLotto.Credit = Convert.ToInt32(CreditsTxt.Text);
                    weeklyLotto.Remarks = LottoWeeklyRemarksTxt.Text;
                    context.Tbl_WeeklyLotto.Add(weeklyLotto);
                }
                else
                {
                    weeklyLotto.IsActive = true;
                    weeklyLotto.ModifiedDate = DateTime.Now;
                    weeklyLotto.ModifiedBy = 1;
                    weeklyLotto.Redemptions = Convert.ToDouble(RedemptionsTxt.Text);
                    weeklyLotto.AmountDue = Convert.ToDouble(AmountDueTxt.Text);
                    weeklyLotto.RetailerBonus = Convert.ToDouble(RetailerBonusTxt.Text);
                    weeklyLotto.OnlineSold = Convert.ToInt32(OnlineSoldtxt.Text);
                    weeklyLotto.Activation = Convert.ToInt32(ActivationTxt.Text);
                    weeklyLotto.Credit = Convert.ToInt32(CreditsTxt.Text);
                    weeklyLotto.Remarks = LottoWeeklyRemarksTxt.Text;
                }

                context.SaveChanges();
                SaveLottoWeeklyDocuments();

            }
        }
        #endregion
        #endregion
        #region Form Events
        private void AllTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        private void label72_Click(object sender, EventArgs e)
        {

        }
        private void textBox15_TextChanged(object sender, EventArgs e)
        {

        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void label35_Click(object sender, EventArgs e)
        {

        }
        private void label48_Click(object sender, EventArgs e)
        {

        }
        private void label65_Click(object sender, EventArgs e)
        {

        }
        private void LottoActivatedAdd5txt_TextChanged(object sender, EventArgs e)
        {

        }
        private void LottoUnActivatedAdd30Txt_TextChanged(object sender, EventArgs e)
        {

        }
        #region Lotto UnActivated Events
        private void LottoUnActivatedAdd1Txt_Leave(object sender, EventArgs e)
        {
            try
            {
                unActivatedLottoFormula();
                AdditionToActivatedSectionAutoPopulate();
            }
            catch (Exception ex)
            {
                ShellComman.ActivityName = "LottoUnActivatedAdd1Txt_Leave";
                ShellComman.ActivityForm = ShellERPActivities.LottoMasterForm;
                ShellComman.ActivityStatus = "Exception";
                ShellComman.ActivityData = "Messgae " + ex.Message + " Inner Exception " + ex.InnerException;
                EmailHandler email = new EmailHandler();
                email.SendExceptionNotification();
                MessageBox.Show(ex.Message);
            }
        }
        #endregion

        #region Lotto Activated Events
        private void LottoActivatedAdd1Txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ActivatedLottoFormula();
                AdditionToScratchAndWinAddQty();
                ScratchWin1AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion
        private void label62_Click(object sender, EventArgs e)
        {

        }
        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }
        private void label9_Click(object sender, EventArgs e)
        {

        }
        private void label39_Click(object sender, EventArgs e)
        {

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void label13_Click(object sender, EventArgs e)
        {

        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }
        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                IntialDocumentsLoad();
                BindDocumentsForAllGrid();
                BindLottoUnActivatedData();
                BindLottoActivatedData();
                BindLottoWeeklySavedRecords();
                LoadScratchAndWinSectionData();
                LoadScratchWinOnlineData();
                unActivatedLottoFormula();
                ActivatedLottoFormula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoMasterForm_Load(object sender, EventArgs e)
        {
            try
            {
                // DateCalender.MaxDate = DateTime.Now.Date;
                IntialDocumentsLoad();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoUnActivatedBtn_Click(object sender, EventArgs e)
        {
            try
            {
                // Set Variable Values

                SetValues();
                SaveLottoUnActivated();
                SaveLottoActivated();
                SaveScratchAndWinSectionData();
                SaveLottoAndWinScratchOnlineData();
                SaveWeeklyLotto();
                MessageBox.Show("Lotto Records Saved Successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpentxt_Leave(object sender, EventArgs e)
        {

        }
        private void LottoOnlineReportTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                LottoOnlineAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void LottoOnlin1RowChange_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin1AutoCalculation();
                TotalSumCalculation();
                TotalActivatedInventory();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpen2txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin2AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpen3txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin3AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpen4txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin4AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpen5txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin5AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpen7txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin7AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpen10txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin10AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpen20txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin20AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoScratchActualOpen30txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ScratchWin30AutoCalculation();
                TotalSumCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LottoActivatedReturn150Txt_Leave(object sender, EventArgs e)
        {
            try
            {
                ActivatedLottoFormula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
