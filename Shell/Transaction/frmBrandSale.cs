﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class FrmBrandSales : Form
    {
        public FrmBrandSales()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Saved!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Updated!");
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Record Deleted!");
        }

        private void btnView_Click(object sender, EventArgs e)
        {
           
            GrpManageSale.Visible = false;

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            GrpManageSale.Visible = true;
        }

        private void FrmBrandSales_Load(object sender, EventArgs e)
        {

            GrpManageSale.Visible = false;
        }


    }
}
