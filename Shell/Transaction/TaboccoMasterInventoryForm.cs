﻿using Framework.Data;
using System;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
using DAL;
using Framework;

namespace Shell.Transaction
{
    public partial class TaboccoMasterInventoryForm : BaseForm
    {
        public TaboccoMasterInventoryForm()
        {
            InitializeComponent();
        }

        #region Private Methods
        #region Common
        private void BindSavedDocumentsGrid(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType).Select(s => new DocumentList { DocId = s.UploadedDocID, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();
                var selectedDocs = (from p in docList
                                    join s in context.Mst_DocUpload on p.DocId equals s.UploadId
                                    select new DocumentList
                                    {
                                        DocId = p.DocId,
                                        DocsIdentity = s.DocsIdentity,
                                        TransactionDate = p.TransactionDate
                                    }).ToList();
                gridView.DataSource = selectedDocs;

                gridView.Columns[1].Visible = false;
                gridView.Columns[3].Visible = false;
            }
        }
        private void BindDocumentsForAllGrid()
        {
            BindSavedDocumentsGrid(DocumentTypeConstants.TobaccoDocType, TobaccoDgv);
            BindSavedDocumentsGrid(DocumentTypeConstants.Chew, ChewDgv);
            BindSavedDocumentsGrid(DocumentTypeConstants.CigarDocType, CigarDgv);
            BindSavedDocumentsGrid(DocumentTypeConstants.ECigar, ECigarDgv);
            BindSavedDocumentsGrid(DocumentTypeConstants.SinglesAndPapers, SinglesDgv);
        }
        private void IntializeDocumentsGrid(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Mst_DocUpload.Where(s => s.DocsType == docType).Select(s => new DocumentList { DocId = s.UploadId, DocsIdentity = s.DocsIdentity, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();
                gridView.DataSource = docList;
                gridView.Columns[1].Visible = false;
                gridView.Columns[3].Visible = false;
            }
        }
        private void IntialDocumentsLoad()
        {
            IntializeDocumentsGrid(DocumentTypeConstants.TobaccoDocType, TobaccoDgv);
            IntializeDocumentsGrid(DocumentTypeConstants.Chew, ChewDgv);
            IntializeDocumentsGrid(DocumentTypeConstants.CigarDocType, CigarDgv);
            IntializeDocumentsGrid(DocumentTypeConstants.ECigar, ECigarDgv);
            IntializeDocumentsGrid(DocumentTypeConstants.SinglesAndPapers, SinglesDgv);
        }
        private bool IsPreviousDaySelected = false;
        private void LoadSavedDocuments(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType).Select(s => new DocumentList { DocId = s.UploadedDocID, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();

                foreach (DataGridViewRow item in gridView.Rows)
                {
                    var docs = docList.Where(s => s.DocId == Convert.ToInt64(item.Cells["DocId"].Value)).FirstOrDefault();
                    if (docs != null && docs.DocId == Convert.ToInt64(item.Cells["DocId"].Value))
                    {
                        item.Cells[0].Value = true;
                    }
                }
            }

        }
        List<string> docIds = new List<string>();
        private void SaveMultipleDocs(string docType, DataGridView gridView)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime tranDate = Convert.ToDateTime(DateCalender.Text);
                    foreach (DataGridViewRow row in gridView.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            int docId = Convert.ToInt32((row.Cells[1].Value));
                            docIds.Add(row.Cells[1].Value.ToString());
                            var docDelete = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType && s.TransactionDate == tranDate).Select(s => new { s }).ToList();
                            foreach (var date in docDelete)
                            {
                                if (Convert.ToDateTime(date.s.TransactionDate) == tranDate)
                                {
                                    context.Tbl_EODCommonDocuments.Remove(date.s);
                                }
                            }
                            context.SaveChanges();
                            var docExist = context.Tbl_EODCommonDocuments.FirstOrDefault(s => s.UploadedDocID == docId && s.DocumentType == docType);
                            if (docExist == null)
                            {
                                Tbl_EODCommonDocuments docsObj = new Tbl_EODCommonDocuments();
                                docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                                docsObj.DocumentType = docType;
                                docsObj.InventoryType = docType;
                                docsObj.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                                docsObj.IsActive = true;
                                docsObj.CreatedBy = Utility.BA_Commman._userId;
                                docsObj.CreatedDate = DateTime.Now;
                                context.Tbl_EODCommonDocuments.Add(docsObj);
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void InitializeReasonList()
        {
            SubstractionReasonList reasonList = new SubstractionReasonList();
            Tobacco8SReasonCmb.DataSource = reasonList.ReasonList();
            Tobacco8SReasonCmb.ValueMember = "ID";
            Tobacco8SReasonCmb.DisplayMember = "Name";
            Tobacco10SReasonCmb.DataSource = reasonList.ReasonList();
            Tobacco10SReasonCmb.ValueMember = "ID";
            Tobacco10SReasonCmb.DisplayMember = "Name";
            ChewReasonCombo.DataSource = reasonList.ReasonList();
            ChewReasonCombo.ValueMember = "ID";
            ChewReasonCombo.DisplayMember = "Name";
            Cigar10sReasonCombo.DataSource = reasonList.ReasonList();
            Cigar10sReasonCombo.ValueMember = "ID";
            Cigar10sReasonCombo.DisplayMember = "Name";
            Cigar8sReasonCombo.DataSource = reasonList.ReasonList();
            Cigar8sReasonCombo.ValueMember = "ID";
            Cigar8sReasonCombo.DisplayMember = "Name";
            Cigar5sReasonCombo.DataSource = reasonList.ReasonList();
            Cigar5sReasonCombo.ValueMember = "ID";
            Cigar5sReasonCombo.DisplayMember = "Name";
            ECigarReasonCmb.DataSource = reasonList.ReasonList();
            ECigarReasonCmb.ValueMember = "ID";
            ECigarReasonCmb.DisplayMember = "Name";
            SinglesReasonCmb.DataSource = reasonList.ReasonList();
            SinglesReasonCmb.ValueMember = "ID";
            SinglesReasonCmb.DisplayMember = "Name";
            MilkReason310mlCmb.DataSource = reasonList.ReasonList();
            MilkReason310mlCmb.ValueMember = "ID";
            MilkReason310mlCmb.DisplayMember = "Name";
            MilkReason473mlCmb.DataSource = reasonList.ReasonList();
            MilkReason473mlCmb.ValueMember = "ID";
            MilkReason473mlCmb.DisplayMember = "Name";
            MilkReason1LCmb.DataSource = reasonList.ReasonList();
            MilkReason1LCmb.ValueMember = "ID";
            MilkReason1LCmb.DisplayMember = "Name";
            MilkReason2LCmb.DataSource = reasonList.ReasonList();
            MilkReason2LCmb.ValueMember = "ID";
            MilkReason2LCmb.DisplayMember = "Name";
            MilkReason4LCmb.DataSource = reasonList.ReasonList();
            MilkReason4LCmb.ValueMember = "ID";
            MilkReason4LCmb.DisplayMember = "Name";
        }
        private bool IsRowSelected(DataGridView gridView)
        {
            int count = 0;
            foreach (DataGridViewRow item in gridView.Rows)
            {
                if (Convert.ToBoolean(item.Cells[0].Value) == true)
                {
                    count++;
                }
            }
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region Tabocco
        private void ResetTobacco8s()
        {
            TobaccoAdd8STxt.Text = "0";
            TobaccoSubstract8STxt.Text = "0";
            TobaccoRemarksTxt.Text = "";
        }
        private void ResetTobacco10s()
        {
            TobaccoAdd10STxt.Text = "0";
            TobaccoSubstract10STxt.Text = "0";
            TobaccoRemarksTxt.Text = "";
        }
        private void LoadPreviosDayTaboccoOpening()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                selectedDate = selectedDate.AddDays(-1);
                var previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Tobacco8S).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    TaboccoBalance8Stxt.Text = previosDayOpening.Balance.ToString();

                }
                else
                {
                    TaboccoBalance8Stxt.Text = "0";
                }
                previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Tobacco10S).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    TobaccoBalance10STxt.Text = previosDayOpening.Balance.ToString();

                }
                else
                {
                    TobaccoBalance10STxt.Text = "0";
                }
            }
        }
        private void LoadTobaccoInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var loadSavedRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Tobacco8S).FirstOrDefault();
                if (loadSavedRecord != null)
                {
                    TaboccoBalance8Stxt.Text = string.IsNullOrEmpty(loadSavedRecord.Balance.ToString()) ? "0" : loadSavedRecord.Balance.ToString();
                    TobaccoAdd8STxt.Text = string.IsNullOrEmpty(loadSavedRecord.Add.ToString()) ? "0" : loadSavedRecord.Add.ToString();
                    TobaccoSubstract8STxt.Text = string.IsNullOrEmpty(loadSavedRecord.Substract.ToString()) ? "0" : loadSavedRecord.Substract.ToString();
                    TobaccoRemarksTxt.Text = loadSavedRecord.Remarks;
                    Tobacco8SReasonCmb.SelectedValue = Convert.ToInt32(loadSavedRecord.SubstractionReason);
                    if (loadSavedRecord.SubstractionReason == "1" && loadSavedRecord.VendorId != null)
                    {
                        Tobacco8SReasonCmb.Enabled = true;
                        BindTobacco8sVendors();
                        TobaccoSite8s.SelectedValue = loadSavedRecord.VendorId;
                        TobaccoSite8s.Enabled = true;
                        TobaccoVendor8sLabel.Text = "Vendor";

                    }
                    else if (loadSavedRecord.SubstractionReason == "2")
                    {

                        Tobacco8SReasonCmb.Enabled = true;
                        BindSite8sDetails();
                        TobaccoSite8s.SelectedValue = loadSavedRecord.StoreId;
                        TobaccoSite8s.Enabled = true;
                        TobaccoVendor8sLabel.Text = "Site";
                    }

                }
                loadSavedRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Tobacco10S).FirstOrDefault();
                if (loadSavedRecord != null)
                {
                    TobaccoBalance10STxt.Text = string.IsNullOrEmpty(loadSavedRecord.Balance.ToString()) ? "0" : loadSavedRecord.Balance.ToString();
                    TobaccoAdd10STxt.Text = string.IsNullOrEmpty(loadSavedRecord.Add.ToString()) ? "0" : loadSavedRecord.Add.ToString();
                    TobaccoSubstract10STxt.Text = string.IsNullOrEmpty(loadSavedRecord.Substract.ToString()) ? "0" : loadSavedRecord.Substract.ToString();
                    TobaccoRemarksTxt.Text = loadSavedRecord.Remarks;
                    Tobacco10SReasonCmb.SelectedValue = Convert.ToInt32(loadSavedRecord.SubstractionReason);
                    if (loadSavedRecord.SubstractionReason == "1")
                    {
                        Tobacco10SReasonCmb.Enabled = true;
                        BindTobacco10sVendors();
                        TobaccoSite10s.SelectedValue = loadSavedRecord.VendorId;
                        TobaccoSite10s.Enabled = true;
                        TobaccoVendor10sLabel.Text = "Vendor";

                    }
                    else if ((loadSavedRecord.SubstractionReason == "2"))
                    {
                        Tobacco10SReasonCmb.Enabled = true;
                        BindSite10sDetails();
                        TobaccoSite10s.SelectedValue = loadSavedRecord.StoreId;
                        TobaccoSite10s.Enabled = true;
                        TobaccoVendor10sLabel.Text = "Site";

                    }
                }
                LoadTobaccoSavedDocuments();
            }
        }
        private void SaveTobaccoInventoryFor8SSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Tobacco8S).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(TaboccoBalance8Stxt.Text);
                    newRecord.Add = Convert.ToInt32(TobaccoAdd8STxt.Text);
                    newRecord.Substract = Convert.ToInt32(TobaccoSubstract8STxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Tobacco8S;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    newRecord.Remarks = TobaccoRemarksTxt.Text;
                    if (Tobacco8SReasonCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Tobacco8SReasonCmb.SelectedValue.ToString();
                        if (Tobacco8SReasonCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(TobaccoSite8s.SelectedValue);
                        }
                        else if (Tobacco8SReasonCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(TobaccoSite8s.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(TaboccoBalance8Stxt.Text);
                    newRecord.Add = Convert.ToInt32(TobaccoAdd8STxt.Text);
                    newRecord.Substract = Convert.ToInt32(TobaccoSubstract8STxt.Text);
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    newRecord.Remarks = TobaccoRemarksTxt.Text;
                    if (Tobacco8SReasonCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Tobacco8SReasonCmb.SelectedValue.ToString();
                        if (Tobacco8SReasonCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(TobaccoSite8s.SelectedValue);
                        }
                        else if (Tobacco8SReasonCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(TobaccoSite8s.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.TobaccoDocType, TobaccoDgv);
            }
        }
        private bool ValidateTobaccoInventoryFor8SFields()
        {
            if (!IsRowSelected(TobaccoDgv) && string.IsNullOrEmpty(TobaccoRemarksTxt.Text))
            {
                MessageBox.Show("Tobacco Section - Either select document or enter remarks");
                return false;
            }
            return true;
        }
        private void SaveTobaccoInventoryFor10SSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Tobacco10S).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(TobaccoBalance10STxt.Text);
                    newRecord.Add = Convert.ToInt32(TobaccoAdd10STxt.Text);
                    newRecord.Substract = Convert.ToInt32(TobaccoSubstract10STxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Tobacco10S;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    newRecord.Remarks = TobaccoRemarksTxt.Text;
                    if (Tobacco8SReasonCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Tobacco10SReasonCmb.SelectedValue.ToString();
                        if (Tobacco10SReasonCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(TobaccoSite10s.SelectedValue);
                        }
                        else if (Tobacco8SReasonCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(TobaccoSite10s.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(TobaccoBalance10STxt.Text);
                    newRecord.Add = Convert.ToInt32(TobaccoAdd10STxt.Text);
                    newRecord.Substract = Convert.ToInt32(TobaccoSubstract10STxt.Text);
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    newRecord.Remarks = TobaccoRemarksTxt.Text;
                    if (Tobacco10SReasonCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Tobacco10SReasonCmb.SelectedValue.ToString();
                        if (Tobacco10SReasonCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(TobaccoSite10s.SelectedValue);
                        }
                        else if (Tobacco10SReasonCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(TobaccoSite10s.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
            }
        }
        private void LoadTobaccoSavedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.TobaccoDocType, TobaccoDgv);
        }
        private void LoadAllTobacco()
        {
            LoadTobaccoInventoryForSelectedDate();
        }
        private void TobaccoAutoFormula()
        {
            LoadPreviosDayTaboccoOpening();
            int Add8 = 0, Substract8 = 0, Balance8 = 0, Add10 = 0, Substract10 = 0, Balance10 = 0, TotalPacks = 0;
            Add8 = Convert.ToInt32(TobaccoAdd8STxt.Text);
            Substract8 = Convert.ToInt32(TobaccoSubstract8STxt.Text);
            Balance8 = Convert.ToInt32(TaboccoBalance8Stxt.Text);
            Add10 = Convert.ToInt32(TobaccoAdd10STxt.Text);
            Substract10 = Convert.ToInt32(TobaccoSubstract10STxt.Text);
            Balance10 = Convert.ToInt32(TobaccoBalance10STxt.Text);
            Balance8 = (Balance8 + Add8) - Substract8;
            //  TaboccoBalance8Stxt.Text = Balance8.ToString();
            Balance10 = (Balance10 + Add10) - Substract10;
            // TobaccoBalance10STxt.Text = Balance10.ToString();
            TotalPacks = (Balance8 * 8 + Balance10 * 10);
            TobaccoTotalPacksTxt.Text = TotalPacks.ToString();
        }
        private void HideShowReason8s(int substractionValue)
        {
            if (substractionValue != 0)
            {
                //TobaccoVendor8sLabel.Enabled = true;
                Tobacco8SReasonCmb.Enabled = true;
            }
            else
            {
                //TobaccoVendor8sLabel.Enabled = false;
                Tobacco8SReasonCmb.Enabled = false;
            }
        }
        private void HideShowReason10s(int substractionValue)
        {
            if (substractionValue != 0)
            {
                //TobaccoVendor10sLabel.Enabled = true;
                Tobacco10SReasonCmb.Enabled = true;
            }
            else
            {
                //TobaccoVendor10sLabel.Enabled = false;
                Tobacco10SReasonCmb.Enabled = false;
            }
        }
        public void BindTobacco8sVendors()
        {
            TobaccoSite8s.DataSource = null;
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            TobaccoSite8s.Items.Insert(0, "--Select--");
            TobaccoSite8s.DataSource = vendorList;
            TobaccoSite8s.DisplayMember = "VendorName";
            TobaccoSite8s.ValueMember = "VendorId";
            TobaccoVendor8sLabel.Text = "Vendor";
        }
        public void BindTobacco10sVendors()
        {
            TobaccoSite10s.DataSource = null;
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            TobaccoSite10s.DataSource = vendorList;
            TobaccoSite10s.DisplayMember = "VendorName";
            TobaccoSite10s.ValueMember = "VendorId";
            TobaccoVendor10sLabel.Text = "Vendor";
        }
        private void BindSite8sDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                TobaccoSite8s.DataSource = null;
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                TobaccoSite8s.DataSource = storeMaster;
                TobaccoSite8s.DisplayMember = "StoreName";
                TobaccoSite8s.ValueMember = "StoreId";
                TobaccoVendor8sLabel.Text = "Site";

            }
        }
        private void BindSite10sDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                TobaccoSite10s.DataSource = null;
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                TobaccoSite10s.DataSource = storeMaster;
                TobaccoSite10s.DisplayMember = "StoreName";
                TobaccoSite10s.ValueMember = "StoreId";
                TobaccoVendor10sLabel.Text = "Site";
            }
        }
        private void MoveInventoryToFrontOffice()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var addition = context.Tbl_EODAddition.FirstOrDefault(s => s.TransactionDate == date && s.AdditionType == "Addition");
                if (addition != null)
                {
                    addition.AdditionType = "Addition";
                    addition.C8Pack = Convert.ToInt32(TobaccoSubstract8STxt.Text);
                    addition.C10Pack = Convert.ToInt32(TobaccoSubstract10STxt.Text);
                    addition.Cigar10Pack = Convert.ToInt32(CigarSubstract10sTxt.Text);
                    addition.Cigar8Pack = Convert.ToInt32(CigarSubstract8sTxt.Text);
                    addition.Cigar5Pack = Convert.ToInt32(CigarSubstract5sTxt.Text);
                    addition.Chews = Convert.ToInt32(ChewSubstractTxt.Text);
                    addition.E_Cigar = Convert.ToInt32(ECigarSubstractTxt.Text);
                    addition.Total = Convert.ToInt32(addition.Cigar10Pack + addition.Cigar8Pack + addition.Cigar5Pack + addition.C8Pack + addition.C10Pack + addition.E_Cigar + addition.Chews);
                    addition.PackTotal = Convert.ToInt32((addition.Cigar8Pack * 8 + addition.C10Pack * 10 + addition.Cigar5Pack * 5 + addition.C8Pack * 8 + addition.C10Pack * 10 + addition.Chews + addition.E_Cigar));
                    addition.Remarks = "Moved from Back office";
                    addition.IsActive = true;
                    addition.ModifiedBy = Utility.BA_Commman._userId;
                    addition.ModifiedDate = DateTime.Now;
                }
                else
                {
                    addition = new Tbl_EODAddition();
                    addition.AdditionType = "Addition";
                    addition.C8Pack = Convert.ToInt32(TobaccoSubstract8STxt.Text);
                    addition.C10Pack = Convert.ToInt32(TobaccoSubstract10STxt.Text);
                    addition.Cigar10Pack = Convert.ToInt32(CigarSubstract10sTxt.Text);
                    addition.Cigar8Pack = Convert.ToInt32(CigarSubstract8sTxt.Text);
                    addition.Cigar5Pack = Convert.ToInt32(CigarSubstract5sTxt.Text);
                    addition.Chews = Convert.ToInt32(ChewSubstractTxt.Text);
                    addition.E_Cigar = Convert.ToInt32(ECigarSubstractTxt.Text);
                    addition.Total = Convert.ToInt32(addition.Cigar10Pack + addition.Cigar8Pack + addition.Cigar5Pack + addition.C8Pack + addition.C10Pack + addition.E_Cigar + addition.Chews);
                    addition.PackTotal = Convert.ToInt32((addition.Cigar8Pack * 8 + addition.C10Pack * 10 + addition.Cigar5Pack * 5 + addition.C8Pack * 8 + addition.C10Pack * 10 + addition.Chews + addition.E_Cigar));
                    addition.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                    addition.Remarks = "Moved from Back office";
                    addition.IsActive = true;
                    addition.CreatedBy = Utility.BA_Commman._userId;
                    addition.CreatedDate = DateTime.Now;
                    context.Tbl_EODAddition.Add(addition);
                }
                context.SaveChanges();
            }
        }

        #endregion
        #region Chew
        private void LoadPreviosDayChewOpening()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                selectedDate = selectedDate.AddDays(-1);
                var previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Chew).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    ChewBalanceTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    ChewBalanceTxt.Text = "0";
                }

            }
        }
        private void LoadChewInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var loadSavedRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Chew).FirstOrDefault();
                if (loadSavedRecord != null)
                {
                    ChewBalanceTxt.Text = loadSavedRecord.Balance.ToString();
                    ChewAddTxt.Text = loadSavedRecord.Add.ToString();
                    ChewSubstractTxt.Text = loadSavedRecord.Substract.ToString();
                    ChewRemarksTxt.Text = loadSavedRecord.Remarks;
                    ChewReasonCombo.SelectedValue = Convert.ToInt32(loadSavedRecord.SubstractionReason);
                    if (loadSavedRecord.SubstractionReason == "1" && loadSavedRecord.VendorId != null)
                    {
                        ChewReasonCombo.Enabled = true;
                        BindChewVendors();
                        ChewSiteCombobox.SelectedValue = loadSavedRecord.VendorId;
                        ChewSiteCombobox.Enabled = true;
                        ChewSiteLabel.Text = "Vendor";

                    }
                    else if (loadSavedRecord.SubstractionReason == "2")
                    {
                        ChewReasonCombo.Enabled = true;
                        BindChewSiteDetails();
                        ChewSiteCombobox.SelectedValue = loadSavedRecord.StoreId;
                        ChewSiteCombobox.Enabled = true;
                        ChewSiteLabel.Text = "Site";

                    }
                }
                LoadChewSavedDocuments();
            }
        }
        private void SaveInventoryForChewSSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Chew).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(ChewBalanceTxt.Text);
                    newRecord.Add = Convert.ToInt32(ChewAddTxt.Text);
                    newRecord.Substract = Convert.ToInt32(ChewSubstractTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Chew;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    newRecord.Remarks = ChewRemarksTxt.Text;
                    if (ChewReasonCombo.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = ChewReasonCombo.SelectedValue.ToString();
                        if (ChewReasonCombo.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(ChewSiteCombobox.SelectedValue);
                        }
                        else if (ChewReasonCombo.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(ChewSiteCombobox.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(ChewBalanceTxt.Text);
                    newRecord.Add = Convert.ToInt32(ChewAddTxt.Text);
                    newRecord.Substract = Convert.ToInt32(ChewSubstractTxt.Text);
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    newRecord.Remarks = ChewRemarksTxt.Text;
                    if (ChewReasonCombo.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = ChewReasonCombo.SelectedValue.ToString();
                        if (ChewReasonCombo.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(ChewSiteCombobox.SelectedValue);
                        }
                        else if (ChewReasonCombo.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(ChewSiteCombobox.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.Chew, ChewDgv);
            }
        }
        private void LoadChewSavedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.Chew, ChewDgv);
        }
        private void LoadChew()
        {
            LoadChewInventoryForSelectedDate();
        }
        private void ChewAutoFormula()
        {
            LoadPreviosDayChewOpening();
            int Add = 0, Substract = 0, Balance = 0;
            Add = Convert.ToInt32(ChewAddTxt.Text);
            Substract = Convert.ToInt32(ChewSubstractTxt.Text);
            Balance = Convert.ToInt32(ChewBalanceTxt.Text);
            Balance = (Balance + Add) - Substract;
            // ChewBalanceTxt.Text = Balance.ToString();
        }
        private void BindChewSiteDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                ChewSiteCombobox.DataSource = storeMaster;
                ChewSiteCombobox.DisplayMember = "StoreName";
                ChewSiteCombobox.ValueMember = "StoreId";
                ChewSiteLabel.Text = "Site";
            }
        }
        public void BindChewVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            ChewSiteCombobox.DataSource = vendorList;
            ChewSiteCombobox.DisplayMember = "VendorName";
            ChewSiteCombobox.ValueMember = "VendorId";
            ChewSiteLabel.Text = "Vendor";
        }
        private void HideShowReasonChew(int substractionValue)
        {
            if (substractionValue != 0)
            {
                //TobaccoVendor8sLabel.Enabled = true;
                ChewReasonCombo.Enabled = true;
            }
            else
            {
                //TobaccoVendor8sLabel.Enabled = false;
                ChewReasonCombo.Enabled = false;
            }
        }
        #endregion
        #region Cigar
        private void LoadPreviosDayCigarOpening()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                selectedDate = selectedDate.AddDays(-1);
                var previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar10S).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    CigarBalance10sTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    CigarBalance10sTxt.Text = "0";
                }
                previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar8S).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    CigarBalance8sTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    CigarBalance8sTxt.Text = "0";
                }
                previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar5S).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    CigarBalance5sTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    CigarBalance5sTxt.Text = "0";
                }

            }
        }
        private void LoadCigarInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var saveRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar10S).FirstOrDefault();
                if (saveRecord != null)
                {
                    CigarBalance10sTxt.Text = saveRecord.Balance.ToString();
                    CigarAdd10sTxt.Text = saveRecord.Add.ToString();
                    CigarSubstract10sTxt.Text = saveRecord.Substract.ToString();
                    CigarRemarksTxt.Text = saveRecord.Remarks;
                    Cigar10sReasonCombo.SelectedValue = Convert.ToInt32(saveRecord.SubstractionReason);
                    if (saveRecord.SubstractionReason == "1" && saveRecord.VendorId != null)
                    {
                        Cigar10sReasonCombo.Enabled = true;
                        BindCigar10sVendors();
                        CigarSite10sCmb.SelectedValue = saveRecord.VendorId;
                        CigarSite10sCmb.Enabled = true;
                        CigarSite10sLabel.Text = "Vendor";

                    }
                    else if (saveRecord.SubstractionReason == "2")
                    {
                        Cigar10sReasonCombo.Enabled = true;
                        BindCigar10sSiteDetails();
                        CigarSite10sCmb.SelectedValue = saveRecord.StoreId;
                        CigarSite10sCmb.Enabled = true;
                        CigarSite10sLabel.Text = "Site";
                    }
                }
                saveRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar8S).FirstOrDefault();
                if (saveRecord != null)
                {
                    CigarBalance8sTxt.Text = saveRecord.Balance.ToString();
                    CigarAdd8sTxt.Text = saveRecord.Add.ToString();
                    CigarSubstract8sTxt.Text = saveRecord.Substract.ToString();
                    CigarRemarksTxt.Text = saveRecord.Remarks;
                    Cigar8sReasonCombo.SelectedValue = Convert.ToInt32(saveRecord.SubstractionReason);
                    if (saveRecord.SubstractionReason == "1" && saveRecord.VendorId != null)
                    {
                        Cigar8sReasonCombo.Enabled = true;
                        BindCigar8sVendors();
                        CigarSite8sCmb.SelectedValue = saveRecord.VendorId;
                        CigarSite8sCmb.Enabled = true;
                        CigarSite8sLabel.Text = "Vendor";

                    }
                    else if (saveRecord.SubstractionReason == "2")
                    {
                        Cigar8sReasonCombo.Enabled = true;
                        BindCigar8sSiteDetails();
                        CigarSite8sCmb.SelectedValue = saveRecord.StoreId;
                        CigarSite8sCmb.Enabled = true;
                        CigarSite8sLabel.Text = "Site";
                    }
                }
                saveRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar5S).FirstOrDefault();
                if (saveRecord != null)
                {
                    CigarBalance5sTxt.Text = saveRecord.Balance.ToString();
                    CigarAdd5sTxt.Text = saveRecord.Add.ToString();
                    CigarAdd5sTxt.Text = saveRecord.Substract.ToString();
                    CigarRemarksTxt.Text = saveRecord.Remarks;
                    Cigar5sReasonCombo.SelectedValue = Convert.ToInt32(saveRecord.SubstractionReason);
                    if (saveRecord.SubstractionReason == "1" && saveRecord.VendorId != null)
                    {
                        Cigar5sReasonCombo.Enabled = true;
                        BindCigar5sVendors();
                        CigarSite5sCmb.SelectedValue = saveRecord.VendorId;
                        CigarSite5sCmb.Enabled = true;
                        CigarSite5sLabel.Text = "Vendor";

                    }
                    else if (saveRecord.SubstractionReason == "2")
                    {
                        Cigar5sReasonCombo.Enabled = true;
                        BindCigar5sSiteDetails();
                        CigarSite5sCmb.SelectedValue = saveRecord.StoreId;
                        CigarSite5sCmb.Enabled = true;
                        CigarSite5sLabel.Text = "Site";
                    }
                }
                LoadCigarSavedDocuments();
            }
        }
        private void SaveInventoryForCigar10SSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar10S).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(CigarBalance10sTxt.Text);
                    newRecord.Add = Convert.ToInt32(CigarAdd10sTxt.Text);
                    newRecord.Substract = Convert.ToInt32(CigarSubstract10sTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Cigar10S;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    newRecord.Remarks = CigarRemarksTxt.Text;
                    if (Cigar10sReasonCombo.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Cigar10sReasonCombo.SelectedValue.ToString();
                        if (Cigar10sReasonCombo.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(CigarSite10sCmb.SelectedValue);
                        }
                        else if (Cigar10sReasonCombo.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(CigarSite10sCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(CigarBalance10sTxt.Text);
                    newRecord.Add = Convert.ToInt32(CigarAdd10sTxt.Text);
                    newRecord.Substract = Convert.ToInt32(CigarSubstract10sTxt.Text);
                    newRecord.ModifiedBy = Utility.BA_Commman._userId;
                    newRecord.Remarks = CigarRemarksTxt.Text;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (Cigar10sReasonCombo.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Cigar10sReasonCombo.SelectedValue.ToString();
                        if (Cigar10sReasonCombo.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(CigarSite10sCmb.SelectedValue);
                        }
                        else if (Cigar10sReasonCombo.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(CigarSite10sCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.CigarDocType, ChewDgv);
            }
        }
        private void SaveInventoryForCigar8SSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar8S).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(CigarBalance8sTxt.Text);
                    newRecord.Add = Convert.ToInt32(CigarAdd8sTxt.Text);
                    newRecord.Substract = Convert.ToInt32(CigarSubstract8sTxt.Text);
                    newRecord.Remarks = CigarRemarksTxt.Text;
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Cigar8S;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (Cigar8sReasonCombo.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Cigar8sReasonCombo.SelectedValue.ToString();
                        if (Cigar8sReasonCombo.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(CigarSite8sCmb.SelectedValue);
                        }
                        else if (Cigar8sReasonCombo.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(CigarSite8sCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(CigarBalance8sTxt.Text);
                    newRecord.Add = Convert.ToInt32(CigarAdd8sTxt.Text);
                    newRecord.Substract = Convert.ToInt32(CigarSubstract8sTxt.Text);
                    newRecord.Remarks = CigarRemarksTxt.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (Cigar8sReasonCombo.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Cigar8sReasonCombo.SelectedValue.ToString();
                        if (Cigar8sReasonCombo.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(CigarSite8sCmb.SelectedValue);
                        }
                        else if (Cigar8sReasonCombo.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(CigarSite8sCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();

            }
        }
        private void SaveInventoryForCigar5SSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Cigar5S).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(CigarBalance5sTxt.Text);
                    newRecord.Add = Convert.ToInt32(CigarAdd5sTxt.Text);
                    newRecord.Substract = Convert.ToInt32(CigarSubstract5sTxt.Text);
                    newRecord.Remarks = CigarRemarksTxt.Text;
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Cigar5S;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (Cigar5sReasonCombo.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Cigar5sReasonCombo.SelectedValue.ToString();
                        if (Cigar5sReasonCombo.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(CigarSite5sCmb.SelectedValue);
                        }
                        else if (Cigar5sReasonCombo.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(CigarSite5sCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(CigarBalance5sTxt.Text);
                    newRecord.Add = Convert.ToInt32(CigarAdd5sTxt.Text);
                    newRecord.Substract = Convert.ToInt32(CigarSubstract5sTxt.Text);
                    newRecord.Remarks = CigarRemarksTxt.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (Cigar5sReasonCombo.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = Cigar5sReasonCombo.SelectedValue.ToString();
                        if (Cigar5sReasonCombo.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(CigarSite5sCmb.SelectedValue);
                        }
                        else if (Cigar5sReasonCombo.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(CigarSite5sCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
            }
        }
        private void LoadCigarSavedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.CigarDocType, CigarDgv);
        }
        private void LoadAllCigar()
        {
            LoadCigarInventoryForSelectedDate();
        }
        private void CigarAutoFormula()
        {
            LoadPreviosDayCigarOpening();

            int Add8 = 0, Substract8 = 0, Balance8 = 0, Add10 = 0, Substract10 = 0, Balance10 = 0, Add5 = 0, Substract5 = 0, Balance5 = 0, TotalPacks = 0;
            Add10 = Convert.ToInt32(CigarAdd10sTxt.Text);
            Substract10 = Convert.ToInt32(CigarSubstract10sTxt.Text);
            Balance10 = Convert.ToInt32(CigarBalance10sTxt.Text);
            Balance10 = (Balance10 + Add10) - Substract10;
            // CigarBalance10sTxt.Text = Balance10.ToString();
            Add8 = Convert.ToInt32(CigarAdd8sTxt.Text);
            Substract8 = Convert.ToInt32(CigarSubstract8sTxt.Text);
            Balance8 = Convert.ToInt32(CigarBalance8sTxt.Text);
            Balance8 = (Balance8 + Add8) - Substract8;
            // CigarBalance8sTxt.Text = Balance8.ToString();
            Add5 = Convert.ToInt32(CigarAdd5sTxt.Text);
            Substract5 = Convert.ToInt32(CigarSubstract5sTxt.Text);
            Balance5 = Convert.ToInt32(CigarBalance5sTxt.Text);
            Balance5 = (Balance5 + Add5) - Substract5;
            // CigarBalance5sTxt.Text = Balance5.ToString();
            TotalPacks = (Balance10 * 10 + Balance8 * 8 + Balance5 * 5);
            CigarTotalPacksTxt.Text = TotalPacks.ToString();
        }
        private void BindCigar10sSiteDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                CigarSite10sCmb.DataSource = storeMaster;
                CigarSite10sCmb.DisplayMember = "StoreName";
                CigarSite10sCmb.ValueMember = "StoreId";
                CigarSite10sLabel.Text = "Site";
            }
        }
        private void BindCigar8sSiteDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                CigarSite8sCmb.DataSource = storeMaster;
                CigarSite8sCmb.DisplayMember = "StoreName";
                CigarSite8sCmb.ValueMember = "StoreId";
                CigarSite8sLabel.Text = "Site";
            }
        }
        private void BindCigar5sSiteDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                CigarSite5sCmb.DataSource = storeMaster;
                CigarSite5sCmb.DisplayMember = "StoreName";
                CigarSite5sCmb.ValueMember = "StoreId";
                CigarSite5sLabel.Text = "Site";
            }
        }
        public void BindCigar10sVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            CigarSite10sCmb.DataSource = vendorList;
            CigarSite10sCmb.DisplayMember = "VendorName";
            CigarSite10sCmb.ValueMember = "VendorId";
            CigarSite10sLabel.Text = "Vendor";
        }
        public void BindCigar8sVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            CigarSite8sCmb.DataSource = vendorList;
            CigarSite8sCmb.DisplayMember = "VendorName";
            CigarSite8sCmb.ValueMember = "VendorId";
            CigarSite8sLabel.Text = "Vendor";
        }
        public void BindCigar5sVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            CigarSite5sCmb.DataSource = vendorList;
            CigarSite5sCmb.DisplayMember = "VendorName";
            CigarSite5sCmb.ValueMember = "VendorId";
            CigarSite5sLabel.Text = "Vendor";
        }
        private void HideShowCigar10sReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                Cigar10sReasonCombo.Enabled = true;
            }
            else
            {
                Cigar10sReasonCombo.Enabled = false;
            }
        }
        private void HideShowCigar8sReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                Cigar8sReasonCombo.Enabled = true;
            }
            else
            {
                Cigar8sReasonCombo.Enabled = false;
            }
        }
        private void HideShowCigar5sReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                Cigar5sReasonCombo.Enabled = true;
            }
            else
            {
                Cigar5sReasonCombo.Enabled = false;
            }
        }
        #endregion
        #region E-Cigar
        private void LoadPreviosDayECigarOpening()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                selectedDate = selectedDate.AddDays(-1);
                var previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.ECigar).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    ECigarBalanceTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    ECigarBalanceTxt.Text = "0";
                }

            }
        }
        private void LoadECigarInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var saveRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.ECigar).FirstOrDefault();
                if (saveRecord != null)
                {
                    ECigarBalanceTxt.Text = saveRecord.Balance.ToString();
                    ECigarAddTxt.Text = saveRecord.Add.ToString();
                    ECigarSubstractTxt.Text = saveRecord.Substract.ToString();
                    ECigarRemarks.Text = saveRecord.Remarks;
                    ECigarReasonCmb.SelectedValue = Convert.ToInt32(saveRecord.SubstractionReason);
                    if (saveRecord.SubstractionReason == "1" && saveRecord.VendorId != null)
                    {
                        ECigarReasonCmb.Enabled = true;
                        BindECigarVendors();
                        ECigarSiteCmb.SelectedValue = saveRecord.VendorId;
                        ECigarSiteCmb.Enabled = true;
                        ECigarSiteLabel.Text = "Vendor";

                    }
                    else if (saveRecord.SubstractionReason == "2")
                    {
                        ECigarReasonCmb.Enabled = true;
                        BindECigarSiteDetails();
                        ECigarSiteCmb.SelectedValue = saveRecord.StoreId;
                        ECigarSiteCmb.Enabled = true;
                        ECigarSiteLabel.Text = "Site";

                    }
                }
                LoadECigarSavedDocuments();
            }
        }
        private void SaveInventoryForECigarSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.ECigar).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(ECigarBalanceTxt.Text);
                    newRecord.Add = Convert.ToInt32(ECigarAddTxt.Text);
                    newRecord.Substract = Convert.ToInt32(ECigarSubstractTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.ECigar;
                    newRecord.Remarks = ECigarRemarks.Text;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (ECigarReasonCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = ECigarReasonCmb.SelectedValue.ToString();
                        if (ECigarReasonCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(ECigarSiteCmb.SelectedValue);
                        }
                        else if (ECigarReasonCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(ECigarSiteCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(ECigarBalanceTxt.Text);
                    newRecord.Add = Convert.ToInt32(ECigarAddTxt.Text);
                    newRecord.Substract = Convert.ToInt32(ECigarSubstractTxt.Text);
                    newRecord.Remarks = ECigarRemarks.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (ECigarReasonCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = ECigarReasonCmb.SelectedValue.ToString();
                        if (ECigarReasonCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(ECigarSiteCmb.SelectedValue);
                        }
                        else if (ECigarReasonCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(ECigarSiteCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.ECigar, ChewDgv);
            }
        }
        private void LoadECigarSavedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.ECigar, ECigarDgv);
        }
        private void ECigarAutoFormula()
        {
            LoadPreviosDayECigarOpening();
            int Add = 0, Substract = 0, Balance = 0;
            Add = Convert.ToInt32(ECigarAddTxt.Text);
            Substract = Convert.ToInt32(ECigarSubstractTxt.Text);
            Balance = Convert.ToInt32(ECigarBalanceTxt.Text);
            // Balance = (Balance + Add) - Substract;
            ECigarBalanceTxt.Text = Balance.ToString();
        }
        private void BindECigarSiteDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                ECigarSiteCmb.DataSource = storeMaster;
                ECigarSiteCmb.DisplayMember = "StoreName";
                ECigarSiteCmb.ValueMember = "StoreId";
                ECigarSiteLabel.Text = "Site";
            }
        }
        public void BindECigarVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            ECigarSiteCmb.DataSource = vendorList;
            ECigarSiteCmb.DisplayMember = "VendorName";
            ECigarSiteCmb.ValueMember = "VendorId";
            ECigarSiteLabel.Text = "Vendor";
        }
        private void HideShowECigarReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                ECigarReasonCmb.Enabled = true;
            }
            else
            {
                ECigarReasonCmb.Enabled = false;
            }
        }
        #endregion
        #region Singles And Papers
        private void LoadPreviosDaySinglesPapersOpening()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                selectedDate = selectedDate.AddDays(-1);
                var previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.SinglesAndPapers).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    SinglesBalanceTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    SinglesBalanceTxt.Text = "0";
                }

            }
        }
        private void LoadSinglesPapersInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var saveRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.SinglesAndPapers).FirstOrDefault();
                if (saveRecord != null)
                {
                    SinglesBalanceTxt.Text = saveRecord.Balance.ToString();
                    SingleAddTxt.Text = saveRecord.Add.ToString();
                    SingleSubstractTxt.Text = saveRecord.Substract.ToString();
                    SingleRemarksTxt.Text = saveRecord.Remarks;
                    SinglesReasonCmb.SelectedValue = Convert.ToInt32(saveRecord.SubstractionReason);
                    if (saveRecord.SubstractionReason == "1" && saveRecord.VendorId != null)
                    {
                        SinglesReasonCmb.Enabled = true;
                        BindSinglesVendors();
                        SinglesSiteCmb.SelectedValue = saveRecord.VendorId;
                        SinglesSiteCmb.Enabled = true;
                        SinglesSiteLabel.Text = "Vendor";

                    }
                    else if (saveRecord.SubstractionReason == "2")
                    {
                        SinglesReasonCmb.Enabled = true;
                        BindSinglesSiteDetails();
                        SinglesSiteCmb.SelectedValue = saveRecord.StoreId;
                        SinglesSiteCmb.Enabled = true;
                        SinglesSiteLabel.Text = "Site";

                    }
                }
                LoadSinglesAndPapersSavedDocuments();
            }
        }
        private void SaveInventoryForSinglesPapersSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.SinglesAndPapers).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(SinglesBalanceTxt.Text);
                    newRecord.Add = Convert.ToInt32(SingleAddTxt.Text);
                    newRecord.Substract = Convert.ToInt32(SingleSubstractTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.SinglesAndPapers;
                    newRecord.Remarks = SingleRemarksTxt.Text;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (SinglesReasonCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = SinglesReasonCmb.SelectedValue.ToString();
                        if (SinglesReasonCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(SinglesSiteCmb.SelectedValue);
                        }
                        else if (SinglesReasonCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(SinglesSiteCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(SinglesBalanceTxt.Text);
                    newRecord.Add = Convert.ToInt32(SingleAddTxt.Text);
                    newRecord.Substract = Convert.ToInt32(SingleSubstractTxt.Text);
                    newRecord.Remarks = SingleRemarksTxt.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (SinglesReasonCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = SinglesReasonCmb.SelectedValue.ToString();
                        if (SinglesReasonCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(SinglesSiteCmb.SelectedValue);
                        }
                        else if (SinglesReasonCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(SinglesSiteCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.SinglesAndPapers, SinglesDgv);
            }
        }
        private void LoadSinglesAndPapersSavedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.SinglesAndPapers, SinglesDgv);
        }
        private void SinglesAndPapersAutoFormula()
        {
            LoadPreviosDaySinglesPapersOpening();
            int Add = 0, Substract = 0, Balance = 0;
            Add = Convert.ToInt32(SingleAddTxt.Text);
            Substract = Convert.ToInt32(SingleSubstractTxt.Text);
            Balance = Convert.ToInt32(SinglesBalanceTxt.Text);
            //  Balance = (Balance + Add) - Substract;
            SinglesBalanceTxt.Text = Balance.ToString();
        }
        private void BindSinglesSiteDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                SinglesSiteCmb.DataSource = storeMaster;
                SinglesSiteCmb.DisplayMember = "StoreName";
                SinglesSiteCmb.ValueMember = "StoreId";
                SinglesSiteLabel.Text = "Site";
            }
        }
        public void BindSinglesVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            SinglesSiteCmb.DataSource = vendorList;
            SinglesSiteCmb.DisplayMember = "VendorName";
            SinglesSiteCmb.ValueMember = "VendorId";
            SinglesSiteLabel.Text = "Vendor";
        }
        private void HideShowSinglesReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                SinglesReasonCmb.Enabled = true;
            }
            else
            {
                SinglesReasonCmb.Enabled = false;
            }
        }
        #endregion
        #region Milk
        private void LoadPreviosDayMilkOpening()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                selectedDate = selectedDate.AddDays(-1);
                var previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk310ml).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    MilkBalance310mlTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    MilkBalance310mlTxt.Text = "0";
                }
                previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk473ml).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    MilkBalance473mlTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    MilkBalance473mlTxt.Text = "0";
                }
                previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk1L).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    MilkBalance1LTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    MilkBalance1LTxt.Text = "0";
                }
                previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk2L).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    MilkBalance2LTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    MilkBalance2LTxt.Text = "0";
                }
                previosDayOpening = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk4L).FirstOrDefault();
                if (previosDayOpening != null)
                {
                    MilkBalance4LTxt.Text = previosDayOpening.Balance.ToString();
                }
                else
                {
                    MilkBalance4LTxt.Text = "0";
                }
            }
        }
        private void LoadMilk310mlInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var savedRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk310ml).FirstOrDefault();
                if (savedRecord != null)
                {
                    MilkBalance310mlTxt.Text = savedRecord.Balance.ToString();
                    MilkAdd310mlTxt.Text = savedRecord.Add.ToString();
                    MilkSubtract310mlTxt.Text = savedRecord.Substract.ToString();
                    MilkRemarksTxt.Text = savedRecord.Remarks;
                    if (!string.IsNullOrEmpty(savedRecord.SubstractionReason))
                    {
                        MilkReason310mlCmb.SelectedValue = Convert.ToInt32(savedRecord.SubstractionReason);
                        if (savedRecord.SubstractionReason == "1" && savedRecord.VendorId != null)
                        {
                            MilkReason310mlCmb.Enabled = true;
                            BindMilk310Vendors();
                            MilkSite310mlCmb.SelectedValue = savedRecord.VendorId;
                            MilkSite310mlCmb.Enabled = true;
                            MilkSite310Label.Text = "Vendor";

                        }
                        else if (savedRecord.SubstractionReason == "2")
                        {
                            MilkReason310mlCmb.Enabled = true;
                            BindMilkSite310Details();
                            MilkReason310mlCmb.SelectedValue = savedRecord.StoreId;
                            MilkSite310mlCmb.Enabled = true;
                            MilkSite310Label.Text = "Site";

                        }
                    }

                }
                LoadMilkSavedDocuments();
            }
        }
        private void LoadMilk473mlInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var savedRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk473ml).FirstOrDefault();
                if (savedRecord != null)
                {
                    MilkBalance473mlTxt.Text = savedRecord.Balance.ToString();
                    MilkAdd473mlTxt.Text = savedRecord.Add.ToString();
                    MilkSubtract473mlTxt.Text = savedRecord.Substract.ToString();
                    MilkRemarksTxt.Text = savedRecord.Remarks;
                    if (!string.IsNullOrEmpty(savedRecord.SubstractionReason))
                    {
                        MilkReason473mlCmb.SelectedValue = Convert.ToInt32(savedRecord.SubstractionReason);
                        if (savedRecord.SubstractionReason == "1" && savedRecord.VendorId != null)
                        {
                            MilkReason473mlCmb.Enabled = true;
                            BindMilk473mlVendors();
                            MilkSite473mlCmb.SelectedValue = savedRecord.VendorId;
                            MilkSite473mlCmb.Enabled = true;
                            MilkSite473Label.Text = "Vendor";

                        }
                        else if (savedRecord.SubstractionReason == "2")
                        {
                            MilkReason473mlCmb.Enabled = true;
                            BindMilkSite473mlDetails();
                            MilkSite473mlCmb.SelectedValue = savedRecord.StoreId;
                            MilkSite473mlCmb.Enabled = true;
                            MilkSite473Label.Text = "Site";

                        }
                    }

                }
            }
        }
        private void LoadMilk1LInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var savedRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk1L).FirstOrDefault();
                if (savedRecord != null)
                {
                    MilkBalance1LTxt.Text = savedRecord.Balance.ToString();
                    MilkAdd1LTxt.Text = savedRecord.Add.ToString();
                    MilkSubtract1LTxt.Text = savedRecord.Substract.ToString();
                    MilkRemarksTxt.Text = savedRecord.Remarks;
                    if (!string.IsNullOrEmpty(savedRecord.SubstractionReason))
                    {
                        MilkReason1LCmb.SelectedValue = Convert.ToInt32(savedRecord.SubstractionReason);
                        if (savedRecord.SubstractionReason == "1" && savedRecord.VendorId != null)
                        {
                            MilkReason1LCmb.Enabled = true;
                            BindMilk1LVendors();
                            MilkSite1LCmb.SelectedValue = savedRecord.VendorId;
                            MilkSite1LCmb.Enabled = true;
                            MilkSite1LLabel.Text = "Vendor";

                        }
                        else if (savedRecord.SubstractionReason == "2")
                        {
                            MilkReason1LCmb.Enabled = true;
                            BindMilkSite1LDetails();
                            MilkSite1LCmb.SelectedValue = savedRecord.StoreId;
                            MilkSite1LCmb.Enabled = true;
                            MilkSite1LLabel.Text = "Site";

                        }
                    }

                }
            }
        }
        private void LoadMilk2LInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var savedRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk2L).FirstOrDefault();
                if (savedRecord != null)
                {
                    MilkBalance2LTxt.Text = savedRecord.Balance.ToString();
                    MilkAdd2LTxt.Text = savedRecord.Add.ToString();
                    MilkSubtract2LTxt.Text = savedRecord.Substract.ToString();
                    MilkRemarksTxt.Text = savedRecord.Remarks;
                    if (!string.IsNullOrEmpty(savedRecord.SubstractionReason))
                    {
                        MilkReason2LCmb.SelectedValue = Convert.ToInt32(savedRecord.SubstractionReason);
                        if (savedRecord.SubstractionReason == "1" && savedRecord.VendorId != null)
                        {
                            MilkReason2LCmb.Enabled = true;
                            BindMilk2LVendors();
                            MilkSite2LCmb.SelectedValue = savedRecord.VendorId;
                            MilkSite2LCmb.Enabled = true;
                            MilkSite2LLabel.Text = "Vendor";

                        }
                        else if (savedRecord.SubstractionReason == "2")
                        {
                            MilkReason2LCmb.Enabled = true;
                            BindMilkSite2LDetails();
                            MilkSite2LCmb.SelectedValue = savedRecord.StoreId;
                            MilkSite2LCmb.Enabled = true;
                            MilkSite2LLabel.Text = "Site";

                        }
                    }

                }
            }
        }
        private void LoadMilk4LInventoryForSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var savedRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk4L).FirstOrDefault();
                if (savedRecord != null)
                {
                    MilkBalance4LTxt.Text = savedRecord.Balance.ToString();
                    MilkAdd4LTxt.Text = savedRecord.Add.ToString();
                    MilkSubtract4LTxt.Text = savedRecord.Substract.ToString();
                    MilkRemarksTxt.Text = savedRecord.Remarks;
                    if (!string.IsNullOrEmpty(savedRecord.SubstractionReason))
                    {
                        MilkReason4LCmb.SelectedValue = Convert.ToInt32(savedRecord.SubstractionReason);
                        if (savedRecord.SubstractionReason == "1" && savedRecord.VendorId != null)
                        {
                            MilkReason4LCmb.Enabled = true;
                            BindMilk4LVendors();
                            MilkSite4LCmb.SelectedValue = savedRecord.VendorId;
                            MilkSite4LCmb.Enabled = true;
                            MilkSite4LLabel.Text = "Vendor";

                        }
                        else if (savedRecord.SubstractionReason == "2")
                        {
                            MilkReason4LCmb.Enabled = true;
                            BindMilkSite4LDetails();
                            MilkSite4LCmb.SelectedValue = savedRecord.StoreId;
                            MilkSite4LCmb.Enabled = true;
                            MilkSite4LLabel.Text = "Site";

                        }
                    }

                }
                else
                {

                }
            }
        }
        private void SaveInventoryForMilk310mlSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk310ml).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(MilkBalance310mlTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd310mlTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract310mlTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk310ml;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (MilkReason310mlCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason310mlCmb.SelectedValue.ToString();
                        if (MilkReason310mlCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite310mlCmb.SelectedValue);
                        }
                        else if (MilkReason310mlCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite310mlCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(MilkBalance310mlTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd310mlTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract310mlTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk310ml;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (MilkReason310mlCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason310mlCmb.SelectedValue.ToString();
                        if (MilkReason310mlCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite310mlCmb.SelectedValue);
                        }
                        else if (MilkReason310mlCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite310mlCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.MilkDoc, MilkDgv);
            }
        }
        private void SaveInventoryForMilk473mlSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk473ml).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(MilkBalance473mlTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd473mlTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract473mlTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk473ml;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (MilkReason473mlCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason473mlCmb.SelectedValue.ToString();
                        if (MilkReason473mlCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite473mlCmb.SelectedValue);
                        }
                        else if (MilkReason473mlCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite473mlCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(MilkBalance473mlTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd473mlTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract473mlTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk473ml;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (MilkReason473mlCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason473mlCmb.SelectedValue.ToString();
                        if (MilkReason473mlCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite473mlCmb.SelectedValue);
                        }
                        else if (MilkReason473mlCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite473mlCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
            }
        }
        private void SaveInventoryForMilk1LSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk1L).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(MilkBalance1LTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd1LTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract1LTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk1L;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (MilkReason1LCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason1LCmb.SelectedValue.ToString();
                        if (MilkReason1LCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite1LCmb.SelectedValue);
                        }
                        else if (MilkReason1LCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite1LCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(MilkBalance1LTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd1LTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract1LTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk1L;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (MilkReason1LCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason1LCmb.SelectedValue.ToString();
                        if (MilkReason1LCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite1LCmb.SelectedValue);
                        }
                        else if (MilkReason1LCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite1LCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
            }
        }
        private void SaveInventoryForMilk2LSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk2L).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(MilkBalance2LTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd2LTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract2LTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk2L;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (MilkReason2LCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason2LCmb.SelectedValue.ToString();
                        if (MilkReason2LCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite2LCmb.SelectedValue);
                        }
                        else if (MilkReason2LCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite2LCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(MilkBalance2LTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd2LTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract2LTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk2L;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (MilkReason2LCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason2LCmb.SelectedValue.ToString();
                        if (MilkReason2LCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite2LCmb.SelectedValue);
                        }
                        else if (MilkReason2LCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite2LCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
            }
        }
        private void SaveInventoryForMilk4LSelectedDate()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime selectedDate = Convert.ToDateTime(DateCalender.Text);
                var newRecord = context.Tbl_TaboccoInventory.Where(s => s.TransactionDate == selectedDate && s.ProductCategoryName == DocumentTypeConstants.Milk4L).FirstOrDefault();
                if (newRecord == null)
                {
                    newRecord = new Tbl_TaboccoInventory();
                    newRecord.Balance = Convert.ToInt32(MilkBalance4LTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd4LTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract4LTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk4L;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.CreatedBy = Utility.BA_Commman._userId; ;
                    newRecord.CreatedDate = DateTime.Now;
                    if (MilkReason4LCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason4LCmb.SelectedValue.ToString();
                        if (MilkReason4LCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite4LCmb.SelectedValue);
                        }
                        else if (MilkReason4LCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite4LCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                    context.Tbl_TaboccoInventory.Add(newRecord);
                }
                else
                {
                    newRecord.Balance = Convert.ToInt32(MilkBalance4LTxt.Text);
                    newRecord.Add = Convert.ToInt32(MilkAdd4LTxt.Text);
                    newRecord.Substract = Convert.ToInt32(MilkSubtract4LTxt.Text);
                    newRecord.TransactionDate = selectedDate;
                    newRecord.ProductCategoryName = DocumentTypeConstants.Milk4L;
                    newRecord.Remarks = MilkRemarksTxt.Text;
                    newRecord.ModifiedBy = Utility.BA_Commman._userId; ;
                    newRecord.ModifiedDate = DateTime.Now;
                    if (MilkReason4LCmb.SelectedIndex > 0)
                    {
                        newRecord.SubstractionReason = MilkReason4LCmb.SelectedValue.ToString();
                        if (MilkReason4LCmb.SelectedIndex == 1)
                        {
                            newRecord.VendorId = Convert.ToInt32(MilkSite4LCmb.SelectedValue);
                        }
                        else if (MilkReason4LCmb.SelectedIndex == 2)
                        {
                            newRecord.StoreId = Convert.ToInt32(MilkSite4LCmb.SelectedValue);
                        }
                        else if (!newRecord.VendorId.HasValue && !newRecord.StoreId.HasValue)
                        {
                            MessageBox.Show("Please select proper reason");
                            return;
                        }
                    }
                }
                context.SaveChanges();
            }
        }
        private void LoadMilkSavedDocuments()
        {
            LoadSavedDocuments(DocumentTypeConstants.MilkDoc, MilkDgv);
        }
        private void Milk310mlAutoFormula()
        {
            LoadPreviosDayMilkOpening();
            int Add = 0, Substract = 0, Balance = 0;
            Add = Convert.ToInt32(MilkAdd310mlTxt.Text);
            Substract = Convert.ToInt32(MilkSubtract310mlTxt.Text);
            Balance = Convert.ToInt32(MilkBalance310mlTxt.Text);
            Balance = (Balance + Add) - Substract;
            // MilkBalance310mlTxt.Text = Balance.ToString();
            HideShow310mlMilkReason(Convert.ToInt32(MilkSubtract310mlTxt.Text));
        }
        private void Milk473mlAutoFormula()
        {
            LoadPreviosDayMilkOpening();
            int Add = 0, Substract = 0, Balance = 0;
            Add = Convert.ToInt32(MilkAdd473mlTxt.Text);
            Substract = Convert.ToInt32(MilkSubtract473mlTxt.Text);
            Balance = Convert.ToInt32(MilkBalance473mlTxt.Text);
            Balance = (Balance + Add) - Substract;
            // MilkBalance473mlTxt.Text = Balance.ToString();
            HideShow473mlMilkReason(Convert.ToInt32(MilkSubtract473mlTxt.Text));
        }
        private void Milk1LAutoFormula()
        {
            LoadPreviosDayMilkOpening();
            int Add = 0, Substract = 0, Balance = 0;
            Add = Convert.ToInt32(MilkAdd1LTxt.Text);
            Substract = Convert.ToInt32(MilkSubtract1LTxt.Text);
            Balance = Convert.ToInt32(MilkBalance1LTxt.Text);
            Balance = (Balance + Add) - Substract;
            //MilkBalance1LTxt.Text = Balance.ToString();
            HideShow1LMilkReason(Convert.ToInt32(MilkSubtract1LTxt.Text));
        }
        private void Milk2LAutoFormula()
        {
            LoadPreviosDayMilkOpening();
            int Add = 0, Substract = 0, Balance = 0;
            Add = Convert.ToInt32(MilkAdd2LTxt.Text);
            Substract = Convert.ToInt32(MilkSubtract2LTxt.Text);
            Balance = Convert.ToInt32(MilkBalance2LTxt.Text);
            Balance = (Balance + Add) - Substract;
            // MilkBalance2LTxt.Text = Balance.ToString();
            HideShow2LMilkReason(Convert.ToInt32(MilkSubtract2LTxt.Text));
        }
        private void Milk4LAutoFormula()
        {
            LoadPreviosDayMilkOpening();
            int Add = 0, Substract = 0, Balance = 0;
            Add = Convert.ToInt32(MilkAdd4LTxt.Text);
            Substract = Convert.ToInt32(MilkSubtract4LTxt.Text);
            Balance = Convert.ToInt32(MilkBalance4LTxt.Text);
            Balance = (Balance + Add) - Substract;
            // MilkBalance4LTxt.Text = Balance.ToString();
            HideShow4LMilkReason(Convert.ToInt32(MilkSubtract4LTxt.Text));
        }
        private void BindMilk310Vendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            MilkSite310mlCmb.DataSource = vendorList;
            MilkSite310mlCmb.DisplayMember = "VendorName";
            MilkSite310mlCmb.ValueMember = "VendorId";
            MilkSite310Label.Text = "Vendor";
        }
        private void BindMilk473mlVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            MilkSite473mlCmb.DataSource = vendorList;
            MilkSite473mlCmb.DisplayMember = "VendorName";
            MilkSite473mlCmb.ValueMember = "VendorId";
            MilkSite473Label.Text = "Vendor";
        }
        private void BindMilk1LVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            MilkSite1LCmb.DataSource = vendorList;
            MilkSite1LCmb.DisplayMember = "VendorName";
            MilkSite1LCmb.ValueMember = "VendorId";
            MilkSite1LLabel.Text = "Vendor";
        }
        private void BindMilk2LVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            MilkSite2LCmb.DataSource = vendorList;
            MilkSite2LCmb.DisplayMember = "VendorName";
            MilkSite2LCmb.ValueMember = "VendorId";
            MilkSite2LLabel.Text = "Vendor";
        }
        private void BindMilk4LVendors()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            var vendorList = _objVendor.GetVendorlist();
            vendorList.Insert(0, new VenderEntity { VendorId = 0, VendorName = "--Select--" });
            MilkSite4LCmb.DataSource = vendorList;
            MilkSite4LCmb.DisplayMember = "VendorName";
            MilkSite4LCmb.ValueMember = "VendorId";
            MilkSite4LLabel.Text = "Vendor";
        }
        private void BindMilkSite310Details()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                MilkSite310mlCmb.DataSource = storeMaster;
                MilkSite310mlCmb.DisplayMember = "StoreName";
                MilkSite310mlCmb.ValueMember = "StoreId";
                MilkSite310Label.Text = "Site";
            }
        }
        private void BindMilkSite473mlDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                MilkSite473mlCmb.DataSource = storeMaster;
                MilkSite473mlCmb.DisplayMember = "StoreName";
                MilkSite473mlCmb.ValueMember = "StoreId";
                MilkSite473Label.Text = "Site";
            }
        }
        private void BindMilkSite1LDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                MilkSite1LCmb.DataSource = storeMaster;
                MilkSite1LCmb.DisplayMember = "StoreName";
                MilkSite1LCmb.ValueMember = "StoreId";
                MilkSite1LLabel.Text = "Site";
            }
        }
        private void BindMilkSite2LDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                MilkSite2LCmb.DataSource = storeMaster;
                MilkSite2LCmb.DisplayMember = "StoreName";
                MilkSite2LCmb.ValueMember = "StoreId";
                MilkSite2LLabel.Text = "Site";
            }
        }
        private void BindMilkSite4LDetails()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var storeMaster = context.Mst_StoreMaster.Where(s => s.IsActive == true).Select(x => new { x.StoreId, x.StoreName }).ToList();
                storeMaster.Insert(0, new { StoreId = 0, StoreName = "--Select--" });
                MilkSite4LCmb.DataSource = storeMaster;
                MilkSite4LCmb.DisplayMember = "StoreName";
                MilkSite4LCmb.ValueMember = "StoreId";
                MilkSite4LLabel.Text = "Site";
            }
        }
        private void HideShow310mlMilkReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                MilkReason310mlCmb.Enabled = true;
            }
            else
            {
                MilkReason310mlCmb.Enabled = false;
            }
        }
        private void HideShow473mlMilkReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                MilkReason473mlCmb.Enabled = true;
            }
            else
            {
                MilkReason473mlCmb.Enabled = false;
            }
        }
        private void HideShow1LMilkReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                MilkReason1LCmb.Enabled = true;
            }
            else
            {
                MilkReason1LCmb.Enabled = false;
            }
        }
        private void HideShow2LMilkReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                MilkReason2LCmb.Enabled = true;
            }
            else
            {
                MilkReason2LCmb.Enabled = false;
            }
        }
        private void HideShow4LMilkReason(int substractionValue)
        {
            if (substractionValue != 0)
            {
                MilkReason4LCmb.Enabled = true;
            }
            else
            {
                MilkReason4LCmb.Enabled = false;
            }
        }
        private void MoveMilkInventoryToFrontOffice()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var addition = context.TBL_MilkAdditionNew.FirstOrDefault(s => s.TransactionDate == date && s.AdditionType == "Milk Addition");
                if (addition != null)
                {
                    addition.AdditionType = "Milk Addition";
                    addition.C310ml = Convert.ToInt32(MilkSubtract310mlTxt.Text);
                    addition.C473ml = Convert.ToInt32(MilkSubtract473mlTxt.Text);
                    addition.C1L = Convert.ToInt32(MilkSubtract1LTxt.Text);
                    addition.C2L = Convert.ToInt32(MilkSubtract2LTxt.Text);
                    addition.C4L = Convert.ToInt32(MilkSubtract4LTxt.Text);
                    addition.Remarks = "Moved from Back office";
                    addition.IsActive = true;
                    addition.ModifiedBy = Utility.BA_Commman._userId;
                    addition.ModifiedDate = DateTime.Now;
                }
                else
                {
                    addition = new TBL_MilkAdditionNew();
                    addition.TransactionDate = date;
                    addition.AdditionType = "Milk Addition";
                    addition.C310ml = Convert.ToInt32(MilkSubtract310mlTxt.Text);
                    addition.C473ml = Convert.ToInt32(MilkSubtract473mlTxt.Text);
                    addition.C1L = Convert.ToInt32(MilkSubtract1LTxt.Text);
                    addition.C2L = Convert.ToInt32(MilkSubtract2LTxt.Text);
                    addition.C4L = Convert.ToInt32(MilkSubtract4LTxt.Text);
                    addition.Remarks = "Moved from Back office";
                    addition.IsActive = true;
                    addition.CreatedBy = Utility.BA_Commman._userId;
                    addition.CreatedDate = DateTime.Now;
                    context.TBL_MilkAdditionNew.Add(addition);
                }
                context.SaveChanges();
            }
        }
        #endregion
        #endregion
        #region Form Events
        private void label13_Click(object sender, EventArgs e)
        {

        }
        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                IntialDocumentsLoad();
                InitializeReasonList();
                #region Tobacco

                LoadTobaccoInventoryForSelectedDate();
                TobaccoAutoFormula();
                #endregion
                #region Chew

                LoadChewInventoryForSelectedDate();
                LoadPreviosDayChewOpening();
                ChewAutoFormula();
                #endregion
                #region Cigar
                LoadPreviosDayCigarOpening();
                LoadCigarInventoryForSelectedDate();
                CigarAutoFormula();

                #endregion
                #region E-Cigar

                LoadPreviosDayECigarOpening();
                LoadECigarInventoryForSelectedDate();
                ECigarAutoFormula();

                #endregion
                #region Singles And Papers
                LoadPreviosDaySinglesPapersOpening();
                LoadSinglesPapersInventoryForSelectedDate();
                SinglesAndPapersAutoFormula();
                #endregion
                #region Milk
                //LoadPreviosDayMilkOpening();
                //LoadMilk310mlInventoryForSelectedDate();
                //LoadMilk473mlInventoryForSelectedDate();
                //LoadMilk1LInventoryForSelectedDate();
                //LoadMilk2LInventoryForSelectedDate();
                //LoadMilk4LInventoryForSelectedDate();
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                #region Tobacco
                if (ValidateTobaccoInventoryFor8SFields())
                {
                    SaveTobaccoInventoryFor8SSelectedDate();
                }
                else { return; }

                SaveTobaccoInventoryFor10SSelectedDate();

                #endregion
                #region Chew
                SaveInventoryForChewSSelectedDate();
                #endregion
                #region E Cigar
                SaveInventoryForECigarSelectedDate();
                #endregion
                #region Cigar
                SaveInventoryForCigar10SSelectedDate();
                SaveInventoryForCigar8SSelectedDate();
                SaveInventoryForCigar5SSelectedDate();

                #endregion
                #region Singles And Papers
                SaveInventoryForSinglesPapersSelectedDate();
                #endregion
                #region Front Office Move
                if ((Convert.ToInt32(TobaccoSubstract8STxt.Text) != 0 && Tobacco10SReasonCmb.SelectedIndex == 1) || (Convert.ToInt32(TobaccoSubstract10STxt.Text) != 0 && Tobacco10SReasonCmb.SelectedIndex == 1) || (Convert.ToInt32(CigarSubstract10sTxt.Text) != 0 && Cigar10sReasonCombo.SelectedIndex == 1) || (Convert.ToInt32(CigarSubstract8sTxt.Text) != 0 && Cigar8sReasonCombo.SelectedIndex == 1) || (Convert.ToInt32(CigarSubstract5sTxt.Text) != 0 && Cigar5sReasonCombo.SelectedIndex == 1) || (Convert.ToInt32(ChewSubstractTxt.Text) != 0 && ChewReasonCombo.SelectedIndex == 1) || (Convert.ToInt32(ECigarSubstractTxt.Text) != 0 && ECigarReasonCmb.SelectedIndex == 1))
                {
                    MoveInventoryToFrontOffice();
                }

                #endregion

                #region Milk
                //SaveInventoryForMilk310mlSelectedDate();
                //SaveInventoryForMilk473mlSelectedDate();
                //SaveInventoryForMilk1LSelectedDate();
                //SaveInventoryForMilk2LSelectedDate();
                //SaveInventoryForMilk4LSelectedDate();
                //if ((Convert.ToInt32(MilkSubtract310mlTxt.Text) != 0 && MilkReason310mlCmb.SelectedIndex == 1) || (Convert.ToInt32(MilkSubtract473mlTxt.Text) != 0 && MilkReason473mlCmb.SelectedIndex == 1) || (Convert.ToInt32(MilkSubtract1LTxt.Text) != 0 && MilkReason1LCmb.SelectedIndex == 1) || (Convert.ToInt32(MilkSubtract2LTxt.Text) != 0 && MilkReason2LCmb.SelectedIndex == 1) || (Convert.ToInt32(MilkSubtract4LTxt.Text) != 0 && MilkReason4LCmb.SelectedIndex == 1))
                //{
                //    MoveMilkInventoryToFrontOffice();
                //}
                #endregion
                MessageBox.Show("Inventory details saved successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void TaboccoMasterInventoryForm_Load(object sender, EventArgs e)
        {
            try
            {
                IntialDocumentsLoad();
                InitializeReasonList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void TobaccoAdd8STxt_Leave(object sender, EventArgs e)
        {
            try
            {
                TobaccoAutoFormula();
                HideShowReason8s(Convert.ToInt32(TobaccoSubstract8STxt.Text));
                HideShowReason10s(Convert.ToInt32(TobaccoSubstract10STxt.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ChewAddTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                ChewAutoFormula();
                HideShowReasonChew(Convert.ToInt32(ChewSubstractTxt.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void CigarAdd10sTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                CigarAutoFormula();
                HideShowCigar10sReason(Convert.ToInt32(CigarSubstract10sTxt.Text));
                HideShowCigar8sReason(Convert.ToInt32(CigarSubstract8sTxt.Text));
                HideShowCigar5sReason(Convert.ToInt32(CigarSubstract5sTxt.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ECigarAddTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                ECigarAutoFormula();
                HideShowECigarReason(Convert.ToInt32(ECigarSubstractTxt.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SingleAddTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                SinglesAndPapersAutoFormula();
                HideShowSinglesReason(Convert.ToInt32(SingleSubstractTxt.Text));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void Tobacco8SReasonCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Tobacco8SReasonCmb.SelectedIndex == 2)
                {
                    BindSite8sDetails();
                    TobaccoSite8s.Enabled = true;
                }
                else if (Tobacco8SReasonCmb.SelectedIndex == 1)
                {
                    BindTobacco8sVendors();
                    TobaccoSite8s.Enabled = true;
                }
                else
                {
                    TobaccoSite8s.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Tobacco10SReasonCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Tobacco10SReasonCmb.SelectedIndex == 2)
                {
                    BindSite10sDetails();
                    TobaccoSite10s.Enabled = true;
                }
                else if (Tobacco10SReasonCmb.SelectedIndex == 1)
                {
                    BindTobacco10sVendors();
                    TobaccoSite10s.Enabled = true;
                }
                else
                {
                    TobaccoSite10s.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ChewReasonCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ChewReasonCombo.SelectedIndex == 2)
                {
                    BindChewSiteDetails();
                    ChewSiteCombobox.Enabled = true;
                }
                else if (ChewReasonCombo.SelectedIndex == 1)
                {
                    BindChewVendors();
                    ChewSiteCombobox.Enabled = true;
                }
                else
                {
                    ChewSiteCombobox.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Cigar10sReasonCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Cigar10sReasonCombo.SelectedIndex == 2)
                {
                    BindCigar10sSiteDetails();
                    CigarSite10sCmb.Enabled = true;
                }
                else if (Cigar10sReasonCombo.SelectedIndex == 1)
                {
                    BindCigar10sVendors();
                    CigarSite10sCmb.Enabled = true;
                }
                else
                {
                    CigarSite10sCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Cigar8sReasonCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Cigar8sReasonCombo.SelectedIndex == 2)
                {
                    BindCigar8sSiteDetails();
                    CigarSite8sCmb.Enabled = true;
                }
                else if (Cigar8sReasonCombo.SelectedIndex == 1)
                {
                    BindCigar8sVendors();
                    CigarSite8sCmb.Enabled = true;
                }
                else
                {
                    CigarSite8sCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Cigar5sReasonCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Cigar5sReasonCombo.SelectedIndex == 2)
                {
                    BindCigar5sSiteDetails();
                    CigarSite5sCmb.Enabled = true;
                }
                else if (Cigar5sReasonCombo.SelectedIndex == 1)
                {
                    BindCigar5sVendors();
                    CigarSite5sCmb.Enabled = true;
                }
                else
                {
                    CigarSite5sCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ECigarReasonCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ECigarReasonCmb.SelectedIndex == 2)
                {
                    BindECigarSiteDetails();
                    ECigarSiteCmb.Enabled = true;
                }
                else if (ECigarReasonCmb.SelectedIndex == 1)
                {
                    BindECigarVendors();
                    ECigarSiteCmb.Enabled = true;
                }
                else
                {
                    ECigarSiteCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SinglesReasonCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (SinglesReasonCmb.SelectedIndex == 2)
                {
                    BindSinglesSiteDetails();
                    SinglesSiteCmb.Enabled = true;
                }
                else if (SinglesReasonCmb.SelectedIndex == 1)
                {
                    BindSinglesVendors();
                    SinglesSiteCmb.Enabled = true;
                }
                else
                {
                    SinglesSiteCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void label45_Click(object sender, EventArgs e)
        {

        }

        private void MilkAdd310mlTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk310mlAutoFormula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MilkAdd473mlTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk473mlAutoFormula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MilkAdd1LTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk1LAutoFormula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void MilkAdd2LTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk2LAutoFormula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MilkAdd4LTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk4LAutoFormula();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MilkReason310mlCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (MilkReason310mlCmb.SelectedIndex == 2)
                {
                    BindMilkSite310Details();
                    MilkSite310mlCmb.Enabled = true;
                }
                else if (MilkReason310mlCmb.SelectedIndex == 1)
                {
                    BindMilk310Vendors();
                    MilkSite310mlCmb.Enabled = true;
                }
                else
                {
                    MilkSite310mlCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (MilkReason473mlCmb.SelectedIndex == 2)
                {
                    BindMilkSite473mlDetails();
                    MilkSite473mlCmb.Enabled = true;
                }
                else if (MilkReason473mlCmb.SelectedIndex == 1)
                {
                    BindMilk473mlVendors();
                    MilkSite473mlCmb.Enabled = true;
                }
                else
                {
                    MilkSite473mlCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MilkReason1LCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (MilkReason1LCmb.SelectedIndex == 2)
                {
                    BindMilkSite1LDetails();
                    MilkSite1LCmb.Enabled = true;
                }
                else if (MilkReason1LCmb.SelectedIndex == 1)
                {
                    BindMilk1LVendors();
                    MilkSite1LCmb.Enabled = true;
                }
                else
                {
                    MilkSite1LCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MilkReason2LCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (MilkReason2LCmb.SelectedIndex == 2)
                {
                    BindMilkSite2LDetails();
                    MilkSite2LCmb.Enabled = true;
                }
                else if (MilkReason2LCmb.SelectedIndex == 1)
                {
                    BindMilk2LVendors();
                    MilkSite2LCmb.Enabled = true;
                }
                else
                {
                    MilkSite2LCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MilkReason4LCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (MilkReason4LCmb.SelectedIndex == 2)
                {
                    BindMilkSite4LDetails();
                    MilkSite4LCmb.Enabled = true;
                }
                else if (MilkReason4LCmb.SelectedIndex == 1)
                {
                    BindMilk4LVendors();
                    MilkSite4LCmb.Enabled = true;
                }
                else
                {
                    MilkSite4LCmb.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
