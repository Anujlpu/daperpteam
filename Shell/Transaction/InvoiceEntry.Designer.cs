﻿namespace Shell
{
    partial class InvoiceEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpManageVendor = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbtDebit = new System.Windows.Forms.RadioButton();
            this.rbtCredit = new System.Windows.Forms.RadioButton();
            this.dgvUploadedDocs = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.ddlVendorName = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInvoiceBal = new System.Windows.Forms.TextBox();
            this.txtcashPayoutAmt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtcashpaidamt = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtGst = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtInvoiceNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ddlPaymnetType = new System.Windows.Forms.ComboBox();
            this.dtpPayment = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grpVendorList = new System.Windows.Forms.GroupBox();
            this.dgvInvoiceList = new System.Windows.Forms.DataGridView();
            this.grpManageVendor.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUploadedDocs)).BeginInit();
            this.grpVendorList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceList)).BeginInit();
            this.SuspendLayout();
            // 
            // grpManageVendor
            // 
            this.grpManageVendor.Controls.Add(this.panel2);
            this.grpManageVendor.Controls.Add(this.dgvUploadedDocs);
            this.grpManageVendor.Controls.Add(this.label16);
            this.grpManageVendor.Controls.Add(this.ddlVendorName);
            this.grpManageVendor.Controls.Add(this.label13);
            this.grpManageVendor.Controls.Add(this.label9);
            this.grpManageVendor.Controls.Add(this.txtInvoiceBal);
            this.grpManageVendor.Controls.Add(this.txtcashPayoutAmt);
            this.grpManageVendor.Controls.Add(this.label11);
            this.grpManageVendor.Controls.Add(this.txtcashpaidamt);
            this.grpManageVendor.Controls.Add(this.label12);
            this.grpManageVendor.Controls.Add(this.txtGst);
            this.grpManageVendor.Controls.Add(this.label7);
            this.grpManageVendor.Controls.Add(this.txtAmount);
            this.grpManageVendor.Controls.Add(this.label6);
            this.grpManageVendor.Controls.Add(this.label62);
            this.grpManageVendor.Controls.Add(this.txtRemarks);
            this.grpManageVendor.Controls.Add(this.txtInvoiceNo);
            this.grpManageVendor.Controls.Add(this.label3);
            this.grpManageVendor.Controls.Add(this.label5);
            this.grpManageVendor.Controls.Add(this.ddlPaymnetType);
            this.grpManageVendor.Controls.Add(this.dtpPayment);
            this.grpManageVendor.Controls.Add(this.label1);
            this.grpManageVendor.Controls.Add(this.btnExit);
            this.grpManageVendor.Controls.Add(this.label10);
            this.grpManageVendor.Controls.Add(this.label8);
            this.grpManageVendor.Controls.Add(this.BtnDelete);
            this.grpManageVendor.Controls.Add(this.BtnSave);
            this.grpManageVendor.Controls.Add(this.label2);
            this.grpManageVendor.Controls.Add(this.btnUpdate);
            this.grpManageVendor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageVendor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageVendor.ForeColor = System.Drawing.Color.Black;
            this.grpManageVendor.Location = new System.Drawing.Point(5, 7);
            this.grpManageVendor.Name = "grpManageVendor";
            this.grpManageVendor.Size = new System.Drawing.Size(1071, 276);
            this.grpManageVendor.TabIndex = 0;
            this.grpManageVendor.TabStop = false;
            this.grpManageVendor.Text = "Invoice Entry";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.rbtDebit);
            this.panel2.Controls.Add(this.rbtCredit);
            this.panel2.Location = new System.Drawing.Point(142, 133);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 29);
            this.panel2.TabIndex = 202;
            // 
            // rbtDebit
            // 
            this.rbtDebit.AutoSize = true;
            this.rbtDebit.Checked = true;
            this.rbtDebit.Location = new System.Drawing.Point(14, 2);
            this.rbtDebit.Name = "rbtDebit";
            this.rbtDebit.Size = new System.Drawing.Size(63, 22);
            this.rbtDebit.TabIndex = 4;
            this.rbtDebit.TabStop = true;
            this.rbtDebit.Text = "Debit";
            this.rbtDebit.UseVisualStyleBackColor = true;
            // 
            // rbtCredit
            // 
            this.rbtCredit.AutoSize = true;
            this.rbtCredit.Location = new System.Drawing.Point(105, 3);
            this.rbtCredit.Name = "rbtCredit";
            this.rbtCredit.Size = new System.Drawing.Size(68, 22);
            this.rbtCredit.TabIndex = 5;
            this.rbtCredit.Text = "Credit";
            this.rbtCredit.UseVisualStyleBackColor = true;
            // 
            // dgvUploadedDocs
            // 
            this.dgvUploadedDocs.AllowDrop = true;
            this.dgvUploadedDocs.AllowUserToAddRows = false;
            this.dgvUploadedDocs.AllowUserToDeleteRows = false;
            this.dgvUploadedDocs.AllowUserToResizeColumns = false;
            this.dgvUploadedDocs.AllowUserToResizeRows = false;
            this.dgvUploadedDocs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvUploadedDocs.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUploadedDocs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvUploadedDocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUploadedDocs.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvUploadedDocs.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvUploadedDocs.Location = new System.Drawing.Point(757, 24);
            this.dgvUploadedDocs.MultiSelect = false;
            this.dgvUploadedDocs.Name = "dgvUploadedDocs";
            this.dgvUploadedDocs.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUploadedDocs.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvUploadedDocs.RowHeadersVisible = false;
            this.dgvUploadedDocs.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvUploadedDocs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvUploadedDocs.Size = new System.Drawing.Size(235, 133);
            this.dgvUploadedDocs.TabIndex = 200;
            this.dgvUploadedDocs.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(670, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 15);
            this.label16.TabIndex = 199;
            this.label16.Text = "Select Docs";
            // 
            // ddlVendorName
            // 
            this.ddlVendorName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVendorName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlVendorName.FormattingEnabled = true;
            this.ddlVendorName.Items.AddRange(new object[] {
            "--Select--",
            "PR",
            "Forign Worker",
            "Student Open Work Permit",
            "Closed Work Permit",
            "Citizen",
            "Work Permit",
            "Tourist",
            "Visitor",
            "Others"});
            this.ddlVendorName.Location = new System.Drawing.Point(141, 31);
            this.ddlVendorName.Name = "ddlVendorName";
            this.ddlVendorName.Size = new System.Drawing.Size(201, 22);
            this.ddlVendorName.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(19, 142);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 15);
            this.label13.TabIndex = 195;
            this.label13.Text = "Transaction Type";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.AliceBlue;
            this.label9.Font = new System.Drawing.Font("Arial", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(377, 198);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 15);
            this.label9.TabIndex = 194;
            this.label9.Text = "Total";
            // 
            // txtInvoiceBal
            // 
            this.txtInvoiceBal.BackColor = System.Drawing.Color.LightGray;
            this.txtInvoiceBal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceBal.ForeColor = System.Drawing.Color.Black;
            this.txtInvoiceBal.Location = new System.Drawing.Point(481, 192);
            this.txtInvoiceBal.Name = "txtInvoiceBal";
            this.txtInvoiceBal.ReadOnly = true;
            this.txtInvoiceBal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtInvoiceBal.Size = new System.Drawing.Size(168, 22);
            this.txtInvoiceBal.TabIndex = 191;
            this.txtInvoiceBal.Text = "0";
            this.txtInvoiceBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtcashPayoutAmt
            // 
            this.txtcashPayoutAmt.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcashPayoutAmt.ForeColor = System.Drawing.Color.Black;
            this.txtcashPayoutAmt.Location = new System.Drawing.Point(481, 157);
            this.txtcashPayoutAmt.MaxLength = 10;
            this.txtcashPayoutAmt.Name = "txtcashPayoutAmt";
            this.txtcashPayoutAmt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtcashPayoutAmt.Size = new System.Drawing.Size(168, 26);
            this.txtcashPayoutAmt.TabIndex = 10;
            this.txtcashPayoutAmt.Text = "0";
            this.txtcashPayoutAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcashPayoutAmt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            this.txtcashPayoutAmt.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.AliceBlue;
            this.label11.Font = new System.Drawing.Font("Arial", 9F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(377, 170);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 15);
            this.label11.TabIndex = 193;
            this.label11.Text = "Cash Payout";
            // 
            // txtcashpaidamt
            // 
            this.txtcashpaidamt.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcashpaidamt.ForeColor = System.Drawing.Color.Black;
            this.txtcashpaidamt.Location = new System.Drawing.Point(481, 122);
            this.txtcashpaidamt.MaxLength = 10;
            this.txtcashpaidamt.Name = "txtcashpaidamt";
            this.txtcashpaidamt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtcashpaidamt.Size = new System.Drawing.Size(168, 26);
            this.txtcashpaidamt.TabIndex = 9;
            this.txtcashpaidamt.Text = "0";
            this.txtcashpaidamt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcashpaidamt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            this.txtcashpaidamt.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.AliceBlue;
            this.label12.Font = new System.Drawing.Font("Arial", 9F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(377, 135);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 15);
            this.label12.TabIndex = 192;
            this.label12.Text = "Cash Paid";
            // 
            // txtGst
            // 
            this.txtGst.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGst.ForeColor = System.Drawing.Color.Black;
            this.txtGst.Location = new System.Drawing.Point(481, 53);
            this.txtGst.MaxLength = 10;
            this.txtGst.Name = "txtGst";
            this.txtGst.Size = new System.Drawing.Size(168, 26);
            this.txtGst.TabIndex = 7;
            this.txtGst.Text = "0";
            this.txtGst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.AliceBlue;
            this.label7.Font = new System.Drawing.Font("Arial", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(377, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 15);
            this.label7.TabIndex = 187;
            this.label7.Text = "GST";
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.ForeColor = System.Drawing.Color.Black;
            this.txtAmount.Location = new System.Drawing.Point(481, 88);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAmount.Size = new System.Drawing.Size(168, 26);
            this.txtAmount.TabIndex = 8;
            this.txtAmount.Text = "0";
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(377, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 15);
            this.label6.TabIndex = 184;
            this.label6.Text = "Total Amount";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(670, 180);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 183;
            this.label62.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(757, 189);
            this.txtRemarks.MaxLength = 100;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(235, 74);
            this.txtRemarks.TabIndex = 12;
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceNo.ForeColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Location = new System.Drawing.Point(481, 18);
            this.txtInvoiceNo.MaxLength = 40;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Size = new System.Drawing.Size(168, 26);
            this.txtInvoiceNo.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(19, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 15);
            this.label3.TabIndex = 176;
            this.label3.Text = "Payment Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(377, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 15);
            this.label5.TabIndex = 172;
            this.label5.Text = "Invoice No";
            // 
            // ddlPaymnetType
            // 
            this.ddlPaymnetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPaymnetType.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPaymnetType.FormattingEnabled = true;
            this.ddlPaymnetType.Location = new System.Drawing.Point(141, 100);
            this.ddlPaymnetType.Name = "ddlPaymnetType";
            this.ddlPaymnetType.Size = new System.Drawing.Size(201, 22);
            this.ddlPaymnetType.TabIndex = 3;
            // 
            // dtpPayment
            // 
            this.dtpPayment.CustomFormat = "MM/dd/yyyy";
            this.dtpPayment.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPayment.Location = new System.Drawing.Point(141, 68);
            this.dtpPayment.Name = "dtpPayment";
            this.dtpPayment.Size = new System.Drawing.Size(201, 21);
            this.dtpPayment.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(19, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 84;
            this.label1.Text = "Invoice Date";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(526, 227);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(103, 37);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.AliceBlue;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(656, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 15);
            this.label10.TabIndex = 76;
            this.label10.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.AliceBlue;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(350, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 15);
            this.label8.TabIndex = 75;
            this.label8.Text = "*";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(414, 227);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(105, 37);
            this.BtnDelete.TabIndex = 15;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(192, 227);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(105, 37);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(19, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 15);
            this.label2.TabIndex = 36;
            this.label2.Text = "Vendor Code";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUpdate.FlatAppearance.BorderSize = 2;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(302, 227);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(105, 37);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // grpVendorList
            // 
            this.grpVendorList.Controls.Add(this.dgvInvoiceList);
            this.grpVendorList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpVendorList.Location = new System.Drawing.Point(9, 290);
            this.grpVendorList.Name = "grpVendorList";
            this.grpVendorList.Size = new System.Drawing.Size(1066, 239);
            this.grpVendorList.TabIndex = 48;
            this.grpVendorList.TabStop = false;
            this.grpVendorList.Text = "Invoices List";
            // 
            // dgvInvoiceList
            // 
            this.dgvInvoiceList.AllowDrop = true;
            this.dgvInvoiceList.AllowUserToAddRows = false;
            this.dgvInvoiceList.AllowUserToDeleteRows = false;
            this.dgvInvoiceList.AllowUserToResizeColumns = false;
            this.dgvInvoiceList.AllowUserToResizeRows = false;
            this.dgvInvoiceList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvInvoiceList.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoiceList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInvoiceList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvInvoiceList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInvoiceList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvInvoiceList.Location = new System.Drawing.Point(3, 18);
            this.dgvInvoiceList.MultiSelect = false;
            this.dgvInvoiceList.Name = "dgvInvoiceList";
            this.dgvInvoiceList.ReadOnly = true;
            this.dgvInvoiceList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvInvoiceList.RowHeadersVisible = false;
            this.dgvInvoiceList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInvoiceList.Size = new System.Drawing.Size(1059, 217);
            this.dgvInvoiceList.TabIndex = 0;
            this.dgvInvoiceList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoiceList_CellClick);
            // 
            // InvoiceEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1090, 530);
            this.Controls.Add(this.grpManageVendor);
            this.Controls.Add(this.grpVendorList);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "InvoiceEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InvoiceEntry";
            this.Load += new System.EventHandler(this.InvoiceEntry_Load);
            this.grpManageVendor.ResumeLayout(false);
            this.grpManageVendor.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUploadedDocs)).EndInit();
            this.grpVendorList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpManageVendor;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.GroupBox grpVendorList;
        private System.Windows.Forms.DataGridView dgvInvoiceList;
        private System.Windows.Forms.DateTimePicker dtpPayment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.TextBox txtInvoiceNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ddlPaymnetType;
        private System.Windows.Forms.TextBox txtGst;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInvoiceBal;
        private System.Windows.Forms.TextBox txtcashPayoutAmt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtcashpaidamt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton rbtCredit;
        private System.Windows.Forms.RadioButton rbtDebit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox ddlVendorName;
        private System.Windows.Forms.DataGridView dgvUploadedDocs;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel2;
    }
}