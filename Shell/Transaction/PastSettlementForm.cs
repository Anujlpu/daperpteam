﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Transaction
{
    public partial class PastSettlementForm : BaseForm
    {
        public PastSettlementForm()
        {
            InitializeComponent();
        }
        public string ActualPastSettlementRemarks { get; set; }

        #region Private Methods
        private void ChildColorConfig(Form childForm)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var childConfig = context.TBL_ChilFormColorFontSettings.FirstOrDefault(s => s.IsActive == true && s.UserID == Utility.BA_Commman._userId);
                    if (childConfig != null)
                    {
                        childForm.BackColor = Color.FromName(childConfig.BackgroundColor);
                        float fl = (float)childConfig.FontSize;
                        FontStyle fontStyle = (FontStyle)Enum.Parse(typeof(FontStyle), childConfig.FontStyle, true);
                        Font childFont = new Font(childConfig.FontName, fl, fontStyle);
                        childForm.ForeColor = Color.FromName(childConfig.FontColor);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator." + ex.Message);
            }
        }
        private void OpenPastSettlement()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime fromDate = FromDatePicker.Value.Date;
                DateTime toDate = ToDatePicker.Value.Date;
                var pastSettlementsList = context.MST_FuelTransaction.Where(s => s.ActualSettlement == 0 || s.ActualSettlement == null && (s.IsPastSettlementDone == false || s.IsPastSettlementDone == null)).ToList();
                var pastSettlements = pastSettlementsList.Where((s => s.TransactionDate.Date >= fromDate && s.TransactionDate <= toDate)).Select(s => new
                {
                    s.TransactionDate,
                    s.FinalSettlement,
                    s.ActualSettlement

                }).OrderBy(s => s.TransactionDate).ToList();

                dgvSettlement.DataSource = pastSettlements;
            }
        }

        #endregion
        #region Form Events
        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                OpenPastSettlement();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        string TransactionDates = string.Empty;
        public string SelectedDates = string.Empty;
        private void SaveAsbtn_Click(object sender, EventArgs e)
        {
            try
            {
                TransactionDates = string.Empty;
                SelectedDates = string.Empty;
                foreach (DataGridViewRow row in dgvSettlement.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {
                        DateTime selectedDates = Convert.ToDateTime((row.Cells[1].Value));
                        SelectedDates += row.Cells[1].Value.ToString() + ",";
                        TransactionDates += row.Cells[1].Value.ToString() + ", "+"Actual Settlement "+"["+ row.Cells[3].Value.ToString()+"]";
                    }
                }

                if (!string.IsNullOrEmpty(SelectedDates))
                {
                    TransactionDates = TransactionDates + " Past Settlement for selected dates has been closed.";
                    ActualPastSettlementRemarks = TransactionDates;
                    MessageBox.Show("Remarks has been added for settlement.");
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Please select dates for past settlement.");
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        #endregion

        private void PastSettlementForm_Load(object sender, EventArgs e)
        {
            ChildColorConfig(this);
        }

        private void dgvSettlement_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show(e.Exception.Message);
        }

        private void dgvSettlement_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    this.dgvSettlement.CommitEdit(DataGridViewDataErrorContexts.Commit);

                //Check the value of cell
                if (this.dgvSettlement.CurrentCell.Value == null)
                {
                    //Use index of TimeOut column
                    this.dgvSettlement.Rows[e.RowIndex].Cells[0].Value = true;

                    //Set other columns values
                }
                else
                {
                    //Use index of TimeOut column
                    this.dgvSettlement.Rows[e.RowIndex].Cells[0].Value = false;

                    //Set other columns values
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }
    }
}
