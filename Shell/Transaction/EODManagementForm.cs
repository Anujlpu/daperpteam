﻿using DAL;
using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Transaction
{
    public partial class EODManagementForm : BaseForm
    {
        public EODManagementForm()
        {
            InitializeComponent();
        }

        #region Master Filter
        private string FilePath(int docId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                Mst_DocUpload doc = context.Mst_DocUpload.FirstOrDefault(s => s.UploadId == docId);
                if (doc != null)
                {
                    return doc.FilePath;
                }
                else
                {
                    return "";
                }
            }
        }
        private void LoadSavedDocuments(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType).Select(s => new DocumentList { DocId = s.UploadedDocID, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();

                foreach (DataGridViewRow item in gridView.Rows)
                {
                    var docs = docList.Where(s => s.DocId == Convert.ToInt64(item.Cells["DocId"].Value)).FirstOrDefault();
                    if (docs != null && docs.DocId == Convert.ToInt64(item.Cells["DocId"].Value))
                    {
                        item.Cells[0].Value = true;
                    }
                }
            }

        }

        private bool IsRowSelected(DataGridView gridView)
        {
            int count = 0;
            foreach (DataGridViewRow item in gridView.Rows)
            {
                if (Convert.ToBoolean(item.Cells[0].Value) == true)
                {
                    count++;
                }
            }
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsAuditior()
        {
            if ((Utility.BA_Commman._Profile == "Auditor"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private bool IsAdmin()
        {
            if ((Utility.BA_Commman._Profile == "SupAdm") || (Utility.BA_Commman._Profile == "Adm"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private bool IsBackEndUser()
        {
            if ((Utility.BA_Commman._Profile == "BE"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion

        #region Tobacco

        private void TobaccoSave()
        {
            try
            {
                int opening = 0, add = 0, substract = 0;

                using (ShellEntities context = new ShellEntities())
                {
                    DateTime date = Convert.ToDateTime(DateCalender.Text);

                    var tobaccoInventory = context.MST_TobaccoInventory.Where(s => s.TransactionDate == date).FirstOrDefault();
                    if (tobaccoInventory == null)
                    {
                        tobaccoInventory = new MST_TobaccoInventory();
                        tobaccoInventory.TobaccoAdd = add;
                        tobaccoInventory.TobaccoOpening = opening;
                        tobaccoInventory.TobaccoSubstract = substract;
                        tobaccoInventory.TransactionDate = date.Date;
                        tobaccoInventory.CreatedBy = Utility.BA_Commman._userId;
                        tobaccoInventory.CeatedDate = DateTime.Now;

                        context.MST_TobaccoInventory.Add(tobaccoInventory);
                    }
                    else
                    {
                        tobaccoInventory.TobaccoAdd = add;
                        tobaccoInventory.TobaccoOpening = opening;
                        tobaccoInventory.TobaccoSubstract = substract;
                        tobaccoInventory.TransactionDate = date.Date;
                        tobaccoInventory.ModifiedBy = Utility.BA_Commman._userId;
                        tobaccoInventory.ModifiedDate = DateTime.Now;

                    }
                    context.SaveChanges();
                    MessageBox.Show("Tobacco Inventory has been saved/updated successfully.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void EnableTobaccoGroup()
        {

            Additiongroup.Enabled = false;
            CigrateesGroup.Enabled = false;
            Chewgroup.Enabled = false;
            CigarGroup.Enabled = false;
            WWFgroup.Enabled = false;
        }
        private void TobaccoTotalAutoCalculation()
        {
            int cigarSold = 0, chewSold = 0, eCigarSold = 0, lighterSold = 0, otherSold = 0,
                cigrateesSold = 0, eodTobReportTotal = 0;
            eodTobReportTotal = Convert.ToInt32(EODTobaccoReportTotalSoldTxt.Text);
            cigarSold = Convert.ToInt32(EODTobaccoReportCigarSoldTxt.Text);
            chewSold = Convert.ToInt32(EODTobaccoReportChewSoldTxt.Text);
            eCigarSold = Convert.ToInt32(EODTobaccoReportECigarSoldTxt.Text);
            lighterSold = Convert.ToInt32(EODTobaccoReportLighterSoldTxt.Text);
            otherSold = Convert.ToInt32(EODTobaccoReportOtherSoldTxt.Text);
            cigrateesSold = (eodTobReportTotal) - (cigarSold + chewSold + eCigarSold + lighterSold + otherSold);
            CigrateesEODSoldTxt.Text = cigrateesSold.ToString();

            // Auto Populate

            ChewEODSoldTxt.Text = EODTobaccoReportChewSoldTxt.Text;
            CigarEODSoldTxt.Text = EODTobaccoReportCigarSoldTxt.Text;
            ECigarEODSoldTxt.Text = EODTobaccoReportECigarSoldTxt.Text;


        }

        #endregion

        #region Addition
        private void AdditionAutoCalculation()
        {
            double pack8 = 0.00, pack10 = 0.00, cigar10pack = 0.00,
                cigar8pack = 0.00, cigar5pack = 0.00, chews = 0.00,
                other = 0.00, total = 0.00, packTotal = 0.00, Ecgar = 0.00;
            pack8 = Convert.ToDouble(Addition8Packtxt.Text);
            pack10 = Convert.ToDouble(Addition10PackTxt.Text);
            cigar10pack = Convert.ToDouble(AdditionCigars10Packtxt.Text);
            cigar8pack = Convert.ToDouble(AdditionCigars8PackTxt.Text);
            cigar5pack = Convert.ToDouble(AdditionCigars5PackTxt.Text);
            chews = Convert.ToDouble(AdditionChewsTxt.Text);
            Ecgar = Convert.ToDouble(AdditionECigarTxt.Text);
            other = Convert.ToDouble(AdditionOtherTxt.Text);
            total = pack10 + pack8 + cigar10pack + cigar8pack + cigar5pack + chews + other + Ecgar;
            packTotal = (pack8 * 8 + pack10 * 10 + cigar8pack * 8 + cigar10pack * 10 + cigar5pack * 5 + chews + other + Ecgar);
            AdditionTotalTxt.Text = total.ToString();
            AdditionPackTotalTxt.Text = packTotal.ToString();
        }
        private void AdditionSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var addition = context.Tbl_EODAddition.FirstOrDefault(s => s.TransactionDate == date && s.AdditionType == "Addition");
                if (addition != null)
                {
                    if (IsAuditior() || IsAdmin())
                    {
                        addition.AdditionType = "Addition";
                        addition.C8Pack = Convert.ToInt32(Addition8Packtxt.Text);
                        addition.C10Pack = Convert.ToInt32(Addition10PackTxt.Text);
                        addition.Cigar8Pack = Convert.ToInt32(AdditionCigars8PackTxt.Text);
                        addition.Cigar5Pack = Convert.ToInt32(AdditionCigars5PackTxt.Text);
                        addition.Cigar10Pack = Convert.ToInt32(AdditionCigars10Packtxt.Text);
                        addition.Chews = Convert.ToInt32(AdditionChewsTxt.Text);
                        addition.E_Cigar = Convert.ToInt32(AdditionECigarTxt.Text);
                        addition.Other = Convert.ToInt32(AdditionOtherTxt.Text);
                        addition.Total = Convert.ToInt32(AdditionTotalTxt.Text);
                        addition.PackTotal = Convert.ToInt32(AdditionPackTotalTxt.Text);
                        addition.Remarks = AdditionRemarksTxt.Text;
                        addition.IsActive = true;
                        addition.ModifiedBy = Utility.BA_Commman._userId;
                        addition.ModifiedDate = DateTime.Now;

                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        addition = new Tbl_EODAddition();
                        addition.AdditionType = "Addition";
                        addition.C8Pack = Convert.ToInt32(Addition8Packtxt.Text);
                        addition.C10Pack = Convert.ToInt32(Addition10PackTxt.Text);
                        addition.Cigar8Pack = Convert.ToInt32(AdditionCigars8PackTxt.Text);
                        addition.Cigar5Pack = Convert.ToInt32(AdditionCigars5PackTxt.Text);
                        addition.Cigar10Pack = Convert.ToInt32(AdditionCigars10Packtxt.Text);
                        addition.Chews = Convert.ToInt32(AdditionChewsTxt.Text);
                        addition.Other = Convert.ToInt32(AdditionOtherTxt.Text);
                        addition.Total = Convert.ToInt32(AdditionTotalTxt.Text);
                        addition.PackTotal = Convert.ToInt32(AdditionPackTotalTxt.Text);
                        addition.Remarks = AdditionRemarksTxt.Text;
                        addition.TransactionDate = date;
                        addition.IsActive = true;
                        addition.CreatedBy = Utility.BA_Commman._userId;
                        addition.CreatedDate = DateTime.Now;
                        context.Tbl_EODAddition.Add(addition);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }


                }
                int success = context.SaveChanges();
                SaveMultipleDocs("Addition", AdditionDgv);
            }
        }
        private void LoadAdditionData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var addition = context.Tbl_EODAddition.FirstOrDefault(s => s.TransactionDate == date && s.AdditionType == "Addition");
                if (addition != null)
                {
                    Addition8Packtxt.Text = string.IsNullOrEmpty(addition.C8Pack.ToString()) ? "0" : addition.C8Pack.ToString();
                    Addition10PackTxt.Text = string.IsNullOrEmpty(addition.C10Pack.ToString()) ? "0" : addition.C10Pack.ToString();
                    AdditionCigars8PackTxt.Text = string.IsNullOrEmpty(addition.Cigar8Pack.ToString()) ? "0" : addition.Cigar8Pack.ToString();
                    AdditionCigars5PackTxt.Text = string.IsNullOrEmpty(addition.Cigar5Pack.ToString()) ? "0" : addition.Cigar5Pack.ToString();
                    AdditionCigars10Packtxt.Text = string.IsNullOrEmpty(addition.Cigar10Pack.ToString()) ? "0" : addition.Cigar10Pack.ToString();
                    AdditionChewsTxt.Text = string.IsNullOrEmpty(addition.Chews.ToString()) ? "0" : addition.Chews.ToString();
                    AdditionECigarTxt.Text = string.IsNullOrEmpty(addition.E_Cigar.ToString()) ? "0" : addition.E_Cigar.ToString();
                    AdditionOtherTxt.Text = string.IsNullOrEmpty(addition.Other.ToString()) ? "0" : addition.Other.ToString();
                    AdditionTotalTxt.Text = string.IsNullOrEmpty(addition.Total.ToString()) ? "0" : addition.Total.ToString();
                    AdditionPackTotalTxt.Text = string.IsNullOrEmpty(addition.PackTotal.ToString()) ? "0" : addition.PackTotal.ToString();
                    AdditionRemarksTxt.Text = addition.Remarks;
                }
            }
        }
        private void LoadAdditionDocuments()
        {
            try
            {
                LoadSavedDocuments("Addition", AdditionDgv);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }

        private bool ValidateAdditionMandatoryFields()
        {
            if (!IsRowSelected(AdditionDgv) && string.IsNullOrEmpty(AdditionRemarksTxt.Text))
            {
                MessageBox.Show("Addition Section - Either select document or enter remarks");
                return false;
            }
            return true;

        }
        #endregion

        #region Cigratees
        private void PreviousDayCigrateesOpening()
        {

        }
        private void CigrateesAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
                countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00, pack8 = 0.00, pack10 = 0.00;
            open = Convert.ToDouble(CigrateesOpentxt.Text);
            pack8 = Convert.ToDouble(Addition8Packtxt.Text);
            pack10 = Convert.ToDouble(Addition10PackTxt.Text);
            actualOpen = Convert.ToDouble(CigrateesActualOpenTxt.Text);
            eodSold = Convert.ToDouble(CigrateesEODSoldTxt.Text);
            close = Convert.ToDouble(CigrateesCloseTxt.Text);
            add = (pack8 * 8 + pack10 * 10);
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            ActualOpenDiffTxt.Text = actualOpenDiff.ToString();
            CigrateesAddTxt.Text = add.ToString();
            CigrateesSubTotaltxt.Text = subtotal.ToString();
            CigrateesCountSoldTxt.Text = countSold.ToString();
            CigrateesDiffTxt.Text = diff.ToString();
        }
        private void CigrateesSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var cigratees = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == "Cigratees");
                if (cigratees != null)
                {
                    if (IsAuditior() || IsAdmin())
                    {
                        cigratees.InventoryType = "Cigratees";
                        cigratees.Open = Convert.ToInt32(CigrateesOpentxt.Text);
                        cigratees.ActualOpen = Convert.ToInt32(CigrateesActualOpenTxt.Text);
                        cigratees.Add = Convert.ToInt32(CigrateesAddTxt.Text);
                        cigratees.SubTotal = Convert.ToInt32(CigrateesSubTotaltxt.Text);
                        cigratees.Close = Convert.ToInt32(CigrateesCloseTxt.Text);
                        cigratees.ActualOpenDifference = Convert.ToInt32(ActualOpenDiffTxt.Text);
                        cigratees.CountSold = Convert.ToInt32(CigrateesCountSoldTxt.Text);
                        cigratees.EODSold = Convert.ToInt32(CigrateesEODSoldTxt.Text);
                        cigratees.Difference = Convert.ToInt32(CigrateesDiffTxt.Text);
                        cigratees.Remarks = CigrateesRemarksTxt.Text;
                        cigratees.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        cigratees.IsActive = true;
                        cigratees.ModifiedBy = Utility.BA_Commman._userId;
                        cigratees.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        cigratees = new Tbl_EODInventory();
                        cigratees.InventoryType = "Cigratees";
                        cigratees.Open = Convert.ToInt32(CigrateesOpentxt.Text);
                        cigratees.ActualOpen = Convert.ToInt32(CigrateesActualOpenTxt.Text);
                        cigratees.Add = Convert.ToInt32(CigrateesAddTxt.Text);
                        cigratees.SubTotal = Convert.ToInt32(CigrateesSubTotaltxt.Text);
                        cigratees.Close = Convert.ToInt32(CigrateesCloseTxt.Text);
                        cigratees.ActualOpenDifference = Convert.ToInt32(ActualOpenDiffTxt.Text);
                        cigratees.CountSold = Convert.ToInt32(CigrateesCountSoldTxt.Text);
                        cigratees.EODSold = Convert.ToInt32(CigrateesEODSoldTxt.Text);
                        cigratees.Difference = Convert.ToInt32(CigrateesDiffTxt.Text);
                        cigratees.Remarks = CigrateesRemarksTxt.Text;
                        cigratees.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        cigratees.IsActive = true;
                        cigratees.CreatedBy = Utility.BA_Commman._userId;
                        cigratees.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(cigratees);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }

                }
                context.SaveChanges();
                SaveMultipleDocs("Cigratees", CigrateesDocsDgv);
            }
        }
        private void LoadCigrateesData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var cigratees = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == "Cigratees");
                if (cigratees != null)
                {
                    CigrateesOpentxt.Text = cigratees.Open.ToString();
                    CigrateesActualOpenTxt.Text = cigratees.ActualOpen.ToString();
                    CigrateesAddTxt.Text = cigratees.Add.ToString();
                    CigrateesSubTotaltxt.Text = cigratees.SubTotal.ToString();
                    CigrateesCloseTxt.Text = cigratees.Close.ToString();
                    ActualOpenDiffTxt.Text = cigratees.ActualOpenDifference.ToString();
                    CigrateesCountSoldTxt.Text = cigratees.CountSold.ToString();
                    CigrateesEODSoldTxt.Text = cigratees.EODSold.ToString();
                    CigrateesDiffTxt.Text = cigratees.Difference.ToString();
                    CigrateesRemarksTxt.Text = cigratees.Remarks.ToString();
                }
            }
        }
        private void LoadCigrateesDocuments()
        {
            try
            {
                LoadSavedDocuments("Cigratees", CigrateesDocsDgv);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private bool ValidateCigrateesMandatoryFields()
        {
            if (!IsRowSelected(CigrateesDocsDgv) && string.IsNullOrEmpty(CigrateesRemarksTxt.Text))
            {
                MessageBox.Show("Cigratees Section - Either select document or enter remarks");
                return false;
            }
            return true;
        }
        #endregion

        #region Chew
        private void PreviousDayChewOpening()
        {

        }
        private void ChewAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
                countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00, chew = 0.00;
            open = Convert.ToDouble(ChewOpentxt.Text);
            chew = Convert.ToDouble(AdditionChewsTxt.Text);
            actualOpen = Convert.ToDouble(ChewActualopentxt.Text);
            eodSold = Convert.ToDouble(ChewEODSoldTxt.Text);
            close = Convert.ToDouble(ChewCloseTxt.Text);
            add = chew;
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            ChewActualOpenDiffTxt.Text = actualOpenDiff.ToString();
            ChewAddTxt.Text = add.ToString();
            ChewSubTotaltxt.Text = subtotal.ToString();
            ChewCountSoldTxt.Text = countSold.ToString();
            ChewDifftxt.Text = diff.ToString();
        }
        private void TotalInventoryCalculationEOD()
        {
            int pack8 = 0, pack10 = 0, cigar10pack = 0, cigar6pack = 0, chews = 0, other = 0, total = 0, packTotal = 0;
            pack8 = Convert.ToInt32(Addition8Packtxt.Text);
            pack10 = Convert.ToInt32(Addition10PackTxt.Text);
            cigar6pack = Convert.ToInt32(AdditionCigars8PackTxt.Text);
            cigar10pack = Convert.ToInt32(AdditionCigars10Packtxt.Text);
            chews = Convert.ToInt32(AdditionChewsTxt.Text);
            other = Convert.ToInt32(AdditionOtherTxt.Text);
            total = pack8 + pack10 + cigar10pack + cigar6pack + chews + other;
            packTotal = (pack8 * 8 + pack10 * 10 + cigar10pack * 10 + cigar6pack * 6 + chews + other);
            AdditionTotalTxt.Text = total.ToString();
            AdditionPackTotalTxt.Text = packTotal.ToString();
        }
        private void ChewsSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var chew = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Chew);
                if (chew != null)
                {
                    if (IsAuditior() || IsAdmin())
                    {
                        chew.Open = Convert.ToInt32(ChewOpentxt.Text);
                        chew.ActualOpen = Convert.ToInt32(ChewActualopentxt.Text);
                        chew.Add = Convert.ToInt32(ChewAddTxt.Text);
                        chew.SubTotal = Convert.ToInt32(ChewSubTotaltxt.Text);
                        chew.Close = Convert.ToInt32(ChewCloseTxt.Text);
                        chew.ActualOpenDifference = Convert.ToInt32(ChewActualOpenDiffTxt.Text);
                        chew.CountSold = Convert.ToInt32(ChewCountSoldTxt.Text);
                        chew.EODSold = Convert.ToInt32(ChewEODSoldTxt.Text);
                        chew.Difference = Convert.ToInt32(ChewDifftxt.Text);
                        chew.Remarks = ChewsRemarksTxt.Text;
                        chew.IsActive = true;
                        chew.ModifiedBy = Utility.BA_Commman._userId;
                        chew.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        chew = new Tbl_EODInventory();
                        chew.InventoryType = "Chew";
                        chew.Open = Convert.ToInt32(ChewOpentxt.Text);
                        chew.ActualOpen = Convert.ToInt32(ChewActualopentxt.Text);
                        chew.Add = Convert.ToInt32(ChewAddTxt.Text);
                        chew.SubTotal = Convert.ToInt32(ChewSubTotaltxt.Text);
                        chew.Close = Convert.ToInt32(ChewCloseTxt.Text);
                        chew.ActualOpenDifference = Convert.ToInt32(ChewActualOpenDiffTxt.Text);
                        chew.CountSold = Convert.ToInt32(ChewCountSoldTxt.Text);
                        chew.EODSold = Convert.ToInt32(ChewEODSoldTxt.Text);
                        chew.Difference = Convert.ToInt32(ChewDifftxt.Text);
                        chew.Remarks = ChewsRemarksTxt.Text;
                        chew.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        chew.IsActive = true;
                        chew.CreatedBy = Utility.BA_Commman._userId;
                        chew.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(chew);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }


                }
                context.SaveChanges();
                SaveMultipleDocs("Chew", ChewsDgv);
            }
        }
        private void LoadChewData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var chew = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Chew);
                if (chew != null)
                {
                    ChewOpentxt.Text = chew.Open.ToString();
                    ChewActualopentxt.Text = chew.ActualOpen.ToString();
                    ChewAddTxt.Text = chew.Add.ToString();
                    ChewSubTotaltxt.Text = chew.SubTotal.ToString();
                    ChewCloseTxt.Text = chew.Close.ToString();
                    ChewActualOpenDiffTxt.Text = chew.ActualOpenDifference.ToString();
                    ChewCountSoldTxt.Text = chew.CountSold.ToString();
                    ChewEODSoldTxt.Text = chew.EODSold.ToString();
                    ChewDifftxt.Text = chew.Difference.ToString();
                    ChewsRemarksTxt.Text = chew.Remarks.ToString();
                }
            }
        }
        private void LoadChewDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.Chew, ChewsDgv);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private bool ValidateChewMandatoryFields()
        {
            if (!IsRowSelected(ChewsDgv) && string.IsNullOrEmpty(ChewsRemarksTxt.Text))
            {
                MessageBox.Show("Chew Section - Either select document or enter remarks");
                return false;
            }
            return true;
        }
        #endregion

        #region Cigars
        private void PreviousDayCigarOpening()
        {

        }
        private void CigarAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00, pack6 = 0.00, pack10 = 0.00,
                countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00, cigar = 0.00;
            open = Convert.ToDouble(CigarOpentxt.Text);
            pack6 = Convert.ToDouble(AdditionCigars8PackTxt.Text);
            pack10 = Convert.ToDouble(AdditionCigars10Packtxt.Text);
            close = Convert.ToDouble(CigrateesCloseTxt.Text);
            add = (pack6 * 6 + pack10 * 10);
            actualOpen = Convert.ToDouble(CigarActualopentxt.Text);
            eodSold = Convert.ToDouble(CigarEODSoldTxt.Text);
            close = Convert.ToDouble(CigarCloseTxt.Text);
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            CigarActualOpenDifftxt.Text = actualOpenDiff.ToString();
            CigarAddTxt.Text = add.ToString();
            CigarSubTotaltxt.Text = subtotal.ToString();
            CigarCountSoldtxt.Text = countSold.ToString();
            CigarDiffTxt.Text = diff.ToString();
        }
        private void CigarSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var chew = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == "Cigar");
                if (chew != null)
                {
                    if (IsAdmin() || IsAuditior())
                    {
                        chew.Open = Convert.ToInt32(CigarOpentxt.Text);
                        chew.ActualOpen = Convert.ToInt32(CigarActualopentxt.Text);
                        chew.Add = Convert.ToInt32(CigarAddTxt.Text);
                        chew.SubTotal = Convert.ToInt32(CigarSubTotaltxt.Text);
                        chew.Close = Convert.ToInt32(CigarCloseTxt.Text);
                        chew.ActualOpenDifference = Convert.ToInt32(CigarActualOpenDifftxt.Text);
                        chew.CountSold = Convert.ToInt32(CigarCountSoldtxt.Text);
                        chew.EODSold = Convert.ToInt32(CigarEODSoldTxt.Text);
                        chew.Difference = Convert.ToInt32(CigarDiffTxt.Text);
                        chew.Remarks = CigarRemarksTxt.Text;
                        chew.IsActive = true;
                        chew.ModifiedBy = Utility.BA_Commman._userId;
                        chew.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        chew = new Tbl_EODInventory();
                        chew.InventoryType = "Cigar";
                        chew.Open = Convert.ToInt32(CigarOpentxt.Text);
                        chew.ActualOpen = Convert.ToInt32(CigarActualopentxt.Text);
                        chew.Add = Convert.ToInt32(CigarAddTxt.Text);
                        chew.SubTotal = Convert.ToInt32(CigarSubTotaltxt.Text);
                        chew.Close = Convert.ToInt32(CigarCloseTxt.Text);
                        chew.ActualOpenDifference = Convert.ToInt32(CigarActualOpenDifftxt.Text);
                        chew.CountSold = Convert.ToInt32(CigarCountSoldtxt.Text);
                        chew.EODSold = Convert.ToInt32(CigarEODSoldTxt.Text);
                        chew.Difference = Convert.ToInt32(CigarDiffTxt.Text);
                        chew.Remarks = CigarRemarksTxt.Text;
                        chew.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        chew.IsActive = true;
                        chew.CreatedBy = Utility.BA_Commman._userId;
                        chew.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(chew);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }


                }
                context.SaveChanges();
                SaveMultipleDocs("Cigar", CigarDgv);
            }
        }
        private void LoadCigarData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var chew = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == "Cigar");
                if (chew != null)
                {
                    CigarOpentxt.Text = chew.Open.ToString();
                    CigarActualopentxt.Text = chew.ActualOpen.ToString();
                    CigarAddTxt.Text = chew.Add.ToString();
                    CigarSubTotaltxt.Text = chew.SubTotal.ToString();
                    CigarCloseTxt.Text = chew.Close.ToString();
                    CigarActualOpenDifftxt.Text = chew.ActualOpenDifference.ToString();
                    CigarCountSoldtxt.Text = chew.CountSold.ToString();
                    CigarEODSoldTxt.Text = chew.EODSold.ToString();
                    CigarDiffTxt.Text = chew.Difference.ToString();
                    CigarRemarksTxt.Text = chew.Remarks.ToString();
                }
            }
        }
        private void LoadCigarDocuments()
        {
            try
            {
                LoadSavedDocuments("Cigar", CigarDgv);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private bool ValidateCigarMandatoryFields()
        {
            if (!IsRowSelected(CigarDgv) && string.IsNullOrEmpty(CigarRemarksTxt.Text))
            {
                MessageBox.Show("Cigar Section - Either select document or enter remarks");
                return false;
            }
            return true;
        }
        #endregion

        #region E-Cigar
        private void EPreviousDayCigarOpening()
        {

        }
        private void ECigarAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
              countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00, ecigar = 0.00;
            open = Convert.ToDouble(ECigarOpenTxt.Text);
            ecigar = Convert.ToDouble(AdditionECigarTxt.Text);
            actualOpen = Convert.ToDouble(ECigarActualOpenTxt.Text);
            eodSold = Convert.ToDouble(ECigarEODSoldTxt.Text);
            close = Convert.ToDouble(ECigarCloseTxt.Text);
            add = ecigar;
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            ECigarActualOpenDiffTxt.Text = actualOpenDiff.ToString();
            ECigarAddTxt.Text = add.ToString();
            ECigarSubTotalTxt.Text = subtotal.ToString();
            ECigarCountSoldTxt.Text = countSold.ToString();
            ECigarDiffTxt.Text = diff.ToString();
        }
        private void ECigarSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var eCigar = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.ECigar);
                if (eCigar != null)
                {
                    if (IsAuditior() || IsAdmin())
                    {
                        eCigar.Open = Convert.ToInt32(ECigarOpenTxt.Text);
                        eCigar.ActualOpen = Convert.ToInt32(ECigarActualOpenTxt.Text);
                        eCigar.Add = Convert.ToInt32(ECigarAddTxt.Text);
                        eCigar.SubTotal = Convert.ToInt32(ECigarSubTotalTxt.Text);
                        eCigar.Close = Convert.ToInt32(ECigarCloseTxt.Text);
                        eCigar.ActualOpenDifference = Convert.ToInt32(ECigarActualOpenDiffTxt.Text);
                        eCigar.CountSold = Convert.ToInt32(ECigarCountSoldTxt.Text);
                        eCigar.EODSold = Convert.ToInt32(ECigarEODSoldTxt.Text);
                        eCigar.Difference = Convert.ToInt32(ECigarDiffTxt.Text);
                        eCigar.Remarks = ECigarRemarksTxt.Text;
                        eCigar.IsActive = true;
                        eCigar.ModifiedBy = Utility.BA_Commman._userId;
                        eCigar.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsAdmin() || IsBackEndUser())
                    {
                        eCigar = new Tbl_EODInventory();
                        eCigar.InventoryType = DocumentTypeConstants.ECigar;
                        eCigar.Open = Convert.ToInt32(ECigarOpenTxt.Text);
                        eCigar.ActualOpen = Convert.ToInt32(ECigarActualOpenTxt.Text);
                        eCigar.Add = Convert.ToInt32(ECigarAddTxt.Text);
                        eCigar.SubTotal = Convert.ToInt32(ECigarSubTotalTxt.Text);
                        eCigar.Close = Convert.ToInt32(ECigarCloseTxt.Text);
                        eCigar.ActualOpenDifference = Convert.ToInt32(ECigarActualOpenDiffTxt.Text);
                        eCigar.CountSold = Convert.ToInt32(ECigarCountSoldTxt.Text);
                        eCigar.EODSold = Convert.ToInt32(ECigarEODSoldTxt.Text);
                        eCigar.Difference = Convert.ToInt32(ECigarDiffTxt.Text);
                        eCigar.Remarks = ECigarRemarksTxt.Text;
                        eCigar.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        eCigar.IsActive = true;
                        eCigar.CreatedBy = Utility.BA_Commman._userId;
                        eCigar.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(eCigar);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;

                    }


                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.ECigar, ECigarDgv);
            }
        }
        private void LoadECigarData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var eCigar = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.ECigar);
                if (eCigar != null)
                {
                    ECigarOpenTxt.Text = eCigar.Open.ToString();
                    ECigarActualOpenTxt.Text = eCigar.ActualOpen.ToString();
                    ECigarAddTxt.Text = eCigar.Add.ToString();
                    ECigarSubTotalTxt.Text = eCigar.SubTotal.ToString();
                    ECigarCloseTxt.Text = eCigar.Close.ToString();
                    ECigarActualOpenDiffTxt.Text = eCigar.ActualOpenDifference.ToString();
                    ECigarCountSoldTxt.Text = eCigar.CountSold.ToString();
                    ECigarEODSoldTxt.Text = eCigar.EODSold.ToString();
                    ECigarDiffTxt.Text = eCigar.Difference.ToString();
                    ECigarRemarksTxt.Text = eCigar.Remarks.ToString();
                }
            }
        }
        private void LoadECigarDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.ECigar, ECigarDgv);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private bool ValidateECigarMandatoryFields()
        {
            if (!IsRowSelected(ECigarDgv) && string.IsNullOrEmpty(ECigarRemarksTxt.Text))
            {
                MessageBox.Show("E-Cigar Section - Either select document or enter remarks");
                return false;
            }
            return true;
        }

        #endregion

        #region  WWF Calc
        private void WWFCalCLastDayWWFCalcOpening()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text).AddDays(-1);
                var wwfCalc = context.TBL_WWFCalc.FirstOrDefault(s => s.TransactionDate == date);
                if (wwfCalc != null)
                {
                    WWFLastDatTotalTxt.Text = wwfCalc.LastDayTotal.ToString();
                }
            }
        }
        private void WWFCalCAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
              countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00, ecigar = 0.00;
            open = Convert.ToDouble(ECigarOpenTxt.Text);
            ecigar = Convert.ToDouble(AdditionECigarTxt.Text);
            actualOpen = Convert.ToDouble(ECigarActualOpenTxt.Text);
            eodSold = Convert.ToDouble(ECigarEODSoldTxt.Text);
            close = Convert.ToDouble(ECigarCloseTxt.Text);
            add = ecigar;
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            ECigarActualOpenDiffTxt.Text = actualOpenDiff.ToString();
            ECigarAddTxt.Text = add.ToString();
            ECigarSubTotalTxt.Text = subtotal.ToString();
            ECigarCountSoldTxt.Text = countSold.ToString();
            ECigarDiffTxt.Text = diff.ToString();
        }
        private void WWFCalCSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var wwfCalc = context.TBL_WWFCalc.FirstOrDefault(s => s.TransactionDate == date);
                if (wwfCalc != null)
                {
                    if (IsAuditior() || IsAdmin())
                    {
                        wwfCalc.LastDayTotal = Convert.ToInt32(WWFLastDatTotalTxt.Text);
                        wwfCalc.Cases = Convert.ToInt32(WWFCasesTxt.Text);
                        wwfCalc.Added = Convert.ToInt32(WWFAddedTxt.Text);
                        wwfCalc.Sold = Convert.ToInt32(WWFSoldTxt.Text);
                        wwfCalc.UsedOutSide = Convert.ToInt32(WWFUsedOutSideTxt.Text);
                        wwfCalc.ActualInventory = Convert.ToInt32(WWFActualInventoryTxt.Text);
                        wwfCalc.ExpectedInventory = Convert.ToInt32(WWFExpectedInventoryTxt.Text);
                        wwfCalc.Balance = Convert.ToInt32(WWFBalanceTxt.Text);
                        wwfCalc.Remarks = WWFRemarksTxt.Text;
                        wwfCalc.IsActive = true;
                        wwfCalc.ModifiedBy = Utility.BA_Commman._userId;
                        wwfCalc.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsAdmin() || IsBackEndUser())
                    {
                        wwfCalc = new TBL_WWFCalc();
                        wwfCalc.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        wwfCalc.LastDayTotal = Convert.ToInt32(WWFLastDatTotalTxt.Text);
                        wwfCalc.Cases = Convert.ToInt32(WWFCasesTxt.Text);
                        wwfCalc.Added = Convert.ToInt32(WWFAddedTxt.Text);
                        wwfCalc.Sold = Convert.ToInt32(WWFSoldTxt.Text);
                        wwfCalc.UsedOutSide = Convert.ToInt32(WWFUsedOutSideTxt.Text);
                        wwfCalc.ActualInventory = Convert.ToInt32(WWFActualInventoryTxt.Text);
                        wwfCalc.ExpectedInventory = Convert.ToInt32(WWFExpectedInventoryTxt.Text);
                        wwfCalc.Balance = Convert.ToInt32(WWFBalanceTxt.Text);
                        wwfCalc.Remarks = WWFRemarksTxt.Text;
                        wwfCalc.IsActive = true;
                        wwfCalc.CreatedBy = Utility.BA_Commman._userId;
                        wwfCalc.CreatedDate = DateTime.Now;
                        context.TBL_WWFCalc.Add(wwfCalc);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;

                    }


                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.WWFCalc, WWFDocsDgv);
            }
        }
        private void LoadWWFCalCData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var wwfCalc = context.TBL_WWFCalc.FirstOrDefault(s => s.TransactionDate == date);
                if (wwfCalc != null)
                {
                    //WWFLastDatTotalTxt.Text = wwfCalc.LastDayTotal.ToString();
                    WWFCasesTxt.Text = wwfCalc.Cases.ToString();
                    WWFAddedTxt.Text = wwfCalc.Added.ToString();
                    WWFSoldTxt.Text = wwfCalc.Sold.ToString();
                    WWFUsedOutSideTxt.Text = wwfCalc.UsedOutSide.ToString();
                    WWFActualInventoryTxt.Text = wwfCalc.ActualInventory.ToString();
                    WWFExpectedInventoryTxt.Text = wwfCalc.ExpectedInventory.ToString();
                    WWFBalanceTxt.Text = wwfCalc.Balance.ToString();
                    WWFRemarksTxt.Text = wwfCalc.Remarks.ToString();
                }
            }
        }
        private void WWFCalCDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.WWFCalc, WWFDocsDgv);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private bool ValidateWWFCalCMandatoryFields()
        {
            if (!IsRowSelected(WWFDocsDgv) && string.IsNullOrEmpty(WWFRemarksTxt.Text))
            {
                MessageBox.Show("WWF Calc Section - Either select document or enter remarks");
                return false;
            }
            return true;
        }
        #endregion

        #region Bind Docs
        private List<DocumentList> BindDocuments(string docType)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Mst_DocUpload.Where(s => s.DocsType == docType).Select(s => new DocumentList { DocId = s.UploadId, DocsIdentity = s.DocsIdentity, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();
                return docList;
            }
        }
        private void LoadDocuments()
        {


            var docListTabbocco = BindDocuments("Lotto UnActivated");

            var docListAddition = BindDocuments("Addition");
            if (docListAddition.Count > 0)
            {
                AdditionDgv.DataSource = docListAddition;
                AdditionDgv.Columns[1].Visible = false;
                AdditionDgv.Columns[3].Visible = false;
            }
            var docListCigratees = BindDocuments("Cigratees");
            if (docListCigratees.Count > 0)
            {
                CigrateesDocsDgv.DataSource = docListCigratees;
                CigrateesDocsDgv.Columns[1].Visible = false;
                CigrateesDocsDgv.Columns[3].Visible = false;
            }
            var docListCigar = BindDocuments("Cigar");
            if (docListCigar.Count > 0)
            {
                CigarDgv.DataSource = docListCigar;
                CigarDgv.Columns[1].Visible = false;
                CigarDgv.Columns[3].Visible = false;
            }
            var docListWWF = BindDocuments("WWF");
            if (docListWWF.Count > 0)
            {
                WWFDocsDgv.DataSource = docListWWF;
                WWFDocsDgv.Columns[1].Visible = false;
                WWFDocsDgv.Columns[3].Visible = false;
            }
            var docListChew = BindDocuments("Chew");
            if (docListChew.Count > 0)
            {
                ChewsDgv.DataSource = docListChew;
                ChewsDgv.Columns[1].Visible = false;
                ChewsDgv.Columns[3].Visible = false;
            }
        }
        #endregion

        #region Save Documents
        List<string> docIds = new List<string>();
        private void SaveMultipleDocs(string docType, DataGridView gridView)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime tranDate = Convert.ToDateTime(DateCalender.Text);
                    foreach (DataGridViewRow row in gridView.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            int docId = Convert.ToInt32((row.Cells[1].Value));
                            docIds.Add(row.Cells[1].Value.ToString());
                            var docDelete = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType && s.TransactionDate == tranDate).Select(s => new { s }).ToList();
                            foreach (var date in docDelete)
                            {
                                if (Convert.ToDateTime(date.s.TransactionDate) == tranDate)
                                {
                                    context.Tbl_EODCommonDocuments.Remove(date.s);
                                }
                            }
                            context.SaveChanges();
                            var docExist = context.Tbl_EODCommonDocuments.FirstOrDefault(s => s.UploadedDocID == docId && s.DocumentType == docType);
                            if (docExist == null)
                            {
                                Tbl_EODCommonDocuments docsObj = new Tbl_EODCommonDocuments();
                                docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                                docsObj.DocumentType = docType;
                                docsObj.InventoryType = docType;
                                docsObj.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                                docsObj.IsActive = true;
                                docsObj.CreatedBy = Utility.BA_Commman._userId;
                                docsObj.CreatedDate = DateTime.Now;
                                context.Tbl_EODCommonDocuments.Add(docsObj);
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        #endregion
        #region Other EOD Sheet Section
        private void EnableOtherGroup()
        {
            Additiongroup.Enabled = true;
            CigrateesGroup.Enabled = true;
            Chewgroup.Enabled = true;
            CigarGroup.Enabled = true;
            WWFgroup.Enabled = true;
        }
        #endregion

        #region Lotto Form Events
        private void Tobaccobtn_Click(object sender, EventArgs e)
        {

            try
            {

                TobaccoSave();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        #endregion

        #region AutoCalculation

        private void Addition8Packtxt_Leave(object sender, EventArgs e)
        {
            try
            {
                CigrateesAutoCalculation();
                TotalInventoryCalculationEOD();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }
        private void AdditionCigars10Packtxt_Leave(object sender, EventArgs e)
        {
            try
            {
                CigarAutoCalculation();
                TotalInventoryCalculationEOD();

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void AdditionChewsTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                ChewAutoCalculation();
                TotalInventoryCalculationEOD();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        #endregion

        #region EOD Parent Save
        private void AdditionSaveBtn_Click(object sender, EventArgs e)
        {

        }

        private void CigrateesSavebtn_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void ChewSaveBtn_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void CigarBtn_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }

        #endregion

        #region Form Events
        private void MasterInventoryManagementForm_Load(object sender, EventArgs e)
        {
            try
            {

                LoadDocuments();
                // Disable group box

                Additiongroup.Enabled = false;
                CigrateesGroup.Enabled = false;
                Chewgroup.Enabled = false;
                CigarGroup.Enabled = false;
                WWFgroup.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void CategorycomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ProductcomboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void BtnSave_Click(object sender, EventArgs e)
        {

        }
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                #region Other EOD Sheet

                LoadDocuments();
                EnableOtherGroup();
                LoadAdditionData();
                LoadAdditionDocuments();
                LoadCigrateesData();
                LoadCigrateesDocuments();
                LoadChewData();
                LoadChewDocuments();
                LoadCigarData();
                LoadCigarDocuments();
                LoadECigarData();
                LoadECigarDocuments();
                WWFCalCLastDayWWFCalcOpening();
                LoadWWFCalCData();
                WWFCalCDocuments();
                WWFCalCAutoCalculation();
                AdditionAutoCalculation();
                CigrateesAutoCalculation();
                ChewAutoCalculation();
                CigarAutoCalculation();
                ECigarAutoCalculation();

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }

        private void LottoActivatedAddTxt_Leave(object sender, EventArgs e)
        {

        }

        private void LottoActivatedBtn_Click(object sender, EventArgs e)
        {

        }

        private void dgvCommon_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0 && e.ColumnIndex == 2)
                {
                    DataGridView dgv = ((DataGridView)(sender));
                    int docId = Convert.ToInt32(dgv.Rows[e.RowIndex].Cells[1].Value);
                    string path = FilePath(docId);
                    if (!string.IsNullOrEmpty(path))
                    {
                        System.Diagnostics.Process.Start("explorer.exe", path);
                    }
                    else
                    {
                        MessageBox.Show("File Path doesn't exist...!");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }

        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateAdditionMandatoryFields())
                {
                    AdditionSave();
                }
                else
                {
                    return;
                }
                if (ValidateCigrateesMandatoryFields())
                {
                    CigrateesSave();
                }
                else
                {
                    return;
                }
                if (ValidateChewMandatoryFields())
                {
                    ChewsSave();
                }
                else
                {
                    return;
                }
                if (ValidateCigarMandatoryFields())
                {
                    CigarSave();
                }
                else
                {
                    return;
                }
                if (ValidateECigarMandatoryFields())
                {
                    ECigarSave();
                }
                else
                {
                    return;
                }
                if (ValidateWWFCalCMandatoryFields())
                {
                    WWFCalCSave();
                }
                else
                {
                    return;
                }

                MessageBox.Show("EOD records has been saved.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }

        private void ECigarActualOpenTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                ECigarAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }

        }
        private void label42_Click(object sender, EventArgs e)
        {

        }

        private void label74_Click(object sender, EventArgs e)
        {

        }

        private void CigrateesRemarksTxt_TextChanged(object sender, EventArgs e)
        {

        }

        private void TobaccoTotalAutoCalculation_Leave(object sender, EventArgs e)
        {
            try
            {

                TobaccoTotalAutoCalculation();
                CigarAutoCalculation();
                ChewAutoCalculation();
                ECigarAutoCalculation();
                AdditionAutoCalculation();
                CigrateesAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }

        }
        private void WWFCasesTxt_Leave(object sender, EventArgs e)
        {
            WWFCalCAutoCalculation();
        }
        #endregion


    }
}
