﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using DAL;
using Framework.Data;
using Framework;

namespace Shell
{
    public partial class BankEntry : Form
    {
        public BankEntry()
        {
            InitializeComponent();
        }
        BAL_BankEntries _obj = new BAL_BankEntries();
        void Bindgridview()
        {
            dgvBankentries.DataSource = _obj.getInvoiceDetails().Tables[0];
        }
        public void binddropdown1()
        {
            dgvCstore.DataSource = null;

            _objDoc = new BAL.BAL_FuelTransaction();

            List<DocsEntity> lst = _objDoc.ListGetDocsList(dtpPayment.Value, "BankEntries");
            if (lst.Count > 0)
            {
                dgvCstore.DataSource = lst;
            }

        }
        private void GridAddColumn()
        {
            if (dgvCstore.DataSource != null)
            {
                dgvCstore.Columns[0].Visible = false;
                dgvCstore.Columns.Insert(1, new DataGridViewCheckBoxColumn());
                dgvCstore.Columns[1].HeaderText = "Select";
                dgvCstore.Columns[1].ReadOnly = false;
                dgvCstore.Columns[2].ReadOnly = true;
                dgvCstore.Columns[1].Width = 50;
                dgvCstore.Columns[2].Visible = false;
                dgvCstore.Columns[3].Visible = false;
                dgvCstore.Columns[4].Visible = false;
            }
            //

        }

        private void BindBankList()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var bankList = (from b in context.MST_BankInfoMaster
                                    where b.IsActive == true
                                    select new
                                    {
                                        b.ID,
                                        b.BankName,
                                    }).ToList();

                    bankList.Insert(0, new { ID = 0, BankName = "--Select--" });
                    BankListcomboBox.DataSource = bankList;
                    BankListcomboBox.DisplayMember = "BankName";
                    BankListcomboBox.ValueMember = "ID";
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator-" + ex.InnerException);
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void dgvCstore_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0 && e.ColumnIndex == 0)
                {
                    DataGridView dgv = ((DataGridView)(sender));
                    if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[0].Value) == false)
                    {
                        dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
                    }

                    string gridselval = "";
                    foreach (DataGridViewRow r in dgv.Rows)
                    {
                        if (Convert.ToBoolean(r.Cells[0].Value))
                        {
                            int i = r.Index;

                            gridselval = gridselval + "," + dgv.Rows[i].Cells[1].Value.ToString();

                        }

                    }
                    if (gridselval.Length > 0)
                    {
                        gridselval = gridselval.Remove(0, 1);
                    }

                    _cklBankStatements = gridselval;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }

        private string _cklBankStatements = String.Empty;


        private void BankEntry_Load(object sender, EventArgs e)
        {
            try
            {
                Bindgridview();
                binddropdown();
                binddropdown1();
                GridAddColumn();
                BindBankList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void rbtPNL_CheckedChanged(object sender, EventArgs e)
        {
            //if (rbtPNL.Checked == true)
            //{
            //    ddlPnlType.Enabled = true;
            //    ddlVendorName.Enabled = false;
            //}

        }
        BAL_FuelTransaction _objDoc;

        private void rbtVendor_CheckedChanged(object sender, EventArgs e)
        {

            //if (rbtVendor.Checked == true)
            //{
            //    ddlPnlType.Enabled = false;
            //    ddlVendorName.Enabled = false;
            //}
        }
        public void binddropdown()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            //ddlVendorName.DataSource = _objVendor.GetVendorlist();
            //ddlVendorName.DisplayMember = "VendorName";
            //ddlVendorName.ValueMember = "VendorID";
            BAL_PNL _ObjPNL = new BAL_PNL();
            //ddlPnlType.DataSource = _ObjPNL.GetPNLlist();
            //ddlPnlType.DisplayMember = "PNLCode";
            //ddlPnlType.ValueMember = "PNLID";

        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (BankEntriesid > 0)
                {
                    int Result = _obj.DeleteEntries(BankEntriesid);

                    if (Result == 1)
                    {
                        MessageBox.Show("Entries DEleted Sucessfully");

                    }
                    else
                    {
                        MessageBox.Show("There is some issue . Please try later!");

                    }
                    ClearValue();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                EntriesUpdates(1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.Message);
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        int BankEntriesid = 0;
        private void EntriesUpdates(int Type)
        {
            if (string.IsNullOrEmpty(BankListcomboBox.Text))
            {
                MessageBox.Show("Please select the Bank Name"); BankListcomboBox.Focus();
                return;
            }
            if (_cklBankStatements == "" && string.IsNullOrEmpty(txtRemarks.Text))
            {
                MessageBox.Show("Please Select the Statement or Enter Remarks");
                return;

            }
            string TranType = string.Empty;
            if (rbtDebit.Checked == true)
                TranType = "Debit";
            else if (rbtcredit.Checked == true)
                TranType = "Credit";

            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                MessageBox.Show("Please Enter the Amount"); txtAmount.Focus();
                return;
            }
            int Result = 0;
            if (Type == 1)
                Result = _obj.InsertBankentries(dtpPayment.Value, TranType,
                     Convert.ToDouble(txtAmount.Text), txtRemarks.Text, _cklBankStatements, BankListcomboBox.Text, 0, 0);
            else
                Result = _obj.UpdateBankEntries(BankEntriesid, dtpPayment.Value, TranType,
                              Convert.ToDouble(txtAmount.Text), txtRemarks.Text, _cklBankStatements, BankListcomboBox.Text, 0, 0);
            if (Result == 1)
            {
                MessageBox.Show("Entries Updated Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }
        string str = "0123456789.";
        private void TXTKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }


        }

        private void ClearValue()
        {
            txtRemarks.Text = "";
            txtAmount.Text = "0";
            BankListcomboBox.Text = "";
            Bindgridview();
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                EntriesUpdates(2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }

        private void dgvBankentries_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dg == DialogResult.Yes)
                    {
                        if (dgvBankentries.Rows[e.RowIndex].Cells["Isclosed"].Value.ToString().ToUpper() == "TRUE")
                        {
                            MessageBox.Show("Entries has been Closed User can Not Modify it.");
                            return;
                        }
                        BankEntriesid = int.Parse(dgvBankentries.Rows[e.RowIndex].Cells["BankEntriesId"].Value.ToString());
                        txtAmount.Text = dgvBankentries.Rows[e.RowIndex].Cells["TotalAmount"].Value.ToString();
                        BankListcomboBox.Text = dgvBankentries.Rows[e.RowIndex].Cells["BankName"].Value.ToString();
                        dtpPayment.Value = Convert.ToDateTime(dgvBankentries.Rows[e.RowIndex].Cells["StatementDate"].Value.ToString());
                        txtRemarks.Text = dgvBankentries.Rows[e.RowIndex].Cells["Remarks"].Value.ToString();
                        string TranType = dgvBankentries.Rows[e.RowIndex].Cells["TransactionType"].Value.ToString();
                        if (TranType.Trim() == "Debit")
                            rbtDebit.Checked = true;
                        else

                            rbtcredit.Checked = true;
                        BtnDelete.Enabled = true;
                        btnUpdate.Enabled = true;
                        BtnSave.Enabled = false;
                    }
                    else
                    {

                        BtnDelete.Enabled = false;
                        btnUpdate.Enabled = false;
                        BtnSave.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void label62_Click(object sender, EventArgs e)
        {

        }

        private void txtRemarks_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
