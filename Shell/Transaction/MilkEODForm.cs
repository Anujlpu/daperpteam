﻿using DAL;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Transaction
{
    public partial class MilkEODForm : BaseForm
    {
        public MilkEODForm()
        {
            InitializeComponent();
        }
        #region Master Filter
        private string FilePath(int docId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                Mst_DocUpload doc = context.Mst_DocUpload.FirstOrDefault(s => s.UploadId == docId);
                if (doc != null)
                {
                    return doc.FilePath;
                }
                else
                {
                    return "";
                }
            }
        }
        private void LoadSavedDocuments(string docType, DataGridView gridView)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType).Select(s => new DocumentList { DocId = s.UploadedDocID, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();

                foreach (DataGridViewRow item in gridView.Rows)
                {
                    var docs = docList.Where(s => s.DocId == Convert.ToInt64(item.Cells["DocId"].Value)).FirstOrDefault();
                    if (docs != null && docs.DocId == Convert.ToInt64(item.Cells["DocId"].Value))
                    {
                        item.Cells[0].Value = true;
                    }
                }
            }

        }

        private bool IsRowSelected(DataGridView gridView)
        {
            int count = 0;
            foreach (DataGridViewRow item in gridView.Rows)
            {
                if (Convert.ToBoolean(item.Cells[0].Value) == true)
                {
                    count++;
                }
            }
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsAuditior()
        {
            if ((Utility.BA_Commman._Profile == "Auditor"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private bool IsAdmin()
        {
            if ((Utility.BA_Commman._Profile == "SupAdm") || (Utility.BA_Commman._Profile == "Adm"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private bool IsBackEndUser()
        {
            if ((Utility.BA_Commman._Profile == "BE"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion
        #region Bind Docs
        private List<DocumentList> BindDocuments(string docType)
        {
            using (ShellEntities context = new ShellEntities())
            {
                string tranDate = Convert.ToDateTime(DateCalender.Text).ToString("MMddyyyy");
                var docList = context.Mst_DocUpload.Where(s => s.DocsType == docType).Select(s => new DocumentList { DocId = s.UploadId, DocsIdentity = s.DocsIdentity, TransactionDate = s.TransactionDate }).ToList();
                docList = docList.Where(s => Convert.ToDateTime(s.TransactionDate).ToString("MMddyyyy") == tranDate).ToList();
                return docList;
            }
        }
        private void LoadDocuments()
        {


            var docListMilk = BindDocuments(DocumentTypeConstants.MilkDoc);
            if (docListMilk.Count > 0)
            {
                MilkAdditionDgv.DataSource = docListMilk;
                MilkAdditionDgv.Columns[1].Visible = false;
                MilkAdditionDgv.Columns[3].Visible = false;
                Milk310mlDGV.DataSource = docListMilk;
                Milk310mlDGV.Columns[1].Visible = false;
                Milk310mlDGV.Columns[3].Visible = false;
                Milk473mlDGV.DataSource = docListMilk;
                Milk473mlDGV.Columns[1].Visible = false;
                Milk473mlDGV.Columns[3].Visible = false;
                Milk1LDGV.DataSource = docListMilk;
                Milk1LDGV.Columns[1].Visible = false;
                Milk1LDGV.Columns[3].Visible = false;
                Milk2LDGV.DataSource = docListMilk;
                Milk2LDGV.Columns[1].Visible = false;
                Milk2LDGV.Columns[3].Visible = false;
                Milk4LDGV.DataSource = docListMilk;
                Milk4LDGV.Columns[1].Visible = false;
                Milk4LDGV.Columns[3].Visible = false;
            }
        }
        #endregion
        #region Save Documents
        List<string> docIds = new List<string>();
        private void SaveMultipleDocs(string docType, DataGridView gridView)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    DateTime tranDate = Convert.ToDateTime(DateCalender.Text);
                    foreach (DataGridViewRow row in gridView.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            int docId = Convert.ToInt32((row.Cells[1].Value));
                            docIds.Add(row.Cells[1].Value.ToString());
                            var docDelete = context.Tbl_EODCommonDocuments.Where(s => s.DocumentType == docType && s.TransactionDate == tranDate).Select(s => new { s }).ToList();
                            foreach (var date in docDelete)
                            {
                                if (Convert.ToDateTime(date.s.TransactionDate) == tranDate)
                                {
                                    context.Tbl_EODCommonDocuments.Remove(date.s);
                                }
                            }
                            context.SaveChanges();
                            var docExist = context.Tbl_EODCommonDocuments.FirstOrDefault(s => s.UploadedDocID == docId && s.DocumentType == docType);
                            if (docExist == null)
                            {
                                Tbl_EODCommonDocuments docsObj = new Tbl_EODCommonDocuments();
                                docsObj.UploadedDocID = Convert.ToInt64(row.Cells[1].Value);
                                docsObj.DocumentType = docType;
                                docsObj.InventoryType = docType;
                                docsObj.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                                docsObj.IsActive = true;
                                docsObj.CreatedBy = Utility.BA_Commman._userId;
                                docsObj.CreatedDate = DateTime.Now;
                                context.Tbl_EODCommonDocuments.Add(docsObj);
                            }
                        }
                    }
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "Milk EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        #endregion
        #region Milk Addition
        private void MilkAdditionAutoCalculation()
        {
            double ml310 = 0.00, ml473 = 0.00, L1 = 0.00,
                L2 = 0.00, L4 = 0.00,
                other = 0.00, total = 0.00;
            ml310 = Convert.ToDouble(MilkAddition310mlTextBox.Text);
            ml473 = Convert.ToDouble(MilkAddition473mlTextBox.Text);
            L1 = Convert.ToDouble(MilkAddition1LTextBox.Text);
            L2 = Convert.ToDouble(MilkAddition2LTextBox.Text);
            L4 = Convert.ToDouble(MilkAddition4LTextBox.Text);
            other = Convert.ToDouble(MilkAdditionOtherTextBox.Text);
            total = ml310 + ml473 + L1 + L2 + L4 + other;
            MilkAdditionTotalTextBox.Text = total.ToString();

        }
        private void MilkAdditionSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var addition = context.TBL_MilkAdditionNew.FirstOrDefault(s => s.TransactionDate == date && s.AdditionType == "Milk Addition");
                if (addition != null)
                {
                    if (IsAuditior() || IsAdmin())
                    {
                        addition.AdditionType = "Milk Addition";
                        addition.C310ml = Convert.ToInt32(MilkAddition310mlTextBox.Text);
                        addition.C473ml = Convert.ToInt32(MilkAddition473mlTextBox.Text);
                        addition.C1L = Convert.ToInt32(MilkAddition1LTextBox.Text);
                        addition.C2L = Convert.ToInt32(MilkAddition2LTextBox.Text);
                        addition.C4L = Convert.ToInt32(MilkAddition4LTextBox.Text);
                        addition.Remarks = MilkAdditionRemarksTextBox.Text;
                        addition.IsActive = true;
                        addition.ModifiedBy = Utility.BA_Commman._userId;
                        addition.ModifiedDate = DateTime.Now;

                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        addition = new TBL_MilkAdditionNew();
                        addition.AdditionType = "Milk Addition";
                        addition.C310ml = Convert.ToInt32(MilkAddition310mlTextBox.Text);
                        addition.C473ml = Convert.ToInt32(MilkAddition473mlTextBox.Text);
                        addition.C1L = Convert.ToInt32(MilkAddition1LTextBox.Text);
                        addition.C2L = Convert.ToInt32(MilkAddition2LTextBox.Text);
                        addition.C4L = Convert.ToInt32(MilkAddition4LTextBox.Text);
                        addition.Remarks = MilkAdditionRemarksTextBox.Text;
                        addition.TransactionDate = date;
                        addition.IsActive = true;
                        addition.CreatedBy = Utility.BA_Commman._userId;
                        addition.CreatedDate = DateTime.Now;
                        context.TBL_MilkAdditionNew.Add(addition);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }
                }
                int success = context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.MilkAdditionDoc, MilkAdditionDgv);
            }
        }
        private void LoadAdditionData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var addition = context.TBL_MilkAdditionNew.FirstOrDefault(s => s.TransactionDate == date && s.AdditionType == "Milk Addition");
                if (addition != null)
                {
                    MilkAddition310mlTextBox.Text = string.IsNullOrEmpty(addition.C310ml.ToString()) ? "0" : addition.C310ml.ToString();
                    MilkAddition473mlTextBox.Text = string.IsNullOrEmpty(addition.C473ml.ToString()) ? "0" : addition.C473ml.ToString();
                    MilkAddition1LTextBox.Text = string.IsNullOrEmpty(addition.C1L.ToString()) ? "0" : addition.C1L.ToString();
                    MilkAddition2LTextBox.Text = string.IsNullOrEmpty(addition.C2L.ToString()) ? "0" : addition.C2L.ToString();
                    MilkAddition4LTextBox.Text = string.IsNullOrEmpty(addition.C4L.ToString()) ? "0" : addition.C4L.ToString();
                    MilkAdditionRemarksTextBox.Text = addition.Remarks;
                }
            }
        }
        private void LoadAdditionDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.MilkAdditionDoc, MilkAdditionDgv);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "Milk EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private bool ValidateAdditionMandatoryFields()
        {
            if (!IsRowSelected(MilkAdditionDgv) && string.IsNullOrEmpty(MilkAdditionRemarksTextBox.Text))
            {
                MessageBox.Show("Milk Addition Section - Either select document or enter remarks");
                return false;
            }
            return true;

        }
        #endregion
        #region Milk 355ml
        private void PreviousDayMilk310mlOpening()
        {

        }
        private void Milk310mlAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
                countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00, pack8 = 0.00, pack10 = 0.00;
            open = Convert.ToDouble(Milk310mlOpenTextBox.Text);
            actualOpen = Convert.ToDouble(Milk310mlActualOpenTextBox.Text);
            eodSold = Convert.ToDouble(Milk310mlEODSoldTextBox.Text);
            close = Convert.ToDouble(Milk310mlCloseTextBox.Text);
            add = Convert.ToDouble(MilkAddition310mlTextBox.Text);
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            Milk310mlActualOpenDiffTextBox.Text = actualOpenDiff.ToString();
            Milk310mlAddTextBox.Text = add.ToString();
            Milk310mlSubTotalTextBox.Text = subtotal.ToString();
            Milk310mlCountSoldTextBox.Text = countSold.ToString();
            Milk310mlDiffTextBox.Text = diff.ToString();
        }
        private void LoadMilk310mlData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk310ml = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk310ml);
                if (milk310ml != null)
                {
                    Milk310mlOpenTextBox.Text = milk310ml.Open.ToString();
                    Milk310mlActualOpenTextBox.Text = milk310ml.ActualOpen.ToString();
                    Milk310mlAddTextBox.Text = milk310ml.Add.ToString();
                    Milk310mlSubTotalTextBox.Text = milk310ml.SubTotal.ToString();
                    Milk310mlCloseTextBox.Text = milk310ml.Close.ToString();
                    Milk310mlActualOpenDiffTextBox.Text = milk310ml.ActualOpenDifference.ToString();
                    Milk310mlCountSoldTextBox.Text = milk310ml.CountSold.ToString();
                    Milk310mlEODSoldTextBox.Text = milk310ml.EODSold.ToString();
                    Milk310mlDiffTextBox.Text = milk310ml.Difference.ToString();
                    Milk310mlRemarksTextBox.Text = milk310ml.Remarks.ToString();
                }
            }
        }
        private void LoadMilk310mlDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.Milk310ml, Milk310mlDGV);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "Milk EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void Milk310mlSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var chew = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == "Cigar");
                if (chew != null)
                {
                    if (IsAdmin() || IsAuditior())
                    {
                        chew.Open = Convert.ToInt32(Milk310mlOpenTextBox.Text);
                        chew.ActualOpen = Convert.ToInt32(Milk310mlActualOpenTextBox.Text);
                        chew.Add = Convert.ToInt32(Milk310mlAddTextBox.Text);
                        chew.SubTotal = Convert.ToInt32(Milk310mlSubTotalTextBox.Text);
                        chew.Close = Convert.ToInt32(Milk310mlCloseTextBox.Text);
                        chew.ActualOpenDifference = Convert.ToInt32(Milk310mlActualOpenDiffTextBox.Text);
                        chew.CountSold = Convert.ToInt32(Milk310mlCountSoldTextBox.Text);
                        chew.EODSold = Convert.ToInt32(Milk310mlEODSoldTextBox.Text);
                        chew.Difference = Convert.ToInt32(Milk310mlDiffTextBox.Text);
                        chew.Remarks = Milk310mlRemarksTextBox.Text;
                        chew.IsActive = true;
                        chew.ModifiedBy = Utility.BA_Commman._userId;
                        chew.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        chew = new Tbl_EODInventory();
                        chew.InventoryType = "Milk 310ml";
                        chew.Open = Convert.ToInt32(Milk310mlOpenTextBox.Text);
                        chew.ActualOpen = Convert.ToInt32(Milk310mlActualOpenTextBox.Text);
                        chew.Add = Convert.ToInt32(Milk310mlAddTextBox.Text);
                        chew.SubTotal = Convert.ToInt32(Milk310mlSubTotalTextBox.Text);
                        chew.Close = Convert.ToInt32(Milk310mlCloseTextBox.Text);
                        chew.ActualOpenDifference = Convert.ToInt32(Milk310mlActualOpenDiffTextBox.Text);
                        chew.CountSold = Convert.ToInt32(Milk310mlCountSoldTextBox.Text);
                        chew.EODSold = Convert.ToInt32(Milk310mlEODSoldTextBox.Text);
                        chew.Difference = Convert.ToInt32(Milk310mlDiffTextBox.Text);
                        chew.Remarks = Milk310mlRemarksTextBox.Text;
                        chew.IsActive = true;
                        chew.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        chew.CreatedBy = Utility.BA_Commman._userId;
                        chew.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(chew);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }


                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.Milk310ml, Milk310mlDGV);
            }
        }
        #endregion
        #region Milk 473ml
        private void PreviousDayMilk473mlOpening()
        {

        }
        private void Milk473mlAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
                countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00;
            open = Convert.ToDouble(Milk473mlOpenTextBox.Text);
            actualOpen = Convert.ToDouble(Milk473mlActualOpenTextBox.Text);
            eodSold = Convert.ToDouble(Milk473mlEODSoldTextBox.Text);
            close = Convert.ToDouble(Milk473mlCloseTextBox.Text);
            add = Convert.ToDouble(MilkAddition473mlTextBox.Text);
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            Milk473mlActualOpenDiffTextBox.Text = actualOpenDiff.ToString();
            Milk473mlAddTextBox.Text = add.ToString();
            Milk473mlSubTotalTextBox.Text = subtotal.ToString();
            Milk473mlCountSoldTextBox.Text = countSold.ToString();
            Milk473mlDiffTextBox.Text = diff.ToString();
        }
        private void LoadMilk473mlData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk473ml = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk473ml);
                if (milk473ml != null)
                {
                    Milk473mlOpenTextBox.Text = milk473ml.Open.ToString();
                    Milk473mlActualOpenTextBox.Text = milk473ml.ActualOpen.ToString();
                    Milk473mlAddTextBox.Text = milk473ml.Add.ToString();
                    Milk473mlSubTotalTextBox.Text = milk473ml.SubTotal.ToString();
                    Milk473mlCloseTextBox.Text = milk473ml.Close.ToString();
                    Milk473mlActualOpenDiffTextBox.Text = milk473ml.ActualOpenDifference.ToString();
                    Milk473mlCountSoldTextBox.Text = milk473ml.CountSold.ToString();
                    Milk473mlEODSoldTextBox.Text = milk473ml.EODSold.ToString();
                    Milk473mlDiffTextBox.Text = milk473ml.Difference.ToString();
                    Milk473mlRemarksTextBox.Text = milk473ml.Remarks.ToString();
                }
            }
        }
        private void LoadMilk473mlDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.Milk473ml, Milk473mlDGV);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "Milk EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void Milk473mlSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk473ml = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk473ml);
                if (milk473ml != null)
                {
                    if (IsAdmin() || IsAuditior())
                    {
                        milk473ml.Open = Convert.ToInt32(Milk473mlOpenTextBox.Text);
                        milk473ml.ActualOpen = Convert.ToInt32(Milk473mlActualOpenTextBox.Text);
                        milk473ml.Add = Convert.ToInt32(Milk473mlAddTextBox.Text);
                        milk473ml.SubTotal = Convert.ToInt32(Milk473mlSubTotalTextBox.Text);
                        milk473ml.Close = Convert.ToInt32(Milk473mlCloseTextBox.Text);
                        milk473ml.ActualOpenDifference = Convert.ToInt32(Milk473mlActualOpenDiffTextBox.Text);
                        milk473ml.CountSold = Convert.ToInt32(Milk473mlCountSoldTextBox.Text);
                        milk473ml.EODSold = Convert.ToInt32(Milk473mlEODSoldTextBox.Text);
                        milk473ml.Difference = Convert.ToInt32(Milk473mlDiffTextBox.Text);
                        milk473ml.Remarks = Milk473mlRemarksTextBox.Text;
                        milk473ml.IsActive = true;
                        milk473ml.ModifiedBy = Utility.BA_Commman._userId;
                        milk473ml.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        milk473ml = new Tbl_EODInventory();
                        milk473ml.InventoryType = DocumentTypeConstants.Milk473ml;
                        milk473ml.Open = Convert.ToInt32(Milk473mlOpenTextBox.Text);
                        milk473ml.ActualOpen = Convert.ToInt32(Milk473mlActualOpenTextBox.Text);
                        milk473ml.Add = Convert.ToInt32(Milk473mlAddTextBox.Text);
                        milk473ml.SubTotal = Convert.ToInt32(Milk473mlSubTotalTextBox.Text);
                        milk473ml.Close = Convert.ToInt32(Milk473mlCloseTextBox.Text);
                        milk473ml.ActualOpenDifference = Convert.ToInt32(Milk473mlActualOpenDiffTextBox.Text);
                        milk473ml.CountSold = Convert.ToInt32(Milk473mlCountSoldTextBox.Text);
                        milk473ml.EODSold = Convert.ToInt32(Milk473mlEODSoldTextBox.Text);
                        milk473ml.Difference = Convert.ToInt32(Milk473mlDiffTextBox.Text);
                        milk473ml.Remarks = Milk473mlRemarksTextBox.Text;
                        milk473ml.IsActive = true;
                        milk473ml.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        milk473ml.CreatedBy = Utility.BA_Commman._userId;
                        milk473ml.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(milk473ml);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }


                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.Milk473ml, Milk473mlDGV);
            }
        }

        #endregion
        #region Milk 1Ltr
        private void PreviousDayMilk1LOpening()
        {

        }
        private void Milk1LAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
                countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00;
            open = Convert.ToDouble(Milk1LOpenTextBox.Text);
            actualOpen = Convert.ToDouble(Milk1LActualOpenTextBox.Text);
            eodSold = Convert.ToDouble(Milk1LEODSoldTextBox.Text);
            close = Convert.ToDouble(Milk1LCloseTextBox.Text);
            add = Convert.ToDouble(MilkAddition1LTextBox.Text);
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            Milk1LActualOpenDiffTextBox.Text = actualOpenDiff.ToString();
            Milk1LAddTextBox.Text = add.ToString();
            Milk1LSubTotalTextBox.Text = subtotal.ToString();
            Milk1LCountSoldTextBox.Text = countSold.ToString();
            Milk1LDiffTextBox.Text = diff.ToString();
        }
        private void LoadMilk1LData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk1Ltr = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk1L);
                if (milk1Ltr != null)
                {
                    Milk1LOpenTextBox.Text = milk1Ltr.Open.ToString();
                    Milk1LActualOpenTextBox.Text = milk1Ltr.ActualOpen.ToString();
                    Milk1LAddTextBox.Text = milk1Ltr.Add.ToString();
                    Milk1LSubTotalTextBox.Text = milk1Ltr.SubTotal.ToString();
                    Milk1LCloseTextBox.Text = milk1Ltr.Close.ToString();
                    Milk1LActualOpenDiffTextBox.Text = milk1Ltr.ActualOpenDifference.ToString();
                    Milk1LCountSoldTextBox.Text = milk1Ltr.CountSold.ToString();
                    Milk1LEODSoldTextBox.Text = milk1Ltr.EODSold.ToString();
                    Milk1LDiffTextBox.Text = milk1Ltr.Difference.ToString();
                    Milk1LRemarksTextBox.Text = milk1Ltr.Remarks.ToString();
                }
            }
        }
        private void LoadMilk1LDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.Milk1L, Milk1LDGV);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "Milk EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void Milk1LSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk1Ltr = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk1L);
                if (milk1Ltr != null)
                {
                    if (IsAdmin() || IsAuditior())
                    {
                        milk1Ltr.Open = Convert.ToInt32(Milk1LOpenTextBox.Text);
                        milk1Ltr.ActualOpen = Convert.ToInt32(Milk1LActualOpenTextBox.Text);
                        milk1Ltr.Add = Convert.ToInt32(Milk1LAddTextBox.Text);
                        milk1Ltr.SubTotal = Convert.ToInt32(Milk1LSubTotalTextBox.Text);
                        milk1Ltr.Close = Convert.ToInt32(Milk1LCloseTextBox.Text);
                        milk1Ltr.ActualOpenDifference = Convert.ToInt32(Milk1LActualOpenDiffTextBox.Text);
                        milk1Ltr.CountSold = Convert.ToInt32(Milk1LCountSoldTextBox.Text);
                        milk1Ltr.EODSold = Convert.ToInt32(Milk1LEODSoldTextBox.Text);
                        milk1Ltr.Difference = Convert.ToInt32(Milk1LDiffTextBox.Text);
                        milk1Ltr.Remarks = Milk1LRemarksTextBox.Text;
                        milk1Ltr.IsActive = true;
                        milk1Ltr.ModifiedBy = Utility.BA_Commman._userId;
                        milk1Ltr.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        milk1Ltr = new Tbl_EODInventory();
                        milk1Ltr.InventoryType = DocumentTypeConstants.Milk1L;
                        milk1Ltr.Open = Convert.ToInt32(Milk1LOpenTextBox.Text);
                        milk1Ltr.ActualOpen = Convert.ToInt32(Milk1LActualOpenTextBox.Text);
                        milk1Ltr.Add = Convert.ToInt32(Milk1LAddTextBox.Text);
                        milk1Ltr.SubTotal = Convert.ToInt32(Milk1LSubTotalTextBox.Text);
                        milk1Ltr.Close = Convert.ToInt32(Milk1LCloseTextBox.Text);
                        milk1Ltr.ActualOpenDifference = Convert.ToInt32(Milk1LActualOpenDiffTextBox.Text);
                        milk1Ltr.CountSold = Convert.ToInt32(Milk1LCountSoldTextBox.Text);
                        milk1Ltr.EODSold = Convert.ToInt32(Milk1LEODSoldTextBox.Text);
                        milk1Ltr.Difference = Convert.ToInt32(Milk1LDiffTextBox.Text);
                        milk1Ltr.Remarks = Milk1LRemarksTextBox.Text;
                        milk1Ltr.IsActive = true;
                        milk1Ltr.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        milk1Ltr.CreatedBy = Utility.BA_Commman._userId;
                        milk1Ltr.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(milk1Ltr);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }


                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.Milk1L, Milk1LDGV);
            }
        }

        #endregion
        #region Milk 2Ltr
        private void PreviousDayMilk2LOpening()
        {

        }
        private void Milk2LAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
                countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00;
            open = Convert.ToDouble(Milk1LOpenTextBox.Text);
            actualOpen = Convert.ToDouble(Milk2LActualOpenTextBox.Text);
            eodSold = Convert.ToDouble(Milk2LEODSoldTextBox.Text);
            close = Convert.ToDouble(Milk2LCloseTextBox.Text);
            add = Convert.ToDouble(MilkAddition2LTextBox.Text);
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            Milk2LActualOpenDiffTextBox.Text = actualOpenDiff.ToString();
            Milk2LAddTextBox.Text = add.ToString();
            Milk2LSubTotalTextBox.Text = subtotal.ToString();
            Milk2LCountSoldTextBox.Text = countSold.ToString();
            Milk2LDiffTextBox.Text = diff.ToString();
        }
        private void LoadMilk2LData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk2Ltr = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk2L);
                if (milk2Ltr != null)
                {
                    Milk2LOpenTextBox.Text = milk2Ltr.Open.ToString();
                    Milk2LActualOpenTextBox.Text = milk2Ltr.ActualOpen.ToString();
                    Milk2LAddTextBox.Text = milk2Ltr.Add.ToString();
                    Milk2LSubTotalTextBox.Text = milk2Ltr.SubTotal.ToString();
                    Milk2LCloseTextBox.Text = milk2Ltr.Close.ToString();
                    Milk2LActualOpenDiffTextBox.Text = milk2Ltr.ActualOpenDifference.ToString();
                    Milk2LCountSoldTextBox.Text = milk2Ltr.CountSold.ToString();
                    Milk2LEODSoldTextBox.Text = milk2Ltr.EODSold.ToString();
                    Milk2LDiffTextBox.Text = milk2Ltr.Difference.ToString();
                    Milk2LRemarksTextBox.Text = milk2Ltr.Remarks.ToString();
                }
            }
        }
        private void LoadMilk2LDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.Milk2L, Milk2LDGV);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "Milk EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void Milk2LSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk2Ltr = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk2L);
                if (milk2Ltr != null)
                {
                    if (IsAdmin() || IsAuditior())
                    {
                        milk2Ltr.Open = Convert.ToInt32(Milk2LOpenTextBox.Text);
                        milk2Ltr.ActualOpen = Convert.ToInt32(Milk2LActualOpenTextBox.Text);
                        milk2Ltr.Add = Convert.ToInt32(Milk2LAddTextBox.Text);
                        milk2Ltr.SubTotal = Convert.ToInt32(Milk2LSubTotalTextBox.Text);
                        milk2Ltr.Close = Convert.ToInt32(Milk2LCloseTextBox.Text);
                        milk2Ltr.ActualOpenDifference = Convert.ToInt32(Milk2LActualOpenDiffTextBox.Text);
                        milk2Ltr.CountSold = Convert.ToInt32(Milk2LCountSoldTextBox.Text);
                        milk2Ltr.EODSold = Convert.ToInt32(Milk2LEODSoldTextBox.Text);
                        milk2Ltr.Difference = Convert.ToInt32(Milk2LDiffTextBox.Text);
                        milk2Ltr.Remarks = Milk2LRemarksTextBox.Text;
                        milk2Ltr.IsActive = true;
                        milk2Ltr.ModifiedBy = Utility.BA_Commman._userId;
                        milk2Ltr.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        milk2Ltr = new Tbl_EODInventory();
                        milk2Ltr.InventoryType = DocumentTypeConstants.Milk2L;
                        milk2Ltr.Open = Convert.ToInt32(Milk2LOpenTextBox.Text);
                        milk2Ltr.ActualOpen = Convert.ToInt32(Milk2LActualOpenTextBox.Text);
                        milk2Ltr.Add = Convert.ToInt32(Milk2LAddTextBox.Text);
                        milk2Ltr.SubTotal = Convert.ToInt32(Milk2LSubTotalTextBox.Text);
                        milk2Ltr.Close = Convert.ToInt32(Milk2LCloseTextBox.Text);
                        milk2Ltr.ActualOpenDifference = Convert.ToInt32(Milk2LActualOpenDiffTextBox.Text);
                        milk2Ltr.CountSold = Convert.ToInt32(Milk2LCountSoldTextBox.Text);
                        milk2Ltr.EODSold = Convert.ToInt32(Milk2LEODSoldTextBox.Text);
                        milk2Ltr.Difference = Convert.ToInt32(Milk2LDiffTextBox.Text);
                        milk2Ltr.Remarks = Milk2LRemarksTextBox.Text;
                        milk2Ltr.IsActive = true;
                        milk2Ltr.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        milk2Ltr.CreatedBy = Utility.BA_Commman._userId;
                        milk2Ltr.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(milk2Ltr);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }


                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.Milk2L, Milk2LDGV);
            }
        }

        #endregion
        #region Milk 4Ltr
        private void PreviousDayMilk4LOpening()
        {

        }
        private void Milk4LAutoCalculation()
        {
            double open = 0.00, actualOpen = 0.00, add = 0.00, subtotal = 0.00, close = 0.00,
                countSold = 0.00, eodSold = 0.00, diff = 0.00, actualOpenDiff = 0.00;
            open = Convert.ToDouble(Milk4LOpenTextBox.Text);
            actualOpen = Convert.ToDouble(Milk4LActualOpenTextBox.Text);
            eodSold = Convert.ToDouble(Milk4LEODSoldTextBox.Text);
            close = Convert.ToDouble(Milk4LCloseTextBox.Text);
            add = Convert.ToDouble(MilkAddition4LTextBox.Text);
            subtotal = actualOpen + add;
            countSold = subtotal - close;
            diff = eodSold - countSold;
            actualOpenDiff = actualOpen - open;
            Milk4LActualOpenDiffTextBox.Text = actualOpenDiff.ToString();
            Milk4LAddTextBox.Text = add.ToString();
            Milk4LSubTotalTextBox.Text = subtotal.ToString();
            Milk4LCountSoldTextBox.Text = countSold.ToString();
            Milk4LDiffTextBox.Text = diff.ToString();
        }
        private void LoadMilk4LData()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk4Ltr = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk4L);
                if (milk4Ltr != null)
                {
                    Milk4LOpenTextBox.Text = milk4Ltr.Open.ToString();
                    Milk4LActualOpenTextBox.Text = milk4Ltr.ActualOpen.ToString();
                    Milk4LAddTextBox.Text = milk4Ltr.Add.ToString();
                    Milk4LSubTotalTextBox.Text = milk4Ltr.SubTotal.ToString();
                    Milk4LCloseTextBox.Text = milk4Ltr.Close.ToString();
                    Milk4LActualOpenDiffTextBox.Text = milk4Ltr.ActualOpenDifference.ToString();
                    Milk4LCountSoldTextBox.Text = milk4Ltr.CountSold.ToString();
                    Milk4LEODSoldTextBox.Text = milk4Ltr.EODSold.ToString();
                    Milk4LDiffTextBox.Text = milk4Ltr.Difference.ToString();
                    Milk4LRemarksTextBox.Text = milk4Ltr.Remarks.ToString();
                }
            }
        }
        private void LoadMilk4LDocuments()
        {
            try
            {
                LoadSavedDocuments(DocumentTypeConstants.Milk4L, Milk4LDGV);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                ShellComman.ActivityForm = "Milk EOD Management";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
        private void Milk4LSave()
        {
            using (ShellEntities context = new ShellEntities())
            {
                DateTime date = Convert.ToDateTime(DateCalender.Text);
                var milk4Ltr = context.Tbl_EODInventory.FirstOrDefault(s => s.TransactionDate == date && s.InventoryType == DocumentTypeConstants.Milk4L);
                if (milk4Ltr != null)
                {
                    if (IsAdmin() || IsAuditior())
                    {
                        milk4Ltr.Open = Convert.ToInt32(Milk4LOpenTextBox.Text);
                        milk4Ltr.ActualOpen = Convert.ToInt32(Milk4LActualOpenTextBox.Text);
                        milk4Ltr.Add = Convert.ToInt32(Milk4LAddTextBox.Text);
                        milk4Ltr.SubTotal = Convert.ToInt32(Milk4LSubTotalTextBox.Text);
                        milk4Ltr.Close = Convert.ToInt32(Milk4LCloseTextBox.Text);
                        milk4Ltr.ActualOpenDifference = Convert.ToInt32(Milk4LActualOpenDiffTextBox.Text);
                        milk4Ltr.CountSold = Convert.ToInt32(Milk4LCountSoldTextBox.Text);
                        milk4Ltr.EODSold = Convert.ToInt32(Milk4LEODSoldTextBox.Text);
                        milk4Ltr.Difference = Convert.ToInt32(Milk4LDiffTextBox.Text);
                        milk4Ltr.Remarks = Milk4LRemarksTextBox.Text;
                        milk4Ltr.IsActive = true;
                        milk4Ltr.ModifiedBy = Utility.BA_Commman._userId;
                        milk4Ltr.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to update, please contact admin.");
                        return;
                    }

                }
                else
                {
                    if (IsBackEndUser() || IsAdmin())
                    {
                        milk4Ltr = new Tbl_EODInventory();
                        milk4Ltr.InventoryType = DocumentTypeConstants.Milk4L;
                        milk4Ltr.Open = Convert.ToInt32(Milk4LOpenTextBox.Text);
                        milk4Ltr.ActualOpen = Convert.ToInt32(Milk4LActualOpenTextBox.Text);
                        milk4Ltr.Add = Convert.ToInt32(Milk4LAddTextBox.Text);
                        milk4Ltr.SubTotal = Convert.ToInt32(Milk4LSubTotalTextBox.Text);
                        milk4Ltr.Close = Convert.ToInt32(Milk4LCloseTextBox.Text);
                        milk4Ltr.ActualOpenDifference = Convert.ToInt32(Milk4LActualOpenDiffTextBox.Text);
                        milk4Ltr.CountSold = Convert.ToInt32(Milk4LCountSoldTextBox.Text);
                        milk4Ltr.EODSold = Convert.ToInt32(Milk4LEODSoldTextBox.Text);
                        milk4Ltr.Difference = Convert.ToInt32(Milk4LDiffTextBox.Text);
                        milk4Ltr.Remarks = Milk4LRemarksTextBox.Text;
                        milk4Ltr.IsActive = true;
                        milk4Ltr.TransactionDate = Convert.ToDateTime(DateCalender.Text);
                        milk4Ltr.CreatedBy = Utility.BA_Commman._userId;
                        milk4Ltr.CreatedDate = DateTime.Now;
                        context.Tbl_EODInventory.Add(milk4Ltr);
                    }
                    else
                    {
                        MessageBox.Show("You don't have permission to save, please contact admin.");
                        return;
                    }


                }
                context.SaveChanges();
                SaveMultipleDocs(DocumentTypeConstants.Milk4L, Milk4LDGV);
            }
        }

        #endregion
        #region Form Events
        private void ViewBtn_Click(object sender, EventArgs e)
        {
            try
            {
                LoadDocuments();
                LoadAdditionData();
                LoadAdditionDocuments();
                MilkAdditionAutoCalculation();
                LoadMilk310mlData();
                LoadMilk310mlDocuments();
                Milk310mlAutoCalculation();
                LoadMilk473mlData();
                LoadMilk473mlDocuments();
                Milk473mlAutoCalculation();
                LoadMilk1LData();
                LoadMilk1LDocuments();
                Milk1LAutoCalculation();
                LoadMilk2LData();
                LoadMilk2LDocuments();
                Milk2LAutoCalculation();
                LoadMilk4LData();
                LoadMilk4LDocuments();
                Milk4LAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MilkEODForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadDocuments();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MilkAddition310mlTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                MilkAdditionAutoCalculation();
                Milk310mlAutoCalculation();
                Milk473mlAutoCalculation();
                Milk1LAutoCalculation();
                Milk2LAutoCalculation();
                Milk4LAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Milk310mlActualOpenTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk310mlAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Milk473mlActualOpenTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk473mlAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Milk1LActualOpenTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk1LAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Milk2LActualOpenTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk2LAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Milk4LActualOpenTextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                Milk4LAutoCalculation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                Milk310mlSave();
                Milk473mlSave();
                Milk1LSave();
                Milk2LSave();
                Milk4LSave();
                MilkAdditionSave();
                MessageBox.Show("Milk EOD information has been saved successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


    }
}
