﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Shell
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new UserLogin());
        }

        //private static bool ValidateSerialRoleBased()
        //{
            
        //}
        private static bool IsAdmin()
        {
            if ((Utility.BA_Commman._Profile == "SupAdm") || (Utility.BA_Commman._Profile == "Adm"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
