﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.SerialValidation
{
    public partial class SystemSecurityValidation : BaseForm
    {
        public SystemSecurityValidation()
        {
            InitializeComponent();
        }

        private void ValidateBtn_Click(object sender, EventArgs e)
        {
            using (ShellEntities context = new ShellEntities())
            {
                var key = Guid.Parse(ProductKeyTxt.Text);
                var serialKey = context.MST_SerialKeys.FirstOrDefault(s => s.SerialKey.Equals(key) && s.IsActive == true &&s.IsApplied==false);
                if (serialKey != null)
                {
                   
                    serialKey.IsApplied = true;
                    serialKey.MacAddress = GetMACAddress();
                    context.SaveChanges();
                    this.Close();
                    UserLogin login = new UserLogin();
                    login.Show();
                }
                else
                {
                    MessageBox.Show("Invalid key, please contact administrator.");
                }
               
            }
        }
        public string GetMACAddress()
        {
            var macAddr =
    (
        from nic in NetworkInterface.GetAllNetworkInterfaces()
        where nic.OperationalStatus == OperationalStatus.Up
        select nic.GetPhysicalAddress().ToString()
    ).FirstOrDefault();
            return macAddr;
        }
    }
}
