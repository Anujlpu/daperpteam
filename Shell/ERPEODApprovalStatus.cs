﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shell
{
    public static class ERPEODApprovalStatus
    {
        public const string Submitted = "Submitted";
        public const string Completed = "Completed";
        public const string Closed = "Closed";
        public const string Reopen = "Reopen";
        public const string ReferBack = "ReferBack";
        public const string Escalated = "Escalated";
    }
}
