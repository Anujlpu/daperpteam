﻿using Framework.Data;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TheArtOfDev.HtmlRenderer.PdfSharp;

namespace Shell
{
    public partial class FrmSalaryCreation : BaseForm
    {
        private const string ERR_UnhandledException = "Unhandled exception occured -";
        public FrmSalaryCreation()
        {
            InitializeComponent();
        }
        #region Private methods

        private void BindEmployee()
        {
            try
            {
                ddlEmployeName.Items.Clear();
                ShellEntities context = new ShellEntities();
                var employees = from m in context.Mst_EmployeeMaster select new { m.EmployeeName, m.EmployeeId };
                ddlEmployeName.Items.Insert(0, "Select");
                foreach (var e in employees)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = e.EmployeeName;
                    item.Value = e.EmployeeId;
                    ddlEmployeName.Items.Add(item);
                }
                ddlEmployeName.DisplayMember = "Text";
                ddlEmployeName.ValueMember = "Value";
                ddlEmployeName.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error occured :" + e.InnerException);
            }

        }
        private void BindEmployeeInfo()
        {
            ComboboxItem item = ddlEmployeName.SelectedItem as ComboboxItem;
            int employeeid = Convert.ToInt32(item.Value);
            var hourlyRateFrom = Convert.ToDateTime(DTPDOJ.Text);
            var rateTo = Convert.ToDateTime(ToDatePicker.Text);
            ShellEntities context = new ShellEntities();
            var employees = from m in context.Mst_EmployeeMaster where m.EmployeeId == employeeid select new { m.Department, m.CurrentWorkingLocation, m.BasicSalary, m.LivingAllowance, m.TravelAllowance, m.ActualHourlySalary, m.HourlyRateEffFrom, m.HourlyRateEffTo, m.PayRollRate, m.PayRollEffFrom, m.PayRollEffTo };
            DepartmenttextBox.Text = employees.FirstOrDefault().Department;
            LocationtextBox.Text = context.Mst_LocationMaster.Where(l => l.LocationId == employees.FirstOrDefault().CurrentWorkingLocation).FirstOrDefault().LocationName;
            txtBasicSalary.Text = employees.FirstOrDefault().BasicSalary.ToString();
            LivingAllowanceTxt.Text = employees.FirstOrDefault().LivingAllowance.ToString();
            TravelAllowanceTxt.Text = employees.FirstOrDefault().TravelAllowance.ToString();
            var employeeHourlyRate = employees.Where(s => s.HourlyRateEffFrom <= hourlyRateFrom && s.HourlyRateEffTo >= rateTo).FirstOrDefault();
            if (employeeHourlyRate != null)
            {
                HourlyRateTxt.Text = employeeHourlyRate.ActualHourlySalary.ToString();
            }
            else
            {
                HourlyRateTxt.Text = "0.00";
                MessageBox.Show("Please contact administrator, hourly rate must be defined.");
                return;
            }
            var employeePayRollRate = employees.Where(s => s.PayRollEffFrom <= hourlyRateFrom && s.PayRollEffTo >= rateTo).FirstOrDefault();
            if (employeePayRollRate != null)
            {
                PayRollTxt.Text = employeeHourlyRate.PayRollRate.ToString();
            }
            else
            {
                PayRollTxt.Text = "0.00";
                MessageBox.Show("Please contact administrator, pay roll rate must be defined.");
                return;
            }
        }

        private void BindSalarygrid()
        {
            try
            {
                dgvSalariesgrid.DataSource = null;
                ShellEntities context = new ShellEntities();
                var employeesSalary = from m in context.Mst_EmployeeSalaries join e in context.Mst_EmployeeMaster on m.EmployeeID equals e.EmployeeId where m.IsActive == true select new { e.EmployeeId, e.EmployeeName, m.FromDate, m.ToDate, m.Workingdays, m.TotalPresentDays, m.NetSalary, m.Deduction };
                dgvSalariesgrid.DataSource = employeesSalary.ToList();
            }
            catch (Exception e)
            {
                MessageBox.Show(ERR_UnhandledException + e.InnerException);
            }


        }

        private void ClearControls()
        {
            DepartmenttextBox.Text = string.Empty;
            LocationtextBox.Text = string.Empty;
            txtBasicSalary.Text = string.Empty;
            DTPDOJ.Text = string.Empty;
            DTPDOJ.Text = string.Empty;
            WorkingDaystextBox.Text = string.Empty;
            txtTotalPresent.Text = string.Empty;
            DeductiontextBox.Text = string.Empty;
            NetSalarytextBox.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            Updatebutton.Enabled = false;
            BtnSave.Enabled = true;
            BtnDelete.Enabled = false;
            GrossPaytextBox.Text = string.Empty;
            PPEndtextBox.Text = string.Empty;
            BonustextBox.Text = string.Empty;
            AdvancetextBox.Text = string.Empty;
            CPPtextBox.Text = string.Empty;
            ELtextBox.Text = string.Empty;
            TaxtextBox.Text = string.Empty;
            DTPDOJ.Text = string.Empty;
            ToDatePicker.Text = string.Empty;
        }

        private void GeneratePaySlip(Mst_EmployeeSalaries emp)
        {
            if (emp != null)
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var emplDetails = context.Mst_EmployeeMaster.Where(e => e.EmployeeId == emp.EmployeeID).FirstOrDefault();
                    var location = context.Mst_LocationMaster.Where(l => l.LocationId == emplDetails.LocationId).FirstOrDefault();
                    string templatePath = ShellComman.SalaryRootDirectory;
                    string empFilePath = emplDetails.EmployeeId + "_" + emp.SalaryDate.ToString("MMM-yyyy") + ".pdf";
                    string newRootFilePath = Path.Combine(templatePath, empFilePath);
                    if (File.Exists(newRootFilePath))
                    {
                        File.Delete(newRootFilePath);
                    }

                    string text = File.ReadAllText(templatePath);
                    text = text.Replace("@CompanyAddress", Convert.ToString(location.LocationDescription));
                    // Date range
                    text = text.Replace("@FromDate", Convert.ToDateTime(emp.FromDate).ToString("dd-MMM-yyyy"));
                    text = text.Replace("@ToDate", Convert.ToDateTime(emp.ToDate).ToString("dd-MMM-yyyy"));
                    text = text.Replace("@month", emp.SalaryDate.ToString("MMM-yyyy"));
                    text = text.Replace("@EmployeeName", emplDetails.EmployeeName.ToString());
                    text = text.Replace("@EmployeeCode", emp.EmployeeID.ToString());
                    text = text.Replace("@Designation", emplDetails.Department.ToString());
                    text = text.Replace("@TotalWorkingDays", emp.Workingdays.ToString());
                    text = text.Replace("@PresentDays", emp.TotalPresentDays.ToString());
                    text = text.Replace("@PPHours", emp.PPHours.ToString());
                    text = text.Replace("@PPRate", emplDetails.BasicHourlySalary.ToString());
                    text = text.Replace("@PPCurrentAmount", emp.GrossPay.ToString());
                    text = text.Replace("@YTDHrsUnit", "");
                    text = text.Replace("@YTDHrsAmount", "");

                    // Living
                    text = text.Replace("@LivingAllowanceCurrentAmount", Convert.ToString(emplDetails.LivingAllowance));
                    text = text.Replace("@YTDLivingAllowanceCurrentAmount", "");

                    text = text.Replace("@TotalEarnings", emp.NetSalary.ToString());
                    text = text.Replace("@YTDTotalEarnings", "");
                    // Deduction
                    text = text.Replace("@CPPAmount", emp.CPP.ToString());
                    text = text.Replace("@YTDCPPAmount", "");
                    text = text.Replace("@ELAmount", Convert.ToString(emp.EL));
                    text = text.Replace("@YTDELAmount", "");

                    // Tax
                    text = text.Replace("@TaxCurrentAmount", Convert.ToString(emp.Tax));
                    text = text.Replace("@YTDTaxAmount", "");
                    text = text.Replace("@AdvanceLOACurrentAmount", "");

                    text = text.Replace("@YTDAdvanceLOACurrentAmount", "");

                    // Total
                    text = text.Replace("@TotalDeductionCurrentAmount", Convert.ToString(emp.Deduction));
                    text = text.Replace("@YTDTotalDeductionCurrentAmount", "");

                    // Net payment
                    text = text.Replace("@NetPay", Convert.ToString(emp.NetSalary));
                    text = text.Replace("@AdvancePayment", Convert.ToString(emp.Advance));

                    PdfDocument pdf = PdfGenerator.GeneratePdf(text, PageSize.A4);
                    PdfPage page = pdf.Pages[0];
                    page.Orientation = PageOrientation.Portrait;

                    XGraphics gfx = XGraphics.FromPdfPage(page, XPageDirection.Downwards);
                    // Draw background
                    gfx.DrawImage(XImage.FromFile(ShellComman.SalaryTemplateLogo), 230, 10, 150, 50);
                    pdf.Save(newRootFilePath);


                }
            }
            else
            {
                MessageBox.Show("Please contact administrator.");
            }
        }

        private void CalculateNetPay()
        {
            var hourlyRate = Convert.ToDecimal(HourlyRateTxt.Text);
            if (hourlyRate <= 0)
            {
                MessageBox.Show("Hourly rate must be greater than 0.");
                return;
            }
            decimal vPay = 0;
            if (VPayCheckBox.Checked && Convert.ToDouble(VacationTakenTextBox.Text) <= 0)
            {
                vPay = Convert.ToDecimal(txtBasicSalary.Text) * 4 / 100;
            }
            var travelAllowance = Convert.ToDecimal(TravelAllowanceTxt.Text);
            var livingAllowance = Convert.ToDecimal(LivingAllowanceTxt.Text);
            var hoursPayment = Convert.ToDecimal(HourlyRateTxt.Text) * Convert.ToDecimal(WorkingHoursTxt.Text);
            var statPay = Convert.ToDecimal(StatPayTextBox.Text) * Convert.ToDecimal(HourlyRateTxt.Text);
            var totalPay = (Convert.ToDecimal(GrossPaytextBox.Text) + Convert.ToDecimal(vPay) + Convert.ToDecimal(BonustextBox.Text) + Convert.ToDecimal(AdvancetextBox.Text) + hoursPayment + statPay+ travelAllowance + livingAllowance);
            var totalDeduction = (Convert.ToDecimal(CPPtextBox.Text) + Convert.ToDecimal(ELtextBox.Text) + Convert.ToDecimal(TaxtextBox.Text));
            var netPay = totalPay - totalDeduction;
            DeductiontextBox.Text = totalDeduction.ToString();
            NetSalarytextBox.Text = netPay.ToString();
        }

        private void CalculateDeduction()
        {
            var totalDeduction = (Convert.ToDecimal(CPPtextBox.Text) + Convert.ToDecimal(ELtextBox.Text) + Convert.ToDecimal(TaxtextBox.Text));
            DeductiontextBox.Text = totalDeduction.ToString();

        }

        private bool SalaryAlreadyGeneratedForTheMonth(DateTime month, int empId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                var salary = context.Mst_EmployeeSalaries.Where(s => s.EmployeeID == empId && s.IsActive == true).Select(s => s.SalaryDate).ToList();
                foreach (var s in salary)
                {
                    if (s.ToString("MM") == month.ToString("MM"))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private Mst_EmployeeSalaries SaveSalary(int employeeid)
        {
            ShellEntities context = new ShellEntities();
            Mst_EmployeeSalaries salary = new Mst_EmployeeSalaries();
            salary.EmployeeID = employeeid;
            salary.SalaryDate = DateTime.Now;
            salary.Workingdays = Convert.ToInt32(WorkingDaystextBox.Text);
            salary.TotalPresentDays = Convert.ToInt32(txtTotalPresent.Text);
            salary.TotalSalary = Convert.ToDouble(NetSalarytextBox.Text);
            salary.Deduction = Convert.ToDouble(DeductiontextBox.Text);
            salary.NetSalary = Convert.ToDouble(NetSalarytextBox.Text);
            salary.Remarks = txtRemarks.Text;
            salary.CreatedDate = DateTime.Now;
            salary.CreatedBy = Utility.BA_Commman._userId;
            salary.IsActive = true;
            salary.GrossPay = Convert.ToDouble(GrossPaytextBox.Text);
            salary.PPHours = Convert.ToDouble(PPEndtextBox.Text);
            salary.Bonus = Convert.ToDouble(BonustextBox.Text);
            salary.Advance = Convert.ToDouble(AdvancetextBox.Text);
            salary.CPP = Convert.ToDouble(CPPtextBox.Text);
            salary.EL = Convert.ToDouble(ELtextBox.Text);
            salary.Tax = Convert.ToDouble(TaxtextBox.Text);
            salary.FromDate = Convert.ToDateTime(DTPDOJ.Text);
            salary.ToDate = Convert.ToDateTime(ToDatePicker.Text);
            if (Convert.ToDouble(VacationGrantedTxt.Text) > 0.00 && (VPayCheckBox.Checked))
            {
                MessageBox.Show("VPay and Vacation Granted can't be filled once.");
                return null;
            }
            if (string.IsNullOrEmpty(VacationGrantedTxt.Text) || Convert.ToDouble(VacationGrantedTxt.Text) == 0.00)
            {
                salary.VPay = VPayCheckBox.Checked;
                var vPay = 0.0d;
                if (VPayCheckBox.Checked)
                {
                    vPay = ((Convert.ToDouble(GrossPaytextBox.Text)) * 4 / 100);
                    salary.VPayTotal = vPay;
                }
                else
                {
                    salary.VPayTotal = vPay;
                }
            }
            salary.Total_11 = Convert.ToDouble(Total11TextBox.Text);
            salary.SuprHoursPaid = Convert.ToDouble(SuperVisorHoursPaidtxt.Text);
            salary.VacationGrantedHours = Convert.ToDouble(VacationGrantedTxt.Text);
            salary.VacationTakenHours = Convert.ToDouble(VacationTakenTextBox.Text);
            salary.StatPay = Convert.ToDouble(StatPayTextBox.Text);
            salary.HourlyRate = Convert.ToDouble(HourlyRateTxt.Text);
            salary.PayRollRate = Convert.ToDouble(PayRollTxt.Text);
            salary.CreatedBy = Utility.BA_Commman._userId;
            salary.CreatedDate = DateTime.Now;
            context.Mst_EmployeeSalaries.Add(salary);
            context.SaveChanges();
            return salary;
        }
        private Mst_EmployeeSalaries UpdateSalary(int employeeid)
        {
            ShellEntities context = new ShellEntities();
            Mst_EmployeeSalaries salary = context.Mst_EmployeeSalaries.Where(s => s.EmployeeID == employeeid && s.IsActive == true).FirstOrDefault();
            salary.SalaryDate = DateTime.Now;
            salary.Workingdays = Convert.ToInt32(WorkingDaystextBox.Text);
            salary.TotalPresentDays = Convert.ToInt32(txtTotalPresent.Text);
            salary.TotalSalary = Convert.ToDouble(NetSalarytextBox.Text);
            salary.Deduction = Convert.ToDouble(DeductiontextBox.Text);
            salary.NetSalary = Convert.ToDouble(NetSalarytextBox.Text);
            salary.Remarks = txtRemarks.Text;
            salary.CreatedDate = DateTime.Now;
            salary.CreatedBy = Utility.BA_Commman._userId;
            salary.IsActive = true;
            salary.GrossPay = Convert.ToDouble(GrossPaytextBox.Text);
            salary.PPHours = Convert.ToDouble(PPEndtextBox.Text);
            salary.Bonus = Convert.ToDouble(BonustextBox.Text);
            salary.Advance = Convert.ToDouble(AdvancetextBox.Text);
            salary.CPP = Convert.ToDouble(CPPtextBox.Text);
            salary.EL = Convert.ToDouble(ELtextBox.Text);
            salary.Tax = Convert.ToDouble(TaxtextBox.Text);
            salary.FromDate = Convert.ToDateTime(DTPDOJ.Text);
            salary.ToDate = Convert.ToDateTime(ToDatePicker.Text);
            if (Convert.ToDouble(VacationGrantedTxt.Text) > 0.00 && (VPayCheckBox.Checked))
            {
                MessageBox.Show("VPay and Vacation Granted can't be filled once.");
                return null;
            }
            if (string.IsNullOrEmpty(VacationGrantedTxt.Text) || Convert.ToDouble(VacationGrantedTxt.Text) == 0.00)
            {
                salary.VPay = VPayCheckBox.Checked;
                var vPay = 0.0d;
                if (VPayCheckBox.Checked)
                {
                    vPay = ((Convert.ToDouble(GrossPaytextBox.Text)) * 4 / 100);
                    salary.VPayTotal = vPay;
                }
                else
                {
                    salary.VPayTotal = vPay;
                }
            }
            salary.Total_11 = Convert.ToDouble(Total11TextBox.Text);
            salary.SuprHoursPaid = Convert.ToDouble(SuperVisorHoursPaidtxt.Text);
            salary.VacationGrantedHours = Convert.ToDouble(VacationGrantedTxt.Text);
            salary.VacationTakenHours = Convert.ToDouble(VacationTakenTextBox.Text);
            salary.StatPay = Convert.ToDouble(StatPayTextBox.Text);
            salary.HourlyRate = Convert.ToDouble(HourlyRateTxt.Text);
            salary.PayRollRate = Convert.ToDouble(PayRollTxt.Text);
            salary.ModifiedBy = Utility.BA_Commman._userId;
            salary.ModifiedDate = DateTime.Now;
            context.SaveChanges();
            return salary;
        }
        #endregion
        #region Form Events

        private void FrmSalaryCreation_Load(object sender, EventArgs e)
        {
            this.BindEmployee();
            BindSalarygrid();
            DTPDOJ.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }

        private void ddlEmployeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlEmployeName.SelectedIndex > 0)
            {
                BindEmployeeInfo();
                CalculateNetPay();
            }

        }

        private void WorkingDaystextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            decimal x;
            if (ch == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else if (!char.IsDigit(ch) && ch != '.' || !Decimal.TryParse(WorkingDaystextBox.Text + ch, out x))
            {

                e.Handled = true;
            }

        }

        private void txtTotalPresent_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            decimal x;
            if (ch == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else if (!char.IsDigit(ch) && ch != '.' || !Decimal.TryParse(txtTotalPresent.Text + ch, out x))
            {
                e.Handled = true;
            }

        }

        private void DeductiontextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            decimal x;
            if (ch == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else if (!char.IsDigit(ch) && ch != '.' || !Decimal.TryParse(DeductiontextBox.Text + ch, out x))
            {
                e.Handled = true;
            }
        }

        private void NetSalarytextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            decimal x;
            if (ch == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else if (!char.IsDigit(ch) && ch != '.' || !Decimal.TryParse(NetSalarytextBox.Text + ch, out x))
            {
                e.Handled = true;
            }
        }

        private void DeductiontextBox_Leave(object sender, EventArgs e)
        {
            NetSalarytextBox.Text = (Convert.ToInt32(NetSalarytextBox.Text) - Convert.ToInt32(DeductiontextBox.Text)).ToString();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ComboboxItem item = ddlEmployeName.SelectedItem as ComboboxItem;
                int employeeid = Convert.ToInt32(item.Value);
                if (!SalaryAlreadyGeneratedForTheMonth(Convert.ToDateTime(DTPDOJ.Text), employeeid))
                {
                    var salary = SaveSalary(employeeid);
                    if (salary != null)
                    {
                        GeneratePaySlip(salary);
                    }

                    BindSalarygrid();
                    ClearControls();
                    MessageBox.Show("Salary saved successfully.");
                }
                else
                {
                    MessageBox.Show("Salary of this employee for this month already saved, please update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.Message);
            }

        }

        private void dgvSalariesgrid_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {



        }

        private void Refreshbutton_Click(object sender, EventArgs e)
        {
            FrmSalaryCreation_Load(null, null);
            ClearControls();
        }
        private void Updatebutton_Click(object sender, EventArgs e)
        {
            try
            {
                ComboboxItem item = ddlEmployeName.SelectedItem as ComboboxItem;
                int employeeid = Convert.ToInt32(item.Value);
                if (SalaryAlreadyGeneratedForTheMonth(Convert.ToDateTime(DTPDOJ.Text), employeeid))
                {
                    GeneratePaySlip(UpdateSalary(employeeid));
                    BindSalarygrid();
                    ClearControls();
                    BindEmployee();
                    MessageBox.Show("Salary Updated successfully.");
                }
                else
                {
                    MessageBox.Show("Salary of this employee is not saved yet, please save first.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Error occured " + ex.Message);
            }
        }
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                ComboboxItem item = ddlEmployeName.SelectedItem as ComboboxItem;
                int employeeid = Convert.ToInt32(item.Value);
                ShellEntities context = new ShellEntities();
                Mst_EmployeeSalaries salary = context.Mst_EmployeeSalaries.Where(s => s.EmployeeID == employeeid && s.IsActive == true).FirstOrDefault();
                salary.IsActive = false;
                context.SaveChanges();
                BindSalarygrid();
                ClearControls();
                BindEmployee();
                MessageBox.Show("Salary Deleted successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Error occured " + ex.InnerException);
            }
        }
        private void Exitbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void WorkingDaystextBox_Leave(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(WorkingDaystextBox.Text) && Convert.ToInt32(WorkingDaystextBox.Text) > 31)
            {

                MessageBox.Show("Please enter value less than equal to 31.");
                WorkingDaystextBox.Focus();
            }

        }

        private void txtTotalPresent_Leave(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(txtTotalPresent.Text) && Convert.ToInt32(txtTotalPresent.Text) > 31)
            {

                MessageBox.Show("Please enter value less than equal to 31.");
                txtTotalPresent.Focus();
            }
        }
        private void AdvancetextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }
        private void PPEndtextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                ComboboxItem item = ddlEmployeName.SelectedItem as ComboboxItem;
                int employeeid = Convert.ToInt32(item.Value);
                ShellEntities context = new ShellEntities();
                Mst_EmployeeMaster emplMaster = context.Mst_EmployeeMaster.Where(s => s.EmployeeId == employeeid && s.IsActive == true).FirstOrDefault();
                GrossPaytextBox.Text = (Convert.ToDecimal(PPEndtextBox.Text) * emplMaster.BasicHourlySalary).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.Message);
            }

        }

        private void BonustextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }

        private void AdvancetextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }

        private void CPPtextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }

        private void ELtextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }

        private void VPaytextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }

        private void BonustextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }

        private void CPPtextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }

        private void ELtextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }

        }

        private void TaxtextBox_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.InnerException);
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            WorkingDaystextBox.Text = string.Empty;
            WorkingDaystextBox.Text = ((Convert.ToDateTime(ToDatePicker.Text) - Convert.ToDateTime(DTPDOJ.Text)).Days + 1).ToString();
        }

      
        private void dgvSalariesgrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dgvSalariesgrid.Rows[e.RowIndex];
                    int id = Convert.ToInt32(row.Cells["EmployeeID"].Value);

                    DialogResult dg = MessageBox.Show("Do You Want to Update/Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dg == DialogResult.Yes)
                    {
                        BtnSave.Enabled = false;
                        Updatebutton.Enabled = true;
                        BtnDelete.Enabled = true;
                        ShellEntities context = new ShellEntities();
                        var employeesSalary = (from m in context.Mst_EmployeeSalaries
                                               join em in context.Mst_EmployeeMaster on m.EmployeeID equals em.EmployeeId
                                               where m.EmployeeID == id
                                               select new
                                               {
                                                   em.EmployeeId,
                                                   em.EmployeeName,
                                                   m.SalaryDate,
                                                   m.Workingdays,
                                                   m.TotalPresentDays,
                                                   m.TotalSalary,
                                                   m.Deduction,
                                                   m.NetSalary,
                                                   m.Remarks,
                                                   em.Department,
                                                   em.CurrentWorkingLocation,
                                                   em.BasicSalary,
                                                   m.GrossPay,
                                                   m.PPHours,
                                                   m.Bonus,
                                                   m.Advance,
                                                   m.CPP,
                                                   m.EL,
                                                   m.Tax,
                                                   m.FromDate,
                                                   m.ToDate,
                                                   m.VPay
                                               }).FirstOrDefault();

                        ddlEmployeName.Items.Clear();

                        ComboboxItem item1 = new ComboboxItem();
                        item1.Text = employeesSalary.EmployeeName;
                        item1.Value = employeesSalary.EmployeeId;
                        ddlEmployeName.Items.Add(item1);

                        ddlEmployeName.DisplayMember = "Text";
                        ddlEmployeName.ValueMember = "Value";
                        ddlEmployeName.SelectedIndex = 0;
                        DepartmenttextBox.Text = employeesSalary.Department;
                        LocationtextBox.Text = context.Mst_LocationMaster.Where(l => l.LocationId == employeesSalary.CurrentWorkingLocation).FirstOrDefault().LocationName;
                        txtBasicSalary.Text = employeesSalary.BasicSalary.ToString();
                        DTPDOJ.Text = employeesSalary.SalaryDate.ToString();
                        WorkingDaystextBox.Text = employeesSalary.Workingdays.ToString();
                        txtTotalPresent.Text = employeesSalary.TotalPresentDays.ToString();
                        NetSalarytextBox.Text = employeesSalary.TotalSalary.ToString();
                        DeductiontextBox.Text = employeesSalary.Deduction.ToString();
                        NetSalarytextBox.Text = employeesSalary.NetSalary.ToString();
                        txtRemarks.Text = employeesSalary.Remarks;
                        GrossPaytextBox.Text = employeesSalary.GrossPay.ToString();
                        PPEndtextBox.Text = employeesSalary.PPHours.ToString();
                        BonustextBox.Text = employeesSalary.Bonus.ToString();
                        AdvancetextBox.Text = employeesSalary.Advance.ToString();
                        CPPtextBox.Text = employeesSalary.CPP.ToString();
                        ELtextBox.Text = employeesSalary.EL.ToString();
                        TaxtextBox.Text = employeesSalary.Tax.ToString();
                        DTPDOJ.Text = employeesSalary.FromDate.ToString();
                        ToDatePicker.Text = employeesSalary.ToDate.ToString();
                        // checkBox1.Checked = Convert.ToBoolean(employeesSalary.VPay);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.Message);
            }

        }
        private void WorkingHoursTxt_Leave(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.Message);
            }
        }
        private void VPayCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CalculateNetPay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ERR_UnhandledException + ex.Message);
            }
        }
        #endregion


    }
}
