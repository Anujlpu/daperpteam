﻿using Framework.Data;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TheArtOfDev.HtmlRenderer.PdfSharp;
namespace Shell.Employee
{
    public partial class FrmEmployeePayRoll : BaseForm
    {
        public FrmEmployeePayRoll()
        {
            InitializeComponent();
        }

        private void Exitbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #region Private Method
        private void GeneratePaySlip(Mst_EmployeeSalaries emp)
        {
            if (emp != null)
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var emplDetails = context.Mst_EmployeeMaster.Where(e => e.EmployeeId == emp.EmployeeID).FirstOrDefault();
                    var location = context.Mst_LocationMaster.Where(l => l.LocationId == emplDetails.LocationId).FirstOrDefault();
                    string templatePath = ShellComman.SalaryRootDirectory;
                    string empFilePath = emplDetails.EmployeeId + "_" + emp.SalaryDate.ToString("MMM-yyyy") + ".pdf";
                    string newRootFilePath = Path.Combine(templatePath, empFilePath);
                    if (File.Exists(newRootFilePath))
                    {
                        File.Delete(newRootFilePath);
                    }

                    string text = File.ReadAllText(templatePath);
                    text = text.Replace("@CompanyAddress", Convert.ToString(location.LocationDescription));
                    // Date range
                    text = text.Replace("@FromDate", Convert.ToDateTime(emp.FromDate).ToString("dd-MMM-yyyy"));
                    text = text.Replace("@ToDate", Convert.ToDateTime(emp.ToDate).ToString("dd-MMM-yyyy"));
                    text = text.Replace("@month", emp.SalaryDate.ToString("MMM-yyyy"));
                    text = text.Replace("@EmployeeName", emplDetails.EmployeeName.ToString());
                    text = text.Replace("@EmployeeCode", emp.EmployeeID.ToString());
                    text = text.Replace("@Designation", emplDetails.Department.ToString());
                    text = text.Replace("@TotalWorkingDays", emp.Workingdays.ToString());
                    text = text.Replace("@PresentDays", emp.TotalPresentDays.ToString());
                    text = text.Replace("@PPHours", emp.PPHours.ToString());
                    text = text.Replace("@PPRate", emplDetails.BasicHourlySalary.ToString());
                    text = text.Replace("@PPCurrentAmount", emp.GrossPay.ToString());
                    text = text.Replace("@YTDHrsUnit", "");
                    text = text.Replace("@YTDHrsAmount", "");

                    // Living
                    text = text.Replace("@LivingAllowanceCurrentAmount", Convert.ToString(emplDetails.LivingAllowance));
                    text = text.Replace("@YTDLivingAllowanceCurrentAmount", "");

                    text = text.Replace("@TotalEarnings", emp.NetSalary.ToString());
                    text = text.Replace("@YTDTotalEarnings", "");
                    // Deduction
                    text = text.Replace("@CPPAmount", emp.CPP.ToString());
                    text = text.Replace("@YTDCPPAmount", "");
                    text = text.Replace("@ELAmount", Convert.ToString(emp.EL));
                    text = text.Replace("@YTDELAmount", "");

                    // Tax
                    text = text.Replace("@TaxCurrentAmount", Convert.ToString(emp.Tax));
                    text = text.Replace("@YTDTaxAmount", "");
                    text = text.Replace("@AdvanceLOACurrentAmount", "");

                    text = text.Replace("@YTDAdvanceLOACurrentAmount", "");

                    // Total
                    text = text.Replace("@TotalDeductionCurrentAmount", Convert.ToString(emp.Deduction));
                    text = text.Replace("@YTDTotalDeductionCurrentAmount", "");

                    // Net payment
                    text = text.Replace("@NetPay", Convert.ToString(emp.NetSalary));
                    text = text.Replace("@AdvancePayment", Convert.ToString(emp.Advance));

                    PdfDocument pdf = PdfGenerator.GeneratePdf(text, PageSize.A4);
                    PdfPage page = pdf.Pages[0];
                    page.Orientation = PageOrientation.Portrait;

                    XGraphics gfx = XGraphics.FromPdfPage(page, XPageDirection.Downwards);
                    // Draw background
                    gfx.DrawImage(XImage.FromFile(ShellComman.SalaryTemplateLogo), 230, 10, 150, 50);
                    pdf.Save(newRootFilePath);


                }
            }
            else
            {
                MessageBox.Show("Please contact administrator.");
            }
        }
        private bool SalaryAlreadyGeneratedForTheMonth(DateTime month, int empId)
        {
            using (ShellEntities context = new ShellEntities())
            {
                var salary = context.Mst_EmployeeSalaries.Where(s => s.EmployeeID == empId && s.IsActive == true).Select(s => s.SalaryDate).ToList();
                foreach (var s in salary)
                {
                    if (s.ToString("MM") == month.ToString("MM"))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        private Mst_EmployeeSalaries SaveSalary(int employeeid)
        {
            ShellEntities context = new ShellEntities();
            Mst_EmployeeSalaries salary = new Mst_EmployeeSalaries();
            salary.EmployeeID = employeeid;
            salary.SalaryDate = DateTime.Now;
            salary.FromDate = Convert.ToDateTime(DTPDOJ.Text);
            salary.ToDate = Convert.ToDateTime(ToDatePicker.Text);
            //salary.VPay = checkBox1.Checked;
            var vPay = 0.0d;
            //if (checkBox1.Checked)
            //{
            //    vPay = ((Convert.ToDouble(GrossPaytextBox.Text)) * 4 / 100);
            //    salary.VPayTotal = vPay;
            //}
            //else
            //{
            //    salary.VPayTotal = vPay;
            //}
            context.Mst_EmployeeSalaries.Add(salary);
            context.SaveChanges();
            return salary;
        }
        private Mst_EmployeeSalaries UpdateSalary(int employeeid)
        {
            ShellEntities context = new ShellEntities();
            Mst_EmployeeSalaries salary = context.Mst_EmployeeSalaries.Where(s => s.EmployeeID == employeeid && s.IsActive == true).FirstOrDefault();
            salary.SalaryDate = Convert.ToDateTime(DTPDOJ.Text);
          
            salary.FromDate = Convert.ToDateTime(DTPDOJ.Text);
            salary.ToDate = Convert.ToDateTime(ToDatePicker.Text);
            //salary.VPay = checkBox1.Checked;
            var vPay = 0.0d;
            //if (checkBox1.Checked)
            //{
            //    vPay = ((Convert.ToDouble(GrossPaytextBox.Text)) * 4 / 100);
            //    salary.VPayTotal = vPay;
            //}
            //else
            //{
            //    salary.VPayTotal = vPay;
            //}
            context.SaveChanges();
            return salary;
        }
        private void BindEmployee()
        {
            try
            {
                ddlEmployeName.Items.Clear();
                ShellEntities context = new ShellEntities();
                var employees = from m in context.Mst_EmployeeMaster select new { m.EmployeeName, m.EmployeeId };
                ddlEmployeName.Items.Insert(0, "Select");
                foreach (var e in employees)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = e.EmployeeName;
                    item.Value = e.EmployeeId;
                    ddlEmployeName.Items.Add(item);
                }
                ddlEmployeName.DisplayMember = "Text";
                ddlEmployeName.ValueMember = "Value";
                ddlEmployeName.SelectedIndex = 0;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error occured :" + e.InnerException);
            }

        }
        #endregion
        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                ComboboxItem item = ddlEmployeName.SelectedItem as ComboboxItem;
                int employeeid = Convert.ToInt32(item.Value);
                if (!SalaryAlreadyGeneratedForTheMonth(Convert.ToDateTime(DTPDOJ.Text), employeeid))
                {
                    GeneratePaySlip(SaveSalary(employeeid));
                    MessageBox.Show("Salary Generated successfully.");
                }
                else
                {
                    MessageBox.Show("Salary of this employee for this month already generated, please update.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmEmployeePayRoll_Load(object sender, EventArgs e)
        {
            this.BindEmployee();
            DTPDOJ.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        }
    }
}
