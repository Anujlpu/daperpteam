﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using Framework.Data;

namespace Shell
{
    public static class ShellComman
    {
        /// <summary>
        /// Set the root dir for salary generate.
        /// </summary>
        /// <returns>dirPath as string</returns>
        /// 
        public static string SalaryRootDirectory
        {
            get
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var systemFolderConfig = context.TBL_SystemFolder.FirstOrDefault(s => s.UserID == Utility.BA_Commman._userId);
                    if (systemFolderConfig != null)
                    {
                        if (!Directory.Exists(systemFolderConfig.SalaryDocumentPath))
                        {
                            Directory.CreateDirectory(systemFolderConfig.SalaryDocumentPath);
                        }
                        string currentDateSys = DateTime.Now.ToString("yyyyMM");
                        string dirPathNew = Path.Combine(systemFolderConfig.SalaryDocumentPath, currentDateSys);
                        if (!Directory.Exists(dirPathNew))
                        {
                            Directory.CreateDirectory(dirPathNew);
                        }
                        return dirPathNew;
                    }
                    else
                    {
                        string dirPath = ConfigurationSettings.AppSettings["SalaryGeneratePath"];
                        if (!Directory.Exists(dirPath))
                        {
                            Directory.CreateDirectory(dirPath);
                        }
                        string currentDate = DateTime.Now.ToString("yyyyMM");
                        dirPath = Path.Combine(dirPath, currentDate);
                        if (!Directory.Exists(dirPath))
                        {
                            Directory.CreateDirectory(dirPath);
                        }
                        return dirPath;
                    }
                }
            }



        }
        public static string ReportExelPath
        {
            get
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var systemFolderConfig = context.TBL_SystemFolder.FirstOrDefault(s => s.UserID == Utility.BA_Commman._userId);
                    if (systemFolderConfig != null)
                    {
                        if (!Directory.Exists(systemFolderConfig.ReportExcelPath))
                        {
                            Directory.CreateDirectory(systemFolderConfig.ReportExcelPath);
                        }
                        string currentDateSys = DateTime.Now.ToString("yyyyMM");
                        string dirPathNew = Path.Combine(systemFolderConfig.ReportExcelPath, currentDateSys);
                        if (!Directory.Exists(dirPathNew))
                        {
                            Directory.CreateDirectory(dirPathNew);
                        }
                        return dirPathNew;
                    }
                    else
                    {
                        if (!Directory.Exists(ConfigurationSettings.AppSettings["ReportExelPath"]))
                        {
                            Directory.CreateDirectory(ConfigurationSettings.AppSettings["ReportExelPath"]);
                        }
                        return ConfigurationSettings.AppSettings["ReportExelPath"];
                    }
                }

            }

        }
        public static string DocumentUploadPath
        {
            get
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var systemFolderConfig = context.TBL_SystemFolder.FirstOrDefault(s => s.UserID == Utility.BA_Commman._userId);
                    if (systemFolderConfig != null)
                    {
                        if (!Directory.Exists(systemFolderConfig.UploadPathKey))
                        {
                            Directory.CreateDirectory(systemFolderConfig.UploadPathKey);
                        }
                        string currentDateSys = DateTime.Now.ToString("yyyyMMdd");
                        string dirPathNew = Path.Combine(systemFolderConfig.UploadPathKey, currentDateSys);
                        if (!Directory.Exists(dirPathNew))
                        {
                            Directory.CreateDirectory(dirPathNew);
                        }
                        return dirPathNew;
                    }
                    else
                    {
                        if (!Directory.Exists(ConfigurationSettings.AppSettings["FileUploadPath"]))
                        {
                            Directory.CreateDirectory(ConfigurationSettings.AppSettings["FileUploadPath"]);
                        }
                        return ConfigurationSettings.AppSettings["FileUploadPath"];
                    }
                }

            }

        }
        public static string Shell_TicketCode { get; set; }
        public static DateTime Shell_Gas_TransactionDate { get; set; }
        public static string Ticket_Request_Type { get; set; }
        public static string SalaryTemplateLogo { get { return ConfigurationSettings.AppSettings["SalaryTemplateLogo"]; } }
        public static string AdminEmailID { get { return ConfigurationSettings.AppSettings["AdminEmailID"]; } }
        public static string NotificationEmailID { get { return ConfigurationSettings.AppSettings["NotificationEmailID"]; } }
        public static string ActivityName { get; set; }
        public static string ActivityStatus { get; set; }
        public static string ActivityData { get; set; }
        public static string ActivityForm { get; set; }
        public static bool PasswordVerified { get; set; }
    }
    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }
    }
}
