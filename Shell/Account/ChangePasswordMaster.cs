﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell.Master
{
    public partial class ChangePasswordMaster : BaseForm
    {
        public ChangePasswordMaster()
        {
            InitializeComponent();
        }

        public void BindGrid()
        {
            BAL.BAL_Login _objLogin = new BAL.BAL_Login();
            dgvUserList.DataSource = _objLogin.GetUserlist();
            dgvUserList.Columns[0].Visible = false;
            dgvUserList.Columns[3].Visible = false;
            dgvUserList.Columns[4].Visible = false;
        }
        private void button1_Click(object sender, EventArgs e)
        {


        }


        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangePasswordMaster_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void dgvUserList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;

            foreach (Form frm in fc)
            {
                if (frm.Text == name)
                {
                    return true;
                }
            }
            return false;
        }
        private void dgvUserList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {

                    if (!CheckOpened("ConfirmPasswordForm"))
                    {
                        ConfirmPasswordForm.UserCode = dgvUserList.Rows[e.RowIndex].Cells["UserCode"].Value.ToString();
                        ConfirmPasswordForm form = new ConfirmPasswordForm();
                        form.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Form is already opened");
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
