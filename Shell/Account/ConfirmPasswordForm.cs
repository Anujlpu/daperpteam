﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shell
{
    public partial class ConfirmPasswordForm : Form
    {
        public ConfirmPasswordForm()
        {
            InitializeComponent();
        }
        public static string UserCode = string.Empty;
        private void button2_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(NewPaswordTxt.Text) && !string.IsNullOrEmpty(ConfirmPasswordtxt.Text))
            {
                if(NewPaswordTxt.Text.Equals(ConfirmPasswordtxt.Text))
                {
                    ChangePassword();
                }
                else
                {
                    MessageBox.Show("New Password and Confirm Password should be same.");
                }
            }
            else
            {
                MessageBox.Show("New Password and Confirm Password should not be empty.");
            }
        }
        private void ChangePassword()
        {
            BAL.BAL_Login Obj = new BAL.BAL_Login();
            int Result = Obj.ChangePassword(UserCode, NewPaswordTxt.Text);
            if (Result == 1)
            {
                MessageBox.Show("Password Sucessfully Changed");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConfirmPasswordForm_Load(object sender, EventArgs e)
        {
            UserCodeTxt.Text = UserCode;
        }
    }
}
