﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using DAL;
using Framework;
using Framework.Data;
using System.Net.NetworkInformation;
using Shell.Master;
using Shell.SerialValidation;

namespace Shell
{
    public partial class UserLogin : BaseForm
    {
        public UserLogin()
        {
            InitializeComponent();
        }

        private void UserLogin_Load(object sender, EventArgs e)
        {
            try
            {
                Utility.BA_Commman._ErrorPath = Application.CommonAppDataPath;
                StartUpUserHandler userHandler = new StartUpUserHandler();
                if (!userHandler.RegisterAdmin())
                {
                    MessageBox.Show("Issue in creating default user, please contact admin.");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool IsAdmin()
        {
            if ((Utility.BA_Commman._Profile == "SupAdm") || (Utility.BA_Commman._Profile == "Adm"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private bool ValidateSerial()
        {
            using (ShellEntities context = new ShellEntities())
            {
                var macAdd = GetMACAddress();
                var validFrom = DateTime.Now.Date;
                var validTo = DateTime.Now.Date;
                var serial = context.MST_SerialKeys.Where(s => s.MacAddress.Equals(macAdd)  && s.IsActive == true && s.IsApplied == true).ToList();
                var serialObj = serial.Where(s => s.ValidFrom.Date <= validFrom && s.ValidTill >= validTo).FirstOrDefault();

                if (serialObj == null)
                {
                    MessageBox.Show("You don't have valid product licence key or your key is expired!, please enter new valid key.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public string GetMACAddress()
        {
            var macAddr =
    (
        from nic in NetworkInterface.GetAllNetworkInterfaces()
        where nic.OperationalStatus == OperationalStatus.Up
        select nic.GetPhysicalAddress().ToString()
    ).FirstOrDefault();
            return macAddr;
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtUserName.Text))
                {
                    MessageBox.Show("Please Enter User Name");
                    return;
                }
                if (String.IsNullOrEmpty(txtPassword.Text))
                {
                    MessageBox.Show("Please Enter Password");
                    return;
                }
                string password = Utility.BA_Commman.Encrypt(txtPassword.Text, true);
                using (ShellEntities context = new ShellEntities())
                {
                    var currentPassword = context.MST_UserMaster.FirstOrDefault(u => u.UserCode == txtUserName.Text);
                    if (currentPassword != null)
                    {
                        string passwordnow = Utility.BA_Commman.Decrypt(currentPassword.Password, true);
                    }
                    var user = (from u in context.MST_UserMaster
                                join
                                r in context.MST_ProfileMaster on u.ProfileId equals r.ProfileId
                                where u.UserCode == txtUserName.Text && u.Password == password && u.IsActive == true
                                select new
                                {
                                    u.UserName,
                                    u.Userid,
                                    r.ProfileCode
                                }).FirstOrDefault();
                    if (user != null)
                    {
                        this.Hide();
                        Utility.BA_Commman._UserName = user.UserName.ToUpper();
                        Utility.BA_Commman._userId = user.Userid;
                        Utility.BA_Commman._Profile = user.ProfileCode;
                        ShellComman.ActivityName = ShellERPActivities.LoginSave;
                        ShellComman.ActivityForm = ShellERPActivities.LoginForm;
                        ShellComman.ActivityStatus = "Success";
                        ShellComman.ActivityData = ShellERPActivities.NotAplicable;
                        EmailHandler email = new EmailHandler();
                        email.SendEmailNotification();
                        if (!IsAdmin())
                        {
                            if (!ValidateSerial())
                            {
                                SystemSecurityValidation form = new SystemSecurityValidation();
                                form.ShowDialog();
                                return;
                            }
                        }
                        MDIParentShell obj = new MDIParentShell();
                        obj.ShowDialog();
                        this.Close();

                    }
                    else
                    {
                        MessageBox.Show("Invalid UserName and Password!.");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
                ShellComman.ActivityForm = "Login";
                ShellComman.ActivityData = "Message : " + ex.Message + " Inner Exception :" + ex.InnerException;
                EmailHandler handler = new EmailHandler();
                handler.SendExceptionNotification();
            }
        }
    }
}
