﻿namespace Shell
{
    partial class ConfirmPasswordForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NewPasswordGrp = new System.Windows.Forms.GroupBox();
            this.ConfirmPasswordtxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.NewPaswordTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.UserCodeTxt = new System.Windows.Forms.TextBox();
            this.NewPasswordGrp.SuspendLayout();
            this.SuspendLayout();
            // 
            // NewPasswordGrp
            // 
            this.NewPasswordGrp.Controls.Add(this.UserCodeTxt);
            this.NewPasswordGrp.Controls.Add(this.label3);
            this.NewPasswordGrp.Controls.Add(this.ConfirmPasswordtxt);
            this.NewPasswordGrp.Controls.Add(this.label1);
            this.NewPasswordGrp.Controls.Add(this.confirmBtn);
            this.NewPasswordGrp.Controls.Add(this.ExitBtn);
            this.NewPasswordGrp.Controls.Add(this.NewPaswordTxt);
            this.NewPasswordGrp.Controls.Add(this.label2);
            this.NewPasswordGrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewPasswordGrp.Location = new System.Drawing.Point(65, 32);
            this.NewPasswordGrp.Name = "NewPasswordGrp";
            this.NewPasswordGrp.Size = new System.Drawing.Size(477, 178);
            this.NewPasswordGrp.TabIndex = 18;
            this.NewPasswordGrp.TabStop = false;
            this.NewPasswordGrp.Text = "New Password";
            // 
            // ConfirmPasswordtxt
            // 
            this.ConfirmPasswordtxt.Location = new System.Drawing.Point(173, 93);
            this.ConfirmPasswordtxt.Name = "ConfirmPasswordtxt";
            this.ConfirmPasswordtxt.PasswordChar = '*';
            this.ConfirmPasswordtxt.Size = new System.Drawing.Size(166, 22);
            this.ConfirmPasswordtxt.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Confirm Password";
            // 
            // confirmBtn
            // 
            this.confirmBtn.Location = new System.Drawing.Point(173, 127);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(88, 31);
            this.confirmBtn.TabIndex = 5;
            this.confirmBtn.Text = "Confirm";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.button2_Click);
            // 
            // ExitBtn
            // 
            this.ExitBtn.Location = new System.Drawing.Point(267, 127);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(88, 31);
            this.ExitBtn.TabIndex = 6;
            this.ExitBtn.Text = "Exit";
            this.ExitBtn.UseVisualStyleBackColor = true;
            this.ExitBtn.Click += new System.EventHandler(this.button3_Click);
            // 
            // NewPaswordTxt
            // 
            this.NewPaswordTxt.Location = new System.Drawing.Point(173, 65);
            this.NewPaswordTxt.Name = "NewPaswordTxt";
            this.NewPaswordTxt.PasswordChar = '*';
            this.NewPaswordTxt.Size = new System.Drawing.Size(166, 22);
            this.NewPaswordTxt.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "New Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "User Code";
            // 
            // UserCodeTxt
            // 
            this.UserCodeTxt.Location = new System.Drawing.Point(173, 35);
            this.UserCodeTxt.Name = "UserCodeTxt";
            this.UserCodeTxt.ReadOnly = true;
            this.UserCodeTxt.Size = new System.Drawing.Size(166, 22);
            this.UserCodeTxt.TabIndex = 10;
            // 
            // ConfirmPasswordForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(618, 222);
            this.Controls.Add(this.NewPasswordGrp);
            this.Name = "ConfirmPasswordForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Confirm Password";
            this.Load += new System.EventHandler(this.ConfirmPasswordForm_Load);
            this.NewPasswordGrp.ResumeLayout(false);
            this.NewPasswordGrp.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox NewPasswordGrp;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.TextBox NewPaswordTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ConfirmPasswordtxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UserCodeTxt;
        private System.Windows.Forms.Label label3;
    }
}