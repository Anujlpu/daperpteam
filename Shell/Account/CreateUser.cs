﻿using Framework;
using Framework.Data;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Shell
{
    public partial class CreateUser : BaseForm
    {
        public CreateUser()
        {
            InitializeComponent();
        }

        private void CreateUser_Load(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                binddropdown();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }
        public void BindGrid()
        {
            BAL.BAL_Login _objLogin = new BAL.BAL_Login();
            dgvUserList.DataSource = _objLogin.GetUserlist();
            dgvUserList.Columns[0].Visible = false;
            dgvUserList.Columns[3].Visible = false;
            dgvUserList.Columns[4].Visible = false;
        }

        public void binddropdown()
        {
            BAL.BAL_Login _objLogin = new BAL.BAL_Login();
            var profiles = _objLogin.GetProfilelist().OrderBy(s => s.ProfileCode).ToList();
            ddlProfile.DataSource = profiles;
            ddlProfile.DisplayMember = "ProfileCode";
            ddlProfile.ValueMember = "ProfileId";


        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dg = MessageBox.Show("Do you want to delete selected record ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    if (txtUserCode.Text != "")
                    {
                        BAL.BAL_Login _objLogin = new BAL.BAL_Login();
                        _objLogin.DeleteUser(txtUserCode.Text);
                        ClearValue();
                        BindGrid();
                        txtUserCode.ReadOnly = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtUserCode.Text))
                {
                    MessageBox.Show("Please Enter the User Code");
                    txtUserCode.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(txtUserName.Text))
                {
                    MessageBox.Show("Please Enter the User Name");
                    txtUserName.Focus();
                    return;
                }
                if (string.IsNullOrEmpty(txtPassword.Text))
                {
                    MessageBox.Show("Please Enter the Password");
                    txtPassword.Focus();
                    return;
                }
                if (ddlProfile.SelectedValue.ToString() == "-1")
                {
                    MessageBox.Show("Please Select  the Profile");
                    return;
                }
                BAL.BAL_Login _objLogin = new BAL.BAL_Login();
                int i = 0;
                if (btnSave.Text == "&Edit User")
                {
                    i = _objLogin.UpdattUser(txtUserCode.Text.Trim(), txtUserName.Text.Trim(), Convert.ToInt32(ddlProfile.SelectedValue.ToString()), 1);
                    if (i > 0)
                    {
                        txtUserCode.ReadOnly = false;
                    }
                }
                else
                {
                    int Usercount = _objLogin.GetUserlist().Where(p => p.UserCode.ToUpper() == txtUserCode.Text.ToUpper()).Count();
                    if (Usercount == 0)
                    {
                        i = _objLogin.InsertUser(txtUserCode.Text.Trim(), txtUserName.Text.Trim(), txtPassword.Text.Trim(), Convert.ToInt32(ddlProfile.SelectedValue.ToString()), 1);
                    }
                    else
                    {
                        MessageBox.Show("User code already created in the system, please use different user code.");
                        txtUserCode.Focus();
                        return;
                    }

                }
                if (i == 1)
                {
                    MessageBox.Show("User created/updated successfully.");
                    BindGrid();

                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");
                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void dgvUserList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    txtUserCode.Text = dgvUserList.Rows[e.RowIndex].Cells["UserCode"].Value.ToString();
                    txtUserName.Text = dgvUserList.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
                    ddlProfile.SelectedValue = dgvUserList.Rows[e.RowIndex].Cells["ProfileId"].Value;
                    using (ShellEntities context = new ShellEntities())
                    {
                        var currentPassword = context.MST_UserMaster.FirstOrDefault(u => u.UserCode == txtUserCode.Text);
                        if (currentPassword != null)
                        {
                            txtPassword.Text = Utility.BA_Commman.Decrypt(currentPassword.Password, true);
                        }
                    }
                    txtUserCode.ReadOnly = true;
                    btnSave.Text = "&Edit User";
                    btnDelete.Enabled = true;
                }
                else
                {
                    ClearValue();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void ClearValue()
        {
            try
            {
                txtPassword.Text = "";
                txtUserCode.Text = "";
                txtUserName.Text = "";
                binddropdown();
                BindGrid();
                btnSave.Text = "&Create User";
                btnDelete.Enabled = false;
                txtUserCode.ReadOnly = false;
                txtUserCode.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            this.ClearValue();
        }
    }
}
