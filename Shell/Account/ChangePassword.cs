﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DAL;
using Framework;

namespace Shell
{
    public partial class ChangePassword : BaseForm
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {
            txtUserName.Text = Utility.BA_Commman._UserName;
            txtOldPwd.Focus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                BAL.BAL_Login Obj = new BAL.BAL_Login();
                List<UserEntity> UserEntity = Obj.Login(txtUserName.Text, txtPassword.Text);
                if (UserEntity != null && UserEntity.Count > 0)
                {
                    if (txtPassword.Text != txtConfirm.Text)
                    {
                        MessageBox.Show("Paswword does not match. New and Confirm password must match.");
                        return;
                    }
                   else if (txtOldPwd.Text == txtConfirm.Text)
                    {
                        MessageBox.Show("Old password and new password can not be same.");
                        return;
                    }
                    int Result = Obj.ChangePassword(txtUserName.Text, txtPassword.Text);
                    if (Result == 1)
                    {
                        MessageBox.Show("Password sucessfully changed");
                    }
                    else
                    {
                        MessageBox.Show("There is some issue . Please try later!");
                    }
                }
                else
                {
                    MessageBox.Show("Please enter correct password.");
                    txtOldPwd.Text = "";
                    txtOldPwd.Focus();
                    return;
                }
                txtUserName.Text = "";
                txtPassword.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.Message);
            }
        }
    }
}
