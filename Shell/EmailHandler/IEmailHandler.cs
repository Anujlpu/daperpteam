﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Shell
{
    interface IEmailHandler
    {
        bool SendEmailNotification();
        bool SendExceptionNotification();
        bool EmailNotificationWithAttachments(Attachment attachment, Salary salary);
    }
}
