﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Shell
{
    public class EmailHandler : IEmailHandler
    {
        public bool SendEmailNotification()
        {
            return Email();

        }
        public bool Email()
        {

            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("sahi6006@gmail.com", "Shell ERP");
            mail.To.Add("anujlpu@hotmail.com");
            mail.CC.Add("anujlpu@hotmail.com");
            mail.Bcc.Add("anujlpu@hotmail.com");
            mail.Bcc.Add("sahi6006@gmail.com");
            mail.Bcc.Add("awesome.peeyush@gmail.com");
            mail.Subject = "Notification from Shell_ERP";
            mail.IsBodyHtml = true;
            mail.Body = EmailBody();
            SmtpServer.EnableSsl = true;
            try
            {
                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential("sahi6006@gmail.com", "1050070171");
                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                return false;

            }
        }
        public bool EmailNotificationWithAttachments(Attachment attachment, Salary salary)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("sahi6006@gmail.com", "Shell ERP");
            mail.To.Add("anujlpu@hotmail.com");
            mail.CC.Add("anujlpu@hotmail.com");
            mail.Bcc.Add("anujlpu@hotmail.com");
            mail.Bcc.Add("sahi6006@gmail.com");
            mail.Bcc.Add("awesome.peeyush@gmail.com");
            mail.Subject = "Notification from Shell_ERP";
            mail.IsBodyHtml = true;
            mail.Body = EmailBody();
            SmtpServer.EnableSsl = true;
            try
            {
                mail.Attachments.Add(attachment);
                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential("sahi6006@gmail.com", "1050070171");
                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                mail.Body = ex.Message;
                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential("sahi6006@gmail.com", "1050070171");
                SmtpServer.Send(mail);
                return false;

            }

        }
        public bool SendExceptionNotification()
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("sahi6006@gmail.com", "Shell ERP");
            mail.To.Add("anujlpu@hotmail.com");
            mail.CC.Add("anujlpu@hotmail.com");
            mail.Bcc.Add("anujlpu@hotmail.com");
            mail.Bcc.Add("sahi6006@gmail.com");
            mail.Bcc.Add("awesome.peeyush@gmail.com");
            mail.Subject = "Exception Notification from Shell_ERP";
            mail.IsBodyHtml = true;
            mail.Body = ExceptionBody();
            SmtpServer.EnableSsl = true;
            try
            {
                SmtpServer.Port = 587;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential("sahi6006@gmail.com", "1050070171");
                SmtpServer.Send(mail);
                return true;
            }
            catch (Exception e)
            {
                return false;

            }
        }
        private string ExceptionBody()
        {
            try
            {
                string path = ConfigurationSettings.AppSettings["EmailTemplatePath"];
                string emailTemplate = File.ReadAllText(path);
                emailTemplate = emailTemplate.Replace("@@UserName", Utility.BA_Commman._UserName);
                emailTemplate = emailTemplate.Replace("@@ActivityName", ShellComman.ActivityName);
                emailTemplate = emailTemplate.Replace(" @@ActivityForm", ShellComman.ActivityForm);
                emailTemplate = emailTemplate.Replace(" @@ActivityData", ShellComman.ActivityData);
                emailTemplate = emailTemplate.Replace("@@IPAddress", ShellERPActivities.IPAddress);
                emailTemplate = emailTemplate.Replace("@@HostName", ShellERPActivities.HostName);
                emailTemplate = emailTemplate.Replace("@@ActivityTime", DateTime.Now.ToString("dd-MMM-yyyy hh mm ss"));
                return emailTemplate;
            }

            catch (Exception e)
            {
                return "No template found, Please contact admin." + e.Message;
            }
        }
        private static string EmailBody()
        {
            try
            {
                string path = ConfigurationSettings.AppSettings["EmailTemplatePath"];
                string emailTemplate = File.ReadAllText(path);
                emailTemplate = emailTemplate.Replace("@@UserName", Utility.BA_Commman._UserName);
                emailTemplate = emailTemplate.Replace("@@ActivityName", ShellComman.ActivityName);
                emailTemplate = emailTemplate.Replace(" @@ActivityForm", ShellComman.ActivityForm);
                emailTemplate = emailTemplate.Replace(" @@ActivityData", ShellComman.ActivityData);
                emailTemplate = emailTemplate.Replace("@@IPAddress", ShellERPActivities.IPAddress);
                emailTemplate = emailTemplate.Replace("@@HostName", ShellERPActivities.HostName);
                emailTemplate = emailTemplate.Replace("@@ActivityTime", DateTime.Now.ToString("dd-MMM-yyyy hh mm ss"));
                return emailTemplate;
            }

            catch (Exception e)
            {
                return "No template found, Please contact admin." + e.Message;
            }


        }
    }

    public class Salary
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public System.DateTime SalaryDate { get; set; }
        public Nullable<int> Workingdays { get; set; }
        public Nullable<int> TotalPresentDays { get; set; }
        public Nullable<double> TotalSalary { get; set; }
        public Nullable<double> Deduction { get; set; }
        public Nullable<double> NetSalary { get; set; }
        public string Remarks { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<double> PPHours { get; set; }
        public Nullable<bool> VPay { get; set; }
        public Nullable<double> Bonus { get; set; }
        public Nullable<double> Advance { get; set; }
        public Nullable<double> CPP { get; set; }
        public Nullable<double> EL { get; set; }
        public Nullable<double> Tax { get; set; }
        public Nullable<double> GrossPay { get; set; }
        public Nullable<System.DateTime> FromDate { get; set; }
        public Nullable<System.DateTime> ToDate { get; set; }
        public Nullable<double> VPayTotal { get; set; }
        public Nullable<double> Total_11 { get; set; }
        public Nullable<double> SuprHoursPaid { get; set; }
        public Nullable<double> VacationGrantedHours { get; set; }
        public Nullable<double> VacationTakenHours { get; set; }
        public Nullable<double> StatPay { get; set; }
        public Nullable<double> HourlyRate { get; set; }
        public Nullable<System.DateTime> HourlyRateEffectiveFrom { get; set; }
        public Nullable<System.DateTime> HourlyRateEffectiveTo { get; set; }
        public Nullable<double> PayRollRate { get; set; }
        public Nullable<System.DateTime> PayRollRateEffectiveFrom { get; set; }
        public Nullable<System.DateTime> PayRollRateEffectiveTo { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
    }
}
