﻿using Framework.Data;
using Shell.Master;
using Shell.Report;
using Shell.Transaction;
using System;
using System.Windows.Forms;
using System.Linq;

namespace Shell
{
    public partial class MDIParentShell : Form
    {
        private int childFormNumber = 0;

        public MDIParentShell()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            LocationMaster childForm = new LocationMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private int OpenInvoiceCount()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var inCount = context.Tbl_Invoices.Where(i => i.IsClosed == false && i.IsActive==true).ToList().Count;
                    return inCount;
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
            return 0;
        }

        private int OpenPNLCount()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var inCount = context.Tbl_PNLEntry.Where(i => i.IsClosed == false && i.IsActive == true).ToList().Count;
                    return inCount;
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
            return 0;
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }
        private void MDIParent1_Load(object sender, EventArgs e)
        {
            CountInvoice.Text = OpenInvoiceCount().ToString();
            CountPNL.Text = OpenPNLCount().ToString();
            DashboardGroupBox.Visible = true;
            lblUserName.Text = "Logedin User : " + Utility.BA_Commman._UserName;

            lblUserName.Text = Utility.BA_Commman.titlecase(lblUserName.Text);
            timer1.Start();
            if (Utility.BA_Commman._Profile == "Adm" && Utility.BA_Commman._Profile == "SupAdm")
            {

                createUserToolStripMenuItem.Visible = true;

            }
            else
            { createUserToolStripMenuItem.Visible = false; }
        }

        private void manageStoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            StoreMaster childForm = new StoreMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void manageProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProductMaster childForm = new ProductMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Start();
            timer1.Interval = 50;
            timer1.Enabled = true;
            lblTime.Text = System.DateTime.Now.ToString("hh:mm:ss tt");

        }

        private void gasTransationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGasTrans childForm = new frmGasTrans();
            childForm.MdiParent = this;

            childForm.Show();
        }

        private void uploadSaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //FrmBrandSales childForm = new FrmBrandSales();
            //childForm.MdiParent = this;
            //childForm.Show();
        }

        private void manageEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEmployeeMaster childForm = new FrmEmployeeMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void manageRolesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmRoles childForm = new FrmRoles();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void managePriceToolStripMenuItem_Click(object sender, EventArgs e)
        {

            FrmPriceMaster childForm = new FrmPriceMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void manageMarginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMarginMaster childForm = new FrmMarginMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void vendorManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmVendorMaster childForm = new FrmVendorMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void uploadDocumentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDocumentMove childForm = new FrmDocumentMove();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void lblticket_Click(object sender, EventArgs e)
        {
            FrmViewTicket childForm = new FrmViewTicket();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void paymnetToVendorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPayment childForm = new FrmPayment();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void paymentToEmployeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPaymnetToEmployee childForm = new FrmPaymnetToEmployee(2);
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void paymentToManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPaymnetToEmployee childForm = new FrmPaymnetToEmployee(1);
            childForm.MdiParent = this;
            childForm.Show();

        }

        private void employeeSalaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmSalaryCreation childForm = new FrmSalaryCreation();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void createUserToolStripMenuItem_Click(object sender, EventArgs e)
        {

            CreateUser childForm = new CreateUser();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangePassword childForm = new ChangePassword();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void lnkLogoff_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            DialogResult dgResult = MessageBox.Show("Do you Want Log Off", "", MessageBoxButtons.YesNo);
            if (dgResult == DialogResult.Yes)
            {
                foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcessesByName("Shell.exe"))
                {
                    process.Kill();
                }
                Application.Restart();
            }
            else
            {
                //this.Close();

            }
        }

        private void dashBoardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DashBoard childForm = new DashBoard();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void transactionViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GasTransactionViewForm gasForm = new GasTransactionViewForm();
            gasForm.ShowDialog();
        }

        private void pNLMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PNLType childForm = new PNLType();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void invoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InvoiceEntry childForm = new InvoiceEntry();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void bankEntriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BankEntry childForm = new BankEntry();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void bankValidationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BankValidation childForm = new BankValidation();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void employeeReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeReportForm childForm = new EmployeeReportForm();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BankInfoMaster childForm = new BankInfoMaster();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void pNLEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PNLEntryForm childForm = new PNLEntryForm();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void bankAndPNLValidationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PNLBankValidation childForm = new PNLBankValidation();
            childForm.MdiParent = this;
            childForm.Show();
        }

        private void CountInvoice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            BankValidation childForm = new BankValidation();
            childForm.MdiParent = this;
            childForm.Show();
            DashboardGroupBox.Visible = false;
        }

        private void CountPNL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PNLBankValidation childForm = new PNLBankValidation();
            childForm.MdiParent = this;
            childForm.Show();
            DashboardGroupBox.Visible = false;
        }
    }
}
