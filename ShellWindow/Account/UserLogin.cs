﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using DAL;

namespace Shell
{
    public partial class UserLogin : Form
    {
        public UserLogin()
        {
            InitializeComponent();
        }

        private void UserLogin_Load(object sender, EventArgs e)
        {
            Utility.BA_Commman._ErrorPath = Application.CommonAppDataPath;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //MDIParent1 obj1 = new MDIParent1();
            //obj1.ShowDialog();
            //this.Close();
            //return;
            BAL_Login ObjLogin = new BAL.BAL_Login();
            if (String.IsNullOrEmpty(txtUserName.Text))
            {
                MessageBox.Show("Please Enter User Name");
                return;
            }
            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Please Enter Password");
                return;
            }

            List<UserEntity> UserEntity = ObjLogin.Login(txtUserName.Text, txtPassword.Text);
            if (UserEntity != null && UserEntity.Count > 0)
            {
                this.Hide();
                Utility.BA_Commman._UserName = UserEntity[0].UserName.ToUpper();
                Utility.BA_Commman._userId = UserEntity[0].UserId;
                Utility.BA_Commman._Profile = UserEntity[0].ProfileCode;
                MDIParentShell obj = new MDIParentShell();
                obj.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid UserName and Password!.");
                return;
            }
        }
    }
}
