﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class CreateUser : Form
    {
        public CreateUser()
        {
            InitializeComponent();
        }

        private void CreateUser_Load(object sender, EventArgs e)
        {
            BindGrid(); binddropdown();
        }
        public void BindGrid()
        {
            BAL.BAL_Login _objLogin = new BAL.BAL_Login();
            dgvUserList.DataSource = _objLogin.GetUserlist();
            dgvUserList.Columns[0].Visible = false;
            dgvUserList.Columns[3].Visible = false;
            dgvUserList.Columns[4].Visible = false;


        }

        public void binddropdown()
        {
            BAL.BAL_Login _objLogin = new BAL.BAL_Login();

            ddlProfile.DataSource = _objLogin.GetProfilelist();
            ddlProfile.DisplayMember = "ProfileCode";
            ddlProfile.ValueMember = "ProfileId";


        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (txtUserCode.Text != "")
            {
                BAL.BAL_Login _objLogin = new BAL.BAL_Login();
                _objLogin.DeleteUser(txtUserCode.Text);
                ClearValue();
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUserCode.Text))
            {
                MessageBox.Show("Please Enter the User Code");
                txtUserCode.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                MessageBox.Show("Please Enter the User Name");
                txtUserName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("Please Enter the Password");
                txtPassword.Focus();
                return;
            }
            if (ddlProfile.SelectedValue.ToString() == "-1")
            {
                MessageBox.Show("Please Select  the Profile");
                return;
            }
            BAL.BAL_Login _objLogin = new BAL.BAL_Login();
            int i = 0;
            if (btnSave.Text == "&Edit User")
            {
                i = _objLogin.UpdattUser(txtUserCode.Text.Trim(), txtUserName.Text.Trim(), Convert.ToInt32(ddlProfile.SelectedValue.ToString()), 1);

            }
            else
            {
                int Usercount = _objLogin.GetUserlist().Where(p => p.UserCode.ToUpper() == txtUserCode.Text.ToUpper()).Count();
                if (Usercount == 0)
                {
                    i = _objLogin.InsertUser(txtUserCode.Text.Trim(), txtUserName.Text.Trim(), txtPassword.Text.Trim(), Convert.ToInt32(ddlProfile.SelectedValue.ToString()), 1);
                }
                else
                {
                    MessageBox.Show("User Code Already Exists");
                    txtUserCode.Focus(); return;

                }

            }
            if (i == 1)
            {
                MessageBox.Show("User Created Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        private void dgvUserList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    txtUserCode.Text = dgvUserList.Rows[e.RowIndex].Cells["UserCode"].Value.ToString();
                    txtUserName.Text = dgvUserList.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
                    ddlProfile.SelectedValue = dgvUserList.Rows[e.RowIndex].Cells["ProfileId"].Value;
                    txtPassword.Text = "lll";
                    txtUserCode.ReadOnly = true;
                    btnSave.Text = "&Edit User";
                }
                else
                {
                    ClearValue();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void ClearValue()
        {
            txtPassword.Text = "";
            txtUserCode.Text = "";
            txtUserName.Text = "";
            binddropdown();
            BindGrid();
            btnSave.Text = "&Create User";
        }
    }
}
