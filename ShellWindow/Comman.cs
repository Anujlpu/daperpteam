﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace Shell
{
    public static class ShellComman
    {
        /// <summary>
        /// Set the root dir for salary generate.
        /// </summary>
        /// <returns>dirPath as string</returns>
        public static string SalaryRootDirectory()
        {
            string dirPath = ConfigurationSettings.AppSettings["SalaryGeneratePath"];
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            string currentDate = DateTime.Now.ToString("yyyyMM");
            dirPath = Path.Combine(dirPath, currentDate);
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            return dirPath;
        }
        public static string SalaryTemplatePath { get { return ConfigurationSettings.AppSettings["SalaryTemplatePath"]; } }
        public static string SalaryTemplateLogo { get { return ConfigurationSettings.AppSettings["SalaryTemplateLogo"]; } }
        public static string ReportExelPath
        {
            get
            {
                if (!Directory.Exists(ConfigurationSettings.AppSettings["ReportExelPath"]))
                {
                    Directory.CreateDirectory(ConfigurationSettings.AppSettings["ReportExelPath"]);
                }
                return ConfigurationSettings.AppSettings["ReportExelPath"];
            }
        }


    }
    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }
    }
}
