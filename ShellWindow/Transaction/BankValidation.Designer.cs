﻿namespace Shell
{
    partial class BankValidation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVendorCode = new System.Windows.Forms.TextBox();
            this.grpCashMngtList = new System.Windows.Forms.GroupBox();
            this.dgvInvoice = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvBankentries = new System.Windows.Forms.DataGridView();
            this.label62 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.rbtCredit = new System.Windows.Forms.RadioButton();
            this.rbtDebit = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.ddlVendorName = new System.Windows.Forms.ComboBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.Radiopanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.AdjustmenttextBox = new System.Windows.Forms.TextBox();
            this.DifferencetextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BankCheckbox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Validatebutton = new System.Windows.Forms.Button();
            this.InvoicedateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.InvoiceStatementbutton = new System.Windows.Forms.Button();
            this.grpCashMngtList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoice)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankentries)).BeginInit();
            this.Radiopanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(10, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 38;
            this.label2.Text = "Vendor Name";
            // 
            // txtVendorCode
            // 
            this.txtVendorCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVendorCode.ForeColor = System.Drawing.Color.Black;
            this.txtVendorCode.Location = new System.Drawing.Point(121, 12);
            this.txtVendorCode.MaxLength = 50;
            this.txtVendorCode.Name = "txtVendorCode";
            this.txtVendorCode.Size = new System.Drawing.Size(202, 21);
            this.txtVendorCode.TabIndex = 37;
            this.txtVendorCode.TextChanged += new System.EventHandler(this.txtVendorCode_TextChanged);
            this.txtVendorCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtVendorCode_KeyUp);
            // 
            // grpCashMngtList
            // 
            this.grpCashMngtList.Controls.Add(this.dgvInvoice);
            this.grpCashMngtList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCashMngtList.Location = new System.Drawing.Point(12, 130);
            this.grpCashMngtList.Name = "grpCashMngtList";
            this.grpCashMngtList.Size = new System.Drawing.Size(682, 158);
            this.grpCashMngtList.TabIndex = 51;
            this.grpCashMngtList.TabStop = false;
            this.grpCashMngtList.Text = "Invoices";
            // 
            // dgvInvoice
            // 
            this.dgvInvoice.AllowDrop = true;
            this.dgvInvoice.AllowUserToAddRows = false;
            this.dgvInvoice.AllowUserToDeleteRows = false;
            this.dgvInvoice.AllowUserToResizeColumns = false;
            this.dgvInvoice.AllowUserToResizeRows = false;
            this.dgvInvoice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvInvoice.BackgroundColor = System.Drawing.Color.White;
            this.dgvInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvoice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckBox});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInvoice.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInvoice.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvInvoice.Location = new System.Drawing.Point(3, 16);
            this.dgvInvoice.MultiSelect = false;
            this.dgvInvoice.Name = "dgvInvoice";
            this.dgvInvoice.ReadOnly = true;
            this.dgvInvoice.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvInvoice.RowHeadersVisible = false;
            this.dgvInvoice.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvInvoice.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvInvoice.Size = new System.Drawing.Size(676, 139);
            this.dgvInvoice.TabIndex = 1;
            this.dgvInvoice.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoice_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvBankentries);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 294);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(675, 190);
            this.groupBox1.TabIndex = 200;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bank Entries";
            // 
            // dgvBankentries
            // 
            this.dgvBankentries.AllowDrop = true;
            this.dgvBankentries.AllowUserToAddRows = false;
            this.dgvBankentries.AllowUserToDeleteRows = false;
            this.dgvBankentries.AllowUserToResizeColumns = false;
            this.dgvBankentries.AllowUserToResizeRows = false;
            this.dgvBankentries.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvBankentries.BackgroundColor = System.Drawing.Color.White;
            this.dgvBankentries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBankentries.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BankCheckbox});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBankentries.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBankentries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBankentries.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvBankentries.Location = new System.Drawing.Point(3, 16);
            this.dgvBankentries.MultiSelect = false;
            this.dgvBankentries.Name = "dgvBankentries";
            this.dgvBankentries.ReadOnly = true;
            this.dgvBankentries.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvBankentries.RowHeadersVisible = false;
            this.dgvBankentries.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvBankentries.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvBankentries.Size = new System.Drawing.Size(669, 171);
            this.dgvBankentries.TabIndex = 1;
            this.dgvBankentries.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBankentries_CellContentClick);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(774, 254);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 202;
            this.label62.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(721, 272);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(206, 77);
            this.txtRemarks.TabIndex = 201;
            // 
            // rbtCredit
            // 
            this.rbtCredit.AutoSize = true;
            this.rbtCredit.Location = new System.Drawing.Point(72, 3);
            this.rbtCredit.Name = "rbtCredit";
            this.rbtCredit.Size = new System.Drawing.Size(52, 17);
            this.rbtCredit.TabIndex = 205;
            this.rbtCredit.TabStop = true;
            this.rbtCredit.Text = "Credit";
            this.rbtCredit.UseVisualStyleBackColor = true;
            this.rbtCredit.CheckedChanged += new System.EventHandler(this.rbtCredit_CheckedChanged);
            // 
            // rbtDebit
            // 
            this.rbtDebit.AutoSize = true;
            this.rbtDebit.Location = new System.Drawing.Point(3, 4);
            this.rbtDebit.Name = "rbtDebit";
            this.rbtDebit.Size = new System.Drawing.Size(50, 17);
            this.rbtDebit.TabIndex = 204;
            this.rbtDebit.TabStop = true;
            this.rbtDebit.Text = "Debit";
            this.rbtDebit.UseVisualStyleBackColor = true;
            this.rbtDebit.CheckedChanged += new System.EventHandler(this.rbtCredit_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(10, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 15);
            this.label13.TabIndex = 203;
            this.label13.Text = "Transaction Type";
            // 
            // ddlVendorName
            // 
            this.ddlVendorName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlVendorName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlVendorName.FormattingEnabled = true;
            this.ddlVendorName.Location = new System.Drawing.Point(473, 11);
            this.ddlVendorName.Name = "ddlVendorName";
            this.ddlVendorName.Size = new System.Drawing.Size(213, 22);
            this.ddlVendorName.TabIndex = 206;
            this.ddlVendorName.SelectedIndexChanged += new System.EventHandler(this.ddlVendorName_SelectedIndexChanged);
            this.ddlVendorName.SelectedValueChanged += new System.EventHandler(this.ddlVendorName_SelectedValueChanged);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(827, 366);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 209;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Enabled = false;
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(721, 366);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 207;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // Radiopanel
            // 
            this.Radiopanel.Controls.Add(this.rbtDebit);
            this.Radiopanel.Controls.Add(this.rbtCredit);
            this.Radiopanel.Location = new System.Drawing.Point(121, 39);
            this.Radiopanel.Name = "Radiopanel";
            this.Radiopanel.Size = new System.Drawing.Size(202, 24);
            this.Radiopanel.TabIndex = 210;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(763, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 15);
            this.label1.TabIndex = 212;
            this.label1.Text = "Adjustment";
            // 
            // AdjustmenttextBox
            // 
            this.AdjustmenttextBox.Location = new System.Drawing.Point(749, 109);
            this.AdjustmenttextBox.Name = "AdjustmenttextBox";
            this.AdjustmenttextBox.Size = new System.Drawing.Size(128, 20);
            this.AdjustmenttextBox.TabIndex = 213;
            this.AdjustmenttextBox.Text = "0.00";
            // 
            // DifferencetextBox
            // 
            this.DifferencetextBox.Enabled = false;
            this.DifferencetextBox.Location = new System.Drawing.Point(749, 221);
            this.DifferencetextBox.Name = "DifferencetextBox";
            this.DifferencetextBox.Size = new System.Drawing.Size(128, 20);
            this.DifferencetextBox.TabIndex = 215;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(763, 203);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 15);
            this.label3.TabIndex = 214;
            this.label3.Text = "Difference";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AliceBlue;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(348, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 15);
            this.label4.TabIndex = 216;
            this.label4.Text = "Vendor Code";
            // 
            // CheckBox
            // 
            this.CheckBox.FalseValue = "false";
            this.CheckBox.HeaderText = "";
            this.CheckBox.MinimumWidth = 15;
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.ReadOnly = true;
            this.CheckBox.TrueValue = "true";
            this.CheckBox.Width = 15;
            // 
            // BankCheckbox
            // 
            this.BankCheckbox.HeaderText = "";
            this.BankCheckbox.Name = "BankCheckbox";
            this.BankCheckbox.ReadOnly = true;
            this.BankCheckbox.Width = 5;
            // 
            // Validatebutton
            // 
            this.Validatebutton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Validatebutton.FlatAppearance.BorderSize = 2;
            this.Validatebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Validatebutton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Validatebutton.Location = new System.Drawing.Point(766, 135);
            this.Validatebutton.Name = "Validatebutton";
            this.Validatebutton.Size = new System.Drawing.Size(90, 32);
            this.Validatebutton.TabIndex = 217;
            this.Validatebutton.Text = "Validate";
            this.Validatebutton.UseVisualStyleBackColor = true;
            this.Validatebutton.Click += new System.EventHandler(this.Validatebutton_Click);
            // 
            // InvoicedateTimePicker
            // 
            this.InvoicedateTimePicker.Location = new System.Drawing.Point(473, 42);
            this.InvoicedateTimePicker.Name = "InvoicedateTimePicker";
            this.InvoicedateTimePicker.Size = new System.Drawing.Size(213, 20);
            this.InvoicedateTimePicker.TabIndex = 218;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(348, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 15);
            this.label5.TabIndex = 219;
            this.label5.Text = "Invoice Date";
            // 
            // InvoiceStatementbutton
            // 
            this.InvoiceStatementbutton.Location = new System.Drawing.Point(121, 80);
            this.InvoiceStatementbutton.Name = "InvoiceStatementbutton";
            this.InvoiceStatementbutton.Size = new System.Drawing.Size(85, 26);
            this.InvoiceStatementbutton.TabIndex = 220;
            this.InvoiceStatementbutton.Text = "View";
            this.InvoiceStatementbutton.UseVisualStyleBackColor = true;
            this.InvoiceStatementbutton.Click += new System.EventHandler(this.InvoiceStatementbutton_Click);
            // 
            // BankValidation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(939, 496);
            this.Controls.Add(this.InvoiceStatementbutton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.InvoicedateTimePicker);
            this.Controls.Add(this.Validatebutton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.DifferencetextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AdjustmenttextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Radiopanel);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.ddlVendorName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpCashMngtList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtVendorCode);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "BankValidation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BankValidation";
            this.Load += new System.EventHandler(this.BankValidation_Load);
            this.grpCashMngtList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoice)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankentries)).EndInit();
            this.Radiopanel.ResumeLayout(false);
            this.Radiopanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVendorCode;
        private System.Windows.Forms.GroupBox grpCashMngtList;
        private System.Windows.Forms.DataGridView dgvInvoice;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvBankentries;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.RadioButton rbtCredit;
        private System.Windows.Forms.RadioButton rbtDebit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox ddlVendorName;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Panel Radiopanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AdjustmenttextBox;
        private System.Windows.Forms.TextBox DifferencetextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BankCheckbox;
        private System.Windows.Forms.Button Validatebutton;
        private System.Windows.Forms.DateTimePicker InvoicedateTimePicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button InvoiceStatementbutton;
    }
}