﻿namespace Shell.Transaction
{
    partial class PNLEntryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpManageVendor = new System.Windows.Forms.GroupBox();
            this.OthertextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbtDebit = new System.Windows.Forms.RadioButton();
            this.rbtCredit = new System.Windows.Forms.RadioButton();
            this.dgvPNLDocs = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.ddlPNLCode = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInvoiceBal = new System.Windows.Forms.TextBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.txtInvoiceNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ddlPaymnetType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.grpVendorList = new System.Windows.Forms.GroupBox();
            this.dgvPNLEntryList = new System.Windows.Forms.DataGridView();
            this.checkbox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.PNLEntryDate = new System.Windows.Forms.DateTimePicker();
            this.grpManageVendor.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNLDocs)).BeginInit();
            this.grpVendorList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNLEntryList)).BeginInit();
            this.SuspendLayout();
            // 
            // grpManageVendor
            // 
            this.grpManageVendor.Controls.Add(this.PNLEntryDate);
            this.grpManageVendor.Controls.Add(this.OthertextBox);
            this.grpManageVendor.Controls.Add(this.label4);
            this.grpManageVendor.Controls.Add(this.panel2);
            this.grpManageVendor.Controls.Add(this.dgvPNLDocs);
            this.grpManageVendor.Controls.Add(this.label16);
            this.grpManageVendor.Controls.Add(this.ddlPNLCode);
            this.grpManageVendor.Controls.Add(this.label13);
            this.grpManageVendor.Controls.Add(this.label9);
            this.grpManageVendor.Controls.Add(this.txtInvoiceBal);
            this.grpManageVendor.Controls.Add(this.txtAmount);
            this.grpManageVendor.Controls.Add(this.label6);
            this.grpManageVendor.Controls.Add(this.label62);
            this.grpManageVendor.Controls.Add(this.txtRemarks);
            this.grpManageVendor.Controls.Add(this.txtInvoiceNo);
            this.grpManageVendor.Controls.Add(this.label3);
            this.grpManageVendor.Controls.Add(this.label5);
            this.grpManageVendor.Controls.Add(this.ddlPaymnetType);
            this.grpManageVendor.Controls.Add(this.label1);
            this.grpManageVendor.Controls.Add(this.btnExit);
            this.grpManageVendor.Controls.Add(this.label8);
            this.grpManageVendor.Controls.Add(this.BtnDelete);
            this.grpManageVendor.Controls.Add(this.BtnSave);
            this.grpManageVendor.Controls.Add(this.label2);
            this.grpManageVendor.Controls.Add(this.btnUpdate);
            this.grpManageVendor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageVendor.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageVendor.ForeColor = System.Drawing.Color.Black;
            this.grpManageVendor.Location = new System.Drawing.Point(12, 12);
            this.grpManageVendor.Name = "grpManageVendor";
            this.grpManageVendor.Size = new System.Drawing.Size(918, 239);
            this.grpManageVendor.TabIndex = 1;
            this.grpManageVendor.TabStop = false;
            this.grpManageVendor.Text = "PNL Entry";
            // 
            // OthertextBox
            // 
            this.OthertextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OthertextBox.ForeColor = System.Drawing.Color.Black;
            this.OthertextBox.Location = new System.Drawing.Point(412, 80);
            this.OthertextBox.MaxLength = 10;
            this.OthertextBox.Name = "OthertextBox";
            this.OthertextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.OthertextBox.Size = new System.Drawing.Size(145, 26);
            this.OthertextBox.TabIndex = 203;
            this.OthertextBox.Text = "0.00";
            this.OthertextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.OthertextBox.Leave += new System.EventHandler(this.OthertextBox_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AliceBlue;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(323, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 15);
            this.label4.TabIndex = 204;
            this.label4.Text = "Other";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.rbtDebit);
            this.panel2.Controls.Add(this.rbtCredit);
            this.panel2.Location = new System.Drawing.Point(122, 115);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(172, 26);
            this.panel2.TabIndex = 202;
            // 
            // rbtDebit
            // 
            this.rbtDebit.AutoSize = true;
            this.rbtDebit.Checked = true;
            this.rbtDebit.Location = new System.Drawing.Point(12, 2);
            this.rbtDebit.Name = "rbtDebit";
            this.rbtDebit.Size = new System.Drawing.Size(54, 19);
            this.rbtDebit.TabIndex = 4;
            this.rbtDebit.TabStop = true;
            this.rbtDebit.Text = "Debit";
            this.rbtDebit.UseVisualStyleBackColor = true;
            // 
            // rbtCredit
            // 
            this.rbtCredit.AutoSize = true;
            this.rbtCredit.Location = new System.Drawing.Point(90, 3);
            this.rbtCredit.Name = "rbtCredit";
            this.rbtCredit.Size = new System.Drawing.Size(58, 19);
            this.rbtCredit.TabIndex = 5;
            this.rbtCredit.Text = "Credit";
            this.rbtCredit.UseVisualStyleBackColor = true;
            // 
            // dgvPNLDocs
            // 
            this.dgvPNLDocs.AllowDrop = true;
            this.dgvPNLDocs.AllowUserToAddRows = false;
            this.dgvPNLDocs.AllowUserToDeleteRows = false;
            this.dgvPNLDocs.AllowUserToResizeColumns = false;
            this.dgvPNLDocs.AllowUserToResizeRows = false;
            this.dgvPNLDocs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvPNLDocs.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPNLDocs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvPNLDocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPNLDocs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.checkbox});
            this.dgvPNLDocs.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvPNLDocs.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvPNLDocs.Location = new System.Drawing.Point(601, 35);
            this.dgvPNLDocs.MultiSelect = false;
            this.dgvPNLDocs.Name = "dgvPNLDocs";
            this.dgvPNLDocs.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPNLDocs.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvPNLDocs.RowHeadersVisible = false;
            this.dgvPNLDocs.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPNLDocs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvPNLDocs.Size = new System.Drawing.Size(311, 98);
            this.dgvPNLDocs.TabIndex = 200;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(722, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 15);
            this.label16.TabIndex = 199;
            this.label16.Text = "Select Docs";
            // 
            // ddlPNLCode
            // 
            this.ddlPNLCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPNLCode.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPNLCode.FormattingEnabled = true;
            this.ddlPNLCode.Items.AddRange(new object[] {
            "--Select--",
            "PR",
            "Forign Worker",
            "Student Open Work Permit",
            "Closed Work Permit",
            "Citizen",
            "Work Permit",
            "Tourist",
            "Visitor",
            "Others"});
            this.ddlPNLCode.Location = new System.Drawing.Point(121, 27);
            this.ddlPNLCode.Name = "ddlPNLCode";
            this.ddlPNLCode.Size = new System.Drawing.Size(173, 22);
            this.ddlPNLCode.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(16, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 15);
            this.label13.TabIndex = 195;
            this.label13.Text = "Transaction Type";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.AliceBlue;
            this.label9.Font = new System.Drawing.Font("Arial", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(323, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 15);
            this.label9.TabIndex = 194;
            this.label9.Text = "Total";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtInvoiceBal
            // 
            this.txtInvoiceBal.BackColor = System.Drawing.Color.LightGray;
            this.txtInvoiceBal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceBal.ForeColor = System.Drawing.Color.Black;
            this.txtInvoiceBal.Location = new System.Drawing.Point(412, 119);
            this.txtInvoiceBal.Name = "txtInvoiceBal";
            this.txtInvoiceBal.ReadOnly = true;
            this.txtInvoiceBal.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtInvoiceBal.Size = new System.Drawing.Size(145, 22);
            this.txtInvoiceBal.TabIndex = 191;
            this.txtInvoiceBal.Text = "0.00";
            this.txtInvoiceBal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInvoiceBal.TextChanged += new System.EventHandler(this.txtInvoiceBal_TextChanged);
            // 
            // txtAmount
            // 
            this.txtAmount.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.ForeColor = System.Drawing.Color.Black;
            this.txtAmount.Location = new System.Drawing.Point(412, 48);
            this.txtAmount.MaxLength = 10;
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAmount.Size = new System.Drawing.Size(145, 26);
            this.txtAmount.TabIndex = 8;
            this.txtAmount.Text = "0.00";
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(323, 54);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 15);
            this.label6.TabIndex = 184;
            this.label6.Text = "Total Amount";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(737, 136);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 183;
            this.label62.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(601, 154);
            this.txtRemarks.MaxLength = 100;
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(311, 65);
            this.txtRemarks.TabIndex = 12;
            // 
            // txtInvoiceNo
            // 
            this.txtInvoiceNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInvoiceNo.ForeColor = System.Drawing.Color.Black;
            this.txtInvoiceNo.Location = new System.Drawing.Point(412, 16);
            this.txtInvoiceNo.MaxLength = 40;
            this.txtInvoiceNo.Name = "txtInvoiceNo";
            this.txtInvoiceNo.Size = new System.Drawing.Size(145, 26);
            this.txtInvoiceNo.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(16, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 15);
            this.label3.TabIndex = 176;
            this.label3.Text = "Payment Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(323, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 15);
            this.label5.TabIndex = 172;
            this.label5.Text = "Invoice No";
            // 
            // ddlPaymnetType
            // 
            this.ddlPaymnetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPaymnetType.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlPaymnetType.FormattingEnabled = true;
            this.ddlPaymnetType.Location = new System.Drawing.Point(121, 87);
            this.ddlPaymnetType.Name = "ddlPaymnetType";
            this.ddlPaymnetType.Size = new System.Drawing.Size(173, 22);
            this.ddlPaymnetType.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 15);
            this.label1.TabIndex = 84;
            this.label1.Text = "Entry Date";
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(451, 197);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.AliceBlue;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(300, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 15);
            this.label8.TabIndex = 75;
            this.label8.Text = "*";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(355, 197);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 15;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(165, 197);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 13;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(16, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 36;
            this.label2.Text = "PNL Code";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnUpdate.FlatAppearance.BorderSize = 2;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(259, 197);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(90, 32);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "&Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // grpVendorList
            // 
            this.grpVendorList.Controls.Add(this.dgvPNLEntryList);
            this.grpVendorList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpVendorList.Location = new System.Drawing.Point(12, 259);
            this.grpVendorList.Name = "grpVendorList";
            this.grpVendorList.Size = new System.Drawing.Size(918, 207);
            this.grpVendorList.TabIndex = 49;
            this.grpVendorList.TabStop = false;
            this.grpVendorList.Text = "PNL Entry List";
            // 
            // dgvPNLEntryList
            // 
            this.dgvPNLEntryList.AllowDrop = true;
            this.dgvPNLEntryList.AllowUserToAddRows = false;
            this.dgvPNLEntryList.AllowUserToDeleteRows = false;
            this.dgvPNLEntryList.AllowUserToResizeColumns = false;
            this.dgvPNLEntryList.AllowUserToResizeRows = false;
            this.dgvPNLEntryList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPNLEntryList.BackgroundColor = System.Drawing.Color.White;
            this.dgvPNLEntryList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.NullValue = null;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPNLEntryList.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvPNLEntryList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPNLEntryList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPNLEntryList.Location = new System.Drawing.Point(3, 16);
            this.dgvPNLEntryList.MultiSelect = false;
            this.dgvPNLEntryList.Name = "dgvPNLEntryList";
            this.dgvPNLEntryList.ReadOnly = true;
            this.dgvPNLEntryList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvPNLEntryList.RowHeadersVisible = false;
            this.dgvPNLEntryList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPNLEntryList.Size = new System.Drawing.Size(912, 188);
            this.dgvPNLEntryList.TabIndex = 0;
            this.dgvPNLEntryList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPNLEntryList_CellClick);
            // 
            // checkbox
            // 
            this.checkbox.HeaderText = "";
            this.checkbox.Name = "checkbox";
            this.checkbox.Width = 5;
            // 
            // PNLEntryDate
            // 
            this.PNLEntryDate.Location = new System.Drawing.Point(122, 59);
            this.PNLEntryDate.Name = "PNLEntryDate";
            this.PNLEntryDate.Size = new System.Drawing.Size(172, 21);
            this.PNLEntryDate.TabIndex = 219;
            // 
            // PNLEntryForm
            // 
            this.ClientSize = new System.Drawing.Size(982, 478);
            this.Controls.Add(this.grpVendorList);
            this.Controls.Add(this.grpManageVendor);
            this.Name = "PNLEntryForm";
            this.Load += new System.EventHandler(this.PNLEntryForm_Load);
            this.grpManageVendor.ResumeLayout(false);
            this.grpManageVendor.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNLDocs)).EndInit();
            this.grpVendorList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNLEntryList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpManageVendor;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbtDebit;
        private System.Windows.Forms.RadioButton rbtCredit;
        private System.Windows.Forms.DataGridView dgvPNLDocs;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox ddlPNLCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInvoiceBal;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.TextBox txtInvoiceNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ddlPaymnetType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.GroupBox grpVendorList;
        private System.Windows.Forms.DataGridView dgvPNLEntryList;
        private System.Windows.Forms.TextBox OthertextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checkbox;
        private System.Windows.Forms.DateTimePicker PNLEntryDate;
    }
}
