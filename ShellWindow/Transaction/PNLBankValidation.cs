﻿using BAL;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using Framework;

namespace Shell.Transaction
{
    public partial class PNLBankValidation : Form
    {
        public PNLBankValidation()
        {
            InitializeComponent();
        }
        private void BindPNLDropDown()
        {
            PNLListDropDown.DisplayMember = "PNLCode";
            PNLListDropDown.ValueMember = "PNLID";
            BAL_PNL _ObjPNL = new BAL_PNL();
            PNLListDropDown.DataSource = _ObjPNL.GetPNLlist();

        }
        private void BindPNLEntryGrid()
        {
            DateTime date = Convert.ToDateTime(InvoicedateTimePicker.Text).Add(DateTime.Now.TimeOfDay);
            long pnlId = Convert.ToInt64(PNLListDropDown.SelectedValue);
            using (ShellEntities context = new ShellEntities())
            {
                var docs = (from m in context.Tbl_PNLEntry
                            join p in context.MST_PaymentType on m.PaymentType equals p.PaymnetTypeId
                            join pn in context.MST_PnlMaster on m.PNLID equals pn.PNLId
                            where m.IsActive == true && m.IsClosed == false && m.PNLID == pnlId
                            select new
                            {
                                m.PNLEntryID,
                                m.PNLID,
                                m.TransactionType,
                                m.TotalAmount,
                                m.EntryDate

                            }).ToList();
                dgvPNLList.DataSource = docs.Where(s => Convert.ToDateTime(s.EntryDate).ToString("yyyyMMdd") == date.ToString("yyyyMMdd")).ToList();
            }
        }
        string TranType = "Debit";
        private void BindStatementGridview()
        {
            DateTime date = Convert.ToDateTime(InvoicedateTimePicker.Text).Add(DateTime.Now.TimeOfDay);
            using (ShellEntities context = new ShellEntities())
            {
                var bankStatements = (from b in context.TBL_BankEntries
                                      where b.TransactionType == TranType && b.IsActive == true && b.IsClosed == false
                                      select new
                                      {
                                          b.BankEntriesId,
                                          b.BankName,
                                          b.TransactionType,
                                          b.TotalAmount,
                                          b.StatementDate
                                      }
                                      ).ToList();
                bankStatements = bankStatements.Where(s => Convert.ToDateTime(s.StatementDate).ToString("yyyyMMdd") == date.ToString("yyyyMMdd")).ToList();
                dgvBankentries.DataSource = bankStatements;
            }
        }
        private void PNLBankValidation_Load(object sender, EventArgs e)
        {
            try
            {
                BindPNLDropDown();

            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }

        private void rbtCredit_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtCredit.Checked == true)
                TranType = "Credit";
            else
                TranType = "Debit";
        }
        List<string> invoiceIds = new List<string>();
        List<string> bankIds = new List<string>();
        private void Validatebutton_Click(object sender, EventArgs e)
        {
            try
            {
                double invoiceTotalAmount = 0;
                double bankTotalAmount = 0;
                double diffAmount = 0;
                string invoicetranType = "";
                string ibanktranType = "";
                if (string.IsNullOrEmpty(PNLListDropDown.SelectedValue.ToString()))
                {
                    MessageBox.Show("Please enter vendor name");
                    PNLListDropDown.Focus();
                }
                else if (dgvPNLList.Rows.Count > 0 && dgvBankentries.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in dgvPNLList.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            invoiceIds.Add(row.Cells[1].Value.ToString());
                            invoiceTotalAmount += Convert.ToDouble(row.Cells[4].Value);
                            invoicetranType = row.Cells[3].Value.ToString().Trim();
                        }
                    }

                    foreach (DataGridViewRow row in dgvBankentries.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            bankIds.Add(row.Cells[1].Value.ToString());
                            bankTotalAmount += Convert.ToDouble(row.Cells[4].Value);
                            ibanktranType = row.Cells[3].Value.ToString();
                        }
                    }

                    if ((invoicetranType = ibanktranType.Trim()) == "Debit")
                    {
                        diffAmount = invoiceTotalAmount - bankTotalAmount;
                    }
                    else
                    {
                        diffAmount = bankTotalAmount - invoiceTotalAmount;
                    }
                    if (!string.IsNullOrEmpty(AdjustmenttextBox.Text) && (Convert.ToDouble(AdjustmenttextBox.Text) > 0 || Convert.ToDouble(AdjustmenttextBox.Text) < 0))
                    {
                        diffAmount = (Convert.ToDouble(diffAmount) - Convert.ToDouble(AdjustmenttextBox.Text));
                    }

                    DifferencetextBox.Text = diffAmount.ToString();
                    if (diffAmount == 0)
                    {
                        MessageBox.Show("You can close selected PNL Items...!");
                        BtnSave.Enabled = true;
                    }
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }
        private void CloseInvoiceandStatements()
        {
            DateTime date = Convert.ToDateTime(InvoicedateTimePicker.Text).Add(DateTime.Now.TimeOfDay);
            using (ShellEntities context = new ShellEntities())
            {
                var bankStatements = (from b in context.Tbl_PNLEntry
                                      where b.TransactionType == TranType && b.IsActive == true && b.IsClosed == false
                                      select b
                                      ).ToList();
                bankStatements = bankStatements.Where(s => bankIds.Contains(s.PNLEntryID.ToString())).ToList();

                foreach (var state in bankStatements)
                {
                    state.IsClosed = true;
                }
                context.SaveChanges();
                var invoice = (from b in context.Tbl_Invoices
                               where b.TransactionType == TranType && b.IsActive == true && b.IsClosed == false
                               select b
                                      ).ToList();
                invoice = invoice.Where(s => invoiceIds.Contains(s.InvoiceId.ToString())).ToList();

                foreach (var state in invoice)
                {
                    state.IsClosed = true;
                }
                context.SaveChanges();
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtRemarks.Text))
            {
                MessageBox.Show("Please Enter the Remarks. W/o Remarks it's values not save!");
                return;
            }


            try
            {

                txtRemarks.Text = "";
                //ddlVendorName.Items.Clear();
                CloseInvoiceandStatements();
                BindPNLEntryGrid();
                BindStatementGridview();
                MessageBox.Show("Open PNL Items has been closed successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void InvoiceStatementbutton_Click(object sender, EventArgs e)
        {
            try
            {
                BindStatementGridview();
                BindPNLEntryGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }
        }

        private void dgvPNLList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    this.dgvPNLList.CommitEdit(DataGridViewDataErrorContexts.Commit);

                //Check the value of cell
                if (this.dgvPNLList.CurrentCell.Value == null)
                {
                    //Use index of TimeOut column
                    this.dgvPNLList.Rows[e.RowIndex].Cells[0].Value = true;

                    //Set other columns values
                }
                else
                {
                    //Use index of TimeOut column
                    this.dgvPNLList.Rows[e.RowIndex].Cells[0].Value = false;

                    //Set other columns values
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }

        private void dgvBankentries_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    this.dgvBankentries.CommitEdit(DataGridViewDataErrorContexts.Commit);

                //Check the value of cell
                if (this.dgvBankentries.CurrentCell.Value == null)
                {
                    //Use index of TimeOut column
                    this.dgvBankentries.Rows[e.RowIndex].Cells[0].Value = true;

                    //Set other columns values
                }
                else
                {
                    //Use index of TimeOut column
                    this.dgvBankentries.Rows[e.RowIndex].Cells[0].Value = false;

                    //Set other columns values
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }
    }
}
