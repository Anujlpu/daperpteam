﻿namespace Shell
{
    partial class frmGasTrans
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnView = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCStoreSale = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGST = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDepositLevy = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFuel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCashDriveAway = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtdailystorerntincgst = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtLotowinner = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dtpTransactionDate = new System.Windows.Forms.DateTimePicker();
            this.grpFuel = new System.Windows.Forms.GroupBox();
            this.dgvFuel = new System.Windows.Forms.DataGridView();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.txtbronzeltr = new System.Windows.Forms.TextBox();
            this.txtGasRemarks = new System.Windows.Forms.TextBox();
            this.txtdiffinsettlement = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.txtfinalsettlement = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txttotalsettlement = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtgstpaidonrent = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtdifindrcr = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtgstreceivedfuel = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtfuelcommexgst = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtVpdvdd = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtBSDltr = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtActualShort = new System.Windows.Forms.TextBox();
            this.txtDiff = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txttotalFuel = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCashBankDep = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtAirmiles = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtdieselltr = new System.Windows.Forms.TextBox();
            this.txtvpowerltr = new System.Windows.Forms.TextBox();
            this.txtVppltr = new System.Windows.Forms.TextBox();
            this.txtSilverltr = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.txtTabbRemarks = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.txtCalstoreother = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.txtshelGisftcard = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtgiftcard = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtphonecard = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtscratchlotto = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtonlinelotto = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txttobbaco = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.txtSalTotal = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtActualSettlement = new System.Windows.Forms.TextBox();
            this.txtCardRembsment = new System.Windows.Forms.TextBox();
            this.txtShellCredit = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtShellDebit = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtRadiantcr = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.txtfairshare = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtActualCommission = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtActualBankDeposit = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtleftBankdeposit = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txttotallotopayout = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtCashReceived = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtCashSort = new System.Windows.Forms.TextBox();
            this.txtCashPaidEmp = new System.Windows.Forms.TextBox();
            this.txtGiftcert = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.txtCashpaidmgt = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCashlottto = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtCashPaid = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtRadiantdb = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtcashpayoutStore = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.grpSale = new System.Windows.Forms.GroupBox();
            this.dgvCstore = new System.Windows.Forms.DataGridView();
            this.label17 = new System.Windows.Forms.Label();
            this.txtCStoreRemarks = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.grpCash = new System.Windows.Forms.GroupBox();
            this.dgvCash = new System.Windows.Forms.DataGridView();
            this.label64 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.txtCashRemarks = new System.Windows.Forms.TextBox();
            this.grpSettlement = new System.Windows.Forms.GroupBox();
            this.dgvSettlement = new System.Windows.Forms.DataGridView();
            this.txtradiantcrdiff = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.txtRadiantdbdiff = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.txtShellRemarks = new System.Windows.Forms.TextBox();
            this.grpTabbocco = new System.Windows.Forms.GroupBox();
            this.dgvTabbacco = new System.Windows.Forms.DataGridView();
            this.label63 = new System.Windows.Forms.Label();
            this.btnTicket = new System.Windows.Forms.Button();
            this.btnTicketBack = new System.Windows.Forms.Button();
            this.btnCals = new System.Windows.Forms.Button();
            this.grpFuel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuel)).BeginInit();
            this.grpSale.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCstore)).BeginInit();
            this.grpCash.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCash)).BeginInit();
            this.grpSettlement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlement)).BeginInit();
            this.grpTabbocco.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabbacco)).BeginInit();
            this.SuspendLayout();
            // 
            // btnView
            // 
            this.btnView.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnView.FlatAppearance.BorderSize = 2;
            this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnView.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Location = new System.Drawing.Point(96, 17);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(63, 32);
            this.btnView.TabIndex = 50;
            this.btnView.Text = "&View";
            this.btnView.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnAdd.FlatAppearance.BorderSize = 2;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(10, 17);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(78, 32);
            this.btnAdd.TabIndex = 49;
            this.btnAdd.Text = "&Create Trans";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(165, 17);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(65, 32);
            this.btnExit.TabIndex = 51;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Bronze ltr";
            // 
            // txtCStoreSale
            // 
            this.txtCStoreSale.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCStoreSale.ForeColor = System.Drawing.Color.Black;
            this.txtCStoreSale.Location = new System.Drawing.Point(129, 14);
            this.txtCStoreSale.MaxLength = 10;
            this.txtCStoreSale.Name = "txtCStoreSale";
            this.txtCStoreSale.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCStoreSale.Size = new System.Drawing.Size(154, 22);
            this.txtCStoreSale.TabIndex = 0;
            this.txtCStoreSale.Text = "0";
            this.txtCStoreSale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCStoreSale.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCStoreSale.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(9, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Silver ltr";
            // 
            // txtDiscount
            // 
            this.txtDiscount.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.ForeColor = System.Drawing.Color.Black;
            this.txtDiscount.Location = new System.Drawing.Point(17, 80);
            this.txtDiscount.MaxLength = 12;
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(83, 22);
            this.txtDiscount.TabIndex = 6;
            this.txtDiscount.Text = "0";
            this.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtDiscount.Leave += new System.EventHandler(this.txtCStoreSale_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "VPower Power ltr";
            // 
            // txtGST
            // 
            this.txtGST.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGST.ForeColor = System.Drawing.Color.Black;
            this.txtGST.Location = new System.Drawing.Point(210, 80);
            this.txtGST.MaxLength = 12;
            this.txtGST.Name = "txtGST";
            this.txtGST.Size = new System.Drawing.Size(83, 22);
            this.txtGST.TabIndex = 8;
            this.txtGST.Text = "0";
            this.txtGST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGST.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtGST.Leave += new System.EventHandler(this.txtCStoreSale_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AliceBlue;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(9, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "VPower Diesel ltr";
            // 
            // txtDepositLevy
            // 
            this.txtDepositLevy.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepositLevy.ForeColor = System.Drawing.Color.Black;
            this.txtDepositLevy.Location = new System.Drawing.Point(306, 80);
            this.txtDepositLevy.MaxLength = 12;
            this.txtDepositLevy.Name = "txtDepositLevy";
            this.txtDepositLevy.Size = new System.Drawing.Size(83, 22);
            this.txtDepositLevy.TabIndex = 9;
            this.txtDepositLevy.Text = "0";
            this.txtDepositLevy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDepositLevy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtDepositLevy.Leave += new System.EventHandler(this.txtCStoreSale_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(9, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Diesel ltr";
            // 
            // txtFuel
            // 
            this.txtFuel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuel.ForeColor = System.Drawing.Color.Black;
            this.txtFuel.Location = new System.Drawing.Point(117, 80);
            this.txtFuel.MaxLength = 12;
            this.txtFuel.Name = "txtFuel";
            this.txtFuel.Size = new System.Drawing.Size(83, 22);
            this.txtFuel.TabIndex = 7;
            this.txtFuel.Text = "0";
            this.txtFuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFuel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtFuel.Leave += new System.EventHandler(this.txtCStoreSale_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(285, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 32);
            this.label7.TabIndex = 15;
            this.label7.Text = "Cards\r\nReimbursements";
            // 
            // txtCashDriveAway
            // 
            this.txtCashDriveAway.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashDriveAway.ForeColor = System.Drawing.Color.Black;
            this.txtCashDriveAway.Location = new System.Drawing.Point(680, 174);
            this.txtCashDriveAway.MaxLength = 12;
            this.txtCashDriveAway.Name = "txtCashDriveAway";
            this.txtCashDriveAway.Size = new System.Drawing.Size(147, 22);
            this.txtCashDriveAway.TabIndex = 8;
            this.txtCashDriveAway.Text = "0";
            this.txtCashDriveAway.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashDriveAway.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashDriveAway.Leave += new System.EventHandler(this.txtAirmiles_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(8, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(108, 30);
            this.label10.TabIndex = 21;
            this.label10.Text = "Daily Store\r\nRent including Gst";
            // 
            // txtdailystorerntincgst
            // 
            this.txtdailystorerntincgst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdailystorerntincgst.ForeColor = System.Drawing.Color.Black;
            this.txtdailystorerntincgst.Location = new System.Drawing.Point(133, 60);
            this.txtdailystorerntincgst.MaxLength = 10;
            this.txtdailystorerntincgst.Name = "txtdailystorerntincgst";
            this.txtdailystorerntincgst.Size = new System.Drawing.Size(146, 22);
            this.txtdailystorerntincgst.TabIndex = 6;
            this.txtdailystorerntincgst.Text = "0";
            this.txtdailystorerntincgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtdailystorerntincgst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtdailystorerntincgst.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.AliceBlue;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(284, 90);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 16);
            this.label12.TabIndex = 23;
            this.label12.Text = "Actual Settlement";
            // 
            // txtLotowinner
            // 
            this.txtLotowinner.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLotowinner.ForeColor = System.Drawing.Color.Black;
            this.txtLotowinner.Location = new System.Drawing.Point(116, 202);
            this.txtLotowinner.MaxLength = 12;
            this.txtLotowinner.Name = "txtLotowinner";
            this.txtLotowinner.Size = new System.Drawing.Size(154, 22);
            this.txtLotowinner.TabIndex = 6;
            this.txtLotowinner.Text = "0";
            this.txtLotowinner.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLotowinner.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtLotowinner.Leave += new System.EventHandler(this.txtAirmiles_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.AliceBlue;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(367, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(106, 16);
            this.label15.TabIndex = 31;
            this.label15.Text = "Transaction Date";
            // 
            // dtpTransactionDate
            // 
            this.dtpTransactionDate.CustomFormat = "MMM/dd/yyyy";
            this.dtpTransactionDate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTransactionDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTransactionDate.Location = new System.Drawing.Point(370, 44);
            this.dtpTransactionDate.Name = "dtpTransactionDate";
            this.dtpTransactionDate.Size = new System.Drawing.Size(111, 22);
            this.dtpTransactionDate.TabIndex = 0;
            this.dtpTransactionDate.ValueChanged += new System.EventHandler(this.dtpTransactionDate_ValueChanged);
            // 
            // grpFuel
            // 
            this.grpFuel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpFuel.Controls.Add(this.dgvFuel);
            this.grpFuel.Controls.Add(this.label71);
            this.grpFuel.Controls.Add(this.label72);
            this.grpFuel.Controls.Add(this.txtbronzeltr);
            this.grpFuel.Controls.Add(this.txtGasRemarks);
            this.grpFuel.Controls.Add(this.txtCashDriveAway);
            this.grpFuel.Controls.Add(this.txtLotowinner);
            this.grpFuel.Controls.Add(this.txtdiffinsettlement);
            this.grpFuel.Controls.Add(this.label50);
            this.grpFuel.Controls.Add(this.txtfinalsettlement);
            this.grpFuel.Controls.Add(this.label49);
            this.grpFuel.Controls.Add(this.txttotalsettlement);
            this.grpFuel.Controls.Add(this.label47);
            this.grpFuel.Controls.Add(this.txtgstpaidonrent);
            this.grpFuel.Controls.Add(this.label46);
            this.grpFuel.Controls.Add(this.txtdifindrcr);
            this.grpFuel.Controls.Add(this.label45);
            this.grpFuel.Controls.Add(this.textBox23);
            this.grpFuel.Controls.Add(this.label6);
            this.grpFuel.Controls.Add(this.txtgstreceivedfuel);
            this.grpFuel.Controls.Add(this.label43);
            this.grpFuel.Controls.Add(this.txtfuelcommexgst);
            this.grpFuel.Controls.Add(this.label42);
            this.grpFuel.Controls.Add(this.txtVpdvdd);
            this.grpFuel.Controls.Add(this.label41);
            this.grpFuel.Controls.Add(this.txtBSDltr);
            this.grpFuel.Controls.Add(this.label40);
            this.grpFuel.Controls.Add(this.label13);
            this.grpFuel.Controls.Add(this.txtActualShort);
            this.grpFuel.Controls.Add(this.txtDiff);
            this.grpFuel.Controls.Add(this.label11);
            this.grpFuel.Controls.Add(this.txttotalFuel);
            this.grpFuel.Controls.Add(this.label29);
            this.grpFuel.Controls.Add(this.label25);
            this.grpFuel.Controls.Add(this.txtCashBankDep);
            this.grpFuel.Controls.Add(this.label27);
            this.grpFuel.Controls.Add(this.label28);
            this.grpFuel.Controls.Add(this.txtAirmiles);
            this.grpFuel.Controls.Add(this.label18);
            this.grpFuel.Controls.Add(this.txtdieselltr);
            this.grpFuel.Controls.Add(this.txtvpowerltr);
            this.grpFuel.Controls.Add(this.txtVppltr);
            this.grpFuel.Controls.Add(this.txtSilverltr);
            this.grpFuel.Controls.Add(this.label5);
            this.grpFuel.Controls.Add(this.label4);
            this.grpFuel.Controls.Add(this.label3);
            this.grpFuel.Controls.Add(this.label2);
            this.grpFuel.Controls.Add(this.label1);
            this.grpFuel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.grpFuel.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpFuel.ForeColor = System.Drawing.Color.Black;
            this.grpFuel.Location = new System.Drawing.Point(9, 373);
            this.grpFuel.Name = "grpFuel";
            this.grpFuel.Size = new System.Drawing.Size(1164, 237);
            this.grpFuel.TabIndex = 3;
            this.grpFuel.TabStop = false;
            this.grpFuel.Text = "Fuel Transaction";
            // 
            // dgvFuel
            // 
            this.dgvFuel.AllowDrop = true;
            this.dgvFuel.AllowUserToAddRows = false;
            this.dgvFuel.AllowUserToDeleteRows = false;
            this.dgvFuel.AllowUserToResizeColumns = false;
            this.dgvFuel.AllowUserToResizeRows = false;
            this.dgvFuel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvFuel.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFuel.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvFuel.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvFuel.Location = new System.Drawing.Point(927, 22);
            this.dgvFuel.MultiSelect = false;
            this.dgvFuel.Name = "dgvFuel";
            this.dgvFuel.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuel.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFuel.RowHeadersVisible = false;
            this.dgvFuel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvFuel.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvFuel.Size = new System.Drawing.Size(222, 106);
            this.dgvFuel.TabIndex = 170;
            this.dgvFuel.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvFuel.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(847, 146);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(58, 15);
            this.label71.TabIndex = 164;
            this.label71.Text = "Remarks";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(832, 26);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(73, 15);
            this.label72.TabIndex = 169;
            this.label72.Text = "Select Docs";
            // 
            // txtbronzeltr
            // 
            this.txtbronzeltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbronzeltr.ForeColor = System.Drawing.Color.Black;
            this.txtbronzeltr.Location = new System.Drawing.Point(116, 22);
            this.txtbronzeltr.MaxLength = 12;
            this.txtbronzeltr.Name = "txtbronzeltr";
            this.txtbronzeltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtbronzeltr.Size = new System.Drawing.Size(154, 22);
            this.txtbronzeltr.TabIndex = 0;
            this.txtbronzeltr.Text = "0";
            this.txtbronzeltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtbronzeltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtbronzeltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtGasRemarks
            // 
            this.txtGasRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGasRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtGasRemarks.Location = new System.Drawing.Point(927, 140);
            this.txtGasRemarks.MaxLength = 150;
            this.txtGasRemarks.Multiline = true;
            this.txtGasRemarks.Name = "txtGasRemarks";
            this.txtGasRemarks.Size = new System.Drawing.Size(196, 59);
            this.txtGasRemarks.TabIndex = 9;
            // 
            // txtdiffinsettlement
            // 
            this.txtdiffinsettlement.BackColor = System.Drawing.Color.LightGray;
            this.txtdiffinsettlement.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdiffinsettlement.ForeColor = System.Drawing.Color.Black;
            this.txtdiffinsettlement.Location = new System.Drawing.Point(448, 720);
            this.txtdiffinsettlement.Name = "txtdiffinsettlement";
            this.txtdiffinsettlement.Size = new System.Drawing.Size(146, 22);
            this.txtdiffinsettlement.TabIndex = 125;
            this.txtdiffinsettlement.Text = "0";
            this.txtdiffinsettlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.AliceBlue;
            this.label50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(331, 724);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(107, 16);
            this.label50.TabIndex = 124;
            this.label50.Text = "Diff in Settlement";
            // 
            // txtfinalsettlement
            // 
            this.txtfinalsettlement.BackColor = System.Drawing.Color.LightGray;
            this.txtfinalsettlement.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfinalsettlement.ForeColor = System.Drawing.Color.Black;
            this.txtfinalsettlement.Location = new System.Drawing.Point(680, 106);
            this.txtfinalsettlement.Name = "txtfinalsettlement";
            this.txtfinalsettlement.Size = new System.Drawing.Size(147, 22);
            this.txtfinalsettlement.TabIndex = 39;
            this.txtfinalsettlement.Text = "0";
            this.txtfinalsettlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(566, 110);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(99, 15);
            this.label49.TabIndex = 122;
            this.label49.Text = "Final Settlement ";
            // 
            // txttotalsettlement
            // 
            this.txttotalsettlement.BackColor = System.Drawing.Color.LightGray;
            this.txttotalsettlement.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalsettlement.ForeColor = System.Drawing.Color.Black;
            this.txttotalsettlement.Location = new System.Drawing.Point(680, 78);
            this.txttotalsettlement.Name = "txttotalsettlement";
            this.txttotalsettlement.Size = new System.Drawing.Size(147, 22);
            this.txttotalsettlement.TabIndex = 37;
            this.txttotalsettlement.Text = "0";
            this.txttotalsettlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(566, 82);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(98, 15);
            this.label47.TabIndex = 118;
            this.label47.Text = "Total Settlement ";
            // 
            // txtgstpaidonrent
            // 
            this.txtgstpaidonrent.BackColor = System.Drawing.Color.LightGray;
            this.txtgstpaidonrent.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgstpaidonrent.ForeColor = System.Drawing.Color.Black;
            this.txtgstpaidonrent.Location = new System.Drawing.Point(680, 50);
            this.txtgstpaidonrent.Name = "txtgstpaidonrent";
            this.txtgstpaidonrent.Size = new System.Drawing.Size(146, 22);
            this.txtgstpaidonrent.TabIndex = 36;
            this.txtgstpaidonrent.Text = "0";
            this.txtgstpaidonrent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(566, 40);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(78, 30);
            this.label46.TabIndex = 116;
            this.label46.Text = "Gst\r\nPaid on Rent";
            // 
            // txtdifindrcr
            // 
            this.txtdifindrcr.BackColor = System.Drawing.Color.Silver;
            this.txtdifindrcr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdifindrcr.ForeColor = System.Drawing.Color.Black;
            this.txtdifindrcr.Location = new System.Drawing.Point(680, 22);
            this.txtdifindrcr.Name = "txtdifindrcr";
            this.txtdifindrcr.ReadOnly = true;
            this.txtdifindrcr.Size = new System.Drawing.Size(146, 22);
            this.txtdifindrcr.TabIndex = 34;
            this.txtdifindrcr.Text = "0";
            this.txtdifindrcr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(566, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(110, 15);
            this.label45.TabIndex = 114;
            this.label45.Text = "Diff in debit / Credit";
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.Gray;
            this.textBox23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.ForeColor = System.Drawing.Color.Black;
            this.textBox23.Location = new System.Drawing.Point(144, 744);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(155, 22);
            this.textBox23.TabIndex = 113;
            this.textBox23.Text = "0";
            this.textBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(15, 744);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 32);
            this.label6.TabIndex = 112;
            this.label6.Text = "Diff Fuel\r\nCommission";
            // 
            // txtgstreceivedfuel
            // 
            this.txtgstreceivedfuel.BackColor = System.Drawing.Color.LightGray;
            this.txtgstreceivedfuel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgstreceivedfuel.ForeColor = System.Drawing.Color.Black;
            this.txtgstreceivedfuel.Location = new System.Drawing.Point(396, 132);
            this.txtgstreceivedfuel.Name = "txtgstreceivedfuel";
            this.txtgstreceivedfuel.Size = new System.Drawing.Size(155, 22);
            this.txtgstreceivedfuel.TabIndex = 109;
            this.txtgstreceivedfuel.Text = "0";
            this.txtgstreceivedfuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(281, 134);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(106, 30);
            this.label43.TabIndex = 108;
            this.label43.Text = "Gst received on\r\nFuel Commission";
            // 
            // txtfuelcommexgst
            // 
            this.txtfuelcommexgst.BackColor = System.Drawing.Color.LightGray;
            this.txtfuelcommexgst.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfuelcommexgst.ForeColor = System.Drawing.Color.Black;
            this.txtfuelcommexgst.Location = new System.Drawing.Point(396, 95);
            this.txtfuelcommexgst.Name = "txtfuelcommexgst";
            this.txtfuelcommexgst.Size = new System.Drawing.Size(155, 22);
            this.txtfuelcommexgst.TabIndex = 107;
            this.txtfuelcommexgst.Text = "0";
            this.txtfuelcommexgst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(281, 89);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(109, 30);
            this.label42.TabIndex = 106;
            this.label42.Text = "Fuel Commission \r\nExcluding Gst";
            // 
            // txtVpdvdd
            // 
            this.txtVpdvdd.BackColor = System.Drawing.Color.LightGray;
            this.txtVpdvdd.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVpdvdd.ForeColor = System.Drawing.Color.Black;
            this.txtVpdvdd.Location = new System.Drawing.Point(396, 58);
            this.txtVpdvdd.Name = "txtVpdvdd";
            this.txtVpdvdd.ReadOnly = true;
            this.txtVpdvdd.Size = new System.Drawing.Size(154, 22);
            this.txtVpdvdd.TabIndex = 105;
            this.txtVpdvdd.Text = "0";
            this.txtVpdvdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(281, 52);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(97, 30);
            this.label41.TabIndex = 104;
            this.label41.Text = "Total  (VPD+VPP\r\nin ltr";
            // 
            // txtBSDltr
            // 
            this.txtBSDltr.BackColor = System.Drawing.Color.LightGray;
            this.txtBSDltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBSDltr.ForeColor = System.Drawing.Color.Black;
            this.txtBSDltr.Location = new System.Drawing.Point(396, 21);
            this.txtBSDltr.Name = "txtBSDltr";
            this.txtBSDltr.ReadOnly = true;
            this.txtBSDltr.Size = new System.Drawing.Size(154, 22);
            this.txtBSDltr.TabIndex = 103;
            this.txtBSDltr.Text = "0";
            this.txtBSDltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(281, 24);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(112, 15);
            this.label40.TabIndex = 102;
            this.label40.Text = "Total  (B+S+D) in ltr";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(726, 213);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 16);
            this.label13.TabIndex = 81;
            this.label13.Text = "Actual Short";
            // 
            // txtActualShort
            // 
            this.txtActualShort.BackColor = System.Drawing.Color.LightGray;
            this.txtActualShort.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualShort.ForeColor = System.Drawing.Color.Black;
            this.txtActualShort.Location = new System.Drawing.Point(806, 209);
            this.txtActualShort.Name = "txtActualShort";
            this.txtActualShort.ReadOnly = true;
            this.txtActualShort.Size = new System.Drawing.Size(65, 22);
            this.txtActualShort.TabIndex = 80;
            this.txtActualShort.Text = "0";
            // 
            // txtDiff
            // 
            this.txtDiff.BackColor = System.Drawing.Color.LightGray;
            this.txtDiff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiff.ForeColor = System.Drawing.Color.Black;
            this.txtDiff.Location = new System.Drawing.Point(655, 209);
            this.txtDiff.Name = "txtDiff";
            this.txtDiff.ReadOnly = true;
            this.txtDiff.Size = new System.Drawing.Size(65, 22);
            this.txtDiff.TabIndex = 79;
            this.txtDiff.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(566, 212);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 15);
            this.label11.TabIndex = 78;
            this.label11.Text = "Diff";
            // 
            // txttotalFuel
            // 
            this.txttotalFuel.BackColor = System.Drawing.Color.LightGray;
            this.txttotalFuel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalFuel.ForeColor = System.Drawing.Color.Black;
            this.txttotalFuel.Location = new System.Drawing.Point(396, 206);
            this.txttotalFuel.Name = "txttotalFuel";
            this.txttotalFuel.ReadOnly = true;
            this.txttotalFuel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txttotalFuel.Size = new System.Drawing.Size(154, 22);
            this.txttotalFuel.TabIndex = 75;
            this.txttotalFuel.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(281, 209);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(33, 15);
            this.label29.TabIndex = 74;
            this.label29.Text = "Total";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(9, 196);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(76, 30);
            this.label25.TabIndex = 72;
            this.label25.Text = "Lotto Winner\r\n+ Gift Card ";
            // 
            // txtCashBankDep
            // 
            this.txtCashBankDep.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashBankDep.ForeColor = System.Drawing.Color.Black;
            this.txtCashBankDep.Location = new System.Drawing.Point(396, 169);
            this.txtCashBankDep.MaxLength = 12;
            this.txtCashBankDep.Name = "txtCashBankDep";
            this.txtCashBankDep.Size = new System.Drawing.Size(154, 22);
            this.txtCashBankDep.TabIndex = 7;
            this.txtCashBankDep.Text = "0";
            this.txtCashBankDep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashBankDep.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashBankDep.Leave += new System.EventHandler(this.txtAirmiles_Leave);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(281, 169);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(81, 30);
            this.label27.TabIndex = 68;
            this.label27.Text = "Cash for\r\nBank Deposit";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(566, 166);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 30);
            this.label28.TabIndex = 66;
            this.label28.Text = "Cash\r\n(Drive Away)";
            // 
            // txtAirmiles
            // 
            this.txtAirmiles.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAirmiles.ForeColor = System.Drawing.Color.Black;
            this.txtAirmiles.Location = new System.Drawing.Point(116, 172);
            this.txtAirmiles.MaxLength = 12;
            this.txtAirmiles.Name = "txtAirmiles";
            this.txtAirmiles.Size = new System.Drawing.Size(154, 22);
            this.txtAirmiles.TabIndex = 5;
            this.txtAirmiles.Text = "0";
            this.txtAirmiles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAirmiles.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtAirmiles.Leave += new System.EventHandler(this.txtAirmiles_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(11, 167);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 15);
            this.label18.TabIndex = 62;
            this.label18.Text = "AirMilesCash ";
            // 
            // txtdieselltr
            // 
            this.txtdieselltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdieselltr.ForeColor = System.Drawing.Color.Black;
            this.txtdieselltr.Location = new System.Drawing.Point(116, 82);
            this.txtdieselltr.MaxLength = 12;
            this.txtdieselltr.Name = "txtdieselltr";
            this.txtdieselltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtdieselltr.Size = new System.Drawing.Size(154, 22);
            this.txtdieselltr.TabIndex = 2;
            this.txtdieselltr.Text = "0";
            this.txtdieselltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtdieselltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtdieselltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtvpowerltr
            // 
            this.txtvpowerltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvpowerltr.ForeColor = System.Drawing.Color.Black;
            this.txtvpowerltr.Location = new System.Drawing.Point(116, 112);
            this.txtvpowerltr.MaxLength = 12;
            this.txtvpowerltr.Name = "txtvpowerltr";
            this.txtvpowerltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtvpowerltr.Size = new System.Drawing.Size(154, 22);
            this.txtvpowerltr.TabIndex = 3;
            this.txtvpowerltr.Text = "0";
            this.txtvpowerltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtvpowerltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtvpowerltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtVppltr
            // 
            this.txtVppltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVppltr.ForeColor = System.Drawing.Color.Black;
            this.txtVppltr.Location = new System.Drawing.Point(116, 142);
            this.txtVppltr.MaxLength = 12;
            this.txtVppltr.Name = "txtVppltr";
            this.txtVppltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtVppltr.Size = new System.Drawing.Size(154, 22);
            this.txtVppltr.TabIndex = 4;
            this.txtVppltr.Text = "0";
            this.txtVppltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVppltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtVppltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // txtSilverltr
            // 
            this.txtSilverltr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSilverltr.ForeColor = System.Drawing.Color.Black;
            this.txtSilverltr.Location = new System.Drawing.Point(116, 52);
            this.txtSilverltr.MaxLength = 12;
            this.txtSilverltr.Name = "txtSilverltr";
            this.txtSilverltr.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtSilverltr.Size = new System.Drawing.Size(154, 22);
            this.txtSilverltr.TabIndex = 1;
            this.txtSilverltr.Text = "0";
            this.txtSilverltr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSilverltr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtSilverltr.Leave += new System.EventHandler(this.FuelKey_Leave);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(705, 16);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 60;
            this.label62.Text = "Remarks";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(101, 42);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(10, 12);
            this.label60.TabIndex = 147;
            this.label60.Text = "+";
            // 
            // txtTabbRemarks
            // 
            this.txtTabbRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTabbRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtTabbRemarks.Location = new System.Drawing.Point(694, 31);
            this.txtTabbRemarks.MaxLength = 150;
            this.txtTabbRemarks.Multiline = true;
            this.txtTabbRemarks.Name = "txtTabbRemarks";
            this.txtTabbRemarks.Size = new System.Drawing.Size(172, 71);
            this.txtTabbRemarks.TabIndex = 10;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(295, 41);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(10, 12);
            this.label59.TabIndex = 146;
            this.label59.Text = "+";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(392, 41);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(10, 12);
            this.label58.TabIndex = 145;
            this.label58.Text = "+";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(489, 41);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(10, 12);
            this.label57.TabIndex = 144;
            this.label57.Text = "+";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(586, 41);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(10, 12);
            this.label56.TabIndex = 143;
            this.label56.Text = "+";
            // 
            // txtCalstoreother
            // 
            this.txtCalstoreother.BackColor = System.Drawing.Color.LightGray;
            this.txtCalstoreother.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalstoreother.ForeColor = System.Drawing.Color.Black;
            this.txtCalstoreother.Location = new System.Drawing.Point(599, 36);
            this.txtCalstoreother.Name = "txtCalstoreother";
            this.txtCalstoreother.ReadOnly = true;
            this.txtCalstoreother.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCalstoreother.Size = new System.Drawing.Size(81, 22);
            this.txtCalstoreother.TabIndex = 142;
            this.txtCalstoreother.Text = "0";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(608, 17);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(89, 16);
            this.label55.TabIndex = 141;
            this.label55.Text = "In Store Other";
            // 
            // txtshelGisftcard
            // 
            this.txtshelGisftcard.BackColor = System.Drawing.Color.White;
            this.txtshelGisftcard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtshelGisftcard.ForeColor = System.Drawing.Color.Black;
            this.txtshelGisftcard.Location = new System.Drawing.Point(502, 36);
            this.txtshelGisftcard.MaxLength = 10;
            this.txtshelGisftcard.Name = "txtshelGisftcard";
            this.txtshelGisftcard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtshelGisftcard.Size = new System.Drawing.Size(81, 22);
            this.txtshelGisftcard.TabIndex = 5;
            this.txtshelGisftcard.Text = "0";
            this.txtshelGisftcard.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtshelGisftcard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtshelGisftcard.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(503, 17);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(99, 16);
            this.label54.TabIndex = 139;
            this.label54.Text = "Shell Gift Cards";
            // 
            // txtgiftcard
            // 
            this.txtgiftcard.BackColor = System.Drawing.Color.White;
            this.txtgiftcard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgiftcard.ForeColor = System.Drawing.Color.Black;
            this.txtgiftcard.Location = new System.Drawing.Point(405, 36);
            this.txtgiftcard.MaxLength = 10;
            this.txtgiftcard.Name = "txtgiftcard";
            this.txtgiftcard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtgiftcard.Size = new System.Drawing.Size(81, 22);
            this.txtgiftcard.TabIndex = 4;
            this.txtgiftcard.Text = "0";
            this.txtgiftcard.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtgiftcard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtgiftcard.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(417, 17);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(66, 16);
            this.label53.TabIndex = 137;
            this.label53.Text = "Gift Cards";
            // 
            // txtphonecard
            // 
            this.txtphonecard.BackColor = System.Drawing.Color.White;
            this.txtphonecard.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphonecard.ForeColor = System.Drawing.Color.Black;
            this.txtphonecard.Location = new System.Drawing.Point(308, 36);
            this.txtphonecard.MaxLength = 10;
            this.txtphonecard.Name = "txtphonecard";
            this.txtphonecard.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtphonecard.Size = new System.Drawing.Size(81, 22);
            this.txtphonecard.TabIndex = 3;
            this.txtphonecard.Text = "0";
            this.txtphonecard.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtphonecard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtphonecard.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(310, 17);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(83, 16);
            this.label52.TabIndex = 135;
            this.label52.Text = "Phone Cards";
            // 
            // txtscratchlotto
            // 
            this.txtscratchlotto.BackColor = System.Drawing.Color.White;
            this.txtscratchlotto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtscratchlotto.ForeColor = System.Drawing.Color.Black;
            this.txtscratchlotto.Location = new System.Drawing.Point(211, 36);
            this.txtscratchlotto.MaxLength = 10;
            this.txtscratchlotto.Name = "txtscratchlotto";
            this.txtscratchlotto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtscratchlotto.Size = new System.Drawing.Size(81, 22);
            this.txtscratchlotto.TabIndex = 2;
            this.txtscratchlotto.Text = "0";
            this.txtscratchlotto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtscratchlotto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtscratchlotto.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(212, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 16);
            this.label8.TabIndex = 133;
            this.label8.Text = "Scratch Lotto";
            // 
            // txtonlinelotto
            // 
            this.txtonlinelotto.BackColor = System.Drawing.Color.White;
            this.txtonlinelotto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtonlinelotto.ForeColor = System.Drawing.Color.Black;
            this.txtonlinelotto.Location = new System.Drawing.Point(114, 36);
            this.txtonlinelotto.MaxLength = 10;
            this.txtonlinelotto.Name = "txtonlinelotto";
            this.txtonlinelotto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtonlinelotto.Size = new System.Drawing.Size(81, 22);
            this.txtonlinelotto.TabIndex = 1;
            this.txtonlinelotto.Text = "0";
            this.txtonlinelotto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtonlinelotto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtonlinelotto.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(115, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 15);
            this.label9.TabIndex = 131;
            this.label9.Text = "Online Lotto";
            // 
            // txttobbaco
            // 
            this.txttobbaco.BackColor = System.Drawing.Color.White;
            this.txttobbaco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttobbaco.ForeColor = System.Drawing.Color.Black;
            this.txttobbaco.Location = new System.Drawing.Point(17, 36);
            this.txttobbaco.MaxLength = 10;
            this.txttobbaco.Name = "txttobbaco";
            this.txttobbaco.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txttobbaco.Size = new System.Drawing.Size(81, 22);
            this.txttobbaco.TabIndex = 0;
            this.txttobbaco.Text = "0";
            this.txttobbaco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txttobbaco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txttobbaco.Leave += new System.EventHandler(this.Key_Leave);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(18, 18);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(56, 16);
            this.label51.TabIndex = 129;
            this.label51.Text = "Tobbaco";
            // 
            // txtSalTotal
            // 
            this.txtSalTotal.BackColor = System.Drawing.Color.LightGray;
            this.txtSalTotal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalTotal.ForeColor = System.Drawing.Color.Black;
            this.txtSalTotal.Location = new System.Drawing.Point(407, 80);
            this.txtSalTotal.Name = "txtSalTotal";
            this.txtSalTotal.ReadOnly = true;
            this.txtSalTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSalTotal.Size = new System.Drawing.Size(154, 22);
            this.txtSalTotal.TabIndex = 65;
            this.txtSalTotal.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(407, 59);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 16);
            this.label24.TabIndex = 64;
            this.label24.Text = "Total";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(117, 62);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 16);
            this.label19.TabIndex = 60;
            this.label19.Text = "Fuel";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(300, 59);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 16);
            this.label20.TabIndex = 58;
            this.label20.Text = "Deposit  Levy";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(213, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 16);
            this.label21.TabIndex = 56;
            this.label21.Text = "GST";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(17, 62);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(94, 16);
            this.label22.TabIndex = 54;
            this.label22.Text = "Discounts (-ve)";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(198, 42);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(10, 12);
            this.label61.TabIndex = 148;
            this.label61.Text = "+";
            // 
            // txtActualSettlement
            // 
            this.txtActualSettlement.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualSettlement.ForeColor = System.Drawing.Color.Black;
            this.txtActualSettlement.Location = new System.Drawing.Point(401, 87);
            this.txtActualSettlement.MaxLength = 10;
            this.txtActualSettlement.Name = "txtActualSettlement";
            this.txtActualSettlement.Size = new System.Drawing.Size(154, 22);
            this.txtActualSettlement.TabIndex = 10;
            this.txtActualSettlement.Text = "0";
            this.txtActualSettlement.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActualSettlement.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtActualSettlement.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // txtCardRembsment
            // 
            this.txtCardRembsment.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCardRembsment.ForeColor = System.Drawing.Color.Black;
            this.txtCardRembsment.Location = new System.Drawing.Point(401, 60);
            this.txtCardRembsment.MaxLength = 10;
            this.txtCardRembsment.Name = "txtCardRembsment";
            this.txtCardRembsment.Size = new System.Drawing.Size(154, 22);
            this.txtCardRembsment.TabIndex = 7;
            this.txtCardRembsment.Text = "0";
            this.txtCardRembsment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCardRembsment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCardRembsment.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // txtShellCredit
            // 
            this.txtShellCredit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShellCredit.ForeColor = System.Drawing.Color.Black;
            this.txtShellCredit.Location = new System.Drawing.Point(608, 20);
            this.txtShellCredit.MaxLength = 10;
            this.txtShellCredit.Name = "txtShellCredit";
            this.txtShellCredit.Size = new System.Drawing.Size(91, 22);
            this.txtShellCredit.TabIndex = 3;
            this.txtShellCredit.Text = "0";
            this.txtShellCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShellCredit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtShellCredit.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(533, 23);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(75, 16);
            this.label68.TabIndex = 158;
            this.label68.Text = "Shell Credit";
            // 
            // txtShellDebit
            // 
            this.txtShellDebit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShellDebit.ForeColor = System.Drawing.Color.Black;
            this.txtShellDebit.Location = new System.Drawing.Point(440, 20);
            this.txtShellDebit.MaxLength = 10;
            this.txtShellDebit.Name = "txtShellDebit";
            this.txtShellDebit.Size = new System.Drawing.Size(91, 22);
            this.txtShellDebit.TabIndex = 2;
            this.txtShellDebit.Text = "0";
            this.txtShellDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtShellDebit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtShellDebit.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(371, 23);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(71, 16);
            this.label69.TabIndex = 156;
            this.label69.Text = "Shell Debit";
            // 
            // txtRadiantcr
            // 
            this.txtRadiantcr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRadiantcr.ForeColor = System.Drawing.Color.Black;
            this.txtRadiantcr.Location = new System.Drawing.Point(277, 20);
            this.txtRadiantcr.MaxLength = 10;
            this.txtRadiantcr.Name = "txtRadiantcr";
            this.txtRadiantcr.Size = new System.Drawing.Size(91, 22);
            this.txtRadiantcr.TabIndex = 1;
            this.txtRadiantcr.Text = "0";
            this.txtRadiantcr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRadiantcr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtRadiantcr.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Arial", 9F);
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(187, 24);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(86, 15);
            this.label67.TabIndex = 154;
            this.label67.Text = "Radiant Credit";
            // 
            // txtfairshare
            // 
            this.txtfairshare.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfairshare.ForeColor = System.Drawing.Color.Black;
            this.txtfairshare.Location = new System.Drawing.Point(627, 69);
            this.txtfairshare.MaxLength = 10;
            this.txtfairshare.Name = "txtfairshare";
            this.txtfairshare.Size = new System.Drawing.Size(118, 22);
            this.txtfairshare.TabIndex = 8;
            this.txtfairshare.Text = "0";
            this.txtfairshare.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtfairshare.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtfairshare.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(558, 72);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(68, 16);
            this.label48.TabIndex = 120;
            this.label48.Text = "Fair Share";
            // 
            // txtActualCommission
            // 
            this.txtActualCommission.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualCommission.ForeColor = System.Drawing.Color.Black;
            this.txtActualCommission.Location = new System.Drawing.Point(132, 85);
            this.txtActualCommission.MaxLength = 10;
            this.txtActualCommission.Name = "txtActualCommission";
            this.txtActualCommission.Size = new System.Drawing.Size(147, 22);
            this.txtActualCommission.TabIndex = 9;
            this.txtActualCommission.Text = "0";
            this.txtActualCommission.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActualCommission.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtActualCommission.Leave += new System.EventHandler(this.txtCardRembsment_Leave);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9F);
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(5, 87);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(112, 15);
            this.label44.TabIndex = 110;
            this.label44.Text = "ActualCommission";
            // 
            // txtActualBankDeposit
            // 
            this.txtActualBankDeposit.BackColor = System.Drawing.Color.LightGray;
            this.txtActualBankDeposit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualBankDeposit.ForeColor = System.Drawing.Color.Black;
            this.txtActualBankDeposit.Location = new System.Drawing.Point(791, 84);
            this.txtActualBankDeposit.Name = "txtActualBankDeposit";
            this.txtActualBankDeposit.ReadOnly = true;
            this.txtActualBankDeposit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtActualBankDeposit.Size = new System.Drawing.Size(102, 22);
            this.txtActualBankDeposit.TabIndex = 101;
            this.txtActualBankDeposit.Text = "0";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(648, 87);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(127, 16);
            this.label39.TabIndex = 100;
            this.label39.Text = "Actual Bank Deposit";
            // 
            // txtleftBankdeposit
            // 
            this.txtleftBankdeposit.BackColor = System.Drawing.Color.LightGray;
            this.txtleftBankdeposit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtleftBankdeposit.ForeColor = System.Drawing.Color.Black;
            this.txtleftBankdeposit.Location = new System.Drawing.Point(791, 56);
            this.txtleftBankdeposit.Name = "txtleftBankdeposit";
            this.txtleftBankdeposit.ReadOnly = true;
            this.txtleftBankdeposit.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtleftBankdeposit.Size = new System.Drawing.Size(102, 22);
            this.txtleftBankdeposit.TabIndex = 99;
            this.txtleftBankdeposit.Text = "0";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(648, 59);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(141, 15);
            this.label38.TabIndex = 98;
            this.label38.Text = "Bal left for Bank Deposit ";
            // 
            // txttotallotopayout
            // 
            this.txttotallotopayout.BackColor = System.Drawing.Color.LightGray;
            this.txttotallotopayout.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotallotopayout.ForeColor = System.Drawing.Color.Black;
            this.txttotallotopayout.Location = new System.Drawing.Point(791, 29);
            this.txttotallotopayout.Name = "txttotallotopayout";
            this.txttotallotopayout.ReadOnly = true;
            this.txttotallotopayout.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txttotallotopayout.Size = new System.Drawing.Size(102, 22);
            this.txttotallotopayout.TabIndex = 97;
            this.txttotallotopayout.Text = "0";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(648, 31);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(111, 16);
            this.label37.TabIndex = 96;
            this.label37.Text = "Total Lotto payout";
            // 
            // txtCashReceived
            // 
            this.txtCashReceived.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashReceived.ForeColor = System.Drawing.Color.Black;
            this.txtCashReceived.Location = new System.Drawing.Point(489, 114);
            this.txtCashReceived.MaxLength = 12;
            this.txtCashReceived.Name = "txtCashReceived";
            this.txtCashReceived.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashReceived.Size = new System.Drawing.Size(155, 22);
            this.txtCashReceived.TabIndex = 7;
            this.txtCashReceived.Text = "0";
            this.txtCashReceived.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashReceived.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashReceived.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9F);
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(297, 116);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(91, 15);
            this.label36.TabIndex = 94;
            this.label36.Text = "Cash Received";
            // 
            // txtCashSort
            // 
            this.txtCashSort.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashSort.ForeColor = System.Drawing.Color.Black;
            this.txtCashSort.Location = new System.Drawing.Point(490, 58);
            this.txtCashSort.MaxLength = 12;
            this.txtCashSort.Name = "txtCashSort";
            this.txtCashSort.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashSort.Size = new System.Drawing.Size(154, 22);
            this.txtCashSort.TabIndex = 5;
            this.txtCashSort.Text = "0";
            this.txtCashSort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashSort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashSort.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // txtCashPaidEmp
            // 
            this.txtCashPaidEmp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashPaidEmp.ForeColor = System.Drawing.Color.Black;
            this.txtCashPaidEmp.Location = new System.Drawing.Point(131, 112);
            this.txtCashPaidEmp.MaxLength = 12;
            this.txtCashPaidEmp.Name = "txtCashPaidEmp";
            this.txtCashPaidEmp.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashPaidEmp.Size = new System.Drawing.Size(155, 22);
            this.txtCashPaidEmp.TabIndex = 3;
            this.txtCashPaidEmp.Text = "0";
            this.txtCashPaidEmp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashPaidEmp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashPaidEmp.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // txtGiftcert
            // 
            this.txtGiftcert.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiftcert.ForeColor = System.Drawing.Color.Black;
            this.txtGiftcert.Location = new System.Drawing.Point(489, 86);
            this.txtGiftcert.MaxLength = 12;
            this.txtGiftcert.Name = "txtGiftcert";
            this.txtGiftcert.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtGiftcert.Size = new System.Drawing.Size(155, 22);
            this.txtGiftcert.TabIndex = 6;
            this.txtGiftcert.Text = "0";
            this.txtGiftcert.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtGiftcert.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(291, 87);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(137, 15);
            this.label14.TabIndex = 90;
            this.label14.Text = "Gift Certificate US dollar";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9F);
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(291, 59);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(167, 15);
            this.label31.TabIndex = 89;
            this.label31.Text = "Cash Short/ added to change";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(9, 117);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(108, 30);
            this.label32.TabIndex = 88;
            this.label32.Text = "Cash Paid to third \r\nparties/employee";
            // 
            // txtCashpaidmgt
            // 
            this.txtCashpaidmgt.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashpaidmgt.ForeColor = System.Drawing.Color.Black;
            this.txtCashpaidmgt.Location = new System.Drawing.Point(490, 30);
            this.txtCashpaidmgt.MaxLength = 12;
            this.txtCashpaidmgt.Name = "txtCashpaidmgt";
            this.txtCashpaidmgt.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashpaidmgt.Size = new System.Drawing.Size(154, 22);
            this.txtCashpaidmgt.TabIndex = 4;
            this.txtCashpaidmgt.Text = "0";
            this.txtCashpaidmgt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashpaidmgt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashpaidmgt.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9F);
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(291, 31);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(193, 15);
            this.label33.TabIndex = 86;
            this.label33.Text = "Cash Paid to Mngt (Deepak/Ricky)";
            // 
            // txtCashlottto
            // 
            this.txtCashlottto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashlottto.ForeColor = System.Drawing.Color.Black;
            this.txtCashlottto.Location = new System.Drawing.Point(131, 56);
            this.txtCashlottto.MaxLength = 12;
            this.txtCashlottto.Name = "txtCashlottto";
            this.txtCashlottto.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashlottto.Size = new System.Drawing.Size(154, 22);
            this.txtCashlottto.TabIndex = 1;
            this.txtCashlottto.Text = "0";
            this.txtCashlottto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashlottto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashlottto.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(9, 56);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(110, 15);
            this.label34.TabIndex = 84;
            this.label34.Text = "Cash Lotto Payout ";
            // 
            // txtCashPaid
            // 
            this.txtCashPaid.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashPaid.ForeColor = System.Drawing.Color.Black;
            this.txtCashPaid.Location = new System.Drawing.Point(131, 84);
            this.txtCashPaid.MaxLength = 12;
            this.txtCashPaid.Name = "txtCashPaid";
            this.txtCashPaid.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCashPaid.Size = new System.Drawing.Size(154, 22);
            this.txtCashPaid.TabIndex = 2;
            this.txtCashPaid.Text = "0";
            this.txtCashPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCashPaid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtCashPaid.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(9, 78);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(99, 30);
            this.label35.TabIndex = 82;
            this.label35.Text = "Cash Paid for \r\nStore Purchases";
            // 
            // txtRadiantdb
            // 
            this.txtRadiantdb.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRadiantdb.ForeColor = System.Drawing.Color.Black;
            this.txtRadiantdb.Location = new System.Drawing.Point(93, 21);
            this.txtRadiantdb.MaxLength = 10;
            this.txtRadiantdb.Name = "txtRadiantdb";
            this.txtRadiantdb.Size = new System.Drawing.Size(91, 22);
            this.txtRadiantdb.TabIndex = 0;
            this.txtRadiantdb.Text = "0";
            this.txtRadiantdb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRadiantdb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtRadiantdb.Leave += new System.EventHandler(this.txtRadiantdb_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9F);
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(6, 23);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(82, 15);
            this.label30.TabIndex = 76;
            this.label30.Text = "Radiant Debit";
            // 
            // txtcashpayoutStore
            // 
            this.txtcashpayoutStore.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcashpayoutStore.ForeColor = System.Drawing.Color.Black;
            this.txtcashpayoutStore.Location = new System.Drawing.Point(131, 31);
            this.txtcashpayoutStore.MaxLength = 12;
            this.txtcashpayoutStore.Name = "txtcashpayoutStore";
            this.txtcashpayoutStore.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtcashpayoutStore.Size = new System.Drawing.Size(154, 22);
            this.txtcashpayoutStore.TabIndex = 0;
            this.txtcashpayoutStore.Text = "0";
            this.txtcashpayoutStore.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcashpayoutStore.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPress_t);
            this.txtcashpayoutStore.Leave += new System.EventHandler(this.txtcashpayoutStore_Leave);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(10, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(116, 30);
            this.label26.TabIndex = 70;
            this.label26.Text = "Cash Payout \r\nfor Store Purchases";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(29, 18);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(80, 15);
            this.label23.TabIndex = 52;
            this.label23.Text = "C-Store Sale ";
            // 
            // grpSale
            // 
            this.grpSale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSale.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpSale.BackColor = System.Drawing.Color.AliceBlue;
            this.grpSale.Controls.Add(this.dgvCstore);
            this.grpSale.Controls.Add(this.label17);
            this.grpSale.Controls.Add(this.txtCStoreRemarks);
            this.grpSale.Controls.Add(this.label16);
            this.grpSale.Controls.Add(this.label23);
            this.grpSale.Controls.Add(this.txtCStoreSale);
            this.grpSale.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.grpSale.Location = new System.Drawing.Point(487, -1);
            this.grpSale.Name = "grpSale";
            this.grpSale.Size = new System.Drawing.Size(686, 103);
            this.grpSale.TabIndex = 0;
            this.grpSale.TabStop = false;
            this.grpSale.Text = "Sale";
            // 
            // dgvCstore
            // 
            this.dgvCstore.AllowDrop = true;
            this.dgvCstore.AllowUserToAddRows = false;
            this.dgvCstore.AllowUserToDeleteRows = false;
            this.dgvCstore.AllowUserToResizeColumns = false;
            this.dgvCstore.AllowUserToResizeRows = false;
            this.dgvCstore.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCstore.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCstore.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCstore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCstore.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvCstore.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvCstore.Location = new System.Drawing.Point(449, 13);
            this.dgvCstore.MultiSelect = false;
            this.dgvCstore.Name = "dgvCstore";
            this.dgvCstore.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCstore.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCstore.RowHeadersVisible = false;
            this.dgvCstore.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvCstore.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvCstore.Size = new System.Drawing.Size(222, 83);
            this.dgvCstore.TabIndex = 57;
            this.dgvCstore.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvCstore.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(29, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 15);
            this.label17.TabIndex = 56;
            this.label17.Text = "Remarks";
            // 
            // txtCStoreRemarks
            // 
            this.txtCStoreRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCStoreRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtCStoreRemarks.Location = new System.Drawing.Point(129, 42);
            this.txtCStoreRemarks.MaxLength = 150;
            this.txtCStoreRemarks.Multiline = true;
            this.txtCStoreRemarks.Name = "txtCStoreRemarks";
            this.txtCStoreRemarks.Size = new System.Drawing.Size(173, 57);
            this.txtCStoreRemarks.TabIndex = 1;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(354, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 15);
            this.label16.TabIndex = 53;
            this.label16.Text = "Select Docs";
            // 
            // grpCash
            // 
            this.grpCash.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpCash.Controls.Add(this.dgvCash);
            this.grpCash.Controls.Add(this.label64);
            this.grpCash.Controls.Add(this.label26);
            this.grpCash.Controls.Add(this.txtcashpayoutStore);
            this.grpCash.Controls.Add(this.label74);
            this.grpCash.Controls.Add(this.txtCashlottto);
            this.grpCash.Controls.Add(this.txtCashRemarks);
            this.grpCash.Controls.Add(this.label34);
            this.grpCash.Controls.Add(this.txtCashPaid);
            this.grpCash.Controls.Add(this.label35);
            this.grpCash.Controls.Add(this.txtCashPaidEmp);
            this.grpCash.Controls.Add(this.label32);
            this.grpCash.Controls.Add(this.txtCashpaidmgt);
            this.grpCash.Controls.Add(this.label33);
            this.grpCash.Controls.Add(this.txtCashSort);
            this.grpCash.Controls.Add(this.label31);
            this.grpCash.Controls.Add(this.txtGiftcert);
            this.grpCash.Controls.Add(this.label14);
            this.grpCash.Controls.Add(this.label36);
            this.grpCash.Controls.Add(this.txtCashReceived);
            this.grpCash.Controls.Add(this.txttotallotopayout);
            this.grpCash.Controls.Add(this.label37);
            this.grpCash.Controls.Add(this.label38);
            this.grpCash.Controls.Add(this.txtleftBankdeposit);
            this.grpCash.Controls.Add(this.label39);
            this.grpCash.Controls.Add(this.txtActualBankDeposit);
            this.grpCash.Location = new System.Drawing.Point(10, 218);
            this.grpCash.Name = "grpCash";
            this.grpCash.Size = new System.Drawing.Size(1162, 156);
            this.grpCash.TabIndex = 2;
            this.grpCash.TabStop = false;
            this.grpCash.Text = "Cash";
            // 
            // dgvCash
            // 
            this.dgvCash.AllowDrop = true;
            this.dgvCash.AllowUserToAddRows = false;
            this.dgvCash.AllowUserToDeleteRows = false;
            this.dgvCash.AllowUserToResizeColumns = false;
            this.dgvCash.AllowUserToResizeRows = false;
            this.dgvCash.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvCash.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCash.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCash.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCash.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvCash.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvCash.Location = new System.Drawing.Point(926, 31);
            this.dgvCash.MultiSelect = false;
            this.dgvCash.Name = "dgvCash";
            this.dgvCash.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCash.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvCash.RowHeadersVisible = false;
            this.dgvCash.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvCash.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvCash.Size = new System.Drawing.Size(222, 105);
            this.dgvCash.TabIndex = 169;
            this.dgvCash.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvCash.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(911, 10);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(73, 15);
            this.label64.TabIndex = 149;
            this.label64.Text = "Select Docs";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Black;
            this.label74.Location = new System.Drawing.Point(648, 118);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(58, 15);
            this.label74.TabIndex = 168;
            this.label74.Text = "Remarks";
            // 
            // txtCashRemarks
            // 
            this.txtCashRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtCashRemarks.Location = new System.Drawing.Point(706, 110);
            this.txtCashRemarks.MaxLength = 150;
            this.txtCashRemarks.Multiline = true;
            this.txtCashRemarks.Name = "txtCashRemarks";
            this.txtCashRemarks.Size = new System.Drawing.Size(198, 42);
            this.txtCashRemarks.TabIndex = 8;
            // 
            // grpSettlement
            // 
            this.grpSettlement.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSettlement.Controls.Add(this.dgvSettlement);
            this.grpSettlement.Controls.Add(this.txtradiantcrdiff);
            this.grpSettlement.Controls.Add(this.label73);
            this.grpSettlement.Controls.Add(this.label66);
            this.grpSettlement.Controls.Add(this.txtRadiantdbdiff);
            this.grpSettlement.Controls.Add(this.label70);
            this.grpSettlement.Controls.Add(this.label65);
            this.grpSettlement.Controls.Add(this.txtShellRemarks);
            this.grpSettlement.Controls.Add(this.txtRadiantdb);
            this.grpSettlement.Controls.Add(this.label30);
            this.grpSettlement.Controls.Add(this.txtShellCredit);
            this.grpSettlement.Controls.Add(this.label68);
            this.grpSettlement.Controls.Add(this.txtRadiantcr);
            this.grpSettlement.Controls.Add(this.label67);
            this.grpSettlement.Controls.Add(this.txtShellDebit);
            this.grpSettlement.Controls.Add(this.label69);
            this.grpSettlement.Controls.Add(this.txtActualCommission);
            this.grpSettlement.Controls.Add(this.label44);
            this.grpSettlement.Controls.Add(this.label7);
            this.grpSettlement.Controls.Add(this.txtdailystorerntincgst);
            this.grpSettlement.Controls.Add(this.label10);
            this.grpSettlement.Controls.Add(this.label12);
            this.grpSettlement.Controls.Add(this.txtfairshare);
            this.grpSettlement.Controls.Add(this.label48);
            this.grpSettlement.Controls.Add(this.txtCardRembsment);
            this.grpSettlement.Controls.Add(this.txtActualSettlement);
            this.grpSettlement.Location = new System.Drawing.Point(10, 613);
            this.grpSettlement.Name = "grpSettlement";
            this.grpSettlement.Size = new System.Drawing.Size(1163, 127);
            this.grpSettlement.TabIndex = 4;
            this.grpSettlement.TabStop = false;
            this.grpSettlement.Text = "Settlement";
            // 
            // dgvSettlement
            // 
            this.dgvSettlement.AllowDrop = true;
            this.dgvSettlement.AllowUserToAddRows = false;
            this.dgvSettlement.AllowUserToDeleteRows = false;
            this.dgvSettlement.AllowUserToResizeColumns = false;
            this.dgvSettlement.AllowUserToResizeRows = false;
            this.dgvSettlement.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvSettlement.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSettlement.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvSettlement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSettlement.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvSettlement.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvSettlement.Location = new System.Drawing.Point(926, 33);
            this.dgvSettlement.MultiSelect = false;
            this.dgvSettlement.Name = "dgvSettlement";
            this.dgvSettlement.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvSettlement.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvSettlement.RowHeadersVisible = false;
            this.dgvSettlement.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvSettlement.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvSettlement.Size = new System.Drawing.Size(222, 83);
            this.dgvSettlement.TabIndex = 173;
            this.dgvSettlement.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvSettlement.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // txtradiantcrdiff
            // 
            this.txtradiantcrdiff.BackColor = System.Drawing.Color.LightGray;
            this.txtradiantcrdiff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtradiantcrdiff.ForeColor = System.Drawing.Color.Black;
            this.txtradiantcrdiff.Location = new System.Drawing.Point(770, 46);
            this.txtradiantcrdiff.Name = "txtradiantcrdiff";
            this.txtradiantcrdiff.ReadOnly = true;
            this.txtradiantcrdiff.Size = new System.Drawing.Size(91, 22);
            this.txtradiantcrdiff.TabIndex = 5;
            this.txtradiantcrdiff.Text = "0";
            this.txtradiantcrdiff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(923, 16);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(73, 15);
            this.label73.TabIndex = 172;
            this.label73.Text = "Select Docs";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(704, 50);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(64, 16);
            this.label66.TabIndex = 162;
            this.label66.Text = "Credit Diff";
            // 
            // txtRadiantdbdiff
            // 
            this.txtRadiantdbdiff.BackColor = System.Drawing.Color.LightGray;
            this.txtRadiantdbdiff.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRadiantdbdiff.ForeColor = System.Drawing.Color.Black;
            this.txtRadiantdbdiff.Location = new System.Drawing.Point(770, 18);
            this.txtRadiantdbdiff.Name = "txtRadiantdbdiff";
            this.txtRadiantdbdiff.ReadOnly = true;
            this.txtRadiantdbdiff.Size = new System.Drawing.Size(91, 22);
            this.txtRadiantdbdiff.TabIndex = 4;
            this.txtRadiantdbdiff.Text = "0";
            this.txtRadiantdbdiff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(702, 22);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(60, 16);
            this.label70.TabIndex = 161;
            this.label70.Text = "Debit Diff";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(701, 94);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(58, 15);
            this.label65.TabIndex = 150;
            this.label65.Text = "Remarks";
            // 
            // txtShellRemarks
            // 
            this.txtShellRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtShellRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtShellRemarks.Location = new System.Drawing.Point(771, 85);
            this.txtShellRemarks.MaxLength = 150;
            this.txtShellRemarks.Multiline = true;
            this.txtShellRemarks.Name = "txtShellRemarks";
            this.txtShellRemarks.Size = new System.Drawing.Size(150, 34);
            this.txtShellRemarks.TabIndex = 11;
            // 
            // grpTabbocco
            // 
            this.grpTabbocco.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTabbocco.Controls.Add(this.dgvTabbacco);
            this.grpTabbocco.Controls.Add(this.label62);
            this.grpTabbocco.Controls.Add(this.label63);
            this.grpTabbocco.Controls.Add(this.txtTabbRemarks);
            this.grpTabbocco.Controls.Add(this.label60);
            this.grpTabbocco.Controls.Add(this.label61);
            this.grpTabbocco.Controls.Add(this.txtDiscount);
            this.grpTabbocco.Controls.Add(this.txtGST);
            this.grpTabbocco.Controls.Add(this.label59);
            this.grpTabbocco.Controls.Add(this.txtDepositLevy);
            this.grpTabbocco.Controls.Add(this.txtFuel);
            this.grpTabbocco.Controls.Add(this.label58);
            this.grpTabbocco.Controls.Add(this.label22);
            this.grpTabbocco.Controls.Add(this.label21);
            this.grpTabbocco.Controls.Add(this.label57);
            this.grpTabbocco.Controls.Add(this.label20);
            this.grpTabbocco.Controls.Add(this.label56);
            this.grpTabbocco.Controls.Add(this.label19);
            this.grpTabbocco.Controls.Add(this.txtCalstoreother);
            this.grpTabbocco.Controls.Add(this.label24);
            this.grpTabbocco.Controls.Add(this.label55);
            this.grpTabbocco.Controls.Add(this.txtSalTotal);
            this.grpTabbocco.Controls.Add(this.txtshelGisftcard);
            this.grpTabbocco.Controls.Add(this.label51);
            this.grpTabbocco.Controls.Add(this.label54);
            this.grpTabbocco.Controls.Add(this.txttobbaco);
            this.grpTabbocco.Controls.Add(this.txtgiftcard);
            this.grpTabbocco.Controls.Add(this.label9);
            this.grpTabbocco.Controls.Add(this.txtonlinelotto);
            this.grpTabbocco.Controls.Add(this.label53);
            this.grpTabbocco.Controls.Add(this.label8);
            this.grpTabbocco.Controls.Add(this.txtscratchlotto);
            this.grpTabbocco.Controls.Add(this.txtphonecard);
            this.grpTabbocco.Controls.Add(this.label52);
            this.grpTabbocco.Location = new System.Drawing.Point(9, 101);
            this.grpTabbocco.Name = "grpTabbocco";
            this.grpTabbocco.Size = new System.Drawing.Size(1164, 115);
            this.grpTabbocco.TabIndex = 1;
            this.grpTabbocco.TabStop = false;
            this.grpTabbocco.Text = "Tabbacco";
            // 
            // dgvTabbacco
            // 
            this.dgvTabbacco.AllowDrop = true;
            this.dgvTabbacco.AllowUserToAddRows = false;
            this.dgvTabbacco.AllowUserToDeleteRows = false;
            this.dgvTabbacco.AllowUserToResizeColumns = false;
            this.dgvTabbacco.AllowUserToResizeRows = false;
            this.dgvTabbacco.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvTabbacco.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTabbacco.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvTabbacco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTabbacco.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.dgvTabbacco.GridColor = System.Drawing.SystemColors.Desktop;
            this.dgvTabbacco.Location = new System.Drawing.Point(927, 16);
            this.dgvTabbacco.MultiSelect = false;
            this.dgvTabbacco.Name = "dgvTabbacco";
            this.dgvTabbacco.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTabbacco.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvTabbacco.RowHeadersVisible = false;
            this.dgvTabbacco.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvTabbacco.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvTabbacco.Size = new System.Drawing.Size(222, 83);
            this.dgvTabbacco.TabIndex = 149;
            this.dgvTabbacco.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellClick);
            this.dgvTabbacco.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCstore_CellContentClick);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(849, 13);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(73, 15);
            this.label63.TabIndex = 67;
            this.label63.Text = "Select Docs";
            // 
            // btnTicket
            // 
            this.btnTicket.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnTicket.FlatAppearance.BorderSize = 2;
            this.btnTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTicket.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTicket.Location = new System.Drawing.Point(165, 55);
            this.btnTicket.Name = "btnTicket";
            this.btnTicket.Size = new System.Drawing.Size(154, 32);
            this.btnTicket.TabIndex = 52;
            this.btnTicket.Text = "Ticket Move Forward";
            this.btnTicket.UseVisualStyleBackColor = true;
            this.btnTicket.Click += new System.EventHandler(this.btnTicket_Click);
            // 
            // btnTicketBack
            // 
            this.btnTicketBack.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnTicketBack.FlatAppearance.BorderSize = 2;
            this.btnTicketBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTicketBack.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTicketBack.Location = new System.Drawing.Point(10, 55);
            this.btnTicketBack.Name = "btnTicketBack";
            this.btnTicketBack.Size = new System.Drawing.Size(133, 32);
            this.btnTicketBack.TabIndex = 53;
            this.btnTicketBack.Text = "Ticket Move Back";
            this.btnTicketBack.UseVisualStyleBackColor = true;
            this.btnTicketBack.Click += new System.EventHandler(this.btnTicketBack_Click);
            // 
            // btnCals
            // 
            this.btnCals.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnCals.FlatAppearance.BorderSize = 2;
            this.btnCals.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCals.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCals.Location = new System.Drawing.Point(236, 17);
            this.btnCals.Name = "btnCals";
            this.btnCals.Size = new System.Drawing.Size(78, 32);
            this.btnCals.TabIndex = 54;
            this.btnCals.Text = "&Calculate";
            this.btnCals.UseVisualStyleBackColor = true;
            this.btnCals.Click += new System.EventHandler(this.btnCals_Click);
            // 
            // frmGasTrans
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1207, 733);
            this.Controls.Add(this.btnCals);
            this.Controls.Add(this.btnTicketBack);
            this.Controls.Add(this.btnTicket);
            this.Controls.Add(this.grpTabbocco);
            this.Controls.Add(this.dtpTransactionDate);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.grpSettlement);
            this.Controls.Add(this.grpSale);
            this.Controls.Add(this.grpCash);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.grpFuel);
            this.Controls.Add(this.btnExit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmGasTrans";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gas Transaction";
            this.Load += new System.EventHandler(this.frmGasTrans_Load);
            this.grpFuel.ResumeLayout(false);
            this.grpFuel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuel)).EndInit();
            this.grpSale.ResumeLayout(false);
            this.grpSale.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCstore)).EndInit();
            this.grpCash.ResumeLayout(false);
            this.grpCash.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCash)).EndInit();
            this.grpSettlement.ResumeLayout(false);
            this.grpSettlement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettlement)).EndInit();
            this.grpTabbocco.ResumeLayout(false);
            this.grpTabbocco.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabbacco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCStoreSale;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtGST;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDepositLevy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFuel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCashDriveAway;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtdailystorerntincgst;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtLotowinner;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dtpTransactionDate;
        private System.Windows.Forms.GroupBox grpFuel;
        private System.Windows.Forms.TextBox txtAirmiles;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtdieselltr;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtvpowerltr;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtVppltr;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtSilverltr;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtbronzeltr;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSalTotal;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtActualSettlement;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtcashpayoutStore;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtCashBankDep;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtCardRembsment;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txttotalFuel;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtRadiantdb;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtActualShort;
        private System.Windows.Forms.TextBox txtDiff;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCashSort;
        private System.Windows.Forms.TextBox txtCashPaidEmp;
        private System.Windows.Forms.TextBox txtGiftcert;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtCashpaidmgt;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtCashlottto;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtCashPaid;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtCashReceived;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtActualBankDeposit;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtleftBankdeposit;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txttotallotopayout;
        private System.Windows.Forms.TextBox txtBSDltr;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtVpdvdd;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtfuelcommexgst;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtgstreceivedfuel;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtActualCommission;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtdifindrcr;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox txtgstpaidonrent;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtfairshare;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txttotalsettlement;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtfinalsettlement;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtdiffinsettlement;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtCalstoreother;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtshelGisftcard;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtgiftcard;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtphonecard;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtscratchlotto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtonlinelotto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txttobbaco;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtRadiantcr;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtShellCredit;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtShellDebit;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.GroupBox grpSale;
        private System.Windows.Forms.GroupBox grpCash;
        private System.Windows.Forms.GroupBox grpSettlement;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCStoreRemarks;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtTabbRemarks;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox txtShellRemarks;
        private System.Windows.Forms.GroupBox grpTabbocco;
        private System.Windows.Forms.TextBox txtradiantcrdiff;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtRadiantdbdiff;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtGasRemarks;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox txtCashRemarks;
        private System.Windows.Forms.Button btnTicket;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Button btnTicketBack;
        private System.Windows.Forms.Button btnCals;
        private System.Windows.Forms.DataGridView dgvCstore;
        private System.Windows.Forms.DataGridView dgvFuel;
        private System.Windows.Forms.DataGridView dgvCash;
        private System.Windows.Forms.DataGridView dgvSettlement;
        private System.Windows.Forms.DataGridView dgvTabbacco;






    }
}

