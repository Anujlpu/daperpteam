﻿namespace Shell
{
    partial class FrmBrandSales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrpManageSale = new System.Windows.Forms.GroupBox();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnbrowse = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCashSort = new System.Windows.Forms.TextBox();
            this.txtCashPaidEmp = new System.Windows.Forms.TextBox();
            this.txtGiftcert = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCashpaidmgt = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCashlottto = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCashPaid = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtLotowinner = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCashPayout = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAirMilesCash = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCashforBank = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCashDriveAway = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFuel = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDepositLevy = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtGST = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCStoreSale = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnView = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.GrpManageSale.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpManageSale
            // 
            this.GrpManageSale.Controls.Add(this.BtnDelete);
            this.GrpManageSale.Controls.Add(this.BtnSave);
            this.GrpManageSale.Controls.Add(this.button1);
            this.GrpManageSale.Controls.Add(this.btnbrowse);
            this.GrpManageSale.Controls.Add(this.label18);
            this.GrpManageSale.Controls.Add(this.txtCashSort);
            this.GrpManageSale.Controls.Add(this.txtCashPaidEmp);
            this.GrpManageSale.Controls.Add(this.txtGiftcert);
            this.GrpManageSale.Controls.Add(this.label17);
            this.GrpManageSale.Controls.Add(this.label6);
            this.GrpManageSale.Controls.Add(this.txtRemarks);
            this.GrpManageSale.Controls.Add(this.label16);
            this.GrpManageSale.Controls.Add(this.label15);
            this.GrpManageSale.Controls.Add(this.txtCashpaidmgt);
            this.GrpManageSale.Controls.Add(this.label13);
            this.GrpManageSale.Controls.Add(this.txtCashlottto);
            this.GrpManageSale.Controls.Add(this.label14);
            this.GrpManageSale.Controls.Add(this.txtCashPaid);
            this.GrpManageSale.Controls.Add(this.label11);
            this.GrpManageSale.Controls.Add(this.txtLotowinner);
            this.GrpManageSale.Controls.Add(this.label12);
            this.GrpManageSale.Controls.Add(this.txtCashPayout);
            this.GrpManageSale.Controls.Add(this.label10);
            this.GrpManageSale.Controls.Add(this.txtAirMilesCash);
            this.GrpManageSale.Controls.Add(this.label9);
            this.GrpManageSale.Controls.Add(this.txtCashforBank);
            this.GrpManageSale.Controls.Add(this.label8);
            this.GrpManageSale.Controls.Add(this.txtCashDriveAway);
            this.GrpManageSale.Controls.Add(this.label7);
            this.GrpManageSale.Controls.Add(this.txtFuel);
            this.GrpManageSale.Controls.Add(this.label5);
            this.GrpManageSale.Controls.Add(this.txtDepositLevy);
            this.GrpManageSale.Controls.Add(this.label4);
            this.GrpManageSale.Controls.Add(this.txtGST);
            this.GrpManageSale.Controls.Add(this.label3);
            this.GrpManageSale.Controls.Add(this.txtDiscount);
            this.GrpManageSale.Controls.Add(this.label2);
            this.GrpManageSale.Controls.Add(this.txtCStoreSale);
            this.GrpManageSale.Controls.Add(this.label1);
            this.GrpManageSale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GrpManageSale.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrpManageSale.ForeColor = System.Drawing.Color.Black;
            this.GrpManageSale.Location = new System.Drawing.Point(9, 35);
            this.GrpManageSale.Name = "GrpManageSale";
            this.GrpManageSale.Size = new System.Drawing.Size(791, 476);
            this.GrpManageSale.TabIndex = 0;
            this.GrpManageSale.TabStop = false;
            this.GrpManageSale.Text = "Brand Sales";
            this.GrpManageSale.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // BtnDelete
            // 
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(445, 425);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 47;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(255, 425);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 46;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(349, 425);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 32);
            this.button1.TabIndex = 48;
            this.button1.Text = "&Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnbrowse
            // 
            this.btnbrowse.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnbrowse.FlatAppearance.BorderSize = 2;
            this.btnbrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbrowse.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbrowse.Location = new System.Drawing.Point(200, 352);
            this.btnbrowse.Name = "btnbrowse";
            this.btnbrowse.Size = new System.Drawing.Size(154, 31);
            this.btnbrowse.TabIndex = 39;
            this.btnbrowse.Text = "B&rowse";
            this.btnbrowse.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(15, 352);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(139, 18);
            this.label18.TabIndex = 38;
            this.label18.Text = "Upload Attachment";
            // 
            // txtCashSort
            // 
            this.txtCashSort.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashSort.ForeColor = System.Drawing.Color.Black;
            this.txtCashSort.Location = new System.Drawing.Point(200, 299);
            this.txtCashSort.Name = "txtCashSort";
            this.txtCashSort.Size = new System.Drawing.Size(154, 26);
            this.txtCashSort.TabIndex = 40;
            // 
            // txtCashPaidEmp
            // 
            this.txtCashPaidEmp.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashPaidEmp.ForeColor = System.Drawing.Color.Black;
            this.txtCashPaidEmp.Location = new System.Drawing.Point(200, 254);
            this.txtCashPaidEmp.Name = "txtCashPaidEmp";
            this.txtCashPaidEmp.Size = new System.Drawing.Size(154, 26);
            this.txtCashPaidEmp.TabIndex = 39;
            // 
            // txtGiftcert
            // 
            this.txtGiftcert.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiftcert.ForeColor = System.Drawing.Color.Black;
            this.txtGiftcert.Location = new System.Drawing.Point(629, 287);
            this.txtGiftcert.Name = "txtGiftcert";
            this.txtGiftcert.Size = new System.Drawing.Size(146, 26);
            this.txtGiftcert.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(364, 287);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(176, 18);
            this.label17.TabIndex = 37;
            this.label17.Text = "Gift Certificate/US dollar";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(13, 302);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 36);
            this.label6.TabIndex = 36;
            this.label6.Text = "Cash Short/ added to\r\n Change";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(629, 326);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(146, 131);
            this.txtRemarks.TabIndex = 35;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(364, 320);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 18);
            this.label16.TabIndex = 34;
            this.label16.Text = "Remarks";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(14, 254);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(137, 36);
            this.label15.TabIndex = 31;
            this.label15.Text = "Cash Paid to third \r\nparties/employee";
            // 
            // txtCashpaidmgt
            // 
            this.txtCashpaidmgt.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashpaidmgt.ForeColor = System.Drawing.Color.Black;
            this.txtCashpaidmgt.Location = new System.Drawing.Point(629, 254);
            this.txtCashpaidmgt.Name = "txtCashpaidmgt";
            this.txtCashpaidmgt.Size = new System.Drawing.Size(146, 26);
            this.txtCashpaidmgt.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(364, 250);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(241, 18);
            this.label13.TabIndex = 29;
            this.label13.Text = "Cash Paid to Mgt (Deepak/Ricky)";
            // 
            // txtCashlottto
            // 
            this.txtCashlottto.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashlottto.ForeColor = System.Drawing.Color.Black;
            this.txtCashlottto.Location = new System.Drawing.Point(200, 220);
            this.txtCashlottto.Name = "txtCashlottto";
            this.txtCashlottto.Size = new System.Drawing.Size(154, 26);
            this.txtCashlottto.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(14, 220);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(140, 18);
            this.label14.TabIndex = 27;
            this.label14.Text = "Cash Lotto Payout ";
            // 
            // txtCashPaid
            // 
            this.txtCashPaid.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashPaid.ForeColor = System.Drawing.Color.Black;
            this.txtCashPaid.Location = new System.Drawing.Point(629, 218);
            this.txtCashPaid.Name = "txtCashPaid";
            this.txtCashPaid.Size = new System.Drawing.Size(146, 26);
            this.txtCashPaid.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(364, 216);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(224, 18);
            this.label11.TabIndex = 25;
            this.label11.Text = "Cash Paid for Store Purchases";
            // 
            // txtLotowinner
            // 
            this.txtLotowinner.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLotowinner.ForeColor = System.Drawing.Color.Black;
            this.txtLotowinner.Location = new System.Drawing.Point(200, 182);
            this.txtLotowinner.Name = "txtLotowinner";
            this.txtLotowinner.Size = new System.Drawing.Size(154, 26);
            this.txtLotowinner.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(14, 182);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(180, 18);
            this.label12.TabIndex = 23;
            this.label12.Text = "Lotto Winner + Gift Card ";
            // 
            // txtCashPayout
            // 
            this.txtCashPayout.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashPayout.ForeColor = System.Drawing.Color.Black;
            this.txtCashPayout.Location = new System.Drawing.Point(629, 182);
            this.txtCashPayout.Name = "txtCashPayout";
            this.txtCashPayout.Size = new System.Drawing.Size(146, 26);
            this.txtCashPayout.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(364, 182);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(239, 18);
            this.label10.TabIndex = 21;
            this.label10.Text = "Cash Payout for Store Purchases";
            // 
            // txtAirMilesCash
            // 
            this.txtAirMilesCash.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAirMilesCash.ForeColor = System.Drawing.Color.Black;
            this.txtAirMilesCash.Location = new System.Drawing.Point(629, 111);
            this.txtAirMilesCash.Name = "txtAirMilesCash";
            this.txtAirMilesCash.Size = new System.Drawing.Size(146, 26);
            this.txtAirMilesCash.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(366, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 18);
            this.label9.TabIndex = 19;
            this.label9.Text = "AirMilesCash ";
            // 
            // txtCashforBank
            // 
            this.txtCashforBank.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashforBank.ForeColor = System.Drawing.Color.Black;
            this.txtCashforBank.Location = new System.Drawing.Point(200, 145);
            this.txtCashforBank.Name = "txtCashforBank";
            this.txtCashforBank.Size = new System.Drawing.Size(154, 26);
            this.txtCashforBank.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(13, 148);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(162, 18);
            this.label8.TabIndex = 17;
            this.label8.Text = "Cashfor Bank Deposit";
            // 
            // txtCashDriveAway
            // 
            this.txtCashDriveAway.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCashDriveAway.ForeColor = System.Drawing.Color.Black;
            this.txtCashDriveAway.Location = new System.Drawing.Point(629, 146);
            this.txtCashDriveAway.Name = "txtCashDriveAway";
            this.txtCashDriveAway.Size = new System.Drawing.Size(146, 26);
            this.txtCashDriveAway.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(364, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 18);
            this.label7.TabIndex = 15;
            this.label7.Text = "Cash(Drive Away)";
            // 
            // txtFuel
            // 
            this.txtFuel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuel.ForeColor = System.Drawing.Color.Black;
            this.txtFuel.Location = new System.Drawing.Point(629, 38);
            this.txtFuel.Name = "txtFuel";
            this.txtFuel.Size = new System.Drawing.Size(146, 26);
            this.txtFuel.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(364, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 18);
            this.label5.TabIndex = 11;
            this.label5.Text = "Fuel";
            // 
            // txtDepositLevy
            // 
            this.txtDepositLevy.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepositLevy.ForeColor = System.Drawing.Color.Black;
            this.txtDepositLevy.Location = new System.Drawing.Point(200, 109);
            this.txtDepositLevy.Name = "txtDepositLevy";
            this.txtDepositLevy.Size = new System.Drawing.Size(154, 26);
            this.txtDepositLevy.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(13, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 18);
            this.label4.TabIndex = 9;
            this.label4.Text = "Deposit  Levy";
            // 
            // txtGST
            // 
            this.txtGST.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGST.ForeColor = System.Drawing.Color.Black;
            this.txtGST.Location = new System.Drawing.Point(629, 74);
            this.txtGST.Name = "txtGST";
            this.txtGST.Size = new System.Drawing.Size(146, 26);
            this.txtGST.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(364, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "GST";
            // 
            // txtDiscount
            // 
            this.txtDiscount.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiscount.ForeColor = System.Drawing.Color.Black;
            this.txtDiscount.Location = new System.Drawing.Point(200, 73);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(154, 26);
            this.txtDiscount.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(13, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Discounts (-ve)";
            // 
            // txtCStoreSale
            // 
            this.txtCStoreSale.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCStoreSale.ForeColor = System.Drawing.Color.Black;
            this.txtCStoreSale.Location = new System.Drawing.Point(200, 37);
            this.txtCStoreSale.Name = "txtCStoreSale";
            this.txtCStoreSale.Size = new System.Drawing.Size(154, 26);
            this.txtCStoreSale.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(13, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "C-Store Sale ";
            // 
            // btnView
            // 
            this.btnView.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnView.FlatAppearance.BorderSize = 2;
            this.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnView.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnView.Location = new System.Drawing.Point(374, 6);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(133, 32);
            this.btnView.TabIndex = 46;
            this.btnView.Text = "&View List";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnAdd.FlatAppearance.BorderSize = 2;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(233, 6);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(135, 32);
            this.btnAdd.TabIndex = 45;
            this.btnAdd.Text = "&Create Sale";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(513, 6);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 48;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // FrmBrandSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(834, 519);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.GrpManageSale);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmBrandSales";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shell Brand Sale";
            this.Load += new System.EventHandler(this.FrmBrandSales_Load);
            this.GrpManageSale.ResumeLayout(false);
            this.GrpManageSale.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpManageSale;
        private System.Windows.Forms.TextBox txtCashPayout;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAirMilesCash;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtCashforBank;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCashDriveAway;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFuel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtDepositLevy;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtGST;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCStoreSale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtCashpaidmgt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCashlottto;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCashPaid;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtLotowinner;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtCashSort;
        private System.Windows.Forms.TextBox txtCashPaidEmp;
        private System.Windows.Forms.TextBox txtGiftcert;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnbrowse;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button button1;
    }
}

