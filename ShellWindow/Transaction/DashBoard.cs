﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;

namespace Shell
{
    public partial class DashBoard : Form
    {
        public DashBoard()
        {
            InitializeComponent();
        }
        private void DgvTicketDetails_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex == 3)
            {
                string Status = "";
                Status = DgvTicketDetails.Rows[e.RowIndex].Cells[3].Value.ToString();
                BAL_FuelTransaction fuelTransaction = new BAL_FuelTransaction();
                int count = fuelTransaction.ListTicketDetails(5).Count;
                if (count > 0)
                {
                    DgvTicketDetails.Visible = true;
                    DgvTicketDetails.DataSource = fuelTransaction.ListTicketDetails(5).ToList().Where(p => p.TicketStatus == Status);
                }
                else
                {
                    DgvTicketDetails.Visible = false;
                }
               // DgvTicketDetails.Columns[1].Visible = true;
                //DgvTicketDetails.Columns[2].Visible = true;
                //DgvTicketDetails.Columns[4].Visible = true;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            BindGrid();


        }

        private void BindGrid()
        {

            string Status = FindStatus();
            BAL_FuelTransaction fuelTransaction = new BAL_FuelTransaction();
            int count = fuelTransaction.ListTicketDetails(3).Count;
            if (count > 0)
            {
                DgvTicketDetails.Visible = true;
                if (Status == "")
                {
                    DgvTicketDetails.DataSource = fuelTransaction.ListTicketDetails(3);
                }
                else
                {
                    DgvTicketDetails.DataSource = fuelTransaction.ListTicketDetails(3).Where(p => p.TicketStatus == Status);
                }
            }
            else
            {
                DgvTicketDetails.Visible = false;
            }
            DgvTicketDetails.Columns[1].Visible = false;
            DgvTicketDetails.Columns[2].Visible = false;
            DgvTicketDetails.Columns[4].Visible = false;
        }

        private static string FindStatus()
        {
            string Status = "";
            if (Utility.BA_Commman._Profile == "FE")
            {
                Status = "Open";
            }
            else if (Utility.BA_Commman._Profile == "BE")
            {
                Status = "Submitted";
            }
            else if (Utility.BA_Commman._Profile == "FE")
            {
                Status = "Completed";
            }
            return Status;
        }


        private void DashBoard_Load(object sender, EventArgs e)
        {
            BindGrid();

        }


    }
}
