﻿using Framework;
using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class BankValidation : Form
    {
        public BankValidation()
        {
            InitializeComponent();
        }
        private void rbtPNL_CheckedChanged(object sender, EventArgs e)
        {
            //if (rbtPNL.Checked == true)
            //{
            //}

        }

        private void rbtVendor_CheckedChanged(object sender, EventArgs e)
        {

            //if (rbtVendor.Checked == true)
            //{

            //}
        }

        private void BankValidation_Load(object sender, EventArgs e)
        {
            rbtDebit.Checked = true;
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgvInvoice.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (chk.Value == chk.FalseValue || chk.Value == null)
                {
                    chk.Value = chk.TrueValue;
                }
                else
                {
                    chk.Value = chk.FalseValue;
                }

            }
            dgvInvoice.EndEdit();
        }
        public void binddropdown()
        {
            //ddlVendorName.DisplayMember = "VendorName";
            //ddlVendorName.ValueMember = "VendorID";
            //if (rbtPNL.Checked == true)
            //{
            //    ddlVendorName.DataSource = _obj.GetVendor(txtVendorCode.Text, "2").Tables[0];
            //}
            //else
            //{
            //    ddlVendorName.DataSource = _obj.GetVendor(txtVendorCode.Text, "1").Tables[0];
            //}
            //ddlVendorName.DataSource = _obj.GetVendor(txtVendorCode.Text, "1").Tables[0];

        }
        private void BindStatementGridview()
        {
            DateTime date = Convert.ToDateTime(InvoicedateTimePicker.Text).Add(DateTime.Now.TimeOfDay);
            using (ShellEntities context = new ShellEntities())
            {
                var bankStatements = (from b in context.TBL_BankEntries
                                      where b.TransactionType == TranType && b.IsActive == true && b.IsClosed == false
                                      select new
                                      {
                                          b.BankEntriesId,
                                          b.BankName,
                                          b.TransactionType,
                                          b.TotalAmount,
                                          b.StatementDate
                                      }
                                      ).ToList();
                bankStatements = bankStatements.Where(s => Convert.ToDateTime(s.StatementDate).ToString("yyyyMMdd") == date.ToString("yyyyMMdd")).ToList();
                dgvBankentries.DataSource = bankStatements;
            }
        }

        private void BindInvoicegrid()
        {
            DateTime date = Convert.ToDateTime(InvoicedateTimePicker.Text).Add(DateTime.Now.TimeOfDay);
            using (ShellEntities context = new ShellEntities())
            {
                var invoiceList = (from b in context.Tbl_Invoices
                                   where b.TransactionType == TranType && b.IsActive == true && b.IsClosed == false
                                   select new
                                   {
                                       b.InvoiceId,
                                       b.InvoiceNo,
                                       b.TransactionType,
                                       b.TotalAmount,
                                       b.InvDate
                                   }
                                      ).ToList();
                invoiceList = invoiceList.Where(s => Convert.ToDateTime(s.InvDate).ToString("yyyyMMdd") == date.ToString("yyyyMMdd")).ToList();
                dgvInvoice.DataSource = invoiceList;
            }
        }
        private void CloseInvoiceandStatements()
        {
            DateTime date = Convert.ToDateTime(InvoicedateTimePicker.Text).Add(DateTime.Now.TimeOfDay);
            using (ShellEntities context = new ShellEntities())
            {
                var bankStatements = (from b in context.TBL_BankEntries
                                      where b.TransactionType == TranType && b.IsActive == true && b.IsClosed == false
                                      select b
                                      ).ToList();
                bankStatements = bankStatements.Where(s => bankIds.Contains(s.BankEntriesId.ToString())).ToList();

                foreach (var state in bankStatements)
                {
                    state.IsClosed = true;
                }
                context.SaveChanges();
                var invoice = (from b in context.Tbl_Invoices
                               where b.TransactionType == TranType && b.IsActive == true && b.IsClosed == false
                               select b
                                      ).ToList();
                invoice = invoice.Where(s => invoiceIds.Contains(s.InvoiceId.ToString())).ToList();

                foreach (var state in invoice)
                {
                    state.IsClosed = true;
                }
                context.SaveChanges();
            }
        }
        private void txtVendorCode_TextChanged(object sender, EventArgs e)
        {

        }
        BAL.BAL_BankValEntries _obj = new BAL.BAL_BankValEntries();

        string VendorId = "0";
        private void ddlVendorName_SelectedValueChanged(object sender, EventArgs e)
        {
            //dgvInvoice.DataSource = null;
            //if (ddlVendorName.SelectedValue != null)
            //{

            //    VendorId = ddlVendorName.SelectedValue.ToString();
            //    dgvInvoice.DataSource = _obj.GetOpenInvoiceDetails(TranType, VendorId).Tables[0];
            //}

        }
        string TranType = string.Empty;
        private void rbtCredit_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtCredit.Checked == true)
                TranType = "Credit";
            else
                TranType = "Debit";

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtRemarks.Text))
            {
                MessageBox.Show("Please Enter the Remarks. W/o Remarks it's values not save!");
                return;
            }


            try
            {
                txtVendorCode.Text = "";
                txtRemarks.Text = "";
                //ddlVendorName.Items.Clear();
                CloseInvoiceandStatements();
                BindInvoicegrid();
                BindStatementGridview();
                MessageBox.Show("Open invoices has been closed successfully.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }


        }

        private void txtVendorCode_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    ddlVendorName.DisplayMember = "VendorName";
                    ddlVendorName.ValueMember = "VendorId";
                    var vendors = (from b in context.Mst_Vendor
                                   where b.IsActive == true && b.VendorName.Contains(txtVendorCode.Text)
                                   select new
                                   {
                                       b.VendorId,
                                       b.VendorName,
                                   }).ToList();
                    ddlVendorName.DataSource = vendors;
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }

        private void ddlVendorName_SelectedIndexChanged(object sender, EventArgs e)
        {
            //dgvInvoice.DataSource = null;
            //if (ddlVendorName.SelectedValue != null)
            //{
            //    VendorId = ddlVendorName.SelectedValue.ToString();
            //    dgvInvoice.DataSource = _obj.GetOpenInvoiceDetails(TranType, VendorId).Tables[0];

            //}
        }

        private void dgvInvoice_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    this.dgvInvoice.CommitEdit(DataGridViewDataErrorContexts.Commit);

                //Check the value of cell
                if (this.dgvInvoice.CurrentCell.Value == null)
                {
                    //Use index of TimeOut column
                    this.dgvInvoice.Rows[e.RowIndex].Cells[0].Value = true;

                    //Set other columns values
                }
                else
                {
                    //Use index of TimeOut column
                    this.dgvInvoice.Rows[e.RowIndex].Cells[0].Value = false;

                    //Set other columns values
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }

        private void dgvBankentries_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                    this.dgvBankentries.CommitEdit(DataGridViewDataErrorContexts.Commit);

                //Check the value of cell
                if (this.dgvBankentries.CurrentCell.Value == null)
                {
                    //Use index of TimeOut column
                    this.dgvBankentries.Rows[e.RowIndex].Cells[0].Value = true;

                    //Set other columns values
                }
                else
                {
                    //Use index of TimeOut column
                    this.dgvBankentries.Rows[e.RowIndex].Cells[0].Value = false;

                    //Set other columns values
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }
        List<string> invoiceIds = new List<string>();
        List<string> bankIds = new List<string>();
        private void Validatebutton_Click(object sender, EventArgs e)
        {
            try
            {
                double invoiceTotalAmount = 0;
                double bankTotalAmount = 0;
                double diffAmount = 0;
                string invoicetranType = "";
                string ibanktranType = "";
                if (string.IsNullOrEmpty(txtVendorCode.Text))
                {
                    MessageBox.Show("Please enter vendor name");
                    txtVendorCode.Focus();
                }
                else if (dgvInvoice.Rows.Count > 0 && dgvBankentries.Rows.Count > 0)
                {
                    foreach (DataGridViewRow row in dgvInvoice.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            invoiceIds.Add(row.Cells[1].Value.ToString());
                            invoiceTotalAmount += Convert.ToDouble(row.Cells[4].Value);
                            invoicetranType = row.Cells[3].Value.ToString().Trim();
                        }
                    }

                    foreach (DataGridViewRow row in dgvBankentries.Rows)
                    {
                        if (Convert.ToBoolean(row.Cells[0].Value) == true)
                        {
                            bankIds.Add(row.Cells[1].Value.ToString());
                            bankTotalAmount += Convert.ToDouble(row.Cells[4].Value);
                            ibanktranType = row.Cells[3].Value.ToString();
                        }
                    }

                    if ((invoicetranType = ibanktranType.Trim()) == "Debit")
                    {
                        diffAmount = invoiceTotalAmount - bankTotalAmount;
                    }
                    else
                    {
                        diffAmount = bankTotalAmount - invoiceTotalAmount;
                    }
                    if (!string.IsNullOrEmpty(AdjustmenttextBox.Text) && (Convert.ToDouble(AdjustmenttextBox.Text) > 0 || Convert.ToDouble(AdjustmenttextBox.Text) < 0))
                    {
                        diffAmount = (Convert.ToDouble(diffAmount) - Convert.ToDouble(AdjustmenttextBox.Text));
                    }

                    DifferencetextBox.Text = diffAmount.ToString();
                    if (diffAmount == 0)
                    {
                        MessageBox.Show("You can close selected invoice...!");
                        BtnSave.Enabled = true;
                    }
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show("Please contact administrator-" + eX.InnerException);
            }
        }

        private void InvoiceStatementbutton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(ddlVendorName.SelectedValue)))
                {
                    MessageBox.Show("You must have to select vendor....");
                    return;
                }
                else
                {
                    BindInvoicegrid();
                    BindStatementGridview();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + ex.InnerException);
            }

        }
    }
}
