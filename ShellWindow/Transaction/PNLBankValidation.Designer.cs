﻿namespace Shell.Transaction
{
    partial class PNLBankValidation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.InvoiceStatementbutton = new System.Windows.Forms.Button();
            this.Validatebutton = new System.Windows.Forms.Button();
            this.DifferencetextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AdjustmenttextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Radiopanel = new System.Windows.Forms.Panel();
            this.rbtDebit = new System.Windows.Forms.RadioButton();
            this.rbtCredit = new System.Windows.Forms.RadioButton();
            this.btnExit = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.PNLListDropDown = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.BankCheckbox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvBankentries = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CheckBox = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvPNLList = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.InvoicedateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.grpCashMngtList = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Radiopanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankentries)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNLList)).BeginInit();
            this.grpCashMngtList.SuspendLayout();
            this.SuspendLayout();
            // 
            // InvoiceStatementbutton
            // 
            this.InvoiceStatementbutton.Location = new System.Drawing.Point(124, 84);
            this.InvoiceStatementbutton.Name = "InvoiceStatementbutton";
            this.InvoiceStatementbutton.Size = new System.Drawing.Size(85, 26);
            this.InvoiceStatementbutton.TabIndex = 240;
            this.InvoiceStatementbutton.Text = "View";
            this.InvoiceStatementbutton.UseVisualStyleBackColor = true;
            this.InvoiceStatementbutton.Click += new System.EventHandler(this.InvoiceStatementbutton_Click);
            // 
            // Validatebutton
            // 
            this.Validatebutton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Validatebutton.FlatAppearance.BorderSize = 2;
            this.Validatebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Validatebutton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Validatebutton.Location = new System.Drawing.Point(773, 129);
            this.Validatebutton.Name = "Validatebutton";
            this.Validatebutton.Size = new System.Drawing.Size(90, 32);
            this.Validatebutton.TabIndex = 237;
            this.Validatebutton.Text = "Validate";
            this.Validatebutton.UseVisualStyleBackColor = true;
            this.Validatebutton.Click += new System.EventHandler(this.Validatebutton_Click);
            // 
            // DifferencetextBox
            // 
            this.DifferencetextBox.Enabled = false;
            this.DifferencetextBox.Location = new System.Drawing.Point(756, 215);
            this.DifferencetextBox.Name = "DifferencetextBox";
            this.DifferencetextBox.Size = new System.Drawing.Size(128, 20);
            this.DifferencetextBox.TabIndex = 235;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(770, 197);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 15);
            this.label3.TabIndex = 234;
            this.label3.Text = "Difference";
            // 
            // AdjustmenttextBox
            // 
            this.AdjustmenttextBox.Location = new System.Drawing.Point(756, 103);
            this.AdjustmenttextBox.Name = "AdjustmenttextBox";
            this.AdjustmenttextBox.Size = new System.Drawing.Size(128, 20);
            this.AdjustmenttextBox.TabIndex = 233;
            this.AdjustmenttextBox.Text = "0.00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(770, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 15);
            this.label1.TabIndex = 232;
            this.label1.Text = "Adjustment";
            // 
            // Radiopanel
            // 
            this.Radiopanel.Controls.Add(this.rbtDebit);
            this.Radiopanel.Controls.Add(this.rbtCredit);
            this.Radiopanel.Location = new System.Drawing.Point(124, 43);
            this.Radiopanel.Name = "Radiopanel";
            this.Radiopanel.Size = new System.Drawing.Size(213, 24);
            this.Radiopanel.TabIndex = 231;
            // 
            // rbtDebit
            // 
            this.rbtDebit.AutoSize = true;
            this.rbtDebit.Checked = true;
            this.rbtDebit.Location = new System.Drawing.Point(3, 4);
            this.rbtDebit.Name = "rbtDebit";
            this.rbtDebit.Size = new System.Drawing.Size(50, 17);
            this.rbtDebit.TabIndex = 204;
            this.rbtDebit.TabStop = true;
            this.rbtDebit.Text = "Debit";
            this.rbtDebit.UseVisualStyleBackColor = true;
            // 
            // rbtCredit
            // 
            this.rbtCredit.AutoSize = true;
            this.rbtCredit.Location = new System.Drawing.Point(72, 3);
            this.rbtCredit.Name = "rbtCredit";
            this.rbtCredit.Size = new System.Drawing.Size(52, 17);
            this.rbtCredit.TabIndex = 205;
            this.rbtCredit.Text = "Credit";
            this.rbtCredit.UseVisualStyleBackColor = true;
            this.rbtCredit.CheckedChanged += new System.EventHandler(this.rbtCredit_CheckedChanged);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(834, 360);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 230;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.Enabled = false;
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(728, 360);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 229;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // PNLListDropDown
            // 
            this.PNLListDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PNLListDropDown.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PNLListDropDown.FormattingEnabled = true;
            this.PNLListDropDown.Location = new System.Drawing.Point(124, 12);
            this.PNLListDropDown.Name = "PNLListDropDown";
            this.PNLListDropDown.Size = new System.Drawing.Size(213, 22);
            this.PNLListDropDown.TabIndex = 228;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(13, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 15);
            this.label13.TabIndex = 227;
            this.label13.Text = "Transaction Type";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(781, 248);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 226;
            this.label62.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(728, 266);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(206, 77);
            this.txtRemarks.TabIndex = 225;
            // 
            // BankCheckbox
            // 
            this.BankCheckbox.HeaderText = "";
            this.BankCheckbox.Name = "BankCheckbox";
            this.BankCheckbox.ReadOnly = true;
            this.BankCheckbox.Width = 5;
            // 
            // dgvBankentries
            // 
            this.dgvBankentries.AllowDrop = true;
            this.dgvBankentries.AllowUserToAddRows = false;
            this.dgvBankentries.AllowUserToDeleteRows = false;
            this.dgvBankentries.AllowUserToResizeColumns = false;
            this.dgvBankentries.AllowUserToResizeRows = false;
            this.dgvBankentries.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvBankentries.BackgroundColor = System.Drawing.Color.White;
            this.dgvBankentries.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBankentries.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BankCheckbox});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.NullValue = null;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBankentries.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBankentries.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBankentries.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvBankentries.Location = new System.Drawing.Point(3, 16);
            this.dgvBankentries.MultiSelect = false;
            this.dgvBankentries.Name = "dgvBankentries";
            this.dgvBankentries.ReadOnly = true;
            this.dgvBankentries.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvBankentries.RowHeadersVisible = false;
            this.dgvBankentries.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvBankentries.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvBankentries.Size = new System.Drawing.Size(669, 171);
            this.dgvBankentries.TabIndex = 1;
            this.dgvBankentries.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBankentries_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvBankentries);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(23, 288);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(675, 190);
            this.groupBox1.TabIndex = 224;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bank Entries";
            // 
            // CheckBox
            // 
            this.CheckBox.FalseValue = "false";
            this.CheckBox.HeaderText = "";
            this.CheckBox.MinimumWidth = 15;
            this.CheckBox.Name = "CheckBox";
            this.CheckBox.ReadOnly = true;
            this.CheckBox.TrueValue = "true";
            this.CheckBox.Width = 15;
            // 
            // dgvPNLList
            // 
            this.dgvPNLList.AllowDrop = true;
            this.dgvPNLList.AllowUserToAddRows = false;
            this.dgvPNLList.AllowUserToDeleteRows = false;
            this.dgvPNLList.AllowUserToResizeColumns = false;
            this.dgvPNLList.AllowUserToResizeRows = false;
            this.dgvPNLList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPNLList.BackgroundColor = System.Drawing.Color.White;
            this.dgvPNLList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPNLList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckBox});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPNLList.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPNLList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPNLList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPNLList.Location = new System.Drawing.Point(3, 16);
            this.dgvPNLList.MultiSelect = false;
            this.dgvPNLList.Name = "dgvPNLList";
            this.dgvPNLList.ReadOnly = true;
            this.dgvPNLList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvPNLList.RowHeadersVisible = false;
            this.dgvPNLList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvPNLList.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvPNLList.Size = new System.Drawing.Size(676, 139);
            this.dgvPNLList.TabIndex = 1;
            this.dgvPNLList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPNLList_CellContentClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(351, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 15);
            this.label5.TabIndex = 239;
            this.label5.Text = "Entry Date";
            // 
            // InvoicedateTimePicker
            // 
            this.InvoicedateTimePicker.Location = new System.Drawing.Point(449, 52);
            this.InvoicedateTimePicker.Name = "InvoicedateTimePicker";
            this.InvoicedateTimePicker.Size = new System.Drawing.Size(213, 20);
            this.InvoicedateTimePicker.TabIndex = 238;
            // 
            // grpCashMngtList
            // 
            this.grpCashMngtList.Controls.Add(this.dgvPNLList);
            this.grpCashMngtList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCashMngtList.Location = new System.Drawing.Point(19, 124);
            this.grpCashMngtList.Name = "grpCashMngtList";
            this.grpCashMngtList.Size = new System.Drawing.Size(682, 158);
            this.grpCashMngtList.TabIndex = 223;
            this.grpCashMngtList.TabStop = false;
            this.grpCashMngtList.Text = "PNL Entry";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(13, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 222;
            this.label2.Text = "PNL Name";
            // 
            // PNLBankValidation
            // 
            this.ClientSize = new System.Drawing.Size(950, 483);
            this.Controls.Add(this.InvoiceStatementbutton);
            this.Controls.Add(this.Validatebutton);
            this.Controls.Add(this.DifferencetextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AdjustmenttextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Radiopanel);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.PNLListDropDown);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.txtRemarks);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.InvoicedateTimePicker);
            this.Controls.Add(this.grpCashMngtList);
            this.Controls.Add(this.label2);
            this.Name = "PNLBankValidation";
            this.Load += new System.EventHandler(this.PNLBankValidation_Load);
            this.Radiopanel.ResumeLayout(false);
            this.Radiopanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBankentries)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPNLList)).EndInit();
            this.grpCashMngtList.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button InvoiceStatementbutton;
        private System.Windows.Forms.Button Validatebutton;
        private System.Windows.Forms.TextBox DifferencetextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AdjustmenttextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Radiopanel;
        private System.Windows.Forms.RadioButton rbtDebit;
        private System.Windows.Forms.RadioButton rbtCredit;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.ComboBox PNLListDropDown;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.DataGridViewCheckBoxColumn BankCheckbox;
        private System.Windows.Forms.DataGridView dgvBankentries;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckBox;
        private System.Windows.Forms.DataGridView dgvPNLList;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker InvoicedateTimePicker;
        private System.Windows.Forms.GroupBox grpCashMngtList;
        private System.Windows.Forms.Label label2;
    }
}
