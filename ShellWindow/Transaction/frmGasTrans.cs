﻿using DAL;
using Shell.Transaction;
using System;
using System.Globalization;
using System.Windows.Forms;
using BAL;
using System.IO;
using System.Collections.Generic;
using System.Drawing;

namespace Shell
{
    public partial class frmGasTrans : Form
    {
        public frmGasTrans()
        {
            InitializeComponent();
        }

        BAL_FuelTransaction _objDoc;
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Saved!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Updated!");
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Record Deleted!");
        }
        private void btnbrowse_Click_1(object sender, EventArgs e)
        {

            //Ask user to select file.
            OpenFileDialog dlg = new OpenFileDialog();
            //dlg.Filter = "JPEG Files(*.jpeg)| Bmp Files(*.bmp)";


            DialogResult dlgRes = dlg.ShowDialog();

            if (dlgRes != DialogResult.Cancel)
            {
                //Set image in picture box


                //Provide file path in txtImagePath text box.

            }

        }

        public void binddropdown()
        {
            dgvCstore.DataSource = null;
            dgvCash.DataSource = null;
            dgvTabbacco.DataSource = null;
            dgvSettlement.DataSource = null;
            dgvFuel.DataSource = null;
            _objDoc = new BAL.BAL_FuelTransaction();
            //ddlCstoreDocs.DataSource = _objDoc.ListGetDocsList(dtpTransactionDate.Value, "Sale");
            //ddlCstoreDocs.DisplayMember = "DocsIdentity";
            //ddlCstoreDocs.ValueMember = "DocId";
            List<DocsEntity> lst = _objDoc.ListGetDocsList(dtpTransactionDate.Value, "Sale");
            if (lst.Count > 0)
            {
                dgvCstore.DataSource = lst;
            }
            lst = _objDoc.ListGetDocsList(dtpTransactionDate.Value, "Tabbocco");
            if (lst.Count > 0)
            {
                dgvTabbacco.DataSource = lst;
            }
            lst = _objDoc.ListGetDocsList(dtpTransactionDate.Value, "Fuel");
            if (lst.Count > 0)
            {
                dgvFuel.DataSource = lst;
            }
            lst = _objDoc.ListGetDocsList(dtpTransactionDate.Value, "Cash");
            if (lst.Count > 0)
            {
                dgvCash.DataSource = lst;
            }
            lst = _objDoc.ListGetDocsList(dtpTransactionDate.Value, "Settlement");
            if (lst.Count > 0)
            {
                dgvSettlement.DataSource = lst;
            }
        }
        private void frmGasTrans_Load(object sender, EventArgs e)
        {
            binddropdown();
            GridAddColumn();

            btnAdd.Visible = false;
            if (Utility.BA_Commman._Profile == "FE")
            {
                grpFuel.Enabled = false;
                grpSettlement.Enabled = false;
                grpCash.Enabled = true;
                grpSale.Enabled = true;
                grpTabbocco.Enabled = false;
                btnAdd.Visible = true;
            }
            else if (Utility.BA_Commman._Profile == "BE")
            {
                grpFuel.Enabled = true;
                grpSettlement.Enabled = true;
                grpCash.Enabled = false;
                grpSale.Enabled = false;
                grpTabbocco.Enabled = true;
                btnAdd.Visible = true;
            }
            else if (Utility.BA_Commman._Profile == "SupAdm" || Utility.BA_Commman._Profile == "Adm")
            {
                grpFuel.Enabled = true;
                grpSettlement.Enabled = true;
                grpCash.Enabled = true;
                grpSale.Enabled = true;
                grpTabbocco.Enabled = true;
                btnAdd.Visible = true;
            }
            else
            {
                grpFuel.Enabled = true;
                grpSettlement.Enabled = true;
                grpCash.Enabled = true;
                grpSale.Enabled = true;
            }
        }

        private void GridAddColumn()
        {
            if (dgvCstore.DataSource != null)
            {
                dgvCstore.Columns[0].Visible = false;
                dgvCstore.Columns.Insert(1, new DataGridViewCheckBoxColumn());
                dgvCstore.Columns[1].HeaderText = "Select";
                dgvCstore.Columns[1].ReadOnly = false;
                dgvCstore.Columns[2].ReadOnly = true;
                dgvCstore.Columns[1].Width = 50;
                dgvCstore.Columns[2].Visible = false;
                dgvCstore.Columns[3].Visible = false;
                dgvCstore.Columns[4].Visible = false;
            }
            //
            if (dgvTabbacco.DataSource != null)
            {
                dgvTabbacco.Columns[0].Visible = false;
                dgvTabbacco.Columns.Insert(1, new DataGridViewCheckBoxColumn());
                dgvTabbacco.Columns[1].HeaderText = "Select";
                dgvTabbacco.Columns[1].ReadOnly = false;
                dgvTabbacco.Columns[2].ReadOnly = true;
                dgvTabbacco.Columns[1].Width = 50;
                dgvTabbacco.Columns[2].Visible = false;
                dgvTabbacco.Columns[3].Visible = false;
                dgvTabbacco.Columns[4].Visible = false;
            }
            // 
            if (dgvCash.DataSource != null)
            {
                dgvCash.Columns[0].Visible = false;
                dgvCash.Columns.Insert(1, new DataGridViewCheckBoxColumn());
                dgvCash.Columns[1].HeaderText = "Select";
                dgvCash.Columns[1].ReadOnly = false;
                dgvCash.Columns[2].ReadOnly = true;
                dgvCash.Columns[1].Width = 50;
                dgvCash.Columns[2].Visible = false;
                dgvCash.Columns[3].Visible = false;
                dgvCash.Columns[4].Visible = false;
            }
            //
            if (dgvFuel.DataSource != null)
            {
                dgvFuel.Columns[0].Visible = false;
                dgvFuel.Columns.Insert(1, new DataGridViewCheckBoxColumn());
                dgvFuel.Columns[1].HeaderText = "Select";
                dgvFuel.Columns[1].ReadOnly = false;
                dgvFuel.Columns[2].ReadOnly = true;
                dgvFuel.Columns[1].Width = 50;
                dgvFuel.Columns[2].Visible = false;
                dgvFuel.Columns[3].Visible = false;
                dgvFuel.Columns[4].Visible = false;
            }
            //
            if (dgvSettlement.DataSource != null)
            {
                dgvSettlement.Columns[0].Visible = false;
                dgvSettlement.Columns.Insert(1, new DataGridViewCheckBoxColumn());
                dgvSettlement.Columns[1].HeaderText = "Select";
                dgvSettlement.Columns[1].ReadOnly = false;
                dgvSettlement.Columns[2].ReadOnly = true;
                dgvSettlement.Columns[1].Width = 50;
                dgvSettlement.Columns[2].Visible = false;
                dgvSettlement.Columns[3].Visible = false;
                dgvSettlement.Columns[4].Visible = false;
            }
        }
        string str = "0123456789.";
        private void KeyPress_t(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 45)
            {

                if (str.IndexOf(e.KeyChar.ToString()) < 0)
                {
                    if (e.KeyChar.ToString() != "\b")
                    {
                        e.Handled = true;
                    }
                }
            }


        }

        double CstoreSale = 0.0, Discounts = 0.0, Fuel = 0.0, Gst = 0.0, Levy = 0.0, TotalSale = 0.0;

        private void AddSale()
        {
            CstoreSale = Convert.ToDouble(txtCStoreSale.Text);
            Discounts = Convert.ToDouble(txtDiscount.Text);
            Fuel = Convert.ToDouble(txtFuel.Text);
            Gst = Convert.ToDouble(txtGST.Text);
            Levy = Convert.ToDouble(txtDepositLevy.Text);
            TotalSale = (CstoreSale + Fuel + Gst + Levy) - Discounts;
            txtSalTotal.Text = TotalSale.ToString();
        }

        double Tobbaco = 0.0, Online_Lottoo = 0.0, Scratch_Lotto = 0.0, Phone_Cards = 0.0, Gift_Cards = 0.0, Shell_Gift_Cards = 0.0, Store_Others = 0.0;

        private void btnbrowse_Click(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CommonFileUploadForm fileForm = new CommonFileUploadForm();
            fileForm.ShowDialog();
        }


        private string _cklCstoredoc, _ckltabbacco, _cklcash, _cklfuel, _cklsettlement;
        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Add New Record
            if (Utility.BA_Commman._Profile == "FE")
            {
                if (_cklCstoredoc == "" && string.IsNullOrEmpty(txtCStoreRemarks.Text))
                {
                    MessageBox.Show("Please Select the C Store Doc Or Enter Remarks");

                }
                else if (_cklcash == "" && string.IsNullOrEmpty(txtCashRemarks.Text))
                {
                    MessageBox.Show("Please Select the Cash Doc Or Enter Remarks");

                }

            }
            else if (Utility.BA_Commman._Profile == "FE")
            {
                if (_ckltabbacco == "" && string.IsNullOrEmpty(txtTabbRemarks.Text))
                {
                    MessageBox.Show("Please Select the Tabbaco Doc Or Enter Remarks");
                }
                else if (_cklfuel == "" && string.IsNullOrEmpty(txtGasRemarks.Text))
                {
                    MessageBox.Show("Please Select the Fuel Doc Or Enter Remarks");
                }
                if (_cklsettlement == "" && string.IsNullOrEmpty(txtShellRemarks.Text))
                {
                    MessageBox.Show("Please Select the Settlement Doc Or Enter Remarks");

                }
            }
            FuelTransactionEntities entities = new FuelTransactionEntities();
            entities.GasTransactionID = 0;
            // C-Store SaleDettails
            #region C-Store SaleDetails
            entities.TransactionDate = Convert.ToDateTime(dtpTransactionDate.Text);
            entities.C_StoreSale = float.Parse((txtCStoreSale.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.C_AttachmentID = _cklCstoredoc;
            entities.C_Store_Remakrs = txtCStoreRemarks.Text;
            #endregion
            #region Cash Part handeled by FE
            entities.CashPayoutForStorePurchase = float.Parse(Convert.ToString(txtcashpayoutStore.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashLottoPayout = float.Parse((txtCashlottto.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashPaidForStorePur = float.Parse(Convert.ToString(txtCashPaid.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashPaidToThirdParties = float.Parse(Convert.ToString(txtCashPaidEmp.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashPaidToMGT = float.Parse((txtCashpaidmgt.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashShort = float.Parse(Convert.ToString(txtCashSort.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.GiftCertificate = float.Parse(Convert.ToString(txtGiftcert.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashReceived = float.Parse((txtCashReceived.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashRemarks = Convert.ToString(txtCashRemarks.Text);
            entities.CashAttachmentID = _cklcash;
            #endregion
            #region Fuel Transaction Details
            entities.Bronze = float.Parse(Convert.ToString(txtbronzeltr.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.Diesel = float.Parse(Convert.ToString(txtdieselltr.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.Silver = float.Parse(Convert.ToString(txtSilverltr.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.VPowerPower = float.Parse(Convert.ToString(txtVppltr.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.VPowerDiesel = float.Parse(Convert.ToString(txtVppltr.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.AirMilesCash = float.Parse(Convert.ToString(txtAirmiles.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.LottoWinnerGiftCard = float.Parse(Convert.ToString(txtLotowinner.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashForBankDeposit = float.Parse(Convert.ToString(txtCashBankDep.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CashDriveAway = float.Parse(Convert.ToString(txtCashDriveAway.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.FuelAttachmentID = _cklfuel;
            entities.FuelRemarks = Convert.ToString(txtGasRemarks.Text);

            #endregion
            #region Settlement
            entities.RadiantDebit = float.Parse(Convert.ToString(txtRadiantdb.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.RadiantCredit = float.Parse(Convert.ToString(txtRadiantcr.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.ShellDebit = float.Parse(Convert.ToString(txtShellDebit.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.ShellCredit = float.Parse(Convert.ToString(txtShellCredit.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.DailyStoreRentincludingGs = float.Parse(Convert.ToString(txtdailystorerntincgst.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.ActualCommission = float.Parse(Convert.ToString(txtActualCommission.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.CardsReimbursements = float.Parse(Convert.ToString(txtCardRembsment.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.ActualSettlement = float.Parse(Convert.ToString(txtActualSettlement.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.FairShare = float.Parse(Convert.ToString(txtfairshare.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.AttachmentIDShell = _cklsettlement;
            entities.ShellRemarks = Convert.ToString(txtShellRemarks.Text);
            #endregion
            #region Tabbaco
            entities.Tabbacco = float.Parse(Convert.ToString(txttobbaco.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.OnlineLotto = float.Parse(Convert.ToString(txtonlinelotto.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.ScratchLotto = float.Parse(Convert.ToString(txtscratchlotto.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.PhoneCard = float.Parse(Convert.ToString(txtphonecard.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.GiftCard = float.Parse(Convert.ToString(txtgiftcard.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.ShellGifTcar = float.Parse(Convert.ToString(txtshelGisftcard.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.Discount = float.Parse(Convert.ToString(txtDiscount.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.Fuel = float.Parse(Convert.ToString(txtFuel.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.GST = float.Parse(Convert.ToString(txtGST.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.DepositLevy = float.Parse(Convert.ToString(txtDepositLevy.Text), CultureInfo.InvariantCulture.NumberFormat);
            entities.TabbbaccoAttachemntid = _ckltabbacco;
            entities.TabboccoRemarks = Convert.ToString(txtTabbRemarks.Text);
            #endregion
            //

            int i = _objDoc.AddEditFuelTransaction(entities);
            if (i == 1)
            {
                MessageBox.Show("Transaction Record Sucessfully");
                cleartextvalue();
            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }



            grpFuel.Visible = true;

        }

        private void txtCStoreSale_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            } AddSale();
        }
        private void Key_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            } AddCtoreSale();
        }
        private void FuelKey_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            } AddFuel();
        }

        private void txtAirmiles_Leave(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            } AddAirmiles();
        }

        private void txtRadiantdb_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            } AddSettlement();
        }

        private void txtcashpayoutStore_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            } AddCash();
        }

        private void txtdailystorerntincgst_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            DailyStoreGST();
        }

        private void DailyStoreGST()
        {
            double dailystorerntincgst = 0.0, gstpaidonrent = 0.0, Fuel = 0.0, difindrcr = 0.0;

            dailystorerntincgst = Convert.ToDouble(txtdailystorerntincgst.Text);
            gstpaidonrent = dailystorerntincgst - (dailystorerntincgst / 1.05);
            txtgstpaidonrent.Text = gstpaidonrent.ToString("0.00");
            Fuel = Convert.ToDouble(txtFuel.Text);
            txttotalsettlement.Text = (dailystorerntincgst + Fuel).ToString("0.00");
            if (string.IsNullOrEmpty(txtdifindrcr.Text))
            {
                txtdifindrcr.Text = "0";

            }
            difindrcr = Convert.ToDouble(txtdifindrcr.Text);
            txtfinalsettlement.Text = (dailystorerntincgst + Fuel + difindrcr).ToString("0.00");
        }

        private void FileOpen(ComboBox ddl)
        {
            _objDoc = new BAL.BAL_FuelTransaction();
            string filepath = _objDoc.GetFilePath(ddl.SelectedValue.ToString());
            if (!String.IsNullOrEmpty(filepath))
            //  File.Open(filepath, FileMode.Open);
            {
                PictureBox PictureBox1 = new PictureBox();
                PictureBox1.Image = new Bitmap(filepath);

                // Add the new control to its parent's controls collection
                this.Controls.Add(PictureBox1);
            }
            else
                MessageBox.Show("There is some issue in file.");
        }

        private void dtpTransactionDate_ValueChanged(object sender, EventArgs e)
        {
            cleartextvalue();
            binddropdown();
            GridAddColumn();
            LoadGridView();
            calculate();
        }

        private void btnTicket_Click(object sender, EventArgs e)
        {
            if (Utility.BA_Commman._Profile == "FE")
            {

                _objDoc.ManageTicket(2, dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
                MessageBox.Show("Ticket Moved to BE User");
            }
            if (Utility.BA_Commman._Profile == "BE")
            {

                _objDoc.ManageTicket(3, dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
                MessageBox.Show("Ticket Moved to Auditor User");
            }

        }

        private void btnTicketBack_Click(object sender, EventArgs e)
        {
            if (Utility.BA_Commman._Profile == "BE")
            {

                _objDoc.ManageTicket(1, dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
                MessageBox.Show("Ticket backed to FE User");
            }
            if (Utility.BA_Commman._Profile != "BE" && Utility.BA_Commman._Profile != "FE")
            {

                _objDoc.ManageTicket(2, dtpTransactionDate.Value.ToString("MM/dd/yyyy"));
                MessageBox.Show("Ticket backed to BE User");
            }

        }
        private void checkgrid(DataGridView dg, string value)
        {
        }
        private void clearcheckgrid(DataGridView dg)
        {
            foreach (DataGridViewRow r in dg.Rows)
            {
                r.Cells[1].Value = false;
            }
        }
        private void cleartextvalue()
        {
            txtdifindrcr.Text = "0";
            #region C-Store Sale Details
            txtCStoreSale.Text = "0";
            // ddlCstoreDocs.SelectedValue = lst[0].C_AttachmentID;
            txtCStoreRemarks.Text = "";
            #endregion
            #region TabbbacoDetial
            txttobbaco.Text = "0";
            txtonlinelotto.Text = "0";
            txtscratchlotto.Text = "0";
            txtphonecard.Text = "0";
            txtgiftcard.Text = "0";
            txtshelGisftcard.Text = "0";
            txtDiscount.Text = "0";
            txtFuel.Text = "0";
            txtGST.Text = "0";
            txtDepositLevy.Text = "0";
            txtTabbRemarks.Text = "";
            #endregion
            #region Fuel Details
            txtbronzeltr.Text = "0";
            txtdieselltr.Text = "0";
            txtSilverltr.Text = "0";
            txtvpowerltr.Text = "0";
            txtVppltr.Text = "0";
            txtAirmiles.Text = "0";
            txtLotowinner.Text = "0";
            txtCashBankDep.Text = "0";
            txtCashDriveAway.Text = "0";
            txtGasRemarks.Text = "";
            #endregion
            #region Cash
            txtcashpayoutStore.Text = "0";
            txtCashlottto.Text = "0";
            txtCashPaid.Text = "0";
            txtCashPaidEmp.Text = "0";
            txtCashpaidmgt.Text = "0";
            txtCashSort.Text = "0";
            txtGiftcert.Text = "0";
            txtCashReceived.Text = "0";
            txtCashRemarks.Text = "";
            #endregion
            #region Settlement
            txtRadiantcr.Text = "0";
            txtRadiantdb.Text = "0";
            txtShellCredit.Text = "0";
            txtShellDebit.Text = "0";
            txtdailystorerntincgst.Text = "0";
            txtActualCommission.Text = "0";
            txtCardRembsment.Text = "0";
            txtActualSettlement.Text = "0";
            txtfairshare.Text = "0";
            txtShellRemarks.Text = "";
            #endregion

            clearcheckgrid(dgvCash);
            clearcheckgrid(dgvCstore);
            clearcheckgrid(dgvFuel); clearcheckgrid(dgvSettlement); clearcheckgrid(dgvTabbacco);
            dgvCash.DataSource = null;
            dgvCstore.DataSource = null;
            dgvFuel.DataSource = null;
            dgvSettlement.DataSource = null;
            dgvTabbacco.DataSource = null;
        }
        private void LoadGridView()
        {
            BAL_FuelTransaction fuelTransaction = new BAL_FuelTransaction();

            List<FuelTransactionEntities> lst = fuelTransaction.ListFuelTransactions(dtpTransactionDate.Value.ToString("MM/dd/yyyy"));

            int count = lst.Count;
            if (count > 0)
            {
                #region C-Store Sale Details
                txtCStoreSale.Text = lst[0].C_StoreSale.ToString();
                _cklCstoredoc = lst[0].C_AttachmentID;
                txtCStoreRemarks.Text = lst[0].C_Store_Remakrs;
                #endregion
                #region TabbbacoDetial
                txttobbaco.Text = lst[0].Tabbacco.ToString();
                txtonlinelotto.Text = lst[0].OnlineLotto.ToString();
                txtscratchlotto.Text = lst[0].ScratchLotto.ToString();
                txtphonecard.Text = lst[0].PhoneCard.ToString();
                txtgiftcard.Text = lst[0].GiftCard.ToString();
                txtshelGisftcard.Text = lst[0].ShellGifTcar.ToString();
                txtDiscount.Text = lst[0].Discount.ToString();
                txtFuel.Text = lst[0].Fuel.ToString();
                txtGST.Text = lst[0].GST.ToString();
                txtDepositLevy.Text = lst[0].DepositLevy.ToString();
                txtTabbRemarks.Text = lst[0].TabboccoRemarks;
                _ckltabbacco = lst[0].TabbbaccoAttachemntid;

                #endregion
                #region Fuel Details
                txtbronzeltr.Text = lst[0].Bronze.ToString();
                txtdieselltr.Text = lst[0].Diesel.ToString();
                txtSilverltr.Text = lst[0].Silver.ToString();
                txtvpowerltr.Text = lst[0].VPowerDiesel.ToString();
                txtVppltr.Text = lst[0].VPowerPower.ToString();
                txtAirmiles.Text = lst[0].AirMilesCash.ToString();
                txtLotowinner.Text = lst[0].LottoWinnerGiftCard.ToString();
                txtCashBankDep.Text = lst[0].CashForBankDeposit.ToString();
                txtCashDriveAway.Text = lst[0].CashDriveAway.ToString();
                _cklfuel = lst[0].FuelAttachmentID;
                txtGasRemarks.Text = lst[0].FuelRemarks;
                #endregion
                #region Cash
                txtcashpayoutStore.Text = lst[0].CashPayoutForStorePurchase.ToString();
                txtCashlottto.Text = lst[0].CashLottoPayout.ToString();
                txtCashPaid.Text = lst[0].CashPaidForStorePur.ToString();
                txtCashPaidEmp.Text = lst[0].CashPaidToThirdParties.ToString();
                txtCashpaidmgt.Text = lst[0].CashPaidToMGT.ToString();
                txtCashSort.Text = lst[0].CashShort.ToString();
                txtGiftcert.Text = lst[0].GiftCertificate.ToString();
                txtCashReceived.Text = lst[0].CashReceived.ToString();
                txtCashRemarks.Text = lst[0].CashRemarks;
                _cklcash = lst[0].CashAttachmentID;
                #endregion
                #region Settlement
                txtRadiantcr.Text = lst[0].RadiantCredit.ToString();
                txtRadiantdb.Text = lst[0].RadiantDebit.ToString();
                txtShellCredit.Text = lst[0].ShellCredit.ToString();
                txtShellDebit.Text = lst[0].ShellDebit.ToString();
                txtdailystorerntincgst.Text = lst[0].DailyStoreRentincludingGs.ToString();
                txtActualCommission.Text = lst[0].ActualCommission.ToString();
                txtCardRembsment.Text = lst[0].CardsReimbursements.ToString();
                txtActualSettlement.Text = lst[0].ActualSettlement.ToString();
                txtfairshare.Text = lst[0].FairShare.ToString();
                _cklsettlement = lst[0].AttachmentIDShell;
                txtShellRemarks.Text = lst[0].ShellRemarks;
                #endregion

                selectgridvalue(dgvCstore, _cklCstoredoc);
                selectgridvalue(dgvTabbacco, _ckltabbacco);
                selectgridvalue(dgvCash, _cklcash);
                selectgridvalue(dgvFuel, _cklfuel);
                selectgridvalue(dgvSettlement, _cklsettlement);


            }
            else
            {


            }


        }

        private void selectgridvalue(DataGridView dgv, string dgvalue)
        {
            if (!string.IsNullOrEmpty(dgvalue))
            {
                for (int i = 0; i < dgvalue.Split(',').Length; i++)
                {
                    foreach (DataGridViewRow item in dgv.Rows)
                    {
                        if (item.Cells["docid"].Value.ToString() == dgvalue[i].ToString())
                        {
                            item.Cells[1].Value = true;

                        }
                    }
                }

            }
        }
        public void calculate()
        {
            AddCtoreSale(); lottopayout(); AddFuel(); AddAirmiles(); AddSettlement();
            AddCash(); CardRem(); DailyStoreGST();
        }
        private void btnCals_Click(object sender, EventArgs e)
        {
            calculate();
        }

        private void AddCtoreSale()
        {
            Tobbaco = Convert.ToDouble(txttobbaco.Text);
            Online_Lottoo = Convert.ToDouble(txtonlinelotto.Text);
            Scratch_Lotto = Convert.ToDouble(txtscratchlotto.Text);
            Phone_Cards = Convert.ToDouble(txtphonecard.Text);
            Gift_Cards = Convert.ToDouble(txtgiftcard.Text);
            Shell_Gift_Cards = Convert.ToDouble(txtshelGisftcard.Text);
            CstoreSale = Convert.ToDouble(txtCStoreSale.Text);
            Store_Others = CstoreSale - (Tobbaco + Online_Lottoo + Scratch_Lotto + Gift_Cards + Phone_Cards + Shell_Gift_Cards);
            txtCalstoreother.Text = Store_Others.ToString("0.00");
        }
        private void lottopayout()
        {
        }
        private void AddFuel()
        {
            double bronze = 0.0, Siler = 0.0, diesiel = 0.0, VPowerD = 0.0, VpowerGas = 0.0, BSDltr = 0.0, VpDvdd = 0.0, FuelComm = 0.0, Gstreceived = 0.0;

            bronze = Convert.ToDouble(txtbronzeltr.Text);
            Siler = Convert.ToDouble(txtSilverltr.Text);
            diesiel = Convert.ToDouble(txtdieselltr.Text);
            VPowerD = Convert.ToDouble(txtvpowerltr.Text);
            VpowerGas = Convert.ToDouble(txtVppltr.Text);
            txtBSDltr.Text = (BSDltr = (bronze + Siler + diesiel)).ToString();
            txtVpdvdd.Text = (VpDvdd = (VPowerD + VpowerGas)).ToString();
            FuelComm = (BSDltr * 1 / 100) + (VpDvdd * 1.25 / 100);
            Gstreceived = FuelComm * 5 / 100;
            txtfuelcommexgst.Text = FuelComm.ToString("0.00");
            txtgstreceivedfuel.Text = Gstreceived.ToString("0.00");
        }
        private void AddAirmiles()
        {
            AddSale();
            double Airmiles = 0.0, CashBankDep = 0.0, CashDrive = 0.0, LotoWinner = 0.0, TotalAirmile = 0.0, difindrcr = 0.0;
            double RadiantDebit = 0.0, RadiantCredit = 0.0;
            RadiantDebit = Convert.ToDouble(txtRadiantdb.Text);
            RadiantCredit = Convert.ToDouble(txtRadiantcr.Text);

            Airmiles = Convert.ToDouble(txtAirmiles.Text);
            CashBankDep = Convert.ToDouble(txtCashBankDep.Text);
            CashDrive = Convert.ToDouble(txtCashDriveAway.Text);
            LotoWinner = Convert.ToDouble(txtLotowinner.Text);
            txttotalFuel.Text = txtFuel.Text.ToString();
            // difindrcr = Convert.ToDouble(txtdifindrcr.Text);
            TotalAirmile = (Airmiles + CashBankDep + CashDrive + LotoWinner + (RadiantCredit + RadiantDebit));
            TotalSale = Convert.ToDouble(txtSalTotal.Text);
            txttotalFuel.Text = TotalAirmile.ToString();
            txtDiff.Text = (TotalAirmile - TotalSale).ToString("0.00");
            Discounts = Convert.ToDouble(txtDiscount.Text);
            txtActualShort.Text = ((TotalAirmile - TotalSale) - Discounts).ToString("0.00");
        }
        private void AddSettlement()
        {
            double RadiantDebit = 0.0, RadiantCredit = 0.0, ShellDebit = 0.0, ShellCredit = 0.0, diff = 0.0;
            RadiantDebit = Convert.ToDouble(txtRadiantdb.Text);
            RadiantCredit = Convert.ToDouble(txtRadiantcr.Text);
            ShellDebit = Convert.ToDouble(txtShellDebit.Text);
            ShellCredit = Convert.ToDouble(txtShellCredit.Text);
            diff = RadiantDebit - ShellDebit;
            txtRadiantdbdiff.Text = (diff).ToString("0.00");
            diff = RadiantCredit - ShellCredit;
            txtradiantcrdiff.Text = (diff).ToString("0.00");
            AddAirmiles();
        }
        private void AddCash()
        {
            double CashPayoutSp = 0.0, CashPayoutLotto = 0.0, CashPaidSP = 0.0, CashPaidEmployee = 0.0, CashPaidMngt = 0.0,
                CashSort = 0.0, Cashreceived = 0.0, CashBankDep = 0.0;
            CashPayoutSp = Convert.ToDouble(txtcashpayoutStore.Text);
            CashPayoutLotto = Convert.ToDouble(txtCashlottto.Text);
            CashPaidEmployee = Convert.ToDouble(txtCashPaidEmp.Text);
            CashPaidSP = Convert.ToDouble(txtCashPaid.Text);
            CashPaidMngt = Convert.ToDouble(txtCashpaidmgt.Text);
            CashPaidSP = Convert.ToDouble(txtCashSort.Text);
            Cashreceived = Convert.ToDouble(txtCashReceived.Text);
            CashBankDep = Convert.ToDouble(txtCashBankDep.Text);
            txtleftBankdeposit.Text = ((Cashreceived + CashBankDep) - ((CashPayoutSp + CashPayoutLotto + CashPaidSP + CashPaidEmployee + CashPaidMngt + CashSort))).ToString("0.00");
            txttotallotopayout.Text = CashPayoutLotto.ToString();
            txtActualBankDeposit.Text = txtleftBankdeposit.Text;
        }

        private void txtCardRembsment_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }

            CardRem();
        }

        private void CardRem()
        {
            double CardRembsment = 0.0, RadiantDebit = 0.0, RadiantCredit = 0.0, difindrcr = 0.0, totalfuelsale = 0.0,
                dailrentgst = 0.0, actualcommission = 0.0, totalset = 0.0, fairshare = 0.0;

            CardRembsment = Convert.ToDouble(txtCardRembsment.Text);
            RadiantDebit = Convert.ToDouble(txtRadiantdb.Text);
            RadiantCredit = Convert.ToDouble(txtRadiantcr.Text);
            difindrcr = (RadiantCredit + RadiantDebit) - CardRembsment;
            txtdifindrcr.Text = difindrcr.ToString("#.##");
            totalfuelsale = Convert.ToDouble(txttotalFuel.Text);
            dailrentgst = Convert.ToDouble(txtdailystorerntincgst.Text);
            fairshare = Convert.ToDouble(txtfairshare.Text);

            actualcommission = Convert.ToDouble(txtActualCommission.Text);
            totalset = (totalfuelsale + dailrentgst) - (RadiantCredit + RadiantDebit + actualcommission);
            txtfinalsettlement.Text = ((totalset + difindrcr) - fairshare).ToString("#.##");
            txttotalsettlement.Text = totalset.ToString("#.##");

        }

        private void dgvCstore_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                DataGridView dgv = ((DataGridView)(sender));
                if (Convert.ToBoolean(dgv.Rows[e.RowIndex].Cells[0].Value) == false)
                {
                    dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
                }

                string gridselval = "";
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    if (Convert.ToBoolean(r.Cells[0].Value))
                    {
                        int i = r.Index;

                        gridselval = gridselval + "," + dgv.Rows[i].Cells[1].Value.ToString();

                    }

                } if (gridselval.Length > 0)
                {
                    gridselval = gridselval.Remove(0, 1);
                }
                switch (dgv.Name)
                {
                    case "dgvCash":
                        _cklcash = gridselval;
                        break;
                    case "dgvCstore":
                        _cklCstoredoc = gridselval;
                        break;
                    case "dgvSettlement":
                        _cklsettlement = gridselval;
                        break;
                    case "dgvTabbacco":
                        _ckltabbacco = gridselval;
                        break;
                    case "dgvFuel":
                        _cklfuel = gridselval;
                        break;
                }
            }
        }
        private void dgvCstore_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            string fileName = "";
            if (e.RowIndex >= 0 && e.ColumnIndex > 0)
            {
                DataGridView dgv = ((DataGridView)(sender));

                fileName = dgv.Rows[e.RowIndex].Cells["FilePath"].Value.ToString();
                System.Diagnostics.Process.Start("explorer.exe", fileName);
            }
        }
    }
}
