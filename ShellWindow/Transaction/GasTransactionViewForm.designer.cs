﻿namespace Shell
{
    partial class GasTransactionViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GasTransactionViewForm));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvRecords = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbtFulecommission = new System.Windows.Forms.RadioButton();
            this.rbtSalevsGst = new System.Windows.Forms.RadioButton();
            this.btnDisplayRecord = new System.Windows.Forms.Button();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtSummary = new System.Windows.Forms.RadioButton();
            this.rbtrentgst = new System.Windows.Forms.RadioButton();
            this.rbtInvoices = new System.Windows.Forms.RadioButton();
            this.rbtDbCardSettlemet = new System.Windows.Forms.RadioButton();
            this.btnExport = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvRecords);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(236, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(636, 461);
            this.groupBox2.TabIndex = 152;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Reports";
            // 
            // dgvRecords
            // 
            this.dgvRecords.AllowDrop = true;
            this.dgvRecords.AllowUserToAddRows = false;
            this.dgvRecords.AllowUserToDeleteRows = false;
            this.dgvRecords.AllowUserToResizeColumns = false;
            this.dgvRecords.AllowUserToResizeRows = false;
            this.dgvRecords.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvRecords.BackgroundColor = System.Drawing.Color.White;
            this.dgvRecords.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRecords.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvRecords.Location = new System.Drawing.Point(3, 16);
            this.dgvRecords.MultiSelect = false;
            this.dgvRecords.Name = "dgvRecords";
            this.dgvRecords.ReadOnly = true;
            this.dgvRecords.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvRecords.RowHeadersVisible = false;
            this.dgvRecords.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvRecords.Size = new System.Drawing.Size(630, 442);
            this.dgvRecords.TabIndex = 169;
            this.dgvRecords.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRecords_CellContentClick);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnExport);
            this.panel1.Controls.Add(this.rbtDbCardSettlemet);
            this.panel1.Controls.Add(this.rbtInvoices);
            this.panel1.Controls.Add(this.rbtrentgst);
            this.panel1.Controls.Add(this.rbtFulecommission);
            this.panel1.Controls.Add(this.rbtSalevsGst);
            this.panel1.Controls.Add(this.btnDisplayRecord);
            this.panel1.Controls.Add(this.dtpTo);
            this.panel1.Controls.Add(this.dtpFrom);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.rbtSummary);
            this.panel1.Location = new System.Drawing.Point(3, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(227, 348);
            this.panel1.TabIndex = 151;
            // 
            // rbtFulecommission
            // 
            this.rbtFulecommission.AutoSize = true;
            this.rbtFulecommission.Checked = true;
            this.rbtFulecommission.Location = new System.Drawing.Point(30, 149);
            this.rbtFulecommission.Name = "rbtFulecommission";
            this.rbtFulecommission.Size = new System.Drawing.Size(140, 17);
            this.rbtFulecommission.TabIndex = 145;
            this.rbtFulecommission.TabStop = true;
            this.rbtFulecommission.Text = "FuelCommission Vs GST";
            this.rbtFulecommission.UseVisualStyleBackColor = true;
            // 
            // rbtSalevsGst
            // 
            this.rbtSalevsGst.AutoSize = true;
            this.rbtSalevsGst.Checked = true;
            this.rbtSalevsGst.Location = new System.Drawing.Point(30, 126);
            this.rbtSalevsGst.Name = "rbtSalevsGst";
            this.rbtSalevsGst.Size = new System.Drawing.Size(91, 17);
            this.rbtSalevsGst.TabIndex = 144;
            this.rbtSalevsGst.TabStop = true;
            this.rbtSalevsGst.Text = "Sales Vs GST";
            this.rbtSalevsGst.UseVisualStyleBackColor = true;
            // 
            // btnDisplayRecord
            // 
            this.btnDisplayRecord.BackColor = System.Drawing.Color.White;
            this.btnDisplayRecord.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDisplayRecord.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnDisplayRecord.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnDisplayRecord.Image = ((System.Drawing.Image)(resources.GetObject("btnDisplayRecord.Image")));
            this.btnDisplayRecord.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDisplayRecord.Location = new System.Drawing.Point(8, 267);
            this.btnDisplayRecord.Name = "btnDisplayRecord";
            this.btnDisplayRecord.Size = new System.Drawing.Size(78, 30);
            this.btnDisplayRecord.TabIndex = 143;
            this.btnDisplayRecord.Text = "&Display";
            this.btnDisplayRecord.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDisplayRecord.UseVisualStyleBackColor = false;
            this.btnDisplayRecord.Click += new System.EventHandler(this.btnDisplayRecord_Click);
            // 
            // dtpTo
            // 
            this.dtpTo.CustomFormat = "dd/MM/yyyy";
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(36, 58);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(151, 20);
            this.dtpTo.TabIndex = 6;
            // 
            // dtpFrom
            // 
            this.dtpFrom.CustomFormat = "dd/MM/yyyy";
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(36, 29);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(151, 20);
            this.dtpFrom.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "TO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "From";
            // 
            // rbtSummary
            // 
            this.rbtSummary.AutoSize = true;
            this.rbtSummary.Checked = true;
            this.rbtSummary.Location = new System.Drawing.Point(30, 103);
            this.rbtSummary.Name = "rbtSummary";
            this.rbtSummary.Size = new System.Drawing.Size(68, 17);
            this.rbtSummary.TabIndex = 1;
            this.rbtSummary.TabStop = true;
            this.rbtSummary.Text = "&Summary";
            this.rbtSummary.UseVisualStyleBackColor = true;
            // 
            // rbtrentgst
            // 
            this.rbtrentgst.AutoSize = true;
            this.rbtrentgst.Checked = true;
            this.rbtrentgst.Location = new System.Drawing.Point(30, 173);
            this.rbtrentgst.Name = "rbtrentgst";
            this.rbtrentgst.Size = new System.Drawing.Size(143, 17);
            this.rbtrentgst.TabIndex = 146;
            this.rbtrentgst.TabStop = true;
            this.rbtrentgst.Text = "Rent VS Gst Paid Report";
            this.rbtrentgst.UseVisualStyleBackColor = true;
            // 
            // rbtInvoices
            // 
            this.rbtInvoices.AutoSize = true;
            this.rbtInvoices.Checked = true;
            this.rbtInvoices.Location = new System.Drawing.Point(30, 198);
            this.rbtInvoices.Name = "rbtInvoices";
            this.rbtInvoices.Size = new System.Drawing.Size(185, 17);
            this.rbtInvoices.TabIndex = 147;
            this.rbtInvoices.TabStop = true;
            this.rbtInvoices.Text = "Cash Invoices Vs Gst Paid Report";
            this.rbtInvoices.UseVisualStyleBackColor = true;
            // 
            // rbtDbCardSettlemet
            // 
            this.rbtDbCardSettlemet.AutoSize = true;
            this.rbtDbCardSettlemet.Checked = true;
            this.rbtDbCardSettlemet.Location = new System.Drawing.Point(30, 221);
            this.rbtDbCardSettlemet.Name = "rbtDbCardSettlemet";
            this.rbtDbCardSettlemet.Size = new System.Drawing.Size(178, 17);
            this.rbtDbCardSettlemet.TabIndex = 148;
            this.rbtDbCardSettlemet.TabStop = true;
            this.rbtDbCardSettlemet.Text = "Debit/Credit Vs Card Settlement \r\n";
            this.rbtDbCardSettlemet.UseVisualStyleBackColor = true;
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.White;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExport.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.btnExport.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnExport.Image = ((System.Drawing.Image)(resources.GetObject("btnExport.Image")));
            this.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExport.Location = new System.Drawing.Point(109, 267);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(78, 30);
            this.btnExport.TabIndex = 149;
            this.btnExport.Text = "&Export";
            this.btnExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExport.UseVisualStyleBackColor = false;
            // 
            // GasTransactionViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(883, 486);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel1);
            this.Name = "GasTransactionViewForm";
            this.Text = "GasTransactionViewForm";
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecords)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgvRecords;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDisplayRecord;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtSummary;
        private System.Windows.Forms.RadioButton rbtSalevsGst;
        private System.Windows.Forms.RadioButton rbtFulecommission;
        private System.Windows.Forms.RadioButton rbtrentgst;
        private System.Windows.Forms.RadioButton rbtInvoices;
        private System.Windows.Forms.RadioButton rbtDbCardSettlemet;
        private System.Windows.Forms.Button btnExport;


    }
}