﻿namespace Shell
{
    partial class MDIParentShell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIParentShell));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.userManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.manageStoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pNLMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dashBoardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageProductToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.managePriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageMarginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eManageBackDateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.manageEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.employeeSalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadDocumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.managePurchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageSaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadSaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gasTransationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cashManagemnetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymnetToVendorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToEmployeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.watcherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.memoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pNLEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankEntriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankValidationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankAndPNLValidationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saleReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gasTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.lblUserName = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.lblTime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lnkLogoff = new System.Windows.Forms.LinkLabel();
            this.DashboardGroupBox = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CountPNL = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.CountInvoice = new System.Windows.Forms.LinkLabel();
            this.invoiceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.DashboardGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.AliceBlue;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.viewMenu,
            this.bankToolStripMenuItem,
            this.toolsMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(869, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userManagementToolStripMenuItem,
            this.newToolStripMenuItem,
            this.toolStripSeparator3,
            this.manageStoreToolStripMenuItem,
            this.vendorManagementToolStripMenuItem,
            this.toolStripSeparator4,
            this.toolStripMenuItem1,
            this.pNLMasterToolStripMenuItem,
            this.dashBoardToolStripMenuItem,
            this.manageProductToolStripMenuItem,
            this.managePriceToolStripMenuItem,
            this.manageMarginToolStripMenuItem,
            this.toolStripSeparator1,
            this.configurationToolStripMenuItem,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(55, 20);
            this.fileMenu.Text = "&Master";
            // 
            // userManagementToolStripMenuItem
            // 
            this.userManagementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createUserToolStripMenuItem,
            this.changePasswordToolStripMenuItem});
            this.userManagementToolStripMenuItem.Name = "userManagementToolStripMenuItem";
            this.userManagementToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.userManagementToolStripMenuItem.Text = "User Management";
            // 
            // createUserToolStripMenuItem
            // 
            this.createUserToolStripMenuItem.Name = "createUserToolStripMenuItem";
            this.createUserToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.createUserToolStripMenuItem.Text = "Create User ";
            this.createUserToolStripMenuItem.Click += new System.EventHandler(this.createUserToolStripMenuItem_Click);
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            this.changePasswordToolStripMenuItem.Click += new System.EventHandler(this.changePasswordToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShowShortcutKeys = false;
            this.newToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.newToolStripMenuItem.Text = "&Manage Location";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.ShowNewForm);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(182, 6);
            // 
            // manageStoreToolStripMenuItem
            // 
            this.manageStoreToolStripMenuItem.Name = "manageStoreToolStripMenuItem";
            this.manageStoreToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.manageStoreToolStripMenuItem.Text = "Manage Store";
            this.manageStoreToolStripMenuItem.Click += new System.EventHandler(this.manageStoreToolStripMenuItem_Click);
            // 
            // vendorManagementToolStripMenuItem
            // 
            this.vendorManagementToolStripMenuItem.Name = "vendorManagementToolStripMenuItem";
            this.vendorManagementToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.vendorManagementToolStripMenuItem.Text = "Vendor Management";
            this.vendorManagementToolStripMenuItem.Click += new System.EventHandler(this.vendorManagementToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(182, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(185, 22);
            this.toolStripMenuItem1.Text = "Bank Master";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // pNLMasterToolStripMenuItem
            // 
            this.pNLMasterToolStripMenuItem.Name = "pNLMasterToolStripMenuItem";
            this.pNLMasterToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.pNLMasterToolStripMenuItem.Text = "PNL Master";
            this.pNLMasterToolStripMenuItem.Click += new System.EventHandler(this.pNLMasterToolStripMenuItem_Click);
            // 
            // dashBoardToolStripMenuItem
            // 
            this.dashBoardToolStripMenuItem.Name = "dashBoardToolStripMenuItem";
            this.dashBoardToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.dashBoardToolStripMenuItem.Text = "DashBoard";
            this.dashBoardToolStripMenuItem.Click += new System.EventHandler(this.dashBoardToolStripMenuItem_Click);
            // 
            // manageProductToolStripMenuItem
            // 
            this.manageProductToolStripMenuItem.Name = "manageProductToolStripMenuItem";
            this.manageProductToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.manageProductToolStripMenuItem.Text = "Manage Product";
            this.manageProductToolStripMenuItem.Click += new System.EventHandler(this.manageProductToolStripMenuItem_Click);
            // 
            // managePriceToolStripMenuItem
            // 
            this.managePriceToolStripMenuItem.Name = "managePriceToolStripMenuItem";
            this.managePriceToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.managePriceToolStripMenuItem.Text = "Manage Price";
            this.managePriceToolStripMenuItem.Click += new System.EventHandler(this.managePriceToolStripMenuItem_Click);
            // 
            // manageMarginToolStripMenuItem
            // 
            this.manageMarginToolStripMenuItem.Name = "manageMarginToolStripMenuItem";
            this.manageMarginToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.manageMarginToolStripMenuItem.Text = "Manage Margin";
            this.manageMarginToolStripMenuItem.Click += new System.EventHandler(this.manageMarginToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(182, 6);
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eManageBackDateToolStripMenuItem});
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.configurationToolStripMenuItem.Text = "Configuration";
            // 
            // eManageBackDateToolStripMenuItem
            // 
            this.eManageBackDateToolStripMenuItem.Name = "eManageBackDateToolStripMenuItem";
            this.eManageBackDateToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.eManageBackDateToolStripMenuItem.Text = "Manage Back Date Entry";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(182, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageEmployeeToolStripMenuItem,
            this.toolStripSeparator7,
            this.employeeSalaryToolStripMenuItem});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(142, 20);
            this.editMenu.Text = "&EmployeeManagement";
            // 
            // manageEmployeeToolStripMenuItem
            // 
            this.manageEmployeeToolStripMenuItem.Name = "manageEmployeeToolStripMenuItem";
            this.manageEmployeeToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.manageEmployeeToolStripMenuItem.Text = "Manage Employee";
            this.manageEmployeeToolStripMenuItem.Click += new System.EventHandler(this.manageEmployeeToolStripMenuItem_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(169, 6);
            // 
            // employeeSalaryToolStripMenuItem
            // 
            this.employeeSalaryToolStripMenuItem.Name = "employeeSalaryToolStripMenuItem";
            this.employeeSalaryToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.employeeSalaryToolStripMenuItem.Text = "Employee Salary";
            this.employeeSalaryToolStripMenuItem.Click += new System.EventHandler(this.employeeSalaryToolStripMenuItem_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uploadDocumentsToolStripMenuItem,
            this.managePurchaseToolStripMenuItem,
            this.manageSaleToolStripMenuItem,
            this.gasTransationToolStripMenuItem,
            this.cashManagemnetToolStripMenuItem,
            this.watcherToolStripMenuItem,
            this.memoToolStripMenuItem,
            this.transactionViewToolStripMenuItem,
            this.invoiceToolStripMenuItem,
            this.pNLEntryToolStripMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(80, 20);
            this.viewMenu.Text = "&Transaction";
            // 
            // uploadDocumentsToolStripMenuItem
            // 
            this.uploadDocumentsToolStripMenuItem.Name = "uploadDocumentsToolStripMenuItem";
            this.uploadDocumentsToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.uploadDocumentsToolStripMenuItem.Text = "Upload Documents";
            this.uploadDocumentsToolStripMenuItem.Click += new System.EventHandler(this.uploadDocumentsToolStripMenuItem_Click);
            // 
            // managePurchaseToolStripMenuItem
            // 
            this.managePurchaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseToolStripMenuItem,
            this.purchaseReturnToolStripMenuItem});
            this.managePurchaseToolStripMenuItem.Name = "managePurchaseToolStripMenuItem";
            this.managePurchaseToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.managePurchaseToolStripMenuItem.Text = "Manage Purchase";
            // 
            // purchaseToolStripMenuItem
            // 
            this.purchaseToolStripMenuItem.Name = "purchaseToolStripMenuItem";
            this.purchaseToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.purchaseToolStripMenuItem.Text = "Purchase";
            // 
            // purchaseReturnToolStripMenuItem
            // 
            this.purchaseReturnToolStripMenuItem.Name = "purchaseReturnToolStripMenuItem";
            this.purchaseReturnToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.purchaseReturnToolStripMenuItem.Text = "Purchase Return";
            // 
            // manageSaleToolStripMenuItem
            // 
            this.manageSaleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uploadSaleToolStripMenuItem,
            this.saleReturnToolStripMenuItem});
            this.manageSaleToolStripMenuItem.Name = "manageSaleToolStripMenuItem";
            this.manageSaleToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.manageSaleToolStripMenuItem.Text = "Manage Sale";
            // 
            // uploadSaleToolStripMenuItem
            // 
            this.uploadSaleToolStripMenuItem.Name = "uploadSaleToolStripMenuItem";
            this.uploadSaleToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.uploadSaleToolStripMenuItem.Text = "Upload Sale";
            this.uploadSaleToolStripMenuItem.Click += new System.EventHandler(this.uploadSaleToolStripMenuItem_Click);
            // 
            // saleReturnToolStripMenuItem
            // 
            this.saleReturnToolStripMenuItem.Name = "saleReturnToolStripMenuItem";
            this.saleReturnToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.saleReturnToolStripMenuItem.Text = "Sale Return";
            // 
            // gasTransationToolStripMenuItem
            // 
            this.gasTransationToolStripMenuItem.Name = "gasTransationToolStripMenuItem";
            this.gasTransationToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.gasTransationToolStripMenuItem.Text = "Gas Transation";
            this.gasTransationToolStripMenuItem.Click += new System.EventHandler(this.gasTransationToolStripMenuItem_Click);
            // 
            // cashManagemnetToolStripMenuItem
            // 
            this.cashManagemnetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paymnetToVendorToolStripMenuItem,
            this.paymentToEmployeeToolStripMenuItem,
            this.paymentToManagementToolStripMenuItem});
            this.cashManagemnetToolStripMenuItem.Name = "cashManagemnetToolStripMenuItem";
            this.cashManagemnetToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.cashManagemnetToolStripMenuItem.Text = "Cash Managemnet";
            // 
            // paymnetToVendorToolStripMenuItem
            // 
            this.paymnetToVendorToolStripMenuItem.Name = "paymnetToVendorToolStripMenuItem";
            this.paymnetToVendorToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.paymnetToVendorToolStripMenuItem.Text = "Payment To Vendor";
            this.paymnetToVendorToolStripMenuItem.Click += new System.EventHandler(this.paymnetToVendorToolStripMenuItem_Click);
            // 
            // paymentToEmployeeToolStripMenuItem
            // 
            this.paymentToEmployeeToolStripMenuItem.Name = "paymentToEmployeeToolStripMenuItem";
            this.paymentToEmployeeToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.paymentToEmployeeToolStripMenuItem.Text = "Payment To Employee";
            this.paymentToEmployeeToolStripMenuItem.Click += new System.EventHandler(this.paymentToEmployeeToolStripMenuItem_Click);
            // 
            // paymentToManagementToolStripMenuItem
            // 
            this.paymentToManagementToolStripMenuItem.Name = "paymentToManagementToolStripMenuItem";
            this.paymentToManagementToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.paymentToManagementToolStripMenuItem.Text = "Payment To Management";
            this.paymentToManagementToolStripMenuItem.Click += new System.EventHandler(this.paymentToManagementToolStripMenuItem_Click);
            // 
            // watcherToolStripMenuItem
            // 
            this.watcherToolStripMenuItem.Name = "watcherToolStripMenuItem";
            this.watcherToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.watcherToolStripMenuItem.Text = "Watcher";
            // 
            // memoToolStripMenuItem
            // 
            this.memoToolStripMenuItem.Name = "memoToolStripMenuItem";
            this.memoToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.memoToolStripMenuItem.Text = "Memo";
            // 
            // transactionViewToolStripMenuItem
            // 
            this.transactionViewToolStripMenuItem.Name = "transactionViewToolStripMenuItem";
            this.transactionViewToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.transactionViewToolStripMenuItem.Text = "TransactionView";
            this.transactionViewToolStripMenuItem.Click += new System.EventHandler(this.transactionViewToolStripMenuItem_Click);
            // 
            // invoiceToolStripMenuItem
            // 
            this.invoiceToolStripMenuItem.Name = "invoiceToolStripMenuItem";
            this.invoiceToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.invoiceToolStripMenuItem.Text = "Invoice";
            this.invoiceToolStripMenuItem.Click += new System.EventHandler(this.invoiceToolStripMenuItem_Click);
            // 
            // pNLEntryToolStripMenuItem
            // 
            this.pNLEntryToolStripMenuItem.Name = "pNLEntryToolStripMenuItem";
            this.pNLEntryToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.pNLEntryToolStripMenuItem.Text = "PNL Entry";
            this.pNLEntryToolStripMenuItem.Click += new System.EventHandler(this.pNLEntryToolStripMenuItem_Click);
            // 
            // bankToolStripMenuItem
            // 
            this.bankToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bankEntriesToolStripMenuItem,
            this.bankValidationToolStripMenuItem,
            this.bankAndPNLValidationToolStripMenuItem});
            this.bankToolStripMenuItem.Name = "bankToolStripMenuItem";
            this.bankToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.bankToolStripMenuItem.Text = "Bank";
            // 
            // bankEntriesToolStripMenuItem
            // 
            this.bankEntriesToolStripMenuItem.Name = "bankEntriesToolStripMenuItem";
            this.bankEntriesToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.bankEntriesToolStripMenuItem.Text = "Bank Entries";
            this.bankEntriesToolStripMenuItem.Click += new System.EventHandler(this.bankEntriesToolStripMenuItem_Click);
            // 
            // bankValidationToolStripMenuItem
            // 
            this.bankValidationToolStripMenuItem.Name = "bankValidationToolStripMenuItem";
            this.bankValidationToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.bankValidationToolStripMenuItem.Text = "Bank and Invoice Validation";
            this.bankValidationToolStripMenuItem.Click += new System.EventHandler(this.bankValidationToolStripMenuItem_Click);
            // 
            // bankAndPNLValidationToolStripMenuItem
            // 
            this.bankAndPNLValidationToolStripMenuItem.Name = "bankAndPNLValidationToolStripMenuItem";
            this.bankAndPNLValidationToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.bankAndPNLValidationToolStripMenuItem.Text = "Bank and PNL Validation";
            this.bankAndPNLValidationToolStripMenuItem.Click += new System.EventHandler(this.bankAndPNLValidationToolStripMenuItem_Click);
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saleReportToolStripMenuItem,
            this.purchaseReportToolStripMenuItem,
            this.gasTransactionToolStripMenuItem,
            this.employeeReportToolStripMenuItem,
            this.invoiceReportToolStripMenuItem});
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(54, 20);
            this.toolsMenu.Text = "&Report";
            // 
            // saleReportToolStripMenuItem
            // 
            this.saleReportToolStripMenuItem.Name = "saleReportToolStripMenuItem";
            this.saleReportToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.saleReportToolStripMenuItem.Text = "Sale Report";
            // 
            // purchaseReportToolStripMenuItem
            // 
            this.purchaseReportToolStripMenuItem.Name = "purchaseReportToolStripMenuItem";
            this.purchaseReportToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.purchaseReportToolStripMenuItem.Text = "Purchase Report";
            // 
            // gasTransactionToolStripMenuItem
            // 
            this.gasTransactionToolStripMenuItem.Name = "gasTransactionToolStripMenuItem";
            this.gasTransactionToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.gasTransactionToolStripMenuItem.Text = "Gas Transaction";
            // 
            // employeeReportToolStripMenuItem
            // 
            this.employeeReportToolStripMenuItem.Name = "employeeReportToolStripMenuItem";
            this.employeeReportToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.employeeReportToolStripMenuItem.Text = "Employee Report";
            this.employeeReportToolStripMenuItem.Click += new System.EventHandler(this.employeeReportToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 431);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(632, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Visible = false;
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusLabel.Text = "Status";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblUserName,
            this.toolStripSeparator2,
            this.lblTime,
            this.toolStripSeparator6});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(869, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // lblUserName
            // 
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(86, 22);
            this.lblUserName.Text = "toolStripLabel1";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // lblTime
            // 
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(86, 22);
            this.lblTime.Text = "toolStripLabel2";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lnkLogoff
            // 
            this.lnkLogoff.AutoSize = true;
            this.lnkLogoff.Font = new System.Drawing.Font("Arial Narrow", 13.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkLogoff.Location = new System.Drawing.Point(730, 24);
            this.lnkLogoff.Name = "lnkLogoff";
            this.lnkLogoff.Size = new System.Drawing.Size(55, 22);
            this.lnkLogoff.TabIndex = 6;
            this.lnkLogoff.TabStop = true;
            this.lnkLogoff.Text = "Log Off";
            this.lnkLogoff.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLogoff_LinkClicked);
            // 
            // DashboardGroupBox
            // 
            this.DashboardGroupBox.Controls.Add(this.label2);
            this.DashboardGroupBox.Controls.Add(this.CountPNL);
            this.DashboardGroupBox.Controls.Add(this.label1);
            this.DashboardGroupBox.Controls.Add(this.CountInvoice);
            this.DashboardGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DashboardGroupBox.Location = new System.Drawing.Point(12, 63);
            this.DashboardGroupBox.Name = "DashboardGroupBox";
            this.DashboardGroupBox.Size = new System.Drawing.Size(323, 180);
            this.DashboardGroupBox.TabIndex = 12;
            this.DashboardGroupBox.TabStop = false;
            this.DashboardGroupBox.Text = "Dashboard";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(16, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 24);
            this.label2.TabIndex = 15;
            this.label2.Text = "Total Open PNL";
            // 
            // CountPNL
            // 
            this.CountPNL.AutoSize = true;
            this.CountPNL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CountPNL.Location = new System.Drawing.Point(241, 107);
            this.CountPNL.Name = "CountPNL";
            this.CountPNL.Size = new System.Drawing.Size(18, 20);
            this.CountPNL.TabIndex = 14;
            this.CountPNL.TabStop = true;
            this.CountPNL.Text = "0";
            this.CountPNL.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CountPNL_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(16, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 24);
            this.label1.TabIndex = 13;
            this.label1.Text = "Total Open Invoices";
            // 
            // CountInvoice
            // 
            this.CountInvoice.AutoSize = true;
            this.CountInvoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CountInvoice.Location = new System.Drawing.Point(241, 56);
            this.CountInvoice.Name = "CountInvoice";
            this.CountInvoice.Size = new System.Drawing.Size(18, 20);
            this.CountInvoice.TabIndex = 12;
            this.CountInvoice.TabStop = true;
            this.CountInvoice.Text = "0";
            this.CountInvoice.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.CountInvoice_LinkClicked);
            // 
            // invoiceReportToolStripMenuItem
            // 
            this.invoiceReportToolStripMenuItem.Name = "invoiceReportToolStripMenuItem";
            this.invoiceReportToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.invoiceReportToolStripMenuItem.Text = "Invoice Report";
            // 
            // MDIParentShell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(869, 453);
            this.Controls.Add(this.DashboardGroupBox);
            this.Controls.Add(this.lnkLogoff);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIParentShell";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shell";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MDIParent1_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.DashboardGroupBox.ResumeLayout(false);
            this.DashboardGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem manageStoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageProductToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem managePriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageMarginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageEmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeSalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem managePurchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageSaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadSaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleReturnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gasTransactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eManageBackDateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gasTransationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem watcherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem memoToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel lblUserName;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel lblTime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem vendorManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cashManagemnetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymnetToVendorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToEmployeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadDocumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem userManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.LinkLabel lnkLogoff;
        private System.Windows.Forms.ToolStripMenuItem dashBoardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionViewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pNLMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem invoiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankEntriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankValidationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pNLEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankAndPNLValidationToolStripMenuItem;
        private System.Windows.Forms.GroupBox DashboardGroupBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel CountPNL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel CountInvoice;
        private System.Windows.Forms.ToolStripMenuItem invoiceReportToolStripMenuItem;
    }
}



