﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;

namespace Shell
{
    public partial class LocationMaster : Form
    {
        public LocationMaster()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLocationCode.Text))
            {
                MessageBox.Show("Please Enter the location Code "); txtLocationCode.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtLocation.Text))
            {
                MessageBox.Show("Please Enter the location name "); txtLocation.Focus();
                return;
            }
            BAL_Location _obj = new BAL_Location();
            BAL_Location _objLocation = new BAL_Location();
            if (_objLocation.GetLocationlist().Where(p => p.LocationCode == txtLocationCode.Text).Count() > 0)
            {
                MessageBox.Show("Location Code Already Exists!.");
                return;
            }
            int Result = _obj.ManageLocation(0, txtLocationCode.Text, txtLocation.Text, txtLocationDescription.Text, Utility.BA_Commman._userId, 1);
            if (Result == 1)
            {
                MessageBox.Show("Location Created Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }
        public void ClearValue()
        {
            txtLocation.Text = "";
            txtLocationCode.Text = "";
            txtLocationDescription.Text = "";
            BindGrid();
            btnUpdate.Enabled = false;
            BtnDelete.Enabled = false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLocationCode.Text))
            {
                MessageBox.Show("Please Enter the location Code "); txtLocationCode.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtLocation.Text))
            {
                MessageBox.Show("Please Enter the location name "); txtLocation.Focus();
                return;
            }

            BAL_Location _obj = new BAL_Location();
            int Result = _obj.ManageLocation(_locationid, txtLocationCode.Text, txtLocation.Text, txtLocationDescription.Text, Utility.BA_Commman._userId, 2);
            if (Result == 1)
            {
                MessageBox.Show("Location Updated Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLocation.Text))
            {
                MessageBox.Show("Please Enter the location name "); txtLocation.Focus();
                return;
            }
            BAL_Location _obj = new BAL_Location();
            int Result = _obj.DeleteLocation(_locationid, Utility.BA_Commman._userId);
            if (Result == 1)
            {
                MessageBox.Show("Location Deleted Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }
        public void BindGrid()
        {
            BAL_Location _objLocation = new BAL_Location();
            dgvLocation.DataSource = _objLocation.GetLocationlist();
            dgvLocation.Columns[0].Visible = false;
            dgvLocation.Columns["LocationId"].Visible = false;
            dgvLocation.Columns["CreatedBy"].Visible = false;
        }

        public int _locationid;
        private void LocationMaster_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void dgvLocation_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    _locationid = int.Parse(dgvLocation.Rows[e.RowIndex].Cells["LocationId"].Value.ToString());
                    txtLocationCode.Text = dgvLocation.Rows[e.RowIndex].Cells["Locationcode"].Value.ToString();
                    txtLocation.Text = dgvLocation.Rows[e.RowIndex].Cells["LocationName"].Value.ToString();
                    txtLocationDescription.Text = dgvLocation.Rows[e.RowIndex].Cells["LocationDescription"].Value.ToString();
                    btnUpdate.Enabled = true;
                    BtnDelete.Enabled = true;
                }
                else
                {
                    btnUpdate.Enabled = false;
                    BtnDelete.Enabled = false;
                }

            }
        }
    }
}
