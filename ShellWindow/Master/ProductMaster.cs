﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class ProductMaster : Form
    {
        public ProductMaster()
        {
            InitializeComponent();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Saved!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Updated!");
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Record Deleted!");
        }
        private void ProductMaster_Load(object sender, EventArgs e)
        {
            grpProductList.Visible = true;
            grpManageProduct.Visible = false;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            grpProductList.Visible = true;
            grpManageProduct.Visible = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            grpProductList.Visible = false;
            grpManageProduct.Visible = true;

        }
    }
}
