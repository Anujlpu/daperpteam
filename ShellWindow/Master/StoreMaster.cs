﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using System.Text.RegularExpressions;

namespace Shell
{
    public partial class StoreMaster : Form
    {
        public StoreMaster()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void BindGrid()
        {
            
            BAL_Store _objStore
                = new BAL_Store();
            dgvStore.DataSource = _objStore.GetStorelist();
            dgvStore.Columns[0].Visible = false;
            dgvStore.Columns["StoreID"].Visible = false;
            dgvStore.Columns["StoreLocationId"].Visible = false;
            dgvStore.Columns["FaxNo"].Visible = false;
            dgvStore.Columns["StoreID"].Visible = false;
            dgvStore.Columns["UserID"].Visible = false;

        }
        string str = "0123456789.";
        private void CtrlKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }
        }


        private void CtrlKeyPress1(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) > 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }
        }

        private void StoreMaster_Load(object sender, EventArgs e)
        {
            BindGrid(); binddropdown();
            ddlApplicationStatus.SelectedItem = "--Select--";
        }
        public void binddropdown()
        {
            BAL.BAL_Location _objLocation = new BAL.BAL_Location();
            ddlStoreLocation.DataSource = _objLocation.GetLocationlist();
            ddlStoreLocation.DisplayMember = "LocationName";
            ddlStoreLocation.ValueMember = "LocationId";
        }
        public void ClearValue()
        {
            // Random _ram = new Random();
            //txtOutletNo.Text = "SHST" + _ram.Next(999999).ToString();
            txtOutletNo.Text = "";
            txtStoreAddress.Text = "";
            BindGrid();
            btnUpdate.Enabled = false;
            BtnDelete.Enabled = false;
            binddropdown();
            txtPhoneNo.Text = "";
            txtEmailId.Text = "";
            txtFaxNo.Text = "";
            txtStorreBrand.Text = "";
            txtOutletNo.Text = "";
            txtNoofEmp.Text = "0";
            txtNoofContractor.Text = "0";
            ddlApplicationStatus.SelectedText = "--Select--";
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtOutletNo.Text))
            {
                MessageBox.Show("Please Enter the Store Code");
                return;
            }
            if (string.IsNullOrEmpty(txtStoreName.Text))
            {
                MessageBox.Show("Please Enter the Store Name");
                return;
            }
            if (string.IsNullOrEmpty(txtPhoneNo.Text))
            {
                MessageBox.Show("Please Enter the PhoneNo");
                return;
            }
            if (ddlApplicationStatus.SelectedText == "--Select--")
            {
                MessageBox.Show("Please Select Application Status");
                ddlApplicationStatus.Focus();
                return;
            }
            BAL_Store _objStore = new BAL_Store();
            int Result = _objStore.ManageStore(0, txtOutletNo.Text, txtStoreName.Text, txtStoreAddress.Text, int.Parse(ddlStoreLocation.SelectedValue.ToString()), Utility.BA_Commman._userId, 1, txtPhoneNo.Text, txtEmailId.Text, txtFaxNo.Text, txtStorreBrand.Text, txtOutletNo.Text, int.Parse(txtNoofEmp.Text), int.Parse(txtNoofContractor.Text), ddlApplicationStatus.Text, dtpAppDate.Value);
            if (Result == 1)
            {
                MessageBox.Show("Store Created Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }
        public int StoreId = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtStoreName.Text))
            {
                MessageBox.Show("Please Enter the Store Name");
                return;
            }
            if (string.IsNullOrEmpty(txtPhoneNo.Text))
            {
                MessageBox.Show("Please Enter the PhoneNo");
                return;
            }

            if (ddlApplicationStatus.SelectedItem.ToString() == "--Select--")
            {
                MessageBox.Show("Please Select Application Status");
                ddlApplicationStatus.Focus();
                return;
            }
            BAL_Store _objStore = new BAL_Store();
            int Result = _objStore.ManageStore(StoreId, txtOutletNo.Text, txtStoreName.Text, txtStoreAddress.Text, int.Parse(ddlStoreLocation.SelectedValue.ToString()), Utility.BA_Commman._userId, 2, txtPhoneNo.Text, txtEmailId.Text, txtFaxNo.Text, txtStorreBrand.Text, txtOutletNo.Text, int.Parse(txtNoofEmp.Text), int.Parse(txtNoofContractor.Text), ddlApplicationStatus.Text, dtpAppDate.Value);
            if (Result == 1)
            {
                MessageBox.Show("Store Updated Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {


            BAL_Store _objStore = new BAL_Store();
            int Result = _objStore.DeleteStore(StoreId, Utility.BA_Commman._userId);
            if (Result == 1)
            {
                MessageBox.Show("Store Deleted Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        private void dgvStore_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    StoreId = int.Parse(dgvStore.Rows[e.RowIndex].Cells["StoreId"].Value.ToString());
                    txtOutletNo.Text = dgvStore.Rows[e.RowIndex].Cells["StoreCode"].Value.ToString();
                    txtStoreName.Text = dgvStore.Rows[e.RowIndex].Cells["StoreName"].Value.ToString();
                    txtStoreAddress.Text = dgvStore.Rows[e.RowIndex].Cells["StoreAddress"].Value.ToString();
                    txtPhoneNo.Text = dgvStore.Rows[e.RowIndex].Cells["PhoneNo"].Value.ToString();
                    txtEmailId.Text = dgvStore.Rows[e.RowIndex].Cells["EmailId"].Value.ToString();
                    txtFaxNo.Text = dgvStore.Rows[e.RowIndex].Cells["FaxNo"].Value.ToString();
                    txtStorreBrand.Text = dgvStore.Rows[e.RowIndex].Cells["StoreBrand"].Value.ToString();
                    // txtOutletNo.Text = dgvStore.Rows[e.RowIndex].Cells["OutletNo"].Value.ToString();
                    txtNoofEmp.Text = dgvStore.Rows[e.RowIndex].Cells["EmpCount"].Value.ToString();
                    txtNoofContractor.Text = dgvStore.Rows[e.RowIndex].Cells["ContractorCount"].Value.ToString();
                    ddlStoreLocation.SelectedValue = int.Parse(dgvStore.Rows[e.RowIndex].Cells["StoreLocationId"].Value.ToString());
                    ddlApplicationStatus.Text = dgvStore.Rows[e.RowIndex].Cells["ApplicationStatus"].Value.ToString();
                    dtpAppDate.Value = Convert.ToDateTime(dgvStore.Rows[e.RowIndex].Cells["AppDate"].Value.ToString());
                    btnUpdate.Enabled = true;
                    BtnDelete.Enabled = true;
                }
                else
                {
                    btnUpdate.Enabled = false;
                    BtnDelete.Enabled = false;
                }
            }
        }

        private void txtEmailId_Leave(object sender, EventArgs e)
        {
            if (!Utility.BA_Commman.ValidateEmail(txtEmailId.Text) == true)
                MessageBox.Show("Enter valid Email Id");
           

        }





    }
}
