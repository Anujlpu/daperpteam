﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class FrmVisaDetails : Form
    {
        public FrmVisaDetails()
        {
            InitializeComponent();
        }

        public DateTime VisaIssueDate { get; set; }
        public DateTime VisaExpireDate { get; set; }
        public string VisaDescription { get; set; }
        private void FrmVisaDetails_Load(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            VisaIssueDate = dtpvisaIssueDate.Value;
            VisaExpireDate = dtpExpireDate.Value;
            VisaDescription  = txtDescription.Text;
            this.Close();
        }
    }
}
