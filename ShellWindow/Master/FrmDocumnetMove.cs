﻿using Framework;
using System;
using System.IO;
using System.Windows.Forms;

namespace Shell
{
    public partial class FrmDocumentMove : Form
    {
        public FrmDocumentMove()
        {
            InitializeComponent();
        }

        private void FrmDocumentMove_Load(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                ddlDoctype.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }
        BAL.BAL_FuelTransaction _ObjDoc;
        public void BindGrid()
        {
            try
            {
                _ObjDoc = new BAL.BAL_FuelTransaction();
                dgvDocumentlis.DataSource = _ObjDoc.ListGetDocsList(dtpTransactionDate.Value);
                dgvDocumentlis.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtIdentity.Text == "")
                {
                    MessageBox.Show("Enter the Documnet Name");
                    txtIdentity.Focus();
                    return;
                }
                if (ddlDoctype.Text == "--Select--")
                {
                    MessageBox.Show("Select the Doc Type");
                    ddlDoctype.Focus();
                    return;
                }
                string root = System.Configuration.ConfigurationSettings.AppSettings["FileUploadPath"];
                string subdir = root + dtpTransactionDate.Value.ToString("yyyyMMdd");
                // If directory does not exist, create it. 
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }
                else if (!Directory.Exists(subdir))
                {
                    Directory.CreateDirectory(subdir);
                }

                if (Openpdf.FileName.ToString() == "")
                {
                    MessageBox.Show("Please Select the File");
                    return;
                }
                if (Openpdf.FileName.ToString() == "")
                {
                    MessageBox.Show("Please Select the File");
                    return;
                }
                string file = DateTime.Now.ToString("yyyyMMddHHmmss");

                file = subdir + "\\" + file + "_" + Openpdf.SafeFileName;
                _ObjDoc = new BAL.BAL_FuelTransaction();
                if (_ObjDoc.ManageDocs(dtpTransactionDate.Value, ddlDoctype.Text, txtIdentity.Text, file) == 1)
                {

                    File.Copy(((System.Windows.Forms.FileDialog)(Openpdf)).FileName, file);

                    MessageBox.Show("Docs Saved in System Sucessfully");

                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }


        }
        public void ClearValue()
        {

            txtIdentity.Text = "";
            pdfUploadbox.Text = "";
            BindGrid();
            BtnDelete.Enabled = false;
            BtnSave.Enabled = true;

        }

        OpenFileDialog Openpdf = new OpenFileDialog();
        private void btnbrowse_Click(object sender, EventArgs e)
        {

            // Openpdf.Filter = "All files|*.*|All files|*.*;";
            if (Openpdf.ShowDialog() == DialogResult.OK)
            {
                string pdfLog = Openpdf.FileName.ToString();
                pdfUploadbox.Text = pdfLog;
            }
            if (Openpdf.FileName.ToString() != "")
            {
                if (ddlDoctype.SelectedItem != "Others")
                {
                    txtIdentity.Text = Openpdf.SafeFileName;
                }
                //  picdoc.Image = new Bitmap(Openpdf.FileName);
            }
        }
        string FilePath = "";
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(FilePath))
                    File.Delete(FilePath);
                if (_ObjDoc.Deletedocs(DocId) == 1)
                {

                    MessageBox.Show("Document deleted successfully.");

                }
                else
                {
                    MessageBox.Show("There is some issue . Please try later!");

                }
                ClearValue();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private int DocId;
        private void dgvDocumentlis_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (dg == DialogResult.Yes)
                    {
                        DocId = int.Parse(dgvDocumentlis.Rows[e.RowIndex].Cells["DocId"].Value.ToString());
                        FilePath = (dgvDocumentlis.Rows[e.RowIndex].Cells["FilePath"].Value.ToString());
                        System.Diagnostics.Process.Start("explorer.exe", FilePath);
                        BtnDelete.Enabled = true;
                        BtnSave.Enabled = false;
                    }
                    else
                    {
                        BtnDelete.Enabled = false;
                        BtnSave.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Shell_Status.ERR_UnhandledException + " " + ex.InnerException);
            }
        }

        private void ddlDoctype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDoctype.SelectedItem == "Others")
            {
                txtIdentity.ReadOnly = false;
            }
            else
            {
                txtIdentity.ReadOnly = true;
            }

        }
    }
}
