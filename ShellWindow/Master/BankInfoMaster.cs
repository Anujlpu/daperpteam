﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
namespace Shell.Master
{
    public partial class BankInfoMaster : Form
    {
        public BankInfoMaster()
        {
            InitializeComponent();
        }

        #region Private Methods

        private void SaveBankInfo()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    MST_BankInfoMaster bankInfo = new MST_BankInfoMaster();
                    bankInfo.BankName = BankNametextBox.Text;
                    bankInfo.AccountNumber = AccountNumtextBox.Text;
                    bankInfo.Address = AddressrichTextBox.Text;
                    bankInfo.IsActive = true;
                    context.MST_BankInfoMaster.Add(bankInfo);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Please contact administrator.");
            }
        }

        private void UpdateBankInfo()
        {
            if (BankID > 0)
            {
                using (ShellEntities context = new ShellEntities())
                {
                    MST_BankInfoMaster bankInfo = context.MST_BankInfoMaster.Where(s => s.ID == BankID && s.IsActive == true).FirstOrDefault();
                    bankInfo.BankName = BankNametextBox.Text;
                    bankInfo.AccountNumber = AccountNumtextBox.Text;
                    bankInfo.Address = AddressrichTextBox.Text;
                    bankInfo.IsActive = true;
                    context.SaveChanges();
                    BtnSave.Enabled = true;
                    btnUpdate.Enabled = false;
                    BtnDelete.Enabled = false;
                    BindBankInfo();
                }
            }
            else
            {
                MessageBox.Show("Please double click grid cell.");
            }
        }

        private void BindBankInfo()
        {
            try
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var bankList = (from b in context.MST_BankInfoMaster
                                    where b.IsActive == true
                                    select new
                                    {
                                        b.ID,
                                        b.BankName,
                                        b.AccountNumber,
                                        b.Address
                                    }).ToList();
                    BankInfodataGridView.DataSource = bankList;
                    this.BankInfodataGridView.Columns[0].Visible = false;
                }


            }
            catch (Exception e)
            {
                MessageBox.Show("Please contact administrator-" + e.InnerException);
            }
        }
        #endregion

        #region Form Events
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void AccountNumtextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(BankNametextBox.Text))
            {
                MessageBox.Show("Please enter bank name.");
                BankNametextBox.Focus();
                return;
            }
            else if (string.IsNullOrEmpty(AccountNumtextBox.Text))
            {
                MessageBox.Show("Please enter account number.");
                AccountNumtextBox.Focus();
                return;
            }
            else if (string.IsNullOrEmpty(AddressrichTextBox.Text))
            {
                MessageBox.Show("Please enter bank address.");
                AddressrichTextBox.Focus();
                return;
            }
            else
            {
                SaveBankInfo();
            }

        }

        private void Clear()
        {

        }
        #endregion

        private int BankID = 0;
        private void BankInfodataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow row = this.BankInfodataGridView.Rows[e.RowIndex];
                int id = Convert.ToInt32(row.Cells[0].Value);
                using (ShellEntities context = new ShellEntities())
                {
                    MST_BankInfoMaster bankInfo = context.MST_BankInfoMaster.Where(s => s.ID == id && s.IsActive == true).FirstOrDefault();
                    BankNametextBox.Text = bankInfo.BankName;
                    AccountNumtextBox.Text = bankInfo.AccountNumber;
                    AddressrichTextBox.Text = bankInfo.Address;
                    BankID = id;
                }

                BtnSave.Enabled = false;
                btnUpdate.Enabled = true;
                BtnDelete.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator-" + ex.InnerException);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(BankNametextBox.Text))
                {
                    MessageBox.Show("Please enter bank name.");
                    BankNametextBox.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(AccountNumtextBox.Text))
                {
                    MessageBox.Show("Please enter account number.");
                    AccountNumtextBox.Focus();
                    return;
                }
                else if (string.IsNullOrEmpty(AddressrichTextBox.Text))
                {
                    MessageBox.Show("Please enter bank address.");
                    AddressrichTextBox.Focus();
                    return;
                }
                else
                {
                    UpdateBankInfo();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator-" + ex.InnerException);
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (BankID > 0)
                {
                    using (ShellEntities context = new ShellEntities())
                    {
                        MST_BankInfoMaster bankInfo = context.MST_BankInfoMaster.Where(s => s.ID == BankID && s.IsActive == true).FirstOrDefault();
                        bankInfo.IsActive = false;
                        context.SaveChanges();
                        BtnSave.Enabled = true;
                        btnUpdate.Enabled = false;
                        BtnDelete.Enabled = false;
                        BindBankInfo();
                    }
                }
                else
                {
                    MessageBox.Show("Please double click grid cell.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Please contact administrator-" + ex.InnerException);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BankInfoMaster_Load(object sender, EventArgs e)
        {
            BindBankInfo();
        }
    }
}
