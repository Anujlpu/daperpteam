﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class FrmMarginMaster : Form
    {
        public FrmMarginMaster()
        {
            InitializeComponent();
        }

        private void FrmMarginMaster_Load(object sender, EventArgs e)
        {
            grpManageMargin.Visible = false;
            grpMarginList.Visible = true;
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Saved!");
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Updated!");
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Record Deleted!");
        }
       
        private void btnView_Click(object sender, EventArgs e)
        {
            grpManageMargin.Visible = false;
            grpMarginList.Visible = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            grpManageMargin.Visible = true;
            grpMarginList.Visible = false;
        }

    }
}
