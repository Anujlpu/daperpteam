﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shell
{
    public partial class FrmPriceMaster : Form
    {
        public FrmPriceMaster()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Saved!");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Data Updated!");
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Record Deleted!");
        }
        private void FrmPriceMaster_Load(object sender, EventArgs e)
        {
            grpManagePrice.Visible = false;
            grpPriceList.Visible = true;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            grpManagePrice.Visible = false;
            grpPriceList.Visible = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            grpManagePrice.Visible = true;
            grpPriceList.Visible = false;
        }
    }
}
