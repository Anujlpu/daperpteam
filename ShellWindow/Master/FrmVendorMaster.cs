﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
namespace Shell
{
    public partial class FrmVendorMaster : Form
    {
        public FrmVendorMaster()
        {
            InitializeComponent();
        }

        private void FrmVendorMaster_Load(object sender, EventArgs e)
        {
            Random _ram = new Random();
            txtVendorCode.Text = "SHVEN" + _ram.Next(999999).ToString();
            BindGrid(); binddropdown();
        }
        public void BindGrid()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();
            dgvVendorList.DataSource = _objVendor.GetVendorlist();
            dgvVendorList.Columns[0].Visible = false;
            dgvVendorList.Columns["Password"].Visible = false;
            dgvVendorList.Columns["PaymentType"].Visible = false;
            dgvVendorList.Columns["Remarks"].Visible = false;
            dgvVendorList.Columns["CreatedBy"].Visible = false;
            dgvVendorList.Columns["ModifiedDate"].Visible = false;
            dgvVendorList.Columns["ModifiedBy"].Visible = false;

        }
        public void binddropdown()
        {
            BAL.BAL_Vendor _objVendor = new BAL.BAL_Vendor();

            ddlPaymentType.DisplayMember = "PaymenttypeCode";
            ddlPaymentType.ValueMember = "PaymentTypeId";
            ddlPaymentType.DataSource = _objVendor.GetPaymentlist();
        }
        string str = "0123456789.";
        private void CtrlKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }
        }
        public int _vendorid;
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            BAL_Vendor _obj = new BAL_Vendor();
            int Result = _obj.DeleteVendor(_vendorid);
            if (Result == 1)
            {
                MessageBox.Show("Vendor Delete Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();

        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtVendorName.Text))
            {
                MessageBox.Show("Please Enter the Vendor Name");
                txtVendorName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtContactNo.Text))
            {
                MessageBox.Show("Please Enter the Contact No");
                txtContactNo.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtAccountNo.Text))
            {
                MessageBox.Show("Please Enter the Accoount No");
                txtAccountNo.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtSEName.Text))
            {
                MessageBox.Show("Please Enter the SE Name");
                txtSEName.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtDap.Text))
            {
                MessageBox.Show("Please Enter the DAP Email id for Invoice");
                txtDap.Focus();
                return;
            }
            if (ddlPaymentType.SelectedValue == null)
            {
                MessageBox.Show("Please Select Payment type");
                ddlPaymentType.Focus();
                return;
            }
            BAL_Vendor _obj = new BAL_Vendor();
            int Result = _obj.ManageVendor(_vendorid, txtVendorCode.Text, txtVendorName.Text, txtContactNo.Text, txtEmailID.Text, txtUserName.Text, txtPAssword.Text, txtAccountNo.Text, txtAddress.Text, txtSEName.Text, txtEmailID.Text, txtMobileNo.Text, txtDap.Text, ddlPaymentType.SelectedValue.ToString(), "", Utility.BA_Commman._userId, Utility.BA_Commman._userId, 2,txtonlineportal.Text);
            if (Result == 1)
            {
                MessageBox.Show("Vendor Update Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtVendorName.Text))
            {
                MessageBox.Show("Please Enter the Vendor Name");
                return;
            }
            if (string.IsNullOrEmpty(txtContactNo.Text))
            {
                MessageBox.Show("Please Enter the Contact No");
                return;
            }
            if (string.IsNullOrEmpty(txtAccountNo.Text))
            {
                MessageBox.Show("Please Enter the Accoount No");
                return;
            }
            if (string.IsNullOrEmpty(txtSEName.Text))
            {
                MessageBox.Show("Please Enter the SE Name");
                return;
            }
            if (string.IsNullOrEmpty(txtDap.Text))
            {
                MessageBox.Show("Please Enter the DAP Email id for Invoice");
                return;
            }
            BAL_Vendor _obj = new BAL_Vendor();
            int Result = _obj.ManageVendor(0, txtVendorCode.Text, txtVendorName.Text, txtContactNo.Text, txtEmailID.Text, txtUserName.Text, txtPAssword.Text, txtAccountNo.Text, txtAddress.Text, txtSEName.Text, txtEmailID.Text, txtMobileNo.Text, txtDap.Text, ddlPaymentType.SelectedValue.ToString(), "", Utility.BA_Commman._userId, Utility.BA_Commman._userId, 1,txtonlineportal.Text);
            if (Result == 1)
            {
                MessageBox.Show("Vendor Created Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }

        private void dgvVendorList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        public void ClearValue()
        {
            Random _ram = new Random();
            txtVendorCode.Text = "SHVEN" + _ram.Next(999999).ToString();
            txtVendorName.Text = "";
            txtContactNo.Text = "";
            txtEmailID.Text = "";
            txtUserName.Text = "";
            txtPAssword.Text = "";
            txtAccountNo.Text = "";
            txtAddress.Text = "";
            txtSEName.Text = "";
            txtSEEMailId.Text = "";
            txtonlineportal.Text = "";
            txtMobileNo.Text = "";
            txtDap.Text = "";
            binddropdown();
            BindGrid();
            BtnDelete.Enabled = false;
            button1.Enabled = false;
            BtnSave.Enabled = true;

        }

        private void dgvVendorList_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0)
            {
                DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    txtVendorCode.Text = dgvVendorList.Rows[e.RowIndex].Cells["VendorCode"].Value.ToString();
                    txtVendorName.Text = dgvVendorList.Rows[e.RowIndex].Cells["VendorName"].Value.ToString();
                    txtContactNo.Text = dgvVendorList.Rows[e.RowIndex].Cells["ContactNo"].Value.ToString();
                    txtEmailID.Text = dgvVendorList.Rows[e.RowIndex].Cells["EmailId"].Value.ToString();
                    txtUserName.Text = dgvVendorList.Rows[e.RowIndex].Cells["OnlineUserName"].Value.ToString();
                    txtPAssword.Text = dgvVendorList.Rows[e.RowIndex].Cells["Password"].Value.ToString();
                    txtAccountNo.Text = dgvVendorList.Rows[e.RowIndex].Cells["AccountNo"].Value.ToString();
                    txtAddress.Text = dgvVendorList.Rows[e.RowIndex].Cells["Address"].Value.ToString();
                    txtSEName.Text = dgvVendorList.Rows[e.RowIndex].Cells["SEName"].Value.ToString();
                    txtSEEMailId.Text = dgvVendorList.Rows[e.RowIndex].Cells["SEEmailId"].Value.ToString();
                    txtMobileNo.Text = dgvVendorList.Rows[e.RowIndex].Cells["SEMobileNo"].Value.ToString();
                    txtDap.Text = dgvVendorList.Rows[e.RowIndex].Cells["DAPEmailId"].Value.ToString();
                    ddlPaymentType.SelectedValue = int.Parse(dgvVendorList.Rows[e.RowIndex].Cells["PaymentType"].Value.ToString());
                    _vendorid = int.Parse(dgvVendorList.Rows[e.RowIndex].Cells["vendorid"].Value.ToString());
                    txtonlineportal.Text = dgvVendorList.Rows[e.RowIndex].Cells["OnlinePortal"].Value.ToString();
                    BtnDelete.Enabled = true;
                    button1.Enabled = true;
                    BtnSave.Enabled = false;
                }
                else
                {
                    BtnDelete.Enabled = false;
                    button1.Enabled = false;
                    BtnSave.Enabled = true;
                }
            }
        }

        private void txtEmailID_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            if (!Utility.BA_Commman.ValidateEmail(((TextBoxBase)(sender)).Text) == true)
            {
                MessageBox.Show("Enter valid Email Id");
                ((TextBoxBase)(sender)).Focus();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
