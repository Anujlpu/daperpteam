﻿namespace Shell
{
    partial class FrmSalaryCreation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpManageEmp = new System.Windows.Forms.GroupBox();
            this.Refreshbutton = new System.Windows.Forms.Button();
            this.Exitbutton = new System.Windows.Forms.Button();
            this.LocationtextBox = new System.Windows.Forms.TextBox();
            this.DepartmenttextBox = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.NetSalarytextBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.DeductiontextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTotalPresent = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ddlEmployeName = new System.Windows.Forms.ComboBox();
            this.WorkingDaystextBox = new System.Windows.Forms.TextBox();
            this.txtBasicSalary = new System.Windows.Forms.TextBox();
            this.DTPDOJ = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Updatebutton = new System.Windows.Forms.Button();
            this.grpEmployeeList = new System.Windows.Forms.GroupBox();
            this.dgvSalariesgrid = new System.Windows.Forms.DataGridView();
            this.ToDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.PPEndtextBox = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.BonustextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.CPPtextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ELtextBox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.TaxtextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.AdvancetextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.GrossPaytextBox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.grpManageEmp.SuspendLayout();
            this.grpEmployeeList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalariesgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // grpManageEmp
            // 
            this.grpManageEmp.Controls.Add(this.checkBox1);
            this.grpManageEmp.Controls.Add(this.GrossPaytextBox);
            this.grpManageEmp.Controls.Add(this.label19);
            this.grpManageEmp.Controls.Add(this.AdvancetextBox);
            this.grpManageEmp.Controls.Add(this.label18);
            this.grpManageEmp.Controls.Add(this.TaxtextBox);
            this.grpManageEmp.Controls.Add(this.label17);
            this.grpManageEmp.Controls.Add(this.ELtextBox);
            this.grpManageEmp.Controls.Add(this.label16);
            this.grpManageEmp.Controls.Add(this.CPPtextBox);
            this.grpManageEmp.Controls.Add(this.label15);
            this.grpManageEmp.Controls.Add(this.BonustextBox);
            this.grpManageEmp.Controls.Add(this.label14);
            this.grpManageEmp.Controls.Add(this.label13);
            this.grpManageEmp.Controls.Add(this.PPEndtextBox);
            this.grpManageEmp.Controls.Add(this.label12);
            this.grpManageEmp.Controls.Add(this.ToDatePicker);
            this.grpManageEmp.Controls.Add(this.label11);
            this.grpManageEmp.Controls.Add(this.Refreshbutton);
            this.grpManageEmp.Controls.Add(this.Exitbutton);
            this.grpManageEmp.Controls.Add(this.LocationtextBox);
            this.grpManageEmp.Controls.Add(this.DepartmenttextBox);
            this.grpManageEmp.Controls.Add(this.label62);
            this.grpManageEmp.Controls.Add(this.txtRemarks);
            this.grpManageEmp.Controls.Add(this.NetSalarytextBox);
            this.grpManageEmp.Controls.Add(this.label8);
            this.grpManageEmp.Controls.Add(this.DeductiontextBox);
            this.grpManageEmp.Controls.Add(this.label4);
            this.grpManageEmp.Controls.Add(this.txtTotalPresent);
            this.grpManageEmp.Controls.Add(this.label3);
            this.grpManageEmp.Controls.Add(this.ddlEmployeName);
            this.grpManageEmp.Controls.Add(this.WorkingDaystextBox);
            this.grpManageEmp.Controls.Add(this.txtBasicSalary);
            this.grpManageEmp.Controls.Add(this.DTPDOJ);
            this.grpManageEmp.Controls.Add(this.label10);
            this.grpManageEmp.Controls.Add(this.label5);
            this.grpManageEmp.Controls.Add(this.label6);
            this.grpManageEmp.Controls.Add(this.label7);
            this.grpManageEmp.Controls.Add(this.BtnDelete);
            this.grpManageEmp.Controls.Add(this.BtnSave);
            this.grpManageEmp.Controls.Add(this.label2);
            this.grpManageEmp.Controls.Add(this.label1);
            this.grpManageEmp.Controls.Add(this.Updatebutton);
            this.grpManageEmp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageEmp.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageEmp.ForeColor = System.Drawing.Color.Black;
            this.grpManageEmp.Location = new System.Drawing.Point(3, 12);
            this.grpManageEmp.Name = "grpManageEmp";
            this.grpManageEmp.Size = new System.Drawing.Size(1033, 320);
            this.grpManageEmp.TabIndex = 44;
            this.grpManageEmp.TabStop = false;
            this.grpManageEmp.Text = "Manage Employee Salary";
            // 
            // Refreshbutton
            // 
            this.Refreshbutton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Refreshbutton.FlatAppearance.BorderSize = 2;
            this.Refreshbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Refreshbutton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Refreshbutton.Location = new System.Drawing.Point(658, 279);
            this.Refreshbutton.Name = "Refreshbutton";
            this.Refreshbutton.Size = new System.Drawing.Size(90, 32);
            this.Refreshbutton.TabIndex = 87;
            this.Refreshbutton.Text = "&Refresh";
            this.Refreshbutton.UseVisualStyleBackColor = true;
            this.Refreshbutton.Click += new System.EventHandler(this.Refreshbutton_Click);
            // 
            // Exitbutton
            // 
            this.Exitbutton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Exitbutton.FlatAppearance.BorderSize = 2;
            this.Exitbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exitbutton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exitbutton.Location = new System.Drawing.Point(562, 279);
            this.Exitbutton.Name = "Exitbutton";
            this.Exitbutton.Size = new System.Drawing.Size(90, 32);
            this.Exitbutton.TabIndex = 86;
            this.Exitbutton.Text = "&Exit";
            this.Exitbutton.UseVisualStyleBackColor = true;
            this.Exitbutton.Click += new System.EventHandler(this.Exitbutton_Click);
            // 
            // LocationtextBox
            // 
            this.LocationtextBox.Enabled = false;
            this.LocationtextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationtextBox.ForeColor = System.Drawing.Color.Black;
            this.LocationtextBox.Location = new System.Drawing.Point(128, 100);
            this.LocationtextBox.Name = "LocationtextBox";
            this.LocationtextBox.Size = new System.Drawing.Size(199, 26);
            this.LocationtextBox.TabIndex = 85;
            // 
            // DepartmenttextBox
            // 
            this.DepartmenttextBox.Enabled = false;
            this.DepartmenttextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DepartmenttextBox.ForeColor = System.Drawing.Color.Black;
            this.DepartmenttextBox.Location = new System.Drawing.Point(128, 71);
            this.DepartmenttextBox.Name = "DepartmenttextBox";
            this.DepartmenttextBox.Size = new System.Drawing.Size(199, 26);
            this.DepartmenttextBox.TabIndex = 84;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F);
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(725, 166);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(58, 15);
            this.label62.TabIndex = 83;
            this.label62.Text = "Remarks";
            // 
            // txtRemarks
            // 
            this.txtRemarks.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRemarks.ForeColor = System.Drawing.Color.Black;
            this.txtRemarks.Location = new System.Drawing.Point(831, 166);
            this.txtRemarks.Multiline = true;
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(199, 57);
            this.txtRemarks.TabIndex = 82;
            // 
            // NetSalarytextBox
            // 
            this.NetSalarytextBox.Enabled = false;
            this.NetSalarytextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NetSalarytextBox.ForeColor = System.Drawing.Color.Black;
            this.NetSalarytextBox.Location = new System.Drawing.Point(831, 135);
            this.NetSalarytextBox.Name = "NetSalarytextBox";
            this.NetSalarytextBox.Size = new System.Drawing.Size(199, 26);
            this.NetSalarytextBox.TabIndex = 80;
            this.NetSalarytextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NetSalarytextBox_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.AliceBlue;
            this.label8.Font = new System.Drawing.Font("Arial", 9F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(725, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 15);
            this.label8.TabIndex = 79;
            this.label8.Text = "Net Pay";
            // 
            // DeductiontextBox
            // 
            this.DeductiontextBox.Enabled = false;
            this.DeductiontextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeductiontextBox.ForeColor = System.Drawing.Color.Black;
            this.DeductiontextBox.Location = new System.Drawing.Point(831, 100);
            this.DeductiontextBox.Name = "DeductiontextBox";
            this.DeductiontextBox.Size = new System.Drawing.Size(199, 26);
            this.DeductiontextBox.TabIndex = 78;
            this.DeductiontextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DeductiontextBox_KeyPress);
            this.DeductiontextBox.Leave += new System.EventHandler(this.DeductiontextBox_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AliceBlue;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(719, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 15);
            this.label4.TabIndex = 77;
            this.label4.Text = "Total Deduction";
            // 
            // txtTotalPresent
            // 
            this.txtTotalPresent.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPresent.ForeColor = System.Drawing.Color.Black;
            this.txtTotalPresent.Location = new System.Drawing.Point(488, 41);
            this.txtTotalPresent.Name = "txtTotalPresent";
            this.txtTotalPresent.Size = new System.Drawing.Size(199, 26);
            this.txtTotalPresent.TabIndex = 76;
            this.txtTotalPresent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTotalPresent_KeyPress);
            this.txtTotalPresent.Leave += new System.EventHandler(this.txtTotalPresent_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(376, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 15);
            this.label3.TabIndex = 75;
            this.label3.Text = "Total Present";
            // 
            // ddlEmployeName
            // 
            this.ddlEmployeName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEmployeName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlEmployeName.FormattingEnabled = true;
            this.ddlEmployeName.Location = new System.Drawing.Point(128, 39);
            this.ddlEmployeName.Name = "ddlEmployeName";
            this.ddlEmployeName.Size = new System.Drawing.Size(199, 22);
            this.ddlEmployeName.TabIndex = 74;
            this.ddlEmployeName.SelectedIndexChanged += new System.EventHandler(this.ddlEmployeName_SelectedIndexChanged);
            // 
            // WorkingDaystextBox
            // 
            this.WorkingDaystextBox.Enabled = false;
            this.WorkingDaystextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WorkingDaystextBox.ForeColor = System.Drawing.Color.Black;
            this.WorkingDaystextBox.Location = new System.Drawing.Point(129, 232);
            this.WorkingDaystextBox.Name = "WorkingDaystextBox";
            this.WorkingDaystextBox.Size = new System.Drawing.Size(199, 26);
            this.WorkingDaystextBox.TabIndex = 71;
            this.WorkingDaystextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.WorkingDaystextBox_KeyPress);
            this.WorkingDaystextBox.Leave += new System.EventHandler(this.WorkingDaystextBox_Leave);
            // 
            // txtBasicSalary
            // 
            this.txtBasicSalary.Enabled = false;
            this.txtBasicSalary.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBasicSalary.ForeColor = System.Drawing.Color.Black;
            this.txtBasicSalary.Location = new System.Drawing.Point(128, 132);
            this.txtBasicSalary.Name = "txtBasicSalary";
            this.txtBasicSalary.Size = new System.Drawing.Size(199, 26);
            this.txtBasicSalary.TabIndex = 65;
            // 
            // DTPDOJ
            // 
            this.DTPDOJ.CustomFormat = "MMM";
            this.DTPDOJ.Location = new System.Drawing.Point(129, 167);
            this.DTPDOJ.Name = "DTPDOJ";
            this.DTPDOJ.Size = new System.Drawing.Size(199, 26);
            this.DTPDOJ.TabIndex = 64;
            this.DTPDOJ.Value = new System.DateTime(2017, 8, 1, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(14, 241);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 15);
            this.label10.TabIndex = 52;
            this.label10.Text = "Workign Days";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(14, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 15);
            this.label5.TabIndex = 50;
            this.label5.Text = "Location";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(14, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 15);
            this.label6.TabIndex = 49;
            this.label6.Text = "Department";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.AliceBlue;
            this.label7.Font = new System.Drawing.Font("Arial", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(14, 177);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 15);
            this.label7.TabIndex = 48;
            this.label7.Text = "From";
            // 
            // BtnDelete
            // 
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(466, 279);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 44;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(276, 279);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 43;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(14, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 15);
            this.label2.TabIndex = 36;
            this.label2.Text = "Employee Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(14, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Basic Salary";
            // 
            // Updatebutton
            // 
            this.Updatebutton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Updatebutton.FlatAppearance.BorderSize = 2;
            this.Updatebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Updatebutton.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Updatebutton.Location = new System.Drawing.Point(370, 279);
            this.Updatebutton.Name = "Updatebutton";
            this.Updatebutton.Size = new System.Drawing.Size(90, 32);
            this.Updatebutton.TabIndex = 45;
            this.Updatebutton.Text = "&Update";
            this.Updatebutton.UseVisualStyleBackColor = true;
            this.Updatebutton.Click += new System.EventHandler(this.Updatebutton_Click);
            // 
            // grpEmployeeList
            // 
            this.grpEmployeeList.Controls.Add(this.dgvSalariesgrid);
            this.grpEmployeeList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpEmployeeList.Location = new System.Drawing.Point(3, 338);
            this.grpEmployeeList.Name = "grpEmployeeList";
            this.grpEmployeeList.Size = new System.Drawing.Size(1033, 257);
            this.grpEmployeeList.TabIndex = 46;
            this.grpEmployeeList.TabStop = false;
            this.grpEmployeeList.Text = "Salary Generation List";
            // 
            // dgvSalariesgrid
            // 
            this.dgvSalariesgrid.AllowDrop = true;
            this.dgvSalariesgrid.AllowUserToAddRows = false;
            this.dgvSalariesgrid.AllowUserToDeleteRows = false;
            this.dgvSalariesgrid.AllowUserToResizeColumns = false;
            this.dgvSalariesgrid.AllowUserToResizeRows = false;
            this.dgvSalariesgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSalariesgrid.BackgroundColor = System.Drawing.Color.White;
            this.dgvSalariesgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvSalariesgrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSalariesgrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSalariesgrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvSalariesgrid.Location = new System.Drawing.Point(3, 16);
            this.dgvSalariesgrid.MultiSelect = false;
            this.dgvSalariesgrid.Name = "dgvSalariesgrid";
            this.dgvSalariesgrid.ReadOnly = true;
            this.dgvSalariesgrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvSalariesgrid.RowHeadersVisible = false;
            this.dgvSalariesgrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvSalariesgrid.Size = new System.Drawing.Size(1027, 238);
            this.dgvSalariesgrid.TabIndex = 1;
            this.dgvSalariesgrid.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSalariesgrid_CellContentDoubleClick);
            // 
            // ToDatePicker
            // 
            this.ToDatePicker.CustomFormat = "MMM";
            this.ToDatePicker.Location = new System.Drawing.Point(129, 200);
            this.ToDatePicker.Name = "ToDatePicker";
            this.ToDatePicker.Size = new System.Drawing.Size(199, 26);
            this.ToDatePicker.TabIndex = 89;
            this.ToDatePicker.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.AliceBlue;
            this.label11.Font = new System.Drawing.Font("Arial", 9F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(14, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 15);
            this.label11.TabIndex = 88;
            this.label11.Text = "To";
            // 
            // PPEndtextBox
            // 
            this.PPEndtextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PPEndtextBox.ForeColor = System.Drawing.Color.Black;
            this.PPEndtextBox.Location = new System.Drawing.Point(488, 71);
            this.PPEndtextBox.Name = "PPEndtextBox";
            this.PPEndtextBox.Size = new System.Drawing.Size(199, 26);
            this.PPEndtextBox.TabIndex = 91;
            this.PPEndtextBox.Leave += new System.EventHandler(this.PPEndtextBox_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(376, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 15);
            this.label12.TabIndex = 90;
            this.label12.Text = "PP End";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(376, 111);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 15);
            this.label13.TabIndex = 92;
            this.label13.Text = "4% V-Pay";
            // 
            // BonustextBox
            // 
            this.BonustextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BonustextBox.ForeColor = System.Drawing.Color.Black;
            this.BonustextBox.Location = new System.Drawing.Point(488, 132);
            this.BonustextBox.Name = "BonustextBox";
            this.BonustextBox.Size = new System.Drawing.Size(199, 26);
            this.BonustextBox.TabIndex = 95;
            this.BonustextBox.Text = "0.00";
            this.BonustextBox.Leave += new System.EventHandler(this.BonustextBox_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(376, 140);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 15);
            this.label14.TabIndex = 94;
            this.label14.Text = "Bonus";
            // 
            // CPPtextBox
            // 
            this.CPPtextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CPPtextBox.ForeColor = System.Drawing.Color.Black;
            this.CPPtextBox.Location = new System.Drawing.Point(488, 200);
            this.CPPtextBox.Name = "CPPtextBox";
            this.CPPtextBox.Size = new System.Drawing.Size(199, 26);
            this.CPPtextBox.TabIndex = 97;
            this.CPPtextBox.Text = "0.00";
            this.CPPtextBox.Leave += new System.EventHandler(this.CPPtextBox_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(376, 208);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 15);
            this.label15.TabIndex = 96;
            this.label15.Text = "CPP";
            // 
            // ELtextBox
            // 
            this.ELtextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ELtextBox.ForeColor = System.Drawing.Color.Black;
            this.ELtextBox.Location = new System.Drawing.Point(488, 232);
            this.ELtextBox.Name = "ELtextBox";
            this.ELtextBox.Size = new System.Drawing.Size(199, 26);
            this.ELtextBox.TabIndex = 99;
            this.ELtextBox.Text = "0.00";
            this.ELtextBox.Leave += new System.EventHandler(this.ELtextBox_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(376, 240);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(22, 15);
            this.label16.TabIndex = 98;
            this.label16.Text = "EL";
            // 
            // TaxtextBox
            // 
            this.TaxtextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TaxtextBox.ForeColor = System.Drawing.Color.Black;
            this.TaxtextBox.Location = new System.Drawing.Point(831, 30);
            this.TaxtextBox.Name = "TaxtextBox";
            this.TaxtextBox.Size = new System.Drawing.Size(199, 26);
            this.TaxtextBox.TabIndex = 101;
            this.TaxtextBox.Text = "0.00";
            this.TaxtextBox.Leave += new System.EventHandler(this.TaxtextBox_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(719, 38);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(25, 15);
            this.label17.TabIndex = 100;
            this.label17.Text = "Tax";
            // 
            // AdvancetextBox
            // 
            this.AdvancetextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdvancetextBox.ForeColor = System.Drawing.Color.Black;
            this.AdvancetextBox.Location = new System.Drawing.Point(488, 164);
            this.AdvancetextBox.Name = "AdvancetextBox";
            this.AdvancetextBox.Size = new System.Drawing.Size(199, 26);
            this.AdvancetextBox.TabIndex = 103;
            this.AdvancetextBox.Text = "0.00";
            this.AdvancetextBox.Leave += new System.EventHandler(this.AdvancetextBox_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.AliceBlue;
            this.label18.Font = new System.Drawing.Font("Arial", 9F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(376, 166);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 15);
            this.label18.TabIndex = 102;
            this.label18.Text = "Advance";
            // 
            // GrossPaytextBox
            // 
            this.GrossPaytextBox.Enabled = false;
            this.GrossPaytextBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrossPaytextBox.ForeColor = System.Drawing.Color.Black;
            this.GrossPaytextBox.Location = new System.Drawing.Point(831, 64);
            this.GrossPaytextBox.Name = "GrossPaytextBox";
            this.GrossPaytextBox.Size = new System.Drawing.Size(199, 26);
            this.GrossPaytextBox.TabIndex = 105;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.AliceBlue;
            this.label19.Font = new System.Drawing.Font("Arial", 9F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(719, 69);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 15);
            this.label19.TabIndex = 104;
            this.label19.Text = "Gross Pay";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(488, 104);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(50, 24);
            this.checkBox1.TabIndex = 106;
            this.checkBox1.Text = "Yes";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // FrmSalaryCreation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(1048, 598);
            this.Controls.Add(this.grpEmployeeList);
            this.Controls.Add(this.grpManageEmp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmSalaryCreation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Salary Generation";
            this.Load += new System.EventHandler(this.FrmSalaryCreation_Load);
            this.grpManageEmp.ResumeLayout(false);
            this.grpManageEmp.PerformLayout();
            this.grpEmployeeList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSalariesgrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpManageEmp;
        private System.Windows.Forms.TextBox WorkingDaystextBox;
        private System.Windows.Forms.TextBox txtBasicSalary;
        private System.Windows.Forms.DateTimePicker DTPDOJ;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Updatebutton;
        private System.Windows.Forms.ComboBox ddlEmployeName;
        private System.Windows.Forms.TextBox NetSalarytextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox DeductiontextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTotalPresent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.GroupBox grpEmployeeList;
        private System.Windows.Forms.TextBox LocationtextBox;
        private System.Windows.Forms.TextBox DepartmenttextBox;
        private System.Windows.Forms.DataGridView dgvSalariesgrid;
        private System.Windows.Forms.Button Exitbutton;
        private System.Windows.Forms.Button Refreshbutton;
        private System.Windows.Forms.DateTimePicker ToDatePicker;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox PPEndtextBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox BonustextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox AdvancetextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TaxtextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox ELtextBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox CPPtextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox GrossPaytextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}