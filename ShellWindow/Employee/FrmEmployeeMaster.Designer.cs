﻿namespace Shell
{
    partial class FrmEmployeeMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpEmployeeList = new System.Windows.Forms.GroupBox();
            this.dgvLocation = new System.Windows.Forms.DataGridView();
            this.btnExit = new System.Windows.Forms.Button();
            this.grpManageEmp = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtTarelAllowan = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtLivingOutAll = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtActuaHSalary = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtbasiHsalary = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtActualSalary = new System.Windows.Forms.TextBox();
            this.txtPEmailId = new System.Windows.Forms.TextBox();
            this.txtOfficeialMailId = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.txtBasicSalary = new System.Windows.Forms.TextBox();
            this.DTPDOJ = new System.Windows.Forms.DateTimePicker();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.ddlcurrentLocation = new System.Windows.Forms.ComboBox();
            this.ddlhiredLocation = new System.Windows.Forms.ComboBox();
            this.ddlRole = new System.Windows.Forms.ComboBox();
            this.ddlLocation = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnSave = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtEmpName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ddlworkPermit = new System.Windows.Forms.ComboBox();
            this.txtdepart = new System.Windows.Forms.TextBox();
            this.ddlEmployemnetType = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.grpEmployeeList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocation)).BeginInit();
            this.grpManageEmp.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpEmployeeList
            // 
            this.grpEmployeeList.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpEmployeeList.Controls.Add(this.dgvLocation);
            this.grpEmployeeList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpEmployeeList.Location = new System.Drawing.Point(4, 411);
            this.grpEmployeeList.Name = "grpEmployeeList";
            this.grpEmployeeList.Size = new System.Drawing.Size(858, 324);
            this.grpEmployeeList.TabIndex = 45;
            this.grpEmployeeList.TabStop = false;
            this.grpEmployeeList.Text = "Employee List";
            // 
            // dgvLocation
            // 
            this.dgvLocation.AllowDrop = true;
            this.dgvLocation.AllowUserToAddRows = false;
            this.dgvLocation.AllowUserToDeleteRows = false;
            this.dgvLocation.AllowUserToResizeRows = false;
            this.dgvLocation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
            this.dgvLocation.BackgroundColor = System.Drawing.Color.White;
            this.dgvLocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.NullValue = null;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLocation.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocation.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvLocation.Location = new System.Drawing.Point(3, 16);
            this.dgvLocation.MultiSelect = false;
            this.dgvLocation.Name = "dgvLocation";
            this.dgvLocation.ReadOnly = true;
            this.dgvLocation.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvLocation.RowHeadersVisible = false;
            this.dgvLocation.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgvLocation.Size = new System.Drawing.Size(852, 305);
            this.dgvLocation.TabIndex = 1;
            this.dgvLocation.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLocation_CellClick);
            // 
            // btnExit
            // 
            this.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnExit.FlatAppearance.BorderSize = 2;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(728, 355);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(88, 32);
            this.btnExit.TabIndex = 24;
            this.btnExit.Text = "&Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // grpManageEmp
            // 
            this.grpManageEmp.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.grpManageEmp.Controls.Add(this.ddlEmployemnetType);
            this.grpManageEmp.Controls.Add(this.label24);
            this.grpManageEmp.Controls.Add(this.txtdepart);
            this.grpManageEmp.Controls.Add(this.label23);
            this.grpManageEmp.Controls.Add(this.label27);
            this.grpManageEmp.Controls.Add(this.label25);
            this.grpManageEmp.Controls.Add(this.label21);
            this.grpManageEmp.Controls.Add(this.txtUserName);
            this.grpManageEmp.Controls.Add(this.label22);
            this.grpManageEmp.Controls.Add(this.txtTarelAllowan);
            this.grpManageEmp.Controls.Add(this.label20);
            this.grpManageEmp.Controls.Add(this.btnExit);
            this.grpManageEmp.Controls.Add(this.txtLivingOutAll);
            this.grpManageEmp.Controls.Add(this.label19);
            this.grpManageEmp.Controls.Add(this.txtActuaHSalary);
            this.grpManageEmp.Controls.Add(this.label18);
            this.grpManageEmp.Controls.Add(this.txtbasiHsalary);
            this.grpManageEmp.Controls.Add(this.label13);
            this.grpManageEmp.Controls.Add(this.txtActualSalary);
            this.grpManageEmp.Controls.Add(this.txtPEmailId);
            this.grpManageEmp.Controls.Add(this.txtOfficeialMailId);
            this.grpManageEmp.Controls.Add(this.txtAddress);
            this.grpManageEmp.Controls.Add(this.txtMobileNo);
            this.grpManageEmp.Controls.Add(this.txtBasicSalary);
            this.grpManageEmp.Controls.Add(this.DTPDOJ);
            this.grpManageEmp.Controls.Add(this.dtpDOB);
            this.grpManageEmp.Controls.Add(this.txtPassword);
            this.grpManageEmp.Controls.Add(this.ddlcurrentLocation);
            this.grpManageEmp.Controls.Add(this.ddlhiredLocation);
            this.grpManageEmp.Controls.Add(this.ddlRole);
            this.grpManageEmp.Controls.Add(this.ddlLocation);
            this.grpManageEmp.Controls.Add(this.label17);
            this.grpManageEmp.Controls.Add(this.label14);
            this.grpManageEmp.Controls.Add(this.label15);
            this.grpManageEmp.Controls.Add(this.label12);
            this.grpManageEmp.Controls.Add(this.label10);
            this.grpManageEmp.Controls.Add(this.label11);
            this.grpManageEmp.Controls.Add(this.label5);
            this.grpManageEmp.Controls.Add(this.label6);
            this.grpManageEmp.Controls.Add(this.label7);
            this.grpManageEmp.Controls.Add(this.label8);
            this.grpManageEmp.Controls.Add(this.label9);
            this.grpManageEmp.Controls.Add(this.BtnDelete);
            this.grpManageEmp.Controls.Add(this.BtnSave);
            this.grpManageEmp.Controls.Add(this.label4);
            this.grpManageEmp.Controls.Add(this.label3);
            this.grpManageEmp.Controls.Add(this.label2);
            this.grpManageEmp.Controls.Add(this.label16);
            this.grpManageEmp.Controls.Add(this.txtEmpName);
            this.grpManageEmp.Controls.Add(this.label1);
            this.grpManageEmp.Controls.Add(this.button1);
            this.grpManageEmp.Controls.Add(this.ddlworkPermit);
            this.grpManageEmp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpManageEmp.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpManageEmp.ForeColor = System.Drawing.Color.Black;
            this.grpManageEmp.Location = new System.Drawing.Point(9, 3);
            this.grpManageEmp.Name = "grpManageEmp";
            this.grpManageEmp.Size = new System.Drawing.Size(850, 402);
            this.grpManageEmp.TabIndex = 1;
            this.grpManageEmp.TabStop = false;
            this.grpManageEmp.Text = "Manage Employee";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.AliceBlue;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(438, 90);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(12, 15);
            this.label23.TabIndex = 91;
            this.label23.Text = "*";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.AliceBlue;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(822, 158);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(12, 15);
            this.label27.TabIndex = 90;
            this.label27.Text = "*";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.AliceBlue;
            this.label25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(438, 245);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(12, 15);
            this.label25.TabIndex = 88;
            this.label25.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.AliceBlue;
            this.label21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(438, 25);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(12, 15);
            this.label21.TabIndex = 85;
            this.label21.Text = "*";
            // 
            // txtUserName
            // 
            this.txtUserName.BackColor = System.Drawing.Color.White;
            this.txtUserName.ForeColor = System.Drawing.Color.Black;
            this.txtUserName.Location = new System.Drawing.Point(205, 83);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserName.MaxLength = 25;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(227, 26);
            this.txtUserName.TabIndex = 4;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.AliceBlue;
            this.label22.Font = new System.Drawing.Font("Arial", 9F);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(14, 86);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 15);
            this.label22.TabIndex = 84;
            this.label22.Text = "Online UserName ";
            // 
            // txtTarelAllowan
            // 
            this.txtTarelAllowan.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTarelAllowan.ForeColor = System.Drawing.Color.Black;
            this.txtTarelAllowan.Location = new System.Drawing.Point(205, 329);
            this.txtTarelAllowan.MaxLength = 5;
            this.txtTarelAllowan.Name = "txtTarelAllowan";
            this.txtTarelAllowan.Size = new System.Drawing.Size(227, 26);
            this.txtTarelAllowan.TabIndex = 9;
            this.txtTarelAllowan.Text = "0";
            this.txtTarelAllowan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTarelAllowan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9F);
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(14, 335);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(98, 15);
            this.label20.TabIndex = 80;
            this.label20.Text = "Travel Allowance";
            // 
            // txtLivingOutAll
            // 
            this.txtLivingOutAll.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLivingOutAll.ForeColor = System.Drawing.Color.Black;
            this.txtLivingOutAll.Location = new System.Drawing.Point(617, 323);
            this.txtLivingOutAll.MaxLength = 5;
            this.txtLivingOutAll.Name = "txtLivingOutAll";
            this.txtLivingOutAll.Size = new System.Drawing.Size(199, 26);
            this.txtLivingOutAll.TabIndex = 21;
            this.txtLivingOutAll.Text = "0";
            this.txtLivingOutAll.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLivingOutAll.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(478, 327);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(119, 15);
            this.label19.TabIndex = 78;
            this.label19.Text = "Living Out Allowance";
            // 
            // txtActuaHSalary
            // 
            this.txtActuaHSalary.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActuaHSalary.ForeColor = System.Drawing.Color.Black;
            this.txtActuaHSalary.Location = new System.Drawing.Point(617, 263);
            this.txtActuaHSalary.MaxLength = 2;
            this.txtActuaHSalary.Name = "txtActuaHSalary";
            this.txtActuaHSalary.Size = new System.Drawing.Size(199, 26);
            this.txtActuaHSalary.TabIndex = 17;
            this.txtActuaHSalary.Text = "0";
            this.txtActuaHSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActuaHSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F);
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(478, 267);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(115, 15);
            this.label18.TabIndex = 76;
            this.label18.Text = "Actual Hourly Salary";
            // 
            // txtbasiHsalary
            // 
            this.txtbasiHsalary.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbasiHsalary.ForeColor = System.Drawing.Color.Black;
            this.txtbasiHsalary.Location = new System.Drawing.Point(205, 239);
            this.txtbasiHsalary.MaxLength = 2;
            this.txtbasiHsalary.Name = "txtbasiHsalary";
            this.txtbasiHsalary.Size = new System.Drawing.Size(227, 26);
            this.txtbasiHsalary.TabIndex = 11;
            this.txtbasiHsalary.Text = "0";
            this.txtbasiHsalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtbasiHsalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.AliceBlue;
            this.label13.Font = new System.Drawing.Font("Arial", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(14, 239);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 15);
            this.label13.TabIndex = 74;
            this.label13.Text = "Basic Hourly Salary";
            // 
            // txtActualSalary
            // 
            this.txtActualSalary.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActualSalary.ForeColor = System.Drawing.Color.Black;
            this.txtActualSalary.Location = new System.Drawing.Point(617, 231);
            this.txtActualSalary.MaxLength = 6;
            this.txtActualSalary.Name = "txtActualSalary";
            this.txtActualSalary.Size = new System.Drawing.Size(199, 26);
            this.txtActualSalary.TabIndex = 14;
            this.txtActualSalary.Text = "0";
            this.txtActualSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtActualSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // txtPEmailId
            // 
            this.txtPEmailId.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPEmailId.ForeColor = System.Drawing.Color.Black;
            this.txtPEmailId.Location = new System.Drawing.Point(617, 171);
            this.txtPEmailId.MaxLength = 50;
            this.txtPEmailId.Name = "txtPEmailId";
            this.txtPEmailId.Size = new System.Drawing.Size(199, 26);
            this.txtPEmailId.TabIndex = 10;
            this.txtPEmailId.Leave += new System.EventHandler(this.txtEmailID_Leave);
            // 
            // txtOfficeialMailId
            // 
            this.txtOfficeialMailId.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOfficeialMailId.ForeColor = System.Drawing.Color.Black;
            this.txtOfficeialMailId.Location = new System.Drawing.Point(617, 139);
            this.txtOfficeialMailId.MaxLength = 50;
            this.txtOfficeialMailId.Name = "txtOfficeialMailId";
            this.txtOfficeialMailId.Size = new System.Drawing.Size(199, 26);
            this.txtOfficeialMailId.TabIndex = 14;
            this.txtOfficeialMailId.Leave += new System.EventHandler(this.txtEmailID_Leave);
            // 
            // txtAddress
            // 
            this.txtAddress.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAddress.ForeColor = System.Drawing.Color.Black;
            this.txtAddress.Location = new System.Drawing.Point(205, 150);
            this.txtAddress.MaxLength = 100;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(227, 51);
            this.txtAddress.TabIndex = 8;
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMobileNo.ForeColor = System.Drawing.Color.Black;
            this.txtMobileNo.Location = new System.Drawing.Point(205, 117);
            this.txtMobileNo.MaxLength = 12;
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(227, 26);
            this.txtMobileNo.TabIndex = 6;
            this.txtMobileNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // txtBasicSalary
            // 
            this.txtBasicSalary.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBasicSalary.ForeColor = System.Drawing.Color.Black;
            this.txtBasicSalary.Location = new System.Drawing.Point(205, 207);
            this.txtBasicSalary.MaxLength = 6;
            this.txtBasicSalary.Name = "txtBasicSalary";
            this.txtBasicSalary.Size = new System.Drawing.Size(227, 26);
            this.txtBasicSalary.TabIndex = 9;
            this.txtBasicSalary.Text = "0";
            this.txtBasicSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBasicSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TXTKeyPress);
            // 
            // DTPDOJ
            // 
            this.DTPDOJ.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DTPDOJ.Location = new System.Drawing.Point(617, 19);
            this.DTPDOJ.Name = "DTPDOJ";
            this.DTPDOJ.Size = new System.Drawing.Size(199, 26);
            this.DTPDOJ.TabIndex = 1;
            // 
            // dtpDOB
            // 
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDOB.Location = new System.Drawing.Point(205, 52);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(227, 26);
            this.dtpDOB.TabIndex = 2;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.White;
            this.txtPassword.ForeColor = System.Drawing.Color.Black;
            this.txtPassword.Location = new System.Drawing.Point(617, 79);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.MaxLength = 12;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(199, 26);
            this.txtPassword.TabIndex = 5;
            // 
            // ddlcurrentLocation
            // 
            this.ddlcurrentLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlcurrentLocation.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlcurrentLocation.FormattingEnabled = true;
            this.ddlcurrentLocation.Location = new System.Drawing.Point(205, 301);
            this.ddlcurrentLocation.Name = "ddlcurrentLocation";
            this.ddlcurrentLocation.Size = new System.Drawing.Size(227, 22);
            this.ddlcurrentLocation.TabIndex = 20;
            // 
            // ddlhiredLocation
            // 
            this.ddlhiredLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlhiredLocation.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlhiredLocation.FormattingEnabled = true;
            this.ddlhiredLocation.Location = new System.Drawing.Point(617, 295);
            this.ddlhiredLocation.Name = "ddlhiredLocation";
            this.ddlhiredLocation.Size = new System.Drawing.Size(199, 22);
            this.ddlhiredLocation.TabIndex = 18;
            // 
            // ddlRole
            // 
            this.ddlRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRole.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlRole.FormattingEnabled = true;
            this.ddlRole.Location = new System.Drawing.Point(617, 203);
            this.ddlRole.Name = "ddlRole";
            this.ddlRole.Size = new System.Drawing.Size(199, 22);
            this.ddlRole.TabIndex = 12;
            // 
            // ddlLocation
            // 
            this.ddlLocation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlLocation.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlLocation.FormattingEnabled = true;
            this.ddlLocation.Location = new System.Drawing.Point(617, 111);
            this.ddlLocation.Name = "ddlLocation";
            this.ddlLocation.Size = new System.Drawing.Size(199, 22);
            this.ddlLocation.TabIndex = 7;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(14, 303);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(146, 15);
            this.label17.TabIndex = 57;
            this.label17.Text = "Current Working Location";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(478, 297);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 15);
            this.label14.TabIndex = 56;
            this.label14.Text = "Hired Loaction";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(14, 271);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 15);
            this.label15.TabIndex = 55;
            this.label15.Text = "Employee Status";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(478, 237);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 15);
            this.label12.TabIndex = 54;
            this.label12.Text = "Actual Salary";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(478, 207);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 15);
            this.label10.TabIndex = 52;
            this.label10.Text = "Role";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(14, 150);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 15);
            this.label11.TabIndex = 51;
            this.label11.Text = "Address";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.AliceBlue;
            this.label5.Font = new System.Drawing.Font("Arial", 9F);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(478, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 15);
            this.label5.TabIndex = 50;
            this.label5.Text = "Location";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.AliceBlue;
            this.label6.Font = new System.Drawing.Font("Arial", 9F);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(478, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 15);
            this.label6.TabIndex = 49;
            this.label6.Text = "Department";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.AliceBlue;
            this.label7.Font = new System.Drawing.Font("Arial", 9F);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(478, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 15);
            this.label7.TabIndex = 48;
            this.label7.Text = "DOJ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F);
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(478, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 15);
            this.label8.TabIndex = 47;
            this.label8.Text = "Personal Email Id";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.AliceBlue;
            this.label9.Font = new System.Drawing.Font("Arial", 9F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(478, 147);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 15);
            this.label9.TabIndex = 46;
            this.label9.Text = "Official Email Id";
            // 
            // BtnDelete
            // 
            this.BtnDelete.Enabled = false;
            this.BtnDelete.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnDelete.FlatAppearance.BorderSize = 2;
            this.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDelete.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(632, 355);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(90, 32);
            this.BtnDelete.TabIndex = 23;
            this.BtnDelete.Text = "&Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnSave
            // 
            this.BtnSave.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnSave.FlatAppearance.BorderSize = 2;
            this.BtnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSave.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(442, 355);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(90, 32);
            this.BtnSave.TabIndex = 21;
            this.BtnSave.Text = "&Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.AliceBlue;
            this.label4.Font = new System.Drawing.Font("Arial", 9F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(478, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 15);
            this.label4.TabIndex = 40;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.AliceBlue;
            this.label3.Font = new System.Drawing.Font("Arial", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(14, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 15);
            this.label3.TabIndex = 38;
            this.label3.Text = "DOB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.AliceBlue;
            this.label2.Font = new System.Drawing.Font("Arial", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(14, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 15);
            this.label2.TabIndex = 36;
            this.label2.Text = "Employee Name";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(14, 118);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 15);
            this.label16.TabIndex = 34;
            this.label16.Text = "Mobile Number";
            // 
            // txtEmpName
            // 
            this.txtEmpName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmpName.ForeColor = System.Drawing.Color.Black;
            this.txtEmpName.Location = new System.Drawing.Point(205, 19);
            this.txtEmpName.MaxLength = 50;
            this.txtEmpName.Name = "txtEmpName";
            this.txtEmpName.Size = new System.Drawing.Size(227, 26);
            this.txtEmpName.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.AliceBlue;
            this.label1.Font = new System.Drawing.Font("Arial", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(14, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Basic Salary";
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(536, 355);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 32);
            this.button1.TabIndex = 22;
            this.button1.Text = "&Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ddlworkPermit
            // 
            this.ddlworkPermit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlworkPermit.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlworkPermit.FormattingEnabled = true;
            this.ddlworkPermit.Location = new System.Drawing.Point(205, 271);
            this.ddlworkPermit.Name = "ddlworkPermit";
            this.ddlworkPermit.Size = new System.Drawing.Size(227, 22);
            this.ddlworkPermit.TabIndex = 16;
            // 
            // txtdepart
            // 
            this.txtdepart.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdepart.ForeColor = System.Drawing.Color.Black;
            this.txtdepart.Location = new System.Drawing.Point(617, 48);
            this.txtdepart.MaxLength = 50;
            this.txtdepart.Name = "txtdepart";
            this.txtdepart.Size = new System.Drawing.Size(199, 26);
            this.txtdepart.TabIndex = 3;
            // 
            // ddlEmployemnetType
            // 
            this.ddlEmployemnetType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlEmployemnetType.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlEmployemnetType.FormattingEnabled = true;
            this.ddlEmployemnetType.Items.AddRange(new object[] {
            "--Select--",
            "Permanent",
            "Part Time",
            "Full Time"});
            this.ddlEmployemnetType.Location = new System.Drawing.Point(205, 362);
            this.ddlEmployemnetType.Name = "ddlEmployemnetType";
            this.ddlEmployemnetType.Size = new System.Drawing.Size(227, 22);
            this.ddlEmployemnetType.TabIndex = 93;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9F);
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(14, 364);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(118, 15);
            this.label24.TabIndex = 94;
            this.label24.Text = "Employeement Type";
            // 
            // FrmEmployeeMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(866, 751);
            this.Controls.Add(this.grpEmployeeList);
            this.Controls.Add(this.grpManageEmp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimizeBox = false;
            this.Name = "FrmEmployeeMaster";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Employee Master";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmEmployeeMaster_Load);
            this.grpEmployeeList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocation)).EndInit();
            this.grpManageEmp.ResumeLayout(false);
            this.grpManageEmp.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpEmployeeList;
        private System.Windows.Forms.DataGridView dgvLocation;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox grpManageEmp;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtEmpName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox ddlcurrentLocation;
        private System.Windows.Forms.ComboBox ddlhiredLocation;
        private System.Windows.Forms.ComboBox ddlRole;
        private System.Windows.Forms.ComboBox ddlLocation;
        private System.Windows.Forms.DateTimePicker DTPDOJ;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtActualSalary;
        private System.Windows.Forms.TextBox txtPEmailId;
        private System.Windows.Forms.TextBox txtOfficeialMailId;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.TextBox txtBasicSalary;
        private System.Windows.Forms.ComboBox ddlworkPermit;
        private System.Windows.Forms.TextBox txtActuaHSalary;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtbasiHsalary;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtTarelAllowan;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtLivingOutAll;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtdepart;
        private System.Windows.Forms.ComboBox ddlEmployemnetType;
        private System.Windows.Forms.Label label24;
    }
}