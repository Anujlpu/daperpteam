﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BAL;
using Utility;
namespace Shell
{
    public partial class FrmEmployeeMaster : Form
    {
        public FrmEmployeeMaster()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        DateTime visaissuedate = DateTime.Now, VisaexpireDate = DateTime.Now;
        string VisaReamarks = "";
        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (txtEmpName.Text == "")
            {
                MessageBox.Show("Please Enter a Employee Name");
                txtEmpName.Focus();
                return;
            }

            if (txtOfficeialMailId.Text == "")
            {
                MessageBox.Show("Please Enter a Employee Mail Id");
                txtOfficeialMailId.Focus();
                return;
            }

            if (txtbasiHsalary.Text == "")
            {
                MessageBox.Show("Please Enter a Employee Basic Salary");
                txtbasiHsalary.Focus();
                return;
            }

            if (txtUserName.Text == "")
            {
                MessageBox.Show("Please Enter a User Name");
                txtUserName.Focus();
                return;
            }
            if (ddlworkPermit.Text.ToString() != "PR" && ddlworkPermit.Text.ToString() != "Citizen")
            {
                FrmVisaDetails childform = new FrmVisaDetails();
                childform.ShowDialog();
                VisaReamarks = childform.VisaDescription;
                visaissuedate = childform.VisaIssueDate;
                VisaexpireDate = childform.VisaExpireDate;
            }


            BAL_Employee _obj = new BAL_Employee();
            int Result = _obj.ManageEmployee(0, txtEmpName.Text, dtpDOB.Value, DTPDOJ.Value, int.Parse(ddlRole.SelectedValue.ToString()), txtdepart.Text, int.Parse(ddlLocation.SelectedValue.ToString()), txtMobileNo.Text, txtOfficeialMailId.Text, txtAddress.Text,
                         txtPEmailId.Text, Convert.ToDouble(txtBasicSalary.Text), Convert.ToDouble(txtbasiHsalary.Text), Convert.ToDouble(txtActualSalary.Text), Convert.ToDouble(txtActuaHSalary.Text),
                         int.Parse(ddlworkPermit.SelectedValue.ToString()), int.Parse(ddlhiredLocation.SelectedValue.ToString()), int.Parse(ddlcurrentLocation.SelectedValue.ToString()), Convert.ToDouble(txtLivingOutAll.Text), Convert.ToDouble(txtTarelAllowan.Text)
                         , visaissuedate, VisaexpireDate, VisaReamarks, "", txtUserName.Text, txtPassword.Text, BA_Commman._userId, 1,ddlEmployemnetType.SelectedItem.ToString());



            if (Result == 1)
            {
                MessageBox.Show("Employee Created Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }
        public void ClearValue()
        {
            txtEmpName.Text = "";
            txtMobileNo.Text = "";
            txtPassword.Text = "";
            txtAddress.Text = "";
            txtBasicSalary.Text = "";
            txtbasiHsalary.Text = "0.00";
            txtActuaHSalary.Text = "0.00";
            txtLivingOutAll.Text = "0.00";
            txtTarelAllowan.Text = "0.00";
            txtPEmailId.Text = "";
            txtOfficeialMailId.Text = "";
            txtActualSalary.Text = "";
            txtUserName.Text = "";
            txtdepart.Text = "";
            ddlEmployemnetType.SelectedItem = "--Select--";
            binddropdown();
            BindGridView();
            BtnDelete.Enabled = false;
            button1.Enabled = false;
            BtnSave.Enabled = true;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (txtEmpName.Text == "")
            {
                MessageBox.Show("Please Enter a Employee Name");
                txtEmpName.Focus();
                return;
            }

            if (txtOfficeialMailId.Text == "")
            {
                MessageBox.Show("Please Enter a Employee Mail Id");
                txtOfficeialMailId.Focus();
                return;
            }

            if (txtbasiHsalary.Text == "")
            {
                MessageBox.Show("Please Enter a Employee Basic Salary");
                txtbasiHsalary.Focus();
                return;
            }

            if (txtUserName.Text == "")
            {
                MessageBox.Show("Please Enter a User Name");
                txtUserName.Focus();
                return;
            }
            if (ddlworkPermit.Text.ToString() != "PR" && ddlworkPermit.Text.ToString() != "Citizen")
            {
                FrmVisaDetails childform = new FrmVisaDetails();
                childform.ShowDialog();
                VisaReamarks = childform.VisaDescription;
                visaissuedate = childform.VisaIssueDate;
                VisaexpireDate = childform.VisaExpireDate;
            }


            BAL_Employee _obj = new BAL_Employee();
            int Result = _obj.ManageEmployee(_EmployeeId, txtEmpName.Text, dtpDOB.Value, DTPDOJ.Value, int.Parse(ddlRole.SelectedValue.ToString()), txtdepart.Text, int.Parse(ddlLocation.SelectedValue.ToString()), txtMobileNo.Text, txtOfficeialMailId.Text, txtAddress.Text,
                         txtPEmailId.Text, Convert.ToDouble(txtBasicSalary.Text), Convert.ToDouble(txtbasiHsalary.Text), Convert.ToDouble(txtActualSalary.Text), Convert.ToDouble(txtActuaHSalary.Text),
                         int.Parse(ddlworkPermit.SelectedValue.ToString()), int.Parse(ddlhiredLocation.SelectedValue.ToString()), int.Parse(ddlcurrentLocation.SelectedValue.ToString()), Convert.ToDouble(txtLivingOutAll.Text), Convert.ToDouble(txtTarelAllowan.Text)
                         , visaissuedate, VisaexpireDate, VisaReamarks, "", txtUserName.Text, txtPassword.Text, BA_Commman._userId, 2,ddlEmployemnetType.SelectedItem.ToString());

            if (Result == 1)
            {
                MessageBox.Show("Employee Updated Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }
        public int _EmployeeId;
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmpName.Text))
            {
                MessageBox.Show("Please Enter Employee Name");
                return;
            }
            BAL_Employee _obj = new BAL_Employee();
            int Result = _obj.DeleteEmployee(_EmployeeId, Utility.BA_Commman._userId);
            if (Result == 1)
            {
                MessageBox.Show("Employee Deleted Sucessfully");

            }
            else
            {
                MessageBox.Show("There is some issue . Please try later!");

            }
            ClearValue();
        }


        public void binddropdown()
        {
            BAL.BAL_Login _objLogin = new BAL.BAL_Login();
            ddlRole.DataSource = _objLogin.GetProfilelist();
            ddlRole.DisplayMember = "ProfileCode";
            ddlRole.ValueMember = "ProfileId";

            BAL.BAL_Location _objLocation = new BAL.BAL_Location();
            ddlLocation.DataSource = _objLocation.GetLocationlist();
            ddlLocation.DisplayMember = "LocationName";
            ddlLocation.ValueMember = "LocationId";

            ddlcurrentLocation.DataSource = _objLocation.GetLocationlist();
            ddlcurrentLocation.DisplayMember = "LocationName";
            ddlcurrentLocation.ValueMember = "LocationId";

            ddlhiredLocation.DataSource = _objLocation.GetLocationlist();
            ddlhiredLocation.DisplayMember = "LocationName";
            ddlhiredLocation.ValueMember = "LocationId";
            BAL_Employee _objEmployee = new BAL_Employee();

            ddlworkPermit.DataSource = _objEmployee.GetEmpStatusList();
            ddlworkPermit.DisplayMember = "EmployeeStatus";
            ddlworkPermit.ValueMember = "EmployeeStatusId";
        }
        string str = "0123456789.";
        private void TXTKeyPress(object sender, KeyPressEventArgs e)
        {
            if (str.IndexOf(e.KeyChar.ToString()) < 0)
            {
                if (e.KeyChar.ToString() != "\b")
                {
                    e.Handled = true;
                }
            }


        }
        private void FrmEmployeeMaster_Load(object sender, EventArgs e)
        {
            binddropdown();
            BindGridView();
        }
        public void BindGridView()
        {
            BAL_Employee _objEmployee = new BAL_Employee();
            dgvLocation.DataSource = _objEmployee.GetEmployeelist();


        }
        private void txtEmailID_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(((TextBoxBase)(sender)).Text))
            {
                ((TextBoxBase)(sender)).Text = "0";

            }
            if (!Utility.BA_Commman.ValidateEmail(((TextBoxBase)(sender)).Text) == true)
            {
                MessageBox.Show("Enter valid Email Id");
                ((TextBoxBase)(sender)).Focus();
            }
        }


        private void dgvLocation_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DialogResult dg = MessageBox.Show("Do You Want to Delete Records ? ", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dg == DialogResult.Yes)
                {
                    _EmployeeId = int.Parse(dgvLocation.Rows[e.RowIndex].Cells["EMployeeID"].Value.ToString());
                    txtEmpName.Text = dgvLocation.Rows[e.RowIndex].Cells["EmployeeName"].Value.ToString();
                    txtMobileNo.Text = dgvLocation.Rows[e.RowIndex].Cells["MobileNo"].Value.ToString();
                    txtOfficeialMailId.Text = dgvLocation.Rows[e.RowIndex].Cells["EmailId"].Value.ToString();
                    txtAddress.Text = dgvLocation.Rows[e.RowIndex].Cells["Address"].Value.ToString();
                    txtPEmailId.Text = dgvLocation.Rows[e.RowIndex].Cells["P_EmailId"].Value.ToString();
                    txtBasicSalary.Text = dgvLocation.Rows[e.RowIndex].Cells["BasicSalary"].Value.ToString();
                    txtbasiHsalary.Text = dgvLocation.Rows[e.RowIndex].Cells["BasicHourlySalary"].Value.ToString();
                    txtActualSalary.Text = dgvLocation.Rows[e.RowIndex].Cells["ActualSalary"].Value.ToString();
                    txtActuaHSalary.Text = dgvLocation.Rows[e.RowIndex].Cells["ActualHourlySalary"].Value.ToString();
                    txtLivingOutAll.Text = dgvLocation.Rows[e.RowIndex].Cells["LivingAllowance"].Value.ToString();
                    txtTarelAllowan.Text = dgvLocation.Rows[e.RowIndex].Cells["TravelAllowance"].Value.ToString();
                    txtUserName.Text = dgvLocation.Rows[e.RowIndex].Cells["UserName"].Value.ToString();
                    txtPassword.Text = dgvLocation.Rows[e.RowIndex].Cells["Password"].Value.ToString();
                    ddlRole.SelectedValue = int.Parse(dgvLocation.Rows[e.RowIndex].Cells["RoleId"].Value.ToString());
                    txtdepart.Text = dgvLocation.Rows[e.RowIndex].Cells["Department"].Value.ToString();
                    ddlLocation.SelectedValue = int.Parse(dgvLocation.Rows[e.RowIndex].Cells["LocationId"].Value.ToString());
                    ddlworkPermit.SelectedValue = int.Parse(dgvLocation.Rows[e.RowIndex].Cells["EmployeeStatus"].Value.ToString());
                    ddlhiredLocation.SelectedValue = int.Parse(dgvLocation.Rows[e.RowIndex].Cells["HiredLocation"].Value.ToString());
                    ddlcurrentLocation.SelectedValue = int.Parse(dgvLocation.Rows[e.RowIndex].Cells["CurrentWorkingLocation"].Value.ToString());
                    ddlEmployemnetType.SelectedItem = dgvLocation.Rows[e.RowIndex].Cells["Employementtype"].Value.ToString();

                    BtnDelete.Enabled = true;
                    button1.Enabled = true;
                    BtnSave.Enabled = false;
                }
                else
                {
                    BtnDelete.Enabled = false;
                    button1.Enabled = false;
                    BtnSave.Enabled = true;
                }


            }
        }


    }
}
