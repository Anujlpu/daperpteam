﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;
namespace BAL
{
    public class BAL_BankValEntries
    {
        DAL_BankValidation _Obj;

        public BAL_BankValEntries()
        { _Obj = new DAL_BankValidation(); }
        public DataSet getInvoiceDetails()
        {
            return _Obj.getBankEntriesDetails();

        }
        public DataSet GetOpenInvoiceDetails(string TranType, string VendorId)
        {
            return _Obj.GetOpenInvoiceDetails(TranType, VendorId);
        }
        public DataSet GetOpenBankDetails(string TranType)
        {
            return _Obj.GetOpenBankDetails(TranType);
        }
        public DataSet GetVendor(string Vendor, string Transtype)
        {
            return _Obj.GetVendor(Vendor,Transtype);
        }

        public int InsertBankValidation(DateTime ValidationDate, int BankEntry, int invoiceID, string Remarks, int CreatedBy, bool IsActive, string UploadPath)
        {

            return _Obj.InsertBankValentries(ValidationDate, BankEntry, invoiceID, Remarks, Utility.BA_Commman._userId, true, UploadPath);
        }
        public int UpdateBankValidation(int BankValidationEntries, DateTime ValidationDate, int BankEntry, int invoiceID, string Remarks, int CreatedBy, bool IsActive, string UploadPath)
        {
            return _Obj.UpdateBankEntries(BankValidationEntries, ValidationDate, BankEntry, invoiceID, Remarks, Utility.BA_Commman._userId, true, UploadPath);
        }
        public int DeleteEntries(int bankentries)
        {
            return _Obj.DeleteEntries(bankentries);
        }
    }
}
