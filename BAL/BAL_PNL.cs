﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
    public class BAL_PNL
    {
        DAL_PNL _Obj;
        public BAL_PNL()
        {
            _Obj = new DAL_PNL();
        }
        public List<PNLEntity> GetPNLlist()
        {
            return _Obj.GetPNLlist();
        }
        public int ManagePNL(int PNLId, string PNLCode, string PNLDescription, int userid, int type)
        {
            PNLEntity _locentity = new PNLEntity();
            _locentity.PNLCode = PNLCode;
            _locentity.PNLDescription = PNLDescription;
            _locentity.CreatedBy = userid;
            _locentity.PNLID = PNLId;
            return _Obj.InsertPNL(_locentity, type);
        }
        public int DeletePNL(int PNLId, int userid)
        {
            PNLEntity _locentity = new PNLEntity();
            _locentity.CreatedBy = userid;
            _locentity.PNLID = PNLId;
            return _Obj.DeletePNL(_locentity);


        }
    }
}
