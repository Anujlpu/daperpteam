﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
namespace BAL
{
    public class BAL_Store
    {
        DAL_Store _Obj;
        public BAL_Store()
        {
            _Obj = new DAL_Store();
        }
        public List<StoreEntity> GetStorelist()
        {
            return _Obj.GetStorelist();
        }
        public int ManageStore(int StoreId, string StoreCode, string StoreName, string StoreAddress, int StoreLocationId, int userid, int type,
           string PhoneNo, string Emailid, string FaxNo, string StoreBrand, string OutletNo, int EmpCount, int ContractorCount, string AppStatus, DateTime AppDate)
        {
            StoreEntity _locentity = new StoreEntity();
            _locentity.OutletNumber = StoreCode;
            _locentity.StoreName = StoreName;
            _locentity.StoreAddress = StoreAddress;
            _locentity.StoreLocationId = StoreLocationId;
            _locentity.Userid = userid;
            _locentity.StoreId = StoreId;
            _locentity.PhoneNo = PhoneNo;
            _locentity.Emailid = Emailid;
            _locentity.FaxNo = FaxNo;
            _locentity.StoreBrand = StoreBrand;
            _locentity.OutletNo = OutletNo;
            _locentity.EmpCount = EmpCount;
            _locentity.ContractorCount = ContractorCount;
            _locentity.AppDate = AppDate;
            _locentity.ApplicationStatus = AppStatus;

            return _Obj.InsertStore(_locentity, type);
        }
        public int DeleteStore(int StoreId, int userid)
        {
            StoreEntity _locentity = new StoreEntity();
            _locentity.Userid = userid;
            _locentity.StoreId = StoreId;
            return _Obj.DeleteStore(_locentity);


        }
    }
}
