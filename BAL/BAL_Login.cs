﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
    public class BAL_Login
    {
        public List<UserEntity> Login(string UserCode, string Password)
        {
            UserEntity user = new UserEntity();
            user.UserCode = UserCode;
            user.Password = Utility.BA_Commman.Encrypt(Password, true);
            DAL_Login _Obj = new DAL_Login(user);
            return _Obj.Login();
        }
        public int DeleteUser(string UserCode)
        {
            UserEntity user = new UserEntity();
            user.UserCode = UserCode;
            DAL_Login _Obj = new DAL_Login(user);
            return _Obj.DeleteUser();
        }
        public int InsertUser(string UserCode, string userName, string Passwword, int ProfileId, int Userid)
        {
            UserEntity user = new UserEntity();
            user.UserCode = UserCode;
            user.UserName = userName;
            user.Profileid = ProfileId;
            user.UserId = Userid;
            user.Password = Utility.BA_Commman.Encrypt(Passwword, true);
            DAL_Login _Obj = new DAL_Login(user);
            return _Obj.InsertUser();
        }
        public int UpdattUser(string UserCode, string userName, int ProfileId, int Userid)
        {
            UserEntity user = new UserEntity();
            user.UserCode = UserCode;
            user.UserName = userName;
            user.Profileid = ProfileId;
            user.UserId = Userid;
            DAL_Login _Obj = new DAL_Login(user);
            return _Obj.UpdattUser();
        }
        public int ChangePassword(string UserCode, string Passwword)
        {
            UserEntity user = new UserEntity();

            user.UserCode = UserCode;
            user.Password = Utility.BA_Commman.Encrypt(Passwword, true);
            DAL_Login _Obj = new DAL_Login(user);
            return _Obj.ChangePwd();
        }
        public List<UserEntity> GetUserlist()
        {
            DAL_Login _Obj = new DAL_Login();
            return _Obj.GetUserlist();
        }
        public List<ProfileEntity> GetProfilelist()
        {
            DAL_Login _Obj = new DAL_Login();
            return _Obj.GetProfilelist();
        }
    }
}
