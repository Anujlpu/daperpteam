﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;
namespace BAL
{
    public class BAL_InvoiceEntries
    {
        DAL_InvoiceEntry _Obj;

        public BAL_InvoiceEntries()
        { _Obj = new DAL_InvoiceEntry(); }
        public DataSet getInvoiceDetails(char s, int InvoiceId)
        {
            if (s == 'E')

                return _Obj.getInvoiceDetails(4, InvoiceId);
            else
                return _Obj.getInvoiceDetails(5, 0);

        }
        public int InsertInvoice(DateTime InvDate, int VendorId, string InvoiceNo
, int PNLType, int PaymentType, string TransacctionType, double GST, double TotalAmount, double Cash
, double Payout, double Total, string Remarks, string UploadPath)
        {
            return _Obj.InsertInvoice(InvDate, VendorId, InvoiceNo, PNLType, PaymentType, TransacctionType, GST, TotalAmount, Cash, Payout, Total, Remarks, Utility.BA_Commman._userId, true, UploadPath);
        }
        public int UpdateInvoice(int InvoiceId, DateTime InvDate, int VendorId, string InvoiceNo
, int PNLType, int PaymentType, string TransacctionType, double GST, double TotalAmount, double Cash
, double Payout, double Total, string Remarks, bool IsActive, string UploadPath)
        {
            return _Obj.UpdateInvoice(InvoiceId, InvDate, VendorId, InvoiceNo, PNLType, PaymentType, TransacctionType, GST, TotalAmount, Cash, Payout, Total, Remarks, Utility.BA_Commman._userId, true, UploadPath);

        }
        public int DeleteInvoice(int InvoiceId)
        {
            return _Obj.DeleteInvoice(InvoiceId);
        }
    }
}
