﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
namespace BAL
{
    public class BAL_PaymentToVendor
    {
        DAL_PaymentToVendor _Obj;
        public BAL_PaymentToVendor()
        {
            _Obj = new DAL_PaymentToVendor();
        }
        public List<PaidPaymentEntity> GetPaidPaymentlist()
        {
            return _Obj.GetPaidPaymentlist();
        }
        public int Managepaidpayment(int PaymentId, DateTime PaymentDate, int TransactionType, int PaymentToId, int PaymentType,
           double Amount, string Remarks, string UploadedDocPath, string InvoiceNo, int userid, int type, double Cash, double Payout)
        {

            PaidPaymentEntity _PaymentEntity = new PaidPaymentEntity();
            _PaymentEntity.PaymentDate = PaymentDate;
            _PaymentEntity.TransactionType = TransactionType;
            _PaymentEntity.PaymentToId = PaymentToId;
            _PaymentEntity.PaymentType = PaymentType;
            _PaymentEntity.Amount = Amount;
            _PaymentEntity.UploadedDocPath = UploadedDocPath;
            _PaymentEntity.Remarks = Remarks;
            _PaymentEntity.UserId = userid;
            _PaymentEntity.PaymentId = PaymentId;
            _PaymentEntity.InvoiceNo = InvoiceNo;
            _PaymentEntity.CashAmount = Cash;
            _PaymentEntity.CashPayoutAmount = Payout;
            return _Obj.InsertPaymentDetails(_PaymentEntity, type);
        }
        public int DeletePayment(int PaymentId, int userid)
        {
            PaidPaymentEntity _PaymentEntity = new PaidPaymentEntity();
            _PaymentEntity.UserId = userid;
            _PaymentEntity.PaymentId = PaymentId;
            return _Obj.DeleteEmployee(_PaymentEntity);


        }
    }
}
