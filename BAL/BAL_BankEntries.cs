﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;
namespace BAL
{
    public class BAL_BankEntries
    {
        DAL_BankEntry _Obj;

        public BAL_BankEntries()
        { _Obj = new DAL_BankEntry(); }
        public DataSet getInvoiceDetails()
        {
            return _Obj.getBankEntriesDetails();

        }
        public int InsertBankentries(DateTime InvDate, string TransacctionType, double TotalAmount, string Remarks, string UploadPath, string BankName, int PNLID, int vendorid)
        {
            return _Obj.InsertBankentries(InvDate, TransacctionType, TotalAmount, Remarks, Utility.BA_Commman._userId, true, UploadPath, BankName,vendorid,PNLID);
        }
        public int UpdateBankEntries(int BankEntries, DateTime InvDate, string TransacctionType, double TotalAmount, string Remarks, string UploadPath, string BankName, int PNLID, int vendorid)
        {
            
            return _Obj.UpdateBankEntries(BankEntries, InvDate, TransacctionType, TotalAmount, Remarks, Utility.BA_Commman._userId, true, UploadPath, BankName,PNLID,vendorid);

        }
        public int DeleteEntries(int bankentries)
        {
            return _Obj.DeleteEntries(bankentries);
        }
    }
}
