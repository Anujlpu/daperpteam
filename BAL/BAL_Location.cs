﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
namespace BAL
{
    public class BAL_Location
    {
        DAL_Location _Obj;
        public BAL_Location()
        {
            _Obj = new DAL_Location();
        }
        public List<LocationEntity> GetLocationlist()
        {
            return _Obj.GetLocationlist();
        }
        public int ManageLocation(int LocationId, string LocationCode, string LocationName, string LocationDescription, int userid, int type)
        {
            LocationEntity _locentity = new LocationEntity();
            _locentity.LocationCode = LocationCode;
            _locentity.LocationName = LocationName;
            _locentity.LocationFullAddress = LocationDescription;
            _locentity.CreatedBy = userid;
            _locentity.LocationId = LocationId;
            return _Obj.InsertLocation(_locentity, type);
        }
        public int DeleteLocation(int LocationId, int userid)
        {
            LocationEntity _locentity = new LocationEntity();
            _locentity.CreatedBy = userid;
            _locentity.LocationId = LocationId;
            return _Obj.DeleteLocation(_locentity);


        }
    }
}
