﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BAL
{
    public class BAL_FuelTransaction
    {
        DAL_FuelTransaction fuelTransaction;

        public BAL_FuelTransaction()
        {

        }
        public List<DocsEntity> ListGetDocsList(DateTime dt)
        {
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.GetDocsList(dt.ToString("MM/dd/yyyy"));
        }
        public string GetFilePath(string Uploadid)
        {
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.GetPath(Uploadid);
        }
        public List<DocsEntity> ListGetDocsList(DateTime dt, string DocType)
        {
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.GetDocsList(dt.ToString("MM/dd/yyyy"), DocType);
        }
        public int ManageDocs(DateTime TransactionDate, string DocsType, string DocsIdentity, string FilePath,int userId)
        {
            DocsEntity docentity = new DocsEntity();
            docentity.TransactionDate = TransactionDate;
            docentity.DocType = DocsType;
            docentity.DocName = DocsIdentity;
            docentity.FilePath = FilePath;
            docentity.UserId = userId;
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.ManageDocs(docentity);


        }
        public int Deletedocs(int docID)
        {

            DocsEntity entity = new DocsEntity();
            entity.DocId = docID;
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.Deletedocs(entity);
        }
        public int ManageTicket(int TicketStatusid, string dt)
        {

            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.ManageTicket(TicketStatusid, dt);
        }
        public List<FuelTransactionEntities> ListFuelTransactions(string dt)
        {
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.GetFuelTransactionList(dt);
        }
        public DataSet ListFuelTransactions()
        {
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.GetFuelTransactionList();
        }
        public DataSet ListFuelTransactions(int type, DateTime frmdate, DateTime todate)
        {
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.FuelTransactionReport(frmdate, todate,type);
        }
        public List<TicketDetails> ListTicketDetails(int type)
        {
            fuelTransaction = new DAL_FuelTransaction();
            return fuelTransaction.GetTicketList(type);
        }

        public int AddEditFuelTransaction(FuelTransactionEntities fueldObj)
        {
            //Add
            if (fueldObj != null)
            {
                fuelTransaction = new DAL_FuelTransaction();
                return fuelTransaction.AddEditFuelTransaction(fueldObj);

            }
            return 0;
        }

    }
}
