﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BAL
{
    public class BAL_Employee
    {
        DAL_Employee _Obj;
        public BAL_Employee()
        {
            _Obj = new DAL_Employee();
        }
        public List<EmployeeEntity> GetEmployeelist()
        {
            return _Obj.GetEmployeelist();
        }
        public List<EmployeeEntity> GetEmployeelist(int Role)
        {
            return _Obj.GetEmployeelist(Role);
        }
        public int ManageEmployee(int EmployeeId, string EmployeeName
            , DateTime DOJ, DateTime DOB, int RoleId, string DepartmentId, int LocationId, string MobileNo
            , string EmailId, string Address, string P_EmailId, double BasicSalary
            , double BasicHourlySalary, double ActualSalary, double ActualHourlySalary, int EmployeeStatus
            , int HiredLocation, int CurrentWorkingLocation, double LivingAllowance, double TravelAllowance
            , DateTime VisaIssueDate, DateTime VisaExpireDate, string VisaDescription, string Remarks
            , string UserName, string Password, int userid, int type, string EmployementType, string EmergencyContactNumber, string EmergencyContactAddress,DateTime HourlyRateEffFrom,DateTime HourlyRateEffTo,float PayRollRate,DateTime PayRollEffFrom,DateTime PayRollEffTo)
        {
            EmployeeEntity _employeeEntity = new EmployeeEntity();
            _employeeEntity.EmployeeName = EmployeeName;
            _employeeEntity.DOJ = DOJ;
            _employeeEntity.DOB = DOB;
            _employeeEntity.RoleId = RoleId;
            _employeeEntity.Department = DepartmentId;
            _employeeEntity.LocationId = LocationId;
            _employeeEntity.MobileNo = MobileNo;
            _employeeEntity.EmailId = EmailId;
            _employeeEntity.Address = Address;
            _employeeEntity.P_EmailId = P_EmailId;
            _employeeEntity.BasicSalary = BasicSalary.ToString();
            _employeeEntity.BasicHourlySalary = BasicHourlySalary.ToString();
            _employeeEntity.ActualSalary = ActualSalary.ToString();
            _employeeEntity.ActualHourlySalary = ActualHourlySalary.ToString();
            _employeeEntity.EmployeeStatus = EmployeeStatus.ToString();
            _employeeEntity.HiredLocation = HiredLocation.ToString();
            _employeeEntity.CurrentWorkingLocation = CurrentWorkingLocation.ToString();
            _employeeEntity.LivingAllowance = LivingAllowance.ToString();
            _employeeEntity.TravelAllowance = TravelAllowance.ToString();
            _employeeEntity.VisaIssueDate = VisaIssueDate;
            _employeeEntity.VisaExpireDate = VisaExpireDate;
            _employeeEntity.VisaDescription = VisaDescription;
            _employeeEntity.Remarks = Remarks;
            _employeeEntity.UserId = userid;
            _employeeEntity.UserName = UserName;
            _employeeEntity.Password = Password;
            _employeeEntity.EmployeeID = EmployeeId;
            _employeeEntity.Employementtype = EmployementType;
            _employeeEntity.EmegencyContactAddress = EmergencyContactAddress;
            _employeeEntity.EmergencyContactNumber = EmergencyContactNumber;
            _employeeEntity.HourlyRateEffFrom = HourlyRateEffFrom;
            _employeeEntity.HourlyRateEffTo = HourlyRateEffTo;
            _employeeEntity.PayRollRate = PayRollRate;
            _employeeEntity.PayRollEffFrom = PayRollEffFrom;
            _employeeEntity.PayRollEffTo = PayRollEffTo;
            return _Obj.InsertUser(_employeeEntity, type);
        }
        public int DeleteEmployee(int EmployeeID, int userid)
        {
            EmployeeEntity _employeeEntity = new EmployeeEntity();
            _employeeEntity.UserId = userid;
            _employeeEntity.EmployeeID = EmployeeID;
            return _Obj.DeleteEmployee(_employeeEntity);


        }
        public List<DepartEntity> GetDepartList()
        {
            return _Obj.GetDepartList();
        }
        public List<EmpStatusEntity> GetEmpStatusList()
        {
            return _Obj.GetEmpStatusList();
        }
    }
}
