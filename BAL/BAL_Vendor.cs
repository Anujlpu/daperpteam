﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
namespace BAL
{
    public class BAL_Vendor
    {
        DAL_Vendor _Obj;
        public BAL_Vendor()
        {
            _Obj = new DAL_Vendor();
        }
        public List<VenderEntity> GetVendorlist()
        {
            return _Obj.GetVendorlist();
        }
        public List<PaymentEntity> GetPaymentlist()
        {
            return _Obj.GetPaymentlist();
        }
        public int ManageVendor(int Vendorid, string VendorCode, string VendorName, string ContactNo, string EmailId, string OnlineUserName,
            string Password, string AccountNo, string Address, string SEName, string SEEmailId, string SEMobileNo, string DAPEmailId,
string PaymentType, string Remarks, int CreatedBy, int ModifiedBy, int Type,string OnlinePortal,string OnlinePortalUserName,string OnlinePortalPassword,string vendorType,string applicationStatus,DateTime applicationDate)
        {
            VenderEntity Vendor = new VenderEntity();
            Vendor.VendorId = Vendorid;
            Vendor.VendorCode = VendorCode;
            Vendor.VendorName = VendorName;
            Vendor.ContactNo = ContactNo;
            Vendor.EmailId = EmailId;
            Vendor.OnlineUserName = OnlineUserName;
            Vendor.StoreEmailPassword = Utility.BA_Commman.Encrypt(Password, true);
            Vendor.AccountNo = AccountNo;
            Vendor.Address = Address;
            Vendor.SEName = SEName;
            Vendor.SEEmailId = SEEmailId;
            Vendor.SEMobileNo = SEMobileNo;
            Vendor.DAPEmailId = DAPEmailId;
            Vendor.PaymentType = PaymentType;
            Vendor.Remarks = Remarks;
            Vendor.CreatedBy = CreatedBy;
            Vendor.ModifiedBy = ModifiedBy;
            Vendor.OnlinePortal = OnlinePortal;
            Vendor.OnlinePortalUserName = OnlinePortalUserName;
            Vendor.OnlinePortalPassword= Utility.BA_Commman.Encrypt(OnlinePortalPassword, true);
            Vendor.VendorType = vendorType;
            Vendor.ApplicationStatus = applicationStatus;
            Vendor.ApplicationDate = applicationDate;
            return _Obj.InsertUser(Vendor, Type);
        }
        public int DeleteVendor(int Vendorid)
        {
            DAL_Vendor _Obj = new DAL_Vendor();
            VenderEntity Vendor = new VenderEntity();
            Vendor.VendorId = Vendorid;
            Vendor.ModifiedBy = Utility.BA_Commman._userId;
            return _Obj.DeleteVendor(Vendor);
        }
    }
}
