﻿using Framework.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    public static class EmailNotifiicationProperties
    {
        public static EmailDbProperties EmailDbProperties
        {
            get
            {
                using (ShellEntities context = new ShellEntities())
                {
                    var emailProperties = context.MST_EmailConfiguration.FirstOrDefault(s => s.IsActive == true);
                    EmailDbProperties dbProperties = new EmailDbProperties();
                    if (emailProperties != null)
                    {
                        dbProperties.FromEmailId = emailProperties.FromEmailID;
                        dbProperties.FromEmailIdPassword = emailProperties.FromEmailIDPassword;
                        dbProperties.FromEmailServer = emailProperties.FromServer;
                        dbProperties.FromEmailPort = Convert.ToInt32(emailProperties.FromPort);
                        dbProperties.FromEmailSubject = emailProperties.FromSubject;
                        dbProperties.FromEmailName = emailProperties.FromName;
                        dbProperties.ToEmailIds = emailProperties.ToEmailIds.Split(';');
                        dbProperties.CcEmailIds = emailProperties.CcEmailIds.Split(';');
                        dbProperties.BccEmailIds = emailProperties.BccEmailIds.Split(';');

                    }

                    return dbProperties;
                }

            }
        }
    }

    public class EmailDbProperties
    {
        public string FromEmailId { get; set; }
        public string FromEmailIdPassword { get; set; }
        public string FromEmailServer { get; set; }
        public int FromEmailPort { get; set; }
        public string FromEmailSubject { get; set; }
        public string FromEmailName { get; set; }
        public string[] ToEmailIds { get; set; }
        public string[] CcEmailIds { get; set; }
        public string[] BccEmailIds { get; set; }
    }
}
