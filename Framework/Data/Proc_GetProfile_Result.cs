//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Framework.Data
{
    using System;
    
    public partial class Proc_GetProfile_Result
    {
        public int ProfileId { get; set; }
        public string ProfileCode { get; set; }
        public string ProfileName { get; set; }
    }
}
