//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Framework.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class MST_BankInfoMaster
    {
        public int ID { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string Address { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> StoreID { get; set; }
        public string StoreName { get; set; }
    }
}
