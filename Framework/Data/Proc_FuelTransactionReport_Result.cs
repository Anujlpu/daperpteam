//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Framework.Data
{
    using System;
    
    public partial class Proc_FuelTransactionReport_Result
    {
        public int GasTransactionID { get; set; }
        public System.DateTime TransactionDate { get; set; }
        public Nullable<double> C_StoreSale { get; set; }
        public string C_AttachmentID { get; set; }
    }
}
