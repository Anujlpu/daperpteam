//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Framework.Data
{
    using System;
    
    public partial class Proc_ManageStore_Result
    {
        public int StoreId { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public Nullable<int> StoreLocationId { get; set; }
        public string LocationCode { get; set; }
        public string PhoneNo { get; set; }
        public string Emailid { get; set; }
        public string FaxNo { get; set; }
        public string StoreBrand { get; set; }
        public string OutletNo { get; set; }
        public Nullable<int> EmpCount { get; set; }
        public Nullable<int> ContractorCount { get; set; }
        public Nullable<System.DateTime> ApplicationDate { get; set; }
        public string ApplicationStatus { get; set; }
    }
}
