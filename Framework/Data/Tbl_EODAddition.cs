//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Framework.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_EODAddition
    {
        public long AdditionID { get; set; }
        public string AdditionType { get; set; }
        public Nullable<int> C8Pack { get; set; }
        public Nullable<int> C10Pack { get; set; }
        public Nullable<int> Cigar10Pack { get; set; }
        public Nullable<int> Cigar8Pack { get; set; }
        public Nullable<int> Cigar5Pack { get; set; }
        public Nullable<int> Chews { get; set; }
        public Nullable<int> E_Cigar { get; set; }
        public Nullable<int> Other { get; set; }
        public Nullable<int> Total { get; set; }
        public Nullable<int> PackTotal { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
    }
}
