//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Framework.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class TBL_InvoiceCheckList
    {
        public int ID { get; set; }
        public Nullable<int> DocumentUploadedID { get; set; }
        public System.DateTime InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string ReceivedBy { get; set; }
        public Nullable<bool> HasReceivingSignature { get; set; }
        public string WhoVerified { get; set; }
        public Nullable<bool> HasVerifiedSignature { get; set; }
        public Nullable<bool> AnyShortage { get; set; }
        public Nullable<bool> AnyShortageClaimed { get; set; }
        public string ClaimRemarks { get; set; }
        public string ClaimInvoiceNumber { get; set; }
        public Nullable<System.DateTime> ClaimInvoiceDate { get; set; }
        public string ClaimItemNumber { get; set; }
        public string ItemDescripion { get; set; }
        public Nullable<double> NetAmount { get; set; }
        public Nullable<double> GST { get; set; }
        public Nullable<double> GrossAmount { get; set; }
        public string ClaimNumber { get; set; }
        public Nullable<System.DateTime> FollowUpDate { get; set; }
        public string ClaimStatus { get; set; }
        public string FollowUpRemarks { get; set; }
        public Nullable<double> ReceivingAmount { get; set; }
        public Nullable<double> Difference { get; set; }
        public string DifferenceRemarks { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
