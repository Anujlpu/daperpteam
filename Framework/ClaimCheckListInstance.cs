﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    public  class ClaimCheckListInstance
    {
        public  string InvoiceNumber { get; set; }
        public  DateTime InvoiceDate { get; set; }
        public  string ItemNumber { get; set; }
        public  string ItemDescripion { get; set; }
        public  float NetAmount { get; set; }
        public  float GST { get; set; }
        public  float GrossAmount { get; set; }
        public  string ClaimNumber { get; set; }
        public  DateTime FollowUpDate { get; set; }
        public  string ClaimStatus { get; set; }
        public  string FollowUpRemarks { get; set; }
        public  float ReceivingAmount { get; set; }
        public  float Difference { get; set; }
        public  string DifferenceRemarks { get; set; }
    }
}
