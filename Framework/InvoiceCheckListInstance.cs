﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
   public  class InvoiceCheckListInstance
    {
        public  string InvoiceNumber { get; set; }
        public  DateTime InvoiceDate { get; set; }
        public  string ReceivedBy { get; set; }
        public  bool HasReceivingSignature { get; set; }
        public  string WhoVerified { get; set; }
        public  bool HasVerifiedSignature { get; set; }
        public  bool AnyShortage { get; set; }
        public  bool AnyShortageClaimed { get; set; }
        public  string ClaimRemarks { get; set; }
    }
}
