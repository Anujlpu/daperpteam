﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    public class SubstractionReasonList
    {
        public List<Reasons> ReasonList()
        {
            List<Reasons> reasonList = new List<Reasons>();
            Reasons reasonObj = new Reasons();
            reasonObj.ID = 0;
            reasonObj.Name = "--Select--";
            reasonList.Add(reasonObj);
            reasonObj = new Reasons();
            reasonObj.ID = 1;
            reasonObj.Name = "Front Office Movement";
            reasonList.Add(reasonObj);
            reasonObj = new Reasons();
            reasonObj.ID = 2;
            reasonObj.Name = "Other Site Movement";
            reasonList.Add(reasonObj);
            return reasonList;
        }
    }

    public class Reasons
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
