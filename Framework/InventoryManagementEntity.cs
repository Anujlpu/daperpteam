﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    public class InventoryManagementEntity
    {
        #region Lotto UnActivated
        public string LottoUnActivatedCategoryID { get; set; }
        public string LottoUnActivatedCategoryName { get; set; }
        public string LottoUnActivatedProductID { get; set; }
        public string LottoUnActivatedProductName { get; set; }
        public int LottoUnActivatedOpening { get; set; }
        public int LottoUnActivatedAdd { get; set; }
        public int LottoUnActivatedSubstract { get; set; }
        public int LottoUnActivatedTotal { get; set; }
        #endregion
        #region Lotto Activated
        public string LottoActivatedCategoryID { get; set; }
        public string LottoActivatedCategoryName { get; set; }
        public string LottoActivatedProductID { get; set; }
        public string LottoActivatedProductName { get; set; }
        public int LottoActivatedOpening { get; set; }
        public int LottoActivatedAdd { get; set; }
        public int LottoActivatedSubstract { get; set; }
        public int LottoActivatedTotal { get; set; }
        #endregion
    }
}
