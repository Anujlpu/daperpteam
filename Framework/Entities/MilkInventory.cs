﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
    public class MilkInventory
    {
        public long InventoryID { get; set; }
        public string InventoryType { get; set; }
        public Nullable<int> Open { get; set; }
        public Nullable<int> ActualOpen { get; set; }
        public Nullable<int> Add { get; set; }
        public Nullable<int> SubTotal { get; set; }
        public Nullable<int> Close { get; set; }
        public Nullable<int> ActualOpenDifference { get; set; }
        public Nullable<int> CountSold { get; set; }
        public Nullable<int> EODSold { get; set; }
        public Nullable<int> Difference { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
    }
}
