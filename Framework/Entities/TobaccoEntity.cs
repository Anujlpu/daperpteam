﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
   public class TobaccoEntity
    {
        public long TaboccoID { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public string ProductCategoryName { get; set; }
        public Nullable<int> Balance { get; set; }
        public Nullable<int> Add { get; set; }
        public Nullable<int> Substract { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<int> VendorId { get; set; }
        public string SubstractionReason { get; set; }
        public string VendorName { get; set; }
        public Nullable<int> StoreId { get; set; }
    }
}
