﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
   public class LottoEODInventoryEntity
    {
        public long LottoEODMasterID { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public string InventoryType { get; set; }
        public Nullable<int> Opening1__50 { get; set; }
        public Nullable<int> Add1__50 { get; set; }
        public Nullable<int> Substract1__50 { get; set; }
        public Nullable<int> Return1__50 { get; set; }
        public Nullable<int> Closing1__50 { get; set; }
        public Nullable<int> Opening1__100 { get; set; }
        public Nullable<int> Add1__100 { get; set; }
        public Nullable<int> Substract1__100 { get; set; }
        public Nullable<int> Return1__100 { get; set; }
        public Nullable<int> Closing1__100 { get; set; }
        public Nullable<int> Opening2_ { get; set; }
        public Nullable<int> Add2_ { get; set; }
        public Nullable<int> Substract2_ { get; set; }
        public Nullable<int> Return2_ { get; set; }
        public Nullable<int> Closing2_ { get; set; }
        public Nullable<int> Opening3_ { get; set; }
        public Nullable<int> Add3_ { get; set; }
        public Nullable<int> Substract3_ { get; set; }
        public Nullable<int> Return3_ { get; set; }
        public Nullable<int> Closing3_ { get; set; }
        public Nullable<int> Opening4_ { get; set; }
        public Nullable<int> Add4_ { get; set; }
        public Nullable<int> Substract4_ { get; set; }
        public Nullable<int> Return4_ { get; set; }
        public Nullable<int> Closing4_ { get; set; }
        public Nullable<int> Opening5_ { get; set; }
        public Nullable<int> Add5_ { get; set; }
        public Nullable<int> Substract5_ { get; set; }
        public Nullable<int> Return5_ { get; set; }
        public Nullable<int> Closing5_ { get; set; }
        public Nullable<int> Opening7_ { get; set; }
        public Nullable<int> Add7_ { get; set; }
        public Nullable<int> Substract7_ { get; set; }
        public Nullable<int> Return7_ { get; set; }
        public Nullable<int> Closing7_ { get; set; }
        public Nullable<int> Opening10_ { get; set; }
        public Nullable<int> Add10_ { get; set; }
        public Nullable<int> Substract10_ { get; set; }
        public Nullable<int> Return10_ { get; set; }
        public Nullable<int> Closing10_ { get; set; }
        public Nullable<int> Opening20_ { get; set; }
        public Nullable<int> Add20_ { get; set; }
        public Nullable<int> Substract20_ { get; set; }
        public Nullable<int> Return20_ { get; set; }
        public Nullable<int> Closing20_ { get; set; }
        public Nullable<int> Opening30__50 { get; set; }
        public Nullable<int> Add30__50 { get; set; }
        public Nullable<int> Substract30__50 { get; set; }
        public Nullable<int> Return30__50 { get; set; }
        public Nullable<int> Closing30__50 { get; set; }
        public Nullable<int> Opening30__100 { get; set; }
        public Nullable<int> Add30__100 { get; set; }
        public Nullable<int> Substract30__100 { get; set; }
        public Nullable<int> Return30__100 { get; set; }
        public Nullable<int> Closing30__100 { get; set; }
        public string Remarks { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<int> TotalUnActivatedLotto { get; set; }
        public Nullable<int> TotalActivatedLotto { get; set; }
    }
}
