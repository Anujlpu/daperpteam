﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Entities
{
    public class MarginMasterEntity
    {
        public long MarginId { get; set; }
        public Nullable<double> MarginAmount { get; set; }
        public Nullable<double> MarginPercentage { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public Nullable<long> ProductId { get; set; }
        public Nullable<System.DateTime> EffectiveFrom { get; set; }
        public Nullable<System.DateTime> EffectiveTo { get; set; }
        public Nullable<double> MarkUp { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<double> discount { get; set; }
        public Nullable<double> OffPerTwoPacks { get; set; }
        public string Remarks { get; set; }
        public Nullable<double> BuyingPrice { get; set; }
        public Nullable<double> BuyingPricePerPiece { get; set; }
        public Nullable<double> CartonMargin { get; set; }
        public Nullable<double> MarginOnTwoPackDeal { get; set; }
        public Nullable<double> MarginOnSellingPrice { get; set; }
        public Nullable<double> DiscountedCartonPrice { get; set; }
        public Nullable<double> DiscountedPrice { get; set; }
        public Nullable<double> SellingPrice { get; set; }
        public Nullable<double> TwoPackDeal { get; set; }
        public Nullable<double> RegularCartonPrice { get; set; }
    }
}
