﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
    public static class CommonProperties
    {
        public static string AdminUserName
        {
            get
            {
                return ConfigurationSettings.AppSettings["AdminUserName"];
            }

        }
        public static string AdminPassword
        {
            get
            {
                return ConfigurationSettings.AppSettings["AdminPassword"];
            }

        }

    }
}
