﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAL;
using DAL;

namespace Framework
{
    public class StartUpUserHandler : IUserHandler
    {
        public bool RegisterAdmin()
        {
            BAL.BAL_Login _objLogin = new BAL.BAL_Login();
            int i = 0;
            int Usercount = _objLogin.GetUserlist().Where(p => p.UserCode.ToUpper() == CommonProperties.AdminUserName).Count();
            if (Usercount == 0)
            {
                i = _objLogin.InsertUser(CommonProperties.AdminUserName, CommonProperties.AdminUserName, CommonProperties.AdminPassword, 1, 1);
                if (i > 0)
                {
                    return true;
                }
            }
            return false;

        }
    }
}
