﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework
{
   public static class InvoiceAndClaimCheckList
    {
        public static string InvoiceNumber { get; set; }
        public static DateTime InvoiceDate { get; set; }
        public static string ReceivedBy { get; set; }
        public static bool HasReceivingSignature { get; set; }
        public static string WhoVerified { get; set; }
        public static bool HasVerifiedSignature { get; set; }
        public static bool AnyShortage { get; set; }
        public static bool AnyShortageClaimed { get; set; }
        public static string ClaimRemarks { get; set; }
        public static string ClaimInvoiceNumber { get; set; }
        public static DateTime ClaimInvoiceDate { get; set; }
        public static string ItemNumber { get; set; }
        public static string ItemDescripion { get; set; }
        public static float NetAmount { get; set; }
        public static float GST { get; set; }
        public static float GrossAmount { get; set; }
        public static string ClaimNumber { get; set; }
        public static DateTime FollowUpDate { get; set; }
        public static string ClaimStatus { get; set; }
        public static string FollowUpRemarks { get; set; }
        public static float ReceivingAmount { get; set; }
        public static float Difference { get; set; }
        public static string DifferenceRemarks { get; set; }
    }
}
